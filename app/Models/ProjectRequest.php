<?php

namespace App\Models;

use Database\Factories\ProjectRequestFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ProjectRequest extends Model
{
    use HasFactory;

    protected $table = 'project_request';

    public function projects(): BelongsToMany
    {
        return $this->belongsToMany(Project::class, 'project_request');
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'project_request');
    }


    /**
     * Create a new factory instance for the model.
     *
     * @return ProjectRequestFactory
     */
    protected static function newFactory(): ProjectRequestFactory
    {
        return ProjectRequestFactory::new();
    }

}
