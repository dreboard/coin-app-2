<?php

namespace App\Models;

use App\Models\Groups\Group;
use Database\Factories\AnnouncementFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Announcement extends Model
{
    use HasFactory;


    protected $fillable = [
        'name',
        'group_id',
        'user_id',
        'message',
    ];


    /**
     * The roles that belong to the user.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }


    /**
     * The roles that belong to the group.
     */
    public function group(): BelongsTo
    {
        return $this->belongsTo(Group::class);
    }



    /**
     * Create a new factory instance for the model.
     *
     * @return Factory
     */
    protected static function newFactory(): Factory
    {
        return AnnouncementFactory::new();
    }
}
