<?php

namespace App\Models;

use Database\Factories\ForumFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Forum extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'group_id',
        'title',
        'body',
        'image_url',
        'slug',
        'publish',
    ];

    protected $appends = ['first_image'];

    protected $with = ['comments','images','user:id,name'];

    public function getFirstImageAttribute() {
        return $this->images->first();
    }

    /**
     * Get the model's images.
     */
    public function images(): MorphMany
    {
        return $this->morphMany(Image::class, 'imageable');
    }
    public function previewImage(): MorphOne
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    /**
     * Get user for the model.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get all the comments for the model.
     */
    public function comments(): MorphMany
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }

    /**
     * Get all the tags for the model.
     */
    public function tags(): MorphToMany
    {
        return $this->morphToMany(Tag::class, 'taggable', 'post_tag');
    }

    /**
     * Get the post that owns the comment.
     */
    public function participants(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'participants', 'forum_id', 'user_id');
    }

    /**
     * Get the parent forumable model (post or other model).
     */
    public function forumable(): MorphTo
    {
        return $this->morphTo();
    }


    /**
     * Create a new factory instance for the model.
     *
     * @return ForumFactory
     */
    protected static function newFactory(): ForumFactory
    {
        return ForumFactory::new();
    }
}
