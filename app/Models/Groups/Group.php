<?php
/**
 * Group | model for groups
 *
 * @package Groups
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Models\Groups;

use App\Helpers\ManageGroupHelper;
use App\Models\Announcement;
use App\Models\User;
use Database\Factories\GroupFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Overtrue\LaravelFollow\Traits\Followable;

class Group extends Model
{
    use HasFactory, Notifiable;

    use Followable;

    protected $fillable = [
        'name',
        'group_type',
        'user_id',
        'description',
        'short_description',
        'ana',
        'image_url',
        'url',
        'first_name',
        'last_name',
        'phone',
        'email',
        'place',
        'address',
        'postalcode',
        'city',
        'state',
        'meets',
        'formed',
        'specialty',
        'private',
        'extra_info',
        'settings',
    ];


    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'group_user', 'user_id', 'group_id');
    }

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class)->using(Position::class);
    }

    public function requests(): HasMany
    {
        return $this->hasMany(User::class, 'group_request');
    }

    public function events(): HasMany
    {
        return $this->hasMany(Event::class, 'group_id');
    }



    public function board_members(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'group_role', 'group_id')
            ->withPivot(['role_id'])
            ->wherePivotIn('role_id', ManageGroupHelper::GROUP_BOARD_POSITIONS);
    }





    public function announcements(): HasMany
    {
        return $this->hasMany(Announcement::class, 'group_id');
    }



    /**
     * Create a new factory instance for the model.
     *
     * @return Factory
     */
    protected static function newFactory(): Factory
    {
        return GroupFactory::new();
    }

}
