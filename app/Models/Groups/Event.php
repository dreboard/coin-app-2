<?php
/**
 * Event | model for group events
 *
 * @package Groups
 * @subpackage Events
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Models\Groups;

use App\Models\User;
use Database\Factories\Groups\EventFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 *
 */
class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'event_type',
        'location',
        'address',
        'postalcode',
        'city',
        'state',
        'description',
        'group_id',
        'user_id',
        'start_at',
        'end_at',
    ];

    /**
     *
     */
    public  const GROUP_EVENT_TYPES = [0 => 'Board Meeting', 1 => 'Meetup',  2 => 'General', 3 =>  'Show', 4 => 'Committee', 5 =>  'Club', 6 =>  'Special'];

    /**
     *
     */
    public  const GROUP_EVENT_LOCATIONS = [0 => 'Virtual', 1 => 'Physical'];


    /**
     * @return BelongsTo
     */
    public function group(): BelongsTo
    {
        return $this->belongsTo(Group::class, 'group_id');
    }

    /**
     * Get the agendas for the event.
     */
    public function agendas(): HasMany
    {
        return $this->hasMany(Agenda::class);
    }

    /**
     * Get the agendas for the event.
     */
    public function participants(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'event_user', 'event_id', 'participant')
            ->withTimestamps();
    }


    /**
     * Create a new factory instance for the model.
     *
     * @return EventFactory
     */
    protected static function newFactory(): EventFactory
    {
        return EventFactory::new();
    }
}
