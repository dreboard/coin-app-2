<?php

namespace App\Models\Groups;

use App\Models\User;
use Database\Factories\Groups\CommitteeFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Committee extends Model
{
    use HasFactory;


    protected $fillable = [
        'group_id',
        'title',
        'chair',
        'seats',
        'details',
        'type',
        'approved_at',
        'expired_at',
    ];


    /**
     * @return BelongsTo
     */
    public function group(): BelongsTo
    {
        return $this->belongsTo(Group::class, 'group_id');
    }

    /**
     * Get the agendas for the event.
     */
    public function agendas(): HasMany
    {
        return $this->hasMany(Agenda::class);
    }

    /**
     * Get the chair.
     */
    public function chair(): BelongsTo
    {
        return $this->belongsTo(User::class, 'chair');
    }

    /**
     * Get the members
     */
    public function members(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'committee_user', 'committee_id', 'user_id')
            ->withTimestamps();
    }

    /**
     * Get the reports.
     */
    public function reports(): MorphMany
    {
        return $this->morphMany(Report::class, 'reportable');
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return CommitteeFactory
     */
    protected static function newFactory(): CommitteeFactory
    {
        return CommitteeFactory::new();
    }
}
