<?php

namespace App\Models\Groups;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Position extends pivot
{
    use HasFactory;

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'group_role','user_id');
    }
    public function groups(): BelongsToMany
    {
        return $this->belongsToMany(Group::class, 'group_role','group_id');
    }
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'group_role','role_id');
    }
}
