<?php
/**
 * Library | Model for Group library
 *
 * @package Groups
 * @subpackage Media
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Models\Groups;

use App\Models\User;
use Database\Factories\Groups\LibraryFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Library extends Model
{
    use HasFactory;

    protected $table = 'library';

    protected $fillable = [
        'group_id',
        'publication_type',
        'publication_title',
        'isbn',
        'details',
        'value',
        'status',
        'received'
    ];


    /**
     * Get the agendas for the event.
     */
    public function lendees(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'library_user', 'library_id', 'user_id')
            ->withTimestamps();
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return LibraryFactory
     */
    protected static function newFactory(): LibraryFactory
    {
        return LibraryFactory::new();
    }
}
