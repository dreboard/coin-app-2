<?php

namespace App\Models\Groups;

use Database\Factories\IssueFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Issue extends Model
{
    use HasFactory;

    protected $table = 'issues';

    protected $fillable = [
        'issue','transparency','vote_level','group_id','start_at','expired_at'
    ];


    public function votes(): HasMany
    {
        return $this->hasMany(IssueVote::class, 'issue_id');
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return IssueFactory
     */
    protected static function newFactory(): IssueFactory
    {
        return IssueFactory::new();
    }
}
