<?php

namespace App\Models\Groups;

use App\Models\User;
use Database\Factories\NomineeFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class GroupNominations extends Model
{
    use HasFactory;

    protected $table = 'group_nominees';

    protected $fillable = [
        'role_id',
        'nominee',
        'nominator',
        'start_at',
        'expired_at',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'nominee');
    }

    public function election(): BelongsTo
    {
        return $this->belongsTo(GroupElection::class);
    }

    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class, 'role_id');
    }


    /**
     * Create a new factory instance for the model.
     *
     * @return Factory
     */
    protected static function newFactory(): Factory
    {
        return NomineeFactory::new();
    }
}
