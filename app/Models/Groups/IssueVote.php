<?php

namespace App\Models\Groups;

use App\Models\User;
use Database\Factories\IssueVoteFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class IssueVote extends Model
{
    use HasFactory;

    protected $fillable = [
        'issue_id',
        'voter',
        'vote',
    ];

    protected $table = 'issue_votes';

    public function issue(): BelongsTo
    {
        return $this->belongsTo(Issue::class, 'issue_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'voter');
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return IssueVoteFactory
     */
    protected static function newFactory(): IssueVoteFactory
    {
        return IssueVoteFactory::new();
    }
}
