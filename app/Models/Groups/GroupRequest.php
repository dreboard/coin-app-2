<?php

namespace App\Models\Groups;

use App\Models\User;
use Database\Factories\GroupRequestFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class GroupRequest extends Model
{
    use HasFactory;


    protected $table = 'group_request';

    public function groups(): BelongsToMany
    {
        return $this->belongsToMany(Group::class, 'group_request');
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'group_request');
    }


    /**
     * Create a new factory instance for the model.
     *
     * @return Factory
     */
    protected static function newFactory(): Factory
    {
        return GroupRequestFactory::new();
    }
}
