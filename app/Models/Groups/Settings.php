<?php

namespace App\Models\Groups;

use Database\Factories\Groups\SettingsFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Settings extends Model
{
    use HasFactory;

    protected $table = 'group_settings';

    protected $fillable = [
        'group_id',
        'edit',
        'posts',
        'events',
        'committees',
        'officers',
        'positions',
        'announcements',
        'members',
        'elections',
        'media',
    ];

    /**
     * Get user for the post.
     */
    public function group(): BelongsTo
    {
        return $this->belongsTo(Group::class);
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return SettingsFactory
     */
    protected static function newFactory(): SettingsFactory
    {
        return SettingsFactory::new();
    }
}
