<?php
/**
 * Agenda | model for group agendas
 *
 * @package Groups
 * @subpackage Events
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Models\Groups;

use Database\Factories\AgendaFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Agenda extends Model
{
    use HasFactory;

    protected $table = 'agendas';

    protected $fillable = [
        'topic',
        'action',
        'description',
        'event_id',
        'group_id',
        'length',
    ];

    public  const GROUP_AGENDA_ACTIONS = [0 => 'Skipped', 1 => 'Completed'];

    /**
     * Get the group that owns the agenda.
     */
    public function group(): BelongsTo
    {
        return $this->belongsTo(Group::class, 'group_id');
    }

    /**
     * Get the event that owns the agenda.
     */
    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class, 'event_id');
    }

    /**
     * Get the issues for the agenda.
     */
    public function issues(): HasMany
    {
        return $this->hasMany(Issue::class, 'issue_id');
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return AgendaFactory
     */
    protected static function newFactory(): AgendaFactory
    {
        return AgendaFactory::new();
    }
}
