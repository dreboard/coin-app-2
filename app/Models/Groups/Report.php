<?php
/**
 * Report | Model for Group Board and Committee Reports
 *
 * @package Groups
 * @subpackage Reporting
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Models\Groups;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Database\Factories\Groups\ReportFactory;

class Report extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'type',
        'overview',
        'background',
        'discussion',
        'budget',
        'conclusion',
        'recommendations',
        'group_id',
        'user_id',
        'published',
        'reportable_id',
        'reportable_type',
    ];

    /**
     * Get the parent reportable model (group or committee).
     */
    public function reportable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return ReportFactory
     */
    protected static function newFactory(): ReportFactory
    {
        return ReportFactory::new();
    }
}
