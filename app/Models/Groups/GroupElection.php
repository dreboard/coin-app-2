<?php

namespace App\Models\Groups;

use Database\Factories\ElectionFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class GroupElection extends Model
{
    use HasFactory;

    public $fillable = [
        'name','election_type','group_id','roles','start_at','expired_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //'roles' => 'array',
    ];

    public function nominees(): HasMany
    {
        return $this->hasMany(GroupNominations::class, 'election_id');
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return Factory
     */
    protected static function newFactory(): Factory
    {
        return ElectionFactory::new();
    }


}
