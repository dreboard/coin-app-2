<?php

namespace App\Models\Groups;

use App\Models\User;
use Database\Factories\VotesFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Vote extends Model
{
    use HasFactory;


    public function candidate(): BelongsTo
    {
        return $this->belongsTo(User::class, 'candidate');
    }

    public function voter(): BelongsTo
    {
        return $this->belongsTo(User::class, 'voter');
    }

    public function election(): BelongsTo
    {
        return $this->belongsTo(GroupElection::class, 'election_id');
    }

    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class, 'role_id');
    }


    /**
     * Create a new factory instance for the model.
     *
     * @return VotesFactory
     */
    protected static function newFactory(): VotesFactory
    {
        return VotesFactory::new();
    }
}
