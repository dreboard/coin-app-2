<?php

namespace App\Models\Users;

use App\Models\User;
use Database\Factories\ViolationFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Violation extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'violation_type',
        'violator',
        'details',
        'reporter',
        'screenshot',
        'url',
        'action',
    ];


    public function violatingUser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'violator_id');
    }

    public function reportingUser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'reporter');
    }


    /**
     * Create a new factory instance for the model.
     *
     * @return Factory
     */
    protected static function newFactory(): Factory
    {
        return ViolationFactory::new();
    }
}
