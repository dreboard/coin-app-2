<?php
/**
 * User Model | model for views/users
 *
 * @package Users
 * @subpackage Manage
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @see Online:         {https://www.tutsmake.com/laravel-8-check-user-online-or-not-tutorial/?ref=morioh.com&utm_source=morioh.com}
 * @see Impersonate:    {https://github.com/404labfr/laravel-impersonate}
 * @see Bannable:       {https://github.com/cybercog/laravel-ban}
 * @see Likes:          {https://github.com/overtrue/laravel-like}
 * @copyright none
 */
namespace App\Models;

use App\Models\Groups\Event;
use App\Models\Groups\Group;
use App\Models\Groups\Role;
use App\Models\Users\Login;
use App\Models\Users\Violation;
use App\Models\Users\Warning;
use Cog\Contracts\Ban\Bannable as BannableContract;
use Cog\Laravel\Ban\Traits\Bannable;
use Database\Factories\UserFactory;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Lab404\Impersonate\Models\Impersonate;
use Laravel\Sanctum\HasApiTokens;
use Overtrue\LaravelFollow\Traits\Follower;
use Overtrue\LaravelFollow\Traits\Followable;
use Overtrue\LaravelLike\Traits\Likeable;
use Overtrue\LaravelLike\Traits\Liker;


class User extends Authenticatable implements MustVerifyEmail, BannableContract
{
    use HasApiTokens, HasFactory, Notifiable;
    use Impersonate;
    use Bannable;
    use Follower;
    use Followable;
    use Liker, Likeable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'banned_until',
        'public_message'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];


    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'banned_at' => 'datetime',
        'banned_until' => 'datetime',
    ];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var mixed
     */
    private mixed $is_admin;


    //protected $with = ['group_roles', 'groups'];

    /**
     * Can a user clone another
     *
     * @return bool
     */
    public function canImpersonate(): bool
    {
        return $this->is_admin == 1;
    }

    /**
     * Can a user be cloned
     *
     * @return bool
     */
    public function canBeImpersonated(): bool
    {
        return !$this->is_admin;
    }

    /**
     * User club manage level
     *
     * @param int $group_id
     * @return int|mixed
     */
    public function userManageLevel(int $group_id)
    {
        return DB::table('group_user')->select('manage')->where('group_id', $group_id)->where('user_id', $this->id)
            ->first()->manage ?? 0;
    }

    /**
     * Is user in club
     *
     * @param int $group_id
     * @return false|Model|Builder|object
     */
    public function isClubUser(int $group_id)
    {
        return DB::table('group_user')->select('member_type')->where('group_id', $group_id)
            ->where('user_id', $this->id)
            ->first() ?? false;
    }

    /**
     * Getter for Status of banned user
     *
     * @param $value
     * @return string
     */
    public function getAccountStatusAttribute($value): string
    {
        if ($value == 1){
            return 'Active';
        }
        return 'Suspended';
    }


    /**
     * Relationship has many Login::class
     *
     * @return HasMany
     */
    public function logins(): HasMany
    {
        return $this->hasMany(Login::class);
    }

    /**
     * Relationship has many Warning::class
     *
     * @return HasMany
     */
    public function warnings(): HasMany
    {
        return $this->hasMany(Warning::class);
    }

    /**
     * Relationship has many Violation::class
     *
     * @return HasMany
     */
    public function violations(): HasMany
    {
        return $this->hasMany(Violation::class, 'reporter');
    }

    /**
     * The groups that belong to the user.
     */
    public function groups(): BelongsToMany
    {
        return $this->belongsToMany(Group::class, 'group_user', 'group_id', 'user_id');
    }

    /**
     * The groups that belong to the user.
     */
    public function requests(): BelongsToMany
    {
        return $this->belongsToMany(Group::class, 'group_request', 'group_id', 'user_id')
            ->wherePivot('type', 'request');
    }

    /**
     * The groups that belong to the user.
     */
    public function invites(): BelongsToMany
    {
        return $this->belongsToMany(Group::class, 'group_request', 'group_id', 'user_id')
            ->wherePivot('type', 'invite');
    }

    /**
     * The project that belong to the user.
     */
    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class, 'user_id');
    }

    /**
     * The projects that belong to the user.
     */
    public function projects(): BelongsToMany
    {
        return $this->belongsToMany(Project::class, 'project_user', 'project_id', 'user_id');
    }

    /**
     * The events that belong to the user.
     */
    public function events(): BelongsToMany
    {
        return $this->belongsToMany(Event::class, 'event_user', 'event_id', 'participant');
    }

    /**
     * The roles that belong to the user.
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'group_role', 'group_id', 'user_id');
    }


    /**
     * Relationship has many Comment::class
     *
     * @return HasMany
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class, 'user_id');
    }


    /**
     * Relationship has many Forum::class
     *
     * @return HasMany
     */
    public function forums(): HasMany
    {
        return $this->hasMany(Forum::class, 'user_id');
    }


    /**
     * Relationship has many Post::class
     *
     * @return HasMany
     */
    public function posts(): HasMany
    {
        return $this->hasMany(Post::class, 'user_id');
    }

    /**
     * Did the user create a group
     *
     * @param $group_id
     * @return bool
     */
    public function hasGroup($group_id): bool
    {
        //dd($group_id);
        return DB::table('group_user')
            ->where('group_user.group_id', $group_id)
            ->where('group_user.user_id', $this->id)
            ->exists();
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return Factory
     */
    protected static function newFactory(): Factory
    {
        return UserFactory::new();
    }

}
