<?php
/**
 * Comment | model for comments
 *
 * @package Mixed
 * @subpackage All
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Models;

use Database\Factories\CommentFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Overtrue\LaravelLike\Traits\Likeable;

class Comment extends Model
{
    use HasFactory;

    use Likeable;

    protected $fillable = ['user_id', 'post_id', 'parent_id', 'body'];

    protected $appends = [
        'commentableCount', 'replyCount'
    ];

    protected $with = ['user', 'replies', 'images'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the parent commentable model (post or other model).
     */
    public function commentable(): MorphTo
    {
        return $this->morphTo();
    }

    public function replies(): HasMany
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }

    /**
     * Get the comments's image.
     */
    public function images(): MorphMany
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function getReplyCountAttribute()
    {
        return $this->replies()->count();
    }

    public function getCommentableCountAttribute()
    {
        return $this->commentable()->count();
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return CommentFactory
     */
    protected static function newFactory(): CommentFactory
    {
        return CommentFactory::new();
    }

}
