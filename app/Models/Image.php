<?php
/**
 * Image | model for images
 *
 * @package Images
 * @subpackage All
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @see https://github.com/overtrue/laravel-follow
 * @copyright none
 */
namespace App\Models;

use Database\Factories\ImageFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Overtrue\LaravelLike\Traits\Likeable;

class Image extends Model
{
    use HasFactory;

    use Likeable;

    protected $fillable = ['image_url', 'user_id', 'title'];


    public static function boot() {
        parent::boot();

        static::deleting(function($image) {
            foreach ($image->comments as $comment){
                $comment->delete();
            }

            $image->comments()->delete();
        });



    }

    /**
     * Get the parent imageable model (user or post).
     */
    public function imageable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * Get user for the model.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }


    /**
     * Get all the posts for the model.
     */
    public function post(): BelongsTo
    {
        return $this->belongsTo(Post::class, 'imageable_id')
            ->where('images.imageable_type', Post::class);
    }


    /**
     * Get all the comments for the model.
     */
    public function forum(): BelongsTo
    {
        return $this->belongsTo(Forum::class, 'imageable_id')
            ->where('images.imageable_type', Forum::class);
    }

    /**
     * Get all the comments for the model.
     */
    public function comments(): MorphMany
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }


    /**
     * Get all the gallery for the model.
     * @useage = $images = Image::whereBelongsTo($gallery)->get();
     */
    public function gallery(): BelongsTo
    {
        return $this->belongsTo(Gallery::class);
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return ImageFactory
     */
    protected static function newFactory(): ImageFactory
    {
        return ImageFactory::new();
    }
}
