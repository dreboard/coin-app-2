<?php
/**
 * Coin | model for coin types
 *
 * @package Coins
 * @subpackage Display
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Models\Coins;

use App\Models\Forum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * @method static findOrFail(int $type_id)
 */
class CoinType extends Model
{

    protected $table = 'cointypes';

    public $timestamps = false;

    protected $casts = ['mms' => 'array'];

    public function helper()
    {
        if(class_exists("App\Helpers\Types\\".str_replace(' ', '', $this->coinType))){
            return "App\Helpers\Types\\".str_replace(' ', '', $this->coinType);
        }
        return false;
    }


    public function keyDates()
    {
        return Coin::select('id','coinName','keyDate')
            ->where('cointypes_id',$this->id)
            ->where('keyDate',1)
            ->get();
    }


    public function semiKeyDates()
    {
        return Coin::select('id','coinName','semiKeyDate')
            ->where('cointypes_id',$this->id)
            ->where('semiKeyDate',1)
            ->get();
    }


    public function mints()
    {
        if(class_exists("App\Helpers\Types\\".str_replace(' ', '', $this->coinType))){
            return "App\Helpers\Types\\".str_replace(' ', '', $this->coinType);
        }
        return false;
    }
    public function image(): string
    {
        return 'cdn.dev-php.site/public/img/coins/'.str_replace(' ', '_', $this->coinType).'jpg';
    }

    /**
     * Get the comments for the blog post.
     */
    public function coins(): HasMany
    {
        return $this->hasMany(Coin::class, 'cointypes_id')->orderBy('coinYear');
    }


    /**
     * Get the category.
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(CoinCategory::class, 'coincats_id');
    }

    /**
     * Get the comments for the blog post.
     */
    public function rolls(): HasMany
    {
        return $this->hasMany(MintRoll::class, 'cointypes_id');
    }


    /**
     * Get the model's images.
     */
    public function forum(): MorphMany
    {
        return $this->morphMany(Forum::class, 'forumable');
    }



}
