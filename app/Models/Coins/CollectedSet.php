<?php

namespace App\Models\Coins;

use App\Models\Image;
use App\Models\Note;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class CollectedSet extends Model
{
    use HasFactory;

    protected $table = 'collected_sets';

    protected $with = ['collected','mintset'];


    /**
     * Get the coin.
     */
    public function mintset(): BelongsTo
    {
        return $this->belongsTo(Mintset::class, 'set_id');
    }

    /**
     * The users that belong to the role.
     */
    public function collected(): HasMany
    {
        return $this->hasMany(Collected::class, 'set_id', 'id');
    }

    /**
     * Get the model's images.
     */
    public function images(): MorphMany
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    /**
     * Get the model's images.
     */
    public function notes(): MorphMany
    {
        return $this->morphMany(Note::class, 'notable');
    }
}
