<?php

namespace App\Models\Coins;

use App\Models\Comment;
use App\Models\Image;
use App\Models\Note;
use Database\Factories\CollectedFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Collected extends Model
{
    use HasFactory;

    protected $table = 'collected'; // collected_coins

    protected $fillable = [
        'nickname',
        'coin_id',
        'sub_type',
        'obv',
        'rev',
        'mms',
        'private',
        'circulated','gradable','cac_sticker','chop_mark_place','chop_mark',
        'tpg_service',
        'tpg_serial_num',
        'slab_condition',
        'special_label',
        'grade','ten_grade','adjectival_grade','strike_attribute','strike_appearance',
        'ngc_star','ngc_plus','pcgs_plus',
        'gradable',
        'color',
        'fullAtt',
        'toned',
        'proof_like',
        'designation',
        'strike_character',
        'slab_generation',
        'anacs_grade_designator',
        'damaged',
        'holed',
        'cleaned',
        'polished',
        'altered',
        'scratched',
        'pvc_damage',
        'corrosion',
        'bent',
        'plugged',
        'whizzing',
        'counterstamp',
        'bag_mark',
        'fingermarks',
        'spotted',
        'slide_marks',
        'lot_id','set_id','roll_id','folder_id','firstday_id',
        'private','locked','favorite','views','value','cost',
        'die_stage','error_coin','variety_coin'
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['coin'];


    public static function boot() {
        parent::boot();

        static::deleting(function() {

            foreach ($this->comments() as $comment) {
                $comment->delete();
            }
            foreach ($this->images() as $image){
                $image->delete();
            }
            foreach ($this->notes() as $note){
                $note->delete();
            }
        });
    }

    /**
     * Scope a query to only include popular users.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeType(Builder $query): Builder
    {
        return $query->where('votes', '>', 100);
    }

    /**
     * Get the coin.
     */
    public function coin(): BelongsTo
    {
        return $this->belongsTo(Coin::class, 'coin_id');
    }

    /**
     * Get the mintset.
     */
    public function collectedSet(): BelongsTo
    {
        return $this->belongsTo(CollectedSet::class, 'set_id', 'id');
    }

    /**
     * Get the mintset.
     */
    public function collectedRoll(): BelongsTo
    {
        return $this->belongsTo(CollectedRoll::class, 'roll_id', 'id');
    }

    /**
     * Get the mintset.
     */
    public function collectedFolder(): BelongsTo
    {
        return $this->belongsTo(CollectedFolder::class, 'firstday_id', 'id');
    }

    /**
     * Get the mintset.
     */
    public function collectedLot(): BelongsTo
    {
        return $this->belongsTo(CollectedLot::class, 'lot_id', 'id');
    }

    /**
     * Get the mintset.
     */
    public function collectedBulk(): BelongsTo
    {
        return $this->belongsTo(CollectedLot::class, 'lot_id', 'id');
    }

    /**
     * Get the mintset.
     */
    public function collectedFirstDay(): BelongsTo
    {
        return $this->belongsTo(CollectedFirstDay::class, 'folder_id', 'id');
    }

    /**
     * Get the model's images.
     */
    public function images(): MorphMany
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    /**
     * Get the model's images.
     */
    public function notes(): MorphMany
    {
        return $this->morphMany(Note::class, 'notable');
    }

    /**
     * Get all the comments for the model.
     */
    public function comments(): MorphMany
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }

    /**
     * Get all the varieties for the model.
     */
    public function varieties(): BelongsToMany
    {
        return $this->belongsToMany(Variety::class, 'collected_variety', 'collected_id', 'variety_id');
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return CollectedFactory
     */
    protected static function newFactory(): CollectedFactory
    {
        return CollectedFactory::new();
    }


}
