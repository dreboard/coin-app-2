<?php

namespace App\Models\Coins;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class CollectedRoll extends Model
{
    use HasFactory;

    protected $table = 'collected_rolls';

    /**
     * The users that belong to the role.
     */
    public function collected(): HasMany
    {
        return $this->hasMany(Collected::class, 'roll_id', 'id');
    }
}
