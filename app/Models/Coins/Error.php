<?php

namespace App\Models\Coins;

use App\Models\Forum;
use App\Models\Note;
use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Error extends Model
{
    use HasFactory;

    protected $table = 'coins_errors';



    /**
     * Get all the varieties for the model.
     */
    public function collected(): BelongsToMany
    {
        return $this->belongsToMany(Collected::class, 'collected_error', 'error_id', 'collected_id');
    }


    /**
     * Get the model's forum posts.
     */
    public function forum(): MorphMany
    {
        return $this->morphMany(Forum::class, 'forumable');
    }


    /**
     * Get the model's images.
     */
    public function posts(): MorphMany
    {
        return $this->morphMany(Post::class, 'postable');
    }


    /**
     * Get the model's images.
     */
    public function notes(): MorphMany
    {
        return $this->morphMany(Note::class, 'notable');
    }


}
