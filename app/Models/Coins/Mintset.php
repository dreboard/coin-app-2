<?php

namespace App\Models\Coins;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Mintset extends Model
{
    use HasFactory;

    protected $table = 'mintset';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        //'coins' => 'array',
    ];


    /**
     * The users that belong to the role.
     */
    public function collectedSets(): HasMany
    {
        return $this->hasMany(CollectedSet::class, 'set_id', 'id');
    }


    /**
     * Get the coin.
     */
    public function coins()
    {
        return Coin::findMany(explode(',', $this->coins));
    }
}
