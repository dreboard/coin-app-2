<?php

namespace App\Models\Coins;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Variety extends Model
{
    protected $table = 'coins_variety';


    /**
     * Get the coin.
     */
    public function coin(): BelongsTo
    {
        return $this->belongsTo(Coin::class, 'coin_id', 'id');
    }

    /**
     * Get all the varieties for the model.
     */
    public function collected(): BelongsToMany
    {
        return $this->belongsToMany(Collected::class, 'collected_variety', 'variety_id', 'collected_id');
    }
}
