<?php

namespace App\Models\Coins;

use Illuminate\Database\Eloquent\Model;

class ViewSeatedLiberty extends Model
{
    public $table = "view_seated_liberty";
}
