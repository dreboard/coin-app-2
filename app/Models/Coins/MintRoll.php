<?php

namespace App\Models\Coins;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MintRoll extends Model
{

    protected $table = 'rollsmint';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['coin'];


    /**
     * Get the category.
     */
    public function coin(): BelongsTo
    {
        return $this->belongsTo(Coin::class, 'coin_id');
    }
}
