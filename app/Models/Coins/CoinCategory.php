<?php

namespace App\Models\Coins;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class CoinCategory extends Model
{


    protected $table = 'coincategories';


    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['types'];

    /**
     * Get the coins
     */
    public function coins(): HasMany
    {
        return $this->hasMany(Coin::class, 'coincats_id');
    }


    /**
     * Get the types.
     */
    public function types(): HasMany
    {
        return $this->hasMany(CoinType::class, 'coincats_id');
    }

}
