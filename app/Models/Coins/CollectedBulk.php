<?php

namespace App\Models\Coins;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class CollectedBulk extends Model
{
    use HasFactory;

    protected $table = 'collected_bulk';

    /**
     * The users that belong to the role.
     */
    public function collected(): HasMany
    {
        return $this->hasMany(CollectedBulk::class, 'bulk_id', 'id');
    }
}
