<?php

namespace App\Models\Coins;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class CollectedLot extends Model
{
    use HasFactory;

    protected $table = 'coin_lots';

    /**
     * The users that belong to the role.
     */
    public function collected(): HasMany
    {
        return $this->hasMany(Collected::class, 'lot_id', 'id');
    }
}
