<?php
/**
 * Coin | model for coins
 *
 * @package Coins
 * @subpackage Display
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Models\Coins;

use App\Helpers\Types\TypeHelper;
use App\Models\Forum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Facades\DB;

/**
 * @property int $id
 * @property string $mintMark
 * @property int $coinYear
 * @property string $coinCategory
 * @property string $coinType
 * @property int $cointypes_id
 * @property int $coincats_id
 * @property string $coinSubCategory
 * @property string $coinName
 * @property string $coinVersion
 * @property string $coinSize
 * @property string $coinMetal
 * @property string $strike
 * @property int $commemorative
 * @property string $commemorativeVersion
 * @property string $commemorativeCategory
 * @property string $commemorativeType
 * @property string $commemorativeGroup
 * @property int $byMint
 * @property int $byMintGold
 * @property float $denomination
 * @property int $keyDate
 * @property int $semiKeyDate
 * @property int $errorCoin
 * @property int $varietyCoin
 * @property string $varietyType
 * @property int $century
 * @property int $release
 * @property string $state
 * @property string $rarity
 * @property string $genre
 * @property string $subGenre
 * @property string $subGenre2
 * @property string $series
 * @property string $seriesType
 * @property string $design
 * @property string $designType
 * @property string $mms
 * @property string $mms2
 * @property string $mms3
 * @property string $mms4
 * @property string $dateSize
 * @property string $obv
 * @property string $obv2
 * @property string $obv3
 * @property string $rev
 * @property string $rev2
 * @property string $rev3
 */
class Coin extends Model
{

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['type', 'category'];


    /**
     * Get the type.
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(CoinType::class, 'cointypes_id');
    }

    /**
     * Get the category.
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(CoinCategory::class, 'coincats_id');
    }

    /**
     * Get the comments for the blog post.
     */
    public function varieties(): HasMany
    {
        return $this->hasMany(CoinVariety::class, 'coin_id');
    }

    // with a relationship
    public function publishedPosts(): HasMany
    {
        return $this->hasMany(CoinVariety::class)->where('published', true);
    }

    /**
     * Get the model's forum posts.
     */
    public function forum(): MorphMany
    {
        return $this->morphMany(Forum::class, 'forumable');
    }
    /**
     * Get the current pricing for the product.
     */
    public function currentPricing(): HasOne
    {
        return $this->hasOne(CoinVariety::class)->ofMany([
            'published_at' => 'max',
            'id' => 'max',
        ], function ($query) {
            $query->where('published_at', '<', now());
        });
    }

    /**
     * Get the mintsets.
     */
    public function mintsets()
    {
        if(in_array($this->coinType, TypeHelper::MINTSET_TYPES)){
            return DB::table('mintsets')
                ->select('id', 'setName')
                ->whereIn('coins',$this->id)
                ->get();
        }
        return [];
    }

}
