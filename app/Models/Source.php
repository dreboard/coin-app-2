<?php
/**
 * Source | model for sources, articles and books
 *
 * @package Writing
 * @subpackage Sources
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Source extends Model
{
    use HasFactory;



    /**
     * Get all the reviews.
     */
    public function reviews(): MorphMany
    {
        return $this->morphMany(Review::class, 'reviewable');
    }

}
