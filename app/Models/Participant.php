<?php
/**
 * Participant | model for participants
 *
 * @package Mixed
 * @subpackage All
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Participant extends Model
{
    use HasFactory;

    protected $table = 'participants';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'forum_id',
        'last_read'
    ];

    public function user(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'participants', 'user_id', 'forum_id');
    }

    /**
     * Get the post that owns the comment.
     */
    public function forums(): BelongsToMany
    {
        return $this->belongsToMany(Forum::class, 'participants', 'forum_id', 'user_id');
    }


}
