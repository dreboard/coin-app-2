<?php

namespace App\Models;

use Database\Factories\GalleryFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Overtrue\LaravelLike\Traits\Likeable;

class Gallery extends Model
{
    use HasFactory;
    use Likeable;
    protected string $tale = 'galleries';

    protected $fillable = [
        'user_id',
        'title',
        'description',
    ];

    protected $with = ['user','images'];

    /**
     * Get user for the post.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the parent galleryable model (post or other model).
     */
    public function galleryable(): MorphTo
    {
        return $this->morphTo();
    }

    public function images(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'gallery_image', 'image_id', 'gallery_id');
    }


    /**
     * Get all the comments for the model.
     */
    public function comments(): MorphMany
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return GalleryFactory
     */
    protected static function newFactory(): GalleryFactory
    {
        return GalleryFactory::new();
    }
}
