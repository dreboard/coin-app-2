<?php

namespace App\Mail\Users;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WarnUserMailable extends Mailable
{
    use Queueable, SerializesModels;

    private array $data = [
        'subject' => 'Your account is now in warning status',
        'body' => 'You have been issued a warning for violating the policies of this website.',
        'from' => 'The Administrator'
    ];

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(private readonly array $warning_data){}

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): static
    {
        return $this
            ->subject($this->warning_data['subject'] ?? $this->data['subject'])
            ->view('email.user.warn_user');
    }
}
