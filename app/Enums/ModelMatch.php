<?php

namespace App\Enums;

enum ModelMatch: string
{
    case ERROR = 'App\Models\Coins\Error';
    case COIN = 'App\Models\Coins\Coin';
    case VARIETY = 'App\Models\Coins\Variety';
    case PROJECT = 'App\Models\Project';
}
