<?php

namespace App\Repositories\Admin;

use App\Models\User;
use App\Models\Users\Login;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;

class AdminUserRepository
{


    public function __construct()
    {
    }


    /**
     * View all users
     *
     * @return Application|Factory|View
     */
    public function getAllUsers()
    {
        return User::withoutBanned()->with('logins')->get();
        //return User::withoutBanned()->get();
    }

    /**
     * Count of all users not banned
     *
     * @return Application|Factory|View
     */
    public function getAllUsersWithoutBannedCount()
    {
        return User::withoutBanned()->count();
    }

    /**
     * Count of all users with banned
     *
     * @return Application|Factory|View
     */
    public function getAllUsersWithBannedCount()
    {
        return User::withBanned()->count();
    }

    /**
     * @return int
     */
    public function getLoginTodayUsers(): int
    {
        return DB::table('logins')->whereDate(
            'created_at', DB::raw("DATE('now')")
        )->count();
    }

    /**
     * @return mixed
     */
    public function getLoginMonthUsers()
    {
        return Login::whereMonth('created_at', date('m'))->count();
    }

    /**
     * @return int
     */
    public function getLoginYearUsers(): int
    {
        return DB::table('logins')->whereYear('created_at', date('Y'))->count();
    }

    /**
     * @return int
     */
    public function countRegisteredUsersToday(): int
    {
        return DB::table('users')->whereDate(
            'created_at', DB::raw("DATE('now')")
        )->count();
    }

    /**
     * @return int
     */
    public function countRegisteredUsersMonth(): int
    {
        return User::whereMonth('created_at', date('m'))->count();
    }

    /**
     * @return int
     */
    public function countRegisteredUsersYear(): int
    {
        return DB::table('users')->whereYear('created_at', date('Y'))->count();
    }

}
