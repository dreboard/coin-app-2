<?php
/**
 * CollectedRepository | actions for CollectedController
 *
 * This class handles saving a collected coin: basic, folder, minset or other auto generated saving
 * @package Collected
 * @subpackage Coins
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Repositories\Coin;

use App\Helpers\GradeHelper;
use App\Models\Coins\Coin;
use App\Models\Coins\CoinType;
use App\Models\Coins\Collected;
use App\Models\Coins\Mintset;
use Illuminate\Support\Facades\DB;

class CollectedRepository
{






    /**
     * Return array of already collected qualifying coins for a set
     *
     * @param $coins
     * @return array
     */
    public function getOpenCollectedCoinsById($coins): array
    {
        $collected = [];
        foreach($coins as $coin){
            $collected[$coin->id]['coin'] = Collected::select('id','nickname', 'grade')
                ->where('user_id', auth()->user()->id)
                ->where('coin_id', $coin->id)
                ->where('set_id', 0)
                ->where('roll_id', 0)
                ->where('folder_id', 0)
                ->get();
            $collected[$coin->id]['grades'] = GradeHelper::gradeList($coin->strike);
            $collected[$coin->id]['count'] = count($collected[$coin->id]['coin']);
        }
        return $collected;
    }


    /**
     * Save all coins of a mintset
     *
     * @param int $set_id
     * @param int $collect_set_id
     * @return void
     */
    public function saveSetCoins(int $set_id, int $collect_set_id): void
    {
        $mintset = Mintset::findOrFail($set_id);
        foreach ($mintset->coins()->pluck('id') as $coin){
            $collected = new Collected;
            $collected->user_id = auth()->user()->id;
            $collected->coin_id = $coin;
            $collected->set_id = $collect_set_id;
            $collected->circulated = 0;
            $collected->locked = 1;
            $collected->save();
        }
    }

    /**
     * Save a coin to a mintset
     *
     * @param int $collect_set_id
     * @param int $coin_id
     * @return void
     */
    public function saveSetCoin(int $collect_set_id, int $coin_id)
    {
        $collected = new Collected;
        $collected->user_id = auth()->user()->id;
        $collected->coin_id = $coin_id;
        $collected->set_id = $collect_set_id;
        $collected->circulated = 0;
        $collected->locked = 1;
        $collected->save();
    }



    public function getCollectedCountByCoin(int $coin_id): int
    {
        return DB::table('collected')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->where('coins.id',$coin_id)
            ->get()->count();
    }
    public function getCollectedCountByCoinStrike(int $coin_id, string $strike): int
    {
        return DB::table('collected')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->where('coins.id',$coin_id)->where('coins.strike', $strike)
            ->get()->count();
    }

    public function getCollectedInvestmentByCoin(int $coin_id)
    {
        return DB::table('collected')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->where('coins.id',$coin_id)
            ->sum('cost') ?? 0.00;
    }

    public function getCollectedInvestmentByCoinStrike(int $coin_id, string $strike)
    {
        return DB::table('collected')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->where('coins.id',$coin_id)->where('coins.strike', $strike)
            ->sum('cost') ?? 0.00;
    }

    public function getCollectedBusinessStrikeCountByType(int $type_id)
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->where('coins.cointypes_id',$type_id)
            ->whereIn('coins.strike',GradeHelper::BUSINESS_STRIKE)
            ->get()->count();
    }

    public function getCollectedProofStrikeCountByType(int $type_id)
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->where('coins.cointypes_id',$type_id)
            ->whereIn('coins.strike',GradeHelper::PROOF_STRIKE)
            ->get()->count();
    }

    public function getCollectedSpecialStrikeCountByType(int $type_id)
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->where('coins.cointypes_id',$type_id)
            ->whereIn('coins.strike',GradeHelper::SPECIAL_STRIKE)
            ->get()->count();
    }


    public function getCollectedByCoin(mixed $coins): array
    {
        $cat_list = [];
        //$coins = Coin::select('id','coinName', 'coinYear', 'mintMark', 'strike')->where('cointypes_id', $type_id)->orderBy('coinYear')->get();//->toArray();
        foreach($coins as $coin){
            $cat_list[$coin->id]['coin'] = $coin;
            $cat_list[$coin->id]['collected'] = $this->getCollectedCountByCoin($coin->id);
            $cat_list[$coin->id]['investment'] = $this->getCollectedInvestmentByCoin($coin->id);
        }
        return $cat_list;
    }

    public function getCollectedByCoinStrike(int $type_id, string $strike): array
    {
        $cat_list = [];
        $coins = Coin::select('id','coinName', 'coinYear', 'mintMark', 'strike','cointypes_id')
            ->where('cointypes_id', $type_id)->where('strike', $strike)
            ->orderBy('coinYear')->get();
        foreach($coins as $coin){
            $cat_list[$coin->id]['coin'] = $coin;
            $cat_list[$coin->id]['collected'] = $this->getCollectedCountByCoinStrike($coin->id, $strike);
            $cat_list[$coin->id]['investment'] = $this->getCollectedInvestmentByCoinStrike($coin->id, $strike);
        }
        return $cat_list;
    }



    public function getTypeDesignVarieties(int $type_id): array
    {
        $design_variety_list = [];
        $design_variety_list['obverse_varieties'] = DB::table('coins_variety')
            ->select('coins_variety.label', 'coins_variety.variety', 'coins_variety.designation', 'coins_variety.type')->distinct()
            ->join('coins', 'coins.id', '=', 'coins_variety.coin_id')
            ->where('coins_variety.variety', 'Obverse Design Variety')
            ->where('coins.cointypes_id',$type_id)
            ->get()->toArray();
        $design_variety_list['reverse_varieties'] = DB::table('coins_variety')
            ->select('coins_variety.label', 'coins_variety.variety', 'coins_variety.designation', 'coins_variety.type')->distinct()
            ->join('coins', 'coins.id', '=', 'coins_variety.coin_id')
            ->where('coins_variety.variety', 'Reverse Design Variety')
            ->where('coins.cointypes_id',$type_id)
            ->get()->toArray();

        $design_variety_list['obverse_coneca'] = Coin::select('obv')->distinct()
            ->where('cointypes_id',$type_id)->where('obv','<>', 'None')
            ->get()->pluck('obv')->toArray();
        $design_variety_list['reverse_coneca'] = Coin::select('rev')->distinct()
            ->where('cointypes_id',$type_id)->where('rev','<>', 'None')
            ->get()->pluck('rev')->toArray();

        return $design_variety_list;
    }


    /**
     * Parse query for obverse OR reverse design
     *
     * @todo move to CollectedRepository $this->collectedRepository->
     * @param $coinType
     * @param $detail
     * @param $variety
     * @return array
     */
    public function getCollectedByCoinDesignVariety($coinType, $detail, $variety): array
    {
        $coins = match ($detail) {
            'obverse' => $this->getCollectedObverseDesignVariety( $coinType, $variety),
            'reverse' => $this->getCollectedReverseDesignVariety( $coinType, $variety),
            default => [],
        };

        return $coins;
    }


    public function getCollectedObverseDesignVariety(CoinType $coinType, string $variety): array
    {
        $coins = Coin::select('id','coinName','strike','cointypes_id')
            ->where('coins.cointypes_id',$coinType->id)
            ->where(function($query) use ($variety) {
                $query->where('coins.obv',$variety)
                    ->orWhere('coins.obv2',$variety)
                    ->orWhere('coins.obv3',$variety);
            })
            ->get();
        $cat_list = [];
        foreach($coins as $coin){
            $cat_list[$coin->id]['coin'] = $coin;
            $cat_list[$coin->id]['variety'] = 'obverse';
            $cat_list[$coin->id]['collected'] = DB::table('collected')
                ->where('collected.user_id',auth()->user()->id)
                ->where('collected.coin_id',$coin->id)->where('collected.obv', $variety)
                ->get()->count();
            $cat_list[$coin->id]['investment'] = DB::table('collected')
                ->where('collected.user_id',auth()->user()->id)
                ->where('collected.coin_id',$coin->id)->where('collected.obv', $variety)
                ->sum('cost') ?? 0.00;;
        }
        return $cat_list;
    }
    public function getCollectedReverseDesignVariety(CoinType $coinType, string $variety): array
    {
        $coins = Coin::select('id','coinName','strike','cointypes_id')
            ->where('coins.cointypes_id',$coinType->id)
            ->where(function($query) use ($variety) {
                $query->where('coins.rev',$variety)
                    ->orWhere('coins.rev2',$variety)
                    ->orWhere('coins.rev3',$variety);
            })
            ->get();
        $cat_list = [];
        foreach($coins as $coin){
            $cat_list[$coin->id]['coin'] = $coin;
            $cat_list[$coin->id]['variety'] = 'reverse';
            $cat_list[$coin->id]['collected'] = DB::table('collected')
                ->where('collected.user_id',auth()->user()->id)
                ->where('collected.coin_id',$coin->id)->where('collected.rev', $variety)
                ->get()->count();
            $cat_list[$coin->id]['investment'] = DB::table('collected')
                ->where('collected.user_id',auth()->user()->id)
                ->where('collected.coin_id',$coin->id)->where('collected.rev', $variety)
                ->sum('cost') ?? 0.00;;
        }
        return $cat_list;
    }
    public function getCollectedMintmarkVariety(int $type_id, string $mint, string $mms): array
    {
        $mms = str_replace('_', ' ', urldecode($mms));
        $mint = str_replace('_', ' ', urldecode($mint));
        $coins = Coin::select('id','coinName','strike', 'mint', 'mintMark','cointypes_id')
            ->where(function($query) use ($mms) {
                $query->where('coins.mms',$mms)
                    ->orWhere('coins.mms2',$mms)
                    ->orWhere('coins.mms3',$mms)
                    ->orWhere('coins.mms4',$mms);
            })
            ->where('coins.mint',$mint)
            ->where('coins.cointypes_id',$type_id)
            ->get();
        $cat_list = [];
        foreach($coins as $coin){
            $cat_list[$coin->id]['coin'] = $coin;
            $cat_list[$coin->id]['variety'] = 'mintmark';
            $cat_list[$coin->id]['collected'] = DB::table('collected')
                ->where('collected.user_id',auth()->user()->id)
                ->where('collected.coin_id',$coin->id)->where('collected.mms', $mms)
                ->get()->count();
            $cat_list[$coin->id]['investment'] = DB::table('collected')
                ->where('collected.user_id',auth()->user()->id)
                ->where('collected.coin_id',$coin->id)->where('collected.mms', $mms)
                ->sum('cost') ?? 0.00;;
        }
        return $cat_list;
    }


    // Design ----------------------------------------------------------------------------


    public function getCollectedBusinessStrikeCountByDesign(string $design)
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->where('coins.design',$design)
            ->whereIn('coins.strike',GradeHelper::BUSINESS_STRIKE)
            ->get()->count();
    }

    public function getCollectedProofStrikeCountByDesign(string $design)
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->where('coins.design',$design)
            ->whereIn('coins.strike',GradeHelper::PROOF_STRIKE)
            ->get()->count();
    }

    /**
     * Get related set, folder or roll
     *
     * @param Collected $collected
     * @return array
     */
    public function getCollectedIn(Collected $collected): array
    {
        if($collected->set_id !== 0){
            $collected_in = $collected->load('collectedSet:id,nickname')->toArray();
        } elseif ($collected->roll_id == 1){
            $collected_in = $collected->load('collectedRoll:id,nickname')->toArray();
        } elseif ($collected->folder_id == 1){
            $collected_in = $collected->load('collectedFolder:id,nickname')->toArray();
        } elseif ($collected->firstday_id == 1){
            $collected_in = $collected->load('collectedFirstDay:id,nickname')->toArray();
        } elseif ($collected->lot_id == 1){
            $collected_in = $collected->load('collectedLot:id,nickname')->toArray();
        } else {
            $collected_in = [];
        }
        return $collected_in;
    }

}
