<?php
/**
 * TypeGradeRepository | Helper for grade reports
 *
 * @package Coins
 * @subpackage Reports
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Repositories\Coin;

use App\Helpers\GradeHelper;
use App\Models\Coins\Collected;

class TypeGradeRepository
{



    /**
     * @param int $type_id
     * @return int
     */
    public function getGradedCount(int $type_id): int
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.cointypes_id', $type_id)
            ->whereNotNull('collected.grade')
            ->get()->count();
    }

    /**
     * Get special strike count by type
     *
     * @param int $type_id
     * @return int
     */
    public function getBusinessStrikeCount(int $type_id): int
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.cointypes_id', $type_id)
            ->whereIn('coins.strike', GradeHelper::BUSINESS_STRIKE)
            ->get()->count();
    }

    /**
     * Get special strike count by type
     *
     * @param int $type_id
     * @return int
     */
    public function getBusinessStrikeGradedCount(int $type_id): int
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.cointypes_id', $type_id)
            ->whereNotNull('collected.grade')
            ->whereIn('coins.strike', GradeHelper::BUSINESS_STRIKE)
            ->get()->count();
    }

    /**
     * Get special strike count by type
     *
     * @param int $type_id
     * @return int
     */
    public function getBusinessStrikeUnGradedCount(int $type_id): int
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.cointypes_id', $type_id)
            ->whereNull('collected.grade')
            ->whereIn('coins.strike', GradeHelper::BUSINESS_STRIKE)
            ->get()->count();
    }

    /**
     * Get special strike count by type
     *
     * @param int $type_id
     * @return int
     */
    public function getBusinessStrikeGradedProCount(int $type_id): int
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.cointypes_id', $type_id)
            ->whereNotNull('collected.grade')
            ->whereIn('coins.strike', GradeHelper::BUSINESS_STRIKE)
            ->get()->count();
    }


    // ------------------------ Proofs
    /**
     * Get special strike count by type
     *
     * @param int $type_id
     * @return int
     */
    public function getProofStrikeGradedCount(int $type_id): int
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.cointypes_id', $type_id)
            ->whereNotNull('collected.grade')
            ->whereIn('coins.strike', GradeHelper::PROOF_STRIKE)
            ->get()->count();
    }

    /**
     * Get special strike count by type
     *
     * @param int $type_id
     * @return int
     */
    public function getProofStrikeUnGradedCount(int $type_id): int
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.cointypes_id', $type_id)
            ->whereNull('collected.grade')
            ->whereIn('coins.strike', GradeHelper::PROOF_STRIKE)
            ->get()->count();
    }

    /**
     * Get special strike count by type
     *
     * @param int $type_id
     * @return int
     */
    public function getProofStrikeGradedProCount(int $type_id): int
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.cointypes_id', $type_id)
            ->whereNotNull('collected.grade')
            ->whereIn('coins.strike', GradeHelper::PROOF_STRIKE)
            ->get()->count();
    }

    /**
     * Get PROOF strike count by type
     *
     * @param int $type_id
     * @return int
     */
    public function getProofStrikeCount(int $type_id): int
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.cointypes_id', $type_id)
            ->whereIn('coins.strike', GradeHelper::PROOF_STRIKE)
            ->get()->count();
    }


    // ------------------------ Special
    /**
     * Get special strike count by type
     *
     * @param int $type_id
     * @return int
     */
    public function getSpecialStrikeGradedCount(int $type_id): int
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.cointypes_id', $type_id)
            ->whereNotNull('collected.grade')
            ->whereIn('coins.strike', GradeHelper::SPECIAL_STRIKE)
            ->get()->count();
    }

    /**
     * Get special strike count by type
     *
     * @param int $type_id
     * @return int
     */
    public function getSpecialStrikeUnGradedCount(int $type_id): int
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.cointypes_id', $type_id)
            ->whereNull('collected.grade')
            ->whereIn('coins.strike', GradeHelper::SPECIAL_STRIKE)
            ->get()->count();
    }

    /**
     * Get special strike 3rd party graded count by type
     *
     * @param int $type_id
     * @return int
     */
    public function getSpecialStrikeGradedProCount(int $type_id): int
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.cointypes_id', $type_id)
            ->whereNotNull('collected.grade')
            ->whereIn('coins.strike', GradeHelper::SPECIAL_STRIKE)
            ->get()->count();
    }

    /**
     * Get special strike count by type
     *
     * @param int $type_id
     * @return int
     */
    public function getSpecialStrikeCount(int $type_id): int
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.cointypes_id', $type_id)
            ->whereIn('coins.strike', GradeHelper::SPECIAL_STRIKE)
            ->get()->count();
    }


    /**
     * @param int $type_id
     * @return int
     */
    public function getUngradedCount(int $type_id): int
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.cointypes_id', $type_id)
            ->whereNull('collected.grade')
            ->get()->count();
    }


    /**
     * @param int $type_id
     * @return int
     */
    public function getRawCount(int $type_id): int
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.cointypes_id', $type_id)
            ->whereNull('collected.tpg_service')
            ->get()->count();
    }


    /**
     * @param int $type_id
     * @return int
     */
    public function getSlabbedCount(int $type_id): int
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.cointypes_id', $type_id)
            ->whereNotNull('collected.tpg_service')
            ->get()->count();
    }

}
