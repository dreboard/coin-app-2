<?php
/**
 * CoinGradeRepository | Helper for grade reports
 *
 * @package Coins
 * @subpackage Reports
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Repositories\Coin;

use App\Helpers\GradeHelper;
use App\Models\Coins\Collected;

class CoinGradeRepository
{



    /**
     * Get graded count by coin
     *
     * @param int $coin_id
     * @return mixed
     */
    public function getGradedCount(int $coin_id)
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.id', $coin_id)
            ->whereNotNull('collected.grade')
            ->get()->count();
    }

    /**
     * Get business count by coin
     *
     * @param int $coin_id
     * @return mixed
     */
    public function getBusinessStrikeCount(int $coin_id)
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.id', $coin_id)
            ->whereIn('coins.strike', GradeHelper::BUSINESS_STRIKE)
            ->get()->count();
    }

    /**
     * Get business strike graded count by coin
     *
     * @param int $coin_id
     * @return mixed
     */
    public function getBusinessStrikeGradedCount(int $coin_id): mixed
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.id', $coin_id)
            ->whereNotNull('collected.grade')
            ->whereIn('coins.strike', GradeHelper::BUSINESS_STRIKE)
            ->get()->count();
    }

    /**
     * Get business strike ungraded count by coin
     *
     * @param int $coin_id
     * @return mixed
     */
    public function getBusinessStrikeUnGradedCount(int $coin_id): mixed
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.id', $coin_id)
            ->whereNull('collected.grade')
            ->whereIn('coins.strike', GradeHelper::BUSINESS_STRIKE)
            ->get()->count();
    }

    /**
     * Get business strike 3rd party graded count by coin
     *
     * @param int $coin_id
     * @return mixed
     */
    public function getBusinessStrikeGradedProCount(int $coin_id): mixed
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.id', $coin_id)
            ->whereNotNull('collected.grade')
            ->whereIn('coins.strike', GradeHelper::BUSINESS_STRIKE)
            ->get()->count();
    }


    // ------------------------ Proofs

    /**
     * Get proof strike count by coin
     *
     * @param int $coin_id
     * @return mixed
     */
    public function getProofStrikeGradedCount(int $coin_id): mixed
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.id', $coin_id)
            ->whereNotNull('collected.grade')
            ->whereNull('collected.tpg_service')
            ->whereIn('coins.strike', GradeHelper::PROOF_STRIKE)
            ->get()->count();
    }

    /**
     * Get ungraded proof strike count by coin
     *
     * @param int $coin_id
     * @return mixed
     */
    public function getProofStrikeUnGradedCount(int $coin_id): mixed
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.id', $coin_id)
            ->whereNull('collected.grade')
            ->whereIn('coins.strike', GradeHelper::PROOF_STRIKE)
            ->get()->count();
    }

    /**
     * Get 3rd party graded proofs strike count by coin
     *
     * @param int $coin_id
     * @return mixed
     */
    public function getProofStrikeGradedProCount(int $coin_id): mixed
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.id', $coin_id)
            ->whereNotNull('collected.grade')
            ->whereNotNull('collected.tpg_service')
            ->whereIn('coins.strike', GradeHelper::PROOF_STRIKE)
            ->get()->count();
    }

    /**
     * Get PROOF strike count by coin
     *
     * @param int $coin_id
     * @return mixed
     */
    public function getProofStrikeCount(int $coin_id): mixed
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.id', $coin_id)
            ->whereIn('coins.strike', GradeHelper::PROOF_STRIKE)
            ->get()->count();
    }


    // ------------------------ Special

    /**
     * Get graded special strike count by coin
     *
     * @param int $coin_id
     * @return mixed
     */
    public function getSpecialStrikeGradedCount(int $coin_id): mixed
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.id', $coin_id)
            ->whereNotNull('collected.grade')
            ->whereIn('coins.strike', GradeHelper::SPECIAL_STRIKE)
            ->get()->count();
    }

    /**
     * Get ungraded special strike count by coin
     *
     * @param int $coin_id
     * @return mixed
     */
    public function getSpecialStrikeUnGradedCount(int $coin_id): mixed
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.id', $coin_id)
            ->whereNull('collected.grade')
            ->whereIn('coins.strike', GradeHelper::SPECIAL_STRIKE)
            ->get()->count();
    }

    /**
     * Get special strike 3rd party graded count by coin
     *
     * @param int $coin_id
     * @return mixed
     */
    public function getSpecialStrikeGradedProCount(int $coin_id): mixed
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.id', $coin_id)
            ->whereNotNull('collected.grade')
            ->whereNotNull('collected.tpg_service')
            ->whereIn('coins.strike', GradeHelper::SPECIAL_STRIKE)
            ->get()->count();
    }

    /**
     * Get special strike count by coin
     *
     * @param int $coin_id
     * @return mixed
     */
    public function getSpecialStrikeCount(int $coin_id): mixed
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.id', $coin_id)
            ->whereIn('coins.strike', GradeHelper::SPECIAL_STRIKE)
            ->get()->count();
    }


    /**
     * Get ungraded count for coin id
     *
     * @param int $coin_id
     * @return mixed
     */
    public function getUngradedCount(int $coin_id): mixed
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.id', $coin_id)
            ->whereNull('collected.grade')
            ->get()->count();
    }


    /**
     * Get unslabbed count for coin id
     *
     * @param int $coin_id
     * @return mixed
     */
    public function getRawCount(int $coin_id): mixed
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.id', $coin_id)
            ->whereNull('collected.tpg_service')
            ->get()->count();
    }


    /**
     * Get slabbed count by coin id
     *
     * @param int $coin_id
     * @return mixed
     */
    public function getSlabbedCount(int $coin_id): mixed
    {
        return Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)
            ->where('coins.id', $coin_id)
            ->whereNotNull('collected.tpg_service')
            ->get()->count();
    }


    /**
     * Get highest graded by coin id
     *
     * @param int $coin_id
     * @return array
     */
    public function getFinestProGrade(int $coin_id): array
    {
        $grades = Collected::select('id','grade', 'tpg_service','nickname')->where('user_id', auth()->user()->id)
            ->where('coin_id', $coin_id)->whereNotNull('tpg_service')->get();
        $graded = $grades->filter(function ($item) {
            return is_numeric($item->grade);
        });
        return [
            'id' => $graded->max('id'),
            'grade' => $graded->max('grade'),
            'tpg_service' => $graded->max('tpg_service'),
            'nickname' => $graded->max('nickname'),
        ];
    }


    /**
     * Get highest graded by coin id
     *
     * @param int $coin_id
     * @return array
     */
    public function getFinestGrade(int $coin_id): array
    {
        $grades = Collected::select('id','grade','nickname')->where('user_id', auth()->user()->id)
            ->where('coin_id', $coin_id)->whereNull('tpg_service')->get();
        $graded = $grades->filter(function ($item) {
            return is_numeric($item->grade);
        });
        return [
            'id' => $graded->max('id'),
            'grade' => $graded->max('grade'),
            'nickname' => $graded->max('nickname'),
        ];
    }


    /**
     * Get highest graded by coin id
     *
     * @param int $coin_id
     * @return array
     */
    public function getFinest(int $coin_id): array
    {
        $data = [];
        $data['raw'] = $this->getFinestGrade($coin_id);
        $data['slabbed'] = $this->getFinestProGrade($coin_id);
        return $data;
    }

}
