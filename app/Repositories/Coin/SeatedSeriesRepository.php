<?php
/**
 * SeatedSeriesRepository | actions for Coins/Collected Seated liberty
 *
 * This class handles Seated liberty coins
 * @package Collected
 * @subpackage Coins
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Repositories\Coin;

class SeatedSeriesRepository
{


    const TYPES = [
            25 => 'Seated Liberty Half Dime',
            8 => 'Seated Liberty Dime',
            13 => 'Seated Liberty Quarter',
            57 => 'Seated Liberty Half Dollar',
            59 => 'Seated Liberty Dollar'
        ];


}
