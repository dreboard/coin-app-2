<?php /** @noinspection ALL */

/**
 * CoinSetRepository | actions for Type Set, Year Set, Date and Mintmark Set, Sets Including Proofs
 *
 * @package Coins
 * @subpackage Collecting
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Repositories\Coin;

use App\Models\Coins\Coin;
use Illuminate\Support\Facades\DB;

class CoinSetRepository
{


    // Type Set, Year Set, Date and Mintmark Set, Sets Including Proofs

    public function generateTypeSet(int $type_id)
    {
        $coins = Coin::select('id','coinName')->where('coins.cointypes_id', $type_id)->orderBy('coinYear')->get();
        $cat_list = [];
        foreach($coins as $coin){
            $cat_list[$coin->id]['coin'] = $coin;
            $cat_list[$coin->id]['collected'] = $this->getTypeSetByYear($type_id);
        }
    }

    /**
     * Get collected count by coin id
     *
     * @param int $type_id
     * @return int
     */
    public function getTypeSetByYear(int $type_id): int
    {
        return DB::table('collected')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->where('coins.cointypes_id',$type_id)
            ->get()->count();
    }




}
