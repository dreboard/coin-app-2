<?php
/**
 * CoinVarietyRepository | queries for coin varieties
 *
 * @package Coins
 * @subpackage Collecting
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Repositories\Coin;

use App\Models\Coins\CoinVariety;

class CoinVarietyRepository
{



    public function getVarietiesTypeList(int $coin_id): array
    {
        $varieties = CoinVariety::select('variety')->distinct()->where('coin_id', $coin_id)->get();

        $variety_list = [];
        foreach($varieties as $variety){
            $variety_list[$variety] = $this->getVarietiesTypeList($coin_id);
        }
        return $variety_list;
    }

    public function getVarietiesByCoin(int $coin_id)
    {
        return CoinVariety::where('coin_id', $coin_id)->get();
    }

    public function getSubTypesByCoin(int $coin_id)
    {
        return CoinVariety::select('sub_type')->distinct()->where('coin_id', $coin_id)->get();
    }

    public function getVarietyTypeByCoin(int $coin_id, string $variety)
    {
        return CoinVariety::select('id','sub_type','label','designation','type')->where('coin_id', $coin_id)
            ->where('variety', $variety)->get();
    }

}
