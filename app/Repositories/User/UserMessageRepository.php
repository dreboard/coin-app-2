<?php

namespace App\Repositories\User;

use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserMessageRepository
{


    public function getUserMessageCount(int $user_id): int
    {
        $messaageCount = DB::table('participants')
            ->where('user_id', $user_id)
            ->count();
        return $messaageCount ?? 0;
    }


    public function getUserSystemMessageCount(int $user_id)
    {
        $user = User::find($user_id);
        return $user->unreadNotifications->count();
    }

}
