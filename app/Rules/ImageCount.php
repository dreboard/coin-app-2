<?php /** @noinspection ALL */

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class ImageCount implements Rule
{

    private int $max_image_count = 4;
    private array $max_model_image_count = [
        'App\Models\Forum' => 4,
        'App\Models\Post' =>  4,
        'App\Models\Project' =>  4,
        'App\Models\Comment' =>  1,
        'App\Models\User' =>  1,
        'App\Models\Coins\Collected' =>  6,
    ];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(private string $model, private int $id)
    {
        //
    }



    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $current_images = DB::table('images')
            ->where('imagable_type', '=', $this->model)
            ->where('imagable_id', '=', $this->id)
            ->count();

        return count($current_images) <= $this->max_model_image_count[$this->model];
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return "You cannot upload more than {$this->max_model_image_count[$this->model]} images.";
    }
}
