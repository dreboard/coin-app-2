<?php

namespace App\Actions;

use App\Events\User\BannedUser;
use App\Events\User\WarnUser;
use App\Http\Controllers\Users\UserViolationsController;
use App\Models\User;
use App\Models\Users\Violation;
use App\Notifications\GeneralUserNotification;
use Illuminate\Support\Facades\DB;

class UserViolationAction
{

    private int $banned_length = 14;

    /**
     * Update and notify all reporters
     *
     * @param User $violator
     * @param string $action
     * @return void
     */
    public function updateUserViolations(User $violator, string $action)
    {
        Violation::where('violator_id', $violator->id)
            ->orWhere('violator', $violator->name)
            ->update(['action' => $action]);

        $reporters = DB::table('violations')
            ->select('reporter')->distinct()
            ->where('violator', $violator->name)
            ->pluck('reporter')
            ->toArray();

        foreach ($reporters as $reporter) {
            $user = User::find($reporter);
            $user->notify(new GeneralUserNotification([
                'subject' => 'Your violation update',
                'body' => 'Your violation has been updated to '.$action,
                'from' => 'The Administrator'
            ]));
        }
    }

    /**
     * Process violator based on action
     *
     * @param $violator_id
     * @param $action
     * @return void
     */
    public function handleViolator($violator_id, $action): void
    {
        $violator = User::find($violator_id);
        $this->updateUserViolations($violator, $action);

        switch ($action){
            case UserViolationsController::ViolationActions['ACTION_WARNED']:
                WarnUser::dispatch($violator);
                break;
            case UserViolationsController::ViolationActions['ACTION_BANNED']:
                event(new BannedUser($violator, $this->banned_length));
                break;
            case UserViolationsController::ViolationActions['ACTION_PENDING']:
            case UserViolationsController::ViolationActions['ACTION_NONE']:
            default;
                break;
        }

    }

}
