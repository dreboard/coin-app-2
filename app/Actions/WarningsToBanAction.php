<?php

namespace App\Actions;

use App\Events\User\BannedUser;
use App\Models\User;
use App\Models\Users\Warning;
use App\Notifications\BanUserNotification;

class WarningsToBanAction
{

    public function countWarnings(int $id): bool
    {
        $count = Warning::where('user_id', $id)->count();
        if ($count >= 3){
            $user = User::find($id);
            event(new BannedUser($user, 30));

            $data = [
                'body' => 'Your account is now in warning status',
                'subject' => 'You have been issued a warning',
                'user_email' => $user->email
            ];

            $user->notify(new BanUserNotification($data));

            return true;
        }
        return false;
    }

}
