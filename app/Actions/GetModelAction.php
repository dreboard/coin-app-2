<?php

namespace App\Actions;

use App\Models\Coins\Coin;
use App\Models\Coins\Error;
use App\Models\Coins\Variety;
use App\Models\Post;
use App\Models\Project;
use Exception;

class GetModelAction
{

    /**
     * @throws Exception
     */
    public function getModel(int $id, string $model): array
    {
        $model_info = [];
        $type = match($model) {
            'coin' => 'App\Models\Coins\Coin',
            'error' => 'App\Models\Coins\Error',
            'project' => 'App\Models\Project',
            'variety' => 'App\Models\Coins\Variety',
            'post' => 'App\Models\Post',
            default => throw new Exception('No Related Model'),
        };
        $model_info['name'] = $model;
        $model_info['type'] = $type;
        $model_info['instance'] = $this->getModelInstance($id, $model);


        return $model_info;

    }

    /**
     * @throws Exception
     */
    private function getModelInstance(int $id, string $model)
    {
        $instance = match($model) {
            'coin' => Coin::findOrFail($id),
            'error' => Error::findOrFail($id),
            'project' => Project::findOrFail($id),
            'variety' => Variety::findOrFail($id),
            'post' => Post::findOrFail($id),
            default => throw new Exception('No Related Model'),
        };

        return $instance;
    }

    /**
     * @throws Exception
     */
    private function getModelRoute(int $id, string $model): string
    {
        $route = match($model) {
            'coin' => route('view_coin', ['coin' => $id]),
            'error' => route('view_coin', ['coin' => $id]),
            'project' => route('view_coin', ['coin' => $id]),
            'variety' => route('view_coin', ['coin' => $id]),
            'post' => route('view_coin', ['coin' => $id]),
            default => throw new Exception('No Related Model'),
        };

        return $route;
    }

}
