<?php

namespace App\Actions\Posts;

use App\Models\Post;
use App\Models\User;
use App\Notifications\NewPostNotification;
use Illuminate\Support\Facades\DB;

class PostNotifyAction
{


    public function newPostNotification(Post $post)
    {
        $followers = DB::table('followables')
            ->select('followable_id', 'users.name', 'users.id', 'users.specialty', 'users.user_type')
            ->where('followables.followable_type', 'App\Models\User')
            ->where('followables.followable_id', auth()->user()->id)
            ->where('users.status', 'good')
            ->join('users', 'users.id', '=', 'followables.user_id')
            ->get();

        foreach ($followers as $user){
            $follower = User::findOrFail($user->id);
            $follower->notify(new NewPostNotification($post));
        }

    }


}
