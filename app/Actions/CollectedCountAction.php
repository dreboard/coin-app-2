<?php
/**
 * CollectedCountAction | actions for all controllers
 *
 * This class handles creating an array of collected coins with counts and investment
 * @package Collected
 * @subpackage Coins
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Actions;

use Illuminate\Support\Facades\DB;

class CollectedCountAction
{


    /**
     * Get collected count array
     *
     * @param mixed $coins
     * @return array
     */
    public function getCollectedByCoin(mixed $coins): array
    {
        $cat_list = [];
        foreach($coins as $coin){
            $cat_list[$coin->id]['coin'] = $coin;
            $cat_list[$coin->id]['collected'] = $this->getCollectedCountByCoin($coin->id);
            $cat_list[$coin->id]['investment'] = $this->getCollectedInvestmentByCoin($coin->id);
        }
        return $cat_list;
    }

    /**
     * Get collected count by coin id
     *
     * @param int $coin_id
     * @return int
     */
    public function getCollectedCountByCoin(int $coin_id): int
    {
        return DB::table('collected')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->where('coins.id',$coin_id)
            ->get()->count();
    }


    /**
     * Get sum of purchases by coin id
     *
     * @param int $coin_id
     * @return float|int|mixed
     */
    public function getCollectedInvestmentByCoin(int $coin_id)
    {
        return DB::table('collected')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->where('coins.id',$coin_id)
            ->sum('cost') ?? 0.00;
    }

}
