<?php
/**
 * SaveLooseSetAction | actions for Collected Model
 *
 * This class handles saving collected coins for loose mintsets
 * @package Collected
 * @subpackage Coins
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Actions\Coins;

use App\Models\Coins\Collected;
use App\Repositories\Coin\CollectedRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Throwable;

readonly class SaveLooseSetAction
{


    public function __construct(
        private CollectedRepository $collectedRepository
    ){}


    /**
     * @param Request $request
     * @param $collected_set
     * @return RedirectResponse|void
     */
    public function saveMintsetCoins(Request $request, $collected_set)
    {
        try{
            $coins_array = $request->input('coin');
            foreach($request->input('coin') as $key => $coin){

                // ***** Collected coin exists, just update and lock
                if(array_key_exists('collected_id', $coin) && $coin['collected_id'] !== 'None'){
                    $collected = Collected::findOrFail($coin['collected_id']);
                    $collected->set_id = $collected_set->id;
                    $collected->circulated = 0;
                    $collected->locked = 1;
                    $collected->save();
                    unset($coins_array[$key]);
                }

                // ***** Collected coin does NOT exists, create and add to set
                if(false === array_key_exists('collected_id', $coin) || $coin['collected_id'] === 'None'){
                    $collected = new Collected;
                    $collected->user_id = auth()->user()->id;
                    $collected->nickname = $collected_set->nickname. ' Coin';
                    $collected->coin_id = $key;
                    $collected->set_id = $collected_set->id;
                    $collected->grade = $coin['grade'] ?? 'None';
                    $collected->tpg_service = $coin['tpg_service'] ?? 'None';
                    $collected->tpg_serial_num = $coin['tpg_serial_num'] ?? 'None';
                    $collected->slab_condition = $coin['slab_condition'] ?? 'Excellent';
                    $collected->circulated = 0;
                    $collected->locked = 1;
                    $collected->save();
                    unset($coins_array[$key]);
                }

            }
            // Save any unselected coins NOT in request, (USER did not select them)
            foreach($coins_array as $key => $to_enter){
                $this->collectedRepository->saveSetCoin($this->collectedSet->id, $to_enter);
            }
        } catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Create');
        }


    }

}
