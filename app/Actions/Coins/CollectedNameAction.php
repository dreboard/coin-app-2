<?php
/**
 * CollectedNameAction | actions for All Collected Model
 *
 * This class handles generating nicknames for collected coins, folders and mintsets
 * @package Collected
 * @subpackage Coins
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Actions\Coins;

use App\Models\Coins\Coin;
use App\Models\Coins\Mintset;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CollectedNameAction
{


    /**
     * Auto generate collected coin nickname
     *
     * @param Coin $coin
     * @return string
     */
    public function generateCoinNickname(Coin $coin): string
    {
        $count = DB::table('collected')
                ->join('coins', 'coins.id', '=', 'collected.coin_id')
                ->where('collected.user_id',auth()->user()->id)
                ->where('collected.coin_id', $coin->id)
                ->get()->count() + 1;
        return $coin->coinName. '#'.$count;
    }

    /**
     * Auto generate collected coin nickname
     *
     * @param Mintset $set
     * @return string
     */
    public function generateSetNickname(Mintset $set): string
    {
        $count = DB::table('collected_sets')
                ->join('mintset', 'mintset.id', '=', 'collected_sets.set_id')
                ->where('collected_sets.user_id',auth()->user()->id)
                ->where('collected_sets.set_id', $set->id)
                ->get()->count() + 1;
        Log::info($set->setName. '#'.$count);
        return $set->setName. '#'.$count;
    }

    /**
     * Auto generate collected coin nickname
     *
     * @param Mintset $set
     * @return string
     */
    public function generateCoinSetNickname(Mintset $set): string
    {
        $count = DB::table('collected_sets')
                ->join('mintset', 'mintset.id', '=', 'collected_sets.set_id')
                ->where('collected_sets.user_id',auth()->user()->id)
                ->where('collected_sets.set_id', $set->id)
                ->get()->count() + 1;
        return $set->setName. '#'.$count;
    }

}
