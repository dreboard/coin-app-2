<?php

namespace App\Http\Traits;

use App\Models\Coins\Collected;
use App\Models\Comment;
use App\Models\Forum;
use App\Models\Image;
use App\Models\Post;
use App\Models\Project;
use App\Models\User;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Support\Facades\Storage;

trait ImageTrait
{

    public function deleteImage(Image $image){
        Storage::delete($image->image_url);
        $image->delete();
    }


    public function deleteImages(Image $image){
        Storage::delete($image->image_url);
        $image->delete();
    }


    /**
     * Get imageable return URL
     *
     * @param Image $image
     * @return string|UrlGenerator|Application
     * @throws Exception
     */
    public function getImageUrl(Image $image): string|UrlGenerator|Application
    {

        return match($image->imageable_type) {
            'App\Models\Forum' => '/forum/' . $image->imageable_id,
            'App\Models\Post' =>  '/post/' . $image->imageable_id,
            'App\Models\Project' =>  '/project/' . $image->imageable_id,
            'App\Models\Comment' =>  '/comments/' . $image->imageable_id,
            'App\Models\User' =>  '/users/' . $image->imageable_id,
            'App\Models\Coins\Collected' =>  url("collected/view_collected/$image->imageable_id"),//'/collected/view_collected/' . $image->imageable_id,
            default => throw new Exception('No Related Image'),
        };
    }


    /**
     * @throws Exception
     */
    public function getModelUrl(string $model, int $id){

        return match(strtolower($model)) {
            'forum' => url("/member/view_forum_post/$id"),
            'post' => url("/member/view_post/$id"),
            'project' =>  url("/member/project_view/$id"),
            'user' =>  url("user/public/$id"),
            'collected' =>  url("collected/view_collected/$id"),
            default => throw new Exception('No Related Image'),
        };
    }

    /**
     * @throws Exception
     */
    public function stringToNamespace(string $model): string
    {

        return match($model) {
            'forum' => 'App\Models\Forum',
            'post' => 'App\Models\Post',
            'project' =>  'App\Models\Project',
            'user' =>  'App\Models\User',
            'collected' =>  'App\Models\Coins\Collected',
            default => throw new Exception('No Related Model'),
        };
    }

    /**
     * @throws Exception
     */
    public function getImageModel(Image $image){

        return match($image->imageable_type) {
            'App\Models\Forum' => Forum::findOrFail($image->imageable_id),
            'App\Models\Post' =>  Post::findOrFail($image->imageable_id),
            'App\Models\Project' =>  Project::findOrFail($image->imageable_id),
            'App\Models\Comment' =>  Comment::findOrFail($image->imageable_id),
            'App\Models\User' =>  User::findOrFail($image->imageable_id),
            'App\Models\Coins\Collected' =>  Collected::findOrFail($image->imageable_id),
            default => throw new Exception('No Related Image'),
        };
    }

}
