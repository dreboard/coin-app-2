<?php
/**
 * GroupTrait | Support for Controllers/Groups
 *
 * @package Groups
 * @subpackage Helper
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Traits;

use App\Helpers\ManageGroupHelper;
use App\Models\Announcement;
use App\Models\Groups\Group;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

trait GroupTrait
{

    /**
     * Is user in group
     *
     * @param int $group_id
     * @return bool
     */
    public function isGroupUser(int $group_id): bool
    {
        if(auth()->user()->is_admin){
            //return true;
        }
        return DB::table('group_user')
            ->where('user_id', auth()->user()->id)
            ->where('group_id', $group_id)
            ->exists();
    }

    /**
     * Is user in committee OR a board member
     *
     * @param int $committee_id
     * @return bool
     */
    public function isCommitteeMember(int $committee_id): bool
    {
        if(auth()->user()->is_admin){
            return true;
        }
        return DB::table('committee_user')
            ->where('user_id', auth()->user()->id)
            ->where('committee_id', $committee_id)
            ->exists();
    }

    /**
     * Is user a group board member
     *
     * @param int $group_id
     * @return bool
     */
    public function isBoardMember(int $group_id): bool
    {
        if(auth()->user()->is_admin){
            return true;
        }
        return DB::table('group_role')
            ->where('group_role.group_id', $group_id)
            ->where('group_role.user_id', auth()->user()->id)
            ->whereIn('role_id', ManageGroupHelper::GROUP_BOARD_POSITIONS)
            ->exists();
    }

    /**
     * Create group announcement
     *
     * @param array $data
     * @return void
     */
    public function createAnnouncement(array $data)
    {
        Announcement::create([
            'user_id' => $data['user_id'],
            'group_id' => $data['group_id'],
            'message' => $data['message'],
        ]);
    }


    /**
     * Save group main image
     *
     * @param Request $request
     * @param Group $group
     * @param string|null $folder
     * @return false|string
     */
    public function processImage(Request $request, Group $group, string $folder = null)
    {
        Storage::delete($group->image_url);
        $path = Storage::putFile('groups/'.$group->id, $request->file('image_url'));

        return $path;
    }


    /**
     * Save group post image
     *
     * @param Request $request
     * @param Post $post
     * @param Group $group
     * @param string|null $folder
     * @return false|string
     */
    public function processPostImage(Request $request, Post $post, Group $group, string $folder = null)
    {
        Storage::delete($post->image_url);
        $path = Storage::putFile('groups/'.$group->id.$folder, $request->file('image_url'));

        return $path;
    }

}
