<?php
/**
 * UserTrait | Support for Controllers/Groups
 *
 * @package Users
 * @subpackage Helper
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Traits;


use App\Models\Groups\Group;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

trait UserTrait
{


    public function isParticipant(int $forum_id): bool
    {
        return DB::table('participants')
            ->where('user_id', auth()->user()->id)
            ->where('forum_id', $forum_id)
            ->exists();
    }

    public function myFollowers(): Collection
    {
        return DB::table('followables')
            ->select('followable_id', 'users.name', 'users.id', 'users.specialty', 'users.user_type')
            ->where('followables.followable_type', 'App\Models\User')
            ->where('followables.followable_id', auth()->user()->id)
            ->join('users', 'users.id', '=', 'followables.user_id')
            ->get();
    }

    public function withMySocial(): array
    {
        $social = [];
        $social['groups'] = DB::table('group_user')
            ->select('group_user.group_id', 'groups.name', 'groups.group_type', 'groups.specialty', 'groups.id')
            ->where('group_user.user_id', auth()->user()->id)
            ->join('groups', 'groups.id', '=', 'group_user.group_id')
            ->get();
        $social['projects'] = DB::table('projects')
            ->select('id', 'title')
            ->where('user_id', auth()->user()->id)
            ->get();
        return $social;
    }

    /**
     * Save group main image
     *
     * @param Request $request
     * @param Group $group
     * @param string|null $folder
     * @return false|string
     */
    public function processImage(Request $request, Group $group, string $folder = null)
    {
        Storage::delete($group->image_url);
        $path = Storage::putFile('groups/'.$group->id, $request->file('image_url'));

        return $path;
    }


    /**
     * Save post image
     *
     * @param Request $request
     * @param Model $model
     * @param string|null $folder
     * @return false|string
     */
    public function processPostImage(Request $request, Model $model, string $folder = null)
    {
        if($model->image_url !== null){
            Storage::delete($model->image_url);
        }
        $path = Storage::putFile($folder, $request->file('image_url'));
        return $path;
    }

}
