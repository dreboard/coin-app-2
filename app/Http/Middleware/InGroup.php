<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class InGroup
{

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param $group_id
     * @return Response|RedirectResponse
     */
    public function handle(Request $request, Closure $next, $group_id)
    {
        Log::info($request->route()->parameter('group_id'));
        $group_user = DB::table('group_user')->select('member_type')->where('group_id', $group_id)
            ->where('user_id', auth()->user()->id)
            ->first();
        if (empty($group_user)) {
            return redirect()->route('group.request', ['group' => $group_id]);
        }
        return $next($request);
    }
}
