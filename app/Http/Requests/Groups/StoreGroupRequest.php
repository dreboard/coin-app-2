<?php

namespace App\Http\Requests\Groups;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|unique:groups|max:255',
            'group_type' => 'required',
            'description' => 'required|unique:groups|max:255',
            'short_description' => 'required',
            'image_url' => 'required',
            'url' => 'required|unique:groups|max:255',
            'email' => 'required',
            'meets' => 'required',
            'formed' => 'required',
            'specialty' => 'required|unique:groups|max:255',
            'private' => ['required',
                            Rule::in(['group', 'club']),
    ],
        ];
    }
}
