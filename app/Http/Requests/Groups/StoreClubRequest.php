<?php

namespace App\Http\Requests\Groups;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreClubRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|unique:groups,name|max:255',
            'user_id' => 'required|integer|unique:groups',
            'group_type' => ['required',
                Rule::in(['group', 'club']),
            ],
            'description' => 'required|string',
            'short_description' => 'sometimes|required|string|max:255',
            'image_url' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'url' => 'sometimes|required|string',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'phone' => 'sometimes|required|string',
            'email' => 'required|email',
            'address' => 'required|string',
            'postalcode' => 'required|string',
            'city' => 'required|string',
            'state' => 'required|string',
            'meets' => 'sometimes|required|string',
            'formed' => 'sometimes|required|string',
            'specialty' => 'sometimes|required|string',
            'private' => ['required',
                Rule::in([0, 1]),
            ],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'user_id.unique' => 'A group already exists for this user',
            'first_name.required' => 'A first name is required to create a group',
            'last_name.required' => 'A last name is required to create a group',
            'description.required' => 'A short description is required to create a group',
        ];
    }


}
