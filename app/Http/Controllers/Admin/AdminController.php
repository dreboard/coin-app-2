<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Users\UserLogin;
use App\Repositories\Admin\AdminUserRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

/**
 *
 */
class AdminController extends Controller
{

    public function __construct(private readonly AdminUserRepository $adminUserRepository)
    {
    }

    /**
     * View the admin dashboard
     *
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        $allActiveCount = $this->adminUserRepository->getAllUsersWithoutBannedCount();
        $allUsersCount = $this->adminUserRepository->getAllUsersWithBannedCount();
        $createdToday = $this->adminUserRepository->countRegisteredUsersToday();
        return view('admin.dashboard', [
            'allActiveCount' => $allActiveCount,
            'allUsersCount' => $allUsersCount,
            'createdToday' => $createdToday
        ]);
    }




}
