<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;

class GroupController extends Controller
{




    /**
     * Show all groups.
     *
     * @return Factory|View|Application
     */
    public function viewGroups(): Factory|View|Application
    {
        $groups = DB::table('groups')
            //->where('categories.parent_id', 0)
            ->select('groups.name', 'groups.id', 'groups.group_type', 'groups.created_at', DB::raw('COUNT(users.id) AS user_count'))
            ->leftJoin('group_user', 'group_user.group_id', '=', 'groups.id')
            ->leftJoin('users', 'group_user.user_id', '=', 'users.id')
            ->groupBy('groups.name', 'groups.id', 'groups.group_type', 'groups.created_at')
            ->orderBy('groups.id')
            ->get();
        //dd($groups);
        return view('admin.group.all', ['groups' => $groups]);
    }


    /**
     * Show all groups.
     *
     * @param $id
     * @return Factory|View|Application
     */
    public function viewGroup($id): Factory|View|Application
    {
        $group = DB::table('groups')->distinct()
            ->select('groups.name', 'groups.group_type', 'groups.id', 'groups.specialty', 'groups.created_at')
            ->join('group_user', 'group_user.group_id', '=', 'groups.id')
            ->join('users', 'group_user.user_id', '=', 'users.id')
            ->where('groups.id', $id)
            ->get();
        $users = DB::table('users')
            ->select('users.name','users.id', 'group_user.member_type', 'group_user.club_position')
            ->leftJoin('group_user', 'group_user.user_id', '=', 'users.id')
            ->leftJoin('groups', 'group_user.group_id', '=', 'groups.id')
            ->where('groups.id', $id)
            ->get();
        //dd($group,$users);
        return view('admin.group.view', ['group' => $group[0], 'users' => $users]);
    }
}
