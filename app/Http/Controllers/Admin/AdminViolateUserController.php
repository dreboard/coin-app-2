<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Users\Violation;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

/**
 *
 */
class AdminViolateUserController extends Controller
{

    /**
     * Violations Reported Against User
     *
     * @param int $user_id
     * @return \Application|\Factory|\View
     */
    public function viewReportedAgainstUserViolations(int $user_id): \Application|\Factory|\View
    {
        $user = User::find($user_id);
        $user_list = Violation::with('reportingUser')->where('violator', 'LIKE', "%$user->name%")
            ->orWhere('violator', 'LIKE', "%$user->email%")->get();

        return view('admin.user.violations_reported_against', [
            'user' => $user,
            'user_list' => $user_list,
        ]);
    }

    /**
     * Violations Reported By User
     *
     * @param int $user_id
     * @return \Application|\Factory|\View
     */
    public function viewReportedByUserViolations(int $user_id): \Application|\Factory|\View
    {
        $user = User::find($user_id);
        $user_list = Violation::where('reporter', $user_id)->get();

        return view('admin.user.violations_reported_by', [
            'user' => $user,
            'user_list' => $user_list,
        ]);
    }

    /**
     * @return Factory|View|Application
     */
    public function viewAllPendingViolations(): Factory|View|Application
    {
        $violations = Violation::where('action', 'Case Pending')->get();

        return view('admin.violations.pending', [
            'violations' => $violations
        ]);
    }

    /**
     * @return Factory|View|Application
     */
    public function viewAllViolations(): Factory|View|Application
    {
        $violations = Violation::all();

        return view('admin.violations.all', [
            'violations' => $violations
        ]);
    }

    /**
     * @param string $violation_type
     * @return Factory|View|Application
     */
    public function viewAllViolationsByType(string $violation_type): Factory|View|Application
    {
        $violations = Violation::where('action', 'Case Pending')->where('violation_type', $violation_type)->get();

        return view('admin.violations.type', [
            'violations' => $violations,
            'violation_type' => $violation_type,
        ]);
    }
}
