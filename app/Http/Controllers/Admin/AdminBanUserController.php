<?php

namespace App\Http\Controllers\Admin;

use App\Events\User\BannedUser;
use App\Events\User\UnbanUser;
use App\Models\User;
use Cog\Contracts\Ban\BanService;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;

class AdminBanUserController
{


    private const ERROR_MSG = 'User could not be banned';

    private const SUCCESS_MSG = 'User is activated';

    private const IS_BANNED_MSG = 'User is banned';

    /**
     *
     */
    public function __construct()
    {
    }

    /**
     * View all banned users
     *
     * @return Application|Factory|View
     */
    public function viewAllBannedUsers(): View|Factory|Application
    {
        $users = User::onlyBanned()->get();
        return view('admin.user-all-banned', compact('users'));
    }

    /**
     * @param int $user_id
     */
    public function banUserPermanent(int $user_id)
    {
        $user = User::find($user_id);
        $user->ban([
            'expired_at' => '+1 month',
        ]);

    }

    /**
     * Ban a user
     *
     * @source {https://github.com/cybercog/laravel-ban#prepare-bannable-model}
     * @param Request $request
     * @return RedirectResponse
     */
    public function banUser(Request $request): RedirectResponse
    {
        $request->validate([
            'length' => 'required',
            'user_id' => 'required|integer'
        ]);

        try{
            $user = User::findOrFail($request->input('user_id'));
            event(new BannedUser($user, (int)$request->input('length')));

            return redirect()->action(
                [AdminUserActionsController::class, 'viewUser'],
                ['user_id' => $request->input('user_id')]
            )->with('error', self::IS_BANNED_MSG);

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return Redirect::back()->withErrors(['msg' => self::ERROR_MSG]);
        }
    }


    /**
     * Unban all users
     *
     * @return RedirectResponse
     */
    public function unbanAllExpiredBannedUsers(): RedirectResponse
    {
        app(BanService::class)->deleteExpiredBans();
        return Redirect::back()->withErrors(['msg' => 'All Expired Users Restored']);
    }

    /**
     * Unban a user
     * @param int $user_id
     * @return RedirectResponse
     */
    public function unbanUser(int $user_id): RedirectResponse
    {
        try {
            $user = User::findOrFail($user_id);
            event(new UnbanUser($user));
            return redirect()->action(
                [AdminUserActionsController::class, 'viewUser'],
                ['user_id' => $user_id]
            )->with('success', self::SUCCESS_MSG);

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return Redirect::back()->withErrors(['msg' => self::ERROR_MSG]);
        }
    }
}
