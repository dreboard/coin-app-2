<?php
/**
 * AdminUserActionsController is a class for viewing and managing users *
 * @package App\Http\Controllers\Admin
 * @author Andre Board
 * @version v1.0.0
 * @since v0.0.1
 * @see http://www.example.com/pear
 */
namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Users\Login;
use App\Models\Users\Violation;
use App\Notifications\GeneralUserNotification;
use App\Repositories\Admin\AdminUserRepository;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;

readonly class AdminUserActionsController
{


    public function __construct(private AdminUserRepository $adminUserRepository)
    {
    }

    /**
     * Find user from search form
     *
     * @param Request $request
     * @return Application|Factory|View
     */
    public function findUser(Request $request): View|Factory|Application
    {
        $request->validate([
            'searchTerm' => 'required|string'
        ]);
        $searchTerm = $request->input('searchTerm');
        $data = User::where('id', '=', $searchTerm)
            ->orWhere('name', 'LIKE', "%$searchTerm%")->get();

        return view('admin.user-results', ['users' => $data]);
    }


    /**
     * View all users
     *
     * @return Application|Factory|View
     */
    public function viewAllUsers(): View|Factory|Application
    {
        $loginsToday = $this->adminUserRepository->getLoginTodayUsers();
        $loginsMonth = $this->adminUserRepository->getLoginMonthUsers();
        $loginsYear = $this->adminUserRepository->getLoginYearUsers();

        $createdToday = $this->adminUserRepository->countRegisteredUsersToday();
        $createdMonth = $this->adminUserRepository->countRegisteredUsersMonth();
        $createdYear = $this->adminUserRepository->countRegisteredUsersYear();

        $users = $this->adminUserRepository->getAllUsers();
        return view('admin.user.all', [
            'users' => $users,
            'loginsToday' => $loginsToday,
            'loginsMonth' => $loginsMonth,
            'loginsYear' => $loginsYear,
            'createdToday' => $createdToday,
            'createdMonth' => $createdMonth,
            'createdYear' => $createdYear

        ]);

    }


    /**
     * View a user
     *
     * @param int $user_id
     * @return \Application|\Factory|\RedirectResponse|\View
     */
    public function viewUser(int $user_id): \Application|\Factory|\RedirectResponse|\View
    {
        $user = User::where('id', $user_id)
            ->withCount('logins')
            ->withCount('warnings')
            ->withCount('bans')
            ->withCount('violations')
            ->first();

        if (!$user) {
            return Redirect::back()->withErrors(['User not found'])->withInput();
        }
        $reported = Violation::where('violator', 'LIKE', "%$user->name%")
            ->orWhere('violator', 'LIKE', "%$user->email%")->count();

        $last_login = Login::where('user_id', $user_id)
            ->select('created_at')
            ->orderBy('created_at', 'DESC')
            ->first();

        return view('admin.user.view', [
            'user' => $user,
            'last_login' => $last_login,
            'reported' => $reported
        ]);
    }

    /**
     * View a user
     *
     * @param int $user_id
     * @return \Application|\Factory|\RedirectResponse|\View
     */
    public function viewUserLogins(int $user_id): \Application|\Factory|\RedirectResponse|\View
    {
        $user = User::where('id', $user_id)
            ->with('logins')
            ->withCount('logins')
            ->first();

        if (!$user) {
            return Redirect::back()->withErrors(['User not found']);
        }

        $last_login = Login::where('user_id', $user_id)
            ->select('created_at')
            ->orderBy('created_at', 'DESC')
            ->first();

        return view('admin.user.logins', [
            'user' => $user,
            'last_login' => $last_login,
        ]);
    }

    /**
     * Delete a user
     *
     * @param int $user_id
     * @return RedirectResponse
     */
    public function deleteUser(int $user_id): RedirectResponse
    {
        User::destroy($user_id);
        return redirect()->route('admin.view_users')->withErrors(['User Deleted']);
    }


    /**
     * Verify a user
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function verifyUser(Request $request): RedirectResponse
    {
        $user = User::find($request->input('user_id'));
        $user->email_verified_at = now();
        $user->save();
        $user->refresh();
        return redirect()->action(
            [AdminUserActionsController::class, 'viewUser'],
            ['user_id' => $user->id]
        )->withSuccess(['User is verified']);
    }

    /**
     * Clone a user
     *
     * @param int $user_id
     * @return RedirectResponse
     */
    public function cloneUser(int $user_id): RedirectResponse
    {
        $user = User::find($user_id);
        Auth::user()->impersonate($user);
        return redirect()->route('admin.dashboard')->withInput()->withErrors(['User cloned']);
    }


    /**
     * View Save user notification form
     *
     * @param $user_id
     * @return Application|Factory|View
     */
    public function newUserMessage($user_id): View|Factory|Application
    {
        $user = User::find($user_id);
        return view('admin.messages.to_user', ['user' => $user]);
    }

    /**
     * Save user notification
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function saveUserMessage(Request $request): RedirectResponse
    {
        try {
            $user = User::findOrFail($request->input('user_id'));
            $data = [];
            $data['subject'] = $request->input('subject');
            $data['body'] = $request->input('body');

            $user->notify(new GeneralUserNotification($data));

            return redirect()->action(
                [AdminUserActionsController::class, 'viewUser'],
                ['user_id' => $user->id]
            )->with('status', 'User message sent');

        } catch (Exception $e){
            Log::error($e->getMessage());
            return back()->withErrors(['user_message'=>'User Message Could Not Be Saved']);
        }

    }

    /**
     * Show change email view
     * @return Application|Factory|View
     */
    public function newMessage(): View|Factory|Application
    {
        return view('admin.message_new');
    }

}
