<?php

namespace App\Http\Controllers\Admin;

use App\Events\User\WarnUser;
use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;

class AdminWarnUserController extends Controller
{

    private const ERROR_MSG = 'User could not be warned';

    private const IS_NOT_WARNED_MSG = 'User is good';

    private const IS_WARNED_MSG = 'User is warned';

    private array $data = [
        'subject' => 'Your account is now in warning status',
        'body' => 'You have been issued a warning for violating the policies of this website.',
        'from' => 'The Administrator'
    ];


    /**
     * Warn a user
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function warnUser(Request $request): RedirectResponse
    {
        $request->validate([
            'user_id' => 'required|integer',
            'status' => 'required|in:good,warn'
        ]);

        try{
            $user = User::findOrFail($request->input('user_id'));
            if($request->input('status') == 'warn'){
                WarnUser::dispatch($user, $this->data);
                $message = self::IS_WARNED_MSG;
            } else {
                $message = self::IS_NOT_WARNED_MSG;
            }
            $user->status = $request->input('status');
            $user->save();
            $user->refresh();

            return redirect()->action(
                [AdminUserActionsController::class, 'viewUser'],
                ['user_id' => $request->input('user_id')]
            )->with('error', $message);

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return Redirect::back()->withErrors(['msg' => self::ERROR_MSG]);
        }
    }
}
