<?php
/**
 * CollectedController | actions for views/coins
 *
 * @package Collected
 * @subpackage Coins
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Collected;

use App\Actions\Coins\CollectedNameAction;
use App\Helpers\GradeHelper;
use App\Helpers\Types\TypeHelper;
use App\Helpers\VarietyHelper;
use App\Http\Controllers\Controller;
use App\Models\Coins\Coin;
use App\Models\Coins\CoinType;
use App\Models\Coins\CoinVariety;
use App\Models\Coins\Collected;
use App\Models\Project;
use App\Models\Tag;
use App\Repositories\Coin\CoinVarietyRepository;
use App\Repositories\Coin\CollectedRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Throwable;

class CollectedController extends Controller
{


    public function __construct(
        private readonly CoinVarietyRepository $coinVarietyRepository,
        private readonly CollectedRepository $collectedRepository,
        private readonly CollectedNameAction   $collectedNameAction)
    {}

    /**
     * Collecting index page
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $categories = TypeHelper::COIN_CATEGORIES;
        $projects = Project::where('user_id', auth()->user()->id)->get();
        return view('collected.index', [
            'projects' => $projects,
            'categories' => $categories,
        ]);
    }




    /**
     * View collected coin
     *
     * @param Collected $collected
     * @return View|Factory|Application
     */
    public function viewCollected(Collected $collected): View|Factory|Application
    {
        $collected = $collected->load('coin', 'notes', 'images', 'varieties');
        $color = in_array($collected->coin->coinCategory,GradeHelper::COLOR_CATEGORIES) ? 1 : 0;
        $full  = in_array($collected->coin->coinType,GradeHelper::FULL_TYPES) ? 1 : 0;
        $proof  = in_array($collected->coin->strike,GradeHelper::PROOF_STRIKE) ? 1 : 0;
        $variety_list = $collected->coin->varieties->pluck('variety')->unique()->toArray();
        $varieties_added = $collected->varieties->sortBy('variety')->toArray();

        $coin_in = $this->collectedRepository->getCollectedIn($collected);
        if($collected->set_id !== 0){
            $collected->load('collectedSet:id,nickname');
        } elseif ($collected->roll_id == 1){
            $coin_in = $collected->collectedSet();
        } else {
            $coin_in = [];
        }

        return view('collected.view', [
            'collected' => $collected,
            'color' => $color,
            'full' => $full,
            'proof' => $proof,
            'variety_list' => $variety_list,
            'varieties_added' => $varieties_added,
            'coin_in' => $coin_in ?? [],
        ]);
    }


    /**
     * Collecting index page ajax info
     *
     * @return JsonResponse
     */
    public function indexInfo(): JsonResponse
    {
        try{
            $coins = Coin::select('id','coinName', 'coinYear', 'mintMark')->where('cointypes_id', $type_id)->orderBy('coinYear')->get()->toArray();
            $image = 'http://cdn.dev-php.site/public/img/coins/'.str_replace(' ', '_', TypeHelper::COIN_TYPES[$type_id]).'.jpg';
            return response()->json(['coins' => $coins, 'image' => $image]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return response()->json(['error' => 'Could not load']);
        }
    }





    /**
     * View start page
     *
     * @return Application|Factory|View
     */
    public function start()
    {
        $categories = TypeHelper::COIN_CATEGORIES;
        return view('collected.start', [
            'categories' => $categories
        ]);
    }


    /**
     * Get array of coin types
     *
     * @param string $type_id
     * @return JsonResponse
     */
    public function populateCoins(string $type_id): JsonResponse
    {
        $coins = Coin::select('id','coinName', 'coinYear', 'mintMark')->where('cointypes_id', $type_id)->orderBy('coinYear')->get()->toArray();
        return response()->json(['coins' => $coins, 'image' => str_replace(' ', '_', TypeHelper::COIN_TYPES[$type_id])]);
    }

    /**
     * Save A coin by ID
     *
     * @param int $id
     * @return Factory|View|Application
     */
    public function createById(int $id): Factory|View|Application
    {
        $coin = Coin::findOrFail($id);
        $gradeList = GradeHelper::gradeList($coin->strike);
        $circulated = GradeHelper::isCirculated($coin->strike);
        $sub_types = VarietyHelper::getSubTypesByCoinId($coin->id);
        $color = in_array($coin->coinCategory,GradeHelper::COLOR_CATEGORIES) ? 1 : 0;
        $full  = in_array($coin->coinType,GradeHelper::FULL_TYPES) ? 1 : 0;
        $proof  = in_array($coin->strike,GradeHelper::PROOF_STRIKE) ? 1 : 0;
        $recent = Collected::where('user_id', auth()->user()->id)->where('coin_id', $id)
            ->latest()
            ->take(5)->get();

        return view('collected.create', [
            'recents' => $recent,
            'coin' => $coin,
            'gradeList' => $gradeList,
            'circulated' => $circulated,
            'color' => $color,
            'full' => $full,
            'proof' => $proof,
            'sub_types' => $sub_types ?? 'None',
        ]);
    }

    public function createMultipleById(Coin $coin, int $amount)
    {
        //$coin = Coin::findOrFail($id);
        $gradeList = GradeHelper::gradeList($coin->strike);
        $circulated = GradeHelper::isCirculated($coin->strike);
        $sub_types = VarietyHelper::getSubTypesByCoinId($coin->id);
        $color = in_array($coin->coinCategory,GradeHelper::COLOR_CATEGORIES) ? 1 : 0;
        $full  = in_array($coin->coinType,GradeHelper::FULL_TYPES) ? 1 : 0;
        $proof  = in_array($coin->strike,GradeHelper::PROOF_STRIKE) ? 1 : 0;
        $recent = Collected::where('user_id', auth()->user()->id)->where('coin_id', $coin->id)
            ->latest()
            ->take(5)->get();
        //dd($sub_types);
        return view('collected.create_multi', [
            'recents' => $recent,
            'coin' => $coin,
            'gradeList' => $gradeList,
            'circulated' => $circulated,
            'color' => $color,
            'full' => $full,
            'proof' => $proof,
            'sub_types' => $sub_types ?? 'None',
            'amount' => $amount,
        ]);
    }



    public function createVariety(Collected $collected)
    {
        $varieties = $collected->coin->load('varieties');
        //
        $types = CoinVariety::select('variety')->distinct()->where('coin_id', $collected->coin->id)->get()->pluck('variety');//->toArray();
       //dd($types);
        $variety_list = [];
        foreach($types as $variety){
            $variety_list[$variety] = $this->coinVarietyRepository->getVarietyTypeByCoin(coin_id: $collected->coin->id, variety: $variety);
        }

        //dd($variety_list);
        $already_collected = DB::table('collected_variety')
            ->join('collected', 'collected.id', '=', 'collected_variety.collected_id')
            ->join('users', 'users.id', '=', 'collected.user_id')
            ->select('collected.id', 'collected.grade', 'collected.created_at')
            ->where('collected_id', $collected->id)
            ->get();

        return view('collected.create_variety', [
            'collected' => $collected,
            'already_collected' => $already_collected,
            'varieties' => $varieties,
        ]);
    }

    public function edit(Collected $collected)
    {
        $tags = Tag::all();
        $followers = $this->myFollowers();
        $collected->load('comments', 'tags', 'user');
        if ($collected->type == 'Collaborative') {
            $participants = $collected->load('participants:id,name'); //DB::table('participants')->where('forum_id', $collected->id)->get();
        }

        return view('members.forum.edit', [
            'followers' => $followers,
            'forum' => $collected,
            'tags' => $tags,
            'participants' => $participants ?? [],
        ]);
    }


    public function quickSave(Request $request): RedirectResponse
    {
        $request->validate([
            'coin_id' => 'required|integer',
            'coin_amount' => 'sometimes|required|integer',
        ]);

        try {
            if ($request->has('coin_amount')) {
                $amount = $request->input('coin_amount');
                for ($i = 1; $i <= $amount; $i++) {
                    $collected = new Collected;
                    $collected->user_id = auth()->user()->id;
                    $collected->coin_id = $request->input('coin_id');
                    $collected->save();
                }
                return redirect()->route('coin.view_coin', ['id' => $request->input('coin_id')]);
            }
            $collected = new Collected;
            $collected->user_id = auth()->user()->id;
            $collected->coin_id = $request->input('coin_id');
            $collected->save();
            return redirect()->route('collected.view_collected', ['id' => $collected->id]);

        } catch (Throwable $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Create');
        }
    }

    public function save(Request $request): RedirectResponse
    {
        $request->validate([
            'title' => 'sometimes|required|string',
            'coin_id' => 'required|integer',
            'image_url.*' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ]);

        try {

            $coin = Coin::findOrFail($request->input('coin_id'));
            $nickname = $this->collectedNameAction->generateCoinNickname($coin);
            $collected = new Collected;
            $collected->user_id = auth()->user()->id;
            $collected->nickname = $request->input('nickname', $nickname);
            $collected->coin_id = $request->input('coin_id');

            $collected->sub_type = $request->input('sub_type', null);
            $collected->obv = $request->input('obv', null);
            $collected->rev = $request->input('rev', null);
            $collected->mms = $request->input('mms', null);

            $collected->private = $request->input('private', null);
            $collected->circulated = $request->input('circulated');

            // grading & appearance
            $collected->tpg_service = $request->input('tpg_service', null);
            $collected->cac_sticker = $request->input('cac_sticker', null);
            $collected->tpg_serial_num = $request->input('tpg_serial_num', null);
            $collected->slab_condition = $request->input('slab_condition', null);
            $collected->special_label = $request->input('special_label', null);

            $collected->ngc_plus = $request->input('ngc_plus', null);
            $collected->pcgs_plus = $request->input('pcgs_plus', null);
            $collected->coa = $request->input('coa', null);
            $collected->coa_cert = $request->input('coa_cert', null);

            // conditional inputs
            if ($request->has('gsa_holder')) {
                $collected->gsa_holder = $request->input('gsa_holder', null);
                $collected->gsa_holder_pro = $request->input('gsa_holder_pro', null);
            }


            $collected->grade = $request->input('grade', null);
            $collected->adjectival_grade = $request->input('adjectival_grade', null);
            $collected->ten_grade = $request->input('ten_grade', null);
            $collected->die_state = $request->input('die_state', null);



            $collected->color = $request->input('color', null);
            $collected->fullAtt = $request->input('fullAtt', null);
            $collected->toned = $request->input('toned', null);
            $collected->proof_like = $request->input('proof_like', null);
            $collected->designation = $request->input('designation', null);
            $collected->strike_character = $request->input('strike_character', null);
            $collected->slab_generation = $request->input('slab_generation', null);
            $collected->strike_appearance = $request->input('strike_appearance', null);

            $collected->chop_mark = $request->input('chop_mark', null);
            $collected->chop_mark_place = $request->input('chop_mark_place', null);

            $collected->damaged = $request->input('damaged', 0);
            $collected->holed = $request->input('holed', 0);
            $collected->cleaned = $request->input('cleaned', 0);
            $collected->polished = $request->input('polished', 0);
            $collected->altered = $request->input('altered', 0);
            $collected->scratched = $request->input('scratched', 0);
            $collected->pvc_damage = $request->input('pvc_damage', 0);
            $collected->corrosion = $request->input('corrosion', 0);
            $collected->bent = $request->input('bent', 0);
            $collected->plugged = $request->input('plugged', 0);
            $collected->whizzing = $request->input('whizzing', 0);
            $collected->counterstamp = $request->input('counterstamp', 0);
            $collected->bag_mark = $request->input('bag_mark', 0);
            $collected->fingermarks = $request->input('fingermarks', 0);
            $collected->spotted = $request->input('spotted', 0);
            $collected->slide_marks = $request->input('slide_marks', 0);

            $collected->private = $request->input('private', 0);
            $collected->save();


            return redirect()->route('collected.view_collected', ['id' => $collected->id]);

        } catch (Throwable $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Coin not saved');
        }
    }


    /**
     * @param Collected $collected
     * @return Factory|View|Application
     */
    public function editImages(Collected $collected): Factory|View|Application
    {
        $collected = $collected->load('images');
        $imgCount = count($collected->images);
        return view('collected.edit_images', [
            'collected' => $collected,
            'imgCount' => $imgCount,
        ]);
    }

    public function saveImages(Request $request): RedirectResponse
    {
        $request->validate([
            'collected_id' => 'required|integer',
            'image_url.*' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ]);

        try {
            $collected = Collected::findOrFail($request->input('collected_id'));
            if ($request->has('image_url')) {
                $folder = 'users/'.Auth::user()->id.'/collected/'.$collected->id;
                foreach ($request->file('image_url') as $file) {
                    $collected->images()->create([
                        'image_url' => Storage::putFile($folder, $file),
                        'user_id' => auth()->user()->id
                    ]);
                }
            }

            return redirect()->route('collected.view_collected', ['collected' => $collected->id])->with('success', 'Images Saved.');

        } catch (Throwable $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Update Images');
        }
    }


    /**
     * Update or save notes for collected coin
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function saveNote(Request $request): JsonResponse
    {
        try {
            $collected = Collected::with('notes','coin')->findOrFail($request->input('collect_id'));
            if (!isset($collected->notes->note)) {
                $collected->notes()->update([
                    'note' => $request->input('note'),
                ]);
            } else {
                $collected->notes()->create([
                    'title' => 'Note for #'.$collected->id,
                    'note' => $request->input('note'),
                    'user_id' => auth()->user()->id
                ]);
            }

            return response()->json(['note' => $request->input('note')]);

        } catch (Throwable $e) {
            Log::error($e->getMessage());
            return response()->json(['error' => 'Could Not Update Notes']);
        }
    }

    /**
     * Update or save notes for collected coin
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function savePrivacy(Request $request): JsonResponse
    {
        try {
            $collected = Collected::findOrFail($request->input('collect_id'));
            $collected->private = $request->input('private');
            $collected->save();
            return response()->json(['private' => $request->input('private')]);

        } catch (Throwable $e) {
            Log::error($e->getMessage());
            return response()->json(['error' => 'Could Not Update Coin']);
        }
    }

    /**
     * Update or save notes for collected coin
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function saveLocked(Request $request): JsonResponse
    {
        try {
            $collected = Collected::findOrFail($request->input('collect_id'));
            $collected->private = $request->input('locked');
            $collected->save();
            return response()->json(['locked' => $request->input('locked')]);

        } catch (Throwable $e) {
            Log::error($e->getMessage());
            return response()->json(['error' => 'Could Not Update Coin']);
        }
    }

    /**
     * Delete collected coin
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(Request $request): RedirectResponse
    {
        $request->validate([
            'collected_id' => 'required|integer',
        ]);
        try{
            $collected = Collected::findOrFail($request->input('collected_id'));
            if ($collected->user_id !== auth()->user()->id) {
                return redirect()->back()->with('error', 'You can not delete this');
            }
            $folder = 'users/'.Auth::user()->id.'/collected/'.$collected->id;
            Storage::deleteDirectory($folder);

            return redirect()->route('coin.view_coin', ['coin' => $request->input('coin_id')])->with('success','Successfully Deleted');

        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Coin Was Not Deleted');
        }
    }

    public function showCollectedVarietiesList(Collected $collected, $variety)
    {
        try{
            $variety = str_replace('_', ' ', $variety);
            $variety_list = CoinVariety::where('coin_id', $collected->coin->id)->where('variety', $variety)->get();
            $variety_other = $collected->coin->varieties->pluck('variety')->unique()->toArray();
            return view('collected.show_variety_list', [
                'collected' => $collected,
                'variety_list' => $variety_list,
                'variety_other' => $variety_other,
                'variety' => $variety,
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Variety Was Not Found');
        }
    }

    public function showCollectedVarieties(Collected $collected, CoinVariety $variety)
    {
        try{
            return view('collected.show_variety', [
                'collected' => $collected,
                'variety' => $variety,
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Variety Was Not Found');
        }
    }


    public function assignVariety(Request $request): RedirectResponse
    {
        $request->validate([
            'collected_id' => 'required|integer',
            'variety_id' => 'required|integer',
        ]);
        try{
            $collected = Collected::findOrFail($request->input('collected_id'));
            $collected->varieties()->attach([$request->input('variety_id')]);
            if ($collected->user_id !== auth()->user()->id) {
                return redirect()->back()->with('error', 'You can not delete this');
            }

            return redirect()->route('collected.view_collected', ['collected' => $request->input('collected_id')])->with('success','Successfully Assigned');

        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Coin Was Not Assigned');
        }
    }


    /**
     * Update or save notes for collected coin
     *
     * @param Collected $collected
     * @return JsonResponse
     */
    public function getInInfo(Collected $collected): JsonResponse
    {
        try {
            $info = [];
            //$collected = Collected::findOrFail($collected->id);

            if($collected->set_id == 1){
                $inType = $collected->load('collectedSet:id,setName')->toArray();
            } elseif ($collected->roll_id == 1){
                $inType = $collected->load('collectedSet:id,setName')->toArray();
            } else {
                $inType = [];
            }

            return response()->json(['in_type' => $inType]);

        } catch (Throwable $e) {
            Log::error($e->getMessage());
            return response()->json(['error' => 'Could Not Update Coin']);
        }
    }

    /**
     * Update or save notes for collected coin
     *
     * @param Collected $collected
     * @return JsonResponse
     */
    public function getVarietyData(int $coin_id, $variety): JsonResponse
    {
        try {
            $variety_list = CoinVariety::select('id','label','variety','designation','type')
                ->where('coin_id', $coin_id)->where('variety', str_replace('_', ' ', $variety))
                ->get()->toArray();
            return response()->json(['variety_list' => $variety_list]);

        } catch (Throwable $e) {
            Log::error($e->getMessage());
            return response()->json(['error' => 'Could Not Update Coin']);
        }
    }


    /**
     * Attach variety to collected coin
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function attachVariety(Request $request): JsonResponse
    {
        $request->validate([
            'collect_id' => 'required|integer',
            'variety_id' => 'required|integer',
        ]);
        try{
            $collected = Collected::findOrFail($request->input('collect_id'));
            if ($collected->user_id !== auth()->user()->id) {
                return response()->json(['error' => 'Varieties Not Removed']);
            }
            $collected->varieties()->attach([$request->input('variety_id')]);
            return response()->json(['success' => 'Varieties Removed']);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return response()->json(['error' => 'Varieties Not Removed']);
        }
    }




    /**
     * Get array of coin types
     *
     * @todo change image url
     */
    public function loadTypeBulkData(int $type_id): JsonResponse
    {
        try{
            $coinType = CoinType::findOrFail($type_id);
            $collected_count = DB::table('collected')
                ->join('coins', 'coins.id', '=', 'collected.coin_id')
                ->where('collected.user_id',auth()->user()->id)
                ->where('coins.cointypes_id',$coinType->id)
                ->whereNull('collected.grade')
                ->whereNull('collected.tpg_service')
                ->where('collected.roll_id', '=', 0)
                ->where('collected.folder_id', '=', 0)
                ->where('collected.set_id', '=', 0)
                ->where('collected.firstday_id', '=', 0)
                ->get()->count();

            if($collected_count === 0){
                $roll_count = 0;
                $roll_value = 0;
            } else {
                $roll_count = round($coinType->rollCount / $collected_count);
                $roll_value = round($roll_count * $coinType->denomination);
            }


            return response()->json(['roll_value' => $roll_value, 'roll_type' => $coinType->coinType, 'roll_count' => $roll_count]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return response()->json(['error' => 'Could not load']);
        }
    }


}
