<?php
/**
 * CollectedSetController | actions for views/mintsets
 *
 * @package Collected
 * @subpackage Mintsets
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Collected;

use App\Actions\Coins\CollectedNameAction;
use App\Actions\Coins\SaveLooseSetAction;
use App\Http\Controllers\Controller;
use App\Models\Coins\Coin;
use App\Models\Coins\Collected;
use App\Models\Coins\CollectedSet;
use App\Models\Coins\Mintset;
use App\Repositories\Coin\CollectedRepository;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Throwable;

class CollectedSetController extends Controller
{


    public function __construct(
        private readonly CollectedRepository $collectedRepository,
        private readonly SaveLooseSetAction  $saveLooseSetAction,
        private readonly CollectedNameAction $collectedNameAction
    ){}


    /**
     * View a mintset
     *
     * @param CollectedSet $collected
     * @return View|Factory|Application
     */
    public function viewMintset(CollectedSet $collected): View|Factory|Application
    {
        $collected = $collected->load('coin');
        $coins = $collected->mintset->coins();
        return view('collected.mintsets.view', [
            'collected' => $collected,
            'coins' => $coins,
        ]);
    }

    /**
     * View collected mintset
     *
     * @param CollectedSet $set
     * @return Application|Factory|View|RedirectResponse
     */
    public function viewCollectedSet(CollectedSet $set)
    {
        if (auth()->user()->id !== $set->user_id) {
            return redirect()->route('coin.set_index')->with('error', 'Could Not perform action');
        }
        $coins = Collected::with('coin')->select('id', 'nickName', 'coin_id')->where('set_id', $set->id)
            ->get();
        return view('coins.mintsets.view_collected_set', [
            'collectedSet' => $set,
            'coins' => $coins,
        ]);
    }

    /**
     * View collected mintset
     *
     * @return Application|Factory|View
     */
    public function viewCollectedSets()
    {
        $collectedSets = CollectedSet::where('user_id', auth()->user()->id)->get();
        //dd($collectedSets);
        return view('coins.mintsets.view_collected_sets', [
            'collectedSets' => $collectedSets
        ]);
    }

    /**
     * View save mintset start page
     *
     * @return Application|Factory|View
     */
    public function start()
    {
        $setYears = Mintset::select('coinYear')->distinct()->orderBy('coinYear', 'asc')->get();
        $setTypes = Mintset::select('setType')->distinct()->orderBy('setType', 'asc')->get();
        return view('coins.mintsets.start', [
            'setYears' => $setYears,
            'setTypes' => $setTypes,
        ]);
    }


    /**
     * Get array of sets by year
     *
     * @param string $year
     * @return JsonResponse
     */
    public function populateSetYears(string $year): JsonResponse
    {
        $sets = Mintset::select('id','setName', 'setType')->where('coinYear', $year)->orderBy('coinYear')->get()->toArray();
        return response()->json(['sets' => $sets]);
    }


    /**
     * Get array of sets by type
     *
     * @param string $set_type
     * @return JsonResponse
     */
    public function populateSetTypes(string $set_type): JsonResponse
    {
        $sets = Mintset::select('id','setName', 'setType')
            ->where('setType', str_replace('_', ' ', $set_type))
            ->orderBy('coinYear')->get()->toArray();
        return response()->json(['sets' => $sets]);
    }


    /**
     * Quick save a mintset
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function quickSave(Request $request): RedirectResponse
    {
        $request->validate([
            'set_id' => 'required|int',
        ]);

        try {
            $mintset = Mintset::findOrFail($request->input('set_id'));
            $collected_set = new CollectedSet();
            $collected_set->user_id = auth()->user()->id;
            $collected_set->set_id = $request->input('set_id');
            $collected_set->save();
            $this->collectedRepository->saveSetCoins($mintset->id, $collected_set->id);

            return redirect()->route('collected.view_collected_set', ['set_id' => $collected_set->id]);

        } catch (Throwable $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Create');
        }
    }


    /**
     * Quick save a mintset
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function saveById(Request $request): RedirectResponse
    {
        $request->validate([
            'set_id' => 'required|int',
        ]);

        try {
            // set_version = default 'Issued'
            $mintset = Mintset::findOrFail($request->input('set_id'));
            $collected_set = new CollectedSet();
            //$nickname = $this->collectedNameAction->generateSetNickname($mintset);
            $collected_set->user_id = auth()->user()->id;
            $collected_set->set_id = $request->input('set_id');

            //$collected_set->nickname = $request->input('nickname', $nickname);
            if (!$request->filled('nickname')) {
                $collected_set->nickname = $this->collectedNameAction->generateSetNickname($mintset);
            }else {
                $collected_set->nickname = $request->input('nickname');
            }


            $collected_set->tpg_service = $request->input('tpg_service', null);
            $collected_set->tpg_serial_num = $request->input('tpg_serial_num', null);
            $collected_set->condition = $request->input('slab_condition', null);
            $collected_set->save();
            $this->collectedRepository->saveSetCoins($mintset->id, $collected_set->id);

            if ($request->filled('note')) {
                $collected_set->notes()->create([
                    'note' => $request->input('note'),
                    'title' => $request->input('nickname', 'Note for '.$collected_set->nickname.' #'.$collected_set->id),
                    'user_id' => auth()->user()->id
                ]);
            }

            return redirect()->route('collected.view_collected_set', ['set' => $collected_set->id]);

        } catch (Throwable $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Create');
        }
    }


    public function createById(int $id)
    {
        $mintset = Mintset::findOrFail($id);
        $coins = $mintset->coins()->toArray();
        $recent = CollectedSet::where('user_id', auth()->user()->id)->where('set_id', $mintset->id)
            ->latest()
            ->take(5)->get();

        return view('coins.mintsets.create', [
            'recents' => $recent,
            'mintset' => $mintset,
            'coins' => $coins,
        ]);
    }



    /**
     * Add loose mintset
     *
     * @return Application|Factory|View
     */
    public function createLooseSet(int $set_id)
    {
        $mintset = Mintset::findOrFail($set_id);
        $coins = $mintset->coins();

        $collected = $this->collectedRepository->getOpenCollectedCoinsById($coins);
        $recents = CollectedSet::where('user_id', auth()->user()->id)->where('set_id', $mintset->id)
            ->latest()
            ->take(5)->get();
        //dd($collected);

        return view('coins.mintsets.create_loose', [
            'mintset' => $mintset,
            'coins' => $coins,
            'collected' => $collected,
            'recents' => $recents,
        ]);
    }
    /**
     * Add loose mintset
     *
     *
     */
    public function saveLooseSet(Request $request): RedirectResponse
    {
        $request->validate([
            'set_id' => 'required|int',
        ]);

        try {
            $mintset = Mintset::findOrFail($request->input('set_id'));
            $set_coins = $mintset->coins()->pluck('id')->toArray();
            $nickname = $this->collectedNameAction->generateCoinNickname($request->input('set_id'));
            $collected_set = new CollectedSet();
            $collected_set->user_id = auth()->user()->id;
            $collected_set->nickname = $request->input('nickname', $nickname);
            $collected_set->set_id = $request->input('set_id');
            $collected_set->set_version = $request->input('set_version');
            $collected_set->private = $request->input('private', 0);
            $collected_set->save();

            if ($request->has('coin')) {
                $this->saveLooseSetAction->saveMintsetCoins($request, $collected_set);
            } else {
                $this->collectedRepository->saveSetCoins($mintset->id, $collected_set->id);
            }

            if ($request->filled('note')) {
                $collected_set->notes()->create([
                    'title' => $request->input('nickname', 'Note for '.$mintset->setName.' #'.$collected_set->id),
                    'note' => $request->input('note'),
                    'user_id' => auth()->user()->id
                ]);
            }
            return redirect()->route('collected.view_collected_set', ['set' => $collected_set->id]);

        } catch (Throwable $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Create');
        }
    }




    /**
     * Add slabbed mintset
     *
     * @return Application|Factory|View
     */
    public function saveSlabbedSet(int $set_id)
    {
        $mintset = Mintset::findOrFail($set_id);
        //dd($mintset->coins()->pluck('id'));
        $coins = Coin::select('id','coinName', 'coinType')
            ->whereIn('id', $mintset->coins()->pluck('id'))
            ->orderBy('denomination')
            ->get()->toArray();

        $recents = CollectedSet::where('user_id', auth()->user()->id)->where('set_id', $mintset->id)
            ->latest()
            ->take(5)->get();
        return view('coins.mintsets.create_slabbed', [
            'mintset' => $mintset,
            'coins' => $coins,
            'coins_count' => count($coins),
            'recents' => $recents,
        ]);
    }


    /**
     * View collected mintset
     *
     * @param Request $request
     * @param CollectedSet $set
     * @return Application|Factory|View|RedirectResponse
     */
    public function delete(Request $request, CollectedSet $set)
    {
        $request->validate([
            'set_id' => 'required|int',
        ]);
        if ($request->user()->cannot('delete', $set)) {

            return redirect()->route('coins.set_index');
        }
        $set = CollectedSet::find($request->input('set_id'));
        $set->delete();
        return view('coins.mintsets.set_index');
    }


    /**
     * View collected mintset
     *
     * @param Request $request
     * @param CollectedSet $set
     * @return Application|Factory|View
     */
    public function deleteAll(Request $request, CollectedSet $set)
    {
        $request->validate([
            'set_id' => 'required|int',
        ]);
        if ($request->user()->cannot('delete', $set)) {
            return view('coins.mintsets.set_index')->with('error', 'Could Not perform action');
        }
        $set = CollectedSet::find($request->input('set_id'));
        Collected::where('set_id', $set->id)->delete();
        $set->delete();
        return view('coins.mintsets.set_index')->with('success', 'Set Deleted');
    }


}
