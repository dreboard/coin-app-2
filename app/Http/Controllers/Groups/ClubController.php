<?php

namespace App\Http\Controllers\Groups;

use App\Http\Controllers\Controller;
use App\Models\Groups\Group;
use Exception;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;

class ClubController extends Controller
{


    /**
     * Shows a message thread.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function view(int $id): \Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
    {
        try {
            $group = Group::findOrFail($id);
            Gate::authorize('view', $group);
            return view('groups.group.view', ['group' => $group]);

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->route('groups.group.manage');
        }
    }


    public function manage()
    {
        return view('groups.group.view');
    }
}
