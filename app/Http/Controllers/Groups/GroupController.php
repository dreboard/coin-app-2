<?php
/**
 * GroupController | actions for views/groups
 *
 * @package Groups
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Groups;

use App\Http\Controllers\Controller;
use App\Models\Groups\Group;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class GroupController extends Controller
{


    /**
     * Groups index page.
     *
     * @return Factory|View|Application
     */
    public function index(): Factory|View|Application
    {
        $featured = Group::inRandomOrder()->limit(1)->get();
        $newest = Group::latest('id', 'name')
            ->first();
        $groupsIFollow = User::with('followables')->get();
        //dd($featured)
        $groups = Group::select('id', 'name', 'specialty','group_type')->get();
        $group = Group::select('id', 'name')->where('user_id', auth()->user()->id)->first();
        return view('groups.index',[
            'my_group' => $group,
            'groups' => $groups,
            'featured' => $featured,
        ]);
    }

    /**
     * Show all groups.
     *
     * @return Factory|View|Application
     */
    public function allGroups(): Factory|View|Application
    {
        $groups = Group::select('id', 'name', 'specialty','group_type')->get();
        return view('groups.all', ['groups' => $groups]);
    }


    /**
     * Show users groups.
     *
     * @return Factory|View|Application
     */
    public function myGroups(): Factory|View|Application
    {
        $groups = DB::table('group_user')
            ->select('group_user.group_id', 'groups.name', 'groups.group_type', 'groups.specialty', 'groups.id')
            ->where('group_user.user_id', auth()->user()->id)
            ->join('groups', 'groups.id', '=', 'group_user.group_id')
            ->get();
        return view('groups.mine', [
            'groups' => $groups
        ]);
    }


    public function viewDirectory()
    {
        $groups = Group::select('id', 'name', 'specialty','group_type')->get();
        $group = Group::select('id', 'name')->where('user_id', auth()->user()->id)->first();
        return view('groups.directory',[
            'my_group' => $group,
            'groups' => $groups,
        ]);
    }


    public function search(string $search)
    {
        $groups = Group::select('id', 'name', 'specialty','group_type')
            ->where('name','LIKE',"$search%")->get();
        $my_group = Group::select('id', 'name')->where('user_id', auth()->user()->id)->first();
        return view('groups.results',[
            'my_group' => $my_group,
            'groups' => $groups,
            'search' => $search,
        ]);
    }


    public function searchForm(Request $request)
    {
        $request->validate([
            'searchTerm' => 'required|string',
        ]);
        $groups = Group::select('id', 'name', 'specialty','group_type')
            ->where('name','LIKE',"%{$request->input('searchTerm')}%")->get();
        $my_group = Group::select('id', 'name')->where('user_id', auth()->user()->id)->first();
        return view('groups.results',[
            'my_group' => $my_group,
            'groups' => $groups,
            'search' => $request->input('searchTerm'),
        ]);
    }

}
