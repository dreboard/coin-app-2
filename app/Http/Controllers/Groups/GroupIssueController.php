<?php
/**
 * GroupIssueController | actions for views/groups/group/issues
 *
 * @package Groups
 * @subpackage Voting
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Groups;

use App\Http\Controllers\Controller;
use App\Http\Traits\GroupTrait;
use App\Models\Groups\Agenda;
use App\Models\Groups\Event;
use App\Models\Groups\Group;
use App\Models\Groups\Issue;
use App\Models\Groups\IssueVote;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class GroupIssueController extends Controller
{


    use GroupTrait;


    public function index(Group $group)
    {

        $issues = Issue::with('votes')->where('group_id', $group->id)->get();

        return view('groups.group.issues.index', [
            'group' => $group,
            'issues' => $issues,
        ]);
    }

    /**
     * View an issue
     *
     * @param Group $group
     * @param Issue $issue
     * @return Application|Factory|View
     */
    public function view(Group $group, Issue $issue)
    {
        $issue_voted_on = IssueVote::where('issue_votes.issue_id', $issue->id)
            ->where('issue_votes.voter', auth()->user()->id)->exists();

        $votes = IssueVote::with('user:id,name,first_name,last_name')->select('issue_votes.vote', 'issue_votes.voter')
            ->where('issue_votes.issue_id', $issue->id)
            ->groupBy('issue_votes.vote', 'issue_votes.voter')
            ->get();
        $grouped_votes = $votes->groupBy('vote');

        $groupwithcount = $grouped_votes->mapWithKeys(function ($group, $key) {
            return [
                $key =>
                    [
                        'vote' => $key, // $key is what we grouped by, it'll be constant by each  group of rows
                        'all_votes' => $group->sum('vote'),
                        'for' => $group->where('vote', 1)->count(),
                        'against' => $group->where('vote', 0)->count(),
                    ]
            ];
        });

        dd($votes,$grouped_votes, $groupwithcount);
        //dd($grouped_votes);

        return view('groups.group.issues.view', [
            'group' => $group,
            'issue' => $issue,
            'votes' => $votes,
            'grouped_votes' => $grouped_votes,
            'groupwithcount' => $groupwithcount,
            'issue_voted_on' => $issue_voted_on
        ]);
    }

    /**
     * Create an issue
     *
     * @param Group $group
     * @return Application|Factory|View|RedirectResponse
     */
    public function create(Group $group): View|Factory|RedirectResponse|Application
    {
        try {
            $events = Event::whereDate('expired_at', '>=', Carbon::now())
            ->where('group_id', $group->id)->get();
            //dd($events);
            return view('groups.group.issues.create', [
                'events' => $events,
                'group' => $group,
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not access group page');
        }
    }


    /**
     * Save an issue
     *
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     */
    public function save(Request $request): View|Factory|RedirectResponse|Application
    {
        //dd($request);
        $request->validate([
            'group_id' => 'required|integer',
            'start_at' => 'date',
            'expired_at' => 'date|nullable|after_or_equal:start_at',
            'issue' => 'required|string|max:100',
            'transparency' => 'required|string',
            'vote_level' => 'required|string',
        ]);

        try {

            $group = Group::findOrFail($request->input('group_id'));
            $issue = Issue::create([
                'start_at' => date('Y-m-d H:i:s', strtotime($request->input('start_at'))),
                'expired_at' => date('Y-m-d H:i:s', strtotime($request->input('expired_at'))),
                'issue' => $request->input('issue'),
                'group_id' => $request->input('group_id'),
                'transparency' => $request->input('transparency'),
                'vote_level' => $request->input('vote_level'),
            ]);

            if ($request->input('event_id') > 0) {
                $this->createAgendaForIssue($issue, $request);
            } else {
                $this->issueMessage($issue, $request);
            }

            return view('groups.group.issue.view', [
                'group' => $group,
                'issue' => $issue,
            ]);

        } catch (Exception $e) {
            Log::error($e->getMessage() . ',' . $e->getTraceAsString());
            return redirect()->back()->with('error', 'Could not save issue');
        }
    }


    public function createAgendaForIssue($issue, $request)
    {
        Agenda::create([
                'topic' => 'Vote on '.$issue->name,
                'action' => 'Pending',
                'description' => 'Vote on '.$issue->name,
                'issue_id' => $issue->id,
                'event_id' => $request->input('event_id'),
                'group_id' => $request->input('group_id'),
                'length' => 600,// CarbonInterval::seconds($agenda->length)->forHumans();
            ]);
    }

    /**
     * Edit an issue
     *
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     */
    public function edit(Request $request): View|Factory|RedirectResponse|Application
    {
        try {
            $request->validate([
                'group_id' => 'required|integer',
                'start_at' => 'date',
                'expired_at' => 'date|nullable|after_or_equal:start_at',
                'issue' => 'required|string|max:100',
                'transparency' => 'required|string',
                'vote_level' => 'required|string',
            ]);

            $group = Group::findOrFail($request->input('group_id'));
            $issue = Issue::create([
                'start_at' => date('Y-m-d H:i:s', strtotime($request->input('start_at'))),
                'expired_at' => date('Y-m-d H:i:s', strtotime($request->input('expired_at'))),
                'issue' => $request->input('issue'),
                'group_id' => $request->input('group_id'),
                'transparency' => $request->input('transparency'),
                'vote_level' => $request->input('vote_level'),
            ]);

            if ($request->has('create_announcement')) {
                $this->electionAnnouncement($request, $request->input('group_id'));
            }
            if ($request->has('send_message')) {
                $this->electionMessage($request, $request->input('group_id'));
            }
            return view('groups.group.issue.view', [
                'group' => $group,
                'issue' => $issue,
            ]);

        } catch (Exception $e) {
            Log::error($e->getMessage() . ',' . $e->getTraceAsString());
            return redirect()->back()->with('error', 'Could not save issue');
        }
    }


}
