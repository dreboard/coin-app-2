<?php
/**
 * GroupEventController | actions for views/groups/group/events
 *
 * @package Events
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Groups;

use App\Helpers\ManageGroupHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\GroupTrait;
use App\Models\Groups\Agenda;
use App\Models\Groups\Event;
use App\Models\Groups\Group;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GroupEventController extends Controller
{

    use GroupTrait;


    public function index(Group $group)
    {
        if (!$this->isGroupUser($group->id)) {
            return redirect()->route('group.public', ['group' => $group->id]);
        }
        $events = Event::where('group_id', $group->id)
            ->whereMonth('start_at', date('m'))->get();
        $event_types = ['Board Meeting', 'Meetup', 'General', 'Show','Committee', 'Club', 'Special'];
        $event_list = [];
        foreach ($event_types as $k => $type){
            $event_list[$type] = Event::where('group_id', $group->id)
                ->whereMonth('start_at', date('m'))
                ->where('event_type', $type)
                ->count();
        }
        //dd($event_list);

        return view('groups.group.events.index', [
            'group' => $group,
            'events' => $events,
            'event_list' => $event_list,
        ]);
    }


    /**
     * View an event
     *
     * @param Group $group
     * @param Event $event
     * @return View|Factory|RedirectResponse|Application
     */
    public function view(Group $group, Event $event): View|Factory|RedirectResponse|Application
    {
        if (!$this->isGroupUser($group->id)) {
            return redirect()->route('group.public', ['group' => $group->id]);
        }
        $event = Event::with('agendas', 'participants')
            ->where('id', $event->id)->first();

        $user_manage_level = auth()->user()->userManageLevel($group->id);
        $secretary = $this->getSecretary($group);
        dd($secretary);
        $eventPast = Carbon::parse($event->expired_at)->isPast();
        $present = (string)DB::table('event_user')
            ->where('event_id', $event->id)
            ->where('participant', auth()->user()->id)
            ->exists();

        //
        return view('groups.group.events.view', [
            'group' => $group,
            'event' => $event,
            'user_manage_level' => $user_manage_level,
            'eventPast' => $eventPast,
            'present' => $present,
        ]);
    }

    public function getEventByType(Group $group, string $type)
    {
        $event = Event::with('agendas', 'participants')->where('group_id', $group->id)->first();

        //dd($event);
        return view('groups.group.events.view', [
            'group' => $group,
            'event' => $event
        ]);
    }

    /**
     * Create an event
     *
     * @param Group $group
     * @return Application|Factory|View|RedirectResponse
     */
    public function create(Group $group): View|Factory|RedirectResponse|Application
    {
        try {
            $events = Event::whereDate('expired_at', '>=', Carbon::now())
                ->where('group_id', $group->id)->get();
            //dd($events);
            return view('groups.group.events.create', [
                'events' => $events,
                'group' => $group,
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not access group page');
        }
    }

    public function getSecretary(Group $group): Collection
    {
        return DB::table('group_role')
            ->select('group_role.id AS board_member_role_id', 'groups.id AS board_member_group_id', 'users.id AS board_member_user_id', 'users.name AS board_member_user_name',
                'users.first_name AS board_member_first_name', 'users.last_name AS board_member_last_name', 'roles.name AS board_member_position_name',
                'group_role.approved_at AS position_approved_at', 'group_role.expired_at AS position_expired_at')
            ->where('group_role.group_id', $group->id)
            ->where('roles.id', ManageGroupHelper::GROUP_BOARD_POSITIONS['Secretary'])
            ->join('users', 'users.id', '=', 'group_role.user_id')
            ->join('roles', 'roles.id', '=', 'group_role.role_id')
            ->join('groups', 'groups.id', '=', 'group_role.group_id')
            ->get();
    }

    public function changeAgendaAction(Request $request): JsonResponse
    {
        $agenda = Agenda::findOrFail($request->input('id'));
        $agenda->action = $request->input('agenda_action');
        $agenda->save();
        return response()->json([
            'agenda_action' => $request->input('agenda_action'),
            'id' => $request->input('id'),
        ]);
    }

    public function addParticipantToEvent(Request $request): JsonResponse
    {
        try{
            $event = Event::findOrFail($request->input('event_id'));
            $event->participants()
                ->attach($request->input('user_id'));
            return response()->json([
                'checked' => 1,
            ]);
        }catch (Exception $e){
            return response()->json([
                'checked' => 0,
            ]);
        }

    }

}
