<?php
/**
 * GroupMessageController | actions for views/groups/group/message
 *
 * @package Groups
 * @subpackage Messaging
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Groups;

use App\Http\Controllers\Controller;
use App\Http\Traits\GroupTrait;
use App\Models\Groups\Group;
use App\Notifications\GroupNotification;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GroupMessageController extends Controller
{

    use GroupTrait;


    /**
     * Group messaging index page
     *
     * @param Group $group
     * @return Application|Factory|View|RedirectResponse
     */
    public function index(Group $group)
    {
        if (!$this->isGroupUser($group->id)) {
            return redirect()->route('group.public', ['group' => $group->id]);
        }

        $data = [];
        $data['subject'] = 'User question';
        $data['body'] = 'Question for the group';
        $group->notify(new GroupNotification($data, $group));
        return view('groups.group.messaging.index', [
            'group' => $group,
        ]);
    }

    /**
     * Group message inbox
     *
     * @param Group $group
     * @return Application|Factory|View|RedirectResponse
     */
    public function inbox(Group $group)
    {
        if (!$this->isGroupUser($group->id)) {
            return redirect()->route('group.public', ['group' => $group->id]);
        }

        return view('groups.group.messaging.inbox', [
            'group' => $group,
            'messages' => $group->notifications,
        ]);
    }


    /**
     * View group message
     *
     * @param Group $group
     * @param $id
     * @return \Application|\Factory|\RedirectResponse|\View
     */
    public function view(Group $group, $id): \Application|\Factory|\RedirectResponse|\View
    {
        try{
            if (!$this->isGroupUser($group->id)) {
                return redirect()->route('group.public', ['group' => $group->id]);
            }
            $notification = $group
                ->notifications
                ->where('id', $id)
                ->firstorFail();
            $notification->markAsRead();
            return view('groups.group.messaging.view', [
                'notification' => $notification,
                'group' => $group,
            ]);
        } catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Notification Not found');
        }
    }

    /**
     * Create group message
     *
     * @param Group $group
     * @return Application|Factory|RedirectResponse|View
     */
    public function groupMessage(Group $group)
    {
        try {
            if (!$this->isGroupUser($group->id)) {
                return redirect()->route('group.public', ['group' => $group->id]);
            }
            $users = DB::table('group_user')
                ->select('group_user.id AS gid','users.id AS uid', 'users.user_type', 'users.name AS uname', 'group_user.member_type', 'group_user.approved_at')
                ->where('group_user.group_id', $group->id)
                ->join('users', 'users.id', '=', 'group_user.user_id')
                ->get();
            return view('groups.group.messaging.group', [
                'users' => $users,
                'group' => $group,
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not access group messages');
        }
    }

    /**
     * Delete a messages
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function destroy(Request $request): RedirectResponse
    {
        try{
            $group = Group::findOrFail($request->input('group_id'));
            $group->notifications()->where([
                'id' => $request->input('notification_id'),
                'notifiable_id' => $group->id
            ])->delete();
            return redirect()->route('group.group_inbox', ['group' => $group])
                ->with('success', 'Message deleted');
        } catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Message Not Deleted');
        }

    }

    /**
     * Delete all messages
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function destroyAll(Request $request): RedirectResponse
    {
        try{
            $group = Group::findOrFail($request->input('group_id'));
            $group->notifications()->delete();

            return redirect()->back()->with('success', 'Messages Deleted');
        } catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Messages Not Deleted');
        }
    }


    public function contactForm(Group $group)
    {
        return view('groups.contact', [
            'group' => $group,
        ]);
    }


    public function contact(Request $request): RedirectResponse
    {
        $request->validate([
            'subject' => 'required|string',
            'body' => 'required|string',
        ]);
        try{
            $group = Group::findOrFail($request->input('group_id'));
            $data = [];
            $data['from'] = auth()->user()->id;
            $data['subject'] = $request->input('subject');
            $data['body'] = $request->input('body');
            $group->notify(new GroupNotification($data, $group));

            return redirect()->route('group.public', ['group' => $group])
                ->with('success', 'Message Sent');
        } catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Messages Not Deleted');
        }
    }

}
