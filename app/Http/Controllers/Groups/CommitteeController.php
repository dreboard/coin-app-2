<?php
/**
 * CommitteeController | actions for views/groups/group/committees
 *
 * @package Groups
 * @subpackage Committee
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Groups;

use App\Helpers\ManageGroupHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\GroupTrait;
use App\Models\Groups\Committee;
use App\Models\Groups\Group;
use App\Models\Groups\Report;
use App\Models\User;
use App\Notifications\GeneralUserNotification;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 *
 */
class CommitteeController extends Controller
{

    use GroupTrait;


    /**
     * @param Group $group
     * @return Application|Factory|View|RedirectResponse
     */
    public function index(Group $group)
    {
        if (!$this->isGroupUser($group->id)) {
            return redirect()->route('group.public', ['group' => $group->id]);
        }
        $committees = Committee::with('chair:id,name,first_name,last_name')->where('group_id', $group->id)->get();

        $typeList = $committees->pluck('type')->toArray();
        return view('groups.group.committees.index', [
            'group' => $group,
            'committees' => $committees,
            'typeList' => $typeList,
        ]);
    }


    /**
     * View a committee
     *
     * @param Group $group
     * @param Committee $committee
     * @return Factory|View|RedirectResponse|Application
     */
    public function view(Group $group, Committee $committee): Factory|View|RedirectResponse|Application
    {
        if (!$this->isGroupUser($group->id)) {
            return redirect()->route('group.public', ['group' => $group->id]);
        }
        try{
            $committee = Committee::with('members:id,name,first_name,last_name')
                ->where('id', $committee->id)
                ->first();
            if($committee){
                $chair = $committee->chair()->get();
            }

            $candidates = DB::table('group_user')
                ->select('group_user.id AS gid', 'users.id AS user_id', 'users.name', 'group_user.club_position', 'group_user.member_type', 'group_user.approved_at',
                    'users.name AS user_name', 'users.first_name AS user_first_name', 'users.last_name AS user_last_name')
                ->where('group_user.group_id', $group->id)
                ->whereIn('group_user.member_type', ManageGroupHelper::NOMINEE_TYPES)
                ->join('users', 'users.id', '=', 'group_user.user_id')
                ->get();

            //dd($candidates);
            return view('groups.group.committees.committee.view', [
                'group' => $group,
                'committee' => $committee,
                'chair' => $chair[0] ?? [],
                'candidates' => $candidates,

            ]);
        }catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not access committee page');
        }

    }


    /**
     * Assign members to committee
     *
     * @param Group $group
     * @param Committee $committee
     * @return Factory|View|RedirectResponse|Application
     */
    public function assignCommitteeMembers(Group $group, Committee $committee): Factory|View|RedirectResponse|Application
    {
        try{
            if (!$this->isCommitteeMember($committee->id) && !$this->isBoardMember($group->id)) {
                return redirect()->back()->with('error','You are not a committee member');
            }
            $committee = Committee::with('members:id,name,first_name,last_name')
                ->where('id', $committee->id)
                ->first();
            $memberList = $committee->members->pluck('id')->toArray();
            $candidates = DB::table('group_user')
                ->select('group_user.id AS gid', 'users.id AS user_id', 'users.name', 'group_user.club_position', 'group_user.member_type', 'group_user.approved_at',
                    'users.name AS user_name', 'users.first_name AS user_first_name', 'users.last_name AS user_last_name')
                ->where('group_user.group_id', $group->id)
                ->whereIn('group_user.member_type', ManageGroupHelper::NOMINEE_TYPES)
                ->whereNotIn('group_user.user_id', $memberList)
                ->join('users', 'users.id', '=', 'group_user.user_id')
                ->get();
            $userIsMember = in_array(auth()->user()->id, $memberList);
            $userIsBoardMember = $this->isBoardMember($group->id);
            return view('groups.group.committees.committee.assign', [
                'group' => $group,
                'committee' => $committee,
                'candidates' => $candidates,
                'userIsMember' => $userIsMember,
                'userIsBoardMember' => $userIsBoardMember,
            ]);
        }catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not access committee page');
        }

    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function saveNewMembers(Request $request): RedirectResponse
    {
        if (!$this->isCommitteeMember($request->input('committee_id'))) {
            return redirect()->back()->with('error','You are not a committee member');
        }
        $request->validate([
            'committee_id' => 'required|integer',
            'members' => 'required|min:1',
        ]);
        try{
            $committee = Committee::with('group:name,id')->findOrFail($request->input('committee_id'));
            foreach ($request->input('members') as $committee_member){
                $this->placeMemberInRole($request->input('group_id'), $committee_member, $committee, ManageGroupHelper::GROUP_POSITIONS['Committee Member']);
            }
            return redirect()->back()->with('success','New members assigned');

        }catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not assign to committee');
        }
    }

    /**
     * Remove member from committee
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function removeCommitteeMember(Request $request): RedirectResponse
    {
        if (!$this->isCommitteeMember($request->input('committee_id'))) {
            return redirect()->back()->with('error','You are not a committee member');
        }
        try{
            DB::table('committee_user')->where('committee_id', $request->input('committee_id'))
                ->where('user_id', $request->input('user_id'))->delete();
            return redirect()->back()->with('success','Member removed');

        }catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could remove member');
        }
    }

    public function newCommitteeChair(Request $request)
    {
        $request->validate([
            'committee_id' => 'required|integer',
            'chair' => 'required|integer',
        ]);
        $committee = Committee::findOrFail($request->input('committee_id'));

        $exists = DB::table('committee_user')
            ->where('user_id', $request->input('user_id'))
            ->where('committee_id', $committee->id)
            ->exists();
        if(!$exists){
            $committee->members()
                ->attach($request->input('user_id'));
            (User::findOrFail($request->input('user_id')))->notify(new GeneralUserNotification($data)); //@todo ??? data
        }

        DB::table('group_role')->insert([
            'group_id' => $committee->group_id,
            'user_id' => $request->input('chair'),
            'role_id' => $this->chooseChairPosition($committee->type),
            'expired_at' => Carbon::now()->addYear(1)
        ]);
    }

    /**
     *
     *
     * @param int $group_id
     * @param int $user_id
     * @param mixed $committee
     * @param int $role_id
     * @return void
     * @todo place in repository
     */
    public function placeMemberInRole(int $group_id, int $user_id, mixed $committee, int $role_id)
    {
        $has_role = DB::table('group_role')->where('group_id', $group_id)
            ->where('role_id', $role_id)->where('user_id', $user_id)->exists();
        $user = User::findOrFail($user_id);
        DB::table('group_role')->insert([
            'group_id' => $group_id,
            'user_id' => $user_id,
            'role_id' => ManageGroupHelper::GROUP_POSITIONS['Committee Member'],
            'expired_at' => Carbon::now()->addYear(1)
        ]);
        $data = [];
        $data['subject'] = 'You have been assigned to '.$committee->title;
        $data['body'] = 'The Chair of the '.$committee->title.' has assigned you as a committee member.  Please visit the Committee section for
            the group '.$committee->group->name;
        $committee->members()
            ->attach($user_id);
        $user->notify(new GeneralUserNotification($data));


    }

    /**
     * Save a committee
     *
     * @param Group $group
     * @return Application|Factory|View|RedirectResponse
     */
    public function create(Group $group): View|Factory|RedirectResponse|Application
    {
        try {
            $types = Arr::flatten(Committee::select('type')
                ->where('group_id', $group->id)
                ->get('type')->toArray());
            //dd($types);
            $candidates = DB::table('group_user')
                ->select('group_user.id AS gid', 'users.id AS user_id', 'users.name', 'group_user.club_position', 'group_user.member_type', 'group_user.approved_at',
                    'users.name AS user_name', 'users.first_name AS user_first_name', 'users.last_name AS user_last_name')
                ->where('group_user.group_id', $group->id)
                ->whereIn('group_user.member_type', ManageGroupHelper::NOMINEE_TYPES)
                ->join('users', 'users.id', '=', 'group_user.user_id')
                ->get();
            return view('groups.group.committees.create', [
                'group' => $group,
                'candidates' => $candidates,
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not save the committee');
        }
    }


    /**
     * @return void
     */
    public function filterCommitteeTypes()
    {
        $committee_types = ManageGroupHelper::GROUP_COMMITTEE_TYPES;
    }


    /**
     * Save Committee
     *
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     */
    public function saveCommittee(Request $request): View|Factory|RedirectResponse|Application
    {
        //dd($request->input('chair') > 0);
        $request->validate([
            'title' => 'required|string|max:100',
            'chair' => 'required|integer',
            'type' => 'required|string',
            'details' => 'required|string',
            'group_id' => 'required|integer',
        ]);

        try {
            $group = Group::findOrFail($request->input('group_id'));
            $committee = Committee::create([
                    'group_id' => $request->input('group_id'),
                    'title' => $request->input('title'),
                    'type' => $request->input('type'),
                    'chair' => $request->input('chair'),
                    'details' => $request->input('details')
                ]);

            if ($request->input('chair') > 0) {
                DB::table('group_role')->insert([
                    'group_id' => $committee->group_id,
                    'user_id' => $request->input('chair'),
                    'role_id' => $this->chooseChairPosition($committee->type),
                    'expired_at' => Carbon::now()->addYear(1)
                ]);
            }
            if ($request->input('members') > 0) {
                foreach ($request->input('members') as $committee_member){
                    $this->placeMemberInRole($request->input('group_id'), $committee_member, $committee
                        , ManageGroupHelper::GROUP_POSITIONS['Committee Member']);
                }
            }
            if ($request->has('create_announcement')) {
                $data = [];
                $data['group_id'] = $request->input('group_id');
                $data['user_id'] = auth()->user()->id;
                $data['message'] = 'The '.$committee->title.' has been created';
                $this->createAnnouncement($data);
            }

            return redirect()->route('group.view_committee', [
                'group' => $group,
                'committee' => $committee,
            ]);

        } catch (Exception $e) {
            Log::error($e->getMessage() . ',' . $e->getTraceAsString());
            return redirect()->back()->with('error', 'Could not save issue');
        }
    }


    /**
     * @param Group $group
     * @param Committee $committee
     * @return Application|Factory|View|RedirectResponse
     */
    public function viewReports(Group $group, Committee $committee)
    {
        try {
            if (!$this->isCommitteeMember($committee->id) && !$this->isBoardMember($group->id)) {
                return redirect()->back()->with('error','You are not a committee member');
            }
            $reports = $committee->reports()->get();

            return view('groups.group.committees.committee.reports', [
                'group' => $group,
                'reports' => $reports,
                'committee' => $committee,
            ]);

        } catch (Exception $e) {
            Log::error($e->getMessage() . ',' . $e->getTraceAsString());
            return redirect()->back()->with('error', 'Could not load reports');
        }
    }

    /**
     * @param Group $group
     * @param Committee $committee
     * @param Report $report
     * @return Application|Factory|View|RedirectResponse
     */
    public function viewReport(Group $group, Committee $committee, Report $report)
    {
        try {
            if (!$this->isCommitteeMember($committee->id) && !$this->isBoardMember($group->id)) {
                return redirect()->back()->with('error','You are not a committee member');
            }

            return view('groups.group.committees.committee.report', [
                'group' => $group,
                'report' => $report,
                'committee' => $committee,
            ]);

        } catch (Exception $e) {
            Log::error($e->getMessage() . ',' . $e->getTraceAsString());
            return redirect()->back()->with('error', 'Could not load report');
        }
    }

    /**
     * Save a report
     *
     * {@internal NO validation as it is a working document, fields are nullable}
     * @param Group $group
     * @param Committee $committee
     * @return Application|Factory|View|RedirectResponse
     */
    public function createReport(Group $group, Committee $committee)
    {
        try {
            if (!$this->isCommitteeMember($committee->id) && !$this->isBoardMember($group->id)) {
                return redirect()->back()->with('error','You are not a committee member');
            }
            return view('groups.group.committees.committee.new_report', [
                'group' => $group,
                'committee' => $committee,
            ]);

        } catch (Exception $e) {
            Log::error($e->getMessage() . ',' . $e->getTraceAsString());
            return redirect()->back()->with('error', 'Could not create report');
        }
    }

    /**
     * Save a report
     *
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     * @see self::updateReport() for missing fields
     */
    public function saveReport(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:100',
            'type' => 'required|string',
        ]);
        try {
            $report = new Report();
            $report->title = $request->input('title');
            $report->type = $request->input('type');
            $report->group_id = $request->input('group_id');
            $report->user_id = auth()->user()->id;
            $report->reportable_type = Committee::class;
            $report->reportable_id = $request->input('committee_id');
            $report->published = $request->input('published', 0);
            $report->save();
            $report->refresh();

            return view('groups.group.committees.committee.report', [
                'group' => Group::findOrFail($request->input('group_id')),
                'report' => $report,
                'committee' => Group::findOrFail($request->input('committee_id')),
            ]);

        } catch (Exception $e) {
            Log::error($e->getMessage() . ',' . $e->getTraceAsString());
            return redirect()->back()->with('error', 'Report not created');
        }
    }

    /**
     * Update a report
     *
     * {@internal NO validation as it is a working document, (*) fields are nullable}
     * @param Request $request
     * @return RedirectResponse
     */
    public function updateReport(Request $request): RedirectResponse
    {
        try {
            $report = Report::findOrFail($request->input('report_id'));
            $report->title = $request->input('title');
            $report->type = $request->input('type');
            $report->user_id = auth()->user()->id;
            $report->overview = $request->input('overview', ''); // *
            $report->background = $request->input('background', ''); // *
            $report->discussion = $request->input('discussion', ''); // *
            $report->budget = $request->input('budget', ''); // *
            $report->conclusion = $request->input('conclusion', ''); // *
            $report->recommendations = $request->input('recommendations'); // *
            $report->published = $request->input('published', 0);
            $report->save();
            $report->refresh();

            return redirect()->back()->with('success', 'Report saved');

        } catch (Exception $e) {
            Log::error($e->getMessage() . ',' . $e->getTraceAsString());
            return redirect()->back()->with('error', 'Report not updated');
        }
    }


    /**
     * @param $committee_type
     * @return array|int
     */
    public function chooseChairPosition($committee_type): array|int
    {
        // ['Educational','Nominations','Membership','Show','General','Financial'];
        return match ($committee_type) {
            'Membership' => ManageGroupHelper::GROUP_POSITIONS['Membership Director'],
            'Educational' => ManageGroupHelper::GROUP_POSITIONS['Activities Director'],
            'Show' => ManageGroupHelper::GROUP_POSITIONS['Show Chairman'],
            'Financial' => ManageGroupHelper::GROUP_POSITIONS['Finance Director'],
            'Nominations' => ManageGroupHelper::GROUP_POSITIONS['Election Director'],
            default => ManageGroupHelper::GROUP_POSITIONS['Committee Chair'], // 'General'
        };
    }

}
