<?php
/**
 * GroupRequestController | actions for views/groups/group/users
 *
 * @package Groups
 * @subpackage Manage
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Groups;

use App\Helpers\ManageGroupHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\GroupTrait;
use App\Models\Groups\Group;
use App\Models\User;
use App\Notifications\GeneralUserNotification;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GroupRequestController extends Controller
{
    use GroupTrait;

    /**
     * View group public page
     *
     * @param Group $group
     * @return Application|Factory|View|RedirectResponse
     */
    public function viewPublicPage(Group $group)
    {
        try {
            if (auth()->user()->isClubUser($group["id"])) {
                return redirect()->action(
                    [ManageGroupController::class, 'view'],
                    ['group' => $group]);
            }
            return view('groups.public', ['group' => $group]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->route('group.all')
                ->with(['error' => 'The group was not found']);
        }
    }

    /**
     * View Public page from group page
     *
     * @param Group $group
     * @return Application|Factory|View|RedirectResponse
     */
    public function viewPublicPagePreview(Group $group)
    {
        try {
            return view('groups.public', ['group' => $group, 'is_member' => true]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Club Was Not Found');
        }
    }

    /**
     * Show join group application
     *
     * @param Group $group
     * @return Application|Factory|View|RedirectResponse
     */
    public function viewApplicationPage(Group $group)
    {
        try {
            if (auth()->user()->isClubUser($group["id"])) {
                return redirect()->action(
                    [ManageGroupController::class, 'view'],
                    ['group' => $group]);
            }
            return view('groups.application', ['group' => $group]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->route('group.all')
                ->with(['error' => 'The group was not found']);
        }
    }

    /**
     * Save group request application
     *
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            $group = Group::findOrFail($request->id);

            if ($request->has(['first_name', 'last_name', 'phone', 'address'])) {
                $request->validate([
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'phone' => 'required',
                    'address' => 'required',
                    'postalcode' => 'required',
                    'city' => 'required',
                    'state' => 'required',
                ]);

                DB::table('users')
                    ->where('id', auth()->user()->id)
                    ->update([
                        'first_name' => $request->input('first_name'),
                        'last_name' => $request->input('last_name'),
                        'phone' => $request->input('phone'),
                        'address' => $request->input('address'),
                        'postalcode' => $request->input('postalcode'),
                        'city' => $request->input('city'),
                        'state' => $request->input('state'),
                    ]);
            }
            DB::table('group_request')->insert([
                'user_id' => auth()->user()->id,
                'group_id' => $request->input('group_id')
            ]);


            return view('groups.request', ['group' => $group]);

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->route('group.all')
                ->with(['error' => 'The group was not found']);;
        }
    }

    /**
     * Process group request
     * @todo ????????
     * @param int $id
     * @return Application|Factory|View|RedirectResponse
     */
    public function process(int $id)
    {
        try {
            $group = Group::findOrFail($id);
            return view('groups.request', ['group' => $group]);

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->route('group.all')
                ->with(['error' => 'The group was not found']);;
        }
    }

    public function joinGroup(Request $request): RedirectResponse
    {
        $request->validate([
            'user_id' => 'required|integer',
            'group_id' => 'required|integer',
        ]);
        try {

            $user = User::findOrFail($request->input('user_id'));
            $group = Group::findOrFail($request->input('group_id'));

            DB::table('group_user')->insert([
                'user_id' => $user->id,
                'group_id' => $group->id,
                'club_position' => 'Member',
                'member_type' => 'Full',
                'member_level' => 'Member',
                'manage' => ManageGroupHelper::MANAGE_LEVELS['Member Level'],
                'created_at' => now(),
                'updated_at' => now(),
                'approved_at' => now(),
            ]);

            $url = "<a href='".route('group.view', ['group' => $group->id])."'>View Club</a>";
            $data = [
                'subject' => 'Your club request is approved',
                'body' => "Your club request for the group ".$group->name." is approved.  Welcome to the club.
                Your my groups page will now list this group. $url",
                'from' => $group->name
            ];
            $user->notify(new GeneralUserNotification($data));

            return to_route('group.view', [
                'group' => $group,
            ])->with('success','Welcome to the group');

        } catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load group');
        }
    }

    /**
     * Process a users request
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function processRequest(Request $request): RedirectResponse
    {
        try{
            $user = User::findOrFail($request->input('uid'));
            $group = Group::findOrFail($request->input('gid'));

            if($request->input('member_type') == 'deny') {
                $this->processDenial(
                    $request->input('uid'),
                    $request->input('gid'),
                    $group->name
                );
                return redirect()->back()->with('error','Membership Denied');
            }

            $user->groups()->attach(
                $request->input('gid'),
                ['member_type' => $request->input('member_type')]
            );

            DB::table('group_request')
                ->where('user_id', '=', $request->input('uid'))
                ->where('group_id', '=', $request->input('gid'))
                ->delete();
            return redirect()->back()->with('status','Memberships Approved');
        } catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could Not Process GroupRequest');
        }
    }


    /**
     * View all group requests
     *
     * @param Group $group
     * @return Application|Factory|View|RedirectResponse
     */
    public function view(Group $group)
    {
        try{
            $requests = DB::table('group_request')
                ->select('group_request.id AS grid','groups.id AS gid','users.id AS uid', 'users.name AS uname', 'group_request.created_at AS gr_created_at')
                ->where('group_request.group_id', $group->id)
                ->join('users', 'users.id', '=', 'group_request.user_id')
                ->join('groups', 'groups.id', '=', 'group_request.group_id')
                ->get() ?? false;
            //$member_director = ManageGroupHelper::GROUP_POSITION_NUMBERS['Membership Director'] ?? 'None';
            $users = DB::table('group_user')
                ->select('group_user.id AS gid','users.id AS uid', 'users.user_type', 'users.name AS uname',
                    'group_user.member_type', 'group_user.approved_at', 'users.first_name', 'users.last_name')
                ->where('group_user.group_id', $group->id)
                ->whereIn('group_user.member_type', ManageGroupHelper::NOMINEE_TYPES)
                ->join('users', 'users.id', '=', 'group_user.user_id')
                ->get();

            $member_director = DB::table('group_role')
                ->select('users.id AS uid', 'users.name AS uname',
                    'roles.name AS rname', 'group_role.approved_at AS  role_approved_at', 'group_role.expired_at AS role_expired_at')
                ->where('group_role.group_id', $group->id)
                ->where('group_role.role_id', ManageGroupHelper::GROUP_POSITION_KEYS['Membership Director'])
                ->join('users', 'users.id', '=', 'group_role.user_id')
                ->join('roles', 'roles.id', '=', 'group_role.role_id')
                ->first() ?? 'None';
            //
            //
            return view('groups.group.users.requests', [
                'requests' => $requests,
                'group' => $group,
                'member_director' => $member_director,
                'director' => ManageGroupHelper::GROUP_POSITION_KEYS['Membership Director'],
                'users' => $users
            ]);
        } catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could Not View GroupRequest');
        }
    }

    public function assignRole(Request $request): RedirectResponse
    {
        try{
            DB::table('group_role')->insert([
                'group_id' => $request->input('group_id'),
                'user_id' => $request->input('user_id'),
                'role_id' => $request->input('role_id'),
                'expired_at' => Carbon::now()->addYear(1),
            ]);
            return redirect()->back()->with('success','Role Assigned');
        } catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Role Not Assigned');
        }
    }



    /**
     * Bulk process all requests
     *
     * @param Request $request
     * @return RedirectResponse|void
     */
    public function processAllRequest(Request $request)
    {
        try{
            $users = DB::table('group_request')
                ->where('group_id', $request->input('group_id'))
                ->pluck('user_id')->toArray();
            $group = Group::findOrFail($request->input('group_id'));
            $data = [
                'subject' => 'Your club request is approved',
                'body' => 'Your club request for the group '.$group->name.' is approved',
                'from' => $group->name
            ];
            if($request->input('member_type') == 'deny') {
                foreach ($users as $user){
                    $this->processDenial(
                        $user,
                        $request->input('group_id'),
                        $group->name
                    );
                    return redirect()->back()->with('error','Memberships Denied');
                }
            } else {
                foreach ($users as $user){
                    $new_user = User::find($user);
                    $new_user->notify(new GeneralUserNotification($data));
                    $new_user->groups()->attach($request->input('group_id'), ['member_type' => $request->input('member_type')]);
                    DB::table('group_request')
                        ->where('user_id', '=', $new_user->id)
                        ->where('group_id', '=', $request->input('group_id'))
                        ->delete();
                }
                return redirect()->back()->with('status','Memberships Approved');
            }
        } catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could Not Process GroupRequest');
        }
    }

    /**
     * Process group request denial
     *
     * @param int $user_id
     * @param int $group_id
     * @param string $name
     * @return void
     */
    public function processDenial(int $user_id, int $group_id, string $name)
    {
        $data = [
            'subject' => 'Your club request is denied',
            'body' => 'Your club request for the group '.$name.' is denied',
            'from' => $name
        ];

        DB::table('group_request')
            ->where('user_id', '=', $user_id)
            ->where('group_id', '=', $group_id)
            ->delete();
        (User::find($user_id))->notify(new GeneralUserNotification($data));
    }


}
