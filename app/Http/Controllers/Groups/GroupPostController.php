<?php /** @noinspection ALL */

/**
 * GroupPostController | actions for views/groups/group/posts
 *
 * @package Groups
 * @subpackage Posts
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Groups;

use App\Helpers\ManageGroupHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\GroupTrait;
use App\Models\Comment;
use App\Models\Groups\Event;
use App\Models\Groups\Group;
use App\Models\Post;
use App\Models\Tag;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class GroupPostController extends Controller
{

    use GroupTrait;

    /**
     * Display a listing of the resource.
     *
     * @param Group $group
     * @return Application|Factory|View
     */
    public function index(Group $group)
    {
        $posts = Post::with(['tags'])
            ->where('group_id', $group->id)
            ->latest()->paginate(10);

        return view('groups.group.posts.index', [
            'group' => $group,
            'posts' => $posts,
        ]);
    }


    /**
     * View a post
     *
     * @param Group $group
     * @param Post $post
     * @return View|Factory|RedirectResponse|Application
     */
    public function view(Group $group, Post $post): View|Factory|RedirectResponse|Application
    {
        if (!$this->isGroupUser($group->id)) {
            return redirect()->route('group.public', ['group' => $group->id]);
        }

        $post = Post::withCount('comments')->with('comments.replies', 'tags')->where('id', $post->id)->first();
        $comments = Comment::with('replies')
            ->where('commentable_id','=',$post->id)
            ->where('commentable_type','=',Post::class)
            ->get();

        if ($post->user_id !== auth()->user()->id) {
            $post->views += 1;
            $post->save();
        }

        $user_manage_level = auth()->user()->userManageLevel($group->id);
        $user_roles = ManageGroupHelper::getUserRoles($group, auth()->user()->id);
        $editor = ManageGroupHelper::getPosition($group, ManageGroupHelper::GROUP_POSITION_KEYS['Editor']);

        return view('groups.group.posts.view', [
            'group' => $group,
            'post' => $post,
            'user_manage_level' => $user_manage_level,
            'editor' => $editor,
            'comments' => $comments,
        ]);
    }


    /**
     * Show the form for creating a post
     *
     * @param Group $group
     * @return Application|Factory|View
     */
    public function create(Group $group)
    {
        $tags = Tag::all();
        $unpublished = Post::where('publish', 0)->where('group_id', $group->id)
            ->where('user_id', auth()->user()->id)
            ->get();

        return view('groups.group.posts.create', [
            'group' => $group,
            'tags' => $tags,
            'unpublished' => $unpublished,
        ]);
    }


    /**
     * Show the form for creating a post
     *
     * @param Group $group
     * @param Post $post
     * @return Application|Factory|View
     */
    public function edit(Group $group, Post $post)
    {
        $tags = Tag::all();
        $post = Post::with('tags')
            ->where('id', $post->id)->first();
        $saved_tags = $post->tags->pluck('id')->toArray();
        $unpublished = Post::where('publish', 0)->where('group_id', $group->id)
            ->where('user_id', auth()->user()->id)
            ->get();

        return view('groups.group.posts.edit', [
            'group' => $group,
            'tags' => $tags,
            'unpublished' => $unpublished,
            'post' => $post,
            'saved_tags' => $saved_tags,
        ]);
    }

    /**
     * Store a newly created post.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'body' => 'required',
            'image_url' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        try {
            $group = Group::findOrFail($request->input('group_id'));
            $post = new Post();

            $post->title = $request->input('title');
            $post->slug = Str::slug($request->input('title'));
            $post->image_url = 'Default.jpg';
            $post->body = $request->input('body');
            $post->group_id = $request->input('group_id');
            $post->publish = $request->input('publish', 0);
            $post->user_id = auth()->user()->id;
            $post->save();

            if ($request->has('image_url')) {
                $folder = '/posts/' . $post->id;
                $post->image_url = $this->processPostImage($request, $post, $group, $folder);
            }
            if ($request->has('tags')) {
                $post->tags()->attach($request->tags);
            }
            $post->save();

            return redirect()->route('group.view_post', ['group' => $group, 'post' => $post->id])
                ->with('status', 'Post Saved Successfully');
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Save Post');
        }

    }

    /**
     * Save post edit post.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Request $request): RedirectResponse
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'body' => 'required',
            'post_id' => 'required|integer',
            'image_url' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        try {
            $group = Group::findOrFail($request->input('group_id'));
            $post = Post::with('tags')
                ->where('id', $request->input('post_id'))->first();

            $post->title = $request->input('title');
            $post->slug = Str::slug($request->input('title'));
            //$post->image_url = 'Default.jpg';
            $post->body = $request->input('body');
            $post->group_id = $request->input('group_id');
            $post->publish = $request->input('publish', 0);
            $post->save();

            if ($request->has('image_url')) {
                $folder = '/posts/' . $post->id;
                $post->image_url = $this->processPostImage($request, $post, $group, $folder);
            }
            if ($request->has('tags')) {
                $post->tags()->sync($request->tags);
            }
            $post->save();

            return redirect()->route('group.view_post', ['group' => $group, 'post' => $post->id])
                ->with('status', 'Post Saved Successfully');
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Save Post');
        }
    }


    public function saveComment(Request $request): RedirectResponse
    {
        $request->validate([
            'comment_body' => 'required',
            'post_id' => 'required|integer',
        ]);
        try {
            $group = Group::findOrFail($request->input('group_id'));
            $post = Post::findOrFail($request->input('post_id'));

            $comment = new Comment();

            $comment->body = $request->input('comment_body');
            $comment->user_id = auth()->user()->id;
            $post->comments()->save($comment);

            return redirect()->route('group.view_post', ['group' => $group, 'post' => $post->id])
                ->with('status', 'Comment Saved Successfully');
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Save Comment');
        }
    }


    public function saveCommentReply(Request $request): RedirectResponse
    {
        $request->validate([
            'comment_body' => 'required',
            'post_id' => 'required|integer',
            'group_id' => 'required|integer',
            'comment_id' => 'required|integer',
        ]);
        try {
            $group = Group::findOrFail($request->input('group_id'));
            $post = Post::findOrFail($request->input('post_id'));

            $reply = new Comment();
            $reply->body = $request->input('comment_body');
            $reply->parent_id = $request->input('comment_id');
            $reply->user_id = auth()->user()->id;
            $post->comments()->save($reply);

            return redirect()->route('group.view_post', ['group' => $group, 'post' => $post->id])
                ->with('status', 'Comment Saved Successfully');
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Save Comment');
        }
    }
}
