<?php
/**
 * GroupCreateController | actions for creating groups
 *
 * @package Groups
 * @subpackage Manage
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Groups;

use App\Helpers\ManageGroupHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Groups\StoreClubRequest;
use App\Http\Traits\GroupTrait;
use App\Models\Groups\Group;
use App\Models\Groups\Settings;
use App\Notifications\GeneralUserNotification;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class GroupCreateController extends Controller
{

    use GroupTrait;

    /**
     * Groups start page.
     *
     * @return Factory|View|Application
     */
    public function start(): Factory|View|Application
    {
        $group = Group::select('id', 'name')->where('user_id', auth()->user()->id)->first();
        return view('groups.start',[
            'my_group' => $group,
        ]);
    }

    /**
     * Creates a new group.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('groups.create');
    }

    /**
     * Creates a new message thread.
     *
     * @return Application|Factory|View
     */
    public function createClub()
    {
        return view('groups.create_club');
    }

    /**
     * Creates a new message thread.
     *
     * @return Application|Factory|View
     */
    public function createAssociation()
    {
        return view('groups.create_association');
    }

    /**
     * Saves a new club
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeGroup(Request $request)
    {
        try {
            $group = Group::create([
                'name' => $request->input('name'),
                'group_type' => $request->input('group_type'),
                'user_id' => auth()->user()->id,
                'description' => $request->input('description'),
                'short_description' => $request->input('short_description'),
                //'image_url' =>  $path ?? 'None',
                'url' => $request->input('url', 'None'),
                'meets' => $request->input('meets'),
                'formed' => $request->input('reporter', date('Y')),
                'specialty' => $request->input('specialty'),
                'private' => $request->input('private'),
                'extra_info' => $request->input('extra_info'),
                'settings' => $request->input('settings'),
            ]);
            if($request->has('image_url')) {
                $this->processImage($request, $group);
            }
            auth()->user()->groups()->attach($group->id, [
                'club_position' => 'Founder',
                'member_type' => 'Full',
                'manage' => ManageGroupHelper::MANAGE_LEVELS['Owner'],
            ]);

            $this->createGroupSettings($group);

            return redirect()->route('group.intro', ['group' => $group->id])
                ->with(['success' => 'Club Created Successfully']);

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->route('group.all')
                ->with(['error' => 'The group was not created']);;
        }
    }

    /**
     * Saves a new club or association
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeClub(StoreClubRequest $request)
    {
        try {
            $group = Group::create([
                'name' => $request->input('name'),
                'group_type' => $request->input('group_type'),
                'user_id' => auth()->user()->id,
                'description' => $request->input('description'),
                'short_description' => $request->input('short_description'),
                'ana' => $request->input('ana'),
                //'image_url' =>  $path ?? 'None',
                'url' => $request->input('url'),
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name'),
                'phone' => $request->input('phone'),
                'email' => $request->input('email'),
                'place' => $request->input('place'),
                'address' => $request->input('address'),
                'postalcode' => $request->input('postalcode'),
                'city' => $request->input('city'),
                'state' => $request->input('state'),
                'meets' => $request->input('meets'),
                'formed' => $request->input('reporter', date('Y')),
                'specialty' => $request->input('specialty'),
                'private' => $request->input('private'),
                'extra_info' => $request->input('extra_info'),
                'settings' => $request->input('settings'),
            ]);
            if($request->has('image_url')) {
                $this->processImage($request, $group);
            }
            auth()->user()->groups()->attach($group->id, [
                'club_position' => 'Member',
                'member_type' => 'Full',
                'manage' => ManageGroupHelper::MANAGE_LEVELS['Executive Level'],
            ]);

            $this->createGroupSettings($group);

            return redirect()->route('group.intro', ['group' => $group->id])
                ->with(['success' => 'Club Created Successfully']);

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->route('group.all')
                ->with(['error' => 'The group was not created']);;
        }
    }
    /**
     * Saves a new club or association
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function groupFromDiscussion(StoreClubRequest $request)
    {
        try {
            $group = Group::create([
                'name' => $request->input('name'),
                'group_type' => $request->input('group_type'),
                'user_id' => auth()->user()->id,
                'description' => $request->input('description'),
                'short_description' => $request->input('short_description'),
                'ana' => $request->input('ana'),
                //'image_url' =>  $path ?? 'None',
                'url' => $request->input('url'),
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name'),
                'phone' => $request->input('phone'),
                'email' => $request->input('email'),
                'place' => $request->input('place'),
                'address' => $request->input('address'),
                'postalcode' => $request->input('postalcode'),
                'city' => $request->input('city'),
                'state' => $request->input('state'),
                'meets' => $request->input('meets'),
                'formed' => $request->input('reporter', date('Y')),
                'specialty' => $request->input('specialty'),
                'private' => $request->input('private'),
                'extra_info' => $request->input('extra_info'),
                'settings' => $request->input('settings'),
            ]);
            if($request->has('image_url')) {
                $this->processImage($request, $group);
            }
            auth()->user()->groups()->attach($group->id, [
                'club_position' => 'Member',
                'member_type' => 'Full',
                'manage' => ManageGroupHelper::MANAGE_LEVELS['Executive Level'],
            ]);

            $this->createGroupSettings($group);

            return redirect()->route('group.intro', ['group' => $group->id])
                ->with(['success' => 'Club Created Successfully']);

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->route('group.all')
                ->with(['error' => 'The group was not created']);;
        }
    }

    /**
     * Create default group settings
     *
     * @param Group $group
     * @return void
     */
    public function createGroupSettings(Group $group)
    {
        Settings::create([
            'group_id' => $group->id,
            'edit' => ManageGroupHelper::MANAGE_LEVELS['Director Level'],
            'posts' => ManageGroupHelper::MANAGE_LEVELS['Staff Level'],
            'events' => ManageGroupHelper::MANAGE_LEVELS['Staff Level'],
            'committees' => ManageGroupHelper::MANAGE_LEVELS['Director Level'],
            'officers' => ManageGroupHelper::MANAGE_LEVELS['Director Level'],
            'positions' => ManageGroupHelper::MANAGE_LEVELS['Director Level'],
            'announcements' => ManageGroupHelper::MANAGE_LEVELS['Director Level'],
            'members' => ManageGroupHelper::MANAGE_LEVELS['Director Level'],
            'elections' => ManageGroupHelper::MANAGE_LEVELS['Director Level'],
            'media' => ManageGroupHelper::MANAGE_LEVELS['Staff Level'],
        ]);
    }

    /**
     * @param Group $group
     * @return void
     */
    public function groupSetup(Group $group)
    {
        auth()->user()->groups()->attach($group->id, [
            'club_position' => 'Founder',
            'member_type' => 'Full',
            'manage' => ManageGroupHelper::MANAGE_LEVELS['Owner'],
        ]);

        $url = "<a href='".route('group.view', ['group' => $group->id])."'>View Club</a>";
        $data = [
            'subject' => 'New Group Created',
            'body' => $group->name." has been created by you.
                Your my groups page can now be accessed, $url",
            'from' => 'The Administrator'
        ];
        auth()->user()->notify(new GeneralUserNotification($data));

    }


}
