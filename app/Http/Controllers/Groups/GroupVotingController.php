<?php /** @noinspection ALL */
/** @noinspection ALL */
/** @noinspection ALL */
/** @noinspection ALL */

/**
 * GroupVotingController | actions for views/groups/group/voting
 *
 * @package Groups
 * @subpackage Voting
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Groups;

use App\Helpers\ManageGroupHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\GroupTrait;
use App\Models\Groups\Group;
use App\Models\Groups\GroupElection;
use App\Models\Groups\Role;
use App\Models\Groups\Vote;
use App\Models\User;
use App\Notifications\GeneralUserNotification;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

/**
 *
 */
class GroupVotingController extends Controller
{

    use GroupTrait;


    /**
     * Show main voting page list
     *
     * @param Group $group
     * @return Application|Factory|View
     */
    public function index(Group $group): View|Factory|Application
    {
        $election = DB::table('group_elections')
            ->select('group_elections.id','group_elections.name',
                'group_elections.election_type', 'group_elections.roles', 'group_elections.start_at', 'group_elections.expired_at')
            ->where('group_elections.group_id', $group->id)
            ->whereDate('group_elections.expired_at','>=', date('Y-m-d'))
            ->get();
        return view('groups.group.voting.index', [
            'group' => $group,
            'election' => $election[0] ?? [],
        ]);
    }


    /**
     * Create a new election
     *
     * @param Group $group
     * @return Application|Factory|View|RedirectResponse
     */
    public function createBoardElection(Group $group): View|Factory|RedirectResponse|Application
    {
        try {
            $positions = ManageGroupHelper::ELECTED_POSITIONS;
            return view('groups.group.voting.create_board_election', [
                'group' => $group,
                'positions' => $positions,
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not access group page');
        }
    }




    /**
     * Save new election
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function saveBoardElection(Request $request)
    {
        $request->validate([
            'group_id' => 'required|integer',
            'start_at' => 'date',
            'expired_at' => 'date|nullable|after_or_equal:start_at',
            'roles.*' => 'required|integer',
        ]);
        try {

            //dd($request);
            //dd($request->input('roles'));
            /*$exist = DB::table('group_role')
                ->where('user_id', $request->input('user_id'))
                ->where('user_id', $request->input('user_id'))
                ->where('role_id', $request->input('role_id'))
                ->exists();

            if($exist){
                return redirect()->back()->with('error','Position Already Assigned');
            }

            DB::table('group_elections')->insert([
                'start_at' => $request->input('start_at'),
                'expired_at' => $request->input('expired_at'),
                'role_id' => $request->input('role_id'),
                'group_id' => $request->input('group_id'),
                'name' => $this->generateElectionName($request),
            ]);*/
            $role = DB::table('roles')->select('name')->where('id', $request->input('role_id'))->first();
            $group = Group::findOrFail($request->input('group_id'));
            $election = GroupElection::create([
                'start_at' => date('Y-m-d H:i:s', strtotime($request->input('start_at'))),
                'expired_at' => date('Y-m-d H:i:s', strtotime($request->input('expired_at'))),
                'roles' => implode(",", $request->input('roles')),
                'election_type' => 'Board',
                'group_id' => $request->input('group_id'),
                'name' => $this->generateElectionName($request),
            ]);

            if($request->has('create_announcement')) {
                $this->electionAnnouncement($request, $request->input('group_id'));
            }
            if($request->has('send_message')) {
                $this->electionMessage($request, $request->input('group_id'));
            }

            return redirect()->route('group.election_progress', ['group' => $group, 'election' => $election]);


        } catch (Exception $e) {
            Log::error($e->getMessage().','.$e->getTraceAsString());
            return redirect()->back()->with('error','Could not save election');
        }
    }


    /**
     * @param Request $request
     * @return RedirectResponse|void
     */
    public function saveElection(Request $request)
    {
        $request->validate([
            'group_id' => 'required|integer',
            'start_at' => 'date',
            'expired_at' => 'date|nullable|after_or_equal:start_at',
            'issue' => 'required|integer',
        ]);
        try {
            $role = Role::select('name')->where('id', $request->input('role_id'))->first();

            $exist = DB::table('group_role')
                ->where('user_id', $request->input('user_id'))
                ->where('user_id', $request->input('user_id'))
                ->where('role_id', $request->input('role_id'))
                ->exists();

            if($exist){
                return redirect()->back()->with('error','Position Already Assigned');
            }

            DB::table('group_elections')->insert([
                'start_at' => $request->input('start_at'),
                'expired_at' => $request->input('expired_at'),
                'role_id' => $request->input('role_id'),
                'group_id' => $request->input('group_id'),
                'name' => $this->generateElectionName($request),
            ]);
            if($request->has('create_announcement')) {
                $this->electionAnnouncement($request, $request->input('group_id'));
            }
            if($request->has('send_message')) {
                $this->electionMessage($request, $request->input('group_id'));
            }

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not save election');
        }
    }


    /**
     * Show voting page with candidates
     *
     * @param Group $group
     * @param GroupElection $election
     * @return Application|Factory|View|RedirectResponse
     */
    public function voteElection(Group $group, GroupElection $election)
    {
        try{
            if(Carbon::parse($election->expired_at)->isPast()){


                return redirect()->action(
                    [GroupVotingController::class, 'electionResults'],
                    ['group' => $group, 'election' => $election]);
            }

            $role_voted_on = DB::table('votes')->select('votes.role_id')//->distinct('votes.role_id')
            ->where('votes.election_id', $election->id)
                ->where('votes.voter', auth()->user()->id)
                ->where('group_elections.group_id', $group->id)
                ->join('group_elections', 'group_elections.id', '=', 'votes.election_id')
                ->get()->pluck('role_id')
                ->toArray();
            //dd($role_voted_on);
            $election1 = GroupElection::with('nominees.user')->findOrFail($election->id);

            if($election1){
                $positions = $election1->nominees->groupBy('role_id')->toArray();
            } else {
                $positions = [];
            }

            return view('groups.group.voting.vote', [
                'group' => $group,
                'election' => $election,
                'positions' => $positions,
                'role_voted_on' => $role_voted_on ?? [],
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load election');
        }
    }

    /**
     * Save election vote
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function saveElectionVotes(Request $request): RedirectResponse
    {
        try {
            $exist = DB::table('votes')
                ->where('role_id', $request->input('role_id'))
                ->where('election_id', $request->input('election_id'))
                ->where('voter', $request->input('voter'))
                ->where('candidate', $request->input('candidate')[0])
                ->exists();

            if($exist){
                return redirect()->back()->with('error','You have already voted on this');
            }

            $request->validate([
                'election_id' => 'required|integer',
                //'candidate' => 'required|array|in:1,2,3,4',
                'voter' => 'required|integer',
                'role_id' => [
                    Rule::unique("votes")->where(
                        function ($query) use ($request) {
                            return $query->where(
                                [
                                    ["election_id", "=", $request->input('election_id')],
                                    ["role_id", "=", $request->input('role_id')],
                                    ["voter", "=", $request->input('voter')],
                                    ["candidate", "=", $request->input('candidate')[0]],
                                ]
                            );
                        }
                    )]]);

            DB::table('votes')->insert([
                'role_id' => $request->input('role_id'),
                'election_id' => $request->input('election_id'),
                'voter' => $request->input('voter'),
                'candidate' => $request->input('candidate')[0],
                "created_at" =>  Carbon::now()
            ]);
            return redirect()->back()->with('success','Thank you for your vote');

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not save your vote');
        }
    }


    /**
     * View election results
     *
     * @param Group $group
     * @param GroupElection $election
     * @return Application|Factory|View|RedirectResponse
     */
    public function electionProgress(Group $group, GroupElection $election)
    {
        if(Carbon::parse($election->expired_at)->isPast()){
            return redirect()->route('election_results', ['group' => $group, 'election' => $election]);
        }

        $voters = DB::table('group_user')
            ->select('group_user.user_id AS voter')
            ->where('group_user.group_id', '=', $group->id)
            ->get()->count();


        //$voters->values()->get(4);
        //
        $results = Vote::with('user', 'role')->select('role_id', 'candidate', DB::raw('COUNT(candidate) as candidate_votes'))
            ->where('election_id', $election->id)
            ->groupBy('role_id', 'candidate')
            ->get();

        $results_by_role = $results->groupBy('role_id');
        $election1 = GroupElection::with('nominees.user')->findOrFail($election->id);
        if($election1){
            $positions = $election1->nominees->groupBy('role_id')->toArray();
        } else {
            $positions = [];
        }
        return view('groups.group.voting.election', [
            'results' => $results,
            'group' => $group,
            'election' => $election,
            'positions' => $positions,
            'results_by_role' => $results_by_role,
            'voters' => $voters,
        ]);
    }

    /**
     * View election results
     *
     * @param Group $group
     * @param GroupElection $election
     * @return Application|Factory|View|RedirectResponse
     */
    public function electionResults(Group $group, GroupElection $election)
    {
        try{
            if(Carbon::parse($election->expired_at)->isPast() === false){
                return redirect()->route('election_progress', ['group' => $group, 'election' => $election]);
            }

            $roles = explode(',', $election->roles);
            foreach($roles as $role){
                $this->assignElectionWinner($group, $election, $role);
            }

            $winners = DB::table('group_role')
                ->select('group_role.id AS bmgrid','groups.id AS bmgid','users.id AS bmuid', 'users.name AS bmuname', 'roles.name AS bmrname', 'group_role.approved_at AS bmapproved_at', 'group_role.expired_at AS bmexpired_at')
                ->where('group_role.group_id', $group->id)
                ->whereIn('roles.id', $roles)
                ->join('users', 'users.id', '=', 'group_role.user_id')
                ->join('roles', 'roles.id', '=', 'group_role.role_id')
                ->join('groups', 'groups.id', '=', 'group_role.group_id')
                ->get();

            return view('groups.group.voting.results', [
                'group' => $group,
                'election' => $election,
                'winners' => $winners,
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load election results');
        }
    }

    /**
     * View election results
     *
     * @param Group $group
     * @param GroupElection $election
     * @return Application|Factory|View|RedirectResponse
     */
    public function pastElection(Group $group, GroupElection $election)
    {
        try{
            $roles = explode(',', $election->roles);

            $results = Vote::with('user', 'role')->select('role_id', 'candidate', DB::raw('COUNT(candidate) as candidate_votes'))
                ->where('election_id', $election->id)
                ->groupBy('role_id', 'candidate')
                ->get();

            $results_by_role = $results->groupBy('role_id');
            $election1 = GroupElection::with('nominees.user')->findOrFail($election->id);
            if($election1){
                $positions = $election1->nominees->groupBy('role_id')->toArray();
            } else {
                $positions = [];
            }
            return view('groups.group.voting.results', [
                'results' => $results,
                'group' => $group,
                'election' => $election,
                'positions' => $positions,
                'results_by_role' => $results_by_role,
            ]);

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load election results');
        }
    }

    /**
     * View election results
     *
     * @param Group $group
     * @return Application|Factory|View|RedirectResponse
     */
    public function pastElections(Group $group)
    {
        try{
            $past_elections = GroupElection::with('nominees')->where('group_id', $group->id)->get();

            return view('groups.group.voting.past_results', [
                'group' => $group,
                'past_elections' => $past_elections,
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load election results');
        }
    }


    public function myVotingHistory(Group $group)
    {
        $results = Vote::with('candidate', 'role', 'election')->select('election_id', 'role_id', 'candidate', DB::raw('COUNT(candidate) as candidate_votes'))
            ->where('voter', auth()->user()->id)
            ->groupBy('election_id', 'role_id', 'candidate')
            ->orderBy('election_id')
            ->get();
        dd($results);

    }


    /**
     * Assign position to election winner
     *
     * @todo move to repository
     * @return void
     */
    public function assignElectionWinner($group, GroupElection $election, int $role_id)
    {
        $this->demotePreviousOffice($group->id, $role_id);
        $winner = DB::table('votes')->select('candidate')
            ->where('role_id', $role_id)
            ->where('election_id', $election->id)
            ->groupBy('candidate')
            ->orderByRaw('count(candidate) DESC')
            ->value('candidate');

        $user = User::findOrFail($winner);

        DB::table('group_role')->insert([
            'user_id' => $user->id,
            'group_id' => $group->id,
            'role_id' => $role_id,
            'approved_at' => now(),
            'expired_at' => Carbon::now()->addYear(1),
        ]);
        DB::table('group_user')
            ->where('user_id', $user->id)
            ->where('group_id', $group->id)
            ->update([
                'club_position' => ManageGroupHelper::ELECTED_POSITIONS[$role_id],
                'manage' => ManageGroupHelper::MANAGE_LEVELS['Executive Level'],
                'member_type' => 'Executive'
            ]);
        $data = [];
        $data['subject'] = 'Congratulation on your win in the '.$election->name.'';
        $data['body'] =  'Congratulation on your win in the '.$election->name.' for the position of '.ManageGroupHelper::ELECTED_POSITIONS[$role_id].' ';

        $user->notify(new GeneralUserNotification($data));

    }


    /**
     * demoting the previous position holder
     *
     * @param $group_id
     * @param int $role_id
     * @return void
     * @todo move to repository
     */
    public function demotePreviousOffice($group_id, int $role_id): void
    {
        DB::table('group_user')
            ->where('club_position', ManageGroupHelper::ELECTED_POSITIONS[$role_id])
            ->where('group_id', $group_id)
            ->update([
                'club_position' => 'Member',
                'manage' => ManageGroupHelper::MANAGE_LEVELS['Member Level'],
                'member_type' => 'Full'
            ]);
    }

    /**
     * Get election candidates
     *
     * @todo move to repository
     * @param int $role_id
     * @param int $election_id
     * @return void
     */
    public function getCandidate(int $role_id, int $election_id)
    {
        $candidates = DB::table('group_nominees')
            ->select('group_nominees.role_id AS election_office_id', 'roles.name',
                'group_nominees.nominee AS candidate')
            ->where('group_nominees.role_id', '=', $role_id)
            ->where('group_nominees.election_id', '=', $election_id)
            ->join('roles', 'roles.id', '=', 'group_nominees.role_id')
            ->join('users', 'users.id', '=', 'group_nominees.nominee')
            ->get();
    }


    /**
     * Nominate a candidate for a position
     *
     * @param Group $group
     * @param GroupElection $election
     * @return Application|Factory|View|RedirectResponse
     */
    public function nominate(Group $group, GroupElection $election)
    {
        try {
            $roles = explode(',', $election->roles);
            $positions = DB::table('roles')->select('id', 'name')->whereIn('id', $roles)->get();
            $users = DB::table('group_user')
                ->select('group_user.id AS gid','users.id AS uid', 'users.user_type', 'users.name AS uname',
                    'group_user.member_type', 'group_user.approved_at', 'users.first_name', 'users.last_name')
                ->where('group_user.group_id', $group->id)
                ->whereIn('group_user.member_type', ManageGroupHelper::NOMINEE_TYPES)
                ->join('users', 'users.id', '=', 'group_user.user_id')
                ->get();
            return view('groups.group.voting.nominate', [
                'users' => $users,
                'group' => $group,
                'election' => $election,
                'positions' => $positions,
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not access nominations page');
        }
    }


    /**
     * Create announcement for upcoming election
     *
     * @param Group $group
     * @param GroupElection $election
     * @return void
     * @todo move to repository
     */
    public function electionAnnouncement(Group $group, GroupElection $election): void
    {

    }

    /**
     * Create group message for upcoming election
     *
     * @param Group $group
     * @param GroupElection $election
     * @return void
     * @todo move to repository
     */
    public function electionMessage(Group $group, GroupElection $election)
    {

    }

    /**
     * Create name for an election
     *
     * @param null $type
     * @return string
     * @todo move to repository
     */
    public function generateElectionName($type = null): string
    {
        return date('Y').' Board Election';
    }
}
