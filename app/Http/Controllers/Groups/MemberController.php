<?php

namespace App\Http\Controllers\Groups;

use App\Http\Controllers\Controller;
use App\Http\Traits\GroupTrait;
use App\Models\Groups\Group;

class MemberController extends Controller
{
    use GroupTrait;


    public function index(Group $group)
    {
        return view('groups.group.members.index', [
            'group' => $group,
        ]);
    }
}
