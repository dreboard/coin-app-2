<?php
/**
 * ManageGroupController | actions for views/groups/group/manage
 *
 * @package Groups
 * @subpackage Manage
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Groups;

use App\Http\Controllers\Controller;
use App\Http\Traits\GroupTrait;
use App\Models\Announcement;
use App\Models\Groups\Event;
use App\Models\Groups\Group;
use App\Models\Groups\Settings;
use App\Models\Post;
use App\Repositories\Group\GroupManageRepository;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/**
 *
 */
class ManageGroupController extends Controller
{

    use GroupTrait;


    public function __construct(protected GroupManageRepository $groupManageRepository )
    {

    }



    /**
     * View group page
     *
     * @param Group $group
     * @return Application|Factory|RedirectResponse|View
     */
    public function view(Group $group)
    {
        try {

  /*          if (!auth()->user()->isClubUser($group->id && auth()->user()->is_admin)) {
                return view('groups.public', [
                    'group' => $group,
                ]);
            }*/
            if (!auth()->user()->isClubUser($group->id)) {
                return view('groups.public', [
                    'group' => $group,
                ]);
            }
            $user_manage_level = auth()->user()->userManageLevel($group->id);

            $events = Event::where('group_id', $group->id)
                ->whereMonth('start_at', date('m'))->get();

            $posts = Post::where('group_id', $group->id)
                ->orderBy('created_at', 'desc')->limit(4)->get();

            $announcement = Announcement::with('user')
                ->latest('created_at')
                ->where('group_id', $group->id)->first();

            $election = DB::table('group_elections')
                ->select('group_elections.id','group_elections.name',
                    'group_elections.election_type', 'group_elections.roles', 'group_elections.start_at', 'group_elections.expired_at')
                ->where('group_elections.group_id', $group->id)
                ->whereDate('group_elections.expired_at','>=', date('Y-m-d'))
                ->get();
            //dd($election);
            return view('groups.group.view', [
                'group' => $group,
                'announcement' => $announcement,
                'user_manage_level' => $user_manage_level,
                'election' => $election[0] ?? [],
                'election_end' => date('2022-11-08'),
                'events' => $events,
                'posts' => $posts
            ]);

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->route('group.all');
        }
    }


    /**
     * Show manage group page
     *
     * @param Group $group
     * @return Application|Factory|RedirectResponse|View
     */
    public function manage(Group $group)
    {
        try {
            if (!$this->isGroupUser($group->id)) {
                return redirect()->route('group.public', ['group' => $group->id]);
            }
            $user_manage_level = auth()->user()->userManageLevel($group->id);
            return view('groups.group.manage.index', [
                'group' => $group,
                'user_manage_level' => $user_manage_level
            ]);

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->route('group.view', ['group' => $group->id]);
        }
    }

    /**
     * Show group intro page
     *
     * @param Group $group
     * @return Application|Factory|RedirectResponse|View
     */
    public function intro(Group $group)
    {
        try {
            if (!$this->isGroupUser($group->id)) {
                return redirect()->route('group.public', ['group' => $group->id]);
            }
            $user_manage_level = auth()->user()->userManageLevel($group->id);
            return view('groups.group.intro', [
                'group' => $group,
                'user_manage_level' => $user_manage_level
            ]);

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->route('group.view', ['group' => $group->id]);
        }
    }

    /**
     * View manage levels page
     *
     * @param Group $group
     * @return Application|Factory|RedirectResponse|View
     */
    public function manageLevels(Group $group)
    {
        try {
            if (!$this->isGroupUser($group->id)) {
                return redirect()->route('group.public', ['group' => $group->id]);
            }
            $settings = Settings::where('group_id', $group->id)->first();
            $user_manage_level = auth()->user()->userManageLevel($group->id);
            return view('groups.group.manage.levels', [
                'group' => $group,
                'user_manage_level' => $user_manage_level,
                'settings' => $settings
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->route('group.view', ['group' => $group->id]);
        }
    }

    /**
     * Creates a manage request
     *
     * @param Group $group
     * @return Application|Factory|RedirectResponse|View
     */
    public function manageRequest(Group $group)
    {
        try {
            if (!$this->isGroupUser($group->id)) {
                return redirect()->route('group.public', ['group' => $group->id]);
            }
            $user_manage_level = auth()->user()->userManageLevel($group->id);
            return view('groups.group.manage.request', [
                'group' => $group,
                'user_manage_level' => $user_manage_level
            ]);

            return redirect()->route('group.view', ['group' => $group->id]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->route('group.view', ['group' => $group->id]);
        }
    }


    /**
     * Edit group details
     *
     * @param Group $group
     * @return Application|Factory|RedirectResponse|View
     */
    public function edit(Group $group)
    {
        if (!$this->isGroupUser($group->id)) {
            return redirect()->route('group.public', ['group' => $group->id]);
        }
        if (auth()->user()->userManageLevel($group->id) > 2) {
            return view('groups.group.manage.edit', ['group' => $group]);
        }
        return redirect()->route('group.view', ['group' => $group->id]);
    }

    /**
     * Save club edit
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function saveEdit(Request $request)
    {
        try {
            $group = Group::findOrFail($request->input('group_id'));

            $group->name = $request->input('group_name');
            $group->group_type = $request->input('group_type');
            $group->description = $request->input('description', null);
            $group->short_description = $request->input('short_description', null);
            $group->ana = $request->input('ana', null);
            $group->url = $request->input('url', null);

            if(in_array(strtolower($group->group_type), ['club','association'])){


            }
            $group->first_name = $request->input('first_name', null);
            $group->last_name = $request->input('last_name', null);
            $group->phone = $request->input('phone', null);
            $group->email = $request->input('email', null);
            $group->address = $request->input('address', null);
            $group->postalcode = $request->input('postalcode', null);
            $group->city = $request->input('city', null);
            $group->state = $request->input('state', null);
            $group->place = $request->input('place', null);
            $group->meets = $request->input('meets', null);
            $group->formed = $request->input('formed', date('Y'));
            $group->specialty = $request->input('specialty', 'General Numismatics');
            $group->private = $request->input('private');
            $group->extra_info = $request->input('extra_info', null);
            if ($request->has('image_url')) {
                $group->image_url = $this->processImage($request, $group);
            }

            $group->save();
            $group->refresh();


            return redirect()->back()->with('success', 'Club Successfully Updated');

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Club Was Not Updated');
        }
    }


    /**
     * Show tranfer group user founder page
     *
     * @param Group $group
     * @return Application|Factory|View|RedirectResponse
     */
    public function groupTransferPage(Group $group)
    {
        if (!$this->isGroupUser($group->id)) {
            return redirect()->route('group.public', ['group' => $group->id]);
        }
        $members = DB::table('group_user')
            ->select('groups.id AS gid', 'group_user.id AS group_user_id','users.id AS uid', 'users.name',
                'users.first_name', 'users.last_name', 'group_user.club_position', 'group_user.member_type', 'group_user.approved_at AS gu_approved_at')
            ->where('group_user.group_id', $group->id)
            ->join('users', 'users.id', '=', 'group_user.user_id')
            ->join('groups', 'groups.id', '=', 'group_user.group_id')
            ->get();
        return view('groups.group.transfer_user', [
            'members' => $members,
            'group' => $group,
        ]);
    }


    /**
     * Transfer Group Ownership
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function groupTransferOwner(Request $request): RedirectResponse
    {
        $group = Group::with('users')->findOrFail($request->input('group_id'));
        $group->user_id = $request->input('user_id');
        $group->save();
        return redirect()->back()->with('status','Ownership Transferred');
    }


    /**
     * Toggle Ajax group settings
     *
     * @see $this->manageLevels()
     * @param Request $request
     * @return JsonResponse
     */
    public function saveSettings(Request $request): JsonResponse
    {
        try{
            $settings = Settings::findOrFail($request->input('setting_id'));
            $settings->{$request->input('manage')} = $request->input('setting');
            $settings->save();
            return response()->json([
                'result' => 'Success',
            ]);
        }catch (Exception $e){
            Log::error($e->getMessage());
            return response()->json([
                'result' => 'Error',
            ]);
        }
    }

    /**
     * Group delete form page
     *
     * @param Group $group
     * @return Application|Factory|View
     */
    public function deleteGroupForm(Group $group)
    {
        return view('groups.group.manage.delete', [
            'group' => $group,
        ]);
    }


    /**
     * Delete group and all resources
     *
     * @todo delete notifications
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function deleteGroup(Request $request): RedirectResponse
    {
        try{
            $group = Group::findOrFail($request->input('group_id'));
            if ($group->user_id !== auth()->user()->id) {
                return redirect()->back()->with('error', 'You can not delete this group');
            }
            $id = $group->id;
            Storage::deleteDirectory('groups/'.$group->id);

            DB::transaction(function () use ($request, $id, $group) {
                $group->delete();
                DB::delete('delete from `agendas` where `group_id` = '.$id);
                DB::delete('delete from `announcements` where `group_id` = '.$id);
                DB::delete('delete from `group_user` where `group_id` = '.$id);
                DB::delete('delete from `committees` where `group_id` = '.$id);
                DB::delete('delete from `events` where `group_id` = '.$id);
                DB::delete('delete from `committees` where `group_id` = '.$id);
                DB::delete('delete from `group_elections` where `group_id` = '.$id);
                DB::delete('delete from `group_analytics` where `group_id` = '.$id);
                DB::delete('delete from `group_request` where `group_id` = '.$id);
                DB::delete('delete from `group_role` where `group_id` = '.$id);
                DB::delete('delete from `issues` where `group_id` = '.$id);
                DB::delete('delete from `library` where `group_id` = '.$id);
                DB::delete('delete from `library_request` where `group_id` = '.$id);
                DB::delete('delete from `post_cards` where `group_id` = '.$id);
                DB::delete('delete from `posts` where `group_id` = '.$id);
                DB::delete('delete from `reports` where `group_id` = '.$id);

            });
            return redirect()->route('group.all')->with('status','Group Was Successfully Deleted');

        }catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Group Was Not Deleted');
        }

    }


}
