<?php
/**
 * GroupUsersController | actions for views/groups/group/users
 *
 * @package Groups
 * @subpackage Users
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Groups;

use App\Helpers\ManageGroupHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\GroupTrait;
use App\Models\Groups\Group;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class GroupUsersController extends Controller
{

    use GroupTrait;

    /**
     * Show all groups.
     *
     * @param $group_id
     * @return \Application|\Factory|\RedirectResponse|\View
     */
    public function viewGroupUsers($group_id): \Application|\Factory|\RedirectResponse|\View
    {
        if (!$this->isGroupUser($group_id)) {
            return redirect()->route('group.public', ['group' => $group_id]);
        }
        $user_manage_level = auth()->user()->userManageLevel($group_id);
        $group = Group::with('users')->findOrFail($group_id);

        $board_members = DB::table('group_role')
            ->select('group_role.id AS board_member_role_id', 'groups.id AS board_member_group_id', 'users.id AS board_member_user_id', 'users.name AS board_member_user_name',
                'users.first_name AS board_member_first_name', 'users.last_name AS board_member_last_name', 'roles.name AS board_member_position_name',
                'group_role.approved_at AS position_approved_at', 'group_role.expired_at AS position_expired_at')
            ->where('group_role.group_id', $group->id)
            ->whereIn('roles.id', ManageGroupHelper::GROUP_BOARD_POSITIONS)
            ->join('users', 'users.id', '=', 'group_role.user_id')
            ->join('roles', 'roles.id', '=', 'group_role.role_id')
            ->join('groups', 'groups.id', '=', 'group_role.group_id')
            ->orderBy('roles.id')
            ->get()->keyBy('board_member_role_id');
        //

        $founder = DB::table('group_user')
            ->select('groups.user_id AS founder', 'users.id AS user_id', 'users.name', 'group_user.club_position', 'group_user.member_type', 'group_user.approved_at',
                'users.name AS user_name', 'users.first_name AS user_first_name', 'users.last_name AS user_last_name')
            ->where('group_user.group_id', $group_id)
            ->join('users', 'users.id', '=', 'group_user.user_id')
            ->join('groups', 'groups.id', '=', 'group_user.group_id')
            ->first();

        $members = DB::table('group_user')
            ->select('group_user.id AS gid', 'users.id AS user_id', 'users.name', 'group_user.club_position', 'group_user.member_type', 'group_user.approved_at',
                'users.name AS user_name', 'users.first_name AS user_first_name', 'users.last_name AS user_last_name')
            ->where('group_user.group_id', $group_id)
            ->join('users', 'users.id', '=', 'group_user.user_id')
            ->get();
        //dd($members);
        return view('groups.group.users.all', [
            'members' => $members,
            'group' => $group,
            'board_members' => $board_members,
            'founder' => $founder,
            'user_manage_level' => $user_manage_level
        ]);
    }

    /**
     * Show all groups.
     *
     * @return \Application|\Factory|\RedirectResponse|\View
     */
    public function viewGroupUser(Group $group, $user_id): \Application|\Factory|\RedirectResponse|\View
    {
        if (!$this->isGroupUser($group->id)) {
            return redirect()->route('group.public', ['group' => $group->id]);
        }
        //$group = Group::with('usersall', 'roles')->findOrFail($group_id);
        //$user = User::with('groups')->findOrFail($user_id);
        //dd($user->toArray());
        $user = DB::table('group_user')
            ->select('group_user.id AS group_user_id', 'groups.id AS the_group_id', 'users.id AS the_users_id',
                'users.user_type', 'users.name AS user_name', 'users.first_name AS user_first_name', 'users.last_name AS user_last_name',
                'group_user.club_position', 'group_user.manage', 'group_user.member_type', 'group_user.approved_at AS the_group_start_date')->distinct()
            ->where('group_user.group_id', $group->id)
            ->where('users.id', $user_id)
            ->join('groups', 'groups.id', '=', 'group_user.group_id')
            ->join('users', 'users.id', '=', 'group_user.user_id')
            ->get();
        $roles = DB::table('group_role')
            ->select('group_role.id AS gid', 'roles.name AS role_name', 'roles.id AS role_id',
                'group_role.approved_at', 'group_role.expired_at')
            ->where('group_role.group_id', $group->id)
            ->where('group_role.user_id', $user_id)
            ->join('users', 'users.id', '=', 'group_role.user_id')
            ->join('roles', 'roles.id', '=', 'group_role.role_id')
            ->get();
        $positions = DB::table('roles')->select('id', 'name')->get();
        //dd($user,$roles);
        return view('groups.group.users.user', [
            'user' => $user,
            'group' => $group,
            'roles' => $roles,
            'positions' => $positions,
        ]);
    }


    /**
     * Remove user OR yourself (remove_type) from group
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function removeUser(Request $request): RedirectResponse
    {
        $user = User::findOrFail($request->input('user_id'));
        $user->groups()->detach($request->input('group_id'));

        if ($request->input('remove_type') == 'group') {
            return redirect()->route('group.view', ['group' => $request->input('group_id')])
                ->with('success', 'User Removed Successfully');
        }
        return redirect()->route('group.all');
    }


    /**
     * Remove user OR yourself (remove_type) from group
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function removeMyself(Request $request): JsonResponse
    {
        try{
            $user = auth()->user()->id;
            //$user->groups()->detach($request->input('group_id'));
            return response()->json([
                'result' => 'Success',
            ]);
        }catch (Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'result' => 'Error',
            ]);
        }
    }

    /**
     * Show all groups.
     *
     * @param $group_id
     * @param $type
     * @return \Application|\Factory|\RedirectResponse|\View
     */
    public function viewGroupUsersByMemberType($group_id, $type): \Application|\Factory|\RedirectResponse|\View
    {
        if (!$this->isGroupUser($group_id)) {
            return redirect()->route('group.public', ['group' => $group_id]);
        }

        $group = Group::with('users')->findOrFail($group_id);

        $members = DB::table('group_user')
            ->select('group_user.id AS gid', 'users.id AS user_id', 'users.name', 'group_user.club_position', 'group_user.member_type', 'group_user.approved_at',
                'users.name AS user_name', 'users.first_name AS user_first_name', 'users.last_name AS user_last_name')
            ->where('group_user.group_id', $group_id)
            ->where('group_user.member_type', $type)
            ->join('users', 'users.id', '=', 'group_user.user_id')
            ->get();
        return view('groups.group.users.by_type', [
            'members' => $members,
            'group' => $group,
        ]);
    }


    /**
     * Get group board members
     *
     * @param Group $group
     * @return \Illuminate\Support\Collection
     * @todo move to GroupManageRepository
     */
    public function getBoardMembers(Group $group): \Illuminate\Support\Collection
    {
        return DB::table('group_role')
            ->select('group_role.id AS bmgrid', 'groups.id AS bmgid', 'users.id AS bmuid', 'users.name AS bmuname', 'roles.name AS bmrname', 'group_role.approved_at AS bmapproved_at', 'group_role.expired_at AS bmexpired_at')
            ->where('group_role.group_id', $group->id)
            ->whereIn('roles.id', [1, 2, 3, 4])
            ->join('users', 'users.id', '=', 'group_role.user_id')
            ->join('roles', 'roles.id', '=', 'group_role.role_id')
            ->join('groups', 'groups.id', '=', 'group_role.group_id')
            ->get();
    }


    /**
     * Show all groups.
     *
     * @param $group_id
     * @param $role_id
     * @return \Application|\Factory|\RedirectResponse|\View
     */
    public function viewGroupUsersByRole($group_id, $role_id): \Application|\Factory|\RedirectResponse|\View
    {
        if (!$this->isGroupUser($group_id)) {
            return redirect()->route('group.public', ['group' => $group_id]);
        }

        $group = Group::with('users')->findOrFail($group_id);

        $members = DB::table('group_role')
            ->select('group_role.id AS board_member_role_id', 'groups.id AS board_member_group_id', 'users.id AS board_member_user_id', 'users.name AS board_member_user_name',
                'users.first_name AS board_member_first_name', 'users.last_name AS board_member_last_name', 'roles.name AS board_member_position_name',
                'group_role.approved_at AS position_approved_at', 'group_role.expired_at AS position_expired_at')
            ->where('group_role.group_id', $group->id)
            ->where('group_role.role_id', $role_id)
            ->whereIn('roles.id', ManageGroupHelper::GROUP_BOARD_POSITIONS)
            ->join('users', 'users.id', '=', 'group_role.user_id')
            ->join('roles', 'roles.id', '=', 'group_role.role_id')
            ->join('groups', 'groups.id', '=', 'group_role.group_id')
            ->get();
        return view('groups.group.users.all', [
            'members' => $members,
            'group' => $group,
        ]);
    }


    /**
     * Show all groups.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function saveUserPosition(Request $request): RedirectResponse
    {
        $request->validate([
            'group_id' => 'required|integer',
            'user_id' => 'required|integer',
            'role_id' => [
                'required|integer',
                Rule::unique("group_role")->where(
                    function ($query) use ($request) {
                        return $query->where(
                            [
                                ["role_id", "=", $request->input('role_id')],
                                ["user_id", "=", $request->input('user_id')],
                                ["group_id", "=", $request->input('group_id')],
                            ]
                        );
                    }

                )]]);

        DB::table('group_role')->insert([
            'group_id' => $request->input('group_id'),
            'user_id' => $request->input('user_id'),
            'role_id' => $request->input('role_id'),
            'expired_at' => Carbon::now()->addYear(1),
        ]);
        return redirect()->back()->with('status', 'Member Assigned');
    }

    /**
     * Update user a role
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function updateUserPosition(Request $request): RedirectResponse
    {
        try {
            $request->validate([
                'group_id' => 'required|integer',
                'user_id' => 'required|integer',
                'role_id' => [
                    Rule::unique("group_role")->where(
                        function ($query) use ($request) {
                            return $query->where(
                                [
                                    ["role_id", "=", $request->input('role_id')],
                                    ["user_id", "=", $request->input('user_id')],
                                    ["group_id", "=", $request->input('group_id')],
                                ]
                            );
                        }

                    )]]);

            $role = DB::table('roles')->select('name')->where('id', $request->input('role_id'))->first();

            $exist = DB::table('group_role')
                ->where('user_id', $request->input('user_id'))
                ->where('user_id', $request->input('user_id'))
                ->where('role_id', $request->input('role_id'))
                ->exists();

            if ($exist) {
                return redirect()->back()->with('error', 'Position Already Assigned');
            }

            DB::table('group_role')->insert([
                'user_id' => $request->input('user_id'),
                'group_id' => $request->input('group_id'),
                'role_id' => $request->input('role_id'),
                'approved_at' => now(),
                'expired_at' => Carbon::now()->addYear(1),
            ]);

            // @todo Move to another method By role_id
            if (in_array($request->input('role_id'), ManageGroupHelper::GROUP_BOARD_POSITIONS)) {
                DB::table('group_user')
                    ->where('user_id', $request->input('user_id'))
                    ->where('group_id', $request->input('group_id'))
                    ->update([
                        'club_position' => $role->name,
                        'manage' => ManageGroupHelper::MANAGE_LEVELS['Executive Level'],
                        'member_type' => 'Executive'
                    ]);
            }
            /*
            DB::table('group_role')
                ->where('user_id', $request->input('uid'))
                ->where('group_id', $request->input('gid'))
                ->update(['role_id' => $request->input('role_id')]);

            */

            return redirect()->back()->with('status', 'Member Assigned');
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Member Not Assigned');
        }

    }


    /**
     * Show all positions of the group
     *
     * @param Group $group
     * @return Factory|View|Application
     */
    public function groupPositionsByUser(Group $group): Factory|View|Application
    {
        $roles = $this->getAssignedRolesMembers($group);

        $positions = DB::table('roles')->select('id', 'name')->get();
        $members = DB::table('group_user')
            ->select('groups.id AS gid', 'group_user.id AS group_user_id', 'users.id AS uid', 'users.name', 'group_role.role_id AS role_name',
                'users.first_name', 'users.last_name', 'group_user.club_position', 'group_user.member_type', 'group_user.approved_at AS gu_approved_at')
            ->where('group_user.group_id', $group->id)
            ->leftJoin('group_role', 'group_role.user_id', '=', 'group_user.user_id')
            ->join('users', 'users.id', '=', 'group_user.user_id')
            ->join('groups', 'groups.id', '=', 'group_user.group_id')
            ->get();
        return view('groups.group.users.positions-user', [
            'members' => $members,
            'positions' => $positions,
            'group' => $group,
            'roles' => $roles
        ]);
    }


    /**
     * Get users with assigned roles
     *
     * @param Group $group
     * @return array
     * @todo move to GroupManageRepository
     */
    public function getAssignedRolesMembers(Group $group): array
    {
        $all_roles = DB::table('roles')->select('id', 'name')->get();
        $roles_array = [];
        //dd($all_roles);
        foreach ($all_roles as $role) {
            $roles_array[$role->name]['role_id'] = $role->id;
            $roles_array[$role->name]['user_info'] = $this->getUserByRole($role->id, $group);
            //$roles[$position->id] = $this->getUserByRole($position->id,  $group) ?? [];
        }
        return $roles_array;
    }


    /**
     * Get user by a role
     *
     * @param int $role_id
     * @param Group $group
     * @return array
     * @todo move to GroupManageRepository
     */
    public function getUserByRole(int $role_id, Group $group): array
    {
        return DB::table('group_role')
            ->select('group_role.id AS grid', 'groups.id AS gid', 'users.id AS uid', 'users.name AS uname', 'roles.name AS rname', 'group_role.approved_at AS  role_approved_at', 'group_role.expired_at AS role_expired_at')
            ->where('group_role.group_id', $group->id)
            ->where('roles.id', $role_id)
            ->rightJoin('users', 'users.id', '=', 'group_role.user_id')
            ->join('roles', 'roles.id', '=', 'group_role.role_id')
            ->join('groups', 'groups.id', '=', 'group_role.group_id')
            ->get()->toArray();
    }


    /**
     * Show all groups.
     *
     * @param Group $group
     * @return Factory|View|Application
     */
    public function groupViewPositions(Group $group): Factory|View|Application
    {
        $roles = $this->getAssignedRolesMembers($group);
        //dd($roles);
        /*$board_members = DB::table('group_role')
            ->select('group_role.id AS bmgrid','groups.id AS bmgid','users.id AS bmuid', 'users.name AS bmuname', 'roles.name AS bmrname', 'group_role.approved_at AS bmapproved_at', 'group_role.expired_at AS bmexpired_at')
            ->where('group_role.group_id', $group->id)
            ->whereIn('roles.id', [2,3,4,5])
            ->join('users', 'users.id', '=', 'group_role.user_id')
            ->join('roles', 'roles.id', '=', 'group_role.role_id')
            ->join('groups', 'groups.id', '=', 'group_user.group_id')
            ->get();*/
        //dd($board_members);

        $positions = DB::table('roles')->select('id', 'name')->get();
        $members = DB::table('group_user')
            ->select('groups.id AS gid', 'users.id AS uid', 'users.name AS uname', 'users.first_name', 'users.last_name',
                'group_user.member_type', 'group_user.approved_at AS gu_approved_at')
            ->where('group_user.group_id', $group->id)
            ->join('users', 'users.id', '=', 'group_user.user_id')
            ->join('groups', 'groups.id', '=', 'group_user.group_id')
            ->get();
        //dd($members);
        return view('groups.group.users.positions', [
            'members' => $members,
            'positions' => $positions,
            'group' => $group,
            'board_members' => [],// $board_members[],
            'roles' => $roles
        ]);
    }

    public function assignPositions(Group $group)
    {
        // `group_role` (`user_id`, `group_id`, `role_id`, `approved_at`, `expired_at`)
        $board_members = DB::table('group_role')
            ->select('group_role.id AS gid', 'users.id AS uid', 'users.name AS uname', 'roles.name AS rname', 'group_role.approved_at', 'group_role.expired_at')
            ->where('group_role.group_id', $group->id)
            ->whereIn('roles.id', [2, 3, 4, 5])
            ->join('users', 'users.id', '=', 'group_role.user_id')
            ->join('roles', 'roles.id', '=', 'group_role.role_id')
            ->get();
        //dd($board_members);

        $positions = DB::table('roles')->select('id', 'name')->get();
        $members = DB::table('group_user')
            ->select('group_user.id AS gid', 'users.id AS user_id', 'users.name', 'group_user.member_type', 'group_user.approved_at')
            ->where('group_user.group_id', $group->id)
            ->join('users', 'users.id', '=', 'group_user.user_id')
            ->get();
        //dd($positions);
        return view('groups.group.users.positions', [
            'members' => $members,
            'positions' => $positions,
            'group' => $group,
            'board_members' => $board_members,
        ]);
    }

    public function removeUserPosition(Request $request): RedirectResponse
    {
        DB::table('group_role')->where([
            'user_id' => $request->input('user_id'),
            'group_id' => $request->input('group_id'),
            'role_id' => $request->input('role_id')
        ])->delete();
        if (in_array($request->input('role_id'), ManageGroupHelper::GROUP_BOARD_POSITIONS)) {
            DB::table('group_user')
                ->where('user_id', $request->input('user_id'))
                ->where('group_id', $request->input('group_id'))
                ->update([
                    'club_position' => 'Member',
                    'manage' => 0,
                    'member_type' => 'Full'
                ]);
        }

        return redirect()->back()->with('status', 'Member Roles Removed');
    }

    public function removeAllUserRoles(Request $request): RedirectResponse
    {

        DB::table('group_role')->where([
            'user_id' => $request->input('user_id'),
            'group_id' => $request->input('group_id'),
        ])->delete();

        DB::table('group_user')
            ->where('user_id', $request->input('uid'))
            ->where('group_id', $request->input('gid'))
            ->update([
                'club_position' => 'Member',
                'manage' => 0,
                'member_type' => 'Full'
            ]);

        return redirect()->back()->with('status', 'Member Roles Removed');
    }


    public function getCurrentUserRoles(Request $request): JsonResponse
    {
        $user = User::find($request->input('user_id'));
        $roles = DB::table('group_role')
            ->select('group_role.role_id', 'roles.name')
            ->where('group_role.user_id', $request->input('user_id'))
            ->where('group_role.group_id', $request->input('group_id'))
            ->join('roles', 'roles.id', '=', 'group_role.role_id')
            ->get();
        if (empty($roles)) {
            return response()->json(['role_id' => 0, 'name' => 'No Position']);
        }
        //Log::info($roles);
        return response()->json($roles);
    }


    /**
     * Get users by  group_user member_type
     *
     * @param Group $group
     * @param string $member_type
     * @return \Application|\Factory|\RedirectResponse|\View
     */
    public function getMemberType(Group $group, string $member_type): \Application|\Factory|\RedirectResponse|\View
    {
        if (!$this->isGroupUser($group->id)) {
            return redirect()->route('group.public', ['group' => $group->id]);
        }
        $requests = DB::table('group_user')
            ->select('group_user.id AS gid', 'users.id AS uid', 'users.name', 'group_request.created_at')
            ->where('group_user.member_type', $member_type)
            ->where('group_user.group_id', $group->id)
            ->join('users', 'users.id', '=', 'group_user.user_id')
            ->get();
        return view('groups.group.users.requests', [
            'requests' => $requests,
            'group' => $group
        ]);
    }

    /**
     * Associate Members to Full Member
     *
     * @param Group $group
     * @return \Application|\Factory|\RedirectResponse|\View
     */
    public function upgradeAssociateMembers(Group $group): \Application|\Factory|\RedirectResponse|\View
    {
        if (!$this->isGroupUser($group->id)) {
            return redirect()->route('group.public', ['group' => $group->id]);
        }
        $requests = DB::table('group_user')
            ->select('group_user.id AS gid', 'users.id AS uid', 'users.name', 'group_request.created_at')
            ->where('group_user.member_type', 'associate')
            ->where('group_user.group_id', $group->id)
            ->join('users', 'users.id', '=', 'group_user.user_id')
            ->get();
        return view('groups.group.users.requests', [
            'requests' => $requests,
            'group' => $group
        ]);
    }


    public function changeUserManageLevel(Request $request): JsonResponse
    {
        try{
            DB::table('group_user')
                ->where('user_id', $request->input('user_id'))
                ->where('group_id', $request->input('group_id'))
                ->update([
                    'club_position' => 'Member',
                    'manage' => $request->input('manage'),
                    'member_type' => 'Full'
                ]);
            return response()->json(['result' => 'Success']);
        }catch (Exception $e){
            Log::error($e->getMessage());
            return response()->json(['result' => 'Error']);
        }
    }

}
