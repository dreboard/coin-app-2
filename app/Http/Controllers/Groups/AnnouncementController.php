<?php

namespace App\Http\Controllers\Groups;

use App\Http\Controllers\Controller;
use App\Models\Announcement;
use App\Models\Groups\Group;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class AnnouncementController extends Controller
{


    /**
     * Creates a new Announcement.
     *
     * @param Group $group
     * @return Application|Factory|View
     */
    public function create(Group $group)
    {
        return view('groups.group.announcements.create', ['group' => $group]);
    }

    /**
     * Creates a new Announcement.
     *
     * @return Application|Factory|View
     */
    public function view(Group $group, Announcement $announcement)
    {
        return view('groups.group.announcements.view', [
            'announcement' => $announcement,
            'group' => $group,
        ]);
    }


    /**
     * Announcement edit form
     *
     * @return Application|Factory|View
     */
    public function edit(Group $group, Announcement $announcement)
    {
        return view('groups.group.announcements.edit', [
            'announcement' => $announcement,
            'group' => $group,
        ]);
    }


    /**
     * Edit an Announcement.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(Request $request): RedirectResponse
    {
        $request->validate([
            'user_id' => 'required|integer',
            'group_id' => 'required|integer',
            'announcement_id' => 'required|integer',
            'message' => 'required|string',
        ]);

        $announcement = Announcement::findOrFail($request->input('announcement_id'));
        $announcement->message = $request->input('message');
        $announcement->save();

        return redirect()->back()->with('status','Announcement Updated');

    }

    /**
     * Show all Announcements.
     *
     * @param Group $group
     * @return \Application|\Factory|\View
     */
    public function all(Group $group): \Application|\Factory|\View
    {
        $announcements = Announcement::with('user')->where('group_id', $group->id)->orderByDesc('created_at')->get();
        return view('groups.group.announcements.all', [
            'announcements' => $announcements,
            'group' => $group,
        ]);
    }

    /**
     * Saves a new Announcement.
     *
     * @param Request $request
     * @param Group $group
     * @return Application|Factory|View
     */
    public function save(Request $request, Group $group)
    {
        $request->validate([
            'user_id' => 'required|integer',
            'group_id' => 'required|integer',
            'message' => 'required|string',
        ]);

        Announcement::create([
            'user_id' => $request->input('user_id'),
            'group_id' => $request->input('group_id'),
            'message' => $request->input('message'),
        ]);

        return view('groups.group.view', ['group' => $group])->with('status', 'Message Sent');
    }

    /**
     * Creates a new Announcement.
     *
     * @return Application|Factory|View
     */
    public function destroy()
    {
        return view('groups.create');
    }

    /**
     * Creates a new Announcement.
     *
     * @return Application|Factory|View
     */
    public function destroyAll()
    {
        return view('groups.create');
    }


}
