<?php

namespace App\Http\Controllers\News;

use Alaouy\Youtube\Facades\Youtube;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Log;


class VideoController extends Controller
{


    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('news.index');
    }


    /**
     *
     * @see https://commentpicker.com/youtube-channel-id.php
     * @see https://stackoverflow.com/questions/14366648/how-can-i-get-a-channel-id-from-youtube
     * @see https://thedebuggers.com/retrieve-playlist-videos-youtube-api/
     * @see https://www.codexworld.com/get-videos-from-youtube-channel-data-api-v3-php/
     *
     * @return Application|Factory|View
     */
    public function loadChannel(string $slug = '')
    {
        try{
            return view('news.channel',[
                'data' => [],
            ]);
            $channel = Youtube::listChannelVideos('UCQ-T-XzN7mJQoYdW5zuGBSA');
            dd($channel); die();



        } catch (Exception $e){
            Log::error($e->getMessage());
            return view('news.index')->with('error', 'Channel Not Reachable!');
        }
    }


    /**
     * @return Application|Factory|View
     */
    public function loadPlaylist(string $slug = '')
    {
        try{
            //$videoList = Youtube::getPopularVideos('us');
            //dd($videoList);
            $channel = Youtube::getPlaylistById('PLu3JNWOxw12je1HNWUz_4hZ6HWwkcb_hH');
            dd($channel); die();

            return view('news.channel',[
                'data' => [],
            ]);

        } catch (Exception $e){
            Log::error($e->getMessage());
            return view('news.index')->with('error', 'Channel Not Reachable!');
        }
    }


    /**
     * @return Application|Factory|View
     */
    public function loadVideo(string $slug = '')
    {
        try{
            //$videoList = Youtube::parseVidFromURL($url);
            //dd($videoList);
            $channel = Youtube::getVideoInfo('PLu3JNWOxw12je1HNWUz_4hZ6HWwkcb_hH');
            dd($channel); die();

            return view('news.channel',[
                'data' => [],
            ]);

        } catch (Exception $e){
            Log::error($e->getMessage());
            return view('news.index')->with('error', 'Channel Not Reachable!');
        }
    }


    /**
     * @return Application|Factory|View
     */
    public function loadSearchVideos(string $slug = '')
    {
        try{
            $channel = Youtube::searchVideos('coin collecting');
            dd($channel); die();

            return view('news.channel',[
                'data' => [],
            ]);

        } catch (Exception $e){
            Log::error($e->getMessage());
            return view('news.index')->with('error', 'Channel Not Reachable!');
        }
    }


}
