<?php

namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Log;
use Vedmant\FeedReader\Facades\FeedReader;

/**
 * @see https://github.com/vedmant/laravel-feed-reader
 * @see http://simplepie.org/api/class-SimplePie_Item.html
 */
class RSSController extends Controller
{
    /**
     * @var array[]
     */
    private array $feeds = [
        'us_mint' => [
            'link' => 'https://www.usmint.gov/feed/usmint/?post_type=press-releases',
            'view' => 'news.mint'
        ],
        'ana' => [
            'link' => 'https://blog.money.org/coin-collecting/rss.xml',
            'view' => 'news.ana'
        ],
        'coin_news' => [
            'link' => 'http://feeds.feedburner.com/CoinNewsnet',
            'view' => 'news.coin_news'
        ],
        'coin_week' => [
            'link' => 'https://coinweek.com/feed/',
            'view' => 'news.coin_week'
        ],
        'coin_update' => [
            'link' => 'http://news.coinupdate.com/feed/',
            'view' => 'news.coin_update'
        ],
        'littleton' => [
            'link' => 'https://blog.littletoncoin.com/feed/',
            'view' => 'news.coin_update'
        ],
        'davidlawrence' => [
            'link' => 'https://blog.davidlawrence.com/rss/',
            'view' => 'news.coin_update'
        ],
        'coinworld' => [
            'link' => 'https://www.feedspot.com/infiniterss.php?_src=followbtn&followfeedid=5343046&q=site:',
            'view' => 'news.coin_update'
        ],
        'coin_show' => [
            'link' => 'https://www.spreaker.com/show/1080503/episodes/feed',
            'view' => 'news.coin_show'
        ],
        'xxxxx' => [
            'link' => 'https://www.numismaticnews.net/feed',
            'view' => 'news.coin_update'
        ]
    ];

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('news.index');
    }


    /**
     * @return Application|Factory|View
     */
    public function loadFeed(string $slug)
    {
        try{
            $mintfeed = FeedReader::read($this->feeds[$slug]['link']);
            $result = [
                'title' => $mintfeed->get_title(),
                'description' => $mintfeed->get_description(),
                'permalink' => $mintfeed->get_permalink(),
            ];
            $i =[];

            foreach ($mintfeed->get_items(0, $mintfeed->get_item_quantity()) as $item) {
                $i['title'] = $item->get_title();
                $i['description'] = $item->get_description();
                $i['id'] = $item->get_id();
                $i['content'] = $item->get_content();
                $i['permalink'] = $item->get_permalink();
                $i['get_date'] = $item->get_date();
                $result['items'][] = $i;
            }
            $result['items'][] = $i;
            //dd($result);
            return view('news.feeds',[
                'data' => $result,
            ]);
        } catch (Exception $e){
            Log::error($e->getMessage());
            return view('news.index')->with('error', 'Feed Not Reachable!');
        }
    }

    /**
     * @return Application|Factory|View
     */
    public function getMintFeed()
    {
        $mintfeed = FeedReader::read($this->feeds['us_mint']);
        $result = [
            'title' => $mintfeed->get_title(),
            'description' => $mintfeed->get_description(),
            'permalink' => $mintfeed->get_permalink(),
        ];
        $i =[];

        foreach ($mintfeed->get_items(0, $mintfeed->get_item_quantity()) as $item) {
            $i['title'] = $item->get_title();
            $i['description'] = $item->get_description();
            $i['id'] = $item->get_id();
            $i['content'] = $item->get_content();
            $i['permalink'] = $item->get_permalink();
            $i['get_date'] = $item->get_date();
            $result['items'][] = $i;
        }
        $result['items'][] = $i;
        return view('news.mint',[
            'data' => $result,
        ]);
    }

    /**
     * @return Application|Factory|View
     */
    public function getANA()
    {
        $mintfeed = FeedReader::read($this->feeds['ana']);
        $result = [
            'title' => $mintfeed->get_title(),
            'description' => $mintfeed->get_description(),
            'permalink' => $mintfeed->get_permalink(),
        ];
        $i =[];

        foreach ($mintfeed->get_items(0, $mintfeed->get_item_quantity()) as $item) {
            $i['title'] = $item->get_title();
            $i['description'] = $item->get_description();
            $i['id'] = $item->get_id();
            $i['content'] = $item->get_content();
            $i['permalink'] = $item->get_permalink();
            $i['get_date'] = $item->get_date();
            $result['items'][] = $i;
        }
        $result['items'][] = $i;
        return view('news.ana',[
            'data' => $result,
        ]);
    }

    /**
     * @return Application|Factory|View
     */
    public function getCoinNewsnetFeed()
    {
        $mintfeed = FeedReader::read($this->feeds['coin_news']);
        $result = [
            'title' => $mintfeed->get_title(),
            'description' => $mintfeed->get_description(),
            'permalink' => $mintfeed->get_permalink(),
        ];
        $i =[];

        foreach ($mintfeed->get_items(0, $mintfeed->get_item_quantity()) as $item) {
            $i['title'] = $item->get_title();
            $i['description'] = $item->get_description();
            $i['id'] = $item->get_id();
            $i['content'] = $item->get_content();
            $i['permalink'] = $item->get_permalink();
            $i['get_date'] = $item->get_date();
            $result['items'][] = $i;
        }
        $result['items'][] = $i;
        return view('news.ana',[
            'data' => $result,
        ]);
    }
}
