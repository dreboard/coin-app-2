<?php
/**
 * MessageController | actions for views/user/message
 *
 * @package Users
 * @subpackage Messaging
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use App\Http\Traits\UserTrait;
use App\Models\Forum;
use App\Models\User;
use App\Notifications\ContactUserNotification;
use App\Notifications\GeneralUserNotification;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class MessageController extends Controller
{

    use UserTrait;

    /**
     * View user messages page
     *
     * @return Application|Factory|View
     */
    public function all(): View|Factory|Application
    {
        $messages = auth()->user()->notifications;
        return view('messages.index', ['messages' => $messages]);
    }


    /**
     * View user messages page
     *
     * @param Request $request
     * @return \RedirectResponse
     */
    public function markAll(Request $request): \RedirectResponse
    {
        try{
            $user = User::findOrFail($request->input('user_id'));
            $user->unreadNotifications()->update(['read_at' => now()]);

            return redirect()->action(
                [MessageController::class, 'all']
            )->with('status', 'System Notifications Updated');



        } catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','System Notifications Not Updated');
        }
    }

    /**
     * View user message
     *
     * @param $id
     * @return \Application|\Factory|\RedirectResponse|\View
     */
    public function view($id): \Application|\Factory|\RedirectResponse|\View
    {
        try{
            $notification = auth()->user()
                ->notifications
                ->where('id', $id)
                ->firstorFail();
            $notification->markAsRead();
            $from = User::findOrFail($notification->data['from_id'],['id', 'name']);

            return view('messages.view', [
                'notification' => $notification,
                'from' => $from
            ]);
        } catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','System Notification Not found');
        }
    }

    public function create()
    {
        $users = $this->myFollowers();

        return view('messages.create', [
            'users' => $users,
        ]);
    }


    public function save(Request $request): RedirectResponse
    {
        $request->validate([
            'subject' => 'required|string',
            'body' => 'required|string',
            'recipients' => 'required|array',
        ]);
        $recipients = count($request->input('recipients'));
        // covert to discussion
        if($recipients > 1){
            $forum = new Forum();
            $forum->user_id = auth()->user()->id;
            $forum->title = $request->input('subject');
            $forum->slug = Str::of($request->input('subject'))->slug('_');
            $forum->body = $request->input('body');
            $forum->private = 1;
            $forum->type = 'discussion';
            $forum->notify = 1;
            $forum->save();
        }

        if ($request->has('image_url')) {
            $folder = '/forum/' . $forum->id;
            $forum->image_url = $this->processPostImage($request, $forum, $folder);
            $forum->save();
            $forum->refresh();
        }
        $data = [
            'from_id' => auth()->user()->id,
            'from' => auth()->user()->name,
            'subject' => $request->input('subject'),
            'body' => $request->input('body'),
            'forum_id' => $forum->id,
            'image_url' => $forum->image_url ?? null,
        ];
        foreach($request->input('recipients') as $recipient){
            $user = User::find($recipient);
            Notification::send($user, new ContactUserNotification($data));
            DB::table('participants')->insert([
                ['forum_id' => $forum->id, 'user_id' => $user->id]
            ]);
        }
        Notification::send(auth()->user(), new ContactUserNotification($data));
        DB::table('participants')->insert([
            ['forum_id' => $forum->id, 'user_id' => auth()->user()->id]
        ]);
        return redirect()->back()->with('success','Message Sent');
    }


    public function reply(Request $request): RedirectResponse
    {
        $request->validate([
            'notification_id' => 'required|string',
            'user_id' => 'required|integer',
            'subject' => 'required|string',
            'body' => 'required|string',
        ]);

        $data = [
            'from_id' => auth()->user()->id,
            'from' => auth()->user()->name,
            'subject' => $request->subject,
            'body' => $request->body,
        ];
        (User::find($request->input('user_id')))->notify(new GeneralUserNotification($data));
        return redirect()->back()->with('status','System Notification Sent');
        //return view('users.messages.system');
    }

    /**
     * Delete a user messages
     *
     * @param Request $request
     * @return Application|Factory|View
     */
    public function destroy(Request $request): Application|Factory|View
    {
        Auth::user()->notifications()->where([
            'id' => $request->input('id'),
            'notifiable_id' => Auth::user()->id
        ])->delete();

        return view('messages.index')->with('success', 'Message deleted');
    }

    /**
     * Delete all user messages
     *
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     */
    public function destroyAll(Request $request)
    {
        try{
            $user = User::findOrFail($request->input('user_id'));
            $user->notifications()->delete();

            return view('messages.index')->with('success', 'Messages deleted');
        } catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','System Notifications Not Deleted');
        }
    }

}
