<?php

namespace App\Http\Controllers\Coins;

use App\Actions\CollectedCountAction;
use App\Helpers\Types\TypeHelper;
use App\Http\Controllers\Controller;
use App\Models\Coins\Coin;
use App\Repositories\Coin\CoinVarietyRepository;
use App\Repositories\Coin\CollectedRepository;
use Illuminate\Support\Facades\DB;

class AmericanEagleController extends Controller
{


    public const EAGLE_TYPES = [
        'American Platinum Eagle' => [
            120 => 'Tenth Ounce Platinum',
            94 => 'Quarter Ounce Platinum',
            96 => 'Half Ounce Platinum',
            100 => 'One Ounce Platinum',
        ],
        'Gold American Eagle' => [
            77 => 'Tenth Ounce Gold',
            82 => 'Quarter Ounce Gold',
            95 => 'Half Ounce Gold',
            99 => 'One Ounce Gold',
        ],
        'American Palladium Eagle' => [
            109 => 'American Palladium Eagle',
        ],
        'American Silver Eagle' => [
            101 => 'Silver American Eagle',
        ]
    ];

    public function __construct(
        private CoinVarietyRepository $coinVarietyRepository,
        private CollectedCountAction $collectedCountAction,
        private CollectedRepository $collectedRepository)
    {}

    public function index()
    {

        $coins = DB::table('collected')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('coins.commemorativeType', 'American Eagle')
            ->orderBy('coins.coinYear')->orderBy('coins.denomination')->get();

        $sets = DB::table('mintset')
            ->select('id','setName')
            ->where('setType', 'American Eagle')
            ->orderBy('coinYear')
            ->get();

        return view('coins.am_eagle.index', [
            'categories' => TypeHelper::COIN_CATEGORIES,
            'types_list' => self::EAGLE_TYPES,
            'coins' => $coins ?? [],
            'sets' => $sets,
        ]);
    }




    public function viewType(string $type)
    {

        $eagle_type = match ($type) {
            'Gold' => 'Gold American Eagle',
            'Palladium' => 'American Palladium Eagle',
            'Platinum' => 'Platinum American Eagle',
            'Silver' => 'Silver American Eagle',
        };

        if($eagle_type == 'Platinum American Eagle'){
            $series_list = Coin::select('series')->distinct()
                ->where('commemorativeVersion',$eagle_type)
                ->where('series', '<>', 'None')
                ->orderBy('series')->get()->pluck('series')->toArray();
        }

        $coins_list = Coin::select('id', 'coinType', 'cointypes_id','coinName', 'coinYear')
            ->where('commemorativeVersion',$eagle_type)
            ->orderBy('coinYear')->orderBy('denomination')->get();

        $coins = $this->collectedCountAction->getCollectedByCoin($coins_list);
        return view('coins.am_eagle.version', [
            'categories' => TypeHelper::COIN_CATEGORIES,
            'types_list' => TypeHelper::COMBINED_LIST,
            'coins' => $coins,
            'criteria' => $type,
            'eagle_type' => $eagle_type,
            'series_list' => $series_list ?? [],
        ]);
    }





}
