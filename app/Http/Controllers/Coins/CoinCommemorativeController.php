<?php

namespace App\Http\Controllers\Coins;

use App\Actions\CollectedCountAction;
use App\Helpers\Types\TypeHelper;
use App\Http\Controllers\Controller;
use App\Models\Coins\Coin;
use App\Repositories\Coin\CoinVarietyRepository;
use App\Repositories\Coin\CollectedRepository;
use Illuminate\Support\Facades\DB;

class CoinCommemorativeController extends Controller
{

    public function __construct(
        private CoinVarietyRepository $coinVarietyRepository,
        private CollectedCountAction $collectedCountAction,
        private CollectedRepository $collectedRepository)
    {}

    public function index()
    {
        $coins_list = Coin::select('id', 'coinType', 'cointypes_id','coinName', 'coinYear')
            ->where('commemorative', 1)
            ->orderBy('coinYear')->orderBy('denomination')->get();

        //$coins = $this->collectedCountAction->getCollectedByCoin($coins_list);
        $commemorativeCategorys = DB::table('coins')
            ->select('commemorativeCategory')->distinct()
            ->where('commemorativeCategory', '<>', 'None')
            ->orderBy('commemorativeCategory')
            ->get();
        $commemorativeTypes = DB::table('coins')
            ->select('commemorativeType')->distinct()
            ->where('commemorativeType', '<>', 'None')
            ->orderBy('commemorativeType')
            ->get();
        return view('coins.commemorative.index', [
            'categories' => TypeHelper::COIN_CATEGORIES,
            'types_list' => TypeHelper::COMBINED_LIST,
            'commemorativeCategorys' => $commemorativeCategorys,
            'commemorativeTypes' => $commemorativeTypes,
            'coins' => $coins_list,
        ]);
    }

    public function viewType(string $type)
    {
        $type = str_replace(' ', '_', $type);
        $coins_list = Coin::select('id', 'coinType', 'cointypes_id','coinName', 'coinYear')
            ->where('commemorativeType',$type)
            ->orderBy('coinYear')->orderBy('denomination')->get();

        $coins = $this->collectedCountAction->getCollectedByCoin($coins_list);
        return view('coins.commemorative.index', [
            'categories' => TypeHelper::COIN_CATEGORIES,
            'types_list' => TypeHelper::COMBINED_LIST,
            'coins' => $coins,
            'criteria' => $type,
        ]);
    }

    public function viewCategory(string $category)
    {
        $category = str_replace(' ', '_', $category);
        $coins_list = Coin::select('id', 'coinType', 'cointypes_id','coinName', 'coinYear')
            ->where('commemorativeCategory',$category)
            ->orderBy('coinYear')->orderBy('denomination')->get();

        $coins = $this->collectedCountAction->getCollectedByCoin($coins_list);
        return view('coins.subset.view_genre', [
            'categories' => TypeHelper::COIN_CATEGORIES,
            'types_list' => TypeHelper::COMBINED_LIST,
            'coins' => $coins,
            'criteria' => $category,
        ]);
    }

}
