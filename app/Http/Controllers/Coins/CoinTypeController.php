<?php
/**
 * CoinTypeController | actions for views/coins
 *
 * @package Coins
 * @subpackage Display
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Coins;

use App\Actions\CollectedCountAction;
use App\Helpers\Types\TypeHelper;
use App\Http\Controllers\Controller;
use App\Models\Coins\Coin;
use App\Models\Coins\CoinType;
use App\Repositories\Coin\CoinVarietyRepository;
use App\Repositories\Coin\CollectedRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Log;
use Throwable;

class CoinTypeController extends Controller
{

    public function __construct(
        private CoinVarietyRepository $coinVarietyRepository,
        private CollectedCountAction $collectedCountAction,
        private CollectedRepository $collectedRepository)
    {}

    /**
     * Show coins home page
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        return view('coins.index');
    }


    /**
     * @param int $type_id
     * @return Factory|View|RedirectResponse|Application
     */
    public function viewCoinType(int $type_id): Factory|View|RedirectResponse|Application
    {
        try{
            $coinType = CoinType::findOrFail($type_id);
            if(class_exists($coinType->helper()) ){
                $typeSpecificLinks = $coinType->helper()::typeSpecificLinks();
                $typeLinks = $coinType->helper()::typeLinks();
                //$releaseList = $coinType->helper()::releaseList($type_id);
            }
            $strikes = Coin::select('strike')->distinct()
                ->where('cointypes_id', $type_id)->get()->pluck('strike')->toArray();
            $designVarieties = $this->collectedRepository->getTypeDesignVarieties($type_id);
            //$mintmarkStyles = TypeHelper::mintmarkLists($coinType);
            $business = $this->collectedRepository->getCollectedBusinessStrikeCountByType($type_id);
            $proofs = $this->collectedRepository->getCollectedProofStrikeCountByType($type_id);
            $coins_list = Coin::select('id','coinName', 'coinYear', 'mintMark', 'strike')->where('cointypes_id', $coinType->id)->orderBy('coinYear')->get();
            $coins = $this->collectedCountAction->getCollectedByCoin($coins_list);
            $yearLists = TypeHelper::yearLists($coinType->id);
            $keyDates = $coinType->keyDates();
            $semiKeyDates = $coinType->semiKeyDates();

            return view('coins.view_type', [
                'coinType' => $coinType,
                'coins' => $coins,
                'design_varieties' => $designVarieties,
                //'mintmarkStyles' => $mintmarkStyles,
                'proofs' => $proofs,
                'strikes' => $strikes,
                'yearLists' => $yearLists,
                'color_cat' => in_array($coinType->category->coinCategory, TypeHelper::COLOR_CATEGORIES) ? 1 : 0,
                'steps_type' => in_array($coinType->category->coinCategory, TypeHelper::COLOR_CATEGORIES) ? 1 : 0,
                'typeLinks' => $typeLinks ?? [],
                'typeSpecificLinks' => $typeSpecificLinks ?? [],
                'releaseList' => $releaseList ?? [],
                'business' => $business,
                'keyDates' => $keyDates,
                'semiKeyDates' => $semiKeyDates,
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load');
        }

    }

    /**
     * View coin type by mint location
     *
     * @param CoinType $coinType
     * @param $mint
     * @return Factory|View|RedirectResponse|Application
     */
    public function viewCoinTypeMint(CoinType $coinType, $mint): Factory|View|RedirectResponse|Application
    {
        try{
            $mint = str_replace('_', ' ', $mint);
            if(class_exists($coinType->helper()) ){
                $typeSpecificLinks = $coinType->helper()::typeSpecificLinks();
                $typeLinks = $coinType->helper()::typeLinks();
                $releaseList = $coinType->helper()::releaseList($coinType->id);
            }
            $strikes = Coin::select('strike')->distinct()
                ->where('cointypes_id', $coinType->id)->get()->pluck('strike')->toArray();
            $coins_list = Coin::select('id','coinName', 'coinYear', 'mintMark', 'strike')
                ->where('cointypes_id', $coinType->id)->where('mint', $mint)
                ->orderBy('coinYear')->get();

            $designVarieties = $this->collectedRepository->getTypeDesignVarieties($coinType->id);
            $mintmarkStyles = Coin::select('mms')->distinct()
                ->where('cointypes_id',$coinType->id)->where('mint',trim($mint))
                ->where('mint',trim($mint))->where('mms','<>', 'None')
                ->get()->pluck('mms')->toArray();
            $business = $this->collectedRepository->getCollectedBusinessStrikeCountByType($coinType->id);
            $proofs = $this->collectedRepository->getCollectedProofStrikeCountByType($coinType->id);
            $coins = $this->collectedCountAction->getCollectedByCoin($coins_list);

            return view('coins.view_type_mint', [
                'coinType' => $coinType,
                'coins' => $coins,
                'mint' => str_replace('_', ' ', $mint),
                'mintmarkStyles' => $mintmarkStyles,
                'proofs' => $proofs,
                'strikes' => $strikes,
                'color_cat' => in_array($coinType->category->coinCategory, TypeHelper::COLOR_CATEGORIES) ? 1 : 0,
                'steps_type' => in_array($coinType->category->coinCategory, TypeHelper::COLOR_CATEGORIES) ? 1 : 0,
                'typeLinks' => $typeLinks ?? [],
                'releaseList' => $releaseList ?? [],
                'typeSpecificLinks' => $typeSpecificLinks ?? [],
                'business' => $business,
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load');
        }

    }


    /**
     * Get array of coin types
     *
     * @todo change image url
     */
    public function loadTypeCoins(string $type_id): JsonResponse
    {
        try{
            $coins = Coin::select('id','coinName', 'coinYear', 'mintMark')->where('cointypes_id', $type_id)->orderBy('coinYear')->get()->toArray();
            $image = 'http://cdn.dev-php.site/public/img/coins/'.str_replace(' ', '_', TypeHelper::COIN_TYPES[$type_id]).'.jpg';
            return response()->json(['coins' => $coins, 'image' => $image]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return response()->json(['error' => 'Could not load']);
        }
    }

    /**
     * View type by strike, (business,proof,...)
     *
     * @param CoinType $coinType
     * @param string $strike
     * @return Factory|View|RedirectResponse|Application
     */
    public function viewCoinTypeStrike(CoinType $coinType, string $strike): Factory|View|RedirectResponse|Application
    {
        try{
            $strike = str_replace('_', ' ', $strike);
            $coins = $this->collectedRepository->getCollectedByCoinStrike($coinType->id, $strike);

            if(class_exists($coinType->helper()) ){
                $typeSpecificLinks = $coinType->helper()::typeSpecificLinks();
                $typeLinks = $coinType->helper()::typeLinks();
            }
            $strikes = Coin::select('strike')->distinct()
                ->where('cointypes_id', $coinType->id)->get()->pluck('strike')->toArray();

            return view('coins.view_type_strike', [
                'coinType' => $coinType,
                'coins' => $coins,
                'strikes' => $strikes,
                'strike' => str_replace('_', ' ', $strike),
                'typeLinks' => $typeLinks ?? [],
                'typeSpecificLinks' => $typeSpecificLinks ?? [],
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load');
        }

    }

    /**
     * View type by Design Variety
     *
     * @param CoinType $coinType
     * @param string $detail
     * @param string $variety
     * @return Factory|View|RedirectResponse|Application
     */
    public function viewCoinTypeDesignVariety(CoinType $coinType, string $detail, string $variety): Factory|View|RedirectResponse|Application
    {
        try{
            $coins = $this->collectedRepository->getCollectedByCoinDesignVariety($coinType, $detail, urldecode($variety));
            $design_variety = str_replace('_', ' ', urldecode($variety));

            if(class_exists($coinType->helper()) ){
                $typeSpecificLinks = $coinType->helper()::typeSpecificLinks();
                $typeLinks = $coinType->helper()::typeLinks();
            }
            $variety_list = match ($detail) {
                'obverse' => Coin::select('obv')
                    ->where('cointypes_id', $coinType->id)->get()->pluck('obv')->unique()->toArray(),
                'reverse' => Coin::select('rev')
                    ->where('cointypes_id', $coinType->id)->get()->pluck('rev')->unique()->toArray(),
                default => [],
            };
            $variety_name = match ($detail) {
                'obverse' => 'Obverse Design Variety',
                'reverse' => 'Reverse Design Variety',
                default => [],
            };
            $strikes = Coin::select('mms')->distinct()
                ->where('cointypes_id', $coinType->id)->get()->pluck('strike')->toArray();

            return view('coins.view_type_design_variety', [
                'coinType' => $coinType,
                'variety' => urldecode($variety),
                'coins' => $coins,
                'strikes' => $strikes,
                'variety_list' => $variety_list,
                'typeLinks' => $typeLinks ?? [],
                'variety_name' => $variety_name,
                'typeSpecificLinks' => $typeSpecificLinks ?? [],
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load');
        }

    }

    /**
     * View type by Mintmark Style
     *
     * @param CoinType $coinType
     * @param string $mint
     * @param string $mms
     * @return Factory|View|RedirectResponse|Application
     */
    public function viewCoinTypeMintMarkStyle(CoinType $coinType, string $mint, string $mms): Factory|View|RedirectResponse|Application
    {
        try{
            $mintMarkStyle = str_replace('_', ' ', urldecode($mms));
            $mint = str_replace('_', ' ', urldecode($mint));

            $coins = $this->collectedRepository->getCollectedMintmarkVariety($coinType->id, $mint, $mintMarkStyle);

            if(class_exists($coinType->helper()) ){
                $typeSpecificLinks = $coinType->helper()::typeSpecificLinks();
                $typeLinks = $coinType->helper()::typeLinks();
            }

            $mm_styles = Coin::select('mms')->distinct()
                ->where('cointypes_id', $coinType->id)->where('mint', $mint)
                ->get()->pluck('mms')->toArray();

            return view('coins.view_type_mms', [
                'coinType' => $coinType,
                'mms' => $mintMarkStyle,
                'coins' => $coins,
                'mm_styles' => $mm_styles,
                'typeLinks' => $typeLinks ?? [],
                'mint' => $mint,
                'typeSpecificLinks' => $typeSpecificLinks ?? [],
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load');
        }

    }




}
