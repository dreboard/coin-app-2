<?php

namespace App\Http\Controllers\Coins\Forum;

use App\Helpers\Types\TypeHelper;
use App\Http\Controllers\Controller;
use App\Models\Coins\Coin;
use App\Models\Coins\CoinType;
use App\Models\Forum;
use App\Repositories\Coin\CollectedRepository;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

class TypeController extends Controller
{


    public function __construct(
        private CollectedRepository $collectedRepository)
    {}


    public function index(int $type_id)
    {
        try{
            $coinType = CoinType::findOrFail($type_id);
            //$coinType = CoinType::with('forum')->select('id','coinName')->where('id',$type_id)->get();


            $forum_posts = Forum::whereHasMorph('forumable', [CoinType::class], function($query) use ($type_id){
                $query->where('forumable_id', $type_id);
            })->paginate(15);


            $strikes = Coin::select('strike')->distinct()
                ->where('cointypes_id', $coinType->id)->get()->pluck('strike')->toArray();
            $designVarieties = $this->collectedRepository->getTypeDesignVarieties($coinType->id);
            $mintmarkStyles = TypeHelper::mintmarkLists($coinType);
            $coins_list = Coin::select('id','coinName', 'coinYear', 'mintMark', 'strike')->where('cointypes_id', $coinType->id)->orderBy('coinYear')->get();

            return view('members.forum.type.index', [
                'coinType' => $coinType,
                'coins_list' => $coins_list,
                'design_varieties' => $designVarieties,
                'mintmarkStyles' => $mintmarkStyles,
                'strikes' => $strikes,
                'forum_posts' => $forum_posts,
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return response()->json(['error' => 'Could not load']);
        }

    }



    public function getForumCountByType(int $type_id): int
    {
        $coins = Coin::select('id')->where('cointypes_id', $type_id)->pluck('id')->toArray();
        $info = DB::table('forums')
            ->where('forums.forumable_id', $type_id)
            ->where('forums.forumable_type', 'App\Models\Coins\CoinType')
            ->orWhere(function($query) use($coins) {
                $query->where('forums.forumable_type','App\Models\Coins\Coin')
                    ->whereIn('forums.forumable_id',$coins);
            })
            ->get()->count();
        //Log::info(var_export(DB::getQueryLog(), true));
        return $info;
    }


    public function getForumLatestByType(int $type_id): string
    {
        $coins = Coin::select('id')->where('cointypes_id', $type_id)->get()->pluck('id')->toArray();
        $info = DB::table('forums')->select('forums.created_at')
            ->where('forums.forumable_id', $type_id)
            ->where('forums.forumable_type', 'App\Models\Coins\CoinType')
            ->orWhere(function($query) use($coins) {
                $query->where('forums.forumable_type','App\Models\Coins\Coin')
                    ->whereIn('forums.forumable_id',$coins);
            })
            ->orderBy('forums.id', 'DESC')->first();
        return $info ? Carbon::parse($info->created_at)->diffForHumans() : 'None';
    }

    /**
     * Get Forum counts for Type/Coin of type
     *
     * @param int $type_id
     * @return JsonResponse
     */
    public function getTypeInfo(int $type_id): JsonResponse
    {
        try{
            $info_list = [];
            $info_list['last'] = $this->getForumLatestByType($type_id);
            $info_list['total'] = $this->getForumCountByType($type_id);

            return response()->json(['data' => $info_list]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return response()->json(['error' => 'Could not load']);
        }
    }



    // end
}
