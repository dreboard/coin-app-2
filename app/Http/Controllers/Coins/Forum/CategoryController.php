<?php

namespace App\Http\Controllers\Coins\Forum;

use App\Helpers\Types\TypeHelper;
use App\Http\Controllers\Controller;
use App\Models\Coins\CoinCategory;
use App\Models\Forum;
use App\Repositories\Coin\CollectedRepository;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

class CategoryController extends Controller
{

    public function __construct(
        private CollectedRepository $collectedRepository)
    {}


    public function index(int $cat_id)
    {
        try{
            $coinCategory = CoinCategory::findOrFail($cat_id);
            /*$forum_posts = Forum::whereHasMorph('forumable', [CoinCategory::class], function($query) use ($cat_id){
                $query->where('forumable_id', $cat_id);
            })->paginate(15);

            $forum_posts = Forum::whereHasMorph('forumable', [CoinCategory::class], function($query) use ($cat_id){
                $query->where(function($query) use ($cat_id) {
                    $query->where('forumable_id', $cat_id);
                    $query->orWhere(function($query) use ($cat_id) {
                        $query->where('forumable_type', 'App\Models\Coins\CoinType')
                            ->whereIn('forumable_id', TypeHelper::COMBINED_LIST[$cat_id]);
                    });
                });
            })->paginate(15);
            */

            $forum_posts = Forum::whereHasMorph('forumable', [CoinCategory::class], function($query) use ($cat_id){
                $query->where('forumable_id', $cat_id);
            })->orWhere(function($query) use ($cat_id) {
                $query->where('forumable_type', 'App\Models\Coins\CoinType')
                    ->whereIn('forumable_id', TypeHelper::COMBINED_LIST[$cat_id]);
            })->paginate(15);




            $type_list = TypeHelper::COMBINED_LIST[$cat_id];

            return view('members.forum.category.index', [
                'coinCategory' => $coinCategory,
                'type_list' => $type_list,
                'forum_posts' => $forum_posts,
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return response()->json(['error' => 'Could not load']);
        }

    }

    public function getForumCountByCategory(int $cat_id): int
    {
        //DB::enableQueryLog();
        $types = array_keys(TypeHelper::COMBINED_LIST[$cat_id]);
        $info = DB::table('forums')
            ->where('forums.forumable_id', $cat_id)
            ->where('forums.forumable_type', 'App\Models\Coins\CoinCategory')
            ->orWhere(function($query) use($types) {
                $query->where('forums.forumable_type','App\Models\Coins\CoinType')
                    ->whereIn('forums.forumable_id',$types);
            })
            ->get()->count();
        //Log::info(var_export(DB::getQueryLog(), true));
        return $info;
    }
    public function getForumLatestByCategory(int $cat_id): string
    {;
        $types = array_keys(TypeHelper::COMBINED_LIST[$cat_id]);
        $info = DB::table('forums')->select('forums.created_at')
            ->where('forums.forumable_id', $cat_id)
            ->where('forums.forumable_type', 'App\Models\Coins\CoinCategory')
            ->orWhere(function($query) use($types) {
                $query->where('forums.forumable_type','App\Models\Coins\CoinType')
                    ->whereIn('forums.forumable_id',$types);
            })
            ->orderBy('forums.id', 'DESC')->first();
        return $info ? Carbon::parse($info->created_at)->diffForHumans() : 'None';
    }

    /**
     * @param $cat_id
     * @return JsonResponse
     */
    public function getCategoryInfo($cat_id): JsonResponse
    {
        try{
            $info_list = [];
            $info_list['last'] = $this->getForumLatestByCategory($cat_id);
            $info_list['total'] = $this->getForumCountByCategory($cat_id);

            return response()->json(['data' => $info_list]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return response()->json(['error' => 'Could not load']);
        }
    }


}
