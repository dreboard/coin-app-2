<?php

namespace App\Http\Controllers\Coins;

use App\Http\Controllers\Controller;
use App\Models\Coins\Coin;
use App\Models\Coins\Collected;
use App\Models\Coins\Mintset;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class CoinYearController extends Controller
{


    /**
     * View a coin
     *
     * @param int|null $year
     * @return View|Factory|Application
     */
    public function viewYear(?int $year = null): View|Factory|Application
    {
        if($year > date('Y') || $year < 1793){ $year = date('Y');}
        if ($year === null) {
            $year = date('Y');
        }

        $coins = Coin::where('coinYear', $year)->orderBy('denomination')->get();
        $mintsets = Mintset::where('coinYear', $year)->get();
        $rolls = [];
        $collected = Collected::select('collected.id','collected.grade','collected.cost','collected.tpg_service', 'collected.coin_id', 'coins.coinType', 'coins.cointypes_id',
            'collected.lot_id', 'collected.set_id', 'collected.roll_id', 'collected.folder_id', 'collected.firstday_id')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)->withCount('coin')
            ->where('coins.coinYear', $year)
            ->get();
        return view('coins.dates.view', [
            'year' => $year,
            'coins' => $coins,
            'mintsets' => $mintsets,
            'rolls' => $rolls,
            'collected' => $collected,
            'count' => count($collected->pluck('coin_count')),
            'cost' => $collected->pluck('cost')->sum(),
        ]);
    }

    /**
     * View a coin
     *
     * @param int $decade
     * @return View|Factory|Application
     */
    public function viewDecade(int $decade): View|Factory|Application
    {
        $coins = Coin::where('coinYear', $decade)->orderBy('denomination')->get();
        $mintsets = Mintset::where('coinYear', $decade)->get();
        $rolls = [];
        $collected = Collected::select('collected.id','collected.grade','collected.cost','collected.tpg_service')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)->withCount('coin')
            ->where('coins.coinYear', $decade)
            ->get();
        return view('coins.dates.view', [
            'year' => $decade,
            'coins' => $coins,
            'mintsets' => $mintsets,
            'rolls' => $rolls,
            'collected' => $collected,
            'count' => count($collected->pluck('coin_count')),
            'cost' => $collected->pluck('cost')->sum(),
        ]);
    }

    /**
     * View a coin
     *
     * @param int $century
     * @return View|Factory|Application
     */
    public function viewCentury(int $century): View|Factory|Application
    {
        $coins = Coin::where('coinYear', $century)->orderBy('denomination')->get();
        $mintsets = Mintset::where('coinYear', $century)->get();
        $rolls = [];
        $collected = Collected::select('collected.id','collected.grade','collected.cost','collected.tpg_service')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)->withCount('coin')
            ->where('coins.coinYear', $century)
            ->get();
        return view('coins.dates.view', [
            'year' => $century,
            'coins' => $coins,
            'mintsets' => $mintsets,
            'rolls' => $rolls,
            'collected' => $collected,
            'count' => count($collected->pluck('coin_count')),
            'cost' => $collected->pluck('cost')->sum(),
        ]);
    }


    public function getCoinCollectedCountForCoinYear($coin_id, $year): JsonResponse
    {
        $count = DB::table('collected')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->where('collected.coin_id', $coin_id)
            ->where('coins.coinYear', $year)
            ->get()->count();
        return response()->json(['count' => $count]);
    }

    public function getSetCollectedCountForMintsetYear($coin_id, $year): JsonResponse
    {
        $count = DB::table('collected_sets')
            ->join('mintset', 'mintset.id', '=', 'collected_sets.set_id')
            ->where('collected_sets.user_id',auth()->user()->id)
            ->where('mintset.coinYear', $year)
            ->get()->count();
        return response()->json(['count' => $count]);
    }


}
