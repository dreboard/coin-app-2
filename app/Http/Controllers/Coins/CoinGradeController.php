<?php
/**
 * CoinGradeController | actions for views/coins/grades
 *
 * @package Coins
 * @subpackage Reports
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Coins;

use App\Helpers\GradeHelper;
use App\Http\Controllers\Controller;
use App\Models\Coins\Coin;
use App\Models\Coins\CoinType;
use App\Models\Coins\Collected;
use App\Repositories\Coin\CoinGradeRepository;
use App\Repositories\Coin\TypeGradeRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use NumberFormatter;
use Throwable;

class CoinGradeController extends Controller
{

    /**
     * @param TypeGradeRepository $typeGradeRepository
     * @param CoinGradeRepository $coinGradeRepository
     */
    public function __construct(
        private TypeGradeRepository $typeGradeRepository,
        private CoinGradeRepository $coinGradeRepository
    ){}


    /**
     * Grade Sheet for coin type
     *
     * @param CoinType $coinType
     * @return Application|Factory|View|RedirectResponse
     */
    public function getTypeGrades(CoinType $coinType): View|Factory|RedirectResponse|Application
    {
        try{
            $has_special = Coin::where('coins.cointypes_id', $coinType->id)
                ->whereIn('coins.strike',GradeHelper::SPECIAL_STRIKE)
                ->get()->count();
            $gradeTable = GradeHelper::getGradeForType($coinType->id);
            return view('coins.grades.view_type', [
                'coinType' => $coinType,
                'gradeTable' => $gradeTable,
                'has_special' => $has_special,

            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could load grades');
        }

    }

    /**
     * Grade Sheet for coin type and strike (Lincoln Wheat Proofs/Special...)
     *
     * @param string $strike
     * @param CoinType $coinType
     * @return Application|Factory|View|RedirectResponse
     */
    public function getTypeStrikeGrades(string $strike, CoinType $coinType): View|Factory|RedirectResponse|Application
    {
        try{
            $gradeTable = GradeHelper::getGradeForTypeForStrike($coinType->id, $strike);
            return view('coins.grades.view_type_strike', [
                'coinType' => $coinType,
                'gradeTable' => $gradeTable,
                'strike' => $strike,
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could load grades');
        }

    }

    /**
     * @param Coin $coin
     * @return Application|Factory|View|RedirectResponse
     */
    public function getCoinGrades(Coin $coin): View|Factory|RedirectResponse|Application
    {
        try{
            $has_special = Coin::where('coins.cointypes_id', $coin->id)
                ->whereIn('coins.strike',GradeHelper::SPECIAL_STRIKE)
                ->get()->count();

            $gradeTable = GradeHelper::getGradeForCoin($coin->id, $coin->strike);
            return view('coins.grades.coin', [
                'coin' => $coin,
                'gradeTable' => $gradeTable,
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could load grades');
        }

    }

    /**
     * View specific grade for coin
     *
     * @param Coin $coin
     * @param $grade
     * @return Application|Factory|View|RedirectResponse
     */
    public function viewGradeForCoin(Coin $coin, $grade): View|Factory|RedirectResponse|Application
    {
        try{
            $gradeDescription = GradeHelper::COIN_GRADING_NUMBERS;
            $collected = Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
                ->where('collected.user_id',auth()->user()->id)
                ->where('coins.id', $coin->id)
                ->where('collected.grade', $grade)
                ->select('collected.grade','collected.id','collected.cost','collected.coin_id','collected.nickName')
                ->get();
            $tpg = [];
            $tpg['pcgs'] = DB::table('collected')
                ->where('collected.user_id',auth()->user()->id)
                ->where('collected.coin_id', $coin->id)
                ->where('collected.tpg_service', 'PCGS')
                ->get()->count();
            $tpg['ngc'] = DB::table('collected')
                ->where('collected.user_id',auth()->user()->id)
                ->where('collected.coin_id', $coin->id)
                ->where('collected.tpg_service', 'NGC')
                ->get()->count();
            $tpg['anacs'] = DB::table('collected')
                ->where('collected.user_id',auth()->user()->id)
                ->where('collected.coin_id', $coin->id)
                ->where('collected.tpg_service', 'ANACS')
                ->get()->count();

            return view('coins.grades.view_coin_grade', [
                'coin' => $coin,
                'collected' => $collected,
                'grade' => $grade,
                'gradeDescription' => $gradeDescription,
                'tpg' => $tpg,
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could load grades');
        }

    }

    /**
     * @param CoinType $coinType
     * @param $grade
     * @return Application|Factory|View|RedirectResponse
     */
    public function viewTypeGrade(CoinType $coinType, $grade): View|Factory|RedirectResponse|Application
    {
        try{
            $collected = Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
                ->where('collected.user_id',auth()->user()->id)
                ->where('coins.cointypes_id', $coinType->id)
                ->where('collected.grade', $grade)
                ->select('collected.grade','collected.id','collected.cost','collected.coin_id','collected.nickName')
                ->get();
            //dd($collected_grades);
            return view('coins.grades.view_grade', [
                'coinType' => $coinType,
                'collected' => $collected,
                'grade' => $grade,
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could load grades');
        }

    }

    /**
     * View grade by type and strike (all Lincoln Wheat MS-69)
     *
     * @param CoinType $coinType
     * @param int $grade
     * @param string $strike
     * @return Application|Factory|View|RedirectResponse
     */
    public function viewTypeStrikeGrade(CoinType $coinType, int $grade, string $strike): View|Factory|RedirectResponse|Application
    {
        try{
            $collected = Collected::join('coins', 'coins.id', '=', 'collected.coin_id')
                ->where('collected.user_id',auth()->user()->id)
                ->where('coins.cointypes_id', $coinType->id)
                ->where('coins.strike', $strike)
                ->where('collected.grade', $grade)
                ->select('collected.grade','collected.id','collected.cost','collected.coin_id','collected.nickName')
                ->get();
            //dd($collected_grades);
            return view('coins.grades.view_strike_grade', [
                'coinType' => $coinType,
                'collected' => $collected,
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could load grades');
        }

    }


    /**
     * Populate data for type reports
     *
     * @param int $type_id
     * @return JsonResponse
     */
    public function getTypeStrikeDetails(int $type_id): JsonResponse
    {
        try{
            $data = [];
            $data['tpg']['pcgs'] = DB::table('collected')->join('coins', 'coins.id', '=', 'collected.coin_id')
                ->where('collected.user_id',auth()->user()->id)
                ->where('coins.cointypes_id', $type_id)
                ->where('collected.tpg_service', 'PCGS')
                ->get()->count();
            $data['tpg']['ngc'] = DB::table('collected')->join('coins', 'coins.id', '=', 'collected.coin_id')
                ->where('collected.user_id',auth()->user()->id)
                ->where('coins.cointypes_id', $type_id)
                ->where('collected.tpg_service', 'NGC')
                ->get()->count();
            $data['tpg']['anacs'] = DB::table('collected')->join('coins', 'coins.id', '=', 'collected.coin_id')
                ->where('collected.user_id',auth()->user()->id)
                ->where('coins.cointypes_id', $type_id)
                ->where('collected.tpg_service', 'ANACS')
                ->get()->count();
            $data['business']['count'] = $this->typeGradeRepository->getBusinessStrikeCount($type_id);
            $data['business']['graded'] = $this->typeGradeRepository->getBusinessStrikeGradedCount($type_id);
            $data['business']['ungraded'] = $this->typeGradeRepository->getBusinessStrikeUnGradedCount($type_id);
            $data['business']['pro_graded'] = $this->typeGradeRepository->getBusinessStrikeGradedProCount($type_id);

            $data['proof']['count'] = $this->typeGradeRepository->getProofStrikeCount($type_id);
            $data['proof']['graded'] = $this->typeGradeRepository->getProofStrikeGradedCount($type_id);
            $data['proof']['ungraded'] = $this->typeGradeRepository->getProofStrikeUnGradedCount($type_id);
            $data['proof']['pro_graded'] = $this->typeGradeRepository->getProofStrikeGradedProCount($type_id);

            $data['special']['count'] = $this->typeGradeRepository->getSpecialStrikeCount($type_id);
            $data['special']['graded'] = $this->typeGradeRepository->getSpecialStrikeGradedCount($type_id);
            $data['special']['ungraded'] = $this->typeGradeRepository->getSpecialStrikeUnGradedCount($type_id);
            $data['special']['pro_graded'] = $this->typeGradeRepository->getSpecialStrikeGradedProCount($type_id);

            $data['graded'] = $this->typeGradeRepository->getGradedCount($type_id);
            $data['ungraded'] = $this->typeGradeRepository->getUngradedCount($type_id);

            $data['raw'] = $this->typeGradeRepository->getRawCount($type_id);
            $data['slabbed'] = $this->typeGradeRepository->getSlabbedCount($type_id);


            return response()->json(['coins' => $data]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return response()->json(['error' => 'Could not load data']);
        }

    }

    /**
     * Populate data for type reports
     *
     * @param int $coin_id
     * @return JsonResponse
     */
    public function getCoinStrikeDetails(int $coin_id): JsonResponse
    {
        try{
            $coin = Coin::find($coin_id);//select('id','strike','cointypes_id')->where('id', $coin_id)->get();
            log::info($coin);
            $data = [];
            $data['tpg']['pcgs'] = DB::table('collected')
                ->where('collected.user_id',auth()->user()->id)
                ->where('collected.coin_id', $coin_id)
                ->where('collected.tpg_service', 'PCGS')
                ->get()->count();
            $data['tpg']['ngc'] = DB::table('collected')
                ->where('collected.user_id',auth()->user()->id)
                ->where('collected.coin_id', $coin_id)
                ->where('collected.tpg_service', 'NGC')
                ->get()->count();
            $data['tpg']['anacs'] = DB::table('collected')
                ->where('collected.user_id',auth()->user()->id)
                ->where('collected.coin_id', $coin_id)
                ->where('collected.tpg_service', 'ANACS')
                ->get()->count();
            $data['tpg']['igc'] = DB::table('collected')
                ->where('collected.user_id',auth()->user()->id)
                ->where('collected.coin_id', $coin_id)
                ->where('collected.tpg_service', 'IGC')
                ->get()->count();
            if(in_array($coin->strike, GradeHelper::BUSINESS_STRIKE)){
                $data['prefix'] = 'MS-';
                $data['business']['count'] = $this->coinGradeRepository->getBusinessStrikeCount($coin_id);
                $data['business']['graded'] = $this->coinGradeRepository->getBusinessStrikeGradedCount($coin_id);
                $data['business']['ungraded'] = $this->coinGradeRepository->getBusinessStrikeUnGradedCount($coin_id);
                $data['business']['pro_graded'] = $this->coinGradeRepository->getBusinessStrikeGradedProCount($coin_id);
            }
            if(in_array($coin->strike, GradeHelper::PROOF_STRIKE)){
                $data['prefix'] = 'PR-';
                $data['proof']['count'] = $this->coinGradeRepository->getProofStrikeCount($coin_id);
                $data['proof']['graded'] = $this->coinGradeRepository->getProofStrikeGradedCount($coin_id);
                $data['proof']['ungraded'] = $this->coinGradeRepository->getProofStrikeUnGradedCount($coin_id);
                $data['proof']['pro_graded'] = $this->coinGradeRepository->getProofStrikeGradedProCount($coin_id);
            }
            if(in_array($coin->strike, GradeHelper::SPECIAL_STRIKE)){
                $data['prefix'] = 'SP-';
                $data['special']['count'] = $this->coinGradeRepository->getSpecialStrikeCount($coin_id);
                $data['special']['graded'] = $this->coinGradeRepository->getSpecialStrikeGradedCount($coin_id);
                $data['special']['ungraded'] = $this->coinGradeRepository->getSpecialStrikeUnGradedCount($coin_id);
                $data['special']['pro_graded'] = $this->coinGradeRepository->getSpecialStrikeGradedProCount($coin_id);
            }

            $data['graded'] = $this->coinGradeRepository->getGradedCount($coin_id);
            $data['ungraded'] = $this->coinGradeRepository->getUngradedCount($coin_id);

            $data['raw'] = $this->coinGradeRepository->getRawCount($coin_id);
            $data['slabbed'] = $this->coinGradeRepository->getSlabbedCount($coin_id);
            $data['finest'] = $this->coinGradeRepository->getFinest($coin_id);

            if($coin->coinType == 'Standing Liberty'){
                $data['full_attribute'] = $this->getFullHeadCoins($coin);
            }


            return response()->json(['coins' => $data]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return response()->json(['error' => 'Could not load data']);
        }
    }

    public function getFullHeadCoins(Coin $coin)
    {
        return Collected::where('collected.user_id',auth()->user()->id)
            ->where('collected.coin_id', $coin->id)
            ->whereNotNull('collected.fullAtt')
            ->select('collected.grade','collected.id','collected.nickName','collected.fullAtt')
            ->get();

    }

    /**
     * Get grade position for saved coin
     *
     * @see https://stackoverflow.com/questions/8102221/php-multidimensional-array-searching-find-key-by-specific-value
     * @param Collected $collected
     * @return JsonResponse
     */
    public function getCoinRank(Collected $collected): JsonResponse
    {
        try{
            $coinByGrade = DB::table('collected')->select('id','grade')->orderBy('grade', 'desc')
                ->where('collected.user_id',auth()->user()->id)
                ->whereNotNull('collected.grade')
                ->where('collected.coin_id', $collected->coin_id)->get()->toArray();

            $coinCount = DB::table('collected')
                ->where('collected.user_id',auth()->user()->id)
                ->whereNotNull('collected.grade')
                ->where('collected.coin_id', $collected->coin_id)->get()->count();

            $key = array_search($collected->id, array_column($coinByGrade, 'id')) + 1;

            $locale = 'en_US';
            $nf = new NumberFormatter($locale, NumberFormatter::ORDINAL);
            $rank = $nf->format($key).' of '.$coinCount;

            return response()->json(['rank' => $rank, 'count' => $coinCount]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return response()->json(['error' => 'Could not load data']);
        }
    }
    public function getRanks($json) {
        $tmp_arr = json_decode($json, TRUE);
        arsort($tmp_arr);
        $uniq_vals = array_values(array_unique($tmp_arr)); // unique values indexed numerically from 0

        foreach ($tmp_arr as $k => $v) {
            $tmp_arr[$k] = array_search($v, $uniq_vals) + 1; //as rank will start with 1
        }
        return $tmp_arr;
    }


}
