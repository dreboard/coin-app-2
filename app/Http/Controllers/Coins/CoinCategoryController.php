<?php
/**
 * CoinCategoryController | actions for views/coins
 *
 * @package Coins
 * @subpackage Display
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Coins;

use App\Helpers\Types\TypeHelper;
use App\Http\Controllers\Controller;
use App\Models\Coins\CoinCategory;
use App\Models\Coins\Collected;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class CoinCategoryController extends Controller
{

    /**
     * Get category by ID
     *
     * @param int $cat_id
     * @return View|Factory|Application
     */
    public function viewCoinCategory(int $cat_id): View|Factory|Application
    {
        $category = CoinCategory::findOrFail($cat_id);
        $total_collected = Collected::where('user_id', auth()->user()->id)
            ->join('coins',function($q) use ($cat_id){
                $q->on('collected.coin_id','coins.id')
                ->where('coins.coincats_id',$cat_id);
            })
            ->get();
        $types_collected = $this->getCollectedByType($cat_id);
        //$years =
        return view('coins.view_category', [
            'category' => $category,
            'collected' => $total_collected,
            'types' => TypeHelper::COMBINED_LIST[$cat_id],
            'types_collected' => $types_collected,
        ]);
    }

    public function getCollectedByType(int $cat_id): array
    {
        $cat_list = [];
        foreach(TypeHelper::COMBINED_LIST[$cat_id] as $type => $value){
            $cat_list[$value]['collected'] = $this->getCollectedCountByType($type);
            $cat_list[$value]['investment'] = $this->getCollectedInvestmentByType($type);
        }
        return $cat_list;
    }

    public function getCollectedCountByType(int $type_id): int
    {
        return DB::table('collected')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->where('coins.cointypes_id',$type_id)
            ->get()->count();
    }

    public function getCollectedInvestmentByType(int $type_id)
    {
        return DB::table('collected')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->where('coins.cointypes_id',$type_id)
            ->sum('cost') ?? 0.00;
    }


    /**
     * Get array of coin types from catagory id
     *
     */
    public function populateCoinType(string $cat_id): JsonResponse
    {
        $types = TypeHelper::COMBINED_LIST[$cat_id];
        return response()->json(['coin_types' => $types]);
    }
}
