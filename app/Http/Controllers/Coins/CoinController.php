<?php
/**
 * CoinController | actions for views/coins
 *
 * @package Coins
 * @subpackage Display
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Coins;

use App\Actions\CollectedCountAction;
use App\Helpers\ErrorHelper;
use App\Helpers\Types\TypeHelper;
use App\Http\Controllers\Controller;
use App\Models\Coins\Coin;
use App\Models\Coins\CoinType;
use App\Models\Coins\Collected;
use App\Repositories\Coin\CoinVarietyRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CoinController extends Controller
{


    public function __construct(
        private CoinVarietyRepository $coinVarietyRepository,
        private CollectedCountAction  $collectedCountAction
    ){}

    /**
     * Show coins home page
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        $genres = DB::table('coins')
            ->select('genre')->distinct()
            ->orderBy('genre')
            ->get();
        $subGenres = DB::table('coins')
            ->select('subGenre')->distinct()
            ->orderBy('subGenre')
            ->get();
        //dd($misc_cat);
        return view('coins.index', [
            'categories' => TypeHelper::COIN_CATEGORIES,
            'types_list' => TypeHelper::COMBINED_LIST,
            'error_categories' => ErrorHelper::ERROR_CATEGORIES,
            'genres' => $genres,
            'subGenres' => $subGenres,
        ]);
    }
    /**
     * Show coins home page
     * @return Application|Factory|View
     */
    public function tags(): View|Factory|Application
    {

        $genres = DB::table('coins')
            ->select('genre')->distinct()
            ->where('genre', '<>', 'None')
            ->orderBy('genre')
            ->get();
        $subGenres = DB::table('coins')
            ->select('subGenre')->distinct()
            ->where('subGenre', '<>', 'None')
            ->orderBy('subGenre')
            ->get();
        $subGenre2s = DB::table('coins')
            ->select('subGenre2')->distinct()
            ->where('subGenre2', '<>', 'None')
            ->orderBy('subGenre2')
            ->get();
        $commemorativeCategorys = DB::table('coins')
            ->select('commemorativeCategory')->distinct()
            ->where('commemorativeCategory', '<>', 'None')
            ->orderBy('commemorativeCategory')
            ->get();
        $commemorativeTypes = DB::table('coins')
            ->select('commemorativeType')->distinct()
            ->where('commemorativeType', '<>', 'None')
            ->orderBy('commemorativeType')
            ->get();
        $states = DB::table('coins')
            ->select('state')->distinct()
            ->where('state', '<>', 'None')
            ->orderBy('state')
            ->get();
        //dd($misc_cat);
        return view('coins.tags', [
            'categories' => TypeHelper::COIN_CATEGORIES,
            'types_list' => TypeHelper::COMBINED_LIST,
            'genres' => $genres,
            'subGenres' => $subGenres,
            'subGenre2s' => $subGenre2s,
            'commemorativeCategorys' => $commemorativeCategorys,
            'commemorativeTypes' => $commemorativeTypes,
            'states' => $states,
        ]);
    }

    public function getGenre(string $genre)
    {
        $genre = str_replace('_', ' ', $genre);
        $coins_list = Coin::select('id', 'coinType', 'cointypes_id','coinName', 'coinYear')
            ->where(function($query) use ($genre) {
                $query->where('coins.genre',$genre)
                    ->orWhere('coins.subGenre',$genre)
                    ->orWhere('commemorativeType',$genre)
                    ->orWhere('commemorativeCategory',$genre)
                    ->orWhere('commemorativeGroup',$genre)
                    ->orWhere('coins.subGenre2',$genre);
            })->orderBy('coinYear')->orderBy('cointypes_id')->get();

        $coins = $this->collectedCountAction->getCollectedByCoin($coins_list);
        return view('coins.subset.view_genre', [
            'categories' => TypeHelper::COIN_CATEGORIES,
            'types_list' => TypeHelper::COMBINED_LIST,
            'coins' => $coins,
            'genre' => str_replace('_', ' ', $genre),
        ]);
    }

    /**
     * View a coin
     *
     * @param Coin $coin
     * @return View|Factory|Application
     */
    public function viewCoin(Coin $coin): View|Factory|Application
    {
        //$varieties_list = CoinVariety::select('variety','slug')->distinct()->where('coin_id', $coin->id)->orderBy('variety')->get();
        $varieties_list = $coin->varieties->unique('variety')->flatten()->pluck('variety');
        //dd($varieties_list);
        $collected = Collected::with('coin')->select('id', 'coin_id','grade','cost','tpg_service','nickname')
            ->where('user_id', auth()->user()->id)->withCount('coin')
            ->where('coin_id', $coin->id)
            ->get();
        $mintsets = DB::table('mintset')
            ->select('id', 'setName')
            ->whereRaw("find_in_set('".$coin->id."',coins)")
            ->get();
        $folders = DB::table('folders')
            ->select('id', 'folderName', 'folderCode')
            ->whereRaw("find_in_set('".$coin->id."',coins)")
            ->get();
        //dd($folders);
        return view('coins.view', [
            'collected' => $collected,
            'coin' => $coin,
            'mintsets' => $mintsets,
            'folders' => $folders,
            'sub_types' => $this->coinVarietyRepository->getSubTypesByCoin($coin->id),
            'varieties_list' => $varieties_list,
            'varieties' => $coin->load('varieties'),
            'color_cat' => in_array($coin->category->coinCategory, TypeHelper::COLOR_CATEGORIES) ? 1 : 0,
            'count' => count($collected->pluck('coin_count')),
            'cost' => $collected->pluck('cost')->sum(),
        ]);
    }


    /**
     * @param int $type_id
     * @return View|Factory|Application
     */
    public function viewCoinType(int $type_id): View|Factory|Application
    {
        $coinType = CoinType::findOrFail($type_id);
        $collected = Collected::where('user_id', auth()->user()->id)
            ->where('coin_id', $type_id)
            ->join('coins',function($q) use ($type_id){
                $q->on('coins.cointypes_id',$type_id);
            })
            ->get();

        $friends_votes = $coinType->with('coins:name,id')
            ->join('coins', 'coins.cointypes_id', '=', $type_id)
            ->get();


        $coins = Coin::select('id','coinName', 'coinYear', 'mintMark')->where('cointypes_id', $type_id)->orderBy('coinYear')->get()->toArray();
        return view('coins.view_type', [
            'collected' => $collected,
            'coin' => $coins,
        ]);
    }

    public function keyDatesList()
    {
        return view('coins.key_dates', [
            'categories' => TypeHelper::COIN_CATEGORIES,
            'types_list' => TypeHelper::COMBINED_LIST,
            'error_categories' => ErrorHelper::ERROR_CATEGORIES,
        ]);
    }

    public function typeKeyDatesList(int $type_id)
    {
        $cache_key = 'type_key_list_'.$type_id;
        $dates = Cache::get("$cache_key", function () use ($type_id) {
            return DB::table('coins')->select('id','coinName', 'coinYear', 'mintMark')
                ->where('cointypes_id', $type_id)->where('keyDate', 1)->orderBy('coinYear')
                ->get()->toArray() ?? [];
        });
        Log::info($dates);
        return response()->json(['key_dates' => $dates]);
    }

}
