<?php /** @noinspection ALL */

namespace App\Http\Controllers\Coins\Errors;

use App\Helpers\ErrorHelper;
use App\Http\Controllers\Controller;
use App\Models\Coins\Error;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

class ErrorController extends Controller
{


    public function index()
    {
        try{
            $categories = ErrorHelper::ERROR_CATEGORIES;
            $master_list = ErrorHelper::MASTER_LIST;



            return view('coins.errors.index', [
                'categories' => $categories,
                'master_list' => $master_list,
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load');
        }

    }



    /**
     * @param string $category
     * @return Factory|View|RedirectResponse|Application
     */
    public function viewCategory(string $category): Factory|View|RedirectResponse|Application
    {
        try{
            $master_list = ErrorHelper::MASTER_LIST[str_replace('_', ' ', $category)];
            $error_categories = ErrorHelper::ERROR_CATEGORIES;
            return view('coins.errors.category', [
                'error_categories' => $error_categories,
                'master_list' => $master_list,
                'category' => str_replace('_', ' ', $category),
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load');
        }
    }


    /**
     * @param string $subcategory
     * @return Factory|View|RedirectResponse|Application
     */
    public function viewSubCategory(string $subcategory): Factory|View|RedirectResponse|Application
    {
        try{
            $sub_info = DB::table('coins_errors')
                ->select( 'id','sub_category')
                ->where('sub_category', str_replace('_', ' ', $subcategory))
                ->where('type', 'None')
                ->get();

            $master_list = DB::table('coins_errors')
                ->where('sub_category', str_replace('_', ' ', $subcategory))
                ->orderBy('sub_category')
                ->get();
            //dd($master_list);
            $error_categories = ErrorHelper::ERROR_CATEGORIES;
            $forum_posts = DB::table('forums')
                ->select( 'id','title')
                ->where( 'forumable_type','App\Models\Coins\Error')
                ->where('forumable_id', $sub_info[0]->id)
                ->orderBy('created_at')
                ->get();
            //

            return view('coins.errors.sub_category', [
                'error_categories' => $error_categories,
                'master_list' => $master_list,
                'forum_posts' => $forum_posts,
                'subcategory' => str_replace('_', ' ', $subcategory),
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load');
        }
    }


    /**
     * @param Error $error
     * @return Factory|View|RedirectResponse|Application
     */
    public function viewError(Error $error): Factory|View|RedirectResponse|Application
    {
        try{
            $sub_list = DB::table('coins_errors')
                ->where('type', $error->type)
                ->orderBy('sub_category')
                ->get();

            if(array_key_exists($error->category, ErrorHelper::VARIETY_LIST)){
                $variety_list = ErrorHelper::VARIETY_LIST[$error->category];
            }

            $forum_posts = $error->load('forum');

            return view('coins.errors.view', [
                'error' => $error,
                'sub_list' => $sub_list,
                'forum_posts' => $forum_posts,
                'variety_list' => $variety_list ?? [],
                'error_categories' => ErrorHelper::ERROR_CATEGORIES,
                'subcategory' => str_replace('_', ' ', $error->type),
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load');
        }
    }


    public function getTypes(string $sub_category)
    {
        try{
            $error_list = DB::table('coins_errors')
                ->select( 'type')->distinct()
                ->where('sub_category', $sub_category)
                ->orderBy('sub_category')
                ->get();
            $full_list = [];
            foreach($error_list as $error){
                $full_list[$error]['error'] = $error; // A sub_category
                $full_list[$error]['sub_types'] = $this->getTypes($error);
            }
            return $full_list;
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load');
        }
    }

    public function findError(Request $request)
    {
        $request->validate([
            'find_error' => 'required|string'
        ]);
        try{
            $error_list = DB::table('coins_errors')
                ->where(function($query) use ($request) {
                    $query->where('category', 'LIKE', '%' . $request->input('find_error') . '%')
                        ->orWhere('sub_category', 'LIKE', '%' . $request->input('find_error') . '%')
                        ->orWhere('type', 'LIKE', '%' . $request->input('find_error') . '%')
                        ->orWhere('sub_type', 'LIKE', '%' . $request->input('find_error') . '%');
                })->get();
            //dd($error_list);
            return view('coins.errors.results', [
                'error_list' => $error_list,
                'search_term' => $request->input('find_error'),
            ]);

        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load');
        }
    }


    public function viewTypes($sub_category)
    {

    }

    public function viewSubTypes($sub_type)
    {

    }


}
