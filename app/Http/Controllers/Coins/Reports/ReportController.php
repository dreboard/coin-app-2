<?php
/**
 * ReportController | actions for all reporting
 *
 * @package Collected
 * @subpackage Coins
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Coins\Reports;

use App\Helpers\Types\TypeHelper;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{


    public function index()
    {
        $categories = TypeHelper::COIN_CATEGORIES;
        return view('collected.reports.index', [
            'categories' => $categories,
            'types_list' => TypeHelper::COMBINED_LIST
        ]);
    }
}
