<?php

namespace App\Http\Controllers\Coins;

use App\Actions\CollectedCountAction;
use App\Helpers\Types\TypeHelper;
use App\Http\Controllers\Controller;
use App\Models\Coins\Coin;
use App\Repositories\Coin\CoinVarietyRepository;
use App\Repositories\Coin\CollectedRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

class MetalController extends Controller
{


    /**
     * @var array|string[]
     * @replaces SELECT DISTINCT(`coinMetal`) FROM `coins`;
     */
    private array $metals = [
        'Silver',
        'Gold',
        'Platinum',
        'Palladium'
    ];

    public function __construct(
        private CoinVarietyRepository $coinVarietyRepository,
        private CollectedCountAction  $collectedCountAction,
        private CollectedRepository   $collectedRepository)
    {
    }

    public function viewType(string $type)
    {
        $type = str_replace(' ', '_', $type);
        $coins_list = Coin::select('id', 'coinType', 'cointypes_id', 'coinName', 'coinYear')
            ->where('coinMetal', $type)
            ->orderBy('coinYear')->orderBy('denomination')->get();

        $coins = $this->collectedCountAction->getCollectedByCoin($coins_list);
        return view('coins.metals.index', [
            'categories' => TypeHelper::COIN_CATEGORIES,
            'types_list' => TypeHelper::COMBINED_LIST,
            'coins' => $coins,
            'criteria' => $type,
        ]);
    }

    public function index()
    {

        $coins_list = DB::table('collected')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->whereIn('coins.coinMetal', $this->metals)
            ->orderBy('coinYear')->orderBy('denomination')->get();
        $coins = $this->collectedCountAction->getCollectedByCoin($coins_list);

        return view('coins.metals.index', [
            'categories' => TypeHelper::COIN_CATEGORIES,
            'types_list' => TypeHelper::COMBINED_LIST,
            'coins' => $coins,
            'metal_array' => $this->metals,
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getMetalChartInfo(Request $request): JsonResponse
    {
        $request->validate([
            'collected_id' => 'required|integer',
            'variety_id' => 'required|integer',
        ]);
        try {
            $metal_array = [];
            foreach ($this->metals as $metal) {
                $metal_array[$metal]['count'] = $this->countMetal($metal);
                $metal_array[$metal]['weight'] = $this->countMetal($metal);
                $metal_array[$metal]['investment'] = $this->countMetal($metal);
            }

            return response()->json(['metal_array' => $metal_array]);

        } catch (Throwable $e) {
            Log::error($e->getMessage());
            return response()->json(['error' => 'Could Not Update Coin']);
        }
    }

    public function countMetal(string $metal): int
    {
        return DB::table('collected')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('coins.coinMetal', $metal)
            ->get()->count();
    }

}
