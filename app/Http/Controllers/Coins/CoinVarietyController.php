<?php
/**
 * CoinVarietyController | actions for views/coins
 *
 * @package Coins
 * @subpackage Varieties
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Coins;

use App\Actions\CollectedCountAction;
use App\Helpers\ErrorHelper;
use App\Helpers\Types\TypeHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\CoinTrait;
use App\Models\Coins\Coin;
use App\Models\Coins\CoinVariety;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Throwable;

class CoinVarietyController extends Controller
{




    use CoinTrait;




    public function __construct(
        private readonly CollectedCountAction $collectedCountAction)
    {}


    /**
     * Show coins home page
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        $genres = DB::table('coins')
            ->select('genre')->distinct()
            ->orderBy('genre')
            ->get();
        $subGenres = DB::table('coins')
            ->select('subGenre')->distinct()
            ->orderBy('subGenre')
            ->get();

        return view('coins.variety.index', [
            'categories' => TypeHelper::COIN_CATEGORIES,
            'types_list' => TypeHelper::COMBINED_LIST,
            'error_categories' => ErrorHelper::ERROR_CATEGORIES,
            'genres' => $genres,
            'subGenres' => $subGenres,
        ]);
    }


    /**
     * Get variety
     *
     * @param CoinVariety $variety
     * @return View|Factory|Application
     */
    public function viewVariety(CoinVariety $variety): View|Factory|Application
    {

        // All broken dies label:list/grouping,type
        // 798 = Generic Die Variety
        //$variety = CoinVariety::findOrFail($variety->id);
        $coin = $variety->load('coin');
        $for_coin = CoinVariety::where('variety', $variety->variety)->where('coin_id', $variety->coin_id)->get();

        $collected = DB::table('collected_variety')
            ->join('collected', 'collected.id', '=', 'collected_variety.collected_id')
            ->join('users', 'users.id', '=', 'collected.user_id')
            ->where('variety_id', $variety->id)
            ->get();
        //
        return view('coins.variety.view', [
            'variety' => $variety,
            'coin' => $coin->coin,
            'collected' => $collected,
            'for_coin' => $for_coin,
        ]);
    }

    /**
     * Get variety by coin and category, Double Die Obverse for 123
     *
     * @param int $coin_id
     * @param string $variety
     * @return View|Factory|Application
     */
    public function viewVarietyListFor(int $coin_id, string $variety): View|Factory|Application
    {
        $coin = Coin::findOrFail($coin_id);
        $variety = str_replace('_', ' ', urldecode($variety));
        $variety_list = CoinVariety::where('variety', $variety)->where('coin_id', $coin->id)->get();


        //$err = CoinVariety::distinct('err_id')->where('variety', $variety)->where('coin_id', $coin->id)->value('err_id');
        //dd($err);
        $all_variety_list = CoinVariety::select('variety','slug','err_id')->distinct()->where('coin_id', $coin->id)->get();
        return view('coins.variety.view_list', [
            'variety' => $variety,
            'coin' => $coin,
            'collected' => $collected ?? [],
            'variety_list' => $variety_list,
            'all_variety_list' => $all_variety_list,
        ]);
    }

    public function attachVariety(Request $request)
    {
        $request->validate([
            'variety_id' => 'required|integer',
            'collected_id' => 'required|integer',
        ]);

        try{
            $forum = new Forum;
            $forum->user_id = auth()->user()->id;
            $forum->title = $request->input('title', $forum->title);
            $forum->slug = Str::of($request->input('title'))->slug('_');
            $forum->body = $request->input('body', $forum->body);
            $forum->private = $request->input('private');
            $forum->type = $request->input('type');
            $forum->notify = 1;
            $forum->save();
        } catch (Throwable $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Update Discussion');
        }
    }

    /**
     * Get varieties for coin
     *
     * @param Coin $coin
     * @return View|Factory|Application
     */
    public function viewVarietiesForCoin(Coin $coin): View|Factory|Application
    {
        $varieties = $coin->load('varieties');
        $types = CoinVariety::select('variety', 'slug')->distinct()->where('coin_id', $coin->id)->get();
        //dd($types);
        return view('coins.variety.by_coin', [
            'varieties' => $varieties,
            'coin' => $coin,
            'types' => $types,
        ]);
    }


}
