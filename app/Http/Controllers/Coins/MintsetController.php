<?php

namespace App\Http\Controllers\Coins;

use App\Http\Controllers\Controller;
use App\Models\Coins\Coin;
use App\Models\Coins\CollectedSet;
use App\Models\Coins\Mintset;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;

class MintsetController extends Controller
{


    /**
     * View Mintset
     *
     * @return View|Factory|Application
     */
    public function index(): View|Factory|Application
    {
        $mintSets = Mintset::select('id','setName', 'setType', 'coinYear')->orderBy('coinYear')->get();
        return view('coins.mintsets.index', [
            'mintSets' => $mintSets
        ]);
    }


    /**
     * View Mintset
     *
     * @param Mintset $mintset
     * @return View|Factory|Application
     */
    public function viewMintset(Mintset $mintset): View|Factory|Application
    {
        $coins = Coin::select('id','coinName', 'coinType')->whereIn('id', $mintset->coins()->pluck('id'))->orderBy('denomination')->get();//->toArray();
        $collected = CollectedSet::where('user_id', auth()->user()->id)->where('set_id', $mintset->id)
            ->get();
        return view('coins.mintsets.view_set', [
            'mintset' => $mintset,
            'collected' => $collected,
            'coins' => $coins,
        ]);
    }



    /**
     * Get array of years for mintsets
     *
     */
    public function loadTypeCoins(string $set_year): JsonResponse
    {
        $coins = Mintset::select('id','setName', 'setType')->where('coinYear', $set_year)->orderBy('coinYear')->get()->toArray();
        return response()->json(['sets' => $set_year]);
    }


    public function createById(int $id)
    {
        $mintset = Mintset::findOrFail($id);
        $recent = CollectedSet::where('user_id', auth()->user()->id)->where('id', $mintset->id)
            ->get();

        return view('coins.mintsets.create', [
            'recents' => $recent,
            'mintset' => $mintset,
        ]);
    }

}
