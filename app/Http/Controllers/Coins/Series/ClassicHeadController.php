<?php

namespace App\Http\Controllers\Coins\Series;

use App\Http\Controllers\Controller;

class ClassicHeadController extends Controller
{



    // 1796–1807
    const TYPES = [
        46 => 'Classic Head Half Cent', // 1809 to 1836
        87 => 'Classic Head Large Cent', //1808 until 1815
        68 => 'Classic Head Quarter Eagle', //1834 to 1839
        75 => 'Classic Head Half Eagle', //1834 to 1837
    ];
}
