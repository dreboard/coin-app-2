<?php

namespace App\Http\Controllers\Coins\Series;

use App\Http\Controllers\Controller;
use App\Models\Coins\Coin;
use App\Models\Coins\Collected;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

class CappedBustController extends Controller
{


    // 1796–1807
    // SELECT * FROM `coins` WHERE `cointypes_id` IN(26,43,111,52);
    const TYPES = [
        26 => 'Capped Bust Half Dime', // Capped Bust Half Dime
        43 => 'Capped Bust Dime', // Capped Bust Dime
        111 => 'Capped Bust Quarter',
        52 => 'Capped Bust Half Dollar',
    ];

    // SELECT DISTINCT(cv.type) from coins_variety cv INNER JOIN coins c ON c.id = cv.coin_id WHERE c.design = 'Liberty Cap' ORDER BY c.coinYear;
    const REFERENCES = [
        'Breen',
        'VarietyPlus',
        'PCGS',
        'Valentine',
        'NGC',
        'Fivaz-Stanton',
        'Wiley-Bugert',
        'None',
        'CONECA',
        'Cherrypickers',
    ];

    /**
     * Show coins home page
     * @return Application|Factory|View|RedirectResponse
     */
    public function index()
    {
        try{
            $results = DB::select( DB::raw("SELECT * from coins_variety cv INNER JOIN coins c ON c.id = cv.coin_id WHERE c.cointypes_id = 44 ORDER BY c.coinYear") );
            return view('coins.designs.capped.index', [
                'types' => self::TYPES,
                'references' => self::REFERENCES,
                'results' => $results,
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could load grades');
        }

    }



    /**
     * View a coin
     *
     * @param int|null $year
     * @return Application|Factory|View|RedirectResponse
     */
    public function viewYear(?int $year = null): View|Factory|RedirectResponse|Application
    {
        try{
            if($year > date('Y') || $year < 1793){ $year = date('Y');}
            if ($year === null) {
                $year = date('Y');
            }

            $coins = Coin::where('coinYear', $year)->where('design','Liberty Cap')->orderBy('denomination')->get();
            $rolls = [];
            $collected = Collected::select('collected.id','collected.grade','collected.cost','collected.tpg_service', 'collected.coin_id', 'coins.coinType', 'coins.cointypes_id',
                'collected.lot_id', 'collected.set_id', 'collected.roll_id', 'collected.folder_id', 'collected.firstday_id')
                ->join('coins', 'coins.id', '=', 'collected.coin_id')
                ->where('collected.user_id', auth()->user()->id)->withCount('coin')
                ->where('coins.coinYear', $year)->where('coins.design','Liberty Cap')
                ->get();
            return view('coins.designs.capped.year', [
                'year' => $year,
                'coins' => $coins,
                'rolls' => $rolls,
                'collected' => $collected,
                'count' => count($collected->pluck('coin_count')),
                'cost' => $collected->pluck('cost')->sum(),
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not process');
        }

    }



    public function getCoinCollectedCountForCoinYear($coin_id, $year): JsonResponse
    {
        $count = DB::table('collected')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->where('collected.coin_id', $coin_id)
            ->where('coins.coinYear', $year)->where('coins.design', '=','Liberty Cap')
            ->get()->count();
        return response()->json(['count' => $count]);
    }

}
