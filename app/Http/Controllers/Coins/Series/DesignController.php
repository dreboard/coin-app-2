<?php

namespace App\Http\Controllers\Coins\Series;

use App\Actions\CollectedCountAction;
use App\Helpers\Types\TypeHelper;
use App\Http\Controllers\Controller;
use App\Models\Coins\Coin;
use App\Models\Coins\CoinType;
use App\Repositories\Coin\CoinVarietyRepository;
use App\Repositories\Coin\CollectedRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Log;
use Throwable;

class DesignController extends Controller
{

    public function __construct(
        private CoinVarietyRepository $coinVarietyRepository,
        private CollectedCountAction $collectedCountAction,
        private CollectedRepository $collectedRepository)
    {}

    /**
     * @param string $design
     * @return Factory|View|RedirectResponse|Application
     */
    public function index(string $design): Factory|View|RedirectResponse|Application
    {
        try{
            $design = str_replace('_', ' ', $design);
            $coins_list = Coin::select('id', 'coinName', 'coinYear', 'design','cointypes_id')
                ->where('design', $design)->orderBy('release')->get();

            $coinType = CoinType::findOrFail($coins_list->pluck('cointypes_id')->unique()[0]);

            if(class_exists($coinType->helper()) ){
                $typeSpecificLinks = $coinType->helper()::typeSpecificLinks();
                $typeLinks = $coinType->helper()::typeLinks();
                $releaseList = $coinType->helper()::releaseList($coinType->id);
            }
            $designVarieties = $this->collectedRepository->getTypeDesignVarieties($coinType->id);
            $mintmarkStyles = TypeHelper::mintmarkLists($coinType);
            $business = $this->collectedRepository->getCollectedBusinessStrikeCountByDesign($design);
            $proofs = $this->collectedRepository->getCollectedProofStrikeCountByDesign($design);
            $coins = $this->collectedCountAction->getCollectedByCoin($coins_list);
            $strikes = Coin::select('strike')->distinct()
                ->where('design', $design)->get()->pluck('strike')->toArray();

            return view('coins.release.type', [
                'coinType' => $coinType,
                'year' => $coins_list->pluck('coinYear')->unique()[0],
                'coins' => $coins,
                'design_varieties' => $designVarieties,
                'mintmarkStyles' => $mintmarkStyles,
                'proofs' => $proofs,
                'strikes' => $strikes,
                'color_cat' => in_array($coinType->category->coinCategory, TypeHelper::COLOR_CATEGORIES) ? 1 : 0,
                'steps_type' => in_array($coinType->category->coinCategory, TypeHelper::COLOR_CATEGORIES) ? 1 : 0,
                'typeLinks' => $typeLinks ?? [],
                'typeSpecificLinks' => $typeSpecificLinks ?? [],
                'releaseList' => $releaseList ?? [],
                'business' => $business,
                'design' => $design,
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load');
        }
    }




    /**
     * Get array of coin types
     *
     * @todo change image url
     */
    public function loadRelatedDesigns(string $design): JsonResponse
    {
        try{
            $coins = Coin::select('id','coinName', 'coinYear', 'mintMark')->where('design', $design)->orderBy('coinYear')->get()->toArray();
            $image = 'http://cdn.dev-php.site/public/img/coins/'.str_replace(' ', '_', TypeHelper::COIN_TYPES[$type_id]).'.jpg';
            return response()->json(['coins' => $coins, 'image' => $image]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return response()->json(['error' => 'Could not load']);
        }
    }




}
