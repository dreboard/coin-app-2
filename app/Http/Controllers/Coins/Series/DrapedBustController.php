<?php

namespace App\Http\Controllers\Coins\Series;

use App\Http\Controllers\Controller;

class DrapedBustController extends Controller
{

    // 1796–1807
    const TYPES = [
        47 => 'Draped Bust Half Cent',
        86 => 'Draped Bust Large Cent',
        27 => 'Draped Bust Half Dime',
        7 => 'Draped Bust Dime',
        12 => 'Draped Bust Quarter',
        54 => 'Draped Bust Half Dollar',
        61 => 'Draped Bust Dollar',
    ];


}
