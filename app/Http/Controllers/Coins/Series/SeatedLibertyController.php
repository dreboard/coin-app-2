<?php

namespace App\Http\Controllers\Coins\Series;

use App\Http\Controllers\Controller;
use App\Models\Coins\Coin;
use App\Models\Coins\CoinType;
use App\Models\Coins\CoinVariety;
use App\Models\Coins\Collected;
use App\Models\Coins\Mintset;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

class SeatedLibertyController extends Controller
{

    const TYPES = [
        8 => 'Seated Liberty Half Dime',
        13 => 'Seated Liberty Dime',
        44 => 'Twenty Cent Piece',
        25 => 'Seated Liberty Quarter',
        57 => 'Seated Liberty Half Dollar',
        59 => 'Seated Liberty Dollar'
    ];

    // SELECT DISTINCT(cv.type) from coins_variety cv INNER JOIN coins c ON c.id = cv.coin_id WHERE c.design = 'Seated Liberty' ORDER BY c.coinYear;
    const REFERENCES = [
        'Breen',
        'VarietyPlus',
        'PCGS',
        'Valentine',
        'NGC',
        'Fivaz-Stanton',
        'Wiley-Bugert',
        'None',
        'CONECA',
        'Cherrypickers',
    ];

    /**
     * Show coins home page
     * @return Application|Factory|View|RedirectResponse
     */
    public function index()
    {
        try{
            $results = DB::select( DB::raw("SELECT * from coins_variety cv INNER JOIN coins c ON c.id = cv.coin_id WHERE c.cointypes_id = 44 ORDER BY c.coinYear") );
            return view('coins.designs.seated.index', [
                'types' => self::TYPES,
                'references' => self::REFERENCES,
                'results' => $results,
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could load grades');
        }

    }

    /**
     * Show coins home page
     * @param int $year
     * @return Application|Factory|View|RedirectResponse
     */
    public function seatedYear(int $year)
    {
        try{
            return view('coins.designs.seated.year', [
                'types' => self::TYPES
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could load grades');
        }

    }

    /**
     * Show coins home page
     * @param int $mint
     * @return Application|Factory|View|RedirectResponse
     */
    public function seatedMint(int $mint)
    {
        try{
            return view('coins.designs.seated.mint', [
                'types' => self::TYPES
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not process');
        }

    }


    /**
     * View a coin
     *
     * @param int|null $year
     * @return Application|Factory|View|RedirectResponse
     */
    public function viewYear(?int $year = null): View|Factory|RedirectResponse|Application
    {
        try{
            if($year > date('Y') || $year < 1793){ $year = date('Y');}
            if ($year === null) {
                $year = date('Y');
            }

            $coins = Coin::where('coinYear', $year)->where('design','Seated Liberty')->orderBy('denomination')->get();
            $rolls = [];
            $collected = Collected::select('collected.id','collected.grade','collected.cost','collected.tpg_service', 'collected.coin_id', 'coins.coinType', 'coins.cointypes_id',
                'collected.lot_id', 'collected.set_id', 'collected.roll_id', 'collected.folder_id', 'collected.firstday_id')
                ->join('coins', 'coins.id', '=', 'collected.coin_id')
                ->where('collected.user_id', auth()->user()->id)->withCount('coin')
                ->where('coins.coinYear', $year)->where('coins.design','Seated Liberty')
                ->get();
            return view('coins.designs.seated.year', [
                'year' => $year,
                'coins' => $coins,
                'rolls' => $rolls,
                'collected' => $collected,
                'count' => count($collected->pluck('coin_count')),
                'cost' => $collected->pluck('cost')->sum(),
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not process');
        }

    }

    /**
     * View a coin
     *
     * @param int $decade
     * @return View|Factory|Application
     */
    public function viewDecade(int $decade): View|Factory|Application
    {
        $coins = Coin::where('coinYear', $decade)->where('design','Seated Liberty')->orderBy('denomination')->get();
        $mintsets = Mintset::where('coinYear', $decade)->get();
        $rolls = [];
        $collected = Collected::select('collected.id','collected.grade','collected.cost','collected.tpg_service')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id', auth()->user()->id)->withCount('coin')
            ->where('coins.coinYear', $decade)
            ->get();
        return view('coins.dates.view', [
            'year' => $decade,
            'coins' => $coins,
            'mintsets' => $mintsets,
            'rolls' => $rolls,
            'collected' => $collected,
            'count' => count($collected->pluck('coin_count')),
            'cost' => $collected->pluck('cost')->sum(),
        ]);
    }




    public function getCoinCollectedCountForCoinYear($coin_id, $year): JsonResponse
    {
        $count = DB::table('collected')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->where('collected.coin_id', $coin_id)
            ->where('coins.coinYear', $year)->where('coins.design', '=','Seated Liberty')
            ->get()->count();
        return response()->json(['count' => $count]);
    }

    // -- Types ------------------------------------------------------------------------------------

    public function viewSeated(int $type): false
    {
        return match ($type) {
//            8 => $this->viewHalfDime($type),
//            13 => $this->viewDime($type),
//            25 => $this->viewQuarter($type),
//            44 => $this->viewTwenty($type),
//            57 => $this->viewHalf($type),
//            59 => $this->viewDollar($type),
            default => false //throw new \Exception('No match value'),
        };
    }


    /**
     * @param $type
     * @return Factory|View|RedirectResponse|Application
     */
    public function viewSeatedType($type): Factory|View|RedirectResponse|Application
    {
        try{
            $attributes = CoinVariety::select('coins.id','coins_variety.coin_id','coins_variety.label', 'coins_variety.variety', 'coins_variety.designation', 'coins_variety.type')
                ->join('coins', 'coins.id', '=', 'coins_variety.coin_id')
                ->where('coins.cointypes_id',$type)->orderBy('coins.coinYear')
                ->get();
            $references = $attributes->pluck('type')->unique()->toArray();
            $series = Coin::select('series')->distinct()
                ->where('cointypes_id', $type)->get()->pluck('series')->toArray();
            $coinType = CoinType::findOrFail($type);

            return view('coins.designs.seated.half_dollar', [
                'types' => self::TYPES,
                'attributes' => $attributes,
                'coinType' => $coinType,
                'references' => $references,
                'series' => $series,
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not process');
        }
    }


}
