<?php
/**
 * LincolnCentController | actions for all lincoln cent coins
 *
 * @package Coins
 * @subpackage Varieties
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Coins\Series;

use App\Actions\CollectedCountAction;
use App\Helpers\MintHelper;
use App\Http\Controllers\Controller;
use App\Models\Coins\Coin;
use App\Models\Coins\CoinType;
use App\Models\Coins\Collected;
use App\Models\Coins\Folder;
use App\Repositories\Coin\CoinVarietyRepository;
use App\Repositories\Coin\CollectedRepository;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

class LincolnCentController extends Controller
{

    // SELECT DISTINCT(cv.type) from coins_variety cv INNER JOIN coins c ON c.id = cv.coin_id WHERE c.design = 'Seated Liberty' ORDER BY c.coinYear;
    const REFERENCES = [
        'Breen',
        'VarietyPlus',
        'PCGS',
        'Valentine',
        'NGC',
        'Fivaz-Stanton',
        'Wiley-Bugert',
        'None',
        'CONECA',
        'Cherrypickers',
    ];

    const BIE_DESIGNATION = [
        'BIE','EIR','IIB','IIBIEIR','ILI','LII','Multiple','RIT','TIY','TYI'
    ];


    const TYPES = [
        2 => 'Lincoln Wheat',
        3 => 'Lincoln Memorial',
        6 => 'Lincoln Bicentennial',
        4 => 'Union Shield',
    ];

    const MINTS = [
        'Philadelphia',
        'San Francisco',
        'Denver',
        'West Point',
    ];


    public function __construct(
        private CoinVarietyRepository $coinVarietyRepository,
        private CollectedCountAction $collectedCountAction,
        private CollectedRepository $collectedRepository)
    {}

    /**
     * Show coins home page
     * @return Application|Factory|View|RedirectResponse
     */
    public function index()
    {
        try{
            //$folders = DB::select( DB::raw("SELECT * FROM `folders` where coinType LIKE '%Lincoln%' ORDER BY `folders`.`startYear` ASC ") );
            $folders = Folder::select('id','folderName','folderCode','startYear')->where('coinType', 'LIKE','%Lincoln%')->orderBy('startYear')->get();
            $designVarieties = $this->getDesignVarieties();
            $mintmarkStyles2 = Coin::select('mms')->distinct()
                ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
                ->get()->pluck('mms')->toArray();
            $mintmarkStyles = $this->mintmarkLists();

            return view('coins.designs.lincoln.index', [
                'types' => self::TYPES,
                'references' => self::REFERENCES,
                'folders' => $folders,
                'design_varieties' => $designVarieties,
                'mintmarkStyles' => $mintmarkStyles,
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could load grades');
        }

    }


    /**
     * View a coin
     *
     * @param int|null $year
     * @return Application|Factory|View|RedirectResponse
     */
    public function viewYear(?int $year = null): View|Factory|RedirectResponse|Application
    {
        try{
            if($year > date('Y') || $year < 1909){ $year = date('Y');}
            if ($year === null) {
                $year = date('Y');
            }
            $designVarieties = $this->getDesignVarietiesForYear($year);
            $sub_types = $this->getYearSubTypes($year);
            //dd($designVarieties);
            $mintmarkStyles = $this->mintmarkYearLists($year);
            $coins = Coin::where('coinYear', $year)->whereIn('cointypes_id', array_keys(self::TYPES))->orderBy('coinYear')->get();
            $rolls = [];
            $collected = Collected::select('collected.id','collected.grade','collected.cost','collected.tpg_service', 'collected.coin_id', 'coins.coinType', 'coins.cointypes_id',
                'collected.lot_id', 'collected.set_id', 'collected.roll_id', 'collected.folder_id', 'collected.firstday_id')
                ->join('coins', 'coins.id', '=', 'collected.coin_id')
                ->where('collected.user_id', auth()->user()->id)->withCount('coin')
                ->where('coins.coinYear', $year)->whereIn('coins.cointypes_id', array_keys(self::TYPES))
                ->get();
            return view('coins.designs.lincoln.year', [
                'year' => $year,
                'coins' => $coins,
                'rolls' => $rolls,
                'collected' => $collected,
                'sub_types' => $sub_types,
                'mintmarkStyles' => $mintmarkStyles,
                'design_varieties' => $designVarieties,
                'count' => count($collected->pluck('coin_count')),
                'cost' => $collected->pluck('cost')->sum(),
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not process');
        }

    }



    public function getCoinCollectedCountForCoinYear($coin_id, $year): JsonResponse
    {
        $count = DB::table('collected')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->where('collected.coin_id', $coin_id)
            ->where('coins.coinYear', $year)->whereIn('coins.cointypes_id', array_keys(self::TYPES))
            ->get()->count();
        return response()->json(['count' => $count]);
    }

    /**
     * View by Design Variety
     *
     * @param string $detail
     * @param string $variety
     * @return Factory|View|RedirectResponse|Application
     */
    public function viewCoinTypeDesignVariety(string $detail, string $variety): Factory|View|RedirectResponse|Application
    {
        try{
            $coins = $this->getCollectedByCoinDesignVariety($detail, urldecode($variety));
            $design_variety = str_replace('_', ' ', urldecode($variety));

            $variety_list = match ($detail) {
                'obverse' => Coin::select('obv')
                    ->whereIn('coins.cointypes_id', array_keys(self::TYPES))->get()->pluck('obv')->unique()->toArray(),
                'reverse' => Coin::select('rev')
                    ->whereIn('coins.cointypes_id', array_keys(self::TYPES))->get()->pluck('rev')->unique()->toArray(),
                default => [],
            };
            $variety_name = match ($detail) {
                'obverse' => 'Obverse Design Variety',
                'reverse' => 'Reverse Design Variety',
                default => [],
            };
            $strikes = Coin::select('mms')->distinct()
                ->whereIn('coins.cointypes_id', array_keys(self::TYPES))->get()->pluck('strike')->toArray();

            return view('coins.designs.lincoln.view_design_variety', [
                'variety' => urldecode($variety),
                'coins' => $coins,
                'detail' => $detail,
                'strikes' => $strikes,
                'variety_list' => $variety_list,
                'typeLinks' => $typeLinks ?? [],
                'variety_name' => $variety_name,
                'typeSpecificLinks' => $typeSpecificLinks ?? [],
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load');
        }

    }

    public function getDesignVarieties(): array
    {
        $design_variety_list = [];
        $design_variety_list['obverse_varieties'] = DB::table('coins_variety')
            ->select('coins_variety.label', 'coins_variety.variety', 'coins_variety.designation', 'coins_variety.type')->distinct()
            ->join('coins', 'coins.id', '=', 'coins_variety.coin_id')
            ->where('coins_variety.variety', 'Obverse Design Variety')
            ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
            ->get()->toArray();
        $design_variety_list['reverse_varieties'] = DB::table('coins_variety')
            ->select('coins_variety.label', 'coins_variety.variety', 'coins_variety.designation', 'coins_variety.type')->distinct()
            ->join('coins', 'coins.id', '=', 'coins_variety.coin_id')
            ->where('coins_variety.variety', 'Reverse Design Variety')
            ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
            ->get()->toArray();

        $design_variety_list['obverse_coneca'] = Coin::select('obv')->distinct()
            ->whereIn('coins.cointypes_id', array_keys(self::TYPES))->where('obv','<>', 'None')
            ->get()->pluck('obv')->toArray();
        $design_variety_list['reverse_coneca'] = Coin::select('rev')->distinct()
            ->whereIn('coins.cointypes_id', array_keys(self::TYPES))->where('rev','<>', 'None')
            ->get()->pluck('rev')->toArray();

        return $design_variety_list;
    }
    public function getDesignVarietiesForYear($year): array
    {
        $design_variety_list = [];
        $design_variety_list['obverse_varieties'] = DB::table('coins_variety')
            ->select('coins_variety.label', 'coins_variety.variety', 'coins_variety.designation', 'coins_variety.type')->distinct()
            ->join('coins', 'coins.id', '=', 'coins_variety.coin_id')
            ->where('coins_variety.variety', 'Obverse Design Variety')
            ->where('coins.coinYear', $year)
            ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
            ->get()->toArray();
        $design_variety_list['reverse_varieties'] = DB::table('coins_variety')
            ->select('coins_variety.label', 'coins_variety.variety', 'coins_variety.designation', 'coins_variety.type')->distinct()
            ->join('coins', 'coins.id', '=', 'coins_variety.coin_id')
            ->where('coins_variety.variety', 'Reverse Design Variety')
            ->where('coins.coinYear', $year)
            ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
            ->get()->toArray();



         $design_variety_list['obverse_coneca2'] = Coin::select('obv','obv2','obv3')->distinct()
             ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
             //->where('obv','<>', 'None')->where('obv2','<>', 'None')->where('obv3','<>', 'None')
             ->where('coins.coinYear', $year)
             ->get()->map->only(['obv','obv2','obv3'])->unique(['obv','obv2','obv3'])->flatten()->toArray();
        /* $design_variety_list['reverse_coneca2'] = Coin::select('rev','rev2','rev3')->distinct()
             ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
             ->where('rev','<>', 'None')->where('rev2','<>', 'None')//->where('rev3','<>', 'None')
             ->where('coins.coinYear', $year)
             ->get()->map->only(['rev','rev2','rev3'])->values()->flatten()->toArray();*/

        $design_variety_list['obverse_coneca'] = DB::select( DB::raw("SELECT DISTINCT concat( obv , ' ' , obv2 , ' ' , obv3 ) AS obverses from coins WHERE `cointypes_id` IN(2,3,4,6) AND coinYear = :coinYear"), array(
            'coinYear' => $year,
        ));
        $design_variety_list['reverse_coneca'] = DB::select( DB::raw("SELECT DISTINCT concat( rev , ' ' , rev2 , ' ' , rev3 ) AS reverses from coins WHERE `cointypes_id` IN(2,3,4,6) AND coinYear = :coinYear"), array(
            'coinYear' => $year,
        ));
        return $design_variety_list;
    }

    public function getYearSubTypes(int $year): array
    {
        return DB::table('coins_variety')->select('coins_variety.sub_type', 'coin_id')->distinct()
            ->join('coins', 'coins.id', '=', 'coins_variety.coin_id')
            ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
            ->where('coins.coinYear', $year)
            ->where('coins_variety.sub_type','<>', 'None')
            ->get()->toArray();
    }

    public function getYearCoinsWithSubtypes(int $year): array
    {
        return DB::table('coins_variety')->select('coins_variety.sub_type', 'coin_id')->distinct()
            ->join('coins', 'coins.id', '=', 'coins_variety.coin_id')
            ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
            ->where('coins.coinYear', $year)
            ->where('coins_variety.sub_type','<>', 'None')
            ->get()->toArray();
    }

    /**
     * Parse query for obverse OR reverse design
     *
     * @param $detail
     * @param $variety
     * @return array
     * @todo move to CollectedRepository $this->collectedRepository->
     */
    public function getCollectedByCoinDesignVariety($detail, $variety): array
    {
        $coins = match ($detail) {
            'obverse' => $this->getCollectedObverseDesignVariety($variety),
            'reverse' => $this->getCollectedReverseDesignVariety($variety),
            default => [],
        };
        return $coins;
    }


    public function getCollectedObverseDesignVariety(string $variety): array
    {
        $coins = Coin::select('id','coinName','strike','cointypes_id')
            ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
            ->where(function($query) use ($variety) {
                $query->where('coins.obv',$variety)
                    ->orWhere('coins.obv2',$variety)
                    ->orWhere('coins.obv3',$variety);
            })
            ->get();
        $cat_list = [];
        foreach($coins as $coin){
            $cat_list[$coin->id]['coin'] = $coin;
            $cat_list[$coin->id]['variety'] = 'obverse';
            $cat_list[$coin->id]['collected'] = DB::table('collected')
                ->where('collected.user_id',auth()->user()->id)
                ->where('collected.coin_id',$coin->id)->where('collected.obv', $variety)
                ->get()->count();
            $cat_list[$coin->id]['investment'] = DB::table('collected')
                ->where('collected.user_id',auth()->user()->id)
                ->where('collected.coin_id',$coin->id)->where('collected.obv', $variety)
                ->sum('cost') ?? 0.00;;
        }
        return $cat_list;
    }
    public function getCollectedReverseDesignVariety(string $variety): array
    {
        $coins = Coin::select('id','coinName','strike','cointypes_id')
            ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
            ->where(function($query) use ($variety) {
                $query->where('coins.rev',$variety)
                    ->orWhere('coins.rev2',$variety)
                    ->orWhere('coins.rev3',$variety);
            })
            ->get();
        $cat_list = [];
        foreach($coins as $coin){
            $cat_list[$coin->id]['coin'] = $coin;
            $cat_list[$coin->id]['variety'] = 'reverse';
            $cat_list[$coin->id]['collected'] = DB::table('collected')
                ->where('collected.user_id',auth()->user()->id)
                ->where('collected.coin_id',$coin->id)->where('collected.rev', $variety)
                ->get()->count();
            $cat_list[$coin->id]['investment'] = DB::table('collected')
                ->where('collected.user_id',auth()->user()->id)
                ->where('collected.coin_id',$coin->id)->where('collected.rev', $variety)
                ->sum('cost') ?? 0.00;;
        }
        return $cat_list;
    }
    public function getCollectedMintmarkVariety(string $mint, string $mms): array
    {
        $mms = str_replace('_', ' ', urldecode($mms));
        $mint = str_replace('_', ' ', urldecode($mint));
        $coins = Coin::select('id','coinName','strike', 'mint', 'mintMark','cointypes_id')
            ->where(function($query) use ($mms) {
                $query->where('coins.mms',$mms)
                    ->orWhere('coins.mms2',$mms)
                    ->orWhere('coins.mms3',$mms)
                    ->orWhere('coins.mms4',$mms);
            })
            ->where('coins.mint',$mint)
            ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
            ->get();
        $cat_list = [];
        foreach($coins as $coin){
            $cat_list[$coin->id]['coin'] = $coin;
            $cat_list[$coin->id]['variety'] = 'mintmark';
            $cat_list[$coin->id]['collected'] = DB::table('collected')
                ->where('collected.user_id',auth()->user()->id)
                ->where('collected.coin_id',$coin->id)->where('collected.mms', $mms)
                ->get()->count();
            $cat_list[$coin->id]['investment'] = DB::table('collected')
                ->where('collected.user_id',auth()->user()->id)
                ->where('collected.coin_id',$coin->id)->where('collected.mms', $mms)
                ->sum('cost') ?? 0.00;;
        }
        return $cat_list;
    }


    public static function mintmarkLists(): array
    {
        $mintList = [];
        foreach (self::MINTS as $mint){
            $mintList[$mint] = Coin::select('mms')->distinct()
                ->whereIn('coins.cointypes_id', array_keys(self::TYPES))->where('mint',trim($mint))
                ->get()->pluck('mms')->toArray();
        }
        return $mintList;
    }

    /**
     * @param int $year
     * @return array
     */
    public static function mintmarkYearLists(int $year): array
    {
        $mintList = [];
        $mintList['mints'] = DB::table('coins')->select('mint')->distinct()
            ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
            ->where('coins.coinYear', $year)
            ->get()->pluck('mint')->toArray();
        foreach ($mintList['mints'] as $mint){

            $mintList[$mint] = Coin::select('mms','mms2','mms3','mms4')->distinct()
                ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
                ->where('mint',trim($mint))
                ->where('coins.coinYear', $year)

                ->get()->map->only(['mms','mms2','mms3','mms4'])->unique(['mms','mms2','mms3','mms4'])->flatten()->toArray();

            /*$mintList[$mint] = Coin::select('mms')->distinct()
                ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
                ->where('mint',trim($mint))
                ->where('coinYear',trim($year))
                ->get()->pluck('mms')->toArray();*/
        }
        return $mintList;
    }


    /**
     * View type by Mintmark Style
     *
     * @param string $mint
     * @param string $mms
     * @return Factory|View|RedirectResponse|Application
     */
    public function viewCoinTypeMintMarkStyle(string $mint, string $mms): Factory|View|RedirectResponse|Application
    {
        try{
            $mintMarkStyle = str_replace('_', ' ', urldecode($mms));
            $mint = str_replace('_', ' ', urldecode($mint));

            $coins = $this->getCollectedMintmarkVariety($mint, $mintMarkStyle);


            $mm_styles = Coin::select('mms')->distinct()
                ->whereIn('coins.cointypes_id', array_keys(self::TYPES))->where('mint', $mint)
                ->get()->pluck('mms')->toArray();

            return view('coins.designs.lincoln.view_mms', [
                'mms' => $mintMarkStyle,
                'coins' => $coins,
                'mm_styles' => $mm_styles,
                'typeLinks' => $typeLinks ?? [],
                'mint' => $mint,
                'typeSpecificLinks' => $typeSpecificLinks ?? [],
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load');
        }

    }


    public function viewLincolnMetal(string $group)
    {

        try{
            $year_array = [];

            $year_array['coins'] = match ($group) {
                '1' => DB::table('coins')->select('id', 'coinName')
                    ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
                    ->where('coins.coinYear', '<', 1943)
                    ->get()->toArray(),
                '2' => DB::table('coins')->select('id', 'coinName')
                    ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
                    ->where('coins.coinYear', '=', 1943)
                    ->get()->toArray(),
                '3' => DB::table('coins')->select('id', 'coinName')
                    ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
                    ->whereIn('coins.coinYear', [1944,1945,1946])
                    ->get()->toArray(),
                '4' => DB::table('coins')->select('id', 'coinName')
                    ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
                    ->whereBetween('coins.coinYear', [1947,1962])
                    ->get()->toArray(),
                '5' => DB::table('coins')->select('id', 'coinName')
                    ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
                    ->whereBetween('coins.coinYear', [1963,1982])
                    ->get()->toArray(),
                '6' => DB::table('coins')->select('id', 'coinName')
                    ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
                    ->where('coins.coinYear', '>', 1982)
                    ->get()->toArray(),
                default => throw new Exception('Unexpected match value'),
            };

            $year_array['content'] = match($group){
                '1' => 'Bronze (95% Copper, 5% Tin and Zinc)',
                '2' => 'Zinc-Coated Steel',
                '3' => 'Gilding Metal (95% Copper, 5% Zinc)',
                '4' => 'Bronze (95% Copper, 5% Tin and Zinc)',
                '5' => 'Gilding Metal (95% Copper, 5% Zinc)',
                '6' => 'Copper-Plated Zinc (97.5% Zinc, 2.5% Copper)',
            };

            return view('coins.designs.lincoln.metal', [
                //'mms' => $mintMarkStyle,
                'coins' => $year_array['coins'],
                //'mm_styles' => $mm_styles,
                //'mint' => $mint,
                'content' => $year_array['content'],
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load');
        }
    }



    public function viewObverseDesignVariety($variety)
    {
        try{
            $series = str_replace('_', ' ', $variety);
            $coins = Coin::select('id','coinName','strike', 'mint', 'mintMark','cointypes_id')
                ->where(function($query) use ($variety) {
                    $query->where('coins.obv',$variety)
                        ->orWhere('coins.obv2',$variety)
                        ->orWhere('coins.obv3',$variety);
                })
                ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
                ->get();
            $variety_list = $this->collectedCountAction->getCollectedByCoin($coins);

            return view('coins.designs.lincoln.obv_designs', [
                'variety' => $variety,
                'coins' => $coins,
                'variety_list' => $variety_list,
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load');
        }
    }


    /**
     * View coin type by mint location
     *
     * @param $mint
     * @return Factory|View|RedirectResponse|Application
     */
    public function viewByMint($mint): Factory|View|RedirectResponse|Application
    {
        try{
            $mint = str_replace('_', ' ', $mint);

            $strikes = Coin::select('strike')->distinct()
                ->whereIn('coins.cointypes_id', array_keys(self::TYPES))->get()->pluck('strike')->toArray();
            $coins_list = Coin::select('id','coinName', 'coinYear', 'mintMark', 'strike')
                ->whereIn('coins.cointypes_id', array_keys(self::TYPES))->where('mint', $mint)
                ->orderBy('coinYear')->get();

            $designVarieties = $this->getLincolnDesignVarietiesByMint($mint);
            $mintmarkStyles = Coin::select('mms')->distinct()
                ->whereIn('coins.cointypes_id', array_keys(self::TYPES))->where('mint',trim($mint))
                ->where('mint',trim($mint))->where('mms','<>', 'None')
                ->get()->pluck('mms')->toArray();
            //dd($mintmarkStyles);
            $coins = $this->collectedCountAction->getCollectedByCoin($coins_list);

            return view('coins.designs.lincoln.view_mint', [
                'coins' => $coins,
                'mint' => str_replace('_', ' ', $mint),
                'mintmarkStyles' => $mintmarkStyles,
                'mintmark' => MintHelper::MINT_MARKS[$mint],
                'strikes' => $strikes,
                'designVarieties' => $designVarieties,
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load');
        }

    }



    public function getLincolnDesignVarietiesByMint(string $mint): array
    {
        $design_variety_list = [];
        $design_variety_list['obverse_varieties'] = DB::table('coins_variety')
            ->select('coins_variety.label', 'coins_variety.variety', 'coins_variety.designation', 'coins_variety.type')->distinct()
            ->join('coins', 'coins.id', '=', 'coins_variety.coin_id')
            ->where('coins_variety.variety', 'Obverse Design Variety')
            ->where('coins.mint',$mint)->whereIn('coins.cointypes_id', array_keys(self::TYPES))
            ->get()->toArray();
        $design_variety_list['reverse_varieties'] = DB::table('coins_variety')
            ->select('coins_variety.label', 'coins_variety.variety', 'coins_variety.designation', 'coins_variety.type')->distinct()
            ->join('coins', 'coins.id', '=', 'coins_variety.coin_id')
            ->where('coins_variety.variety', 'Reverse Design Variety')
            ->where('coins.mint',$mint)->whereIn('coins.cointypes_id', array_keys(self::TYPES))
            ->get()->toArray();

        $design_variety_list['obverse_coneca'] = Coin::select('obv')->distinct()
            ->where('mint',$mint)->where('obv','<>', 'None')->whereIn('coins.cointypes_id', array_keys(self::TYPES))
            ->get()->pluck('obv')->toArray();
        $design_variety_list['reverse_coneca'] = Coin::select('rev')->distinct()
            ->where('mint',$mint)->where('rev','<>', 'None')->whereIn('coins.cointypes_id', array_keys(self::TYPES))
            ->get()->pluck('rev')->toArray();

        return $design_variety_list;
    }


    public function bieList()
    {
        try{
            $bie_labels = DB::select( DB::raw("SELECT DISTINCT(`description`), `designation` FROM `coins_variety` WHERE `grouping` = 'Liberty Broken Die' ORDER BY `designation`") );

            return view('coins.designs.lincoln.view_bie', [
                'bie_labels' => $bie_labels,
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load');
        }
    }



    public function bieByType($variety)
    {
        try{
            $coins_list = DB::table('coins_variety')
                ->select( 'coins_variety.id AS variety_id','coins_variety.coin_id','coins.coinName','coins_variety.description','coins_variety.label', 'coins_variety.sub_type', 'coins_variety.grouping', 'coins_variety.variety', 'coins_variety.designation', 'coins_variety.type')
                ->join('coins', 'coins.id', '=', 'coins_variety.coin_id')
                ->where('coins_variety.grouping', 'Liberty Broken Die')
                ->where('coins_variety.description', $variety)
                ->whereIn('coins.cointypes_id', array_keys(self::TYPES))
                ->orderBy('coins.coinYear')
                ->get();
            //
            $bie_labels = DB::select( DB::raw("SELECT DISTINCT(`description`), `designation` FROM `coins_variety` WHERE `grouping` = 'Liberty Broken Die' ORDER BY LEFT(designation, 6) ASC; ") );
            $designation = $coins_list->pluck('designation')->unique()[0];
            //$coins = $this->collectedCountAction->getCollectedByCoin($coins_list);

            return view('coins.designs.lincoln.view_bie_type', [
                'coins_list' => $coins_list,
                'bie_labels' => $bie_labels,
                'designation' => $designation,
                'label' => $variety,
            ]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load');
        }
    }

    public function getCoinCollectedCountForBIE($coin_id, $year): JsonResponse
    {
        $count = DB::table('collected')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->where('collected.coin_id', $coin_id)
            ->where('coins.coinYear', $year)->whereIn('coins.cointypes_id', array_keys(self::TYPES))
            ->get()->count();
        return response()->json(['count' => $count]);
    }

    /**
     * Get related designs for other side of coin
     *
     * @todo Solve json problem in blade
     * @param Request $request
     * @return JsonResponse
     */
    public function getOtherDesigns(Request $request): JsonResponse
    {
        /*$request->validate([
            'design_variety' => Rule::in(['obverse', 'reverse'])
        ]);*/
        try{

            $variety_list = match ($request->input('current_design')) {
                'obverse' => DB::table('coins')->select('rev')->distinct()
                    ->where('obv', $request->input('design_variety'))
                    ->whereIn('coins.cointypes_id', array_keys(self::TYPES))->get()->pluck('rev')->unique(),
                'reverse' => DB::table('coins')->select('obv')->distinct()
                    ->where('rev', $request->input('design_variety'))
                    ->whereIn('coins.cointypes_id', array_keys(self::TYPES))->get()->pluck('obv')->unique(),
                default => [],
            };
            Log::info(dump($variety_list));
            return response()->json(['variety_list' => $variety_list]);
        }catch (Throwable $e){
            Log::error($e->getMessage());
            return response()->json(['error' => 'Could not load']);
        }

    }


}
