<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class BannedController extends Controller
{

    public function banned()
    {
        return view('auth.banned');
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     * sbeier@example.org
     */
    public function restoreBan(Request $request): RedirectResponse
    {

        $request->validate([
            'email' => ['required', 'string', 'email', 'max:255']

        ]);
        //$user = User::findOrFail(Auth::user()->id);
        $user = User::with('bans')->where('email', '=', $request->email)
            ->first();

        $text = $user->isBanned();
        //dd($user->bans[0]['expired_at']);
        //dd($this->displayHumanTimeLeft($user->bans[0]['expired_at']));
        $text .= $this->displayHumanTimeLeft($user->bans[0]['expired_at']).', expires on '.Carbon::parse($user->bans[0]['expired_at'])->format('M d Y, h:i:s');

        //$text = $this->checkIfBanned($user);

        return redirect()->route('banned')->with('status','GroupRequest Sent to '.$request->email.' bool:'.$text);

    }

    /**
     * @param $user
     * @return mixed
     */
    public function checkIfBanned($user)
    {
        $user = User::findOrFail($user->id);
        return $user->isBanned();
    }

    public function displayHumanTimeLeft($expires_at)
    {
        $now = Carbon::now();
        if ($now->diffInDays($expires_at) > 0) {
            return $now->diffInDays($expires_at) . Str::plural(' day', $now->diffInDays($expires_at)) . ' left';
        }
    }

}
