<?php
/**
 * UserActionsController | actions for views/users
 *
 * @package Users
 * @subpackage Manage
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;


class UserActionsController extends Controller
{

    /**
     * Show change password view
     * @return Application|Factory|View
     */
    public function changePassword(): View|Factory|Application
    {
        return view('users.settings.edit');
    }

    /**
     * Show change email view
     * @return Application|Factory|View
     */
    public function changeEmail(): View|Factory|Application
    {
        return view('users.settings.edit-mail');
    }

    /**
     * User save new password
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function savePassword(Request $request): RedirectResponse
    {
        $user = User::findOrFail(Auth::user()->id);
        $request->validate([
            'current_password' => 'required',
            'password' => 'required|same:confirm_password|min:6',
            'confirm_password' => 'required',
        ]);

        if (!Hash::check($request->input('current_password'), $user->password)) {
            return back()->withErrors(['current_password' => 'Current password is not valid']);
        }

        $user->password = Hash::make($request->password);
        $user->save();
        return redirect()->back()->with('status','password successfully updated');

    }

    /**
     * User edit current email
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function saveEmail(Request $request): RedirectResponse
    {

        $user = User::findOrFail(Auth::user()->id);
        $request->validate([
            'email' => 'required|same:confirm_email|string|email|unique:users',
            'confirm_email' => 'required',
        ]);

        $user->email = $request->email;
        $user->email_verified_at = null;
        $user->save();

        return redirect()->back()->with('status','email successfully updated');
    }

    /**
     * User edit current email
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function savePublicProfile(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => 'required|string',
            'youtube' => 'nullable|string',
            'public_message' => 'nullable|string',
            'ebay_id' => 'nullable|string',
            'ebay_store' => 'nullable|string',
            'ana' => 'nullable|string',
            'cac' => 'nullable|string',
            'png' => 'nullable|string',
            'pcgs' => 'nullable|string',
        ]);
        try{
            $user = User::findOrFail(Auth::user()->id);
            $user->name = $request->input('name');
            $user->youtube = $request->input('youtube', null);
            $user->public_message = $request->input('public_message', null);
            $user->pcgs = $request->input('pcgs', null);
            $user->ebay_id = $request->input('ebay_id', null);
            $user->ebay_store = $request->input('ebay_store', null);
            $user->ana = $request->input('ana', null);
            $user->cac = $request->input('cac', null);
            $user->png = $request->input('png', null);
            $user->profile_visibility = $request->input('profile_visibility', null);
            $user->user_type = $request->input('user_type', 'Collector');
            $user->user_level = $request->input('user_level', null);
            $user->specialty = $request->input('specialty', 'General Numismatics');
            $user->save();

            return redirect()->back()->with('success','Profile successfully updated');
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Save Profile');
        }

    }


    /**
     * Toggle user visibility
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function toggleVisibility(Request $request): JsonResponse
    {
        $int = $request->input('profile_visibility') == 0 ? 1 : 0;

        if($request->input('profile_visibility') == 0){
            $int = 1;
        } else {
            $int = 0;
        }
        if ($request->input('id') !== auth()->user()->id) {
            abort(401);
        }
        $user = User::find($request->input('id'));
        $user->profile_visibility = $int;
        $user->save();
        $user->refresh();
        return response()->json($int);
    }




    /**
     * Save User Avatar
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function saveUserAvatar(Request $request): JsonResponse
    {
        $request->validate([
            'image' => 'required|base64dimensions:min_width=100,min_height=100',
        ]);
        try{
            $user = User::findOrFail($request->input('user_id'));
            $image = $request->image;
            $imageInfo = explode(";base64,", $image);
            $imgExt = str_replace('data:image/', '', $imageInfo[0]);
            $image = str_replace(' ', '+', $imageInfo[1]);
            $imageName = time().".".$imgExt;

            //Storage::deleteDirectory('users/'.Auth::user()->id.'/profile');
            if($user->avatar !== null){
                Storage::delete($user->avatar);
            }
            $path = 'users/'.Auth::user()->id.'/profile/'.$imageName;

            Storage::put($path, base64_decode($image)); // WORKS
            $user->avatar = $path;
            $user->save();
            $user->refresh();
            return response()->json([
                'result' => Storage::exists($user->avatar),
                'url' => asset('storage/'.$user->avatar)
            ]);
        }catch (Exception $e){
            Log::error($e->getMessage());
            return response()->json(['result' => 'Error']);
        }
    }


    public function editUserAvatar()
    {
        return view('users.settings.edit_avatar');
    }


    /**
     * Delete a user account
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function deleteAccount(Request $request): RedirectResponse
    {
        if($request->input('user_id') === Auth::user()->id){

            Auth::user()->notifications()
                ->where('notifiable_id', Auth::user()->id)
                ->delete();
            Storage::deleteDirectory('users/'.Auth::user()->id);



            User::destroy($request->input('user_id'));
            return redirect()->route('logout')->withInput()->withErrors(['User Deleted']);
        }
        Log::error('User Account Could Not Be Deleted for User ID: '.Auth::user()->id);
        return back()->withErrors(['user_delete'=>'User Account Could Not Be Deleted']);

    }
}
