<?php
/**
 * UserViolationsController | actions for views/users
 *
 * @package Users
 * @subpackage Violations
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Users;

use App\Actions\UserViolationAction;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Users\Violation;
use App\Notifications\GeneralUserNotification;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserViolationsController extends Controller
{

    public const ViolationActions = [
        'ACTION_NONE' => 'No Action',
        'ACTION_PENDING' => 'Case Pending',
        'ACTION_WARNED' => 'User has been placed on the warning list',
        'ACTION_BANNED' => 'User has been banned',
    ];

    public function __construct(protected UserViolationAction $userViolationAction)
    {

    }

    /**
     * @param null $id
     * @return Factory|View|Application
     */
    public function violationCreate($id = null): Factory|View|Application
    {
        if($id){
            $user = User::findOrFail($id);
        }
        //$user = User::findOrFail($id) ?? [];
        return view('users.violations.create', ['user' => $user ?? []]);
    }


    /**
     * Save a new Violation
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function violationSave(Request $request): RedirectResponse
    {
        $validator = Validator::make($request->all(), [
            'violation_type' => 'required|string',
            'violator_id' => 'sometimes|required',
            'violator' => 'exclude_if:violator_id,true|sometimes|required|string',
            'details' => 'required|string',
            'url' => 'required|string',
            'screenshot' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        try{

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            $violation = new Violation();
            $violation->violation_type = $request->input('violation_type');
            $violation->details = $request->input('details');
            $violation->reporter = Auth::user()->id;
            $violation->url = $request->input('url');
            $violation->violator = $request->input('violator');
            $violation->violation_type = $request->input('violation_type');

            if ($request->has('screenshot')) {
                //Storage::delete($violation->screenshot);
                $violation->screenshot = Storage::putFile('violation/'.time(), $request->file('screenshot'));
            }
            if ($request->has('violator_id')){
                $user = User::find($request->input('violator_id'));
                $violation->violator = $user->name;
                $violation->violator_id = $request->input('violator_id');
            }
            $violation->save();

            auth()->user()->notify(new GeneralUserNotification([
                'body' => 'Your Violation report has been saved',
                'subject' => 'Your Violation report',
                'from' => 'The Administrator'
            ]));

            return redirect()->route('user.violation_view', ['id' => $violation->id])
                ->with('success', 'Violation Created Successfully');

        } catch (Exception $e){
            Log::error($e->getMessage());
            return Redirect::back()->withInput();
        }
    }


    /**
     * View a Violation
     *
     * @param int $id
     * @return \Application|\Factory|\RedirectResponse|\View
     */
    public function violationView(int $id): \Application|\Factory|\RedirectResponse|\View
    {
        $violation = Violation::find($id);
        $reporter = User::find($violation->reporter);
        if (!Gate::allows('view-violation', $violation)){
            return redirect()->route('user.dashboard');
        }
        if ($violation->violator_id){
            $violator = User::find($violation->violator_id);
        } else {
            $violator = [];
        }
        $user_list = User::where('email', 'LIKE', "%$violation->violator%")
            ->orWhere('name', 'LIKE', "%$violation->violator%")->get();

        return view('users.violations.view', [
            'violation' => $violation,
            'user_list' => $user_list,
            'reporter' => $reporter,
            'violator' => $violator
        ]);
    }

    /**
     * Update a Violation
     *
     * @param Request $request
     * @return \RedirectResponse
     */
    public function violationUpdate(Request $request): \RedirectResponse
    {
        $request->validate([
            'action' => 'required|string',
            'isMultiple' => 'sometimes|integer',
            'violator_id' => 'sometimes|integer',
        ]);
        $violation = Violation::find($request->input('violation_id'));
        $violation->action = $request->input('action');
        $violation->read_at = now();

        $reporter = User::find($request->input('reporter'));

        if ($request->has('violator_id')){
            $this->userViolationAction->handleViolator($request->input('violator_id'), $request->input('action'));
            $violation->violator_id = $request->input('violator_id');
        }
        $violation->save();
        $reporter->notify(new GeneralUserNotification([
            'body' => 'Your Violation report has been changed to '.$request->input('action'),
            'subject' => 'Your Violation Update',
            'from' => 'The Administrator'
        ]));

        return redirect()->action(
            [UserViolationsController::class, 'violationView'],
            ['id' => $violation->id]);
    }


    /**
     * Violations list Reported Against user
     *
     * @return Factory|View|Application
     */
    public function violationViewReportedAgainst(): Factory|View|Application
    {
        $violations = Violation::where('violator', auth()->user()->name)
            ->orWhere('violator_id', auth()->user()->id)->get();

        return view('users.violations.against', [
            'violations' => $violations
        ]);
    }

    /**
     * View all reported Violation
     *
     * @return \Application|\Factory|\View
     */
    public function violationViewReported(): \Application|\Factory|\View
    {
        $violations = Violation::where('reporter', auth()->user()->id)
            ->get();
        return view('users.violations.reported', [
            'violations' => $violations
        ]);
    }

}
