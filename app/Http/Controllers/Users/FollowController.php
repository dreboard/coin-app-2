<?php
/**
 * FollowController | actions for views/users/follow
 *
 * @package Users
 * @subpackage Manage
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @see https://github.com/overtrue/laravel-follow
 * @copyright none
 */
namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Groups\Group;
use App\Models\Project;
use App\Models\User;
use App\Notifications\GeneralUserNotification;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

/**
 *
 */
class FollowController extends Controller
{


    /**
     * Toggle follow a user
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function followUser(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'model_type' => 'required|string|in:user,project,group',
            'model_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            Log::error(print_r($validator->errors()->all()));
            return response()->json(['error'=> $validator->errors()->all()]);
        }

        try{
            $model = match($request->input('model_type')){
                'user' => User::findOrFail($request->input('model_id')),
                'project' => Project::findOrFail($request->input('model_id')),
                'group' => Group::findOrFail($request->input('model_id')),
            };

            auth()->user()->toggleFollow($model);

            return response()->json(['result' => 'Success']);

        }catch (Exception $e){
            Log::error($e->getMessage());
            return response()->json(['result' => 'Error']);
        }
    }


    /**
     * Users I follow
     *
     * @return Application|Factory|View|RedirectResponse
     */
    public function userIFollow()
    {
        try{
            $users = DB::table('followables')
                ->select('followable_id', 'users.name', 'users.id', 'users.user_type', 'users.specialty')
                ->where('followables.followable_type', 'App\Models\User')
                ->where('followables.user_id', auth()->user()->id)
                ->join('users', 'users.id', '=', 'followables.followable_id')
                ->get();

            return view('users.follow.users', [
                'users' => $users,
                'type' => 'followedByMe'
            ]);
        }catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load users');
        }
    }


    /**
     * Users following me
     *
     * @return Application|Factory|View|RedirectResponse
     */
    public function usersWhofollowMe()
    {
        try{
            $users = DB::table('followables')
                ->select('followable_id', 'users.name', 'users.id', 'users.specialty', 'users.user_type')
                ->where('followables.followable_type', 'App\Models\User')
                ->where('followables.followable_id', auth()->user()->id)
                ->join('users', 'users.id', '=', 'followables.user_id')
                ->get();

            return view('users.follow.follow_me', [
                'users' => $users,
                'type' => 'followingMe'
            ]);
        }catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load users');
        }
    }


    /**
     * Groups I follow
     *
     * @return Application|Factory|View|RedirectResponse
     */
    public function groupsIFollow()
    {
        try{
            $users = DB::table('followables')
                ->select('followable_id', 'groups.name', 'groups.id', 'groups.group_type', 'groups.specialty')
                ->where('followables.followable_type', 'App\Models\Groups\Group')
                ->where('followables.user_id', auth()->user()->id)
                ->join('groups', 'groups.id', '=', 'followables.followable_id')
                ->get();

            return view('users.follow.groups', [
                'groups' => $users,
                'type' => 'followedByMe'
            ]);
        }catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load groups');
        }
    }


    /**
     * Unfollow a model
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function unfollowModel(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'model_type' => 'required|string|in:App\Models\Groups\Group,App\Models\User,App\Models\Project',
            'model_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            Log::error(print_r($validator->errors()->all()));
            return response()->json(['error'=> $validator->errors()->all()]);
        }

        try{
            $model_type = match($request->input('model_type')){
                'user' => 'App\Models\User',
                'project' => 'App\Models\Project',
                'group' => 'App\Models\Groups\Group',
            };

           DB::table('followables')
                ->where('followables.followable_id', $request->input('model_id'))
                ->where('followables.followable_type', $model_type)
                ->where('followables.user_id', auth()->user()->id)
                ->delete();
            return response()->json(['result' => 'Success']);

        }catch (Exception $e){
            Log::error($e->getMessage());
            return response()->json(['result' => 'Error']);
        }
    }


    /**
     * Unfollow all models
     *
     * @return RedirectResponse
     */
    public function unfollowAll(Request $request): RedirectResponse
    {
        $request->validate([
            'model_type' => 'required|string|in:App\Models\Groups\Group,App\Models\User',
        ]);
        try{
            $users = DB::table('followables')
                ->where('followables.followable_type', $request->input('model_type'))
                ->where('followables.user_id', auth()->user()->id)
                ->delete();
            return redirect()->back()->with('success','All groups unfollowed');
        }catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not load users');
        }
    }

    /**
     * Remove all models following me
     *
     * @return RedirectResponse
     */
    public function removeAllFollowingUsers(Request $request): RedirectResponse
    {
        $request->validate([
            'model_type' => 'required|string|in:App\Models\Groups\Group,App\Models\User',
        ]);
        try{
            DB::table('followables')
                ->where('followables.followable_type', $request->input('model_type'))
                ->where('followables.followable_id', auth()->user()->id)
                ->delete();
            return redirect()->back()->with('success','All users removed');
        }catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not removed users');
        }
    }

    /**
     * Remove a model following me
     *
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public function removeFollowingUser(Request $request)
    {
        $request->validate([
            //'model_type' => 'required|string|in:App\Models\Groups\Group,App\Models\User',
            'model_id' => 'required|integer',
        ]);
        try{
            DB::table('followables')
                ->where('followables.followable_type', $request->input('model_type'))
                ->where('followables.user_id', $request->input('model_id'))
                ->where('followables.followable_id', auth()->user()->id)
                ->delete();

            return response()->json([
                'result' => 'Success',
            ]);
        }catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could not removed users');
        }
    }


    /**
     * Send message to all followers
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function contactFollowers(Request $request): JsonResponse
    {
        try{
            $followers = DB::table('followables')
                ->select('users.id')
                ->where('followables.followable_type', 'App\Models\User')
                ->where('followables.followable_id', auth()->user()->id)
                ->join('users', 'users.id', '=', 'followables.user_id')
                ->get();

            $data = [
                'subject' => $request->input('subject'),
                'body' => $request->input('body'),
                'from' => auth()->user()->name
            ];

            foreach ($followers as $user){
                $follower = User::findOrFail($user->id);
                $follower->notify(new GeneralUserNotification($data));
            }

            return response()->json([
                'result' => 'Success',
            ]);

        }catch (Exception $e){
            Log::error($e->getMessage());
            return response()->json([
                'result' => 'Error',
            ]);
        }
    }

}

