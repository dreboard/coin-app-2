<?php
/**
 * InviteController | actions for views/users/follow
 *
 * @package Users
 * @subpackage Manage
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @see https://github.com/overtrue/laravel-follow
 * @copyright none
 */
namespace App\Http\Controllers\Users;

use App\Helpers\ManageGroupHelper;
use App\Http\Controllers\Controller;
use App\Models\Groups\Group;
use App\Models\Project;
use App\Models\User;
use App\Notifications\GeneralUserNotification;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class InviteController extends Controller
{

    /**
     * Invite a model
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function inviteToGroup(Request $request): JsonResponse
    {
        try{

            $user = User::findOrFail($request->input('user_id'));
            $group = Group::findOrFail($request->input('group_id'));

            DB::table('group_user')->insert([
                'user_id' => $user->id,
                'group_id' => $group->id,
                'club_position' => 'Member',
                'member_type' => 'Full',
                'member_level' => 'Member',
                'manage' => ManageGroupHelper::MANAGE_LEVELS['Member Level'],
                'created_at' => now(),
                'updated_at' => now(),
                'approved_at' => now(),
            ]);

            $url = "<a href='".route('group.view', ['group' => $group->id])."'>View Club</a>";
            $data = [
                'subject' => "You are now part of the group ".$group->name,
                'body' => "Your have been placed in the group $group->name  Welcome to the club.
                Your my groups page will now list this group. $url",
                'from' => $group->name
            ];
            $user->notify(new GeneralUserNotification($data));

            return response()->json([
                'result' => 'Success',
            ]);

        }catch (Exception $e){
            Log::error($e->getMessage());
            return response()->json([
                'result' => 'Error',
            ]);
        }
    }

    /**
     * Invite a model
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function inviteToProject(Request $request): JsonResponse
    {
        $request->validate([
            'project' => 'required|integer',
            'user_id' => 'required|integer',
        ]);
        try{
            $project = Project::findOrFail($request->input('project'));
            $user = User::findOrFail($request->input('user_id'));


            DB::table('project_user')->insert([
                'user_id' => $user->id,
                'project_id' => $project->id,
                'project_level' => 'Full', // ['Full', 'Guest', 'Invited']
                'created_at' => now(),
                'updated_at' => now(),
                'approved_at' => now(),
            ]);

            $url = "<a href='".route('member.project_view', ['id' => $project->id])."'>View Project</a>";
            $data = [
                'subject' => 'Your club request is approved',
                'body' => "You have been added as a memmber to ".$project->name.".  Welcome to the project.
                Your my projects page will now list this ".$project->name.".. $url",
                'from' => $project->name
            ];
            $user->notify(new GeneralUserNotification($data));


            return response()->json([
                'result' => 'Success',
            ]);

        }catch (Exception $e){
            Log::error($e->getMessage());
            return response()->json([
                'result' => 'Error',
            ]);
        }
    }


    /**
     * Can the user approve project request
     *
     * @param Project $project
     * @return bool
     */
    public function canInviteUserToProject(Project $project): bool
    {
        if(auth()->user()->id == $project->user_id){
            return true;
        }
        return false;
    }


    /**
     * Can the user approve member
     *
     * @param Group $group
     * @return bool
     */
    public function canInviteUserToGroup(Group $group): bool
    {
        if(auth()->user()->userManageLevel($group->id) > ManageGroupHelper::MANAGE_LEVELS['Staff Level'] || auth()->user()->id == $group->user_id){
            return true;
        }
        return false;
    }
}
