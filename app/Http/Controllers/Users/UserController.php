<?php
/**
 * UserController | actions for views/users
 *
 * @package Users
 * @subpackage Manage
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Users;

use App\Helpers\ManageGroupHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\UserTrait;
use App\Models\Groups\Group;
use App\Models\Post;
use App\Models\User;
use App\Models\Users\Login;
use App\Models\Users\Warning;
use App\Notifications\GeneralUserNotification;
use App\Repositories\User\UserMessageRepository;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 *
 */
class UserController extends Controller
{

    use UserTrait;


    public function __construct(private UserMessageRepository $userMessageRepository)
    {

    }

    /**
     * View user intro page
     *
     * @return Application|Factory|View
     */
    public function viewIntroPage()
    {
        return view('users.intro.index');
    }

    /**
     * View user intro page
     *
     * @return Application|Factory|View
     */
    public function viewMyArea()
    {
        $featured = Post::with(['tags'])->where('user_id', auth()->user()->id)->where('featured', 1)
            ->first();
        //dd($featured->images);
        $group = Group::select('id', 'name')->where('user_id', auth()->user()->id)->first();
        $messageCount = $this->userMessageRepository->getUserMessageCount(auth()->user()->id);
        $systemMessageCount = $this->userMessageRepository->getUserSystemMessageCount(auth()->user()->id);
        return view('users.my_area',[
            'pageTitle' => 'My Dashboard',
            'messageCount' => $messageCount,
            'systemMessageCount' => $systemMessageCount,
            'featured' => $featured,
            'my_group' => $group,
        ]);
    }




    /**
     * View user private profile page
     *
     * @return Application|Factory|View
     */
    public function viewProfile(): View|Factory|Application
    {
        $last_login_date = Login::select('created_at')
            ->where('user_id', '=', auth()->user()->id)
            ->latest()
            ->offset(1)
            ->limit(1)->value('created_at');
        $last_login = $last_login_date ? date('D jS M Y, g:ia', strtotime($last_login_date)) : 'No Data';
        return view('users.settings.profile', ['last_login' => $last_login]);
    }

    /**
     * View user public profile page
     *
     * @param int $user_id
     * @return Application|Factory|View
     */
    public function viewPublicProfile(int $user_id): View|Factory|Application
    {
        $user = User::where('id', '=', $user_id)
            ->where('profile_visibility', 1)->first();

        //$social = $this->withMySocial();

        $invitedGroups = DB::table('group_user')
            ->select('groups.id', 'groups.name')
            ->where('group_user.user_id', auth()->user()->id)
            ->where('group_user.manage', '>', ManageGroupHelper::MANAGE_LEVELS['Staff Level'])
            ->join('groups', 'groups.id', '=', 'group_user.group_id')
            ->get();
        $invitedProjects = DB::table('project_user')
            ->select('projects.id', 'projects.title')
            ->where('project_user.user_id', auth()->user()->id)
            ->whereIn('project_user.project_level',  ['Manager', 'Associate'])
            ->join('projects', 'projects.id', '=', 'project_user.project_id')
            ->get();

        $groups = DB::table('group_user')
            ->select('groups.id', 'groups.name')
            ->where('group_user.user_id', $user_id)
            ->join('groups', 'groups.id', '=', 'group_user.group_id')
            ->get();
        return view('users.public', [
            'user' => $user,
            'groups' => $groups,
            'invitedProjects' => $invitedProjects,
            'invitedGroups' => $invitedGroups,
        ]);
    }

    /**
     * Load public profile form
     *
     * @return Application|Factory|View
     */
    public function editPublicProfile()
    {
        return view('users.settings.edit_profile');
    }

    /**
     * Save public profile details
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function savePublicProfile(Request $request): RedirectResponse
    {
        $request->validate([
            'ana' => 'required|string',
            'body' => 'required|string',
        ]);
        try{
            $user = User::findOrFail(auth()->user()->id);
            $user->ana = $request->input('ana', null);
            $user->png = $request->input('png', null);
            $user->cac = $request->input('cac', null);
            $user->pgcs = $request->input('pgcs', null);
            $user->ebay_id = $request->input('ebay_id', null);
            $user->ebay_store = $request->input('ebay_store', null);
            $user->youtube = $request->input('youtube', null);
            $user->public_message = $request->input('public_message', null);
            $user->save();
            $user->refresh();

            return redirect()->back()->with('success', 'Profile Updated');
        } catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Profile Not Updated');
        }
    }


    /**
     * Contact user
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function contact(Request $request): RedirectResponse
    {
        $request->validate([
            'subject' => 'required|string',
            'body' => 'required|string',
        ]);
        try{
            $user = User::findOrFail($request->input('user_id'));
            $data = [];
            $data['from'] = auth()->user()->id;
            $data['subject'] = $request->input('subject');
            $data['body'] = $request->input('body');
            $user->notify(new GeneralUserNotification($data));

            return redirect()->back()->with('success', 'Message Sent');
        } catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Messages Not Deleted');
        }
    }


    /**
     * Toggle follow a user
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function followUser(Request $request): JsonResponse
    {
        try{
            $user = User::findOrFail($request->input('user_id'));
            auth()->user()->toggleFollow($user);
            return response()->json([
                'result' => 'Success',
            ]);
        }catch (Exception $e){
            Log::error($e->getMessage());
            return response()->json([
                'result' => 'Error',
            ]);
        }
    }


    /**
     * Users I follow
     *
     * @return Application|Factory|View|RedirectResponse
     */
    public function followedUsers()
    {
        try{
            $users = DB::table('followables')
                ->select('followable_id', 'users.name', 'users.id')
                ->where('followables.followable_type', 'App\Models\User')
                ->where('followables.user_id', auth()->user()->id)
                ->join('users', 'users.id', '=', 'followables.followable_id')
                ->get();

            return view('users.settings.edit_profile', [
                'users' => $users,
            ]);
        }catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Messages Not Deleted');
        }
    }

    /**
     * View user directory page
     *
     * @return Application|Factory|View
     */
    public function viewDirectory()
    {
        $users = User::select('id', 'name', 'user_type')
            ->where('profile_visibility', '=', 1)
            ->get();

        return view('users.directory', ['users' => $users]);
    }

    /**
     * View user warning page
     *
     * @return Application|Factory|View
     */
    public function viewWarningPage(): View|Factory|Application
    {
        $warnings = Warning::where('user_id', '=', auth()->user()->id)
            ->count();
        return view('users.warning_page', ['warnings' => $warnings]);
    }


    public function search(string $search)
    {
        $users = User::select('id', 'name', 'specialty','user_type', 'specialty')
            ->where('name','LIKE',"$search%")->get();
        return view('users.results',[
            'users' => $users,
        ]);
    }

    public function formSearch(Request $request)
    {
        $request->validate([
            'searchTerm' => 'required|string',
        ]);
        $users = User::select('id', 'name', 'specialty','user_type', 'specialty')
            ->where('name','LIKE',"%{$request->input('searchTerm')}%")->get();
        return view('users.results',[
            'users' => $users,
        ]);
    }

    public function getUserType(string $type)
    {
        $users = User::select('id', 'name', 'specialty','user_type', 'specialty')
            ->where('user_type',$type)->get();
        return view('users.types',[
            'users' => $users,
            'type' => $type
        ]);
    }

}
