<?php
/**
 * DashController | actions for views/users
 *
 * @package Users
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Groups\Group;
use App\Models\Policy;
use App\Repositories\User\UserMessageRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;

class DashController extends Controller
{

    public function __construct(private UserMessageRepository $userMessageRepository)
    {

    }

    /**
     * Main dashboard view
     *
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        $group = Group::select('id')->where('user_id', auth()->user()->id)->first();
        $messageCount = $this->userMessageRepository->getUserMessageCount(auth()->user()->id);
        $systemMessageCount = $this->userMessageRepository->getUserSystemMessageCount(auth()->user()->id);
        return view('users.dashboard',[
            'pageTitle' => 'My Dashboard',
            'messageCount' => $messageCount,
            'systemMessageCount' => $systemMessageCount,
            'group' => $group,
        ]);
    }

    /**
     * New User Page
     *
     * @return Factory|View|Application
     */
    public function newUser(): Factory|View|Application
    {
        return view('users.intro.index');
    }


    /**
     * Main policy page
     * @return Factory|View|Application
     */
    public function viewPolicyPage(): Factory|View|Application
    {
        return view('users.policies');
    }

    /**
     * View a policy
     *
     * @param int $id
     * @return \Application|\Factory|\View
     * @todo all policies
     */
    public function viewPolicyTypePage(int $id): \Application|\Factory|\View
    {
        $policy_type = Policy::find($id);
        $policy_type2 = DB::table('policies')->where('id', '=', $id)->first();
        //dd($policy_type);
        return view('users.policy_type', [
            'policy_type' => $policy_type,
            'pageTitle' => $policy_type->title
        ]);
    }



}
