<?php
/**
 * WriterController | actions for views/members/writers
 *
 * @package Groups
 * @subpackage Writing
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Review;
use App\Models\Source;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WriterController extends Controller
{

    /**
     * View Writer intro page
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $posts = Post::with(['user' => function($query) {
            $query->where('created_at', '>=', Carbon::now()->subWeeks(1));
        }])->paginate(10);
        //dd($posts);
        return view('members.writers.index', [
            'posts' => $posts,
        ]);
    }

    /**
     * View reviews page
     *
     * @return Application|Factory|View
     */
    public function viewBooks()
    {
        $books = Source::where('publication_type', 'Book')->get();
        return view('members.writers.books', [
            'books' => $books,
        ]);
    }

    /**
     * View reviews page
     *
     * @return Application|Factory|View
     */
    public function bookReviews()
    {
        $books = Source::where('publication_type', 'Book')->get();
        return view('members.writers.reviews', [
            'books' => $books,
        ]);
    }

    /**
     * View reviews page
     *
     * @param int $id
     * @return Application|Factory|View|RedirectResponse
     */
    public function reviewBook(int $id)
    {
        $book = Source::with('reviews')->where('id', $id)->first();
        //dd(get_class($book));
        $reviewed_on = Review::where('reviewable_id', $id)
            ->where('user_id', auth()->user()->id)
            ->where('reviewable_type', get_class($book))->exists();
        if($reviewed_on){
            return redirect()->back()->with('error','You have already reviewed this.');
        }
        return view('members.writers.review_book', [
            'book' => $book,
            'reviewed_on' => $reviewed_on,
        ]);
    }

    /**
     * View reviews page
     *
     * @return Application|Factory|View
     */
    public function viewBook(int $id)
    {
        $book = Source::with('reviews')->where('id', $id)->first();
        //dd($book);
        return view('members.books.view', [
            'book' => $book,
        ]);
    }

    /**
     * View reviews page
     *
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     */
    public function saveBookReview(Request $request)
    {
        $request->validate([
            'comment' => 'required|string',
        ]);
        try {
            $source = Source::findOrFail($request->input('source_id'));
            $review = new Review();
            $review->user_id = auth()->user()->id;
            $review->comment = $request->input('comment');
            $review->score = $request->input('score') ?? 0;
            $review->reviewable_type = Source::class;
            $review->reviewable_id = $source->id;
            $review->save();
            $source->reviews()->save($review);

            return view('members.books.view', [
                'book' => Source::with('reviews')->where('id', $source->id)->first(),
            ])->with('status', 'Review Saved Successfully');

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Save Review');
        }
    }



}
