<?php
/**
 * PostController | actions for views/groups/group/posts
 *
 * @package Post
 * @subpackage Writing
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use App\Http\Traits\ImageTrait;
use App\Http\Traits\UserTrait;
use App\Models\Comment;
use App\Models\Post;
use App\Models\Tag;
use App\Models\User;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PostController extends Controller
{

    use UserTrait;

    use ImageTrait;

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $posts = Post::with(['tags'])->latest()->paginate(10);

        return view('members.posts.index', [
            'posts' => $posts,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function manage()
    {
        $posts = Post::with(['tags'])->where('user_id', auth()->user()->id)
            ->latest()->paginate(10);

        return view('members.posts.manage', [
            'posts' => $posts,
        ]);
    }

    /**
     * Display a my posts
     *
     * @return Application|Factory|View
     */
    public function myPosts()
    {
        $posts = Post::with(['tags'])->where('user_id', auth()->user()->id)
            ->latest()->paginate(10);

        return view('members.posts.manage', [
            'posts' => $posts,
        ]);
    }

    /**
     * Display a my posts
     *
     * @return Application|Factory|View
     */
    public function myUnpublishedPosts()
    {
        $posts = Post::with(['tags'])->where('user_id', auth()->user()->id)
            ->where('publish', 0)
            ->get();

        return view('members.posts.unpublished', [
            'posts' => $posts,
        ]);
    }

    /**
     * Display users posts
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function userPosts(int $id)
    {
        $user = User::findOrFail($id);
        $featured = Post::with(['tags'])->where('user_id', $id)->where('featured', 1)
            ->first();
        //dd($featured);
        $posts = Post::with(['tags'])->where('user_id', $id)
        ->latest()->paginate(10);

        return view('members.posts.user', [
            'posts' => $posts,
            'featured' => $featured,
            'user' => $user,
        ]);
    }

    /**
     * Get posts by tag
     *
     * @see https://laravel.com/docs/9.x/eloquent-relationships#inline-relationship-existence-queries
     * @param string $tag
     * @return Application|Factory|View
     */
    public function postsByTag(string $tag)
    {
        $tag = Tag::where('slug', $tag)
            ->value('name');

        $posts = Post::whereRelation(
            'tags', 'name', '=', $tag
        )->paginate(10);
        //dd($posts);
       return view('members.posts.tag', [
            'posts' => $posts,
            'tag' => $tag,
        ]);
    }

    /**
     * View a post
     *
     * @param Post $post
     * @return View|Factory|RedirectResponse|Application
     */
    public function view(Post $post): View|Factory|RedirectResponse|Application
    {
        $post = Post::withCount('comments')->with('comments.replies', 'tags', 'user')->where('id', $post->id)->first();
        $comments = Comment::with('replies')
            ->where('commentable_id','=',$post->id)
            ->where('commentable_type','=',Post::class)
            ->get();

        if ($post->user_id !== auth()->user()->id) {
            $post->views += 1;
            $post->save();
        }

        return view('members.posts.view', [
            'post' => $post,
            'comments' => $comments,
        ]);
    }


    /**
     * Show the form for creating a post
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $tags = Tag::all();
        $unpublished = Post::where('user_id', auth()->user()->id)
            ->get();

        return view('members.posts.create', [
            'tags' => $tags,
            'unpublished' => $unpublished,
        ]);
    }


    /**
     * Show the form for editing a post
     *
     * @param Post $post
     * @return Application|Factory|View
     */
    public function edit(Post $post)
    {
        $tags = Tag::all();
        $post = Post::with('tags', 'images')
            ->where('id', $post->id)->first();
        $saved_tags = $post->tags->pluck('id')->toArray();
        $imgCount = count($post->images);
        $unpublished = Post::where('publish', 0)
            ->where('user_id', auth()->user()->id)
            ->get();

        return view('members.posts.edit', [
            'tags' => $tags,
            'unpublished' => $unpublished,
            'post' => $post,
            'saved_tags' => $saved_tags,
            'imgCount' => $imgCount,
        ]);
    }

    /**
     * Set all post to featured = 0, There can only be 1
     *
     * @return void
     */
    public function resetFeatured()
    {
        Post::where('user_id', auth()->user()->id)
            ->update(['featured' => 0]);
    }

    /**
     * Store a newly created post.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'body' => 'required_unless:publish,0',
            'image_url.*' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        try {
            $post = new Post();
            $post->title = $request->input('title');
            $post->slug = Str::slug($request->input('title'));
            $post->image_url = 'Default.jpg';
            $post->body = $request->input('body');
            $post->publish = $request->input('publish', 0);
            $post->public = $request->input('public', 0);
            if ($request->has('featured')) {
                $this->resetFeatured();
                $post->featured = $request->input('featured');
            }

            $post->user_id = auth()->user()->id;
            $post->save();

         /*   if ($request->has('image_url')) {
                $folder = '/posts/' . $post->id;
                $post->image_url = $this->processPostImage($request, $post, $folder);
            }*/
            if ($request->has('image_url')) {
                $folder = '/posts/' . $post->id;
                foreach ($request->file('image_url') as $file) {
                    $post->images()->create([
                        'image_url' => Storage::putFile($folder, $file),
                        'user_id' => auth()->user()->id
                    ]);
                }
            }

            if ($request->has('tags')) {
                $post->tags()->attach($request->tags);
            }
            $post->save();

            return redirect()->route('member.view_post', ['post' => $post->id])
                ->with('status', 'Post Saved Successfully');
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Save Post');
        }

    }

    /**
     * Save post edit.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(Request $request): RedirectResponse
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'body' => 'required',
            'post_id' => 'required|integer',
            'image_url.*' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        try {
            $post = Post::with('tags')
                ->where('id', $request->input('post_id'))->first();

            if($post->user_id !== auth()->user()->id){
                return redirect()->back()->with('error', 'You Can Not Edit This Post');
            }

            $post->title = $request->input('title');
            $post->slug = Str::slug($request->input('title'));
            $post->body = $request->input('body');
            $post->publish = $request->input('publish');
            if ($request->has('featured')) {
                $this->resetFeatured();
                $post->featured = $request->input('featured');
            }
            if ($request->has('tags')) {
                $post->tags()->sync($request->tags);
            }
            $post->save();
            /*if ($request->has('image_url')) {
                $folder = '/posts/' . $post->id;
                $post->image_url = $this->processPostImage($request, $post, $folder);
            }*/

            if ($request->has('removeImage')) {
                $folder = '/posts/' . $post->id;
                foreach ($request->input('removeImage') as $file) {
                    $post->images()->create([
                        'image_url' => Storage::putFile($folder, $file),
                        'user_id' => auth()->user()->id
                    ]);
                }
            }

            if ($request->has('image_url')) {
                $folder = '/posts/' . $post->id;
                foreach ($request->file('image_url') as $file) {
                    $post->images()->create([
                        'image_url' => Storage::putFile($folder, $file),
                        'user_id' => auth()->user()->id
                    ]);
                }
            }

            return redirect()->route('member.view_post', ['post' => $post->id])
                ->with('status', 'Post Saved Successfully');
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Save Post');
        }
    }


    public function saveComment(Request $request): RedirectResponse
    {
        $request->validate([
            'comment_body' => 'required',
            'post_id' => 'required|integer',
        ]);
        try {
            $post = Post::findOrFail($request->input('post_id'));

            $comment = new Comment();

            $comment->body = $request->input('comment_body');
            $comment->user_id = auth()->user()->id;
            $post->comments()->save($comment);

            return redirect()->route('member.view_post', ['post' => $post->id])
                ->with('status', 'Comment Saved Successfully');
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Save Comment');
        }
    }


    public function saveCommentReply(Request $request): RedirectResponse
    {
        $request->validate([
            'comment_body' => 'required',
            'post_id' => 'required|integer',
            'comment_id' => 'required|integer',
        ]);
        try {
            $post = Post::findOrFail($request->input('post_id'));

            $reply = new Comment();
            $reply->body = $request->input('comment_body');
            $reply->parent_id = $request->input('comment_id');
            $reply->user_id = auth()->user()->id;
            $post->comments()->save($reply);

            return redirect()->route('member.view_post', ['post' => $post->id])
                ->with('status', 'Comment Saved Successfully');
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Save Comment');
        }
    }
}
