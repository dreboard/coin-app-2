<?php

namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use App\Models\Participant;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LeaveController extends Controller
{


    public function leaveForum(Request $request): JsonResponse
    {
        $request->validate([
            'forum_id' => 'required|integer',
            'user_id' => 'required|integer',
        ]);
        try{
            Participant::where('forum_id', $request->input('forum_id'))
            ->where('user_id', $request->input('user_id'))->delete();

            return response()->json([
                'result' => 'Success'
            ]);
        }catch (Exception $e){
            Log::error($e->getMessage());
            return response()->json([
                'result' => 'Error',
            ]);
        }

    }
}
