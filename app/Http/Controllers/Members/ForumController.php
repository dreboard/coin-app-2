<?php
/**
 * ForumController | actions for views/members/forum
 *
 * @package Post
 * @subpackage Writing
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */

namespace App\Http\Controllers\Members;

use App\Actions\GetModelAction;
use App\Helpers\Types\TypeHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\UserTrait;
use App\Models\Coins\Coin;
use App\Models\Coins\Error;
use App\Models\Comment;
use App\Models\Forum;
use App\Models\Participant;
use App\Models\Tag;
use App\Models\User;
use App\Notifications\ContactUserNotification;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Throwable;

class ForumController extends Controller
{

    use UserTrait;



    public function __construct(
        private GetModelAction $getModelAction)
    {}

    /**
     * View user intro page
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $categories = TypeHelper::COIN_CATEGORIES;
        $image = Storage::url('forum/default.jpg');
        $posts = Forum::with('user:id,name')->paginate(15);
        //dd($image);
        return view('members.forum.index', [
            'categories' => $categories,
            'posts' => $posts //DB::table('forums')->paginate(15)
        ]);
    }

    /**
     * View a discussion
     *
     * @param Forum $forum
     * @return View|Factory|RedirectResponse|Application
     */
    public function view(Forum $forum): View|Factory|RedirectResponse|Application
    {
        try{
            $forum->load('comments.replies', 'comments.replies.images', 'images', 'tags', 'user', 'participants:id,name');

            //dd($forum);

            $route = match($forum->forumable_type) {
                'App\Models\Forum' => '/forum/' . $forum->forumable_id,
                'App\Models\Post' =>  '/post/' . $forum->forumable_id,
                'App\Models\Project' =>  '/project/' . $forum->forumable_id,
                'App\Models\Comment' =>  '/comments/' . $forum->forumable_id,
                'App\Models\User' =>  '/users/' . $forum->forumable_id,
                'App\Models\Coins\Collected' =>  '/users/'.auth()->user()->id.'/collected/' . $forum->forumable_id,
                default => throw new Exception('No Related Image'),
            };

            if ($forum->type == 'Collaborative') {
                //$participants = $forum->load('participants:id,name'); //DB::table('participants')->where('forum_id', $forum->id)->get();
            }
            // update views and participants
            if ($forum->user->id !== auth()->user()->id) {
                $forum->views += 1;
                $forum->save();

                if (!$this->isParticipant($forum->id)) {
                    Participant::updateOrCreate(
                        ['user_id' => auth()->user()->id, 'forum_id' => $forum->id, 'last_read' => now()]
                    );
                }
            }

            return view('members.forum.view', [
                'forum' => $forum,
                'participants' => $participants ?? [],
            ]);
        } catch (Throwable $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Load Discussion');
        }
    }

    /**
     * View a discussions by model instance
     *
     * @param int $id
     * @param string $model
     * @return View|Factory|RedirectResponse|Application
     */
    public function viewById(int $id, string $model): View|Factory|RedirectResponse|Application
    {
        try{
            $model_info = $this->getModelAction->getModel($id, $model);
            $forum_posts = Forum::select( 'id','title')
                ->where( 'forumable_type',$model_info['type'])
                ->where('forumable_id', $id)
                ->orderBy('created_at')
                ->get();
            return view('members.forum.by_id', [
                'forum_posts' => $forum_posts,
                'model' => $model_info,
                'participants' => $participants ?? [],
            ]);
        } catch (Throwable $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Load Discussion');
        }
    }

    /**
     * View a discussions by model instance
     *
     * @param int $id
     * @return View|Factory|RedirectResponse|Application
     */
    public function viewByError(int $id): View|Factory|RedirectResponse|Application
    {
        try{
            $error = Error::findOrFail($id);
            $posts = $error->forum()->paginate(10);

            return view('members.forum.errors.by_id', [
                'error' => $error,
                'posts' => $posts ?? [],
            ]);
        } catch (Throwable $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Load Discussion');
        }
    }

    /**
     * View a discussions by model instance
     *
     * @param int $id
     * @return View|Factory|RedirectResponse|Application
     */
    public function viewByCoin(int $id): View|Factory|RedirectResponse|Application
    {
        try{
            $coin = Coin::findOrFail($id);
            $posts = $coin->forum()->paginate(10);

            return view('members.forum.coin.by_id', [
                'coin' => $coin,
                'posts' => $posts ?? [],
            ]);
        } catch (Throwable $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Load Discussion');
        }
    }


    public function create()
    {
        $followers = $this->myFollowers();
        $tags = Tag::all();
        return view('members.forum.create', [
            'followers' => $followers,
            'tags' => $tags,
        ]);
    }

    public function edit(Forum $forum)
    {
        $tags = Tag::all();
        $followers = $this->myFollowers();
        $forum->load('comments', 'tags', 'user');
        if ($forum->type == 'Collaborative') {
            $participants = $forum->load('participants:id,name'); //DB::table('participants')->where('forum_id', $forum->id)->get();
        }

        return view('members.forum.edit', [
            'followers' => $followers,
            'forum' => $forum,
            'tags' => $tags,
            'participants' => $participants ?? [],
        ]);
    }

    public function save(Request $request): RedirectResponse
    {
        $request->validate([
            'title' => 'required|string',
            'body' => 'required|string',
            'recipients' => 'sometimes|required|array',
            'image_url.*' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ]);

        try {
            //$recipients = count($request->input('recipients'));
            // covert to discussion
            $forum = new Forum;
            $forum->user_id = auth()->user()->id;
            $forum->title = $request->input('title', $forum->title);
            $forum->slug = Str::of($request->input('title'))->slug('_');
            $forum->body = $request->input('body', $forum->body);
            $forum->private = $request->input('private');
            $forum->type = $request->input('type');
            $forum->notify = 1;
            $forum->save();

            if ($request->has('image_url')) {
                $folder = '/forum/' . $forum->id;
                foreach ($request->file('image_url') as $file) {
                    $forum->images()->create([
                        'image_url' => Storage::putFile($folder, $file),
                        'user_id' => auth()->user()->id
                    ]);
                }
            }
            $data = [
                'from_id' => auth()->user()->id,
                'from' => auth()->user()->name,
                'subject' => $request->input('subject'),
                'body' => $request->input('body'),
                'forum_id' => $forum->id,
                'image_url' => $forum->image_url ?? null,
            ];

            if ($request->has('recipients')) {
                foreach ($request->input('recipients') as $recipient) {
                    $user = User::find($recipient);
                    Notification::send($user, new ContactUserNotification($data));
                    DB::table('participants')->insert([
                        ['forum_id' => $forum->id, 'user_id' => $user->id]
                    ]);
                }
                Notification::send(auth()->user(), new ContactUserNotification($data));
                DB::table('participants')->insert([
                    ['forum_id' => $forum->id, 'user_id' => auth()->user()->id]
                ]);
            }

            return redirect()->route('member.view_forum_post', ['forum' => $forum->id])
                ->with('status', 'Created Successfully');

        } catch (Throwable $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Update Discussion');
        }
    }

    public function update(Request $request): RedirectResponse
    {
        $request->validate([
            'forum_id' => 'required|integer',
            'title' => 'required|string',
            'body' => 'required|string',
            'recipients' => 'sometimes|required|array',
            'image_url.*' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            //[new ImageCount('App\Models\Forum',$request->input('forum_id'))],
        ]);

        try {
            $recipients = count($request->input('recipients'));
            // covert to discussion
            $forum = Forum::findOrFail($request->input('forum_id'));
            $forum->user_id = auth()->user()->id;
            $forum->title = $request->input('title', $forum->title);
            $forum->slug = Str::of($request->input('title'))->slug('_');
            $forum->body = $request->input('body', $forum->body);
            $forum->private = $request->input('private');
            $forum->type = $request->input('type');
            $forum->notify = 1;
            $forum->save();

            if ($request->has('image_url')) {
                $folder = '/forum/' . $forum->id;
                foreach ($request->file('image_url') as $file) {
                    $forum->images()->create([
                        'image_url' => Storage::putFile($folder, $file),
                        'user_id' => auth()->user()->id
                    ]);
                }
            }
            $data = [
                'from_id' => auth()->user()->id,
                'from' => auth()->user()->name,
                'subject' => $request->input('subject'),
                'body' => $request->input('body'),
                'forum_id' => $forum->id,
                'image_url' => $forum->image_url ?? null,
            ];

            if ($request->has('recipients')) {
                foreach ($request->input('recipients') as $recipient) {
                    $user = User::find($recipient);
                    Notification::send($user, new ContactUserNotification($data));
                    DB::table('participants')->insert([
                        ['forum_id' => $forum->id, 'user_id' => $user->id]
                    ]);
                }
            }

            Notification::send(auth()->user(), new ContactUserNotification($data));
            DB::table('participants')->insert([
                ['forum_id' => $forum->id, 'user_id' => auth()->user()->id]
            ]);
            return redirect()->back()->with('success', 'Discussion Updated');
        } catch (Throwable $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Update Discussion');
        }
    }


    public function getForumPostByUser($id)
    {
        try {
            $user = User::findOrFail($id, ['id', 'name']);
            $discussions = Forum::with('images')
                ->where('user_id', '=', $user->id)
                ->where('private', '=', 0)
                ->paginate(10);
            //dd($discussions);
            return view('members.forum.user', [
                'posts' => $discussions,
                'user' => $user,
            ]);
        } catch (Throwable $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Fins Discussions');
        }

    }

    public function createDisussion()
    {
        $users = $this->myFollowers();

        return view('members.forum.discussion', [
            'users' => $users,
        ]);
    }

    public function myDisussions()
    {
        $users = $this->myFollowers();
        $discussions = Forum::where('user_id', auth()->user()->id);
        return view('members.forum.discussion', [
            'users' => $users,
        ]);
    }

    public function saveComment(Request $request): RedirectResponse
    {
        $request->validate([
            'comment_body' => 'required',
            'forum_id' => 'required|integer',
            'image_url' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        try {
            $forum = Forum::findOrFail($request->input('forum_id'));
            $comment = new Comment();
            $comment->body = $request->input('comment_body');
            $comment->user_id = auth()->user()->id;
            $forum->comments()->save($comment);

            if ($request->has('image_url')) {
                $folder = '/comments/' . $comment->id;
                $comment->images()->create([
                    'image_url' => Storage::putFile($folder, $request->file('image_url'))
                ]);
            }
            return redirect()->route('member.view_forum_post', ['forum' => $forum->id])
                ->with('status', 'Comment Saved Successfully');
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Save Comment');
        }
    }


    public function saveCommentReply(Request $request): RedirectResponse
    {
        $request->validate([
            'comment_body' => 'required',
            'forum_id' => 'required|integer',
            'comment_id' => 'required|integer',
        ]);
        try {
            $forum = Forum::findOrFail($request->input('forum_id'));

            $reply = new Comment();
            $reply->body = $request->input('comment_body');
            $reply->parent_id = $request->input('comment_id');
            $reply->user_id = auth()->user()->id;
            $forum->comments()->save($reply);

            return redirect()->route('member.view_forum_post', ['forum' => $forum->id])
                ->with('status', 'Comment Saved Successfully');
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Save Comment');
        }
    }


}
