<?php
/**
 * ProjectController | actions for views/users/members/projects
 *
 * @package Users
 * @subpackage Activities
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use App\Models\Groups\Group;
use App\Models\Project;
use App\Models\User;
use App\Notifications\ProjectNotification;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ProjectController extends Controller
{


    /**
     * View project intro  page
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('members.projects.index');
    }

    /**
     * View create project page
     *
     * @return Application|Factory|View
     */
    public function intro()
    {
        return view('members.projects.intro');
    }


    /**
     * View create project page
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('members.projects.create');
    }

    /**
     * View create project page
     *
     * @return Application|Factory|View
     */
    public function createCoinProject($id)
    {
        $coins = DB::table('coins')
            ->select('id', 'coinName')
            ->where('cointypes_id', $id)
            ->orderBy('coinYear')
            ->get();
        //dd($coins);
        return view('members.projects.create', [
            'coins' => $coins
        ]);
    }

    /**
     * View create project page
     *
     * @param Project $project
     * @return Application|Factory|View
     */
    public function edit(Project $project)
    {
        return view('members.projects.create');
    }

    /**
     * Save created project
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Request $request): RedirectResponse
    {
        $request->validate([
            'title' => 'required|string',
            'description' => 'required|string',
            'type' => 'required|string',
        ]);
        try{
            $project = new Project();
            $project->title = $request->input('title');
            $project->description = $request->input('description');
            $project->overview = $request->input('overview');
            $project->type = $request->input('type');
            $project->description = $request->input('description');

            $project->save();

            return redirect()->back()->with('success', 'Project Created');
        } catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Project Not Created');
        }
    }

    /**
     * View all projects page
     *
     * @return Application|Factory|View
     */
    public function all(Project $project)
    {
        $projects = Project::where('user_id', auth()->user()->id)->get();
        return view('members.projects.view', ['projects' => $projects]);
    }

    /**
     * View project page
     *
     * @return Application|Factory|View
     */
    public function view(Project $project)
    {
        $project->load('user:id,name', 'associates:id,name');
        $is_associate = in_array(auth()->user()->id, $project->associates->pluck('id')->toArray());
        return view('members.projects.view', [
            'project' => $project,
            'is_associate' => $is_associate,
        ]);
    }

    /**
     * View project page
     *
     * @return Application|Factory|View
     */
    public function publicView(Project $project)
    {
        $project->load('user:id,name', 'associates:id,name');
        $is_associate = in_array(auth()->user()->id, $project->associates->pluck('id')->toArray());
        return view('members.projects.public', [
            'project' => $project,
            'is_associate' => $is_associate,
        ]);
    }





    /**
     * Merge project with a group
     *
     * @return Application|Factory|View
     */
    public function attachToGroup(Project $project, Group $group)
    {
        $project->load('user:id,name', 'associates:id,name');
        return view('members.projects.view', ['project' => $project]);
    }





    /**
     * Remove user OR yourself (remove_type) from project
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function removeUser(Request $request): RedirectResponse
    {
        $user = User::findOrFail($request->input('user_id'));
        $user->projects()->detach($request->input('project_id'));

        if ($request->input('remove_type') == 'project') {
            return redirect()->route('members.projects.view', ['project' => $request->input('project_id')])
                ->with('success', 'User Removed Successfully');
        }
        return redirect()->route('members.project_index');
    }



    public function contact(Request $request): RedirectResponse
    {
        $request->validate([
            'subject' => 'required|string',
            'body' => 'required|string',
        ]);
        try{
            $project = Project::findOrFail($request->input('project_id'));
            $data = [];
            $data['from'] = auth()->user()->id;
            $data['subject'] = $request->input('subject');
            $data['body'] = $request->input('body');
            $project->notify(new ProjectNotification($data, $project));

            return redirect()->route('members.project_view', ['project' => $project])
                ->with('success', 'Message Sent');
        } catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Messages Not Deleted');
        }
    }

    public function progress(Request $request): JsonResponse
    {
        $request->validate([
            'progress' => 'required|integer|digits_between:1,100',
            'project_id' => 'required|integer',
        ]);
        try{
            $project = Project::findOrFail($request->input('project_id'));
            $project->load('user:id,name', 'associates:id,name');

            $project->progress = $request->input('progress');
            $project->save();

            $data = [];
            $data['from'] = $project->user->name;
            $data['subject'] = "$project->title Progress Updates";
            $data['body'] = "$project->title has been updated to $request->input('progress')% ";
            $project->associates->notify(new ProjectNotification($data, $project));

            return response()->json(['result' => 'Success']);
        }catch (Exception $e){
            Log::error($e->getMessage());
            return response()->json(['result' => 'Error']);
        }
    }



    /**
     * Remove member from project
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function removeMember(Request $request): RedirectResponse
    {
        try{
            $project = Project::findOrFail($request->input('project_id'));

            if($project->user_id !== auth()->user()->id) {
                return redirect()->back()->with('error', 'You can not perform this action');
            }

            DB::table('project_user')->where('project_id', $request->input('project_id'))
                ->where('user_id', $request->input('user_id'))->delete();
            return redirect()->back()->with('success','Member removed');

        }catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error','Could remove member');
        }
    }

    /**
     * Delete project and all resources
     *
     * @todo delete notifications
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(Request $request): RedirectResponse
    {
        try{
            $project = Project::findOrFail($request->input('project_id'));
            $project->load('user:id,name', 'associates:id,name');

            if($project->user_id !== auth()->user()->id) {
                return redirect()->back()->with('error', 'You can not delete this project');
            }
            $id = $project->id;
            $title = $project->title;
            Storage::deleteDirectory('projects/'.$project->id);

            $this->notifyUsersThatProjectDeleted($project);
            DB::transaction(function () use ($request, $id, $project) {
                $project->delete();
                DB::delete('delete from `project_request` where `project_id` = '.$id);
                DB::delete('delete from `project_user` where `project_id` = '.$id);
                DB::delete('delete from `reports` where `reportable_type` = "App\Models\Project", `reportable_id` = '.$id);

            });
            return redirect()->route('members.projects.index')->with('status','Project Was Successfully Deleted');

        }catch (Exception $e){
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Project Was Not Deleted');
        }
    }

    /**
     * @throws Exception
     */
    public function notifyUsersThatProjectDeleted(Project $project)
    {
        try{
            $project = Project::findOrFail($project->id);
            $project->load('associates:id,name');

            $data = [];
            $data['from'] = $project->user->name;
            $data['subject'] = "$project->title Progress Has Been Deleted";
            $data['body'] = "$project->title has been deleted. Thank you all for your contributions.";
            $project->associates->notify(new ProjectNotification($data, $project));

        }catch (Exception $e){
            Log::error($e->getMessage());
            throw($e);
        }
    }

}
