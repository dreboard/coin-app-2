<?php
/**
 * ResearchController | actions for views/users
 *
 * @package Users
 * @subpackage Activities
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class ResearchController extends Controller
{



    /**
     * View Research intro page
     *
     * @return Application|Factory|View
     */
    public function index()
    {

        $new_projects = Project::where('private', 0)
            ->orderBy('created_at', 'desc')->limit(2)->get();
        $all_projects = Project::with(['user:id,name'])->select('id', 'title', 'user_id')->where('private', 0)->get();
        //dd($all_projects);
        return view('members.research.index', [
            'new_projects' => $new_projects,
            'all_projects' => $all_projects
        ]);
    }

}
