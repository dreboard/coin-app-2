<?php
/**
 * ImageController | model for images
 *
 * @package Images
 * @subpackage All
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @see https://github.com/overtrue/laravel-follow
 * @copyright none
 */
namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use App\Http\Traits\ImageTrait;
use App\Http\Traits\UserTrait;
use App\Models\Comment;
use App\Models\Forum;
use App\Models\Image;
use App\Models\Post;
use App\Models\Project;
use App\Models\User;
use App\Rules\ImageCount;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Throwable;

class ImageController extends Controller
{

    use UserTrait;

    use ImageTrait;


    public function viewGallery(Model $model){
        $model_relation = Image::query()
            ->whereMophedTo('imagable', $model)
            ->get();
    }


    /**
     * @param Image $image
     * @return Factory|View|Application
     * @throws Exception
     */
    public function viewImage(Image $image): Factory|View|Application
    {
        //dd($image);
        //$image = Image::findOrFail($image->id);
        $route = $this->getImageUrl($image);
        return view('members.images.view', [
            'image' => $image,
            'route' => $route,
        ]);
    }


    public function viewImages(string $model, int $id){
        if(class_exists("App\Models\\".ucfirst($model))){
            $class = "App\Models\\".ucfirst($model);
        } else {
            $class = "App\Models\Coins\\".ucfirst($model);
        }

        Log::info(class_exists("App\Models\Coins\\".ucfirst($model)));
        Log::info(class_exists("App\Models\\".ucfirst($model)));
        $related_model = $class::with('images', 'images.comments')->where('id', $id)->get();
        $route = $this->getModelUrl($model, $id);

        return view('members.images.view_all', [
            'images' => $related_model[0],
            'route' => $route,
        ]);
    }



    public function update(Request $request)
    {
        $image_imput = intval($request->input('image_id'));
        $image = Image::findOrFail($image_imput);

        $request->validate([
            'image_url.*' => ['sometimes,required,image,mimes:jpeg,png,jpg,gif,svg,max:2048', new ImageCount($image->imageable_type, $image->id)],
            'title' => 'sometimes|required|string|max:255',
        ]);

        try {

            $folder = match($image->imageable_type) {
                'App\Models\Forum' => '/forum/' . $image->imageable_id,
                'App\Models\Post' =>  '/post/' . $image->imageable_id,
                'App\Models\Project' =>  '/project/' . $image->imageable_id,
                'App\Models\Comment' =>  '/comments/' . $image->imageable_id,
                'App\Models\User' =>  '/users/' . $image->imageable_id,
                'App\Models\Coins\Collected' =>  '/users/'.auth()->user()->id.'/collected/' . $image->imageable_id,
                default => throw new Exception('No Related Image'),
            };

            $image->title = $request->input('title', null);

            if ($request->has('image_url')) {
                Storage::delete($image->image_url);
                $image->image_url = Storage::putFile($folder, $request->file('image_url'));
            }

            $image->save();
            $image->refresh();

            return redirect($this->getImageUrl( $image))->with('success', 'Image Updated');
        } catch (Throwable $e) {
            Log::error($e->getMessage(), ['id' => $image->id]);
            return redirect($this->getImageUrl( $image))->with('error', 'Could Not Update Image');
        }
    }

    public function delete(Request $request)
    {
        $request->validate([
            'image_id' => 'required|integer',
            'image_model_id' => 'required|integer',
            'image_model' => 'required|string',
        ]);
        try {
            $model = match($request->input('image_model')) {
                'App\Models\Forum' => Forum::findOrFail($request->input('image_model_id')),
                'App\Models\Post' =>  Post::findOrFail($request->input('image_model_id')),
                'App\Models\Project' =>  Project::findOrFail($request->input('image_model_id')),
                'App\Models\Comment' =>  Comment::findOrFail($request->input('image_model_id')),
                'App\Models\User' =>  User::findOrFail($request->input('image_model_id')),
                default => throw new Exception('No Related Image'),
            };
            //Log::info($model->images);
            //return response()->json(['success' => 'Image Deleted', 'image_count' => $model->images()->count()]);


            $image = Image::findOrFail($request->input('image_id'));
            if(Storage::exists($image->image_url)){
                Storage::delete($image->image_url);
            }

            $route = $this->getImageUrl($image);
            $model->images()->where('id',$request->input('image_id'))->delete();
            $model->refresh();
            $image_count = $model->images()->count();


            if( $request->ajax() ) {
                return response()->json(['success' => 'Image Deleted', 'image_count' => $image_count]);
            }

            return redirect($route)->with('success', 'Image Deleted');
        } catch (Throwable $e) {
            Log::error($e->getMessage());
            if( request()->ajax() ) {
                return response()->json(['error' => 'Could Not Delete Image']);
            }
            return redirect()->back()->with('error', 'Could Not Delete Image');
        }
    }


    public function deleteAll(Request $request)
    {
        $request->validate([
            'image_model_id' => 'required|integer',
            'image_model' => 'required|string',
        ]);
        try {
            $model = match($request->input('image_model')) {
                'App\Models\Forum' => Forum::findOrFail($request->input('image_model_id')),
                'App\Models\Post' =>  Post::findOrFail($request->input('image_model_id')),
                'App\Models\Project' =>  Project::findOrFail($request->input('image_model_id')),
                'App\Models\Comment' =>  Comment::findOrFail($request->input('image_model_id')),
                'App\Models\User' =>  User::findOrFail($request->input('image_model_id')),
                default => throw new Exception('No Related Image'),
            };

            $image = Image::findOrFail($request->input('image_id'));

            if(Storage::exists($image->image_url)){
                Storage::delete($image->image_url);
            }

            $route = $this->getImageUrl($image);
            $model->images()->where('id',$request->input('image_id'))->delete();
            $model->refresh();
            $image_count = $model->images()->count();


            if( $request->ajax() ) {
                return response()->json(['success' => 'Image Deleted', 'image_count' => $image_count]);
            }

            return redirect($route)->with('success', 'Image Deleted');
        } catch (Throwable $e) {
            Log::error($e->getMessage());
            if( $request->ajax() ) {
                return response()->json(['error' => 'Could Not Delete Image']);
            }
            return redirect()->back()->with('error', 'Could Not Delete Image');
        }
    }

    public function saveComment(Request $request): RedirectResponse
    {
        $request->validate([
            'comment_body' => 'required',
            'image_id' => 'required|integer',
        ]);
        try {
            $image = Image::findOrFail($request->input('image_id'));
            $comment = new Comment();

            $comment->body = $request->input('comment_body');
            $comment->user_id = auth()->user()->id;
            $image->comments()->save($comment);

            return redirect()->route('member.view_image', ['image' => $image->id])
                ->with('status', 'Comment Saved Successfully');
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Could Not Save Comment');
        }
    }


}
