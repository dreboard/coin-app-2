<?php

namespace App\Listeners\User;

use App\Events\User\WarnUser;
use App\Models\Users\Warning;

class ProcessWarningListener
{

    /**
     * Handle the event.
     *
     * @param WarnUser $event
     * @return void
     */
    public function handle(WarnUser $event)
    {
        Warning::create([
            'user_id' => $event->user->id,
            'created_at' => now()
        ]);
        $event->user->status = 'warn';
        $event->user->profile_visibility = 0;

        $event->user->save();
        $event->user->refresh();
    }
}
