<?php

namespace App\Listeners\User;

use App\Events\User\WarnUser;
use App\Notifications\WarnUserNotification;

class WarningNotificationListener
{

    private array $data = [
            'subject' => 'Your account is now in warning status',
            'body' => 'You have been issued a warning for violating the policies of this website.',
            'from' => 'The Administrator'
        ];

    /**
     * Handle the event.
     *
     * @param WarnUser $event
     * @return void
     */
    public function handle(WarnUser $event)
    {
        $event->user->notify(new WarnUserNotification($this->data));
    }
}
