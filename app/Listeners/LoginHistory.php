<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class LoginHistory
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Save user login to the login_history table
     *
     * @param Login $event
     * @return Login
     */
    public function handle(Login $event): Login
    {
        DB::table('logins')->insert([
            'user_id' => $event->user->id,
            'ip_address' => $this->getClientIPaddress(),
            'created_at' => now(),
            'login_time' => time()
        ]);
        return $event;
    }

    /**
     * Get user IP address
     * @return mixed
     */
    private function getClientIPaddress(): mixed
    {

        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }
        $client = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote = $_SERVER['REMOTE_ADDR'] ??' 0.0.0.0';

        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $clientIp = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $clientIp = $forward;
        } else {
            $clientIp = $remote;
        }

        return $clientIp;
    }
}
