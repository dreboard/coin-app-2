<?php

namespace App\Listeners\Banned;

use App\Events\User\UnbanUser;
use Exception;
use Illuminate\Support\Facades\Log;

class RestoreBannedUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UnbanUser $event
     * @return void
     * @throws Exception
     */
    public function handle(UnbanUser $event)
    {
        try{
            $event->user->unban();
        } catch (Exception $e) {
            Log::error('caught in '. __CLASS__.' '.$e->getMessage());
            throw $e;
        }
    }
}
