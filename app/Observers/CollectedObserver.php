<?php

namespace App\Observers;

use App\Models\Coins\Collected;

class CollectedObserver
{
    /**
     * Handle the Collected "created" event.
     *
     * @param  Collected  $collected
     * @return void
     */
    public function created(Collected $collected)
    {
        //
    }

    /**
     * Handle the Collected "updated" event.
     *
     * @param  Collected  $collected
     * @return void
     */
    public function updated(Collected $collected)
    {
        //
    }

    /**
     * Handle the Collected "deleted" event.
     *
     * @param  Collected  $collected
     * @return void
     */
    public function deleted(Collected $collected)
    {
        //
    }

    /**
     * Handle the Collected "restored" event.
     *
     * @param  Collected  $collected
     * @return void
     */
    public function restored(Collected $collected)
    {
        //
    }

    /**
     * Handle the Collected "force deleted" event.
     *
     * @param  Collected  $collected
     * @return void
     */
    public function forceDeleted(Collected $collected)
    {
        //
    }
}
