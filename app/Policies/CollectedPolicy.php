<?php

namespace App\Policies;

use App\Models\Coins\Collected;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CollectedPolicy
{
    use HandlesAuthorization;

    /**
     * Perform pre-authorization checks.
     *
     * @param User $user
     * @param string $ability
     * @return bool
     */
    public function before(User $user, string $ability): bool
    {
        if ($user->is_admin) {
            return true;
        }
        return false;
    }


    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Collected $collected
     * @return bool
     */
    public function view(User $user, Collected $collected): bool
    {
        return $user->id === $collected->user_id;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Collected $collected
     * @return bool
     */
    public function update(User $user, Collected $collected): bool
    {
        return $user->id === $collected->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param  Collected  $collected
     * @return bool
     */
    public function delete(User $user, Collected $collected): bool
    {
        return $user->id === $collected->user_id;
    }

}
