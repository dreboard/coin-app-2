<?php

namespace App\Policies;


use App\Models\Groups\Group;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class GroupPolicy
{
    use HandlesAuthorization;


    protected array $accepted_types = [
        'associate', 'full', 'executive'
    ];

    /**
     * Perform pre-authorization checks.
     *
     * @return void
     */
    public function before()
    {
        //return auth()->user()->is_admin;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Group $group
     * @return void
     */
    public function view(User $user, Group $group)
    {

    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  Group  $group
     * @return void
     */
    public function manage(Group $group)
    {

    }


    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param  Group  $group
     * @return bool
     */
    public function update(User $user, Group $group)
    {
        return auth()->user()->id === $group->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param  Group  $group
     * @return bool
     */
    public function delete(User $user, Group $group)
    {
        return auth()->user()->id === $group->user_id;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param User $user
     * @param  Group  $group
     * @return bool
     */
    public function restore(User $user, Group $group)
    {
        return auth()->user()->id === $group->user_id;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param User $user
     * @param  Group  $group
     * @return bool
     */
    public function forceDelete(User $user, Group $group)
    {
        return auth()->user()->id === $group->user_id;
    }
}
