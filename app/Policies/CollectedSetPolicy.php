<?php

namespace App\Policies;

use App\Models\Coins\CollectedSet;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CollectedSetPolicy
{
    use HandlesAuthorization;

    /**
     * Perform pre-authorization checks.
     *
     * @param User $user
     * @param string $ability
     * @return void
     */
    public function before(User $user, string $ability)
    {

    }


    /**
     * Determine whether the user can update the model.
     *
     * @param CollectedSet $collectedSet
     * @return bool
     */
    public function update(CollectedSet $collectedSet): bool
    {
        return auth()->user()->id === $collectedSet->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param CollectedSet $collectedSet
     * @return bool
     */
    public function delete(CollectedSet $collectedSet): bool
    {
        return auth()->user()->id === $collectedSet->user_id;
    }

}
