<?php

namespace App\Notifications;

use App\Mail\Users\WarnUserMailable;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class WarnUserNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @param array $warning_data
     */
    public function __construct(private readonly array $warning_data){}

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(mixed $notifiable): array
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return WarnUserMailable
     */
    public function toMail(mixed $notifiable): WarnUserMailable
    {
        return (new WarnUserMailable($this->warning_data))
            ->to($notifiable->email);
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array
     */
    public function toDatabase(): array
    {
        return $this->warning_data;
    }

}
