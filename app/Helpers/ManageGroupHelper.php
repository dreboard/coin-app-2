<?php
/**
 * ManageGroupHelper | helper methods for groups controllers and views
 *
 * This helper class calculates manage levels and group positions using integers.  Works also in tandem
 * with the roles, group_roles and elections tables
 *
 * @package Groups
 * @subpackage Helper
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Helpers;

use App\Models\Groups\Group;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 *
 */
class ManageGroupHelper
{

    /**
     * @var array[]
     */
    public  const MEMBER_TYPES = [0 => 'Guest', 'Associate', 1 =>  'Full', 'Retired', 4=> 'Executive'];

    /**
     * @var array[]
     */
    public const NOMINEE_TYPES = ['Full', 'Executive'];


    /**
     * @var array[]
     */
    public const GROUP_BOARD_POSITIONS = [
        'President' => 1,
        'Vice President' => 2,
        '2nd Vice President' => 3,
        'Treasurer' => 4,
        'Secretary' => 5,
    ];

    /**
     * @var array[]
     */
    public const MANAGE_LEVELS = [
        'Member Level' => 1, 'Staff Level' => 2, 'Director Level' => 3, 'Executive Level' => 4, 'Owner' => 5
    ];

    /**
     * @var array[]
     */
    public const ELECTED_POSITIONS = [
        1 => 'President', 2 => 'Vice President', 3 => '2nd Vice President', 4 => 'Treasurer', 5 => 'Secretary'
    ];

    /**
     * @var array[]
     */
    public const MANAGE_POSITIONS_LEVEL = [

        'President' => self::MANAGE_LEVELS['Executive Level'],
        'Vice President' => self::MANAGE_LEVELS['Executive Level'],
        'Treasurer' => self::MANAGE_LEVELS['Executive Level'],
        'Secretary' => self::MANAGE_LEVELS['Executive Level'],
        'Advisor' => self::MANAGE_LEVELS['Director Level'],
        'Program Chairman' => self::MANAGE_LEVELS['Director Level'],
        'Director' => self::MANAGE_LEVELS['Director Level'],
        'Membership Director' => self::MANAGE_LEVELS['Director Level'],
        'Activities Director' => self::MANAGE_LEVELS['Director Level'],
        'Committee Chair' => self::MANAGE_LEVELS['Director Level'],
        'Committee Member' => self::MANAGE_LEVELS['Staff Level'],
        'Dealer Director' => self::MANAGE_LEVELS['Staff Level'],
        'Librarian' => self::MANAGE_LEVELS['Staff Level'],
        'Historian' => self::MANAGE_LEVELS['Staff Level'],
        'Awards Director' => self::MANAGE_LEVELS['Staff Level'],
        'ANA Club Rep' => self::MANAGE_LEVELS['Staff Level'],
        'Show Chairman' => self::MANAGE_LEVELS['Staff Level'],
        'Bourse' => self::MANAGE_LEVELS['Staff Level'],
        'Show Promoter' => self::MANAGE_LEVELS['Staff Level'],
        'Show Rep' => self::MANAGE_LEVELS['Staff Level'],
        'Show Hospitality' => self::MANAGE_LEVELS['Staff Level'],
        'Auction Manager' => self::MANAGE_LEVELS['Staff Level'],
        'Advertising' => self::MANAGE_LEVELS['Staff Level'],
        'Marketing' => self::MANAGE_LEVELS['Staff Level'],
        'Sales Manager' => self::MANAGE_LEVELS['Staff Level'],
        'Editor' => self::MANAGE_LEVELS['Staff Level'],
        'Webmaster' => self::MANAGE_LEVELS['Staff Level'],
        'Social Media Manager' => self::MANAGE_LEVELS['Staff Level'],
        'News Editor' => self::MANAGE_LEVELS['Staff Level'],
        'Legal' => self::MANAGE_LEVELS['Staff Level'],
        'Site Manager' => self::MANAGE_LEVELS['Executive Level'],
        'Consignment' => self::MANAGE_LEVELS['Staff Level'],
        '2nd Vice President' => self::MANAGE_LEVELS['Executive Level'],
        'President Emeritus' => self::MANAGE_LEVELS['Staff Level'],
    ];


    /**
     * @var array[]
     */
    public  const GROUP_VOTE_LEVELS = ['Majority' => 0, 'Unanimous' => 1];
    public  const GROUP_TRANSPARENCY_LEVELS = ['Votes' => 0, 'All' => 1];
    public  const GROUP_VOTE_RESULT = [0 => 'Against',  1 => 'For'];


    public const REPORT_TYPES = [
        'Activity','Financial','Research','Progress','Annual','Inventory','Performance','Committee'
    ];

    /**
     * @var array[]
     */
    public const GROUP_COMMITTEE_POSITIONS = [
        'President' => 1,
        'Vice President' => 2,
        'Committee Chair' => 12,
        'Committee Member' => 14,
        'Treasurer' => 3,
        'Secretary' => 4,
    ];

    /**
     * @var array[]
     */
    public const MULTIPLE_MEMBER_POSITIONS = [
        'Show Hospitality',
        'Committee Member',
        'Advisor',
        'Consignment',
        'President Emeritus',
        'Writer',
    ];

    public const GROUP_COMMITTEE_TYPES = ['Educational','Nominations','Membership','Show','General','Financial','Awards','Special'];

    public const GROUP_COMMITTEE_TYPES_ARRAY = [
        'Educational' => [
            'type' => 'Educational',
            'description' => 'To develop and implement programs and activities which will provide members with the opportunity to achieve the level of knowledge and skill necessary to serve the public with competence and professionalism.',
            'chair' => 'Activities Director',
            'members' => 'Activities Director, Librarian, Historian'
        ],
        'Nominations' => [
            'type' => 'Nominations',
            'description' => 'To select nominees whose experience and qualities meet the needs of the organization. To contact prospective nominees and obtain their consent to serve if elected.',
            'chair' => 'Committee Chair',
            'members' => 'Committee Member'
        ],
        'Membership' => [
            'type' => 'Membership',
            'description' => 'The role of membership committees is to provide services and resources to their members. The committee is responsible for creating and implementing a membership plan, promoting membership throughout the year, providing membership reports at association meetings and distributing membership cards. The Membership Guide is also written by the committee.',
            'chair' => 'Membership Director',
            'members' => 'Committee Member, ANA Club Rep, Marketing'
        ],
        'Show' => [
            'type' => 'Show',
            'description' => 'To plan, execute and evaluate all aspects of presenting or representing the club during a coin show.',
            'chair' => 'Show Chairman',
            'members' => 'Show Rep, Show Hospitality, Sales Manager, Advertising, Auction Manager'
        ],
        'General' => [
            'type' => 'General',
            'description' => 'Responsible for the day to day management and operation of the organisation, or to provide assistance to the Board for specific matters',
            'chair' => 'Committee Chair',
            'members' => 'Committee Member'
        ],
        'Special' => [
            'type' => 'Special',
            'description' => 'Formed to act on special matters that may involve a conflict of interest for a member or members of the board of directors.',
            'chair' => 'President',
            'members' => 'Committee Member'
        ],
        'Financial' => [
            'type' => 'Financial',
            'description' => 'Provides financial analysis, advice, and oversight of the organizations budget. Their sole responsibility is to ensure the organization is operating with the financial resources it needs to provide programs and services to the community.',
            'chair' => 'Treasurer',
            'members' => 'Marketing, Sales Manager, Advertising, Committee Member, Auction Manager'
        ],
    ];


    /**
     * For factories
     * @var array[]
     */
    public const GROUP_POSITIONS = [
        'President' => 1,
        'Vice President' => 2,
        'Treasurer' => 4,
        'Secretary' => 5,
        'Director' => 6,
        'Awards Director' => 7,
        'Advisor' => 8,
        'Program Director' => 9,
        'Membership Director' => 10,
        'Activities Director' => 11,
        'Finance Director' => 34,
        'Election Director' => 35,
        'Committee Chair' => 12,
        'Show Chairman' => 13,
        'Committee Member' => 14,
        'Dealer Director' => 15,
        'Librarian' => 16,
        'Historian' => 17,
        'ANA Club Rep' => 18,
        'Bourse' => 19,
        'Show Promoter' => 20,
        'Show Rep' => 21,
        'Show Hospitality' => 22,
        'Advertising' => 23,
        'Marketing' => 24,
        'Sales Manager' => 25,
        'Editor' => 26,
        'Webmaster' => 27,
        'Social Media Manager' => 28,
        'News Editor' => 29,
        'Legal' => 30,
        'Site Manager' => 31,
        'Consignment' => 32,
        'President Emeritus' => 33
    ];


    /**
     * For factories
     * @var array[]
     */
    public const GROUP_POSITION_KEYS = [
        'President' => 1,
        'Vice President' => 2,
        '2nd Vice President' => 3,
        'Treasurer' => 4,
        'Secretary' => 5,
        'Director' => 6,
        'Awards Director' => 7,
        'Advisor' => 8,
        'Program Chairman' => 9,
        'Membership Director' => 10,
        'Activities Director' => 11,
        'Finance Director' => 12,
        'Election Director' => 13,
        'Committee Chair' => 14,
        'Show Chairman' => 13,
        'Committee Member' => 14,
        'Dealer Director' => 15,
        'Librarian' => 16,
        'Historian' => 17,
        'ANA Club Rep' => 18,
        'Bourse' => 19,
        'Show Promoter' => 20,
        'Show Rep' => 21,
        'Show Hospitality' => 22,
        'Advertising' => 23,
        'Marketing' => 24,
        'Sales Manager' => 25,
        'Editor' => 26,
        'Webmaster' => 27,
        'Social Media Manager' => 28,
        'News Editor' => 29,
        'Legal' => 30,
        'Site Manager' => 31,
        'Consignment' => 32,
        'President Emeritus' => 33
    ];


    /**
     * For factories
     * @var array[]
     */
    public const GROUP_POSITION_NUMBERS = [
        1 =>'President',
        2 =>'Vice President',
        3 =>'Treasurer',
        4 =>'Secretary',
        5 =>'Director',
        6 =>'Awards Director',
        7 =>'Program Director',
        8 =>'Membership Director',
        9 =>'Activities Director',
        10 =>'Committee Chair',
        11 =>'Show Chairman',
        12 =>'Special Events Coordinator',
        13 =>'Advisor',
        14 =>'Committee Member',
        15 =>'Dealer Director',
        16 =>'Sergeant-at-Arms',
        17 =>'Librarian',
        18 =>'Historian',
        19 =>'ANA Club Rep',
        20 =>'Bourse',
        21 =>'Show Promoter',
        22 =>'Show Rep',
        23 =>'Show Hospitality',
        24 =>'Marketing',
        25 =>'Sales Manager',
        26 =>'Editor',
        27 =>'Webmaster',
        28 =>'Social Media Manager',
        29 =>'Writer',
        30 =>'Legal',
        31 =>'Site Manager',
        32 =>'Writer',
        33 =>'Consignment',
        34 =>'2nd Vice President',
        35 =>'President Emeritus',
    ];

    /**
     * @param int $key
     */
    public static function getRoleKey(int $key)
    {

    }



    public static function getPosition(Group $group, int $position): Collection
    {
        return DB::table('group_role')
            ->select('group_role.id AS member_role_id', 'users.id AS member_user_id', 'users.name AS member_user_name',
                'users.first_name AS member_first_name', 'users.last_name AS member_last_name', 'roles.name AS member_position_name',
                'group_role.approved_at AS position_approved_at', 'group_role.expired_at AS position_expired_at')
            ->where('group_role.group_id', $group->id)
            ->where('roles.id', $position)
            ->join('users', 'users.id', '=', 'group_role.user_id')
            ->join('roles', 'roles.id', '=', 'group_role.role_id')
            ->join('groups', 'groups.id', '=', 'group_role.group_id')
            ->get();
    }

    public static function getUserRoles(Group $group, int $user_id): array
    {
        return DB::table('group_role')
            ->select('group_role.id')
            ->where('group_role.group_id', $group->id)
            ->where('group_role.user_id', $user_id)
            ->get()->pluck('id')->toArray();
    }
}
