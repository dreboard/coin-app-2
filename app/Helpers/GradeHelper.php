<?php
/**
 * GradeHelper | helper methods for coin controllers and views
 *
 * This helper class lists grades for each coin
 *
 * @package Coins
 * @subpackage Reports
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Helpers;

use App\Models\Coins\Collected;
use Illuminate\Support\Facades\DB;

/**
 *
 */
class GradeHelper
{

    /**
     * @var array[]
     */
    public array $strikes = [
        'Business','Proof','Matte Finish','Satin Finish','Special Mint','Reverse Proof','Matte Proof',
        'Enhanced Uncirculated','Special Strike','Enhanced Reverse Proof'
    ];

    /**
     * Array of business strike coins
     * @var array[]
     */
    public const BUSINESS_STRIKE = [
        'Business', 'Uncirculated'
    ];


    /**
     * Array of proof strike coins
     * @var array[]
     */
    public const PROOF_STRIKE = ['Proof', 'Matte Proof', 'Reverse Proof', 'Enhanced Reverse Proof'];


    /**
     * Array of special strike coins
     * @var array[]
     */
    public const SPECIAL_STRIKE = [
        'Enhanced', 'Enhanced Uncirculated','Matte Finish','Satin Finish','Special Mint','Special Strike'
    ];

    /**
     * @var array[]
     */
    public const Winged_Liberty_Dime = [
        'Smalent', 'Largent', 'Halent'
    ];


    /**
     * Array of coins to assign color
     * @var array[]
     */
    public const COLOR_CATEGORIES = [
        'Small Cent', 'Large Cent', 'Half Cent'
    ];


    /**
     * Array of coins to assign full bands
     * @var array[]
     */
    public const FULL_BAND_TYPES = [
        'Roosevelt Dime', 'Mercury Dime'
    ];

    /**
     * Array of coins to assign full designations
     * @var array[]
     */
    public const FULL_TYPES = [
        'Jefferson Nickel',
        'Standing Liberty',
        'Mercury Dime',
        'Franklin Half Dollar',
        'Roosevelt Dime'
    ];

    /**
     * Array of coins to assign problems
     */
    public const PROBLEMS = ['problem','damaged','holed','cleaned','polished','altered','scratched','pvc_damage','corrosion','bent','plugged'];

    /**
     * Array of coin grade numbers
     * @var array[]
     */
    public const COIN_GRADES = [0,1,2,3,4,6,8,10,12,15,20,25,30,35,40,45,50,53,55,58,60,61,62,63,64,65,66,67,68,69,70];
    /**
     * Array of coin grade numbers
     * @var array[]
     */
    public const TEN_POINT_COIN_GRADES = [1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.25, 8.5, 8.75, 9, 9.1, 9.2, 9.3, 9.4, 9.5, 9.6, 9.7, 9.8, 9.9, 10];

    /**
     * Array of business strike coin grade prefixes
     * @var array[]
     */
    public const COIN_MS_GRADES_PREFIX = [
        0 => 'Basal 0',1 => 'PO-1',2 => 'FR-2',3 => 'AG-3',4 => 'G-4',6 => 'G-6',
        8 => 'VG-8',10 => 'VG-10',12 => 'F-12',15 => 'F-15',
        20 => 'VF-20',25 => 'VF-25',30 => 'VF-30',
        35 => 'VF-35',40 => 'EF-40',45 => 'EF-45',50 => 'AU-50',
        53 => 'AU-53',55 => 'AU-55',58 => 'AU-58',
        60 => 'MS-60',61 => 'MS-61',62 => 'MS-62',63 => 'MS-63',64 => 'MS-64',65 => 'MS-65',66 => 'MS-66',
        67 => 'MS-67',68 => 'MS-68',69 => 'MS-69',70 => 'MS-70'
    ];

    /**
     * Array of proof strike coin grade prefixes
     * @var array[]
     */
    public const COIN_PR_GRADES_PREFIX = [
        0 => 'PR-0',1 => 'PR-1',2 => 'PR-2',3 => 'PR-3',4 => 'PR-4',6 => 'PR-6',
        8 => 'PR-8',10 => 'PR-10',12 => 'PR-12',15 => 'PR-15',
        20 => 'PR-20',25 => 'PR-25',30 => 'PR-30',
        35 => 'PR-35',40 => 'PR-40',45 => 'PR-45',50 => 'PR-50',
        53 => 'PR-53',55 => 'PR-55',58 => 'PR-58',
        60 => 'PR-60',61 => 'PR-61',62 => 'PR-62',63 => 'PR-63',64 => 'PR-64',65 => 'PR-65',66 => 'PR-66',
        67 => 'PR-79',68 => 'PR-68',69 => 'PR-69',70 => 'PR-70'
    ];

    /**
     * Array of special strike coin grade prefixes
     * @var array[]
     */
    public const COIN_SP_GRADES_PREFIX = [
        0 => 'Basal 0',1 => 'PO-1',2 => 'FR-2',3 => 'AG-3',4 => 'G-4',6 => 'G-6',
        8 => 'VG-8',10 => 'VG-10',12 => 'F-12',15 => 'F-15',
        20 => 'VF-20',25 => 'VF-25',30 => 'VF-30',
        35 => 'VF-35',40 => 'EF-40',45 => 'EF-45',50 => 'AU-50',
        53 => 'AU-53',55 => 'AU-55',58 => 'AU-58',
        60 => 'SP-60',61 => 'SP-61',62 => 'SP-62',63 => 'SP-63',64 => 'SP-64',65 => 'SP-65',66 => 'SP-66',
        67 => 'SP-67',68 => 'SP-68',69 => 'SP-69',70 => 'SP-70'
    ];


    /**
     * Array of business strike coin grade prefixes
     * @var array[]
     */
    public const GRADE_LINKS = [
        'Liberty Cap Half Cent' => 48,
        'Braided Hair Half Cent' => 45,
        'Classic Head Half Cent' => 46,
        'Draped Bust Half Cent' => 47,
        'Flying Eagle' => 1,
        'Flowing Hair Large Cent' => 51,
        'Braided Hair Liberty Head Large Cent' => 89,
        'Coronet Head Cent' => 88,
        'Classic Head Large Cent' => 87,
        'Draped Bust Large Cent' => 86,
        'Lincoln Wheat' => [
            'PCGS' => 'https://www.pcgs.com/photograde#/Lincoln/Grades',
            'NGC' => 'https://www.ngccoin.com/coin-grading-guide/grading-lincoln-cents/'
        ],
        'Lincoln Memorial' => [
            'PCGS' => 'https://www.pcgs.com/photograde#/Lincoln/Grades',
            'NGC' => 'https://www.ngccoin.com/coin-grading-guide/grading-lincoln-cents/'
        ],
        'Union Shield' => [
            'PCGS' => 'https://www.pcgs.com/photograde#/Lincoln/Grades',
            'NGC' => 'https://www.ngccoin.com/coin-grading-guide/grading-lincoln-cents/'
        ],
        'Indian Head Cent' => [
            'PCGS' => 'https://www.pcgs.com/photograde#/Indian/Grades',
            'NGC' => 'https://www.ngccoin.com/coin-grading-guide/grading-flying-eagle-cents/'
        ],
        'Lincoln Bicentennial' => [
            'PCGS' => 'https://www.pcgs.com/photograde#/Lincoln/Grades',
            'NGC' => 'https://www.ngccoin.com/coin-grading-guide/grading-lincoln-cents/'
        ],
        'Liberty Cap Large Cent' => 85,
        'Two Cent Piece' => 49,
        'Silver Three Cent' => 50,
        'Nickel Three Cent' => 84,
        'Return to Monticello' => 35,
        'Flowing Hair Half Dime' => 28,
        'Draped Bust Half Dime' => 27,
        'Capped Bust Half Dime' => 26,
        'Seated Liberty Half Dime' => 25,
        'Jefferson Nickel' => 23,
        'Indian Head Nickel' => 22,
        'Liberty Head Nickel' => 21,
        'Shield Nickel' => 20,
        'Westward Journey' => 34,
        'Capped Bust Dime' => 43,
        'Seated Liberty Dime' => 8,
        'Draped Bust Dime' => 7,
        'Barber Dime' => 9,
        'Mercury Dime' => 10,
        'Roosevelt Dime' => 11,
        'Twenty Cent Piece' => 44,
        'American Women' =>  131,
        'Crossing the Delaware' => 130,
        'America the Beautiful Quarter' => 42,
        'District of Columbia and US Territories' => 18,
        'State Quarter' => 17,
        'Washington Quarter' => 16,
        'Standing Liberty' => 15,
        'Barber Quarter' => 14,
        'Seated Liberty Quarter' => 13,
        'Draped Bust Quarter' => 12,
        'Liberty Cap Quarter' => 111,
        'Commemorative Quarter' => 114,
        'Capped Bust Quarter' => 41,
        'Franklin Half Dollar' => 37,
        'Walking Liberty' => 38,
        'Barber Half Dollar' => 39,
        'Kennedy Half Dollar' => 60,
        'Seated Liberty Half Dollar' => 57,
        'Draped Bust Half Dollar' => 54,
        'Flowing Hair Half Dollar' => 53,
        'Capped Bust Half Dollar' => 52,
        'Commemorative Half Dollar' => 29,
        'Silver American Eagle' => 101,
        'American Innovation Dollar' => 110,
        'Commemorative Gold Dollar' => 115,
        'Commemorative Dollar' => 112,
        'Susan B Anthony Dollar' => 40,
        'Peace Dollar' => 64,
        'Sacagawea Dollar' => 63,
        'Flowing Hair Dollar' => 62,
        'Draped Bust Dollar' => 61,
        'Seated Liberty Dollar' => 59,
        'Gobrecht Dollar' => 58,
        'Eisenhower Dollar' => 30,
        'Presidential Dollar' => 31,
        'Liberty Head Gold Dollar' => 55,
        'Trade Dollar' => 32,
        'Morgan Dollar' => 33,
        'Indian Princess Gold Dollar' => 65,
        'Liberty Cap Quarter Eagle' => 66,
        'Commemorative Quarter Eagle' => 116,
        'Indian Head Quarter Eagle' => 70,
        'Liberty Head Quarter Eagle' => 69,
        'Classic Head Quarter Eagle' => 68,
        'Turban Head Quarter Eagle' => 67,
        'Coronet Head Quarter Eagle' => 127,
        'Indian Princess Three Dollar' => 71,
        'Four Dollar Stella' => 72,
        'Commemorative Five Dollar' => 118,
        'Capped Bust Half Eagle' => 121,
        'Coronet Head Half Eagle' => 128,
        'Indian Head Half Eagle' => 78,
        'Tenth Ounce Gold' => 77,
        'Liberty Head Half Eagle' => 76,
        'Classic Head Half Eagle' => 75,
        'Liberty Cap Half Eagle' => 74,
        'Turban Head Half Eagle' => 73,
        'Tenth Ounce Buffalo' => 124,
        'Commemorative Ten Dollar' => 119,
        'Tenth Ounce Platinum' => 120,
        'Coronet Head Eagle' => 125,
        'Liberty Cap Eagle' => 126,
        'First Spouse' => 122,
        'Turban Head Eagle' => 79,
        'Liberty Head Eagle' => 80,
        'Indian Head Eagle' => 81,
        'Quarter Ounce Gold' => 82,
        'Quarter Ounce Buffalo' => 123,
        'Coronet Head Double Eagle' => 91,
        'Saint Gaudens Double Eagle' => 92,
        'Half Ounce Gold' => 98,
        'American Palladium Eagle' => 109,
        'Half Ounce Buffalo' => 93,
        'Quarter Ounce Platinum' => 94,
        'One Ounce Buffalo' => 97,
        'Commemorative Fifty Dollar' => 117,
        'One Ounce Gold' => 99,
        'Gold American Eagle' => 108,
        'Half Ounce Platinum' => 96,
        'One Ounce Platinum' => 100,
        'American Liberty Union' => 129,
    ];

    /**
     * Array of coin grade groups
     * @var array[]
     */
    public const COIN_ADJECTIVAL_GRADES = [
        'Uncirculated' => [60,61,62],
        'Choice Uncirculated' => [63,64],
        'Gem Uncirculated'  => [65,66],
        'Superb Gem Uncirculated' => [67,68,69],
        'Perfect Uncirculated' [70]
    ];

    /**
     * Array of coin grade groups
     * @var array[]
     */
    public const COIN_GRADE_NUMBERSS = [
        0,1,2,3,4,6,8,10,12,15,20,25,30,35,40,45,50,53,55,58,60,61,62,63,64,65,66,67,68,69,70
    ];


    /**
     * @param int $type_id
     * @return string
     */
    public static function getGradeForType(int $type_id): string
    {

        $collected_grades = DB::table('collected')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->select('grade')
            ->where('coins.cointypes_id',$type_id)
            ->get();

        $grade_table = "
         <tr class='keyRow text-center alert alert-primary'>
                    <td class='gradeCell'>Basal 0</td>
                    <td class='gradeCell'>PO-1</td>
                    <td class='gradeCell'>FR-2</td>
                    <td class='gradeCell'>AG-3</td>
                    <td class='gradeCell'>G-4</td>
                    <td class='gradeCell'>G-6</td>
                    <td class='gradeCell'>VG-8</td>
                    <td class='gradeCell'>VG-10</td>
                    <td class='gradeCell'>F-12</td>
                    <td class='gradeCell'>F-15</td>
                </tr>
         <tr class='keyRow text-center'>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 0])."'>".self::countGradeByType(0, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 1])."'>".self::countGradeByType(1, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 2])."'>".self::countGradeByType(2, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 3])."'>".self::countGradeByType(3, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 4])."'>".self::countGradeByType(4, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 6])."'>".self::countGradeByType(6, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 8])."'>".self::countGradeByType(8, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 10])."'>".self::countGradeByType(10, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 12])."'>".self::countGradeByType(12, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 15])."'>".self::countGradeByType(15, $type_id)."</a></td>
                </tr>
        ";

        $grade_table .= "
         <tr class='keyRow text-center alert alert-primary'>
                    <td class='gradeCell'>VF-20</td>
                    <td class='gradeCell'>VF-25</td>
                    <td class='gradeCell'>VF-30</td>
                    <td class='gradeCell'>VF-35</td>
                    <td class='gradeCell'>EF-40</td>
                    <td class='gradeCell'>EF-45</td>
                    <td class='gradeCell'>AU-50</td>
                    <td class='gradeCell'>AU-53</td>
                    <td class='gradeCell'>AU-55</td>
                    <td class='gradeCell'>AU-58</td>
                </tr>
         <tr class='keyRow text-center'>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 20])."'>".self::countGradeByType(20, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 25])."'>".self::countGradeByType(25, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 30])."'>".self::countGradeByType(30, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 35])."'>".self::countGradeByType(35, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 40])."'>".self::countGradeByType(40, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 45])."'>".self::countGradeByType(45, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 50])."'>".self::countGradeByType(50, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 53])."'>".self::countGradeByType(53, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 55])."'>".self::countGradeByType(55, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 58])."'>".self::countGradeByType(58, $type_id)."</a></td>
                </tr>
        ";

        $grade_table .= "
         <tr class='keyRow text-center alert alert-primary'>
                    <td class='gradeCell'>PR/MS-60</td>
                    <td class='gradeCell'>PR/MS-61</td>
                    <td class='gradeCell'>PR/MS-62</td>
                    <td class='gradeCell'>PR/MS-63</td>
                    <td class='gradeCell'>PR/MS-64</td>
                    <td class='gradeCell'>PR/MS-65</td>
                    <td class='gradeCell'>PR/MS-66</td>
                    <td class='gradeCell'>PR/MS-67</td>
                    <td class='gradeCell'>PR/MS-68</td>
                    <td class='gradeCell'>PR/MS-69</td>
                </tr>
         <tr class='keyRow text-center'>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 60])."'>".self::countGradeByType(60, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 61])."'>".self::countGradeByType(61, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 62])."'>".self::countGradeByType(62, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 63])."'>".self::countGradeByType(63, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 64])."'>".self::countGradeByType(64, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 65])."'>".self::countGradeByType(65, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 66])."'>".self::countGradeByType(66, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 67])."'>".self::countGradeByType(67, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 68])."'>".self::countGradeByType(68, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 69])."'>".self::countGradeByType(69, $type_id)."</a></td>
                </tr>
        ";

        $grade_table .= "
         <tr class='keyRow text-center alert alert-primary'>
                    <td class='gradeCell'>PR/MS-70</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                </tr>
         <tr class='keyRow text-center'>
                    <td class='gradeCell'><a href='".route('coin.coin_grade', ['coinType' => $type_id, 'grade' => 70])."'>".self::countGradeByType(70, $type_id)."</a></td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                </tr>
        ";
        return $grade_table;
    }


    /**
     * Grades for coin type and strike
     *
     * @param string $strike
     * @param int $type_id
     * @return string
     */
    public static function getGradeForTypeForStrike(int $type_id, string $strike): string
    {

        $prefix = self::strikePrefix($strike);
        //Log::info($strike );
        $grade_table = "
         <tr class='keyRow text-center alert alert-primary'>
                    <td class='gradeCell'>".self::lowerGradePrefix('Basal-', $strike)."0</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('PO-', $strike)."1</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('FR-', $strike)."2</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('AG-', $strike)."3</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('G-', $strike)."4</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('G-', $strike)."6</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('VG-', $strike)."8</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('VG-', $strike)."10</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('F-', $strike)."12</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('F-', $strike)."15</td>
                </tr>
         <tr class='keyRow text-center'>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 0])."'>".self::countStrikeGradeByType($strike, 0, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 1])."'>".self::countStrikeGradeByType($strike, 1, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 2])."'>".self::countStrikeGradeByType($strike, 2, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 3])."'>".self::countStrikeGradeByType($strike, 3, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 4])."'>".self::countStrikeGradeByType($strike, 4, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 6])."'>".self::countStrikeGradeByType($strike, 6, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 8])."'>".self::countStrikeGradeByType($strike, 8, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 10])."'>".self::countStrikeGradeByType($strike, 10, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 12])."'>".self::countStrikeGradeByType($strike, 12, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 15])."'>".self::countStrikeGradeByType($strike, 15, $type_id)."</a></td>
                </tr>

         <tr class='keyRow text-center alert alert-primary'>
                    <td class='gradeCell'>".self::lowerGradePrefix('VF-', $strike)."20</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('VF-', $strike)."25</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('VF-', $strike)."30</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('VF-', $strike)."35</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('EF-', $strike)."40</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('EF-', $strike)."45</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('AU-', $strike)."50</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('AU-', $strike)."53</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('AU-', $strike)."55</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('AU-', $strike)."58</td>
                </tr>
         <tr class='keyRow text-center'>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 20])."'>".self::countStrikeGradeByType($strike, 20, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 25])."'>".self::countStrikeGradeByType($strike, 25, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 30])."'>".self::countStrikeGradeByType($strike, 30, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 35])."'>".self::countStrikeGradeByType($strike, 35, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 40])."'>".self::countStrikeGradeByType($strike, 40, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 45])."'>".self::countStrikeGradeByType($strike, 45, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 50])."'>".self::countStrikeGradeByType($strike, 50, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 53])."'>".self::countStrikeGradeByType($strike, 53, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 55])."'>".self::countStrikeGradeByType($strike, 55, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 58])."'>".self::countStrikeGradeByType($strike, 58, $type_id)."</a></td>
                </tr>

         <tr class='keyRow text-center alert alert-primary'>
                    <td class='gradeCell'>{$prefix}60</td>
                    <td class='gradeCell'>{$prefix}61</td>
                    <td class='gradeCell'>{$prefix}62</td>
                    <td class='gradeCell'>{$prefix}63</td>
                    <td class='gradeCell'>{$prefix}64</td>
                    <td class='gradeCell'>{$prefix}65</td>
                    <td class='gradeCell'>{$prefix}66</td>
                    <td class='gradeCell'>{$prefix}67</td>
                    <td class='gradeCell'>{$prefix}68</td>
                    <td class='gradeCell'>{$prefix}69</td>
                </tr>
         <tr class='keyRow text-center'>
                   <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 60])."'>".self::countStrikeGradeByType($strike, 60, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 61])."'>".self::countStrikeGradeByType($strike, 61, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 62])."'>".self::countStrikeGradeByType($strike, 62, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 63])."'>".self::countStrikeGradeByType($strike, 63, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 64])."'>".self::countStrikeGradeByType($strike, 64, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 65])."'>".self::countStrikeGradeByType($strike, 65, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 66])."'>".self::countStrikeGradeByType($strike, 66, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 67])."'>".self::countStrikeGradeByType($strike, 67, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 68])."'>".self::countStrikeGradeByType($strike, 68, $type_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 69])."'>".self::countStrikeGradeByType($strike, 69, $type_id)."</a></td>
                </tr>

         <tr class='keyRow text-center alert alert-primary'>
                    <td class='gradeCell'>{$prefix}70</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                </tr>
         <tr class='keyRow text-center'>
                    <td class='gradeCell'><a href='".route('coin.coin_strike_grade', ['strike' => $strike, 'coinType' => $type_id, 'grade' => 70])."'>".self::countStrikeGradeByType($strike, 70, $type_id)."</a></td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                </tr>
        ";
        return $grade_table;
    }


    /**
     * Assign prefix by strike
     *
     * @param string $current
     * @param string $strike
     * @return string
     */
    public static function lowerGradePrefix(string $current, string $strike): string
    {
        if(in_array($strike, self::SPECIAL_STRIKE) || in_array($strike, ['special','business']) || in_array($strike, self::BUSINESS_STRIKE)){
            return $current;
        }
        return "PR-";
    }

    /**
     * @param int $coin_id
     * @param string $strike
     * @return string
     */
    public static function getGradeForCoin(int $coin_id, string $strike): string
    {

        $prefix = self::strikePrefix($strike);

        if(in_array($strike, self::PROOF_STRIKE)){

        }

        $grade_table = "
         <tr class='keyRow text-center alert alert-primary'>
                    <td class='gradeCell'>".self::lowerGradePrefix('Basal-', $strike)."0</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('PO-', $strike)."1</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('FR-', $strike)."2</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('AG-', $strike)."3</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('G-', $strike)."4</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('G-', $strike)."6</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('VG-', $strike)."8</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('VG-', $strike)."10</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('F-', $strike)."12</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('F-', $strike)."15</td>
                </tr>
         <tr class='keyRow text-center'>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 0])."'>".self::countGradeByCoinID(0, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 1])."'>".self::countGradeByCoinID(1, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 2])."'>".self::countGradeByCoinID(2, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 3])."'>".self::countGradeByCoinID(3, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 4])."'>".self::countGradeByCoinID(4, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 6])."'>".self::countGradeByCoinID(6, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 8])."'>".self::countGradeByCoinID(8, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 10])."'>".self::countGradeByCoinID(10, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 12])."'>".self::countGradeByCoinID(12, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 15])."'>".self::countGradeByCoinID(15, $coin_id)."</a></td>
                </tr>
        ";

        $grade_table .= "
         <tr class='keyRow text-center alert alert-primary'>
                    <td class='gradeCell'>".self::lowerGradePrefix('VF-', $strike)."20</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('VF-', $strike)."25</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('VF-', $strike)."30</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('VF-', $strike)."35</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('EF-', $strike)."40</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('EF-', $strike)."45</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('AU-', $strike)."50</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('AU-', $strike)."53</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('AU-', $strike)."55</td>
                    <td class='gradeCell'>".self::lowerGradePrefix('AU-', $strike)."58</td>
                </tr>
         <tr class='keyRow text-center'>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 20])."'>".self::countGradeByCoinID(20, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 25])."'>".self::countGradeByCoinID(25, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 30])."'>".self::countGradeByCoinID(30, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 35])."'>".self::countGradeByCoinID(35, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 40])."'>".self::countGradeByCoinID(40, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 45])."'>".self::countGradeByCoinID(45, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 50])."'>".self::countGradeByCoinID(50, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 53])."'>".self::countGradeByCoinID(53, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 55])."'>".self::countGradeByCoinID(55, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 58])."'>".self::countGradeByCoinID(58, $coin_id)."</a></td>
                </tr>
        ";

        $grade_table .= "
         <tr class='keyRow text-center alert alert-primary'>
                    <td class='gradeCell'>{$prefix}60</td>
                    <td class='gradeCell'>{$prefix}61</td>
                    <td class='gradeCell'>{$prefix}62</td>
                    <td class='gradeCell'>{$prefix}63</td>
                    <td class='gradeCell'>{$prefix}64</td>
                    <td class='gradeCell'>{$prefix}65</td>
                    <td class='gradeCell'>{$prefix}66</td>
                    <td class='gradeCell'>{$prefix}67</td>
                    <td class='gradeCell'>{$prefix}68</td>
                    <td class='gradeCell'>{$prefix}69</td>
                </tr>
         <tr class='keyRow text-center'>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 60])."'>".self::countGradeByCoinID(60, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 61])."'>".self::countGradeByCoinID(61, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 62])."'>".self::countGradeByCoinID(62, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 63])."'>".self::countGradeByCoinID(63, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 64])."'>".self::countGradeByCoinID(64, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 65])."'>".self::countGradeByCoinID(65, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 66])."'>".self::countGradeByCoinID(66, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 67])."'>".self::countGradeByCoinID(67, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 68])."'>".self::countGradeByCoinID(68, $coin_id)."</a></td>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 69])."'>".self::countGradeByCoinID(69, $coin_id)."</a></td>
                </tr>
        ";

        $grade_table .= "
         <tr class='keyRow text-center alert alert-primary'>
                    <td class='gradeCell'>{$prefix}70</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                </tr>
         <tr class='keyRow text-center'>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 70])."'>".self::countGradeByCoinID(70, $coin_id)."</a></td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                </tr>
        ";
        return $grade_table;
    }


    /**
     * Count grade by type (#of MS-68 for Lincoln Wheat)
     *
     * @param $grade
     * @param $type_id
     * @return int
     */
    public static function countGradeByType($grade, $type_id): int
    {
        return DB::table('collected')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->select('grade')
            ->where('coins.cointypes_id',$type_id)
            ->where('collected.grade',$grade)
            ->get()->count();
    }


    /**
     * Count grade by type (#of PROOF MS-68 for Lincoln Wheat)
     *
     * @param $grade
     * @param $type_id
     * @param $strike
     * @return int
     */
    public static function countStrikeGradeByType($strike, $grade, $type_id): int
    {
        return DB::table('collected')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->select('grade')
            ->where('coins.cointypes_id',$type_id)
            ->where('coins.strike',$strike)
            ->where('collected.grade',$grade)
            ->get()->count();
    }

    /**
     * Count grade by coin ID
     *
     * @param $grade
     * @param $coin_id
     * @return int
     */
    public static function countGradeByCoinID($grade, $coin_id): int
    {
        return DB::table('collected')
            ->join('coins', 'coins.id', '=', 'collected.coin_id')
            ->where('collected.user_id',auth()->user()->id)
            ->select('grade')
            ->where('coins.id',$coin_id)
            ->where('collected.grade',$grade)
            ->get()->count();
    }

    /**
     * Assign prefix by strike
     *
     * @param $strike
     * @return string
     */
    public static function strikePrefix($strike): string
    {
        if(in_array($strike, self::SPECIAL_STRIKE) || $strike == 'special'){
            return "SP-";
        }
        if(in_array($strike, self::PROOF_STRIKE) || $strike == 'proof'){
            return "PR-";
        }
        return "MS-";
    }

    /**
     * Assign prefix by strike
     *
     * @param $setType
     * @return string
     */
    public static function setStrikePrefix($setType): string
    {
        // SELECT DISTINCT(`setType`) FROM `mintset` ORDER BY `setType` ASC;
        $setTypes = ['3 Coin Set', 'American Buffalo', 'American Eagle', 'Circulated Coin Set', 'Coin & Chronicles Set', 'Commemorative', 'Limited Edition',
            'Mint', 'Proof', 'Silver Proof', 'Souvenir Set', 'Special Mint', 'Uncirculated Coin Set'];
        $setPRTypes = ['3 Coin Set', 'American Buffalo', 'American Eagle', 'Circulated Coin Set', 'Coin & Chronicles Set', 'Commemorative', 'Limited Edition',
            'Mint', 'Proof', 'Silver Proof', 'Souvenir Set', 'Special Mint', 'Uncirculated Coin Set'];
        $setMSTypes = ['3 Coin Set', 'American Buffalo', 'American Eagle', 'Circulated Coin Set', 'Coin & Chronicles Set', 'Commemorative', 'Limited Edition',
            'Mint', 'Proof', 'Silver Proof', 'Souvenir Set', 'Special Mint', 'Uncirculated Coin Set'];
        $setSPTypes = ['3 Coin Set', 'American Buffalo', 'American Eagle', 'Circulated Coin Set', 'Coin & Chronicles Set', 'Commemorative', 'Limited Edition',
            'Mint', 'Proof', 'Silver Proof', 'Souvenir Set', 'Special Mint', 'Uncirculated Coin Set'];
        if(in_array($setType, $setSPTypes)){
            return "SP-";
        }
        if(in_array($setType, $setPRTypes)){
            return "PR-";
        }
        return "MS-";
    }




    /**
     * Load correct grade list for saving collected coin
     *
     * @param $strike
     * @return string
     */
    public static function gradeList($strike): string
    {
        if(in_array($strike, self::PROOF_STRIKE)){
            return 'collected.includes.proof';
        }
        if(in_array($strike, self::SPECIAL_STRIKE)){
            return 'collected.includes.special';
        }
        return 'collected.includes.business';
    }

    /**
     * Is strike a circulated coin
     *
     * @param $strike
     * @return int
     */
    public static function isCirculated($strike): int
    {
        if (in_array($strike, self::PROOF_STRIKE) || in_array($strike, self::SPECIAL_STRIKE)) {
            return 0;
        }
        return 1;
    }


    /**
     * Array of coin grade descriptions
     * @return array[]
     */
    public static function getGradeDescription(): array
    {
        return [
            0 => '',
            1 => 'Clear enough to identify, date may be worn smooth with one side of the coin blanked. Coins that are very badly corroded may also fall under this category. ',
            2 => 'Entire coin is worn flat, with only traces of peripheral lettering still visible.',
            3 => 'Readable lettering although very heavily worn. The date and design may be worn smooth.',
            4 => 'Rims mostly full but may be flat or slightly worn into peripheral letter in spots.',
            6 => 'Rims and peripheral lettering full but design now flat and visible only in outline form.',
            8 => 'Most central detail now worn flat. Some inner lettering still visible. Rims remain full.',
            10 => 'Considerable wear has flattened most of the fine detail. Most lettering remains readable.',
            12 => 'About half of detail now worn flat. All lettering remains visible.',
            15 => 'Slightly less than half of finer detail worn flat. All lettering remains sharp and clear.',
            20 => 'Moderate wear with some loss of detail evident in design.',
            25 => 'Entire surface shows some wear, but major design features remain clear.',
            30 => 'Wear now evident over entire surface. Intricate design detail beginning to flatten.',
            35 => 'Light wear is seen over entire surface though all major detail still visible.',
            40 => 'All design elements still show, but high points now worn flat. Little to no luster remains.',
            45 => 'High points of design show light wear. A bit of luster may still be visible in protected areas.',
            50 => 'Trace of wear now seen on higher points of design. Bits of luster may remain.',
            53 => 'Slight flatness and loss of luster visible on high points of design. Some luster remains.Full detail, with light friction on the high points. Considerable luster remains.',
            55 => 'Full detail, with light friction on the high points. Considerable luster remains.',
            58 => 'Only the slightest friction on the highest points. Virtually full luster.',
            60 => 'No wear. May be poorly struck with many heavy marks or hairlines.',
            61 => 'No wear, with average or weak strike. Multiple heavy marks or hairlines allowed.',
            62 => 'No wear, with average or below average strike. Numerous marks or hairlines.',
            63 => 'Average or slightly weak strike with moderate marks or hairlines.',
            64 => 'Average or better strike with scattered marks or hairlines, though none severe.',
            65 => 'Above average strike with minor marks or hairlines, mostly out of focal areas.',
            66 => 'Well struck with a few marks or hairlines, not in focal areas.',
            67 => 'Very well struck with minor imperfections visible without magnification.',
            68 => 'Only the slightest weakness in strike with a few tiny imperfections barely visible.Virtually fully struck with minuscule imperfections visible upon close inspection.',
            69 => 'Virtually fully struck with minuscule imperfections visible upon close inspection.',
            70 => 'Has no trace of wear, handling, scratches or contact with other coins from a (5x) magnification.',
        ];
    }
    public const COIN_GRADING_NUMBERS = [
        0 => '',
        1 => 'Clear enough to identify, date may be worn smooth with one side of the coin blanked. Coins that are very badly corroded may also fall under this category. ',
        2 => 'Entire coin is worn flat, with only traces of peripheral lettering still visible.',
        3 => 'Readable lettering although very heavily worn. The date and design may be worn smooth.',
        4 => 'Rims mostly full but may be flat or slightly worn into peripheral letter in spots.',
        6 => 'Rims and peripheral lettering full but design now flat and visible only in outline form.',
        8 => 'Most central detail now worn flat. Some inner lettering still visible. Rims remain full.',
        10 => 'Considerable wear has flattened most of the fine detail. Most lettering remains readable.',
        12 => 'About half of detail now worn flat. All lettering remains visible.',
        15 => 'Slightly less than half of finer detail worn flat. All lettering remains sharp and clear.',
        20 => 'Moderate wear with some loss of detail evident in design.',
        25 => 'Entire surface shows some wear, but major design features remain clear.',
        30 => 'Wear now evident over entire surface. Intricate design detail beginning to flatten.',
        35 => 'Light wear is seen over entire surface though all major detail still visible.',
        40 => 'All design elements still show, but high points now worn flat. Little to no luster remains.',
        45 => 'High points of design show light wear. A bit of luster may still be visible in protected areas.',
        50 => 'Trace of wear now seen on higher points of design. Bits of luster may remain.',
        53 => 'Slight flatness and loss of luster visible on high points of design. Some luster remains.Full detail, with light friction on the high points. Considerable luster remains.',
        55 => 'Full detail, with light friction on the high points. Considerable luster remains.',
        58 => 'Only the slightest friction on the highest points. Virtually full luster.',
        60 => 'No wear. May be poorly struck with many heavy marks or hairlines.',
        61 => 'No wear, with average or weak strike. Multiple heavy marks or hairlines allowed.',
        62 => 'No wear, with average or below average strike. Numerous marks or hairlines.',
        63 => 'Average or slightly weak strike with moderate marks or hairlines.',
        64 => 'Average or better strike with scattered marks or hairlines, though none severe.',
        65 => 'Above average strike with minor marks or hairlines, mostly out of focal areas.',
        66 => 'Well struck with a few marks or hairlines, not in focal areas.',
        67 => 'Very well struck with minor imperfections visible without magnification.',
        68 => 'Only the slightest weakness in strike with a few tiny imperfections barely visible.Virtually fully struck with minuscule imperfections visible upon close inspection.',
        69 => 'Virtually fully struck with minuscule imperfections visible upon close inspection.',
        70 => 'Has no trace of wear, handling, scratches or contact with other coins from a (5x) magnification.',
    ];



    /**
     * Grades for coin type and strike
     *
     * @param int $coin_id
     * @return string
     */
    public static function getFullHeadCollected(int $coin_id): string
    {
        $coins = Collected::where('collected.user_id',auth()->user()->id)
            ->where('collected.coin_id', $coin_id)
            ->whereNotNull('collected.fullAtt')
            ->select('collected.grade','collected.id','collected.nickName','collected.fullAtt')
            ->get();
        $grade_table = "
         <tr class='keyRow text-center'>
                    <td class='gradeCell'><a href='".route('coin.coin_id_grade', ['coin' => $coin_id, 'grade' => 70])."'>".self::countGradeByCoinID(70, $coin_id)."</a></td>
                    <td class='gradeCell'>&nbsp;</td>
                    <td class='gradeCell'>&nbsp;</td>
                </tr>
        ";
        return $grade_table;
    }

}
