<?php

namespace App\Helpers;

class ImageHelper
{


    public static function addTextToCell($img, $cellX, $cellWidth, $cellY, $cellHeight, $text)
    {
        // Calculate text size
        $text_box = imagettfbbox(20, 0, 'OpenSans', $text);
        $text_width = $text_box[2]-$text_box[0];
        $text_height = $text_box[7]-$text_box[1];

        // Calculate x/y position
        $textx = $cellX + ($cellWidth / 2) - $text_width;
        $texty = $cellY - ($cellHeight / 2) - $text_height;

        // Set color and draw
        $color = imagecolorallocate($img, 0, 0, 255);
        imagettftext($img, 20, 0, $textx, $texty, $color, 'OpenSans', $text);
    }
}
