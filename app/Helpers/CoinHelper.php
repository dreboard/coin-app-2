<?php
/**
 * CoinHelper | helper for CoinTypeController, CoinCategoryController, CoinController
 *
 * @package Coins
 * @subpackage Display
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Helpers;

abstract class CoinHelper
{

    // SELECT * FROM `cointypes` ORDER BY `denomination`, LEFT(`dates`, 4) ASC;
    /**
     * @var array
     */

    public const COIN_TYPES = [
        113 => 'No Type',
        48 => 'Liberty Cap Half Cent',
        47 => 'Draped Bust Half Cent',
        46 => 'Classic Head Half Cent',
        45 => 'Braided Hair Half Cent',
        51 => 'Flowing Hair Large Cent',
        85 => 'Liberty Cap Large Cent',
        86 => 'Draped Bust Large Cent',
        87 => 'Classic Head Large Cent',
        88 => 'Coronet Head Cent',
        89 => 'Braided Hair Liberty Head Large Cent',
        1 => 'Flying Eagle',
        90 => 'Mixed Cents',
        102 => 'No Type',
        5 => 'Indian Head Cent',
        2 => 'Lincoln Wheat',
        3 => 'Lincoln Memorial',
        6 => 'Lincoln Bicentennial',
        4 => 'Union Shield',
        49 => 'Two Cent Piece',
        50 => 'Silver Three Cent',
        84 => 'Nickel Three Cent',
        28 => 'Flowing Hair Half Dime',
        27 => 'Draped Bust Half Dime',
        26 => 'Capped Bust Half Dime',
        25 => 'Seated Liberty Half Dime',
        20 => 'Shield Nickel',
        103 => 'Mixed Nickels',
        21 => 'Liberty Head Nickel',
        22 => 'Indian Head Nickel',
        23 => 'Jefferson Nickel',
        34 => 'Westward Journey',
        35 => 'Return to Monticello',
        7 => 'Draped Bust Dime',
        104 => 'Mixed Dimes',
        43 => 'Capped Bust Dime',
        8 => 'Seated Liberty Dime',
        9 => 'Barber Dime',
        10 => 'Mercury Dime',
        11 => 'Roosevelt Dime',
        44 => 'Twenty Cent Piece',
        12 => 'Draped Bust Quarter',
        105 => 'Mixed Quarters',
        41 => 'Capped Bust Quarter',
        111 => 'Liberty Cap Quarter',
        13 => 'Seated Liberty Quarter',
        14 => 'Barber Quarter',
        114 => 'Commemorative Quarter',
        15 => 'Standing Liberty',
        16 => 'Washington Quarter',
        17 => 'State Quarter',
        18 => 'District of Columbia and US Territories',
        42 => 'America the Beautiful Quarter',
        130 => 'Crossing the Delaware',
        131 => 'American Women',
        53 => 'Flowing Hair Half Dollar',
        106 => 'Mixed Half Dollars',
        54 => 'Draped Bust Half Dollar',
        52 => 'Capped Bust Half Dollar',
        57 => 'Seated Liberty Half Dollar',
        29 => 'Commemorative Half Dollar',
        39 => 'Barber Half Dollar',
        38 => 'Walking Liberty',
        37 => 'Franklin Half Dollar',
        60 => 'Kennedy Half Dollar',
        62 => 'Flowing Hair Dollar',
        107 => 'Mixed Dollars',
        61 => 'Draped Bust Dollar',
        58 => 'Gobrecht Dollar',
        59 => 'Seated Liberty Dollar',
        55 => 'Liberty Head Gold Dollar',
        65 => 'Indian Princess Gold Dollar',
        32 => 'Trade Dollar',
        33 => 'Morgan Dollar',
        112 => 'Commemorative Dollar',
        115 => 'Commemorative Gold Dollar',
        64 => 'Peace Dollar',
        30 => 'Eisenhower Dollar',
        40 => 'Susan B Anthony Dollar',
        101 => 'Silver American Eagle',
        63 => 'Sacagawea Dollar',
        31 => 'Presidential Dollar',
        110 => 'American Innovation Dollar',
        66 => 'Draped Bust Quarter Eagle',
        67 => 'Capped Bust Quarter Eagle',
        68 => 'Classic Head Quarter Eagle',
        69 => 'Liberty Head Quarter Eagle',
        127 => 'Coronet Head Quarter Eagle',
        70 => 'Indian Head Quarter Eagle',
        116 => 'Commemorative Quarter Eagle',
        71 => 'Indian Princess Three Dollar',
        72 => 'Four Dollar Stella',
        73 => 'Turban Head Half Eagle',
        74 => 'Liberty Cap Half Eagle',
        75 => 'Classic Head Half Eagle',
        76 => 'Liberty Head Half Eagle',
        128 => 'Coronet Head Half Eagle',
        121 => 'Capped Bust Half Eagle',
        78 => 'Indian Head Half Eagle',
        77 => 'Tenth Ounce Gold',
        118 => 'Commemorative Five Dollar',
        124 => 'Tenth Ounce Buffalo',
        79 => 'Turban Head Eagle',
        126 => 'Liberty Cap Eagle',
        80 => 'Liberty Head Eagle',
        125 => 'Coronet Head Eagle',
        81 => 'Indian Head Eagle',
        119 => 'Commemorative Ten Dollar',
        82 => 'Quarter Ounce Gold',
        120 => 'Tenth Ounce Platinum',
        122 => 'First Spouse',
        123 => 'Quarter Ounce Buffalo',
        91 => 'Coronet Head Double Eagle',
        92 => 'Saint Gaudens Double Eagle',
        95 => 'Half Ounce Gold',
        98 => 'Half Ounce Gold',
        94 => 'Quarter Ounce Platinum',
        93 => 'Half Ounce Buffalo',
        109 => 'American Palladium Eagle',
        117 => 'Commemorative Fifty Dollar',
        99 => 'One Ounce Gold',
        96 => 'Half Ounce Platinum',
        108 => 'Gold American Eagle',
        97 => 'One Ounce Buffalo',
        100 => 'One Ounce Platinum',
        129 => 'American Liberty Union',
    ];



    public const TYPES_LIST = [
        'Liberty Cap Half Cent' => 48,
        'Braided Hair Half Cent' => 45,
        'Classic Head Half Cent' => 46,
        'Draped Bust Half Cent' => 47,
        'Flying Eagle' => 1,
        'Flowing Hair Large Cent' => 51,
        'Braided Hair Liberty Head Large Cent' => 89,
        'Coronet Head Cent' => 88,
        'Classic Head Large Cent' => 87,
        'Draped Bust Large Cent' => 86,
        'Lincoln Wheat' => 2,
        'Lincoln Memorial' => 3,
        'Union Shield' => 4,
        'Indian Head Cent' => 5,
        'Lincoln Bicentennial' => 6,
        'Liberty Cap Large Cent' => 85,
        'Two Cent Piece' => 49,
        'Silver Three Cent' => 50,
        'Nickel Three Cent' => 84,
        'Return to Monticello' => 35,
        'Flowing Hair Half Dime' => 28,
        'Draped Bust Half Dime' => 27,
        'Capped Bust Half Dime' => 26,
        'Seated Liberty Half Dime' => 25,
        'Jefferson Nickel' => 23,
        'Indian Head Nickel' => 22,
        'Liberty Head Nickel' => 21,
        'Shield Nickel' => 20,
        'Westward Journey' => 34,
        'Capped Bust Dime' => 43,
        'Seated Liberty Dime' => 8,
        'Draped Bust Dime' => 7,
        'Barber Dime' => 9,
        'Mercury Dime' => 10,
        'Roosevelt Dime' => 11,
        'Twenty Cent Piece' => 44,
        'American Women' =>  131,
        'Crossing the Delaware' => 130,
        'America the Beautiful Quarter' => 42,
        'District of Columbia and US Territories' => 18,
        'State Quarter' => 17,
        'Washington Quarter' => 16,
        'Standing Liberty' => 15,
        'Barber Quarter' => 14,
        'Seated Liberty Quarter' => 13,
        'Draped Bust Quarter' => 12,
        'Liberty Cap Quarter' => 111,
        'Commemorative Quarter' => 114,
        'Capped Bust Quarter' => 41,
        'Franklin Half Dollar' => 37,
        'Walking Liberty' => 38,
        'Barber Half Dollar' => 39,
        'Kennedy Half Dollar' => 60,
        'Seated Liberty Half Dollar' => 57,
        'Draped Bust Half Dollar' => 54,
        'Flowing Hair Half Dollar' => 53,
        'Capped Bust Half Dollar' => 52,
        'Commemorative Half Dollar' => 29,
        'Silver American Eagle' => 101,
        'American Innovation Dollar' => 110,
        'Commemorative Gold Dollar' => 115,
        'Commemorative Dollar' => 112,
        'Susan B Anthony Dollar' => 40,
        'Peace Dollar' => 64,
        'Sacagawea Dollar' => 63,
        'Flowing Hair Dollar' => 62,
        'Draped Bust Dollar' => 61,
        'Seated Liberty Dollar' => 59,
        'Gobrecht Dollar' => 58,
        'Eisenhower Dollar' => 30,
        'Presidential Dollar' => 31,
        'Liberty Head Gold Dollar' => 55,
        'Trade Dollar' => 32,
        'Morgan Dollar' => 33,
        'Indian Princess Gold Dollar' => 65,
        'Liberty Cap Quarter Eagle' => 66,
        'Commemorative Quarter Eagle' => 116,
        'Indian Head Quarter Eagle' => 70,
        'Liberty Head Quarter Eagle' => 69,
        'Classic Head Quarter Eagle' => 68,
        'Turban Head Quarter Eagle' => 67,
        'Coronet Head Quarter Eagle' => 127,
        'Indian Princess Three Dollar' => 71,
        'Four Dollar Stella' => 72,
        'Commemorative Five Dollar' => 118,
        'Capped Bust Half Eagle' => 121,
        'Coronet Head Half Eagle' => 128,
        'Indian Head Half Eagle' => 78,
        'Tenth Ounce Gold' => 77,
        'Liberty Head Half Eagle' => 76,
        'Classic Head Half Eagle' => 75,
        'Liberty Cap Half Eagle' => 74,
        'Turban Head Half Eagle' => 73,
        'Tenth Ounce Buffalo' => 124,
        'Commemorative Ten Dollar' => 119,
        'Tenth Ounce Platinum' => 120,
        'Coronet Head Eagle' => 125,
        'Liberty Cap Eagle' => 126,
        'First Spouse' => 122,
        'Turban Head Eagle' => 79,
        'Liberty Head Eagle' => 80,
        'Indian Head Eagle' => 81,
        'Quarter Ounce Gold' => 82,
        'Quarter Ounce Buffalo' => 123,
        'Coronet Head Double Eagle' => 91,
        'Saint Gaudens Double Eagle' => 92,
        'Half Ounce Gold' => 98,
        'American Palladium Eagle' => 109,
        'Half Ounce Buffalo' => 93,
        'Quarter Ounce Platinum' => 94,
        'One Ounce Buffalo' => 97,
        'Commemorative Fifty Dollar' => 117,
        'One Ounce Gold' => 99,
        'Gold American Eagle' => 108,
        'Half Ounce Platinum' => 96,
        'One Ounce Platinum' => 100,
        'American Liberty Union' => 129,
    ];

    public const COIN_CATEGORIES = [
        45 => 'Half Cent',
        83 => 'Small Cent',
        46 => 'Large Cent',
        47 => 'Two Cent',
        48 => 'Three Cent',
        23 => 'Nickel',
        28 => 'Half Dime',
        43 => 'Dime',
        44 => 'Twenty Cent',
        84 => 'Quarter',
        37 => 'Half Dollar',
        63 => 'Dollar',
        87 => 'Gold Dollar',
        69 => 'Quarter Eagle',
        71 => 'Three Dollar',
        72 => 'Four Dollar',
        74 => 'Five Dollar',
        82 => 'Ten Dollar',
        86 => 'Twenty Dollar',
        89 => 'Twenty Five Dollar',
        90 => 'Fifty Dollar',
        91 => 'One Hundred Dollar'
    ];

    public const ONE_HUNDRED_DOLLAR = [
        91 => [100 => 'One Ounce Platinum', 129 => 'American Liberty Union']
    ];
    public const FIFTY_DOLLAR = [
        89 => [
            97 => 'One Ounce Buffalo',
            117 => 'Commemorative Fifty Dollar',
            99 => 'One Ounce Gold',
            108 => 'Gold American Eagle',
            96 => 'Half Ounce Platinum',
        ]
    ];
    public const TWENTY_FIVE_DOLLAR = [
        91 => [
            95 => 'Half Ounce Gold',
            109 => 'American Palladium Eagle',
            93 => 'Half Ounce Buffalo',
            94 => 'Quarter Ounce Platinum',
            98 => 'Half Ounce Gold',
        ]
    ];
    public const TWENTY_DOLLAR = [
        86 => [
            91 => 'Coronet Head Double Eagle',
            92 => 'Saint Gaudens Double Eagle',
        ]
    ];
    public const TEN_DOLLAR = [
        82 => [
            119 => 'Commemorative Ten Dollar',
            120 => 'Tenth Ounce Platinum',
            125 => 'Coronet Head Eagle',
            126 => 'Liberty Cap Eagle',
            122 => 'First Spouse',
            79 => 'Turban Head Eagle',
            80 => 'Liberty Head Eagle',
            81 => 'Indian Head Eagle',
            82 => 'Quarter Ounce Gold',
            123 => 'Quarter Ounce Buffalo',
        ]
    ];
    public const FIVE_DOLLAR = [
        74 => [
            118 => 'Commemorative Five Dollar',
            121 => 'Capped Bust Half Eagle',
            128 => 'Coronet Head Half Eagle',
            78 => 'Indian Head Half Eagle',
            77 => 'Tenth Ounce Gold',
            76 => 'Liberty Head Half Eagle',
            75 => 'Classic Head Half Eagle',
            74 => 'Liberty Cap Half Eagle',
            73 => 'Turban Head Half Eagle',
            124 => 'Tenth Ounce Buffalo',
        ]
    ];
    public const FOUR_DOLLAR = [
        72 => [72 => 'Four Dollar Stella']
    ];
    public const THREE_DOLLAR = [
        71 => [71 => 'Indian Princess Three Dollar']
    ];
    public const QUARTER_EAGLE = [
        69 => [
            66 => 'Liberty Cap Quarter Eagle',
            116 => 'Commemorative Quarter Eagle',
            70 => 'Indian Head Quarter Eagle',
            69 => 'Liberty Head Quarter Eagle',
            68 => 'Classic Head Quarter Eagle',
            67 => 'Turban Head Quarter Eagle',
            127 => 'Coronet Head Quarter Eagle',
        ]
    ];
    public const DOLLAR = [
        63 => [
            62 => 'Flowing Hair Dollar',
            107 => 'Mixed Dollars',
            61 => 'Draped Bust Dollar',
            58 => 'Gobrecht Dollar',
            59 => 'Seated Liberty Dollar',
            55 => 'Liberty Head Gold Dollar',
            65 => 'Indian Princess Gold Dollar',
            32 => 'Trade Dollar',
            33 => 'Morgan Dollar',
            112 => 'Commemorative Dollar',
            115 => 'Commemorative Gold Dollar',
            64 => 'Peace Dollar',
            30 => 'Eisenhower Dollar',
            40 => 'Susan B Anthony Dollar',
            101 => 'Silver American Eagle',
            63 => 'Sacagawea Dollar',
            31 => 'Presidential Dollar',
            110 => 'American Innovation Dollar',
        ]
    ];
    public const HALF_DOLLAR = [
        37 => [
            53 => 'Flowing Hair Half Dollar',
            106 => 'Mixed Half Dollars',
            54 => 'Draped Bust Half Dollar',
            52 => 'Capped Bust Half Dollar',
            57 => 'Seated Liberty Half Dollar',
            29 => 'Commemorative Half Dollar',
            39 => 'Barber Half Dollar',
            38 => 'Walking Liberty',
            37 => 'Franklin Half Dollar',
            60 => 'Kennedy Half Dollar',
        ]
    ];
    public const QUARTER = [
        84 => [
            12 => 'Draped Bust Quarter',
            105 => 'Mixed Quarters',
            41 => 'Capped Bust Quarter',
            111 => 'Liberty Cap Quarter',
            13 => 'Seated Liberty Quarter',
            14 => 'Barber Quarter',
            114 => 'Commemorative Quarter',
            15 => 'Standing Liberty',
            16 => 'Washington Quarter',
            17 => 'State Quarter',
            18 => 'District of Columbia and US Territories',
            42 => 'America the Beautiful Quarter',
            130 => 'Crossing the Delaware',
            131 => 'American Women',
        ]
    ];
    public const TWENTY_CENT = [
        44 => [44 => 'Twenty Cent Piece']
    ];
    public const DIME = [
        43 => [
            7 => 'Draped Bust Dime',
            104 => 'Mixed Dimes',
            43 => 'Capped Bust Dime',
            8 => 'Seated Liberty Dime',
            9 => 'Barber Dime',
            10 => 'Mercury Dime',
            11 => 'Roosevelt Dime',
        ]
    ];
    public const NICKEL = [
        23 => [
            20 => 'Shield Nickel',
            103 => 'Mixed Nickels',
            21 => 'Liberty Head Nickel',
            22 => 'Indian Head Nickel',
            23 => 'Jefferson Nickel',
            34 => 'Westward Journey',
            35 => 'Return to Monticello',
        ]
    ];
    public const HALF_DIME = [
        28 => [
            28 => 'Flowing Hair Half Dime',
            27 => 'Draped Bust Half Dime',
            26 => 'Capped Bust Half Dime',
            25 => 'Seated Liberty Half Dime',
        ]
    ];
    public const THREE_CENT = [
        48 => [
            50 => 'Silver Three Cent',
            84 => 'Nickel Three Cent',
        ]
    ];
    public const TWO_CENT = [
        91 => [49 => 'Two Cent Piece']
    ];
    public const SMALL_CENT = [
        83 => [
            1 => 'Flying Eagle',
            90 => 'Mixed Cents',
            102 => 'No Type',
            5 => 'Indian Head Cent',
            2 => 'Lincoln Wheat',
            3 => 'Lincoln Memorial',
            6 => 'Lincoln Bicentennial',
            4 => 'Union Shield',
        ]
    ];
    public const LARGE_CENT = [
        46 => [
            51 => 'Flowing Hair Large Cent',
            85 => 'Liberty Cap Large Cent',
            86 => 'Draped Bust Large Cent',
            87 => 'Classic Head Large Cent',
            88 => 'Coronet Head Cent',
            89 => 'Braided Hair Liberty Head Large Cent',
        ]
    ];
    public const HALF_CENT = [
        45 => [
            48 => 'Liberty Cap Half Cent',
            47 => 'Draped Bust Half Cent',
            46 => 'Classic Head Half Cent',
            45 => 'Braided Hair Half Cent',
        ]
    ];


    public const COMBINED_LIST = [
        91 => [100 => 'One Ounce Platinum', 129 => 'American Liberty Union'],
        90 => [
            97 => 'One Ounce Buffalo',
            117 => 'Commemorative Fifty Dollar',
            99 => 'One Ounce Gold',
            108 => 'Gold American Eagle',
            96 => 'Half Ounce Platinum',
        ],
        89 => [
            95 => 'Half Ounce Gold',
            109 => 'American Palladium Eagle',
            93 => 'Half Ounce Buffalo',
            94 => 'Quarter Ounce Platinum',
            98 => 'Half Ounce Gold',
        ],
        86 => [
            91 => 'Coronet Head Double Eagle',
            92 => 'Saint Gaudens Double Eagle',
        ],
        82 => [
            119 => 'Commemorative Ten Dollar',
            120 => 'Tenth Ounce Platinum',
            125 => 'Coronet Head Eagle',
            126 => 'Liberty Cap Eagle',
            122 => 'First Spouse',
            79 => 'Turban Head Eagle',
            80 => 'Liberty Head Eagle',
            81 => 'Indian Head Eagle',
            82 => 'Quarter Ounce Gold',
            123 => 'Quarter Ounce Buffalo',
        ],
        74 => [
            118 => 'Commemorative Five Dollar',
            121 => 'Capped Bust Half Eagle',
            128 => 'Coronet Head Half Eagle',
            78 => 'Indian Head Half Eagle',
            77 => 'Tenth Ounce Gold',
            76 => 'Liberty Head Half Eagle',
            75 => 'Classic Head Half Eagle',
            74 => 'Liberty Cap Half Eagle',
            73 => 'Turban Head Half Eagle',
            124 => 'Tenth Ounce Buffalo',
        ],
        72 => [72 => 'Four Dollar Stella'],
        71 => [71 => 'Indian Princess Three Dollar'],
        69 => [
            66 => 'Liberty Cap Quarter Eagle',
            116 => 'Commemorative Quarter Eagle',
            70 => 'Indian Head Quarter Eagle',
            69 => 'Liberty Head Quarter Eagle',
            68 => 'Classic Head Quarter Eagle',
            67 => 'Turban Head Quarter Eagle',
            127 => 'Coronet Head Quarter Eagle',
        ],
        63 => [
            62 => 'Flowing Hair Dollar',
            61 => 'Draped Bust Dollar',
            58 => 'Gobrecht Dollar',
            59 => 'Seated Liberty Dollar',
            55 => 'Liberty Head Gold Dollar',
            65 => 'Indian Princess Gold Dollar',
            32 => 'Trade Dollar',
            33 => 'Morgan Dollar',
            112 => 'Commemorative Dollar',
            115 => 'Commemorative Gold Dollar',
            64 => 'Peace Dollar',
            30 => 'Eisenhower Dollar',
            40 => 'Susan B Anthony Dollar',
            101 => 'Silver American Eagle',
            63 => 'Sacagawea Dollar',
            31 => 'Presidential Dollar',
            110 => 'American Innovation Dollar',
        ],
        37 => [
            53 => 'Flowing Hair Half Dollar',
            54 => 'Draped Bust Half Dollar',
            52 => 'Capped Bust Half Dollar',
            57 => 'Seated Liberty Half Dollar',
            29 => 'Commemorative Half Dollar',
            39 => 'Barber Half Dollar',
            38 => 'Walking Liberty',
            37 => 'Franklin Half Dollar',
            60 => 'Kennedy Half Dollar',
        ],
        84 => [
            12 => 'Draped Bust Quarter',
            41 => 'Capped Bust Quarter',
            111 => 'Liberty Cap Quarter',
            13 => 'Seated Liberty Quarter',
            14 => 'Barber Quarter',
            114 => 'Commemorative Quarter',
            15 => 'Standing Liberty',
            16 => 'Washington Quarter',
            17 => 'State Quarter',
            18 => 'District of Columbia and US Territories',
            42 => 'America the Beautiful Quarter',
            130 => 'Crossing the Delaware',
            131 => 'American Women',
        ],
        44 => [44 => 'Twenty Cent Piece'],
        43 => [
            7 => 'Draped Bust Dime',
            43 => 'Capped Bust Dime',
            8 => 'Seated Liberty Dime',
            9 => 'Barber Dime',
            10 => 'Mercury Dime',
            11 => 'Roosevelt Dime',
        ],
        23 => [
            20 => 'Shield Nickel',
            21 => 'Liberty Head Nickel',
            22 => 'Indian Head Nickel',
            23 => 'Jefferson Nickel',
            34 => 'Westward Journey',
            35 => 'Return to Monticello',
        ],
        28 => [
            28 => 'Flowing Hair Half Dime',
            27 => 'Draped Bust Half Dime',
            26 => 'Capped Bust Half Dime',
            25 => 'Seated Liberty Half Dime',
        ],
        48 => [
            50 => 'Silver Three Cent',
            84 => 'Nickel Three Cent',
        ],
        47 => [49 => 'Two Cent Piece'],
        83 => [
            1 => 'Flying Eagle',
            5 => 'Indian Head Cent',
            2 => 'Lincoln Wheat',
            3 => 'Lincoln Memorial',
            6 => 'Lincoln Bicentennial',
            4 => 'Union Shield',
        ],
        46 => [
            51 => 'Flowing Hair Large Cent',
            85 => 'Liberty Cap Large Cent',
            86 => 'Draped Bust Large Cent',
            87 => 'Classic Head Large Cent',
            88 => 'Coronet Head Cent',
            89 => 'Braided Hair Liberty Head Large Cent',
        ],
        29 => [
            48 => 'Liberty Cap Half Cent',
            47 => 'Draped Bust Half Cent',
            46 => 'Classic Head Half Cent',
            45 => 'Braided Hair Half Cent',
        ]
    ];



    public const EARLY_AMERICAN_COPPERS = [
        'Half Cent' => 29,
        'Large Cent' => 46,
    ];

    /**
     * @var array[]
     */
    public const SNOW_TYPES = ['Indian Head', 'Flying Eagle'];

    /**
     * @var array[]
     */
    public const VAM_TYPES = [33 => 'Morgan Dollar', 64 => 'Peace Dollar'];



    /**
     * Array of copper coins to add color attribute
     * @return array
     */
    public const COLOR_CATEGORIES = [
        29 => 'Half Cent',
        83 => 'Small Cent',
        46 => 'Large Cent',
    ];

    /**
     * Array of copper coins to add color attribute
     * @return array
     */
    public const COIN_MINTS = [
        'Carson City' => 'CC',
        'Charlotte' => 'C',
        'Dahlonega' => 'D',
        'Denver' => 'D',
        'New Orleans' => 'O',
        'Philadelphia' => 'P',
        'San Francisco' => 'S',
        'West Point' => 'W',
    ];




}
