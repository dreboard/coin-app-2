<?php /** @noinspection ALL */

namespace App\Helpers\Types;

use App\Models\Coins\Coin;
use App\Models\Coins\CoinType;
use Illuminate\Support\Facades\Log;
use Throwable;

abstract class TypeHelper
{

    // SELECT * FROM `cointypes` ORDER BY `denomination`, LEFT(`dates`, 4) ASC;
    /**
     * @var array
     */

    public const COIN_TYPES = [
        //113 => 'No Type',
        48 => 'Liberty Cap Half Cent',
        47 => 'Draped Bust Half Cent',
        46 => 'Classic Head Half Cent',
        45 => 'Braided Hair Half Cent',
        51 => 'Flowing Hair Large Cent',
        85 => 'Liberty Cap Large Cent',
        86 => 'Draped Bust Large Cent',
        87 => 'Classic Head Large Cent',
        88 => 'Coronet Head Cent',
        89 => 'Braided Hair Liberty Head Large Cent',
        1 => 'Flying Eagle',
        //90 => 'Mixed Cents',
        //102 => 'No Type',
        5 => 'Indian Head Cent',
        2 => 'Lincoln Wheat',
        3 => 'Lincoln Memorial',
        6 => 'Lincoln Bicentennial',
        4 => 'Union Shield',
        49 => 'Two Cent Piece',
        50 => 'Silver Three Cent',
        84 => 'Nickel Three Cent',
        28 => 'Flowing Hair Half Dime',
        27 => 'Draped Bust Half Dime',
        26 => 'Capped Bust Half Dime',
        25 => 'Seated Liberty Half Dime',
        20 => 'Shield Nickel',
        //103 => 'Mixed Nickels',
        21 => 'Liberty Head Nickel',
        22 => 'Indian Head Nickel',
        23 => 'Jefferson Nickel',
        34 => 'Westward Journey',
        35 => 'Return to Monticello',
        7 => 'Draped Bust Dime',
        //104 => 'Mixed Dimes',
        43 => 'Capped Bust Dime',
        8 => 'Seated Liberty Dime',
        9 => 'Barber Dime',
        10 => 'Mercury Dime',
        11 => 'Roosevelt Dime',
        44 => 'Twenty Cent Piece',
        12 => 'Draped Bust Quarter',
        //105 => 'Mixed Quarters',
        111 => 'Capped Bust Quarter',
        13 => 'Seated Liberty Quarter',
        14 => 'Barber Quarter',
        114 => 'Commemorative Quarter',
        15 => 'Standing Liberty',
        16 => 'Washington Quarter',
        17 => 'State Quarter',
        18 => 'District of Columbia and US Territories',
        42 => 'America the Beautiful Quarter',
        130 => 'Crossing the Delaware',
        131 => 'American Women',
        53 => 'Flowing Hair Half Dollar',
        //106 => 'Mixed Half Dollars',
        54 => 'Draped Bust Half Dollar',
        52 => 'Capped Bust Half Dollar',
        57 => 'Seated Liberty Half Dollar',
        29 => 'Commemorative Half Dollar',
        39 => 'Barber Half Dollar',
        38 => 'Walking Liberty',
        37 => 'Franklin Half Dollar',
        60 => 'Kennedy Half Dollar',
        62 => 'Flowing Hair Dollar',
        //107 => 'Mixed Dollars',
        61 => 'Draped Bust Dollar',
        58 => 'Gobrecht Dollar',
        59 => 'Seated Liberty Dollar',
        55 => 'Liberty Head Gold Dollar',
        65 => 'Indian Princess Gold Dollar',
        32 => 'Trade Dollar',
        33 => 'Morgan Dollar',
        112 => 'Commemorative Dollar',
        115 => 'Commemorative Gold Dollar',
        64 => 'Peace Dollar',
        30 => 'Eisenhower Dollar',
        40 => 'Susan B Anthony Dollar',
        101 => 'Silver American Eagle',
        63 => 'Sacagawea Dollar',
        31 => 'Presidential Dollar',
        110 => 'American Innovation Dollar',
        66 => 'Draped Bust Quarter Eagle',
        67 => 'Capped Bust Quarter Eagle',
        68 => 'Classic Head Quarter Eagle',
        69 => 'Liberty Head Quarter Eagle',
        70 => 'Indian Head Quarter Eagle',
        116 => 'Commemorative Quarter Eagle',
        71 => 'Indian Princess Three Dollar',
        72 => 'Four Dollar Stella',
        73 => 'Turban Head Half Eagle',
        74 => 'Liberty Cap Half Eagle',
        75 => 'Classic Head Half Eagle',
        76 => 'Liberty Head Half Eagle',
        128 => 'Coronet Head Half Eagle',
        121 => 'Capped Bust Half Eagle',
        78 => 'Indian Head Half Eagle',
        77 => 'Tenth Ounce Gold',
        118 => 'Commemorative Five Dollar',
        124 => 'Tenth Ounce Buffalo',
        79 => 'Turban Head Eagle',
        126 => 'Liberty Cap Eagle',
        80 => 'Liberty Head Eagle',
        125 => 'Coronet Head Eagle',
        81 => 'Indian Head Eagle',
        119 => 'Commemorative Ten Dollar',
        82 => 'Quarter Ounce Gold',
        120 => 'Tenth Ounce Platinum',
        122 => 'First Spouse',
        123 => 'Quarter Ounce Buffalo',
        91 => 'Liberty Head Double Eagle',
        92 => 'Saint Gaudens Double Eagle',
        95 => 'Half Ounce Gold',
        94 => 'Quarter Ounce Platinum',
        93 => 'Half Ounce Buffalo',
        109 => 'American Palladium Eagle',
        117 => 'Commemorative Fifty Dollar',
        99 => 'One Ounce Gold',
        96 => 'Half Ounce Platinum',
        108 => 'Gold American Eagle',
        97 => 'One Ounce Buffalo',
        100 => 'One Ounce Platinum',
        129 => 'American Liberty Union',
    ];



    public const TYPES_LIST = [
        'Liberty Cap Half Cent' => 48,
        'Braided Hair Half Cent' => 45,
        'Classic Head Half Cent' => 46,
        'Draped Bust Half Cent' => 47,
        'Flying Eagle' => 1,
        'Flowing Hair Large Cent' => 51,
        'Braided Hair Liberty Head Large Cent' => 89,
        'Coronet Head Cent' => 88,
        'Classic Head Large Cent' => 87,
        'Draped Bust Large Cent' => 86,
        'Lincoln Wheat' => 2,
        'Lincoln Memorial' => 3,
        'Union Shield' => 4,
        'Indian Head Cent' => 5,
        'Lincoln Bicentennial' => 6,
        'Liberty Cap Large Cent' => 85,
        'Two Cent Piece' => 49,
        'Silver Three Cent' => 50,
        'Nickel Three Cent' => 84,
        'Return to Monticello' => 35,
        'Flowing Hair Half Dime' => 28,
        'Draped Bust Half Dime' => 27,
        'Capped Bust Half Dime' => 26,
        'Seated Liberty Half Dime' => 25,
        'Jefferson Nickel' => 23,
        'Indian Head Nickel' => 22,
        'Liberty Head Nickel' => 21,
        'Shield Nickel' => 20,
        'Westward Journey' => 34,
        'Capped Bust Dime' => 43,
        'Seated Liberty Dime' => 8,
        'Draped Bust Dime' => 7,
        'Barber Dime' => 9,
        'Mercury Dime' => 10,
        'Roosevelt Dime' => 11,
        'Twenty Cent Piece' => 44,
        'American Women' =>  131,
        'Crossing the Delaware' => 130,
        'America the Beautiful Quarter' => 42,
        'District of Columbia and US Territories' => 18,
        'State Quarter' => 17,
        'Washington Quarter' => 16,
        'Standing Liberty' => 15,
        'Barber Quarter' => 14,
        'Seated Liberty Quarter' => 13,
        'Draped Bust Quarter' => 12,
        'Capped Bust Quarter' => 111,
        'Commemorative Quarter' => 114,
        'Franklin Half Dollar' => 37,
        'Walking Liberty' => 38,
        'Barber Half Dollar' => 39,
        'Kennedy Half Dollar' => 60,
        'Seated Liberty Half Dollar' => 57,
        'Draped Bust Half Dollar' => 54,
        'Flowing Hair Half Dollar' => 53,
        'Capped Bust Half Dollar' => 52,
        'Commemorative Half Dollar' => 29,
        'Silver American Eagle' => 101,
        'American Innovation Dollar' => 110,
        'Commemorative Gold Dollar' => 115,
        'Commemorative Dollar' => 112,
        'Susan B Anthony Dollar' => 40,
        'Peace Dollar' => 64,
        'Sacagawea Dollar' => 63,
        'Flowing Hair Dollar' => 62,
        'Draped Bust Dollar' => 61,
        'Seated Liberty Dollar' => 59,
        'Gobrecht Dollar' => 58,
        'Eisenhower Dollar' => 30,
        'Presidential Dollar' => 31,
        'Liberty Head Gold Dollar' => 55,
        'Trade Dollar' => 32,
        'Morgan Dollar' => 33,
        'Indian Princess Gold Dollar' => 65,
        'Liberty Cap Quarter Eagle' => 66,
        'Commemorative Quarter Eagle' => 116,
        'Indian Head Quarter Eagle' => 70,
        'Liberty Head Quarter Eagle' => 69,
        'Classic Head Quarter Eagle' => 68,
        'Turban Head Quarter Eagle' => 67,
        'Indian Princess Three Dollar' => 71,
        'Four Dollar Stella' => 72,
        'Commemorative Five Dollar' => 118,
        'Capped Bust Half Eagle' => 121,
        'Coronet Head Half Eagle' => 128,
        'Indian Head Half Eagle' => 78,
        'Tenth Ounce Gold' => 77,
        'Liberty Head Half Eagle' => 76,
        'Classic Head Half Eagle' => 75,
        'Liberty Cap Half Eagle' => 74,
        'Turban Head Half Eagle' => 73,
        'Tenth Ounce Buffalo' => 124,
        'Commemorative Ten Dollar' => 119,
        'Tenth Ounce Platinum' => 120,
        'Coronet Head Eagle' => 125,
        'Liberty Cap Eagle' => 126,
        'First Spouse' => 122,
        'Turban Head Eagle' => 79,
        'Liberty Head Eagle' => 80,
        'Indian Head Eagle' => 81,
        'Quarter Ounce Gold' => 82,
        'Quarter Ounce Buffalo' => 123,
        'Liberty Head Double Eagle' => 91,
        'Saint Gaudens Double Eagle' => 92,
        'Half Ounce Gold' => 95,
        'American Palladium Eagle' => 109,
        'Half Ounce Buffalo' => 93,
        'Quarter Ounce Platinum' => 94,
        'One Ounce Buffalo' => 97,
        'Commemorative Fifty Dollar' => 117,
        'One Ounce Gold' => 99,
        'Gold American Eagle' => 108,
        'Half Ounce Platinum' => 96,
        'One Ounce Platinum' => 100,
        'American Liberty Union' => 129,
    ];

    public const COIN_CATEGORIES = [
        45 => 'Half Cent',
        83 => 'Small Cent',
        46 => 'Large Cent',
        47 => 'Two Cent',
        48 => 'Three Cent',
        23 => 'Nickel',
        28 => 'Half Dime',
        43 => 'Dime',
        44 => 'Twenty Cent',
        84 => 'Quarter',
        37 => 'Half Dollar',
        63 => 'Dollar',
        87 => 'Gold Dollar',
        69 => 'Quarter Eagle',
        71 => 'Three Dollar',
        72 => 'Four Dollar',
        74 => 'Five Dollar',
        82 => 'Ten Dollar',
        86 => 'Twenty Dollar',
        89 => 'Twenty Five Dollar',
        90 => 'Fifty Dollar',
        91 => 'One Hundred Dollar'
    ];

    public const COIN_CATEGORIES_LIST = [
        'Half Cent' => 45,
         'Small Cent' => 83,
         'Large Cent' => 46,
        'Two Cent' => 47,
        'Three Cent' => 48,
         'Nickel' => 23,
         'Half Dime' => 28,
         'Dime' => 43,
         'Twenty Cen => 44t',
         'Quarter' => 84,
         'Half Dollar' => 37,
         'Dollar' => 63,
         'Gold Dollar' => 87,
         'Quarter Eagle' => 69,
         'Three Dollar' => 71,
         'Four Dollar' => 72,
        'Five Dollar' => 74,
        'Ten Dollar' => 82,
        'Twenty Dollar' => 86,
        'Twenty Five Dollar' => 89,
        'Fifty Dollar' => 90,
        'One Hundred Dollar => 91'
    ];

    public const ONE_HUNDRED_DOLLAR = [
        91 => [100 => 'One Ounce Platinum', 129 => 'American Liberty Union']
    ];
    public const FIFTY_DOLLAR = [
        89 => [
            97 => 'One Ounce Buffalo',
            117 => 'Commemorative Fifty Dollar',
            99 => 'One Ounce Gold',
            108 => 'Gold American Eagle',
            96 => 'Half Ounce Platinum',
        ]
    ];
    public const TWENTY_FIVE_DOLLAR = [
        91 => [
            95 => 'Half Ounce Gold',
            109 => 'American Palladium Eagle',
            93 => 'Half Ounce Buffalo',
            94 => 'Quarter Ounce Platinum',
        ]
    ];
    public const TWENTY_DOLLAR = [
        86 => [
            91 => 'Liberty Head Double Eagle',
            92 => 'Saint Gaudens Double Eagle',
        ]
    ];
    public const TEN_DOLLAR = [
        82 => [
            119 => 'Commemorative Ten Dollar',
            120 => 'Tenth Ounce Platinum',
            125 => 'Coronet Head Eagle',
            126 => 'Liberty Cap Eagle',
            122 => 'First Spouse',
            79 => 'Turban Head Eagle',
            80 => 'Liberty Head Eagle',
            81 => 'Indian Head Eagle',
            82 => 'Quarter Ounce Gold',
            123 => 'Quarter Ounce Buffalo',
        ]
    ];
    public const FIVE_DOLLAR = [
        74 => [
            118 => 'Commemorative Five Dollar',
            121 => 'Capped Bust Half Eagle',
            128 => 'Coronet Head Half Eagle',
            78 => 'Indian Head Half Eagle',
            77 => 'Tenth Ounce Gold',
            76 => 'Liberty Head Half Eagle',
            75 => 'Classic Head Half Eagle',
            74 => 'Liberty Cap Half Eagle',
            73 => 'Turban Head Half Eagle',
            124 => 'Tenth Ounce Buffalo',
        ]
    ];
    public const FOUR_DOLLAR = [
        72 => [72 => 'Four Dollar Stella']
    ];
    public const THREE_DOLLAR = [
        71 => [71 => 'Indian Princess Three Dollar']
    ];
    public const QUARTER_EAGLE = [
        69 => [
            66 => 'Liberty Cap Quarter Eagle',
            116 => 'Commemorative Quarter Eagle',
            70 => 'Indian Head Quarter Eagle',
            69 => 'Liberty Head Quarter Eagle',
            68 => 'Classic Head Quarter Eagle',
            67 => 'Turban Head Quarter Eagle',
        ]
    ];
    public const DOLLAR = [
        63 => [
            62 => 'Flowing Hair Dollar',
            107 => 'Mixed Dollars',
            61 => 'Draped Bust Dollar',
            58 => 'Gobrecht Dollar',
            59 => 'Seated Liberty Dollar',
            55 => 'Liberty Head Gold Dollar',
            65 => 'Indian Princess Gold Dollar',
            32 => 'Trade Dollar',
            33 => 'Morgan Dollar',
            112 => 'Commemorative Dollar',
            115 => 'Commemorative Gold Dollar',
            64 => 'Peace Dollar',
            30 => 'Eisenhower Dollar',
            40 => 'Susan B Anthony Dollar',
            101 => 'Silver American Eagle',
            63 => 'Sacagawea Dollar',
            31 => 'Presidential Dollar',
            110 => 'American Innovation Dollar',
        ]
    ];
    public const HALF_DOLLAR = [
        37 => [
            53 => 'Flowing Hair Half Dollar',
            106 => 'Mixed Half Dollars',
            54 => 'Draped Bust Half Dollar',
            52 => 'Capped Bust Half Dollar',
            57 => 'Seated Liberty Half Dollar',
            29 => 'Commemorative Half Dollar',
            39 => 'Barber Half Dollar',
            38 => 'Walking Liberty',
            37 => 'Franklin Half Dollar',
            60 => 'Kennedy Half Dollar',
        ]
    ];
    public const QUARTER = [
        84 => [
            12 => 'Draped Bust Quarter',
            105 => 'Mixed Quarters',
            111 => 'Capped Bust Quarter',
            13 => 'Seated Liberty Quarter',
            14 => 'Barber Quarter',
            114 => 'Commemorative Quarter',
            15 => 'Standing Liberty',
            16 => 'Washington Quarter',
            17 => 'State Quarter',
            18 => 'District of Columbia and US Territories',
            42 => 'America the Beautiful Quarter',
            130 => 'Crossing the Delaware',
            131 => 'American Women',
        ]
    ];
    public const TWENTY_CENT = [
        44 => [44 => 'Twenty Cent Piece']
    ];
    public const DIME = [
        43 => [
            7 => 'Draped Bust Dime',
            104 => 'Mixed Dimes',
            43 => 'Capped Bust Dime',
            8 => 'Seated Liberty Dime',
            9 => 'Barber Dime',
            10 => 'Mercury Dime',
            11 => 'Roosevelt Dime',
        ]
    ];
    public const NICKEL = [
        23 => [
            20 => 'Shield Nickel',
            103 => 'Mixed Nickels',
            21 => 'Liberty Head Nickel',
            22 => 'Indian Head Nickel',
            23 => 'Jefferson Nickel',
            34 => 'Westward Journey',
            35 => 'Return to Monticello',
        ]
    ];
    public const HALF_DIME = [
        28 => [
            28 => 'Flowing Hair Half Dime',
            27 => 'Draped Bust Half Dime',
            26 => 'Capped Bust Half Dime',
            25 => 'Seated Liberty Half Dime',
        ]
    ];
    public const THREE_CENT = [
        48 => [
            50 => 'Silver Three Cent',
            84 => 'Nickel Three Cent',
        ]
    ];
    public const TWO_CENT = [
        91 => [49 => 'Two Cent Piece']
    ];
    public const SMALL_CENT = [
        83 => [
            1 => 'Flying Eagle',
            90 => 'Mixed Cents',
            102 => 'No Type',
            5 => 'Indian Head Cent',
            2 => 'Lincoln Wheat',
            3 => 'Lincoln Memorial',
            6 => 'Lincoln Bicentennial',
            4 => 'Union Shield',
        ]
    ];
    public const LARGE_CENT = [
        46 => [
            51 => 'Flowing Hair Large Cent',
            85 => 'Liberty Cap Large Cent',
            86 => 'Draped Bust Large Cent',
            87 => 'Classic Head Large Cent',
            88 => 'Coronet Head Cent',
            89 => 'Braided Hair Liberty Head Large Cent',
        ]
    ];
    public const HALF_CENT = [
        45 => [
            48 => 'Liberty Cap Half Cent',
            47 => 'Draped Bust Half Cent',
            46 => 'Classic Head Half Cent',
            45 => 'Braided Hair Half Cent',
        ]
    ];


    public const COMBINED_LIST = [
        91 => [100 => 'One Ounce Platinum', 129 => 'American Liberty Union'],
        90 => [
            97 => 'One Ounce Buffalo',
            117 => 'Commemorative Fifty Dollar',
            99 => 'One Ounce Gold',
            108 => 'Gold American Eagle',
            96 => 'Half Ounce Platinum',
        ],
        89 => [
            95 => 'Half Ounce Gold',
            109 => 'American Palladium Eagle',
            93 => 'Half Ounce Buffalo',
            94 => 'Quarter Ounce Platinum',
        ],
        86 => [
            91 => 'Liberty Head Double Eagle',
            92 => 'Saint Gaudens Double Eagle',
        ],
        82 => [
            119 => 'Commemorative Ten Dollar',
            120 => 'Tenth Ounce Platinum',
            125 => 'Coronet Head Eagle',
            126 => 'Liberty Cap Eagle',
            122 => 'First Spouse',
            79 => 'Turban Head Eagle',
            80 => 'Liberty Head Eagle',
            81 => 'Indian Head Eagle',
            82 => 'Quarter Ounce Gold',
            123 => 'Quarter Ounce Buffalo',
        ],
        74 => [
            118 => 'Commemorative Five Dollar',
            121 => 'Capped Bust Half Eagle',
            128 => 'Coronet Head Half Eagle',
            78 => 'Indian Head Half Eagle',
            77 => 'Tenth Ounce Gold',
            76 => 'Liberty Head Half Eagle',
            75 => 'Classic Head Half Eagle',
            74 => 'Liberty Cap Half Eagle',
            73 => 'Turban Head Half Eagle',
            124 => 'Tenth Ounce Buffalo',
        ],
        72 => [72 => 'Four Dollar Stella'],
        71 => [71 => 'Indian Princess Three Dollar'],
        69 => [
            66 => 'Liberty Cap Quarter Eagle',
            116 => 'Commemorative Quarter Eagle',
            70 => 'Indian Head Quarter Eagle',
            69 => 'Liberty Head Quarter Eagle',
            68 => 'Classic Head Quarter Eagle',
            67 => 'Turban Head Quarter Eagle',
        ],
        87 => [
            55 => 'Liberty Head Gold Dollar',
            65 => 'Indian Princess Gold Dollar',
            115 => 'Commemorative Gold Dollar',
        ],
        63 => [
            62 => 'Flowing Hair Dollar',
            61 => 'Draped Bust Dollar',
            58 => 'Gobrecht Dollar',
            59 => 'Seated Liberty Dollar',
            55 => 'Liberty Head Gold Dollar',
            65 => 'Indian Princess Gold Dollar',
            32 => 'Trade Dollar',
            33 => 'Morgan Dollar',
            112 => 'Commemorative Dollar',
            115 => 'Commemorative Gold Dollar',
            64 => 'Peace Dollar',
            30 => 'Eisenhower Dollar',
            40 => 'Susan B Anthony Dollar',
            101 => 'Silver American Eagle',
            63 => 'Sacagawea Dollar',
            31 => 'Presidential Dollar',
            110 => 'American Innovation Dollar',
        ],
        37 => [
            53 => 'Flowing Hair Half Dollar',
            54 => 'Draped Bust Half Dollar',
            52 => 'Capped Bust Half Dollar',
            57 => 'Seated Liberty Half Dollar',
            29 => 'Commemorative Half Dollar',
            39 => 'Barber Half Dollar',
            38 => 'Walking Liberty',
            37 => 'Franklin Half Dollar',
            60 => 'Kennedy Half Dollar',
        ],
        84 => [
            12 => 'Draped Bust Quarter',
            111 => 'Capped Bust Quarter',
            13 => 'Seated Liberty Quarter',
            14 => 'Barber Quarter',
            114 => 'Commemorative Quarter',
            15 => 'Standing Liberty',
            16 => 'Washington Quarter',
            17 => 'State Quarter',
            18 => 'District of Columbia and US Territories',
            42 => 'America the Beautiful Quarter',
            130 => 'Crossing the Delaware',
            131 => 'American Women',
        ],
        44 => [44 => 'Twenty Cent Piece'],
        43 => [
            7 => 'Draped Bust Dime',
            43 => 'Capped Bust Dime',
            8 => 'Seated Liberty Dime',
            9 => 'Barber Dime',
            10 => 'Mercury Dime',
            11 => 'Roosevelt Dime',
        ],
        23 => [
            20 => 'Shield Nickel',
            21 => 'Liberty Head Nickel',
            22 => 'Indian Head Nickel',
            23 => 'Jefferson Nickel',
            34 => 'Westward Journey',
            35 => 'Return to Monticello',
        ],
        28 => [
            28 => 'Flowing Hair Half Dime',
            27 => 'Draped Bust Half Dime',
            26 => 'Capped Bust Half Dime',
            25 => 'Seated Liberty Half Dime',
        ],
        48 => [
            50 => 'Silver Three Cent',
            84 => 'Nickel Three Cent',
        ],
        47 => [49 => 'Two Cent Piece'],
        83 => [
            1 => 'Flying Eagle',
            5 => 'Indian Head Cent',
            2 => 'Lincoln Wheat',
            3 => 'Lincoln Memorial',
            6 => 'Lincoln Bicentennial',
            4 => 'Union Shield',
        ],
        46 => [
            51 => 'Flowing Hair Large Cent',
            85 => 'Liberty Cap Large Cent',
            86 => 'Draped Bust Large Cent',
            87 => 'Classic Head Large Cent',
            88 => 'Coronet Head Cent',
            89 => 'Braided Hair Liberty Head Large Cent',
        ],
        45 => [
            48 => 'Liberty Cap Half Cent',
            47 => 'Draped Bust Half Cent',
            46 => 'Classic Head Half Cent',
            45 => 'Braided Hair Half Cent',
        ]
    ];


    /**
     * Array of early copper categories
     * @example {k=cointypes_id, v=coinType}
     * @return array
     */
    public const EARLY_AMERICAN_COPPERS = [
        29 => 'Half Cent',
        46 => 'Large Cent',
    ];

    /**
     * Array of SNOW types
     * @example {k=cointypes_id, v=coinType}
     * @return array
     */
    public const SNOW_TYPES = [
        1 => 'Flying Eagle',
        5 => 'Indian Head Cent',
    ];

    public const LINCOLN_TYPES = [
        2 => 'Lincoln Wheat',
        3 => 'Lincoln Memorial',
        6 => 'Lincoln Bicentennial',
        4 => 'Union Shield',
    ];

    public const JEFFERSON_TYPES = [
        23 => 'Jefferson Nickel',
        34 => 'Westward Journey',
        35 => 'Return to Monticello',
    ];

    /**
     * Array of VAM types
     * @example {k=cointypes_id, v=coinType}
     * @return array
     */
    public const VAM_TYPES = [33 => 'Morgan Dollar', 64 => 'Peace Dollar'];



    /**
     * Array of copper coins to add color attribute
     * @example {k=cointypes_id, v=coinType}
     * @return array
     */
    public const STEP_TYPES = [
        29 => 'Half Cent',
        83 => 'Small Cent',
        46 => 'Large Cent',
    ];

    /**
     * Array of copper coins to add color attribute
     * @example {k=cointypes_id, v=coinType}
     * @return array
     */
    public const COLOR_CATEGORIES = [
        29 => 'Half Cent',
        83 => 'Small Cent',
        46 => 'Large Cent',
        47 => 'Two Cent',
    ];


    /**
     * Array of copper coins to add color attribute
     * @example {k=cointypes_id, v=coinType}
     * @return array
     */
    static public function getColorCategories(): array
    {
        return [
            29 => 'Half Cent',
            83 => 'Small Cent',
            46 => 'Large Cent',
            47 => 'Two Cent',
        ];
    }


    /**
     * Array of full types, head, steps, bell
     * @example {k=cointypes_id, v=coinType}
     * @return array
     */
    public array $fullTypes = [
        'Jefferson Nickel',
        'Standing Liberty',
        'Mercury Dime',
        'Franklin Half Dollar',
        'Roosevelt Dime'
    ];

    /**
     * Array of Seated Types
     * @example {k=cointypes_id, v=coinType}
     * @return array
     */
    static public function getSeatedTypes(): array
    {
        //return DB::raw('SELECT * FROM ViewSeatedLibertyTypes');
        return [
            8 => 'Seated Liberty Half Dime',
            13 => 'Seated Liberty Dime',
            25 => 'Seated Liberty Quarter',
            57 => 'Seated Liberty Half Dollar',
            59 => 'Seated Liberty Dollar'
        ];
    }

    /**
     * Array of Draped Types
     * @example {k=cointypes_id, v=coinType}
     * @return array
     */
    static public function getDrapedTypes(): array
    {
        return [
            7 => 'Draped Bust Half Cent',
            12 => 'Draped Bust Large Cent',
            27 => 'Draped Bust Half Dime',
            47 => 'Draped Bust Dime',
            54 => 'Draped Bust Quarter',
            61 => 'Draped Bust Half Dollar',
            86 => 'Draped Bust Dollar'
        ];
    }

    /**
     * Array of Capped Types
     * @example {k=cointypes_id, v=coinType}
     * @return array
     */
    static public function getCappedTypes(): array
    {
        return [
            26 => 'Capped Bust Half Dime',
            111 => 'Capped Bust Quarter',
            43 =>'Capped Bust Quarter',
            52 => 'Capped Bust Half Dollar',
            121 => 'Capped Bust Half Eagle'
        ];
    }

    /**
     * Array of Full Bands Types
     * @example {k=cointypes_id, v=coinType}
     * @return array
     */
    public const FULL_BAND_TYPES = [
            11 => 'Roosevelt Dime',
            10 => 'Mercury Dime',
        ];


    /**
     * Types in mint sets
     * @var array
     */
    public const MINTSET_TYPES = [

        2 => 'Lincoln Wheat',
        3 => 'Lincoln Memorial',
        6 => 'Lincoln Bicentennial',
        4 => 'Union Shield',
        23 => 'Jefferson Nickel',
        34 => 'Westward Journey',
        35 => 'Return to Monticello',
        10 => 'Mercury Dime',
        11 => 'Roosevelt Dime',

        15 => 'Standing Liberty',
        16 => 'Washington Quarter',
        17 => 'State Quarter',
        18 => 'District of Columbia and US Territories',
        42 => 'America the Beautiful Quarter',
        130 => 'Crossing the Delaware',
        131 => 'American Women',

        38 => 'Walking Liberty',
        37 => 'Franklin Half Dollar',
        60 => 'Kennedy Half Dollar',

        112 => 'Commemorative Dollar',
        115 => 'Commemorative Gold Dollar',

        30 => 'Eisenhower Dollar',
        40 => 'Susan B Anthony Dollar',
        101 => 'Silver American Eagle',
        63 => 'Sacagawea Dollar',
        31 => 'Presidential Dollar',
        110 => 'American Innovation Dollar',

        77 => 'Tenth Ounce Gold',
        118 => 'Commemorative Five Dollar',
        124 => 'Tenth Ounce Buffalo',

        119 => 'Commemorative Ten Dollar',
        82 => 'Quarter Ounce Gold',
        120 => 'Tenth Ounce Platinum',
        122 => 'First Spouse',
        123 => 'Quarter Ounce Buffalo',

        95 => 'Half Ounce Gold',
        94 => 'Quarter Ounce Platinum',
        93 => 'Half Ounce Buffalo',
        109 => 'American Palladium Eagle',
        99 => 'One Ounce Gold',
        96 => 'Half Ounce Platinum',
        108 => 'Gold American Eagle',
        97 => 'One Ounce Buffalo',
        100 => 'One Ounce Platinum',
    ];


    public const RELEASE_TYPES = [
        6 =>  'Lincoln Bicentennial',
        17 => 'State Quarter',
        18 => 'District of Columbia and US Territories',
        42 => 'America the Beautiful Quarter',
        130 => 'Crossing the Delaware',
        131 => 'American Women',
        110 => 'American Innovation Dollar',
        31 => 'Presidential Dollar',
        122 => 'First Spouse',
    ];

    public const AMERICAN_EAGLE_TYPES = [
            120 => 'Tenth Ounce Platinum',
            94 => 'Quarter Ounce Platinum',
            96 => 'Half Ounce Platinum',
            100 => 'One Ounce Platinum',
            77 => 'Tenth Ounce Gold',
            82 => 'Quarter Ounce Gold',
            95 => 'Half Ounce Gold',
            99 => 'One Ounce Gold',
            109 => 'American Palladium Eagle',
            101 => 'Silver American Eagle',
    ];

    public static function mintmarkLists(CoinType $coinType): array
    {
        $mintList = [];
        $type_mms = [];
        foreach (explode(', ', $coinType->mints) as $mint){
            $mintList[$mint]['mms'] = Coin::select('mms')->distinct()
                ->where('cointypes_id',$coinType->id)->where('mint',trim($mint))
                ->get()->pluck('mms')->toArray();
            $mintList[$mint]['mms2'] = Coin::select('mms2')->distinct()
                ->where('cointypes_id',$coinType->id)->where('mint',trim($mint))
                ->get()->pluck('mms2')->toArray();
            $mintList[$mint]['mms3'] = Coin::select('mms3')->distinct()
                ->where('cointypes_id',$coinType->id)->where('mint',trim($mint))
                ->get()->pluck('mms3')->toArray();
            $mintList[$mint]['mms4'] = Coin::select('mms4')->distinct()
                ->where('cointypes_id',$coinType->id)->where('mint',trim($mint))
                ->get()->pluck('mms4')->values()->toArray();
            $mintList[$mint]['all'] = collect(array_merge($mintList[$mint]['mms'], $mintList[$mint]['mms2'],$mintList[$mint]['mms3'],$mintList[$mint]['mms4']))->unique()->sort()->toArray();
            $type_mms[$mint] = array_filter($mintList[$mint]['all'], function($v) {
                return $v !== 'None';
            });

            //$mintList[$mint]['final'] = collect($mintList[$mint]['all'])->unique()->toArray();
        }
        return $mintList;
    }


    public static function yearLists(int $type_id)
    {
        return Coin::select('coinYear')->distinct()
            ->where('cointypes_id',$type_id)
            ->orderBy('coinYear')
            ->get()->pluck('coinYear')->toArray();
    }



    public static function mintmarkMMMLists(CoinType $coinType = null)
    {
        try{
            $mintList = [];
            $type_mms = [];
            //ini_set('memory_limit', '2048M');
            ini_set("max_execution_time", "-1");
            ini_set("memory_limit", "-1");
            ignore_user_abort(true);
            set_time_limit(0);
            foreach (self::COIN_TYPES as $type_id => $val){
                $coinType = CoinType::findOrFail($type_id);
                foreach (explode(', ', $coinType->mints) as $mint){
                    $mintList[$mint]['mms'] = Coin::select('mms')->distinct()
                        ->where('cointypes_id',$coinType->id)->where('mint',trim($mint))
                        ->get()->pluck('mms')->toArray();
                    $mintList[$mint]['mms2'] = Coin::select('mms2')->distinct()
                        ->where('cointypes_id',$coinType->id)->where('mint',trim($mint))
                        ->get()->pluck('mms2')->toArray();
                    $mintList[$mint]['mms3'] = Coin::select('mms3')->distinct()
                        ->where('cointypes_id',$coinType->id)->where('mint',trim($mint))
                        ->get()->pluck('mms3')->toArray();
                    $mintList[$mint]['mms4'] = Coin::select('mms4')->distinct()
                        ->where('cointypes_id',$coinType->id)->where('mint',trim($mint))
                        ->get()->pluck('mms4')->values()->toArray();
                    $mintList[$mint]['all'] = collect(array_merge($mintList[$mint]['mms'], $mintList[$mint]['mms2'],$mintList[$mint]['mms3'],$mintList[$mint]['mms4']))->unique()->sort()->toArray();
                    $type_mms[$mint] = array_filter($mintList[$mint]['all'], function($v) {
                        return $v !== 'None';
                    });
                    $coinType->mms = json_encode($type_mms);
                    $coinType->save();
                    //$mintList[$mint]['final'] = collect($mintList[$mint]['all'])->unique()->toArray();
                }

                echo 'Finished';
            }
        }catch (Throwable $e){
            Log::error($e->getMessage());
            echo $e->getMessage();
        }

    }



}
