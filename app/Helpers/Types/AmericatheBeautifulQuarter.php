<?php

namespace App\Helpers\Types;

use Illuminate\Support\Facades\DB;

class AmericatheBeautifulQuarter
{



    public string $type = 'State Quarter';


    /**
     * General links for all types (Will be the same)
     */
    public static function typeLinks(): array
    {
        return [
            'PCGS Coinfacts' => 'https://www.pcgs.com/coinfacts/category/washington-50-states-quarters-1999-2008/720',
            'PCGS Price Guide' => 'https://www.pcgs.com/auctionprices/category/washington-50-states-quarters-1999-2008/720',
            'PCGS Population Report' => 'https://www.pcgs.com/pop/detail/washington-50-states-quarters-1999-2008/720',
            'NGC VarietyPlus' => 'https://www.ngccoin.com/variety-plus/united-states/quarters/state-and-territorial-quarters-1999-2009/',
            'NGC Price Guide' => 'https://www.ngccoin.com/price-guide/united-states/quarters/88/',
        ];
    }

    /**
     * General links for all types (Will be unique for type)
     */
    public static function typeSpecificLinks(): array
    {
        return [
            'Wikipedia' => 'https://en.wikipedia.org/wiki/50_State_quarters',
        ];
    }


    public static function releaseList($type_id): array
    {
        $release_list = [];
        $releases = DB::table('coins')
            ->select('design','coinYear')->distinct()
            ->where("cointypes_id", $type_id)->orderBy('release')
            ->get()->toArray();
        foreach($releases as $release){
            $release_list[$release->coinYear][] = $release->design;
        }
        return $release_list;
    }



}
