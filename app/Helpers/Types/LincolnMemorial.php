<?php
/**
 * GradeHelper | helper methods for Lincoln Memorial coin controllers and views
 *
 *
 * @package Coins
 * @subpackage Helper
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Helpers\Types;

class LincolnMemorial
{

    public string $type = 'Lincoln Memorial';


    /**
     * General links for all types (Will be the same)
     */
    public static function typeLinks(): array
    {
        return [
            'PCGS Coinfacts' => 'https://www.pcgs.com/coinfacts/category/half-cents/lincoln-cent-modern-1959-date/47',
            'PCGS Price Guide' => 'https://www.pcgs.com/prices/detail/lincoln-cent-modern-1959-date/47/most-active',
            'PCGS Population Report' => 'https://www.pcgs.com/pop/detail/lincoln-cent-modern-1959-date/47',
            'NGC VarietyPlus' => 'https://www.ngccoin.com/variety-plus/united-states/cents/lincoln-cents-memorial-reverse-1959-2008/',
            'NGC Price Guide' => 'https://www.ngccoin.com/price-guide/united-states/cents/100/',
        ];
    }

    /**
     * General links for all types (Will be unique for type)
     */
    public static function typeSpecificLinks(): array
    {
        return [
            'The Lincoln Cent Resource' => 'http://www.lincolncentresource.com/index.html',
            '1 Million Pennies Project' => 'http://1millionpenniesproject.blogspot.com/p/lincoln-cent-die-variety-cheat-sheet.html',
            'Lincoln Cent Doubled Die Book' => 'http://varietyvista.com/01a%20LC%20Doubled%20Dies%20Vol%201/index.htm',
            'BIE Die Breaks' => 'http://cuds-on-coins.com/lincoln-cent-bies/',
            'Coppercoins Reference Guide' => 'https://www.coppercoins.com/lincoln/index.htm',
            'Wikipedia' => 'https://www.ngccoin.com/variety-plus/united-states/cents/lincoln-cents-wheat-reverse-1909-1958/?page=1',
        ];
    }

    public static function releaseList()
    {

    }


    public static function metalTable(): string
    {
        return "
            <div id='metal_content_table' class=\"card mt-3\">
                <div class=\"card-body\">
                    <table class=\"table\">
                        <tbody>
                        <tr style=\"text-align: left;\">
                            <th>Years</th>
                            <th>Material</th>
                            <th>Weight<br>(grams)</th>
                        </tr>
                        <tr>
                            <td><a href=\"{{ route('coin.view_lincoln_metal', ['group' => 4]) }}\">1959–1962</a></td>
                            <td>Bronze (95% copper, 5% Tin and Zinc)</td>
                            <td>3.11
                            </td>
                        </tr>
                        <tr>
                            <td><a href=\"{{ route('coin.view_lincoln_metal', ['group' => 5]) }}\">1962 – September 1982</a></td>
                            <td>Gilding Metal (95% Copper, 5% Zinc)</td>
                            <td>3.11
                            </td>
                        </tr>
                        <tr>
                            <td><a href=\"{{ route('coin.view_lincoln_metal', ['group' => 6]) }}\">October 1982 – Present</a></td>
                            <td>Copper-Plated Zinc (97.5% Zinc, 2.5% Copper)</td>
                            <td>2.5
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        ";
    }


}
