<?php
/**
 * GradeHelper | helper methods for Lincoln Wheat coin controllers and views
 *
 * This helper class lists additional data for Lincoln Wheat
 *
 * @package Coins
 * @subpackage Helper
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Helpers\Types;

class LincolnWheat
{

    public string $type = 'Lincoln Wheat';


    /**
     * General links for all types (Will be the same)
     */
    public static function typeLinks(): array
    {
        return [
            'PCGS Coinfacts' => 'https://www.pcgs.com/coinfacts/category/lincoln-cent-wheat-reverse-1909-1958/46',
            'PCGS Price Guide' => 'https://www.pcgs.com/prices/detail/lincoln-cent-wheat-reverse/46/most-active',
            'PCGS Population Report' => 'https://www.pcgs.com/pop/detail/lincoln-cent-wheat-reverse-1909-1958/46',
            'NGC VarietyPlus' => 'https://www.ngccoin.com/variety-plus/united-states/cents/lincoln-cents-wheat-reverse-1909-1958/?page=1',
            'NGC Price Guide' => 'https://www.ngccoin.com/price-guide/united-states/cents/99/',
        ];
    }

    /**
     * General links for all types (Will be unique for type)
     */
    public static function typeSpecificLinks(): array
    {
        return [
            'The Lincoln Cent Resource' => 'http://www.lincolncentresource.com/index.html',
            '1 Million Pennies Project' => 'http://1millionpenniesproject.blogspot.com/p/lincoln-cent-die-variety-cheat-sheet.html',
            'Lincoln Cent Doubled Die Book' => 'http://varietyvista.com/01a%20LC%20Doubled%20Dies%20Vol%201/index.htm',
            'BIE Die Breaks' => 'http://cuds-on-coins.com/lincoln-cent-bies/',
            'Coppercoins Reference Guide' => 'https://www.coppercoins.com/lincoln/index.htm',
            'Wikipedia' => 'https://www.ngccoin.com/variety-plus/united-states/cents/lincoln-cents-wheat-reverse-1909-1958/?page=1',
        ];
    }


    /**
     * @var array[]
     */
    public const NGC_STRIKE_CHARACTERS = [
        'FB'        => 'Full Bands',
        'FB DPL'    => 'Full Bands, Deep Prooflike',
        'FB PL'     => 'Full Bands, Prooflike',
    ];


    public static function metalTable(): string
    {
        return "
            <div id='metal_content_table' class=\"card mt-3\">
                <div class=\"card-body\">
                    <table class=\"table\">
                        <tbody>
                        <tr style=\"text-align: left;\">
                            <th>Years</th>
                            <th>Material</th>
                            <th>Weight<br>(grams)</th>
                        </tr>
                        <tr>
                            <td><a href=\"{{ route('coin.view_lincoln_metal', ['group' => 1]) }}\"> 1909–1942</a> </td>
                            <td>Bronze (95% Copper, 5% Tin and Zinc)</td>
                            <td>3.11
                            </td>
                        </tr>
                        <tr>
                            <td><a href=\"{{ route('coin.view_lincoln_metal', ['group' => 2]) }}\">1943</a></td>
                            <td>Zinc-Coated Steel</td>
                            <td>2.72
                            </td>
                        </tr>
                        <tr>
                            <td><a href=\"{{ route('coin.view_lincoln_metal', ['group' => 3]) }}\">1944–1946</a></td>
                            <td>Gilding Metal (95% Copper, 5% Zinc)</td>
                            <td>3.11
                            </td>
                        </tr>
                        <tr>
                            <td><a href=\"{{ route('coin.view_lincoln_metal', ['group' => 4]) }}\">1947–1958</a></td>
                            <td>Bronze (95% copper, 5% Tin and Zinc)</td>
                            <td>3.11
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        ";
    }



}
