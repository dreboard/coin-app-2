<?php
/**
 * GradeHelper | helper methods for Lincoln Wheat coin controllers and views
 *
 * This helper class lists additional data for Lincoln Wheat
 *
 * @package Coins
 * @subpackage Helper
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Helpers\Types;

class MorganDollar
{

    public string $type = 'Lincoln Wheat';


    /**
     * General links for all types (Will be the same)
     */
    public static function typeLinks(): array
    {
        return [
            'PCGS Coinfacts' => 'https://www.pcgs.com/coinfacts/category/morgan-dollar-1878-1921/744',
            'PCGS Price Guide' => 'https://www.pcgs.com/prices/detail/morgan-dollar/744/most-active',
            'PCGS Population Report' => 'https://www.pcgs.com/pop/detail/morgan-dollar-1878-1921/744',
            'NGC VarietyPlus' => 'https://www.ngccoin.com/variety-plus/united-states/dollars/morgan-dollars-1878-1921/',
            'NGC Price Guide' => 'https://www.ngccoin.com/price-guide/united-states/dollars/49/',
        ];
    }

    /**
     * General links for all types (Will be unique for type)
     */
    public static function typeSpecificLinks(): array
    {
        return [
            'VAMWorld' => 'https://www.coppercoins.com/lincoln/index.htm',
            'Wikipedia' => 'https://en.wikipedia.org/wiki/Morgan_dollar',
        ];
    }


    /**
     * @var array[]
     */
    public const NGC_STRIKE_CHARACTERS = [
        'FB'        => 'Full Bands',
        'FB DPL'    => 'Full Bands, Deep Prooflike',
        'FB PL'     => 'Full Bands, Prooflike',
    ];


}
