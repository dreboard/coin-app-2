<?php

namespace App\Helpers\Types;

class MercuryDime
{

    public string $type = 'Mercury Dime';

    public int $id = 31;


    /**
     * General links for all types (Will be the same)
     */
    public static function typeLinks(): array
    {
        return [
            'PCGS Coinfacts' => 'https://www.pcgs.com/coinfacts/category/mercury-dime-1916-1945/703',
            'PCGS Price Guide' => 'https://www.pcgs.com/prices/detail/mercury-dime/703/most-active',
            'NGC VarietyPlus' => 'https://www.ngccoin.com/coin-explorer/united-states/dimes/mercury-dimes-1916-1945/',
            'NGC Price Guide' => 'https://www.ngccoin.com/variety-plus/united-states/dimes/mercury-dimes-1916-1945/?page=1',
            'Greysheet' => 'https://www.greysheet.com/coin-prices/series-landing/mercury-dimes',
        ];
    }

    /**
     * General links for all types (Will be unique for type)
     */
    public static function typeSpecificLinks(): array
    {
        return [

            'Wikipedia' => 'https://en.wikipedia.org/wiki/Mercury_dime',
        ];
    }
    /**
     * @var array[]
     */
    public const NGC_STRIKE_CHARACTERS = [
        'FB'        => 'Full Bands',
        'FB DPL'    => 'Full Bands, Deep Prooflike',
        'FB PL'     => 'Full Bands, Prooflike',
    ];


}
