<?php

namespace App\Helpers;

class MintHelper
{


    public const MINT_MARKS = [
        'Philadelphia' =>  'None',
        'Denver' => 'D',
        'Dahlonega' => 'D',
        'Charlotte' => 'C',
        'Carson City' => 'CC',
        'New Orleans' => 'O',
        'San Francisco' => 'S',
        'West Point' => 'W',
    ];


}
