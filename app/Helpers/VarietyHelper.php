<?php
/**
 * VarietyHelper | helper methods for coin controllers and views
 *
 * This helper class lists varieties for each coin
 *
 * @package Coins
 * @subpackage Helper
 * @author Andre Board <dre.board@gmail.com>
 * @version 1.0
 * @access public
 * @see https://github.com/dreboard/coin-app
 * @copyright none
 */
namespace App\Helpers;

use App\Models\Coins\CoinVariety;

class VarietyHelper
{

    // SELECT DISTINCT(`sub_type`) FROM `coins_variety` ORDER BY `coins_variety`.`sub_type` ASC
    public const MASTER_VARIETY_LIST = [
        'Abraded Die Variety',
        'Abraded Rev Die',
        'Blundered Die',
        'Branch Mint Proof',
        'Brassy Plating',
        'Broken Die Error',
        'Broken Die Error, Die Clash',
        'Clashed Die',
        'Clashed Die, Repunched Date',
        'Date Over Inverted Date',
        'Date Slant',
        'Die Break',
        'Die Chip',
        'Die Clash',
        'Die Clash, Doubled Die Obverse',
        'Die Crack',
        'Die Gouge',
        'Die Repunched Date',
        'Die Rust',
        'Die Scratches',
        'Die Variety',
        'Digit Punch',
        'Dot Below 5',
        'Double Date',
        'Doubled Die Obverse',
        'Doubled Die Obverse, Clashed Die',
        'Doubled Die Obverse, Doubled Die Reverse',
        'Doubled Die Obverse, Misplaced Date',
        'Doubled Die Obverse, Overdate',
        'Doubled Die Obverse, Overdate, Repunched Mintmark',
        'Doubled Die Obverse, Repunched Date',
        'Doubled Die Obverse, Repunched Date, Misplaced Dat...',
        'Doubled Die Obverse, Repunched Mintmark',
        'Doubled Die Obverse, Triple Punched Date',
        'Doubled Die Reverse',
        'Doubled Die Reverse, Doubled Die Obverse',
        'Doubled Die Reverse, Repunched Mintmark',
        'Doubled Working Hub',
        'Dual Mintmark',
        'Edge Lettering Error',
        'Eisenhower Dollar Variety',
        'Engravers Error',
        'Enhanced Die',
        'Enhanced Reverse Die',
        'Filing Lines',
        'Intentional Die Alteration',
        'Inverted Mintmark',
        'Large Date',
        'Large Over Small Date',
        'Lathe Lines',
        'Major Variety',
        'Master Die Obverse',
        'Master Die Reverse',
        'Mint Error',
        'Mint Mark Placement',
        'Mintmark Position',
        'Mintmark Size',
        'Mintmark Style',
        'Misplaced Date',
        'Misplaced Mintmark',
        'Missing Design Element',
        'Missing Designer Initials',
        'Narrow Date',
        'No Center Dots',
        'None',
        'Normal Die',
        'Obverse Design Variety',
        'Oddity',
        'Off-Center Clash',
        'Over Date',
        'Over Mintmark',
        'Overdate',
        'Overdate, Doubled-Die Obverse',
        'Polished Die',
        'Proof Finish',
        'Prototype Reverse',
        'Quadruple Die Reverse',
        'Re-Engraved Design',
        'Re-Engraved Die',
        'Recut Date',
        'Reed Count',
        'Repunched Date',
        'Repunched Date, Doubled Date',
        'Repunched Date, Doubled Die Obverse',
        'Repunched Date, Misplaced Date',
        'Repunched Date, Missing Leaf',
        'Repunched Date, Tripled Die Obverse',
        'Repunched Mintmark',
        'Repunched Mintmark, Doubled Die Obverse',
        'Repunched Mintmark, Doubled Die Obverse, Doubled D...',
        'Repunched Mintmark, Doubled Die Reverse',
        'Restrike',
        'Reverse Design Variety',
        'Reverse Die Aberrations',
        'Reverse of 1869 (Shallow N)',
        'Rotated Die',
        'Rusted Die',
        'Scribe Mark, Missing Leaf',
        'Series Doubled Obverse',
        'Series Doubled Reverse',
        'Small Motto',
        'Small Over Large Date',
        'The Bleeder',
        'Trail Die',
        'Trail Die, Repunched Mintmark',
        'Transitional Rev Die',
        'Transitional Reverse Design',
        'Triple Die Obverse',
        'Triple Die Reverse',
        'Triple Punched Date',
        'Tripled Die Obverse',
        'Tripled Die Reverse',
        'Type 1',
        'Type 2',
        'Working Hub Obverse',
        'Working Hub Reverse',
    ];
    public const DIE_BREAK_VARIETY_LIST = [
        'Broken Die Error',
        'Die Break',
        'Die Chip',
        'Die Crack',
        'Die Gouge',
    ];
    public const MINTMARK_VARIETY_LIST = [
        'Dual Mintmark',
        'Inverted Mintmark',
        'Mint Mark Placement',
        'Mintmark Position',
        'Mintmark Size',
        'Mintmark Style',
        'Misplaced Mintmark',
        'Over Mintmark',
        'Repunched Mintmark',
    ];
    public const DATE_VARIETY_LIST = [
        'Date Over Inverted Date',
        'Date Slant',
        'Double Date',
        'Doubled Die Obverse, Misplaced Date',
        'Doubled Die Obverse, Overdate',
        'Doubled Die Obverse, Overdate, Repunched Mintmark',
        'Doubled Die Obverse, Repunched Date',
        'Doubled Die Obverse, Repunched Date, Misplaced Dat...',
        'Doubled Die Obverse, Triple Punched Date',
        'Large Date',
        'Large Over Small Date',
        'Misplaced Date',
        'Narrow Date',
        'Over Date',
        'Overdate',
        'Overdate, Doubled-Die Obverse',
        'Recut Date',
        'Repunched Date',
        'Repunched Date, Doubled Date',
        'Repunched Date, Doubled Die Obverse',
        'Repunched Date, Misplaced Date',
        'Repunched Date, Missing Leaf',
        'Repunched Date, Tripled Die Obverse',
        'Triple Punched Date',
    ];


    public const DOUBLE_DIE_LIST = [
        'Doubled Die Obverse',
        'Doubled Die Reverse',
        'Doubled Die Reverse, Doubled Die Obverse',
        'Doubled Working Hub',
        'Master Die Obverse',
        'Master Die Reverse',
        'Quadruple Die Reverse',
        'Series Doubled Obverse',
        'Series Doubled Reverse',
        'Triple Die Obverse',
        'Triple Die Reverse',
        'Tripled Die Obverse',
        'Tripled Die Reverse',
    ];


    public const VARIETY_ERROR_MATCH = [
        12 => [
            'Doubled Die Obverse',
            'Doubled Die Reverse',
            'Doubled Working Hub',
            'Master Die Obverse',
            'Master Die Reverse',
            'Quadruple Die Reverse',
            'Series Doubled Obverse',
            'Series Doubled Reverse',
            'Triple Die Obverse',
            'Triple Die Reverse',
            'Triple Punched Date',
            'Tripled Die Obverse',
            'Tripled Die Reverse',
            'Working Hub Obverse',
            'Working Hub Reverse'
        ]
    ];



    public static function getDoubleDieVarieties(int $coin_id)
    {
        return CoinVariety::whereIn('variety', self::DOUBLE_DIE_LIST)->get();
    }

    public static function getDateVarieties(int $coin_id)
    {
        return CoinVariety::whereIn('variety', self::DATE_VARIETY_LIST)->get();
    }

    public static function getMintMarkVarieties(int $coin_id)
    {
        return CoinVariety::whereIn('variety', self::MINTMARK_VARIETY_LIST)->get();
    }

    public static function getDieBreakVarieties(int $coin_id)
    {
        return CoinVariety::whereIn('variety', self::DIE_BREAK_VARIETY_LIST)->get();
    }


    public function getDistinctVarietiesByCoinId(int $coin_id)
    {
        return CoinVariety::select('variety')->distinct()
            ->where('coin_id', $coin_id)->get();
    }


    public static function getSubTypesByCoinId(int $coin_id)
    {
        return CoinVariety::select('sub_type')->distinct()
            ->where('coin_id', $coin_id)->get()->pluck('sub_type')->toArray();
    }


}
