<?php

namespace App\Providers;

use App\Interfaces\Users\InterfaceUserRepository;
use App\Models\User;
use App\Models\Users\Violation;
use App\Repositories\User\UserRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(InterfaceUserRepository::class, UserRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //$this->registerPolicies();

        Blade::if('admin', function () {
            if (auth()->user() && auth()->user()->is_admin) {
                return 1;
            }
            return 0;
        });
        Blade::if('good', function () {
            if (auth()->user()->status === 'good') {
                return 1;
            }
            return 0;
        });


        Gate::define('view-violation', function (User $user, Violation $violation) {
            return $user->id === $violation->reporter || $user->is_admin || $user->name === $violation->violator;
        });


        Gate::define('view-model', function (User $user, Model $model) {
            return $user->id === $model->user_id || $user->is_admin;
        });
    }
}
