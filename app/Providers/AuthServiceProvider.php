<?php

namespace App\Providers;

use App\Models\Coins\Collected;
use App\Models\Coins\CollectedSet;
use App\Models\Groups\Group;
use App\Policies\CollectedPolicy;
use App\Policies\CollectedSetPolicy;
use App\Policies\GroupPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Group::class => GroupPolicy::class,
        Collected::class => CollectedPolicy::class,
        CollectedSet::class => CollectedSetPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
