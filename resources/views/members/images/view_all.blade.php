@extends('layouts.user.main')
@section('pageTitle', 'View Image')
@section('content')

    <h3 class="mt-4">View Image</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.user_types', ['type' => 'Writer']) }}">Writers Directory</a>
        </li>
        <li class="breadcrumb-item"><a href="{{ route('member.user_posts', ['id' => auth()->user()->id]) }}">My
                Posts</a></li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <a class="btn btn-primary" href="{{ $route }}" >Back</a>


            <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-inner">
                    @foreach($images->images as $image)
                        <div class="carousel-item @if ($loop->first) active @endif">
                            <img src="{{asset('storage/'.$image->image_url)}}" class="d-block w-100"
                                 alt="{{$image->title ?? ''}}"/>
                            <div class="carousel-caption d-none d-md-block">
                                <h5>{!! $image->title !!}</h5>
                                <p>
                                    <a href="{{ route('member.view_image', ['image' => $image->id]) }}">View</a>
                                </p>
                            </div>
                        </div>
                    @endforeach
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>






            <!-- End posts -->

            <h4 class="mt-3">Comments</h4>

            <a class="btn btn-primary" href="{{ $route }}" >Back</a>
        </div>
        <!-- Side widgets-->
        @include('members.writers.partials.side')
    </div>
    @push('styles')
        <style>

        </style>
    @endpush
    @push('scripts')
        <script>



            (function () {
                try {



                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
