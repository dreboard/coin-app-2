@extends('layouts.user.main')
@section('pageTitle', 'Create An Article')
@section('content')

    <h3 class="mt-4">Create An Article</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.user_types', ['type' => 'Writer']) }}">Writers Directory</a></li>
        <li class="breadcrumb-item"><a href="{{ route('member.user_posts', ['id' => auth()->user()->id]) }}">My Posts</a></li>
    </ol>
    <div class="container">

        <div class="row">
            <div class="col-md-8">
                <form id="post_form" action="{{ route('member.save_posts') }}"
                      method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-12">
                        <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                        <div class="mb-3">
                            <label for="text" class="form-label">Title</label>
                            <input type="text" class="form-control" name="title" id="title" value="{{ old('title') }}">
                            <div id="titleHelp" class="form-text">Article Title</div>
                            @error('title')
                                <div class="text-sm text-red-600">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="image_path" class="form-label">Attach an image</label>
                            <input class="form-control" type="file" name="image_url" id="image_url"
                                   onchange="preview()">
                            <img id="image_preview_frame" src="" width="100px" height="100px"/>
                        </div>

                        <div class="form-group mb-3">
                            <label class="control-label">Article Text</label>
                            <textarea id="post_body" name="body" class="form-control">{{ old('body') }}</textarea>
                        </div>

                        <div class="form-group mb-3">
                            <div class="accordion" id="accordionExample">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingOne">
                                        <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                data-bs-target="#collapseOne" aria-expanded="true"
                                                aria-controls="collapseOne">
                                            <span id="checkboxgroupText">Tags (Up to 3)</span>
                                        </button>
                                    </h2>
                                    <div id="collapseOne" class="accordion-collapse collapse show"
                                         aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                        <div id="checkboxgroup" class="accordion-body">

                                            @foreach($tags->chunk(3) as $tagGroup)
                                                <div class="row">
                                                    @foreach($tagGroup as $tag)
                                                        <div class="col-md-3 form-check">
                                                            <input class="form-check-input" name="tags[]"
                                                                   type="checkbox" value="{{$tag->id}}"
                                                                   id="tag_{{$tag->id}}">
                                                            <label class="form-check-label" for="tag_{{$tag->id}}">
                                                                {{$tag->name}}
                                                            </label>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" value="1" name="publish" id="publish_post" checked>
                                    <label class="form-check-label" for="publish_post">
                                        Publish
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" value="0" name="publish" id="draft_post">
                                    <label class="form-check-label" for="draft_post">
                                        Save As Draft
                                    </label>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" value="1" name="public" id="public_post" checked>
                                    <label class="form-check-label" for="public_post">
                                        Public Listing
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" value="0" name="public" id="private_post">
                                    <label class="form-check-label" for="private_post">
                                        Private Listing
                                    </label>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="1" name="featured" id="featured_post">
                                    <label class="form-check-label" for="featured_post">
                                        Featured Listing
                                    </label>
                                </div>

                            </div>
                        </div>

                        <!-- Submit Form Input -->
                        <div class="col-3 mt-3">
                            <button id="post_form_btn" type="submit" class="btn btn-primary form-control">Create</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-md-4">
                <table class="table fixed_header table-striped">
                    <thead>
                    <tr>
                        <th scope="col">Unpublished</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($unpublished as $unpublishedPost)
                        <tr>
                            <td class="w-75">
                                <a class="group_link"
                                   href="{{ route('member.edit_posts', ['post' => $unpublishedPost->id]) }}"
                                   title="{{$unpublishedPost->title}}">{{ Str::limit($unpublishedPost->title, 38) }}</a>
                            </td>
                            <td>
                                {{ \Carbon\Carbon::parse($unpublishedPost->created_at)->diffForHumans() }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="card">
                    <div class="card-body">
                        <a href="#" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#GroupTypeModal">Group
                            Type</a>
                        <a href="#" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#exampleModal2"
                           2>Specialty</a>
                    </div>
                </div>
            </div>

            <div class="mt-5"></div>
            <div class="modal fade" id="GroupTypeModal" tabindex="-1" aria-labelledby="GroupTypeModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="GroupTypeModalLabel">Group Type</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            ...
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel2"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="exampleModalLabel2">Specialty</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            ...
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
@push('styles')
    <style>
        .ck-editor__editable {
            min-height: 400px;
        }

        .fixed_header {
            width: 400px;
            table-layout: fixed;
            border-collapse: collapse;
        }

        .fixed_header tbody {
            display: block;
            width: 100%;
            overflow: auto;
            height: 250px;
        }

        .fixed_header thead tr {
            display: block;
        }

        .fixed_header thead {
            background: black;
            color: #fff;
        }

        .fixed_header th,
        .fixed_header td {
            padding: 5px;
            text-align: left;
            width: 200px;
        }
    </style>
@endpush
@push('scripts')
    <script src="https://cdn.ckeditor.com/ckeditor5/35.2.1/classic/ckeditor.js"></script>
    <script>
        let image_preview_frame = document.getElementById('image_preview_frame');

        function preview() {
            image_preview_frame.src = URL.createObjectURL(event.target.files[0]);
        }

        (function () {
            try {

                function preview() {
                    frame.src = URL.createObjectURL(event.target.files[0]);
                }

                function onlyOneCheckBox() {
                    let checkboxgroupText = document.getElementById('checkboxgroupText');
                    let checkboxgroup = document.getElementById('checkboxgroup').getElementsByTagName("input");
                    let limit = 3;
                    for (var i = 0; i < checkboxgroup.length; i++) {
                        checkboxgroup[i].onclick = function () {
                            var checkedcount = 0;
                            for (var i = 0; i < checkboxgroup.length; i++) {
                                checkedcount += (checkboxgroup[i].checked) ? 1 : 0;
                            }
                            if (checkedcount > limit) {
                                console.log("You can select maximum of " + limit + " checkbox.");
                                checkboxgroupText.innerText = "You can select maximum of " + limit + " checkbox.";
                                this.checked = false;
                            }
                        }
                    }
                }

                onlyOneCheckBox();


                ClassicEditor.defaultConfig = {
                    toolbar: {
                        items: [
                            'heading',
                            '|',
                            'bold',
                            'italic',
                            '|',
                            'bulletedList',
                            'numberedList',
                            '|',
                            'insertTable',
                            '|',
                            'undo',
                            'redo'
                        ]
                    },
                    image: {
                        toolbar: [
                            'imageStyle:full',
                            'imageStyle:side',
                            '|',
                            'imageTextAlternative'
                        ]
                    },
                    table: {
                        contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells']
                    },
                    language: 'en'
                };


                ClassicEditor
                    .create(document.querySelector('#post_body')).catch(error => {
                    console.error(error);
                });


            } catch (error) {
                if (ENVIRONMENT === 'local') {
                    console.error(error);
                }
            }
        })();
    </script>
@endpush
