@extends('layouts.user.main')
@section('pageTitle', 'View Image')
@section('content')

    <h3 class="mt-4">View Image</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.user_types', ['type' => 'Writer']) }}">Writers Directory</a>
        </li>
        <li class="breadcrumb-item"><a href="{{ route('member.user_posts', ['id' => auth()->user()->id]) }}">My
                Posts</a></li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <!-- start posts -->
            <div class="btn-group mb-3" role="group" aria-label="Basic example">
                <a class="btn btn-primary" href="{{ $route }}" >Back</a>
                @if($image->user_id == auth()->user()->id)
                    <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#exampleModal">
                        Delete
                    </button>
                @endif
            </div>
            @if($image->user_id == auth()->user()->id)
                <div class="card">
                    <div class="card-body">
                        <p><span class="fw-bold">Image URL:</span></p>
                        <input type="text" readonly class="form-control-plaintext" id="urlTxt" value="{{asset('storage/'.$image->image_url)}}">
                        <button id="copyBtn" class="btn btn-warning" onclick="copyLink()" type="button">Copy Link</button>
                    </div>
                </div>
            @endif

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form class="row row-cols-lg-auto g-3 align-items-center"  method="post"
                                  action="{{ route('member.image_delete') }}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="image_id" value="{{ $image->id }}" />
                                <button type="submit" class="btn btn-danger">Delete this image</button>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div id='ex1' class="col-sm-12 zoom mb-4">
                    <img id="main_image" src="{{asset('storage/'.$image->image_url)}}" width="80%" alt="{{$image->title ?? ''}}" />

                </div>
                <div class="col-sm-6">
                    <div class="magnifier"></div>
                </div>
            </div>



            <div>




                <p class="mt-3 mb-3">{!! $image->title !!}</p>
                @if($image->user_id == auth()->user()->id)
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Update Image</h5>
                            <form class="row row-cols-lg-auto g-3 align-items-center"  method="post"
                                  action="{{ route('member.image_edit') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="col-12">
                                    <input type="text" class="form-control" id="title" name="title" placeholder="Image Title" value="{{ old('title', $image->title) }}">
                                </div>
                                <div class="col-12">
                                    <input class="form-control" type="file" name="image_url" id="image_url">
                                </div>
                                <input type="hidden" name="image_id" value="{{ $image->id }}" />
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary">Change Image</button>
                                </div>
                            </form>
                        </div>
                    </div>
                @endif
            </div>
            <!-- End posts -->

            <h4 class="mt-3">Comments</h4>
            <section class="mb-5">
                <div class="card bg-light">
                    <div class="card-body">
                        <!-- Comment form-->
                        @include('layouts.partials.comment_replies', ['comments' => $image->comments, 'post_id' => $image->id])
                        <form class="mb-4" action="{{ route('member.save_image_comment') }}" method="post">
                            @csrf
                            <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                            <input type="hidden" name="image_id" value="{{$image->id }}">
                            <input type="hidden" name="group_id" value="0">

                            <div class="form-group mb-3">
                                <label class="control-label">Comment</label>
                                <textarea id="comment_body" name="comment_body" class="form-control" rows="3"
                                          placeholder="Join the discussion and leave a comment!"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                    </div>
                </div>
            </section>
            <a class="btn btn-primary" href="{{ $route }}" >Back</a>
        </div>
        <!-- Side widgets-->
        @include('members.writers.partials.side')
    </div>
    @push('styles')
        <style>
            .display-comment .display-comment {
                margin-left: 40px
            }
            .ck-editor__editable {
                min-height: 250px;
            }

        </style>
    @endpush
    @push('scripts')
        <script src="https://cdn.ckeditor.com/ckeditor5/35.2.1/classic/ckeditor.js"></script>
        <script>



            let copyBtn = document.getElementById("copyBtn");
            function copyLink() {
                var urlTxt = document.getElementById("urlTxt");
                urlTxt.select();// Select the text field
                urlTxt.setSelectionRange(0, 99999); // For mobile devices
                navigator.clipboard.writeText(urlTxt.value);// Copy the text inside the text field
                console.log("Copied the text: " + urlTxt.value);
                document.getElementById("copyBtn").classList.add('btn-success');
                document.getElementById("copyBtn").innerText = "Copied.";
            }

            (function () {
                try {

                    ClassicEditor.defaultConfig = {
                        toolbar: {
                            items: [
                                'heading',
                                '|',
                                'bold',
                                'italic',
                                '|',
                                'bulletedList',
                                'numberedList',
                                '|',
                                'insertTable',
                                '|',
                                'undo',
                                'redo'
                            ]
                        },
                        image: {
                            toolbar: [
                                'imageStyle:full',
                                'imageStyle:side',
                                '|',
                                'imageTextAlternative'
                            ]
                        },
                        table: {
                            contentToolbar: [ 'tableColumn', 'tableRow', 'mergeTableCells' ]
                        },
                        language: 'en'
                    };



                    ClassicEditor
                        .create( document.querySelector( '#comment_body' )).catch( error => {
                        console.error( error );
                    } );



                    // -- Image Zoom -----------------------------------------------------------------------------

                    (function ($) {
                        var defaults = {
                            url: false,
                            callback: false,
                            target: false,
                            duration: 120,
                            on: 'mouseover' // other options: 'grab', 'click', 'toggle'
                        };

                        // Core Zoom Logic, independent of event listeners.
                        $.zoom = function(target, source, img) {
                            var outerWidth,
                                outerHeight,
                                xRatio,
                                yRatio,
                                offset,
                                position = $(target).css('position');

                            // The parent element needs positioning so that the zoomed element can be correctly positioned within.
                            $(target).css({
                                position: /(absolute|fixed)/.test() ? position : 'relative',
                                overflow: 'hidden'
                            });

                            $(img)
                                .addClass('zoomImg')
                                .css({
                                    position: 'absolute',
                                    top: 0,
                                    left: 0,
                                    opacity: 0,
                                    width: img.width,
                                    height: img.height,
                                    border: 'none',
                                    maxWidth: 'none'
                                })
                                .appendTo(target);

                            return {
                                init: function() {
                                    outerWidth = $(target).outerWidth();
                                    outerHeight = $(target).outerHeight();
                                    xRatio = (img.width - outerWidth) / $(source).outerWidth();
                                    yRatio = (img.height - outerHeight) / $(source).outerHeight();
                                    offset = $(source).offset();
                                },
                                move: function (e) {
                                    var left = (e.pageX - offset.left),
                                        top = (e.pageY - offset.top);

                                    top = Math.max(Math.min(top, outerHeight), 0);
                                    left = Math.max(Math.min(left, outerWidth), 0);

                                    img.style.left = (left * -xRatio) + 'px';
                                    img.style.top = (top * -yRatio) + 'px';
                                }
                            };
                        };

                        $.fn.zoom = function (options) {
                            return this.each(function () {
                                var
                                    settings = $.extend({}, defaults, options || {}),
                                    //target will display the zoomed iamge
                                    target = settings.target || this,
                                    //source will provide zoom location info (thumbnail)
                                    source = this,
                                    img = new Image(),
                                    $img = $(img),
                                    mousemove = 'mousemove',
                                    clicked = false;

                                // If a url wasn't specified, look for an image element.
                                if (!settings.url) {
                                    settings.url = $(source).find('img').attr('src');
                                    if (!settings.url) {
                                        return;
                                    }
                                }

                                img.onload = function () {
                                    var zoom = $.zoom(target, source, img);

                                    function start(e) {
                                        zoom.init();
                                        zoom.move(e);

                                        // Skip the fade-in for IE8 and lower since it chokes on fading-in
                                        // and changing position based on mousemovement at the same time.
                                        $img.stop()
                                            .fadeTo($.support.opacity ? settings.duration : 0, 1);
                                    }

                                    function stop() {
                                        $img.stop()
                                            .fadeTo(settings.duration, 0);
                                    }

                                    if (settings.on === 'grab') {
                                        $(source).mousedown(
                                            function (e) {
                                                $(document).one('mouseup',
                                                    function () {
                                                        stop();

                                                        $(document).unbind(mousemove, zoom.move);
                                                    }
                                                );

                                                start(e);

                                                $(document)[mousemove](zoom.move);

                                                e.preventDefault();
                                            }
                                        );
                                    } else if (settings.on === 'click') {
                                        $(source).click(
                                            function (e) {
                                                if (clicked) {
                                                    // bubble the event up to the document to trigger the unbind.
                                                    return;
                                                } else {
                                                    clicked = true;
                                                    start(e);
                                                    $(document)[mousemove](zoom.move);
                                                    $(document).one('click',
                                                        function () {
                                                            stop();
                                                            clicked = false;
                                                            $(document).unbind(mousemove, zoom.move);
                                                        }
                                                    );
                                                    return false;
                                                }
                                            }
                                        );
                                    } else if (settings.on === 'toggle') {
                                        $(source).click(
                                            function (e) {
                                                if (clicked) {
                                                    stop();
                                                } else {
                                                    start(e);
                                                }
                                                clicked = !clicked;
                                            }
                                        );
                                    } else {
                                        zoom.init(); // Pre-emptively call init because IE7 will fire the mousemove handler before the hover handler.

                                        $(source).hover(
                                            start,
                                            stop
                                        )[mousemove](zoom.move);
                                    }

                                    if ($.isFunction(settings.callback)) {
                                        settings.callback.call(img);
                                    }
                                };

                                img.src = settings.url;
                            });
                        };

                        $.fn.zoom.defaults = defaults;
                    }(window.jQuery));

                    $(document).ready(function(){
                        $('#ex1').zoom();
                    });



                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
