@extends('layouts.user.main')
@section('pageTitle', 'Articles by user')
@section('content')

    <h3 class="mt-4">Articles by {{ $user->name }}</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.user_types', ['type' => 'Writer']) }}">Writers Directory</a>
        </li>
        <li class="breadcrumb-item"><a href="{{ route('member.user_posts', ['id' => auth()->user()->id]) }}">My
                Posts</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8 posts_div  mb-4">
            @if($user->id !== auth()->user()->id)
                <div class="mb-3">
                    @if(auth()->user()->isFollowing($user))
                        <button type="button" class="btn btn-success" id="follow_model_btn">
                            Following
                        </button>
                    @else
                        <button type="button" class="btn btn-primary" id="follow_model_btn">
                            Follow
                        </button>
                    @endif
                </div>
            @endif

            <!-- featured posts -->
            @if(!empty($featured))
                @if($posts->currentPage() == 1)
                    <div class="card mb-4">
                        @if($featured->image_url == 'https://via.placeholder.com/150')
                            <a href="#!"><img class="card-img-top" src="https://via.placeholder.com/150"
                                              alt="..."/></a>
                        @else
                            <figure class="mb-4">
                                <a href="{{ route('member.view_post', ['post' => $featured->id]) }}">
                                    <img class="card-img-top post_image" src="{{asset('storage/'.$featured->image_url)}}" width="250" alt="" />
                                </a>
                            </figure>
                        @endif

                        <div class="card-body">
                            <div
                                class="small text-muted">{{ Carbon\Carbon::parse($featured->created_at)->diffForHumans() }}</div>
                            <h2 class="card-title">{{ $featured->title }}</h2>
                            <h3 class="card-title">Featured Post</h3>
                            <p class="card-text">{!! Str::limit($featured->body, 50) !!}</p>
                            <a class="btn btn-primary" href="{{ route('member.view_post', ['post' => $featured->id]) }}">Read more →</a>
                        </div>
                    </div>
                @endif
            @endif
            <!-- start posts -->
            @if(!empty($posts) || count($posts) > 0)
                @foreach($posts as $post)
                    @if($post->featured == 0)
                        <div
                            class="card row-hover pos-relative py-3 px-3 mb-3 border-primary border-top-0 border-right-0 border-bottom-0 rounded-0">
                            <div class="row align-items-center">
                                <div class="col-md-8 mb-3 mb-sm-0">
                                    <h5>
                                        <a href="{{ route('member.view_post', ['post' => $post->id]) }}"
                                           class="text-primary">{{ $post->title }}</a>
                                    </h5>
                                    <p class="text-sm"><span class="op-6">Posted</span> <a class="text-black"
                                                                                           href="{{ route('member.view_post', ['post' => $post->id]) }}">{{ Carbon\Carbon::parse($post->created_at)->diffForHumans() }}</a>
                                        <span class="op-6">ago</span>
                                    <div class="text-sm op-5">
                                        @foreach($post->tags as $tag)
                                            <a class="text-black mr-2" href="{{ route('member.posts_by_tag', ['tag' => $tag->slug]) }}">#{{$tag->name}}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-md-4 op-7">
                                    <div class="row text-center op-7">
                                        <div class="col px-1"><i class="ion-ios-chatboxes-outline icon-1x"></i> <span
                                                class="d-block text-sm">{{$post->comments->count()}} Replys</span></div>
                                        <div class="col px-1"><i class="ion-ios-eye-outline icon-1x"></i> <span
                                                class="d-block text-sm">{{$post->views}} Views</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach

                {{ $posts->links('pagination::bootstrap-5') }}

            @endif
                @if(empty($posts) || count($posts) < 1)
                <p>No Current Articles by {{ $user->name }}</p>
            @endif
            <!-- End posts -->
                <div class="h-20"></div>
        </div>

        <!-- Side widgets-->
        @include('members.writers.partials.side')

    </div>

@endsection
@push('styles')
    <style>
        .posts_div nav {
            margin-bottom: 12px;
        }
        .post_image {
            width: 50%;
            height: auto
        }
        figure {
            text-align: center;
        }
    </style>
@endpush
@push('scripts')
    <script>
        (function (ENVIRONMENT) {
            try {
                let follow_model_btn = document.getElementById('follow_model_btn');
                follow_model_btn.addEventListener('click', function () {

                    fetch('{{ route('user.user_follow') }}', {
                        headers: {"Content-Type": "application/json; charset=utf-8"},
                        method: 'POST',
                        body: JSON.stringify({
                            _token: @json(csrf_token()),
                            model_type: 'user',
                            model_id: {{ $user->id ?? auth()->user()->id }},
                        })
                    }).then(response => response.json())
                        .then(data => {
                            console.log(data);
                            if (data.result == 'Success') {
                                follow_model_btn.innerHTML = 'Following';
                                follow_model_btn.classList.add('btn-success')
                            } else {
                                follow_model_btn.classList.add('text-danger');
                                follow_model_btn.innerHTML = 'Error';
                            }
                        })
                });
            } catch (error) {
                if (ENVIRONMENT === 'local') {
                    console.error(error);
                }
            }
        })();
    </script>
@endpush
