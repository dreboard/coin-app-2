@extends('layouts.user.main')
@section('pageTitle', 'My Dashboard')
@section('sidebar')
    @parent
    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')

    <h1 class="mt-4">Members Area</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Home</li>
    </ol>

    <div class="row">
        <div class="col-sm-4 callout callout-primary">
            <h4>My Collection</h4>
            <div id="apmex-widget-spotprice"
                 style="width:280px;height:200px;background-color:#FFFFFF;">
                <div id="apmex-widget-spot-frame-target">
                    <iframe
                        src="http://widgets.apmex.com/widget/spotprice/?w=280&amp;h=180&amp;mtls=GSPL&amp;arf=True&amp;rint=5&amp;srf=False&amp;tId=1&amp;cId=4f19ff34-00ad-40b0-bf24-a5514e56d73a&amp;wId=1"
                        frameBorder="0" width="280" height="180" scrolling="no" style="display:block;"></iframe>
                </div>
                <div id="apmex-widget-spot-target-footer" style="text-align: center; text-decoration: none;">
                    <a href="http://www.apmex.com" target="_blank" style="color: #000000; text-decoration: none;">
                        <img src="https://widgets.apmex.com/content/themes/logos/blue.png" style="border:0px;"/></a>
                </div>
            </div>
        </div>
        @if(optional(auth()->user())->status !== 'warn')
            <div class="col-sm-4 callout callout-primary">
                <div class="row">
                    <div class="col-4">
                        <h4>Groups</h4>
                    </div>
                    <div class="col-8">
                        <div class="row">
                            <div class="col-12">
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    @if(is_null($group))
                                        <a href="{{ route('group.start') }}" type="button" class="btn btn-primary">Create</a>
                                    @else
                                        <a href="{{ route('group.mine') }}" type="button" class="btn btn-primary">Mine</a>
                                    @endif
                                        <a href="{{ route('group.all') }}" type="button" class="btn btn-secondary">All</a>
                                        <a href="{{ route('user.groups_i_follow') }}" type="button" class="btn btn-primary">Followed</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="col-sm-4 callout callout-primary">
            <h4>Members Area</h4>
            <p>This is a primary callout.</p>
            <h4>My Area</h4>
            <p>My primary <a class="btn btn-secondary" href="{{ route('group.all') }}" class="">See All</a>.</p>

        </div>

    </div>



    <div class="card-group">


        <div class="card">
            <div class="card-body">
                <h5 class="card-title">My Projects</h5>
                <ul class="list-group list-group-flush">
                    <a href="#" class="list-group-item list-group-item-action">Project 1</a>
                    <a href="#" class="list-group-item list-group-item-action">Project 2</a>
                    <a href="#" class="list-group-item list-group-item-action">Project 3</a>
                </ul>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Groups</h5>
                <p class="card-text"><a href="{{ route('group.all') }}" class="list-group-item list-group-item-action">See All</a></p>
                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                @if(is_null($group))
                    <a href="{{ route('group.start') }}" class="list-group-item list-group-item-action">Create</a>
                @else
                    <a href="{{ route('group.mine') }}" class="list-group-item list-group-item-action">My Groups</a>
                @endif
            </div>
        </div>
        <div class="card" style="width: 18rem;">
            <ul class="list-group list-group-flush">
                <a href="{{ route('user.message_all') }}" class="list-group-item list-group-item-action">
                    <span class="@if($systemMessageCount > 0) text-danger @endif @if(auth()->user()->status === 'warn') text-danger @endif">
                        System Messages
                    </span>
                    <span class="badge @if($systemMessageCount > 0) bg-danger @else bg-primary @endif rounded-pill">{{ $systemMessageCount }}</span>
                </a>
                @good
                    <a href="{{ route('messages.messages') }}" class="list-group-item list-group-item-action">User Messages <span class="badge bg-primary rounded-pill">{{$messageCount}}</span></a>
                    <a href="{{ route('user.view_directory') }}" class="list-group-item list-group-item-action">User Directory</a>
                @endgood
                <a href="{{ route('user.view_directory') }}" class="list-group-item list-group-item-action">Shop</a>
                <a href="{{ route('news.all') }}" class="list-group-item list-group-item-action">News</a>
            </ul>
            <div class="card-footer">
                Card footer
            </div>
        </div>
    </div>

    <div class="callout callout-primary">
        <h4>Primary Callout</h4>
        This is a primary callout.
    </div>
    <div class="card-group">
        <div class="card">
            <img src="..." class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Shop</h5>
                <p class="card-text">This is a widontent is a little bit longer.</p>
                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
            </div>
        </div>
        <div class="card">
            <img src="..." class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">News</h5>
                <p class="card-text">This card has supporting text onal content.</p>
                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
            </div>
        </div>
        <div class="card">
            <img src="..." class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">This is a wider car card haeight action.</p>
                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
            </div>
        </div>
    </div>

@endsection
<script>
    import {Admin} from "../../../public/js/admin";
    export default {
        components: {Admin}
    }
</script>
