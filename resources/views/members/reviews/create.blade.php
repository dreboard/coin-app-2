@extends('layouts.user.main')
@section('title', 'Create A Project')
@section('content')

    <h3 class="mt-4">Create A Project</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.user_types', ['type' => 'Research']) }}">Scholar Directory</a></li>
    </ol>
    <div class="container">

        <div class="row">
            <div class="col-md-8">
                <form action="{{ route('group.store_club') }}" method="post" id="groupForm" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                    <input type="hidden" name="group_type" value="club">
                    <div class="form-group mb-3">
                        <label for="title" class="control-label">Club Name</label>
                        <input type="text" class="form-control" name="title" placeholder="Project Title"
                               value="{{ old('title') }}">
                    </div>

                    <div class="form-group mb-3">
                        <label for="description" class="control-label">Description</label>
                        <textarea name="description" id="description" class="form-control" placeholder="Detailed Description">{{ old('description') }}</textarea>
                    </div>



                    <div class="form-group mt-3">
                        <button type="submit" class="btn btn-primary form-control">Save</button>
                    </div>
                </form>
            </div>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">About Groups</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
                        Group Type
                        <p class="card-text">All users are allowed to create one group.</p>
                        <a href="#" class="card-link" data-bs-toggle="modal" data-bs-target="#GroupTypeModal">Group Type</a>
                        <a href="#" class="card-link" data-bs-toggle="modal" data-bs-target="#exampleModal"2>Specialty</a>
                    </div>
                </div>
            </div>

            <div class="mt-5"></div>
            <div class="modal fade" id="GroupTypeModal" tabindex="-1" aria-labelledby="GroupTypeModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="GroupTypeModalLabel">Group Type</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            ...
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel2" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="exampleModalLabel2">Specialty</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            ...
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
