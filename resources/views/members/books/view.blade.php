@extends('layouts.user.main')
@section('pageTitle', 'View Book')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">{{ $book->title }}</h3>
        </div>
        <div class="col-6">
                <div class="dropdown mt-4 float-end">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                            data-bs-toggle="dropdown" aria-expanded="false">
                        CHANGE
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <li><a class="dropdown-item"
                               href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">Back To Settings</a>
                        </li>
                        <li><a class="dropdown-item"
                               href="{{ route('user.edit_public', ['user' => auth()->user()->id]) }}">Edit This
                                Profile</a>
                        </li>
                        <li><a class="dropdown-item"
                               href="{{ route('user.user_edit_avatar', ['user' => auth()->user()->id]) }}">Change Image</a>
                        </li>
                    </ul>
                </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('member.writers') }}">Writers Area</a></li>
            <li class="breadcrumb-item"><a href="{{ route('user.public', ['id' => auth()->user()->id]) }}">My
                    Articles</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <table class="table">
                <tr>
                    <th scope="row">Author</th>
                    <td>{{ $book->author }}</td>
                </tr>
                <tr>
                    <th scope="row">ISBN</th>
                    <td>{{ $book->isbn }}</td>
                </tr>
                <tr>
                    <th scope="row">Publisher</th>
                    <td colspan="2">{{ $book->publisher }}</td>
                </tr>
            </table>

            <div>
                <p class="mt-3 mb-3">{!! $book->description !!}</p>
            </div>

            <hr />
            <h3>Reviews</h3>
            @forelse($book->reviews as $review)
                <p>{!! $review->comment !!}</p>
            @empty
                <p>No reviews yet</p>
            @endforelse
        </div>

        <!-- Side widgets-->
        @include('members.writers.partials.side')
    </div>

@endsection

