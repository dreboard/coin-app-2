@extends('layouts.user.main')
@section('pageTitle', "Discussion Area $coin->coinName $coin->coinType")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Discussion Area</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Links
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_coin', ['coin' => $coin->id]) }}">Back To {{$coin->coinName}}</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('member.forum_cat_index', ['cat_id' => $coin->coincats_id]) }}">{{ $coin->coinCategory }} Discussions</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('member.forum_type_index', ['type_id' => $coin->cointypes_id]) }}">{{$coin->coinType}}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('member.forum') }}">Forum Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('member.forum_user', ['id' => auth()->user()->id ]) }}">My Forum</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <!-- Featured blog post-->


            <div class="alert alert-primary" role="alert">
                    <h5 class="card-title">Topic</h5>
                    <a href="{{ route('coin.view_coin', ['coin' => $coin->id]) }}">{{$coin->coinName}} {{$coin->coinType}}</a> discussions.  Also see
                    <a href="{{ route('member.forum_type_index', ['type_id' => $coin->cointypes_id]) }}">{{$coin->coinType}}</a> discussions.
            </div>


            @forelse($posts as $post)
                <div
                    class="card row-hover pos-relative py-3 px-3 mb-3 border-primary border-top-0 border-right-0 border-bottom-0 rounded-0">
                    <div class="row align-items-center">
                        <div class="col-md-8 mb-3 mb-sm-0">
                            <h5>
                                <a href="{{ route('member.view_forum_post', ['forum' => $post->id]) }}" class="text-primary">{{$post->title}}</a>
                            </h5>
                            <p class="text-sm">
                                <span class="op-6">Posted</span>
                                <a class="text-black"
                                   href="#">{{ Carbon\Carbon::parse($post->created_at)->diffForHumans() }}</a>
                                <span class="op-6">ago by</span>
                                <a class="text-black" href="{{ route('user.public', ['id' => $post->user->id]) }}">
                                    {{$post->user->name}}
                                </a>
                            </p>
                            <div class="text-sm op-5">
                                @foreach($post->tags as $tag)
                                    <a class="text-black mr-2"
                                       href="{{ route('member.posts_by_tag', ['tag' => $tag->slug]) }}">#{{$tag->name}}</a>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-4 op-7">
                            <div class="row text-center op-7">
                                <div class="col px-1"><i class="ion-ios-chatboxes-outline icon-1x"></i> <span
                                        class="d-block text-sm">{{$post->comments->count()}} Replys</span></div>
                                <div class="col px-1"><i class="ion-ios-eye-outline icon-1x"></i> <span
                                        class="d-block text-sm">{{$post->views}} Views</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <p>No discussions</p>
            @endforelse
            {{ $posts->links('pagination::bootstrap-5') }}
            <!-- End posts -->


        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Create A Discussion</div>
                <div class="card-body">
                    <div class="input-group">
                        <a href="{{ route('member.forum_create') }}" class="btn btn-primary" id="button-search"
                           type="button">Go!</a>
                    </div>
                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Categories</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="{{ route('member.forum_category', ['slug' => 'collecting']) }}">Collecting</a>
                                </li>
                                <li><a href="{{ route('member.forum_category', ['slug' => 'grading']) }}">Grading</a>
                                </li>
                                <li><a href="{{ route('member.forum_category', ['slug' => 'general']) }}">General
                                        Numismatics</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">

                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.view_directory') }}">User Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            @endif
        </div>

    </div>
    @push('styles')
        <style>
            .forum_index_img {
                height: 30px;
                width: auto;
            }
        </style>
    @endpush
    @push('scripts')
        <script>
            (function ($) {
                try {


                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            }(window.jQuery));
        </script>
    @endpush
@endsection

