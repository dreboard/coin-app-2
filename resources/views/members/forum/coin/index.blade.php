@extends('layouts.user.main')
@section('pageTitle', "$coinCategory->coinCategory Discussion Area")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Discussion Area</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Profile Pages
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">Back To Settings</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.edit_public', ['user' => auth()->user()->id]) }}">Edit This
                            Profile</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_edit_avatar', ['user' => auth()->user()->id]) }}">Change Image</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('member.forum') }}">Forum Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('member.forum_user', ['id' => auth()->user()->id ]) }}">My Forum</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('coin.view_category', ['id' => $coinCategory->id]) }}">{{ $coinCategory->coinCategory }}</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->

        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                {{ $coinCategory->coinCategory }} Discussion Area.
            </div>

            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th class="fw-bold">#</th>
                        <th class="fw-bold">Category</th>
                        <th class="fw-bold">Last</th>
                        <th class="fw-bold">Total</th>
                    </tr>

                    @foreach($type_list as $id => $type)
                        <tr>
                            <th><a href="{{ route('coin.view_type', ['id' => $id]) }}">
                                    <img src="{{config('app.image_url')}}{{ str_replace(' ', '_', $type) }}.jpg"
                                         class="forum_index_img" alt="..."></a></th>
                            <td><span class="cat_id"><a href="{{ route('member.forum_type_index', ['type_id' => $id]) }}"
                                    >{{ $type }}</a></span></td>
                            <td><span id="cat_date"></span>  2 dats ago</td>
                            <td><span id="cat_count"></span>55</td>
                        </tr>

                    @endforeach
                </table>
            </div>


            @forelse($forum_posts as $post)
                <div
                    class="card row-hover pos-relative py-3 px-3 mb-3 border-primary border-top-0 border-right-0 border-bottom-0 rounded-0">
                    <div class="row align-items-center">
                        <div class="col-md-8 mb-3 mb-sm-0">
                            <h5>
                                <a href="{{ route('member.view_forum_post', ['forum' => $post->id]) }}" class="text-primary">{{$post->title}}</a>
                            </h5>
                            <p class="text-sm">
                                <span class="op-6">Posted</span>
                                <a class="text-black"
                                   href="#">{{ Carbon\Carbon::parse($post->created_at)->diffForHumans() }}</a>
                                <span class="op-6">ago by</span>
                                <a class="text-black" href="{{ route('user.public', ['id' => $post->user->id]) }}">
                                    {{$post->user->name}}
                                </a>
                            </p>
                            <div class="text-sm op-5">
                                @foreach($post->tags as $tag)
                                    <a class="text-black mr-2"
                                       href="{{ route('member.posts_by_tag', ['tag' => $tag->slug]) }}">#{{$tag->name}}</a>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-4 op-7">
                            <div class="row text-center op-7">
                                <div class="col px-1"><i class="ion-ios-chatboxes-outline icon-1x"></i> <span
                                        class="d-block text-sm">{{$post->comments->count()}} Replys</span></div>
                                <div class="col px-1"><i class="ion-ios-eye-outline icon-1x"></i> <span
                                        class="d-block text-sm">{{$post->views}} Views</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <p>No discussions for {{ $coinCategory->coinCategory }}</p>
            @endforelse
            {{ $forum_posts->links('pagination::bootstrap-5') }}
            <!-- End posts -->
        </div>
        <!-- Side widgets-->
        @include('members.forum.side')

    </div>
    @push('styles')
        <style>
            .forum_index_img {
                height:30px; width:auto;
            }
        </style>
    @endpush
    @push('scripts')
        <script>
            (function ($) {
                try {

                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            }(window.jQuery));
        </script>
    @endpush
@endsection

