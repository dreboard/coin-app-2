@extends('layouts.user.main')
@section('pageTitle', 'Create Discussion')
@section('content')

    <h3 class="mt-4">Create Discussion</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('member.forum') }}">Forum Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('member.forum_user', ['id' => auth()->user()->id ]) }}">My Forum</a></li>
    </ol>

        <div class="row">
            <div class="col-md-12">
                <form id="post_form" action="{{ route('member.forum_save') }}"
                      method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-12">
                        <div class="mb-3">
                            <label for="forum_title" class="form-label">Title</label>
                            <input type="text" class="form-control" name="title" id="forum_title" value="{{ old('title') }}">
                            <div id="titleHelp" class="form-text">Article Title</div>
                            @error('title')
                            <div class="text-sm text-red-600">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="row mb-3">

                            <div class="col">
                                <label for="forum_type" class="form-label">Discussion Type</label>
                                <select name="type" id="forum_type" class="form-select" aria-label="Default select example">
                                    <option value="Question">Select Type</option>
                                    <option @if(old('forum_type') == 'Collaborative') selected @endif value="Collaborative">Collaborative (Selected Members)</option>
                                    <option @if(old('forum_type') == 'Discussion') selected @endif value="Discussion">Discussion (Open to all)</option>
                                    <option @if(old('forum_type') == 'Question') selected @endif value="Question">Question (Open to all)</option>
                                </select>
                            </div>
                            <div class="col">
                                <label for="forum_private" class="form-label">Display In Forum</label>
                                <select name="private" id="forum_private" class="form-select" aria-label="Default select example">
                                    <option value="Question">Select Level</option>
                                    <option @if(old('private') == 0) selected @endif value="1">Yes, Open</option>
                                    <option @if(old('private') == 1) selected @endif value="0">No, Private</option>
                                </select>
                            </div>

                        </div>

                        <div class="card mb-3">
                            <div class="card-body">
                                <h5 class="card-title">Add/Remove Participants</h5>
                                <div class="table-responsive">
                                    <table class="table table-hover datatable mt-3">
                                        <thead>
                                        <tr>
                                            <th scope="col">User</th>
                                            <th scope="col">Type</th>
                                            <th class="text-end" scope="col">Include</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($followers as $user)
                                            <tr>
                                                <td class="text-start">
                                                    <label for="user_{{ $user->id }}" title="{{ $user->name }}">
                                                        {{ $user->name }}
                                                    </label>
                                                </td>
                                                <td>{{ucfirst($user->user_type)}}</td>
                                                <td style="padding-right: 45px;" class="text-end">
                                                    <input id="user_{{ $user->id }}"
                                                           type="checkbox" name="recipients[]"
                                                           value="{{ $user->id }}" @if(old('recipients')) checked @endif
                                                    ></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                        <div class="card mb-3">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-8"><h5 class="card-title">Add Images</h5></div>
                                    <div class="col-4"><button id="image_clear_btn" type="button" class="btn btn-danger">Clear All</button>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="mb-1">
                                            <label for="image_url_1" class="form-label">Image 1</label>
                                            <input class="form-control img_input" type="file" name="image_url[]" id="image_url_1" onchange="preview_1()">
                                        </div>
                                        <div class="mb-1">
                                            <label for="image_url_2" class="form-label">Image 2</label>
                                            <input class="form-control img_input" type="file" name="image_url[]" id="image_url_2" onchange="preview_2()">
                                        </div>
                                        <div class="mb-1">
                                            <label for="image_url_3" class="form-label">Image 3</label>
                                            <input class="form-control img_input" type="file" name="image_url[]" id="image_url_3" onchange="preview_3()">
                                        </div>
                                        <div class="mb-1">
                                            <label for="image_url_4" class="form-label">Image 4</label>
                                            <input class="form-control img_input" type="file" name="image_url[]" id="image_url_4" onchange="preview_4()">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-6 align-middle text-center"><img id="image_preview_frame_1" src="" class="image_preview"/></div>
                                            <div class="col-sm-6 align-middle text-center"><img id="image_preview_frame_2" src="" class="image_preview"/></div>
                                            <div class="col-sm-6 align-middle text-center"><img id="image_preview_frame_3" src="" class="image_preview"/></div>
                                            <div class="col-sm-6 align-middle text-center"><img id="image_preview_frame_4" src="" class="image_preview"/></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group mb-3">
                            <label class="control-label">Article Text</label>
                            <textarea id="forum_body" name="body" class="form-control">{{ old('body') }}</textarea>
                        </div>

                        <div class="form-group mb-3">
                            <div class="accordion" id="accordionExample">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingOne">
                                        <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                data-bs-target="#collapseOne" aria-expanded="true"
                                                aria-controls="collapseOne">
                                            <span id="checkboxgroupText">Tags (Up to 3)</span>
                                        </button>
                                    </h2>
                                    <div id="collapseOne" class="accordion-collapse collapse show"
                                         aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                        <div id="checkboxgroup" class="accordion-body">

                                            @foreach($tags->chunk(3) as $tagGroup)
                                                <div class="row">
                                                    @foreach($tagGroup as $tag)
                                                        <div class="col-md-3 form-check">
                                                            <input class="form-check-input" name="tags[]"
                                                                   type="checkbox" value="{{$tag->id}}"
                                                                   id="tag_{{$tag->id}}">
                                                            <label class="form-check-label" for="tag_{{$tag->id}}">
                                                                {{$tag->name}}
                                                            </label>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" value="1" name="publish" id="publish_post" checked>
                                    <label class="form-check-label" for="publish_post">
                                        Publish
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" value="0" name="publish" id="draft_post">
                                    <label class="form-check-label" for="draft_post">
                                        Save As Draft
                                    </label>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" value="1" name="public" id="public_post" checked>
                                    <label class="form-check-label" for="public_post">
                                        Public Listing
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" value="0" name="public" id="private_post">
                                    <label class="form-check-label" for="private_post">
                                        Private Listing
                                    </label>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="1" name="featured" id="featured_post">
                                    <label class="form-check-label" for="featured_post">
                                        Featured Listing
                                    </label>
                                </div>

                            </div>
                        </div>

                        <!-- Submit Form Input -->
                        <div class="col-3 mt-3">
                            <button id="post_form_btn" type="submit" class="btn btn-primary form-control">Create</button>
                        </div>
                    </div>
                </form>
            </div>


        </div>


@endsection
@push('styles')
    <style>
        .image_preview {
            max-height: 200px;
            width:100%; height: auto;
        }
    </style>
@endpush
@push('scripts')
    <script src="https://cdn.ckeditor.com/ckeditor5/35.2.1/classic/ckeditor.js"></script>
    <script defer>

        let image_preview_frame_1 = document.getElementById('image_preview_frame_1');
        let image_preview_frame_2 = document.getElementById('image_preview_frame_2');
        let image_preview_frame_3 = document.getElementById('image_preview_frame_3');
        let image_preview_frame_4 = document.getElementById('image_preview_frame_4');
        image_preview_frame_1.style.display = "none";
        function preview_1() {
            image_preview_frame_1.style.display = "inline";
            image_preview_frame_1.src = URL.createObjectURL(event.target.files[0]);
        }
        function preview_2() {
            image_preview_frame_2.style.display = "inline";
            image_preview_frame_2.src = URL.createObjectURL(event.target.files[0]);
        }
        function preview_3() {
            image_preview_frame_3.style.display = "inline";
            image_preview_frame_3.src = URL.createObjectURL(event.target.files[0]);
        }
        function preview_4() {
            image_preview_frame_4.style.display = "inline";
            image_preview_frame_4.src = URL.createObjectURL(event.target.files[0]);
        }


        document.getElementById('image_clear_btn').addEventListener('click', function() {
            image_preview_frame_1.src = "";
            image_preview_frame_2.src = "";
            image_preview_frame_3.src = "";
            image_preview_frame_4.src = "";
            document.getElementById('image_url_1').value = '';
            document.getElementById('image_url_2').value = '';
            document.getElementById('image_url_3').value = '';
            document.getElementById('image_url_4').value = '';
        });

        (function () {
            try {


                function onlyOneCheckBox() {
                    let checkboxgroupText = document.getElementById('checkboxgroupText');
                    let checkboxgroup = document.getElementById('checkboxgroup').getElementsByTagName("input");
                    let limit = 3;
                    for (var i = 0; i < checkboxgroup.length; i++) {
                        checkboxgroup[i].onclick = function () {
                            var checkedcount = 0;
                            for (var i = 0; i < checkboxgroup.length; i++) {
                                checkedcount += (checkboxgroup[i].checked) ? 1 : 0;
                            }
                            if (checkedcount > limit) {
                                console.log("You can select maximum of " + limit + " checkbox.");
                                checkboxgroupText.innerText = "You can select maximum of " + limit + " checkbox.";
                                this.checked = false;
                            }
                        }
                    }
                }

                onlyOneCheckBox();


                ClassicEditor.defaultConfig = {
                    toolbar: {
                        items: [
                            'heading',
                            '|',
                            'bold',
                            'italic',
                            '|',
                            'bulletedList',
                            'numberedList',
                            '|',
                            'insertTable',
                            '|',
                            'undo',
                            'redo'
                        ]
                    },
                    image: {
                        toolbar: [
                            'imageStyle:full',
                            'imageStyle:side',
                            '|',
                            'imageTextAlternative'
                        ]
                    },
                    table: {
                        contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells']
                    },
                    language: 'en'
                };


                ClassicEditor
                    .create(document.querySelector('#forum_body')).catch(error => {
                    console.error(error);
                });


            } catch (error) {
                if (ENVIRONMENT === 'local') {
                    console.error(error);
                }
            }
        })();
    </script>
@endpush
