<div class="col-lg-4">
    <!-- Search widget-->
    <div class="card mb-4">
        <div class="card-header">Create A Discussion</div>
        <div class="card-body">
            <div class="input-group">
                <a href="{{ route('member.forum_create') }}" class="btn btn-primary" id="button-search" type="button">Go!</a>
            </div>
        </div>
    </div>


    <!-- Categories widget-->
    <div class="card mb-4">
        <div class="card-header">Categories</div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="list-unstyled mb-0">
                        <li><a href="{{ route('member.forum_category', ['slug' => 'collecting']) }}">Collecting</a></li>
                        <li><a href="{{ route('member.forum_category', ['slug' => 'collecting']) }}">Grading</a></li>
                        <li><a href="{{ route('member.forum_category', ['slug' => 'collecting']) }}">General Numismatics</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="card mb-4">
        <div class="card-header">Projects</div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <ul class="list-unstyled mb-0">

                        <li><a href="{{ route('user.user_types', ['type' => 'Research']) }}">Researchers Directory</a></li>
                    </ul>
                </div>
                <div class="col-sm-6">
                    <ul class="list-unstyled mb-0">
                        <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                        <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>
