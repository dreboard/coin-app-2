@extends('layouts.user.main')
@section('pageTitle', 'Discussions by user')
@section('content')

    <h3 class="mt-4">Discussions by {{ $user->name }}</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('member.forum') }}">Forum Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('member.forum_user', ['id' => auth()->user()->id ]) }}">My Forum</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8 posts_div  mb-4">
            @if($user->id !== auth()->user()->id)
                <div class="mb-3">
                    @if(auth()->user()->isFollowing($user))
                        <button type="button" class="btn btn-success" id="follow_model_btn">
                            Following
                        </button>
                    @else
                        <button type="button" class="btn btn-primary" id="follow_model_btn">
                            Follow
                        </button>
                    @endif
                </div>
            @endif

            <!-- featured posts -->

            <!-- start posts -->
            @if(!empty($posts) || count($posts) > 0)
                @foreach($posts as $post)
                        <div
                            class="card row-hover pos-relative py-3 px-3 mb-3 border-primary border-top-0 border-right-0 border-bottom-0 rounded-0">
                            <div class="row align-items-center">
                                <div class="col-md-8 mb-3 mb-sm-0">
                                    <div class="col-sm-6">
                                        @if(isset($post->images->image_url))
                                            <figure class="mb-4">
                                                <img src="{{asset('storage/'.$post->previewImage->image_url)}}" style="width: 80%; height: auto" alt=""/>
                                            </figure>
                                        @endif

                                    </div>
                                    <h5>
                                        <a href="{{ route('member.view_forum_post', ['forum' => $post->id]) }}"
                                           class="text-primary">{{ $post->title }}</a>
                                    </h5>
                                    <p class="text-sm"><span class="op-6">Posted</span>
                                        <a class="text-black"
                                           href="{{ route('member.view_forum_post', ['forum' => $post->id]) }}">
                                            {{ Carbon\Carbon::parse($post->created_at)->diffForHumans() }}
                                        </a>
                                        <span class="op-6">ago</span>
                                    <div class="text-sm op-5">
                                        @foreach($post->tags as $tag)
                                            <a class="text-black mr-2" href="{{ route('member.posts_by_tag', ['tag' => $tag->slug]) }}">#{{$tag->name}}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-md-4 op-7">
                                    <div class="row text-center op-7">
                                        <div class="col px-1"><i class="ion-ios-chatboxes-outline icon-1x"></i> <span
                                                class="d-block text-sm">{{$post->comments->count()}} Replys</span></div>
                                        <div class="col px-1"><i class="ion-ios-eye-outline icon-1x"></i> <span
                                                class="d-block text-sm">{{$post->views}} Views</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                @endforeach

                {{ $posts->links('pagination::bootstrap-5') }}

            @endif
                @if(empty($posts) || count($posts) < 1)
                <p>No Current Articles by {{ $user->name }}</p>
            @endif
            <!-- End posts -->
                <div class="h-20"></div>
        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">{{ $user->name }}</div>
                <div class="card-body">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        @if(auth()->user()->isFollowing($user))
                            <button type="button" class="btn btn-success" id="follow_model_btn">
                                Following
                            </button>
                        @else
                            <button type="button" class="btn btn-primary" id="follow_model_btn">
                                Follow
                            </button>
                        @endif
                        <button type="button" class="btn btn-secondary" data-bs-toggle="modal"
                                data-bs-target="#userMessageModal">Contact
                        </button>
                        <button type="button" class="btn btn-warning">Report</button>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="userMessageModal" tabindex="-1"
                         aria-labelledby="userMessageModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="modal-title fs-5" id="userMessageModalLabel">
                                        Contact {{ $user->name }}</h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form method="post" id="user_message_form"
                                          action="{{ route('user.user_contact') }}">
                                        @csrf
                                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                                        <div class="mb-3">
                                            <label for="user_message_subject" class="form-label">Subject</label>
                                            <input class="form-control" id="user_message_subject" name="subject"
                                                   type="text"/>
                                        </div>
                                        <div class="mb-3">
                                            <label for="user_message_body" class="form-label">Message</label>
                                            <textarea class="form-control" id="user_message_body" name="body"
                                                      rows="3"></textarea>
                                        </div>
                                        <div class="col-12">
                                            <button id="user_message_btn" type="submit" class="btn btn-primary">Send
                                            </button>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a target="_blank" href="{{ route('view_policy', ['id' => 4]) }}" class="btn btn-warning">Spam Policy</a>
                                        <button href="{{ route('messages.messages') }}" type="button" class="btn btn-primary">My Messages</button>
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal end -->


                </div>
            </div>

            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Connect</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.writers') }}">Writers Section</a></li>
                                <li><a href="{{ route('user.user_types', ['type' => 'Writer']) }}">Writers Directory</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="https://www.nlgonline.org/">The Numismatic Literary Guild</a>
                                </li>
                                <li><a class="external_link" href="https://www.coinbooks.org/index.html">Numismatic Bibliomania
                                        Society</a></li>
                                <li><a class="external_link" href="https://nnp.wustl.edu/">The Newman Numismatic Portal</a></li>
                                <li><a class="external_link" href="https://donum.numismatics.org/">Database Of Numismatic
                                        Materials</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
@push('styles')
    <style>
        .posts_div nav {
            margin-bottom: 12px;
        }
        .post_image {
            width: 50%;
            height: auto
        }
        figure {
            text-align: center;
        }
    </style>
@endpush
@push('scripts')
    <script>
        (function (ENVIRONMENT) {
            try {
                let follow_model_btn = document.getElementById('follow_model_btn');
                follow_model_btn.addEventListener('click', function () {

                    fetch('{{ route('user.user_follow') }}', {
                        headers: {"Content-Type": "application/json; charset=utf-8"},
                        method: 'POST',
                        body: JSON.stringify({
                            _token: @json(csrf_token()),
                            model_type: 'user',
                            model_id: {{ $user->id ?? auth()->user()->id }},
                        })
                    }).then(response => response.json())
                        .then(data => {
                            console.log(data);
                            if (data.result == 'Success') {
                                follow_model_btn.innerHTML = 'Following';
                                follow_model_btn.classList.add('btn-success')
                            } else {
                                follow_model_btn.classList.add('text-danger');
                                follow_model_btn.innerHTML = 'Error';
                            }
                        })
                });
            } catch (error) {
                if (ENVIRONMENT === 'local') {
                    console.error(error);
                }
            }
        })();
    </script>
@endpush
