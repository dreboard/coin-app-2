@extends('layouts.user.main')
@section('pageTitle', 'View Forum Post')
@section('content')

    <h3 class="mt-4">{{$forum->title}}</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('member.forum') }}">Forum Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('member.forum_user', ['id' => auth()->user()->id ]) }}">My Forum</a></li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                        aria-expanded="false">
                    Options
                </button>
                <ul class="dropdown-menu">
                    @if($forum->user_id == auth()->user()->id)
                        <li><a class="dropdown-item" href="{{ route('member.forum_edit', ['forum' => $forum->id]) }}">Edit
                                Post</a></li>
                        <li><a class="dropdown-item" href="{{ route('member.forum_edit', ['forum' => $forum->id]) }}">Change Images</a></li>
                        <li><a class="dropdown-item" href="{{ route('member.forum_edit', ['forum' => $forum->id]) }}">Create
                                Project With Participants</a></li>
                        <li><a class="dropdown-item" href="{{ route('member.forum_edit', ['forum' => $forum->id]) }}">Create
                                Group With Participants</a></li>
                    @endif
                    @if($forum->user_id !== auth()->user()->id)
                        <li><a data-bs-toggle="modal" data-bs-target="#exampleModal2" class="dropdown-item"
                               href="{{ route('member.forum_edit', ['forum' => $forum->id]) }}">Leave Discussion</a>
                        </li>
                    @endif
                </ul>
            </div>

            <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel2"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="exampleModalLabel2">Specialty</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <h4>This is a private invitation only group</h4>
                            <a class="btn btn-danger" href="{{ route('member.forum_edit', ['forum' => $forum->id]) }}">Leave
                                Discussion</a>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <!-- start posts -->
            <div class="card row-hover pos-relative py-3 px-3 mb-3 border-primary border-top-0 border-right-0 border-bottom-0 rounded-0">
                <div class="row align-items-center">
                    <div class="col-md-8 mb-3 mb-sm-0">
                        <h5>
                            {{$forum->title}}
                        </h5>
                        <p class="text-sm"><span class="op-6">Posted</span>
                            <a class="text-black" href="#">
                                {{ Carbon\Carbon::parse($forum->created_at)->diffForHumans() }}
                            </a>
                            By <a class="text-black" href="{{ route('member.forum_user', ['id' => $forum->user->id ]) }}">
                                {{ $forum->user->name }}
                            </a>
                        </p>
                        <div class="text-sm op-5">
                            @if($forum->forumable_type == "App\Models\Coins\CoinType")
                                <a class="text-black mr-2" href="{{ route('member.forum_type_index', ['type_id' => $forum->forumable_id]) }}">
                                    {{Config::get('constants.coins.type_name_from_id')[$forum->forumable_id]}}
                                </a>

                            @endif

                            @foreach($forum->tags as $tag)
                                <a class="text-black mr-2" href="{{ route('member.posts_by_tag', ['tag' => $tag->slug]) }}">#{{$tag->name}}</a>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-4 op-7">
                        <div class="row text-center op-7">
                            <div class="col px-1"><span class="d-block text-sm">{{$forum->comments_count}} Replys</span>
                            </div>
                            <div class="col px-1"><span class="d-block text-sm">{{$forum->views}} Views</span></div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="row">
                @foreach($forum->images as $image)
                    <div class="col-sm-6">
                        <figure class="mb-4">
                            <a href="{{ route('member.view_image', ['image' => $image->id]) }}">
                                <img src="{{asset('storage/'.$image->image_url)}}" style="width: 80%; height: auto" alt=""/>
                            </a>
                        </figure>
                    </div>
                @endforeach
            </div>






            <p class="mt-3 mb-3">{!! $forum->body !!}</p>

            <!-- End posts -->

            <section class="mb-5 mt-5">
                <div class="card bg-light">
                    <div class="card-body">
                        <!-- Comment form-->
                        @include('layouts.partials.forum_comment_replies', ['comments' => $forum->comments, 'forum_id' => $forum->id])
                        <form class="mb-4" action="{{ route('member.save_forum_comment') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                            <input type="hidden" name="forum_id" value="{{$forum->id }}">
                            <input type="hidden" name="group_id" value="0">

                            <div class="mb-3">
                                <label for="image_path" class="form-label">Attach an image</label>
                                <input class="form-control" type="file" name="image_url" id="image_url">
                            </div>
                            <div class="form-group mb-3">
                                <label class="control-label">Comment</label>
                                <textarea id="comment_body" name="comment_body" class="form-control" rows="3"
                                          placeholder="Join the discussion and leave a comment!"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                    </div>
                </div>
            </section>
        </div>


        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Participants</div>
                <div class="card-body">
                    <div class="row">
                        @foreach($forum->participants->chunk(3) as $tagGroup)
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    @foreach($tagGroup as $participant)
                                        <li>
                                            <a href="{{ route('user.public', ['id' => $participant->id]) }}">{{ $participant->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Connect</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.writers') }}">Writers Section</a></li>
                                <li><a href="{{ route('user.user_types', ['type' => 'Writer']) }}">Writers Directory</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="https://www.nlgonline.org/">The Numismatic Literary
                                        Guild</a>
                                </li>
                                <li><a class="external_link" href="https://www.coinbooks.org/index.html">Numismatic
                                        Bibliomania
                                        Society</a></li>
                                <li><a class="external_link" href="https://nnp.wustl.edu/">The Newman Numismatic Portal</a>
                                </li>
                                <li><a class="external_link" href="https://donum.numismatics.org/">Database Of Numismatic
                                        Materials</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('styles')
        <style>
            .display-comment .display-comment {
                margin-left: 40px
            }

            .ck-editor__editable {
                min-height: 250px;
            }
        </style>
    @endpush
    @push('scripts')
        <script src="https://cdn.ckeditor.com/ckeditor5/35.2.1/classic/ckeditor.js"></script>
        <script>
            (function () {
                try {
                    ClassicEditor.defaultConfig = {
                        toolbar: {
                            items: [
                                'heading',
                                '|',
                                'bold',
                                'italic',
                                '|',
                                'bulletedList',
                                'numberedList',
                                '|',
                                'insertTable',
                                '|',
                                'undo',
                                'redo'
                            ]
                        },
                        image: {
                            toolbar: [
                                'imageStyle:full',
                                'imageStyle:side',
                                '|',
                                'imageTextAlternative'
                            ]
                        },
                        table: {
                            contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells']
                        },
                        language: 'en'
                    };


                    ClassicEditor
                        .create(document.querySelector('#comment_body')).catch(error => {
                        console.error(error);
                    });

                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
