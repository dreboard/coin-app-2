@extends('layouts.user.main')
@section('pageTitle', "$coinType->coinType Discussion Area")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Discussion Area</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Profile Pages
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">Back To Settings</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.edit_public', ['user' => auth()->user()->id]) }}">Edit This
                            Profile</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_edit_avatar', ['user' => auth()->user()->id]) }}">Change Image</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('member.forum') }}">Forum Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('member.forum_user', ['id' => auth()->user()->id ]) }}">My Forum</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('member.forum_cat_index', ['cat_id' => $coinType->coincats_id]) }}">{{ $coinType->coinCategory }}</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->

        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                <a
                    href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">{{ $coinType->coinType }}</a> Discussion Area.
            </div>

            <!-- Featured blog post-->
            {{--@if($forum_posts->currentPage() == 1)
                <div class="card mb-4">
                    <div class="card-body">
                        <figure class="mb-4">
                            <img src="{{asset('storage/forum/default.jpg')}}" width="250" alt=""/>
                        </figure>
                        <div class="small text-muted">January 1, 2022</div>
                        <h2 class="card-title">Featured Discussion</h2>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis
                            aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi
                            vero voluptate voluptatibus possimus, veniam magni quis!</p>
                        <a class="btn btn-primary" href="#!">Read more →</a>
                    </div>
                </div>
            @endif--}}
            <!-- Nested row for non-featured posts-->
            <!-- start posts -->
            @forelse($forum_posts as $post)
                <div
                    class="card row-hover pos-relative py-3 px-3 mb-3 border-primary border-top-0 border-right-0 border-bottom-0 rounded-0">
                    <div class="row align-items-center">
                        <div class="col-md-8 mb-3 mb-sm-0">
                            <h5>
                                <a href="{{ route('member.view_forum_post', ['forum' => $post->id]) }}" class="text-primary">{{$post->title}}</a>
                            </h5>
                            <p class="text-sm">
                                <span class="op-6">Posted</span>
                                <a class="text-black"
                                   href="#">{{ Carbon\Carbon::parse($post->created_at)->diffForHumans() }}</a>
                                <span class="op-6">ago by</span>
                                <a class="text-black" href="{{ route('user.public', ['id' => $post->user->id]) }}">
                                    {{$post->user->name}}
                                </a>
                            </p>
                            <div class="text-sm op-5">
                                @foreach($post->tags as $tag)
                                    <a class="text-black mr-2"
                                       href="{{ route('member.posts_by_tag', ['tag' => $tag->slug]) }}">#{{$tag->name}}</a>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-4 op-7">
                            <div class="row text-center op-7">
                                <div class="col px-1"><i class="ion-ios-chatboxes-outline icon-1x"></i> <span
                                        class="d-block text-sm">{{$post->comments->count()}} Replys</span></div>
                                <div class="col px-1"><i class="ion-ios-eye-outline icon-1x"></i> <span
                                        class="d-block text-sm">{{$post->views}} Views</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <p>No discussions for {{ $coinType->coinType }}</p>
            @endforelse
            {{ $forum_posts->links('pagination::bootstrap-5') }}
            <!-- End posts -->
        </div>
        <!-- Side widgets-->
        @include('members.forum.side')

    </div>

@endsection

