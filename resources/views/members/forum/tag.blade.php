@extends('layouts.user.main')
@section('pageTitle', 'Articles by user')
@section('content')

    <h3 class="mt-4">Articles Labeled {{ $tag }}</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        {{ route('user.writers') }}
        <li class="breadcrumb-item"><a href="{{ route('user.user_types', ['type' => 'Writer']) }}">Writers Directory</a>
        </li>
        <li class="breadcrumb-item"><a href="{{ route('member.user_posts', ['id' => auth()->user()->id]) }}">My
                Articles</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8 posts_div  mb-4">
            <!-- start posts -->
            @if(!empty($posts) || count($posts) > 0)
                @foreach($posts as $post)
                        <div
                            class="card row-hover pos-relative py-3 px-3 mb-3 border-primary border-top-0 border-right-0 border-bottom-0 rounded-0">
                            <div class="row align-items-center">
                                <div class="col-md-8 mb-3 mb-sm-0">
                                    <h5>
                                        <a href="{{ route('member.view_post', ['post' => $post->id]) }}"
                                           class="text-primary">{{ $post->title }}</a>
                                    </h5>
                                    <p class="text-sm"><span class="op-6">Posted</span> <a class="text-black"
                                                                                           href="{{ route('member.view_post', ['post' => $post->id]) }}">{{ Carbon\Carbon::parse($post->created_at)->diffForHumans() }}</a>
                                        <span class="op-6">ago</span>
                                        By <a class="text-black" href="{{ route('member.user_posts', ['id' => $post->user->id]) }}">
                                            {{ $post->user->name }}
                                        </a>
                                    <div class="text-sm op-5">
                                        @foreach($post->tags as $tag)
                                            <a class="text-black mr-2" href="{{ route('member.posts_by_tag', ['tag' => $tag->slug]) }}">#{{$tag->name}}</a>&nbsp;
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-md-4 op-7">
                                    <div class="row text-center op-7">
                                        <div class="col px-1"><i class="ion-ios-chatboxes-outline icon-1x"></i> <span
                                                class="d-block text-sm">{{$post->comments->count()}} Replys</span></div>
                                        <div class="col px-1"><i class="ion-ios-eye-outline icon-1x"></i> <span
                                                class="d-block text-sm">{{$post->views}} Views</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                @endforeach

                {{ $posts->links('pagination::bootstrap-5') }}

            @endif
                @if(empty($posts) || count($posts) < 1)
                <p>No Current Articles with {{ $tag }}</p>
            @endif
            <!-- End posts -->
                <div class="h-20"></div>
        </div>

        <!-- Side widgets-->
        @include('members.writers.partials.side')

    </div>

@endsection
@push('styles')
    <style>
        .posts_div nav {
            margin-bottom: 12px;
        }
        .post_image {
            width: 50%;
            height: auto
        }
        figure {
            text-align: center;
        }
    </style>
@endpush

