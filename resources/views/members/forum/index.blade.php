@extends('layouts.user.main')
@section('pageTitle', 'Discussion Area')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Discussion Area</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Profile Pages
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">Back To Settings</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.edit_public', ['user' => auth()->user()->id]) }}">Edit This
                            Profile</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_edit_avatar', ['user' => auth()->user()->id]) }}">Change Image</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.directory') }}">User Directory</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.public', ['id' => auth()->user()->id]) }}">My
                Profile</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <!-- Featured blog post-->

            <div class="card mb-3">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                    <a href="#" class="card-link">Card link</a>
                    <a href="#" class="card-link">Another link</a>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th class="fw-bold">#</th>
                        <th class="fw-bold">Category</th>
                        <th class="fw-bold">Last</th>
                        <th class="fw-bold">Total</th>
                    </tr>

                    @foreach($categories as $id => $category)
                        <tr class="info_row">
                            <th><a href="{{ route('coin.view_category', ['id' => $id]) }}">
                                    <img src="{{config('app.image_url')}}{{ str_replace(' ', '_', $category) }}.jpg"
                                         class="forum_index_img" alt="..."></a></th>
                            <td><span class="cat_ids" data-category="{{$id}}"><a
                                        href="{{ route('member.forum_cat_index', ['cat_id' => $id]) }}"
                                    >{{ $category }}</a></span></td>
                            <td><span class="cat_date" id="cat_date_{{$id}}">Loading...</span></td>
                            <td><span class="cat_count" id="cat_count_{{$id}}">Loading...</span></td>
                        </tr>

                    @endforeach
                </table>
            </div>


        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Create A Discussion</div>
                <div class="card-body">
                    <div class="input-group">
                        <a href="{{ route('member.forum_create') }}" class="btn btn-primary" id="button-search"
                           type="button">Go!</a>
                    </div>
                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Categories</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="{{ route('member.forum_category', ['slug' => 'collecting']) }}">Collecting</a>
                                </li>
                                <li><a href="{{ route('member.forum_category', ['slug' => 'grading']) }}">Grading</a>
                                </li>
                                <li><a href="{{ route('member.forum_category', ['slug' => 'general']) }}">General
                                        Numismatics</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">

                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.view_directory') }}">User Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            @endif
        </div>

    </div>
    @push('styles')
        <style>
            .forum_index_img {
                height: 30px;
                width: auto;
            }
        </style>
    @endpush
    @push('scripts')
        <script>
            (function ($) {
                try {
                    const cat_ids = document.querySelectorAll('.cat_ids');
                    Array.from(cat_ids)
                        .forEach(function (cat_id) {
                            window.addEventListener('load', function (event) {
                                let url = "{{ route('member.get_category_info', [':cat_id']) }}".replace(':cat_id', cat_id.dataset.category);
                                fetch(url, {
                                    "method": "GET",
                                }).then(
                                    response => {
                                        response.json().then(
                                            data => {
                                                document.getElementById("cat_count_" + cat_id.dataset.category).innerHTML = data.data.total;
                                                document.getElementById("cat_date_" + cat_id.dataset.category).innerHTML = data.data.last;
                                            }
                                        )
                                    })
                            }, false)
                        })

                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            }(window.jQuery));
        </script>
    @endpush
@endsection

