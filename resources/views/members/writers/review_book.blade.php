@extends('layouts.user.main')
@section('pageTitle', 'Book Review')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">{{ $book->title }}: Review</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    CHANGE
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">Back To Settings</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.edit_public', ['user' => auth()->user()->id]) }}">Edit This
                            Profile</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_edit_avatar', ['user' => auth()->user()->id]) }}">Change Image</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.directory') }}">Writers Area</a></li>
        <li class="breadcrumb-item"><a href="{{ route('member.view_books') }}">All Books</a></li>
    </ol>

    <div class="row">
        <div class="col-lg-8">
            <form action="{{ route('member.save_review') }}" method="post" id="groupForm" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="source_id" value="{{ $book->id }}">

                <div class="mb-3">
                    <label for="comment" class="form-label">Review</label>
                    <textarea name="comment" id="comment" class="form-control"
                              placeholder="Detailed comment">{{ old('comment') }}</textarea>
                </div>
                <div class="mb-3">
                    <label for="score" class="form-label">Rating</label>
                    <select name="score" id="score" class="form-select" aria-label="Default select example">
                        <option selected>1 to 5 Stars</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                        <option value="4">Four</option>
                        <option value="5">Five</option>
                    </select>
                </div>



                <div class="form-group mt-3">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>


        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">My Area</div>
                <div class="card-body">

                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                aria-expanded="false">
                            Create Something
                        </button>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="{{ route('group.start') }}">Article</a></li>
                            <li><a class="dropdown-item" href="{{ route('group.start') }}">Review</a></li>
                            <li><a class="dropdown-item" href="{{ route('group.start') }}">Book</a></li>
                        </ul>
                    </div>
                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Connect</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('user.user_types', ['type' => 'Writer']) }}">Writers Directory</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="https://www.nlgonline.org/">The Numismatic Literary
                                        Guild</a></li>
                                <li><a class="external_link" href="https://www.coinbooks.org/index.html">Numismatic
                                        Bibliomania Society</a></li>
                                <li><a class="external_link" href="https://nnp.wustl.edu/">The Newman Numismatic
                                        Portal</a></li>
                                <li><a class="external_link" href="https://donum.numismatics.org/">Database Of
                                        Numismatic Materials</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
@push('styles')
    <style>
        .ck-editor__editable {
            min-height: 350px;
        }
        label {
            font-weight: bold;
        }
    </style>
@endpush
@push('scripts')
    <script src="https://cdn.ckeditor.com/ckeditor5/35.2.1/classic/ckeditor.js"></script>
    <script>
        (function () {
            try {
                ClassicEditor.defaultConfig = {
                    toolbar: {
                        items: [
                            'heading',
                            '|',
                            'bold',
                            'italic',
                            '|',
                            'bulletedList',
                            'numberedList',
                            '|',
                            'insertTable',
                            '|',
                            'undo',
                            'redo'
                        ]
                    },
                    image: {
                        toolbar: [
                            'imageStyle:full',
                            'imageStyle:side',
                            '|',
                            'imageTextAlternative'
                        ]
                    },
                    table: {
                        contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells']
                    },
                    language: 'en'
                };


                ClassicEditor
                    .create(document.querySelector('#comment')).catch(error => {
                    console.error(error);
                });


            } catch (error) {
                if (ENVIRONMENT === 'local') {
                    console.error(error);
                }
            }
        })();
    </script>
@endpush
