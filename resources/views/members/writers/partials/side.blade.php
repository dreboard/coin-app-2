<div class="col-lg-4">
    <!-- Search widget-->
    <div class="card mb-4">
        <div class="card-header">My Area</div>
        <div class="card-body">
            <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                        aria-expanded="false">
                    Create Something
                </button>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="{{ route('member.create_posts') }}">Article</a></li>
                    <li><a class="dropdown-item" href="{{ route('member.create_posts') }}">Review</a></li>
                    <li><a class="dropdown-item" href="{{ route('member.create_posts') }}">Book</a></li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Categories widget-->
    <div class="card mb-4">
        <div class="card-header">Connect</div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <ul class="list-unstyled mb-0">
                        <li><a href="{{ route('user.writers') }}">Writers Section</a></li>
                        <li><a href="{{ route('user.user_types', ['type' => 'Writer']) }}">Writers Directory</a></li>
                    </ul>
                </div>
                <div class="col-sm-6">
                    <ul class="list-unstyled mb-0">
                        <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                        <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="card mb-4">
        <div class="card-header">Resources</div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="list-unstyled mb-0">
                        <li><a class="external_link" href="https://www.nlgonline.org/">The Numismatic Literary Guild</a>
                        </li>
                        <li><a class="external_link" href="https://www.coinbooks.org/index.html">Numismatic Bibliomania
                                Society</a></li>
                        <li><a class="external_link" href="https://nnp.wustl.edu/">The Newman Numismatic Portal</a></li>
                        <li><a class="external_link" href="https://donum.numismatics.org/">Database Of Numismatic
                                Materials</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
