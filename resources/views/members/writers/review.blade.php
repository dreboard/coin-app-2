@extends('layouts.user.main')
@section('pageTitle', 'Writers Area')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Writers Area</h3>
        </div>
        <div class="col-6">
                <div class="dropdown mt-4 float-end">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                            data-bs-toggle="dropdown" aria-expanded="false">
                        CHANGE
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <li><a class="dropdown-item"
                               href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">Back To Settings</a>
                        </li>
                        <li><a class="dropdown-item"
                               href="{{ route('user.edit_public', ['user' => auth()->user()->id]) }}">Edit This
                                Profile</a>
                        </li>
                        <li><a class="dropdown-item"
                               href="{{ route('user.user_edit_avatar', ['user' => auth()->user()->id]) }}">Change Image</a>
                        </li>
                    </ul>
                </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.directory') }}">Writers Area</a></li>
            <li class="breadcrumb-item"><a href="{{ route('user.public', ['id' => auth()->user()->id]) }}">My
                    Articles</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">


            <div class="table-responsive">
                <table id="user_directory_tbl" class="table datatable">
                    <thead>
                    <tr>
                        <th class="fw-bold">Name</th>
                        <th class="fw-bold">Profile</th>
                        <th class="fw-bold">Type</th>
                        <th class="fw-bold">Specialty</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th class="fw-bold">Title</th>
                        <th class="fw-bold">Author</th>
                        <th class="fw-bold">Type</th>
                        <th class="fw-bold">Review</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach ($books as $book)
                        <tr>
                            <td class="text-start" style="width: 40%"><a href="{{ route('member.view_book', ['id' => $book->id]) }}">{{ $book->title }}</a></td>
                            <td class="text-start"><a href="{{ route('member.view_book', ['id' => $book->id]) }}">View</a></td>
                            <td class="text-start">
                                <a href="{{ route('member.view_book', ['id' => $book->id]) }}">{{ $book->id }}</a>
                            </td>
                            <td class="text-start">
                                <a href="{{ route('member.review_book', ['id' => $book->id]) }}">Review</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">My Area</div>
                <div class="card-body">

                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Create Something
                        </button>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="{{ route('group.start') }}">Article</a></li>
                            <li><a class="dropdown-item" href="{{ route('group.start') }}">Review</a></li>
                            <li><a class="dropdown-item" href="{{ route('group.start') }}">Book</a></li>
                        </ul>
                    </div>
                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Connect</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('user.user_types', ['type' => 'Writer']) }}">Writers Directory</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="https://www.nlgonline.org/">The Numismatic Literary Guild</a></li>
                                <li><a class="external_link" href="https://www.coinbooks.org/index.html">Numismatic Bibliomania Society</a></li>
                                <li><a class="external_link" href="https://nnp.wustl.edu/">The Newman Numismatic Portal</a></li>
                                <li><a class="external_link" href="https://donum.numismatics.org/">Database Of Numismatic Materials</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

