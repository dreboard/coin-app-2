@extends('layouts.user.main')
@section('pageTitle', 'My Unpublished Posts')
@section('content')

    <!-- top widgets-->
    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">My Unpublished Posts</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    My Posts
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('member.user_posts', ['id' => auth()->user()->id]) }}">My Posts</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('member.unpublished_posts') }}">My Unpublished</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.posts_index') }}">All Posts</a></li>
        <li class="breadcrumb-item"><a href="{{ route('member.user_posts', ['id' => auth()->user()->id]) }}">My Posts</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8 posts_div  mb-4">


            <div class="table-responsive">
                <table id="user_datatable" class="table datatable table-hover table-striped">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Created</th>
                        <th>Specialty</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Title</th>
                        <th>Created</th>
                        <th>Specialty</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($posts as $post)
                        <tr id="groupRow{{$post->id}}">
                            <td class="text-start w-75"><a href="{{ route('member.edit_posts', ['post' => $post->id]) }}">{{ Str::limit($post->title, 40) }}</a></td>
                            <td class="text-start">{{ Carbon\Carbon::parse($post->created_at)->diffForHumans() }}</td>
                            <td class="text-start">
                                <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#exampleModal"><i class="bi bi-trash"></i></button>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <!-- Side widgets-->
        @include('members.writers.partials.side')

    </div>

@endsection
@push('styles')
    <style>

    </style>
@endpush
@push('scripts')
    <script>
        (function (ENVIRONMENT) {
            try {

            } catch (error) {
                if (ENVIRONMENT === 'local') {
                    console.error(error);
                }
            }
        })();
    </script>
@endpush
