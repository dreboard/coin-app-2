@extends('layouts.user.main')
@section('pageTitle', 'Edit An Article')
@section('content')

    <h3 class="mt-4"><span class="fw-bold">Edit: </span>{{$post->title}}</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.posts_index') }}">All Posts</a></li>
        <li class="breadcrumb-item"><a href="{{ route('member.user_posts', ['id' => auth()->user()->id]) }}">My Posts</a></li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <a class="btn btn-primary" href="{{ route('member.view_post', ['post' => $post->id]) }}">Back to article</a>

            <form id="edit_post_form" action="{{ route('member.save_posts_edit') }}"
                  method="post" id="groupForm" enctype="multipart/form-data" class="mt-3">
                @csrf
                <div class="col-md-12">
                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                    <input type="hidden" name="group_id" value="0">
                    <input type="hidden" name="post_id" value="{{$post->id }}">
                    <div class="form-group mb-3">
                        <label for="text" class="form-label">Title</label>
                        <input type="text" class="form-control" name="title" id="title"
                               value="{{ old('title', $post->title) }}">
                        @error('title')
                        <div class="text-sm text-red-600">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="card mb-3">
                        <div class="card-body">
                            @if (!empty($post->images))
                                <div class="row mb-2">
                                    <div class="col-10 float-start">
                                        <h5>Current Images (<span id="current_count_display">{{$imgCount}}</span>) (Allowed up to 4)</h5>
                                    </div>
                                    <div class="col-2 float-end">
                                        <button data-bs-toggle="modal" data-bs-target="#deleteImagesModal" type="button" class="btn btn-danger">Delete All</button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="modal fade" id="deleteImagesModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h1 class="modal-title fs-5" id="exampleModalLabel">Delete All Images</h1>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <button id="image_delete_all_btn" type="button" class="btn btn-large btn-danger">Yes Delete All</button>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row row-cols-4 edit_preview_div text-center">
                                    @foreach($post->images as $image)
                                        <div id="image_preview_div_{{$image->id}}" class="col form-check">
                                            <figure class="mb-4">
                                                <img id="image_tag_{{$image->id}}" class="edit_preview image_preview_tags" src="{{asset('storage/'.$image->image_url)}}"
                                                     alt=""/>
                                            </figure>
                                            <button type="button"
                                                    id="image_delete_btn_{{$image->id}}"
                                                    data-image_id="{{$image->id}}"
                                                    data-post_id="{{$post->id}}"
                                                    onclick="deleteImg({{$image->id}})"
                                                    class="btn btn-danger image_delete_btns">
                                                <i class="bi bi-trash-fill"></i>
                                            </button>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                    {{-- Start Image--}}

{{--                    @if($imgCount == 4)--}}
                        <div id="images_full_div" class="card mb-3 alert alert-warning">
                            <div class="card-body">
                                <p>Your image count is 4, please delete an image above or change it</p>
                            </div>
                        </div>
{{--                    @else--}}
                        <div id="all_image_inputs_div" class="card mb-3">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-10"><h5 class="card-title">Add Images</h5></div>
                                    <div class="col-2">
                                        <button id="image_clear_btn" type="button" class="btn btn-danger">Clear All
                                        </button>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div id="image_div_1" class="mb-1">
                                            <label for="image_url_1" class="form-label">Select Image</label>
                                            <input class="form-control img_input" type="file" name="image_url[]"
                                                   id="image_url_1" onchange="preview('image_preview_frame_1')">
                                        </div>
                                        <div id="image_div_2" class="mb-1">
                                            <label for="image_url_2" class="form-label">Select Image</label>
                                            <input class="form-control img_input" type="file" name="image_url[]"
                                                   id="image_url_2" onchange="preview('image_preview_frame_2')">
                                        </div>
                                        <div id="image_div_3" class="mb-1">
                                            <label for="image_url_3" class="form-label">Select Image</label>
                                            <input class="form-control img_input" type="file" name="image_url[]"
                                                   id="image_url_3" onchange="preview('image_preview_frame_3')">
                                        </div>
                                        <div id="image_div_4" class="mb-1">
                                            <label for="image_url_4" class="form-label">Select Image</label>
                                            <input class="form-control img_input" type="file" name="image_url[]"
                                                   id="image_url_4" onchange="preview('image_preview_frame_4')">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-6 align-middle text-center"><img
                                                    id="image_preview_frame_1"
                                                    src=""
                                                    class="image_preview"/>
                                            </div>
                                            <div class="col-sm-6 align-middle text-center"><img
                                                    id="image_preview_frame_2"
                                                    src=""
                                                    class="image_preview"/>
                                            </div>
                                            <div class="col-sm-6 align-middle text-center"><img
                                                    id="image_preview_frame_3"
                                                    src=""
                                                    class="image_preview"/>
                                            </div>
                                            <div class="col-sm-6 align-middle text-center"><img
                                                    id="image_preview_frame_4"
                                                    src=""
                                                    class="image_preview"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
{{--                    @endif--}}


                    {{-- End Image--}}

                    <div class="form-group mb-3">
                        <label class="control-label">Message</label>
                        <textarea id="post_body" name="body"
                                  class="form-control">{{ old('body', $post->body) }}</textarea>
                    </div>

                    <div class="form-group mb-3">
                        <div class="accordion" id="accordionExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne">
                                        <span id="checkboxgroupText">Tags (Up to 3)</span>
                                    </button>
                                </h2>
                                <div id="collapseOne" class="accordion-collapse collapse show"
                                     aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                    <div id="checkboxgroup" class="accordion-body">

                                        @foreach($tags->chunk(3) as $tagGroup)
                                            <div class="row">
                                                @foreach($tagGroup as $tag)
                                                    <div class="col-md-3 form-check">
                                                        <input class="form-check-input" name="tags[]"
                                                               type="checkbox" value="{{$tag->id}}"
                                                               id="tag_{{$tag->id}}"
                                                               @if(in_array($tag->id, $saved_tags)) checked @endif>
                                                        <label class="form-check-label" for="tag_{{$tag->id}}">
                                                            {{$tag->name}}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-5">
                        <div class="col-4">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" value="1" name="publish" id="publish_post"
                                       @if($post->publish == 1) checked @endif>
                                <label class="form-check-label" for="publish_post">
                                    Publish
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" value="0" name="publish" id="draft_post"
                                       @if($post->publish == 0) checked @endif>
                                <label class="form-check-label" for="draft_post">
                                    Save As Draft
                                </label>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" value="1" name="public" id="public_post"
                                       @if($post->public == 1) checked @endif>
                                <label class="form-check-label" for="public_post">
                                    Public Listing
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" value="0" name="public" id="private_post"
                                       @if($post->public == 0) checked @endif>
                                <label class="form-check-label" for="private_post">
                                    Private Listing
                                </label>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="1" name="featured"
                                       id="featured_post" @if($post->featured == 1) checked @endif>
                                <label class="form-check-label" for="featured_post">
                                    Featured Listing
                                </label>
                            </div>
                        </div>
                    </div>

                    <!-- Submit Form Input -->
                    <div class="col-3">
                        <button type="submit" class="btn btn-primary form-control">Update</button>
                    </div>
                </div>
            </form>
            <div class="mh-100"></div>
        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Your Unpublished</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">

                            <table class="table fixed_header table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">Unpublished</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($unpublished as $unpublishedPost)
                                    <tr>
                                        <td class="w-75">
                                            <a class="group_link"
                                               href="{{ route('member.edit_posts', ['post' => $unpublishedPost->id]) }}"
                                               title="{{$unpublishedPost->title}}">{{ Str::limit($unpublishedPost->title, 38) }}</a>
                                        </td>
                                        <td>
                                            {{ \Carbon\Carbon::parse($unpublishedPost->created_at)->diffForHumans() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
@push('styles')
    <style>
        .image_preview {
            max-height: 200px;
            width: 100%;
            height: auto;
        }

        .edit_preview_div img {
            max-height: 100px;
            width: 100%;
            height: auto;
        }

        .edit_preview_div2 img {
            object-fit: cover;
        }

        .edit_preview_div img {
            object-fit: cover;
        }
    </style>
@endpush
@push('scripts')
    <script src="https://cdn.ckeditor.com/ckeditor5/35.2.1/classic/ckeditor.js"></script>
    <script>
        try{
            let image_count = {{$imgCount}};
            let images_full_div = document.getElementById('images_full_div');
            let all_image_inputs_div = document.getElementById('all_image_inputs_div');
            let image_preview_frame_2 = document.getElementById('image_preview_frame_2');
            let image_preview_frame_3 = document.getElementById('image_preview_frame_3');
            let image_preview_frame_4 = document.getElementById('image_preview_frame_4');
            let image_url_1 = document.getElementById('image_url_1');
            let image_url_2 = document.getElementById('image_url_2');
            let image_url_3 = document.getElementById('image_url_3');
            let image_url_4 = document.getElementById('image_url_4');
            let image_div_1 = document.getElementById('image_div_1');
            let image_div_2 = document.getElementById('image_div_2');
            let image_div_3 = document.getElementById('image_div_3');
            let image_div_4 = document.getElementById('image_div_4');
            images_full_div.style.display = "none";

            if (image_count === 1) {image_div_4.style.display = "none";}
            if (image_count === 2) {image_div_3.style.display = "none";image_div_4.style.display = "none";}
            if (image_count === 3) {image_div_2.style.display = "none";image_div_3.style.display = "none";image_div_4.style.display = "none";}
            if (image_count === 4) {
                images_full_div.style.display = "block";
                all_image_inputs_div.style.display = "none";
            }

            function preview(id) {
                document.getElementById(id).style.display = "inline";
                document.getElementById(id).src = URL.createObjectURL(event.target.files[0]);
            }

            function deleteImg() {

            }

            function preview_2() {
                image_preview_frame_2.style.display = "inline";
                image_preview_frame_2.src = URL.createObjectURL(event.target.files[0]);
            }

            function preview_3() {
                image_preview_frame_3.style.display = "inline";
                image_preview_frame_3.src = URL.createObjectURL(event.target.files[0]);
            }

            function preview_4() {
                image_preview_frame_4.style.display = "inline";
                image_preview_frame_4.src = URL.createObjectURL(event.target.files[0]);
            }


            document.getElementById('image_clear_btn').addEventListener('click', function () {
                if (image_preview_frame_1 !== null) {image_preview_frame_1.src = "";}
                if (image_preview_frame_2 !== null) {image_preview_frame_2.src = "";}
                if (image_preview_frame_3 !== null) {image_preview_frame_3.src = "";}
                if (image_preview_frame_4 !== null) {image_preview_frame_4.src = "";}
                if (image_url_1 !== null) {image_url_1.value = '';}
                if (image_url_2 !== null) {image_url_2.value = '';}
                if (image_url_3 !== null) {image_url_3.value = '';}
                if (image_url_4 !== null) {image_url_4.value = '';}
            });
        } catch (error) {
            if (ENVIRONMENT === 'local') {
                console.error(error);
            }
        }

        (function () {
            try {


                let image_delete_btns = document.getElementsByClassName("image_delete_btns");
                let result_span = document.getElementById('result_span');
                let current_count_display = document.getElementById('current_count_display');

                for (var i = 0; i < image_delete_btns.length; i++) {
                    image_delete_btns[i].addEventListener('click', function () {
                        //console.log(this.dataset.image_id);
                        fetch('{{ route('member.image_delete') }}', {
                            headers: {"Content-Type": "application/json; charset=utf-8"},
                            method: 'POST',
                            body: JSON.stringify({
                                _token: @json(csrf_token()),
                                image_id: this.dataset.image_id,
                                image_model: "App\\Models\\Post",
                                image_model_id: this.dataset.post_id,
                            })
                        }).then(response => response.json())
                            .then(data => {
                                console.log(data);
                                if (data.success === 'Image Deleted') {

                                    current_count_display.innerHTML = data.image_count;
                                    document.getElementById('image_preview_div_' + this.dataset.image_id).style.display = "none";
                                    if (data.image_count === 1) {
                                        image_div_1.style.display = "block";
                                        image_div_2.style.display = "block";
                                        image_div_3.style.display = "block";
                                        image_div_4.style.display = "none";
                                        all_image_inputs_div.style.display = "block";
                                        images_full_div.style.display = "none";
                                    }
                                    if (data.image_count === 2) {
                                        image_div_1.style.display = "block";
                                        image_div_2.style.display = "block";
                                        image_div_3.style.display = "none";
                                        image_div_4.style.display = "none";
                                        all_image_inputs_div.style.display = "block";
                                        images_full_div.style.display = "none";
                                    }
                                    if (data.image_count === 3) {
                                        image_div_1.style.display = "block";
                                        image_div_2.style.display = "none";
                                        image_div_3.style.display = "none";
                                        image_div_4.style.display = "none";
                                        all_image_inputs_div.style.display = "block";
                                        images_full_div.style.display = "none";
                                    }
                                    if (data.image_count === 4) {
                                        images_full_div.style.display = "block";
                                        all_image_inputs_div.style.display = "none";
                                    }
                                    if (data.image_count === 0) {
                                        images_full_div.style.display = "none";
                                        image_div_1.style.display = "block";
                                        image_div_2.style.display = "block";
                                        image_div_3.style.display = "block";
                                        image_div_4.style.display = "block";
                                        all_image_inputs_div.style.display = "block";
                                    }
                                } else {
                                    result_span.classList.add('text-danger');
                                    result_span.innerHTML = 'Error In Unfollowing';
                                }
                            })
                    });
                }
                document.getElementById('image_delete_all_btn').addEventListener('click', function () {
                    //console.log(this.dataset.image_id);
                    fetch('{{ route('member.image_delete') }}', {
                        headers: {"Content-Type": "application/json; charset=utf-8"},
                        method: 'POST',
                        body: JSON.stringify({
                            _token: @json(csrf_token()),
                            image_id: this.dataset.image_id,
                            image_model: "App\\Models\\Post",
                            image_model_id: this.dataset.post_id,
                        })
                    }).then(response => response.json())
                        .then(data => {
                            console.log(data);
                            if (data.success === 'Image Deleted') {

                                current_count_display.innerHTML = data.image_count;
                                document.getElementById('image_preview_div_' + this.dataset.image_id).style.display = "none";
                                if (data.image_count === 1) {
                                    image_div_1.style.display = "block";
                                    image_div_2.style.display = "block";
                                    image_div_3.style.display = "block";
                                    image_div_4.style.display = "none";
                                    all_image_inputs_div.style.display = "block";
                                    images_full_div.style.display = "none";
                                }
                                if (data.image_count === 2) {
                                    image_div_1.style.display = "block";
                                    image_div_2.style.display = "block";
                                    image_div_3.style.display = "none";
                                    image_div_4.style.display = "none";
                                    all_image_inputs_div.style.display = "block";
                                    images_full_div.style.display = "none";
                                }
                                if (data.image_count === 3) {
                                    image_div_1.style.display = "block";
                                    image_div_2.style.display = "none";
                                    image_div_3.style.display = "none";
                                    image_div_4.style.display = "none";
                                    all_image_inputs_div.style.display = "block";
                                    images_full_div.style.display = "none";
                                }
                                if (data.image_count === 4) {
                                    images_full_div.style.display = "block";
                                    all_image_inputs_div.style.display = "none";
                                }
                                if (data.image_count === 0) {
                                    images_full_div.style.display = "none";
                                    image_div_1.style.display = "block";
                                    image_div_2.style.display = "block";
                                    image_div_3.style.display = "block";
                                    image_div_4.style.display = "block";
                                    all_image_inputs_div.style.display = "block";
                                }
                            } else {
                                result_span.classList.add('text-danger');
                                result_span.innerHTML = 'Error In Unfollowing';
                            }
                        })
                });


                function onlyOneCheckBox() {
                    let checkboxgroupText = document.getElementById('checkboxgroupText');
                    let checkboxgroup = document.getElementById('checkboxgroup').getElementsByTagName("input");
                    let limit = 3;
                    for (var i = 0; i < checkboxgroup.length; i++) {
                        checkboxgroup[i].onclick = function () {
                            var checkedcount = 0;
                            for (var i = 0; i < checkboxgroup.length; i++) {
                                checkedcount += (checkboxgroup[i].checked) ? 1 : 0;
                            }
                            if (checkedcount > limit) {
                                console.log("You can select maximum of " + limit + " checkbox.");
                                checkboxgroupText.innerText = "You can select maximum of " + limit + " checkbox.";
                                this.checked = false;
                            }
                        }
                    }
                }

                onlyOneCheckBox();


                ClassicEditor.defaultConfig = {
                    toolbar: {
                        items: [
                            'heading',
                            '|',
                            'bold',
                            'italic',
                            '|',
                            'bulletedList',
                            'numberedList',
                            '|',
                            'insertTable',
                            '|',
                            'undo',
                            'redo'
                        ]
                    },
                    image: {
                        toolbar: [
                            'imageStyle:full',
                            'imageStyle:side',
                            '|',
                            'imageTextAlternative'
                        ]
                    },
                    table: {
                        contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells']
                    },
                    language: 'en'
                };


                ClassicEditor
                    .create(document.querySelector('#post_body'))
                    .catch(error => {
                    console.error(error);
                });


            } catch (error) {
                if (ENVIRONMENT === 'local') {
                    console.error(error);
                }
            }
        })();



    </script>
@endpush
