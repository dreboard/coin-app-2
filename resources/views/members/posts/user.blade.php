@extends('layouts.user.main')
@section('pageTitle', 'Articles by user')
@section('content')


    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Posts by {{ $user->name }}</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    My Posts
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('member.user_posts', ['id' => auth()->user()->id]) }}">My Posts</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('member.unpublished_posts') }}">My Unpublished</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.posts_index') }}">All Posts</a></li>
        <li class="breadcrumb-item"><a href="{{ route('member.user_posts', ['id' => auth()->user()->id]) }}">My Posts</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8 posts_div  mb-4">
            @if($user->id !== auth()->user()->id)
                <div class="mb-3">
                    @if(auth()->user()->isFollowing($user))
                        <button type="button" class="btn btn-success" id="follow_model_btn">
                            Following
                        </button>
                    @else
                        <button type="button" class="btn btn-primary" id="follow_model_btn">
                            Follow
                        </button>
                    @endif
                </div>
            @endif

            <!-- featured posts -->
                @if($featured)
                    <div class="row">
                        <div class="col-sm-3">
                            @if($featured->images[0]->image_url == 'https://via.placeholder.com/150')
                                <img class="card-img-top" src="https://via.placeholder.com/150" />
                            @else
                                <figure class="mb-4">
                                    <a href="{{ route('member.view_post', ['post' => $featured->id]) }}">
                                        <img class="card-img-top post_image" src="{{asset('storage/'.$featured->images[0]->image_url)}}" alt="" />
                                    </a>
                                </figure>
                            @endif
                        </div>
                        <div class="col-sm-9">
                            <div class="card-body">
                                <div
                                    class="small text-muted">{{ Carbon\Carbon::parse($featured->created_at)->diffForHumans() }}</div>
                                <h2 class="card-title">{{ Str::limit($featured->title, 40) }}</h2>
                                <h3 class="card-title">Featured Post</h3>
                                <p class="card-text">{!! Str::limit($featured->body, 50) !!}</p>
                                <a class="btn btn-primary" href="{{ route('member.view_post', ['post' => $featured->id]) }}">Read more →</a>
                            </div>
                        </div>
                    </div>
                @endif
            <!-- start posts -->
            @if(!empty($posts) || count($posts) > 0)
                @foreach($posts as $post)
                    @if($post->featured == 0)
                        <div
                            class="card row-hover pos-relative py-3 px-3 mb-3 border-primary border-top-0 border-right-0 border-bottom-0 rounded-0">
                            <div class="row align-items-center">
                                <div class="col-md-8 mb-3 mb-sm-0">
                                    <h5>
                                        <a href="{{ route('member.view_post', ['post' => $post->id]) }}"
                                           class="text-primary">{{ $post->title }}</a>
                                    </h5>
                                    <p class="text-sm"><span class="op-6">Posted</span> <a class="text-black"
                                                                                           href="{{ route('member.view_post', ['post' => $post->id]) }}">{{ Carbon\Carbon::parse($post->created_at)->diffForHumans() }}</a>
                                        <span class="op-6">ago</span>
                                    <div class="text-sm op-5">
                                        @foreach($post->tags as $tag)
                                            <a class="text-black mr-2" href="{{ route('member.posts_by_tag', ['tag' => $tag->slug]) }}">#{{$tag->name}}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-md-4 op-7">
                                    <div class="row text-center op-7">
                                        <div class="col px-1"><i class="ion-ios-chatboxes-outline icon-1x"></i> <span
                                                class="d-block text-sm">{{$post->comments->count()}} Replys</span></div>
                                        <div class="col px-1"><i class="ion-ios-eye-outline icon-1x"></i> <span
                                                class="d-block text-sm">{{$post->views}} Views</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach

                {{ $posts->links('pagination::bootstrap-5') }}

            @endif
                @if(empty($posts) || count($posts) < 1)
                <p>No Current Articles by {{ $user->name }}</p>
            @endif
            <!-- End posts -->
                <div class="h-20"></div>
        </div>

        <!-- Side widgets-->
        @include('members.writers.partials.side')

    </div>

@endsection
@push('styles')
    <style>
        .posts_div nav {
            margin-bottom: 12px;
        }
        .post_image {
            width: 100%;
            height: auto
        }
        figure {
            text-align: center;
        }
    </style>
@endpush
@push('scripts')
    <script>
        (function (ENVIRONMENT) {
            try {
                let follow_model_btn = document.getElementById('follow_model_btn');
                follow_model_btn.addEventListener('click', function () {

                    fetch('{{ route('user.user_follow') }}', {
                        headers: {"Content-Type": "application/json; charset=utf-8"},
                        method: 'POST',
                        body: JSON.stringify({
                            _token: @json(csrf_token()),
                            model_type: 'user',
                            model_id: {{ $user->id ?? auth()->user()->id }},
                        })
                    }).then(response => response.json())
                        .then(data => {
                            console.log(data);
                            if (data.result == 'Success') {
                                follow_model_btn.innerHTML = 'Following';
                                follow_model_btn.classList.add('btn-success')
                            } else {
                                follow_model_btn.classList.add('text-danger');
                                follow_model_btn.innerHTML = 'Error';
                            }
                        })
                });
            } catch (error) {
                if (ENVIRONMENT === 'local') {
                    console.error(error);
                }
            }
        })();
    </script>
@endpush
