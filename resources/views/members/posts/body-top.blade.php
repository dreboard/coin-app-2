<div class="row">
    <div class="col-6 float-start">
        <h3 class="mt-4">Articles Area</h3>
    </div>
    <div class="col-6">
        <div class="dropdown mt-4 float-end">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                    data-bs-toggle="dropdown" aria-expanded="false">
                Profile Pages
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                <li><a class="dropdown-item"
                       href="{{ route('member.user_posts', ['id' => auth()->user()->id]) }}">My Posts</a>
                </li>
                <li><a class="dropdown-item"
                       href="{{ route('member.unpublished_posts') }}">My Unpublished</a>
                </li>
            </ul>
        </div>
    </div>
</div>
