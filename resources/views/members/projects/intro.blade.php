@extends('layouts.user.main')
@section('pageTitle', 'Create A Project (Intro)')
@section('content')

    <h3 class="mt-4">Create A Project (Intro)</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.research') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.user_types', ['type' => 'Research']) }}">Researchers
                Directory</a></li>
        <li class="breadcrumb-item"><a href="{{ route('member.user_projects') }}">My Projects</a></li>
    </ol>

    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-6">
                    <div class="alert alert-primary" role="alert">
                        <h4>By Coin</h4>
                        <div class="input-group">
                            <select id="typeList" class="form-select">
                                <option value="{{ route('member.project_coin_create', ['id' => 113]) }}">No Type
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 48]) }}">Liberty Cap Half
                                    Cent
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 47]) }}">Draped Bust Half
                                    Cent
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 46]) }}">Classic Head
                                    Half Cent
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 45]) }}">Braided Hair
                                    Half Cent
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 102]) }}">No Type
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 1]) }}">Flying Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 51]) }}">Flowing Hair
                                    Large Cent
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 90]) }}">Mixed Cents
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 89]) }}">Braided Hair
                                    Liberty Head Large Cent
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 88]) }}">Coronet Head
                                    Cent
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 87]) }}">Classic Head
                                    Large Cent
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 3]) }}">Lincoln
                                    Memorial
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 2]) }}">Lincoln Wheat
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 4]) }}">Union Shield
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 85]) }}">Liberty Cap
                                    Large Cent
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 6]) }}">Lincoln
                                    Bicentennial
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 5]) }}">Indian Head
                                    Cent
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 86]) }}">Draped Bust
                                    Large Cent
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 49]) }}">Two Cent Piece
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 50]) }}">Silver Three
                                    Cent
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 84]) }}">Nickel Three
                                    Cent
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 34]) }}">Westward
                                    Journey
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 35]) }}">Return to
                                    Monticello
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 28]) }}">Flowing Hair
                                    Half Dime
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 27]) }}">Draped Bust Half
                                    Dime
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 26]) }}">Liberty Cap Half
                                    Dime
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 25]) }}">Seated Liberty
                                    Half Dime
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 22]) }}">Indian Head
                                    Nickel
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 21]) }}">Liberty Head
                                    Nickel
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 20]) }}">Shield Nickel
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 23]) }}">Jefferson
                                    Nickel
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 103]) }}">Mixed Nickels
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 43]) }}">Liberty Cap
                                    Dime
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 104]) }}">Mixed Dimes
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 8]) }}">Seated Liberty
                                    Dime
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 7]) }}">Draped Bust
                                    Dime
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 9]) }}">Barber Dime
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 10]) }}">Winged Liberty
                                    Dime
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 11]) }}">Roosevelt Dime
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 44]) }}">Twenty Cent
                                    Piece
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 14]) }}">Barber Quarter
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 131]) }}">American
                                    Women
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 13]) }}">Seated Liberty
                                    Quarter
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 12]) }}">Draped Bust
                                    Quarter
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 105]) }}">Mixed
                                    Quarters
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 15]) }}">Standing
                                    Liberty
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 16]) }}">Washington
                                    Quarter
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 41]) }}">Capped Bust
                                    Quarter
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 42]) }}">America the
                                    Beautiful Quarter
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 111]) }}">Liberty Cap
                                    Quarter
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 114]) }}">Commemorative
                                    Quarter
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 130]) }}">Crossing the
                                    Delaware
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 18]) }}">District of
                                    Columbia and US Territories
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 17]) }}">State Quarter
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 106]) }}">Mixed Half
                                    Dollars
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 52]) }}">Capped Bust Half
                                    Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 60]) }}">Kennedy Half
                                    Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 53]) }}">Flowing Hair
                                    Half Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 38]) }}">Walking
                                    Liberty
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 39]) }}">Barber Half
                                    Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 29]) }}">Commemorative
                                    Half Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 57]) }}">Seated Liberty
                                    Half Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 37]) }}">Franklin Half
                                    Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 54]) }}">Draped Bust Half
                                    Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 33]) }}">Morgan Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 107]) }}">Mixed Dollars
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 40]) }}">Susan B Anthony
                                    Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 62]) }}">Flowing Hair
                                    Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 59]) }}">Seated Liberty
                                    Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 58]) }}">Gobrecht
                                    Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 110]) }}">American
                                    Innovation Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 32]) }}">Trade Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 64]) }}">Peace Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 61]) }}">Draped Bust
                                    Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 63]) }}">Sacagawea
                                    Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 55]) }}">Liberty Head
                                    Gold Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 101]) }}">Silver American
                                    Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 115]) }}">Commemorative
                                    Gold Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 30]) }}">Eisenhower
                                    Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 31]) }}">Presidential
                                    Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 112]) }}">Commemorative
                                    Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 65]) }}">Indian Princess
                                    Gold Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 127]) }}">Coronet Head
                                    Quarter Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 116]) }}">Commemorative
                                    Quarter Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 70]) }}">Indian Head
                                    Quarter Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 66]) }}">Draped Bust
                                    Quarter Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 67]) }}">Capped Bust
                                    Quarter Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 68]) }}">Classic Head
                                    Quarter Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 69]) }}">Liberty Head
                                    Quarter Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 71]) }}">Indian Princess
                                    Three Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 72]) }}">Four Dollar
                                    Stella
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 78]) }}">Indian Head Half
                                    Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 75]) }}">Classic Head
                                    Half Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 73]) }}">Turban Head Half
                                    Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 77]) }}">Tenth Ounce
                                    Gold
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 76]) }}">Liberty Head
                                    Half Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 118]) }}">Commemorative
                                    Five Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 121]) }}">Capped Bust
                                    Half Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 124]) }}">Tenth Ounce
                                    Buffalo
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 128]) }}">Coronet Head
                                    Half Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 74]) }}">Liberty Cap Half
                                    Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 119]) }}">Commemorative
                                    Ten Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 120]) }}">Tenth Ounce
                                    Platinum
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 122]) }}">First Spouse
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 125]) }}">Coronet Head
                                    Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 126]) }}">Liberty Cap
                                    Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 123]) }}">Quarter Ounce
                                    Buffalo
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 79]) }}">Turban Head
                                    Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 82]) }}">Quarter Ounce
                                    Gold
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 81]) }}">Indian Head
                                    Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 80]) }}">Liberty Head
                                    Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 91]) }}">Coronet Head
                                    Double Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 92]) }}">Saint Gaudens
                                    Double Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 93]) }}">Half Ounce
                                    Buffalo
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 94]) }}">Quarter Ounce
                                    Platinum
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 95]) }}">Half Ounce
                                    Gold
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 98]) }}">Half Ounce
                                    Gold
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 109]) }}">American
                                    Palladium Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 97]) }}">One Ounce
                                    Buffalo
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 96]) }}">Half Ounce
                                    Platinum
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 99]) }}">One Ounce Gold
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 108]) }}">Gold American
                                    Eagle
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 117]) }}">Commemorative
                                    Fifty Dollar
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 129]) }}">American
                                    Liberty Union
                                </option>
                                <option value="{{ route('member.project_coin_create', ['id' => 100]) }}">One Ounce
                                    Platinum
                                </option>
                            </select>
                            <button id="typeListBtn" class="btn btn-secondary" type="button">Load</button>
                        </div>
                    </div>
                </div>
                <script>


                </script>
                <div class="col-6">
                    <div class="alert alert-primary" role="alert">
                        A simple primary alert—check it out! <a href="{{ route('member.project_intro') }}"
                                                                class="btn btn-sm btn-primary" id="button-search"
                                                                type="button">Go!</a>
                    </div>
                </div>
            </div>


            <div class="alert alert-primary" role="alert">
                A simple primary alert—check it out! <a href="{{ route('member.project_intro') }}"
                                                        class="btn btn-sm btn-primary" id="button-search"
                                                        type="button">Go!</a>
            </div>


        </div>

        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">About Projects</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
                    Group Type
                    <p class="card-text">All users are allowed to create one group.</p>
                    <a href="#" class="card-link" data-bs-toggle="modal" data-bs-target="#GroupTypeModal">Group Type</a>
                    <a href="#" class="card-link" data-bs-toggle="modal" data-bs-target="#exampleModal" 2>Specialty</a>
                </div>
            </div>
        </div>


    </div>

@endsection
@push('styles')
    <style>

        .fixed_header {
            width: 400px;
            table-layout: fixed;
            border-collapse: collapse;
        }

        .fixed_header tbody {
            display: block;
            width: 100%;
            overflow: auto;
            height: 250px;
        }

        .fixed_header thead tr {
            display: block;
        }

        .fixed_header thead {
            background: black;
            color: #fff;
        }

        .fixed_header th,
        .fixed_header td {
            padding: 5px;
            text-align: left;
            width: 200px;
        }
    </style>
@endpush
@push('scripts')
    <script>
        //  onchange="window.open(this.options[this.selectedIndex].value,'_top')"
        (function () {
            try {
                let typeList = document.getElementById("typeList");
                document.getElementById("typeListBtn").onclick = function(){
                    window.open(typeList.options[typeList.selectedIndex].value,'_top');
                };
            } catch (error) {
                if (ENVIRONMENT === 'local') {
                    console.error(error);
                }
            }
        })();
    </script>
@endpush
