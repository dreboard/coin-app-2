@extends('layouts.user.main')
@section('pageTitle', 'Create A Project')
@section('content')

    <h3 class="mt-4">Create A Project</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.research') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.user_types', ['type' => 'Research']) }}">Researchers Directory</a></li>
        <li class="breadcrumb-item"><a href="{{ route('member.user_projects') }}">My Projects</a></li>
    </ol>

        <div class="row">
            <div class="col-md-8">
                <form class="formatForm" action="{{ route('member.project_save') }}" method="post" id="projectForm" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                    <div class="form-group mb-3">
                        <label for="title" class="control-label">Project Name</label>
                        <input type="text" class="form-control" name="title" placeholder="Project Title"
                               value="{{ old('title') }}">
                    </div>

                    <div class="form-group mb-3">
                        <label for="coin_id" class="control-label">Select Coin</label>
                        <select name="coin_id" class="form-select" size="3" aria-label="size 3 select example">
                            @foreach($coins as $coin)
                                <option selected value="{{$coin->id}}">{{$coin->coinName}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group mb-3">
                        <label for="coin_variety" class="control-label">Variety</label>
                        <select name="coin_variety" id="coin_variety" class="form-select" size="3" aria-label="size 3 select example">
                            <optgroup label="Mint Marks">
                                <option selected value="RPM">Repunched Mint Mark (RPM)</option>
                                <option selected value="OMM">Over Mint Mark (OMM)</option>
                                <option selected value="DMM">Dual Mint Mark (DMM)</option>
                                <option selected value="IMM">Inverted Mint Mark (IMM)</option>
                                <option selected value="MMP">MintMark Placement (MMP)</option>
                                <option selected value="IMM">MintMark Style (MMS)</option>
                            </optgroup>
                            <optgroup label="Double Die">
                                <option selected value="DDO">Doubled Die Obverse (DDO)</option>
                                <option selected value="DDR">Doubled Die Reverse (DDR)</option>
                                <option selected value="MDO">Master Die Doubled Obverse (MDO)</option>
                                <option selected value="MDR">Master Die Doubled Reverse (MDR)</option>
                                <option selected value="SDO">Series Hub Doubled Obverse (SDO)</option>
                                <option selected value="SDR">Series Hub Doubled Reverse (SDR)</option>
                                <option selected value="WHO">Working Hub Doubled Obverse (WHO)</option>
                                <option selected value="WHR">Working Hub Doubled Reverse (WHR)</option>
                            </optgroup>
                            <optgroup label="Dates">
                                <option selected value="RPD">RePunched Date (RPD)</option>
                                <option selected value="MPD">Misplaced Date (MPD)</option>
                                <option selected value="OVD">Overdate (OVD)</option>
                            </optgroup>
                            <optgroup label="Design">
                                <option selected value="ODV">Obverse Design Variety (ODV)</option>
                                <option selected value="RPL">Reverse Design Variety (RDV)</option>
                                <option selected value="TRD">Transitional Reverse Design (TRD)</option>
                                <option selected value="RED">Re-Engraved Design (RED)</option>
                            </optgroup>
                            <optgroup label="Other">
                                <option selected value="RPL">Repunched Letter (RPL)</option>
                                <option selected value="ROT">Rotated Die (ROT)</option>
                            </optgroup>
                            <option selected value="RPD">186ickel</option>
                            <option selected value="DDO">186Nickel</option>
                        </select>
                    </div>


                    <div class="form-group mb-3">
                        <label for="coin_error" class="control-label">Variety</label>
                        <select name="coin_error" id="coin_error" class="form-select" size="3" aria-label="size 3 select example">
                            <optgroup label="MintMarks">
                                <option selected value="RPM">Repunched Mint Mark (RPM)</option>
                                <option selected value="OMM">Over Mint Mark (OMM)</option>

                            </optgroup>
                            <optgroup label="DoubleDie">
                                <option selected value="DDO">Doubled Die Obverse (DDO)</option>
                                <option selected value="DDR">Doubled Die Reverse (DDR)</option>
                                <option selected value="MDO">Master Die Doubled Obverse (MDO)</option>

                            </optgroup>
                            <optgroup label="Dates">
                                <option selected value="RPD">RePunched Date (RPD)</option>
                                <option selected value="MPD">Misplaced Date (MPD)</option>
                                <option selected value="OVD">Overdate (OVD)</option>
                            </optgroup>
                            <optgroup label="Striking Errors">
                                <option selected value="ODV">Unstruck Blank Type I</option>
                                <option selected value="ODV">Unstruck Planchet Type II</option>
                                <option selected value="ODV">Rotated die</option>
                                <option selected value="ODV">Pivoted die</option>
                                <option selected value="ODV">Obveari</option>
                                <option selected value="ODV">Obveari</option>
                                <option selected value="ODV">Obveari</option>
                                <option selected value="ODV">Obveari</option>
                                <option selected value="ODV">Obveari</option>
                                <option selected value="ODV">Obveari</option>
                                <option selected value="ODV">Obveari</option>
                                <option selected value="ODV">Obveari</option>
                                <option selected value="ODV">Obveari</option>
                                <option selected value="ODV">Obveari</option>
                                <option selected value="ODV">Obveari</option>
                                <option selected value="ODV">Obveari</option>
                                <option selected value="ODV">Obveari</option>
                                <option selected value="ODV">Obveari</option>
                                <option selected value="ODV">Obveari</option>
                                <option selected value="ODV">Obveari</option>

                            </optgroup>
                            <optgroup label="Other">
                                <option selected value="RPL">Repunched Letter (RPL)</option>
                                <option selected value="ROT">Rotated Die (ROT)</option>
                            </optgroup>
                            <option selected value="RPD">186ickel</option>
                            <option selected value="DDO">186Nickel</option>
                        </select>
                    </div>

                    <div class="form-group mb-3">
                        <label for="private" class="control-label">Privacy</label>
                        <select name="private" id="private" class="form-select" aria-label="size 3 select example">
                            <option @if(old('private') == 0) selected @endif value="0">Private</option>
                            <option @if(old('private') == 1) selected @endif value="1">Open (Listed in Projects)</option>
                        </select>
                    </div>

                    <div class="form-group mb-3">
                        <label for="overview" class="control-label">Type</label>
                        <select name="type" class="form-select" size="2" aria-label="size 3 select example">
                            <option @if(old('type') == "Research") selected @endif value="Research">Research</option>
                            <option @if(old('type') == "Error") selected @endif value="Error">Error</option>
                            <option @if(old('type') == "Variety") selected @endif value="Variety">Variety</option>
                        </select>
                    </div>

                    <div class="form-group mb-3">
                        <label for="overview" class="control-label">Overview</label>
                        <textarea name="overview" id="project_overview" class="form-control" placeholder="Detailed Description">{{ old('overview') }}</textarea>
                    </div>

                    <div class="form-group mb-3">
                        <label for="description" class="control-label">Description</label>
                        <textarea name="description" id="project_description" class="form-control" placeholder="Detailed Description">{{ old('description') }}</textarea>
                    </div>

                    <div class="form-group mt-3">
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </form>
            </div>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">About Projects</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
                        Group Type
                        <p class="card-text">All users are allowed to create one group.</p>
                        <a href="#" class="card-link" data-bs-toggle="modal" data-bs-target="#GroupTypeModal">Group Type</a>
                        <a href="#" class="card-link" data-bs-toggle="modal" data-bs-target="#exampleModal"2>Specialty</a>
                    </div>
                </div>
            </div>

            <div class="mt-5"></div>
            <div class="modal fade" id="GroupTypeModal" tabindex="-1" aria-labelledby="GroupTypeModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="GroupTypeModalLabel">Group Type</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            ...
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel2" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="exampleModalLabel2">Specialty</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            ...
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>



@endsection
@push('styles')
    <style>
        .ck-editor__editable {
            min-height: 400px;
        }

        .fixed_header {
            width: 400px;
            table-layout: fixed;
            border-collapse: collapse;
        }

        .fixed_header tbody {
            display: block;
            width: 100%;
            overflow: auto;
            height: 250px;
        }

        .fixed_header thead tr {
            display: block;
        }

        .fixed_header thead {
            background: black;
            color: #fff;
        }

        .fixed_header th,
        .fixed_header td {
            padding: 5px;
            text-align: left;
            width: 200px;
        }
    </style>
@endpush
@push('scripts')
    <script src="https://cdn.ckeditor.com/ckeditor5/35.2.1/classic/ckeditor.js"></script>
    <script>

        (function () {
            try {
                ClassicEditor.defaultConfig = {
                    toolbar: {
                        items: [
                            'heading',
                            '|',
                            'bold',
                            'italic',
                            '|',
                            'bulletedList',
                            'numberedList',
                            '|',
                            'insertTable',
                            '|',
                            'undo',
                            'redo'
                        ]
                    },
                    image: {
                        toolbar: [
                            'imageStyle:full',
                            'imageStyle:side',
                            '|',
                            'imageTextAlternative'
                        ]
                    },
                    table: {
                        contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells']
                    },
                    language: 'en'
                };


                ClassicEditor
                    .create(document.querySelector('#project_description')).catch(error => {
                    console.error(error);
                });


            } catch (error) {
                if (ENVIRONMENT === 'local') {
                    console.error(error);
                }
            }
        })();
    </script>
@endpush
