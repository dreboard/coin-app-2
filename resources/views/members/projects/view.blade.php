@extends('layouts.user.main')
@section('pageTitle', 'View A Project')
@section('content')


    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Research Area</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Project Pages
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('member.project_view', ['project' => $project->id]) }}">Reports</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('member.project_view', ['project' => $project->id]) }}">Images</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('member.project_view', ['project' => $project->id]) }}">Views/Comments</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>


    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.research') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.user_types', ['type' => 'Research']) }}">Scholar
                Directory</a>
        </li>
        <li class="breadcrumb-item"><a href="{{ route('member.user_projects') }}">My Projects</a></li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div
                class="card row-hover pos-relative py-3 px-3 mb-3 border-primary border-top-0 border-right-0 border-bottom-0 rounded-0">
                <div class="row align-items-center">
                    <div class="col-md-8 mb-3 mb-sm-0">
                        <h5>
                            {{$project->title}}
                        </h5>
                        <p class="text-sm"><span class="op-6">Posted</span>
                            <a class="text-black" href="#">
                                {{ Carbon\Carbon::parse($project->created_at)->diffForHumans() }}
                            </a>
                            By <a class="text-black"
                                  href="{{ route('member.user_posts', ['id' => $project->user->id]) }}">
                                {{ $project->user->name }}
                            </a>
                        </p>
                    </div>
                    <div class="col-md-4 op-7">
                        <div class="row text-center op-7">
                            <div class="col px-1"><span
                                    class="d-block text-sm">{{$project->comments_count}} Replys</span></div>
                            <div class="col px-1"><span class="d-block text-sm">{{$project->views}} Views</span></div>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <figure class="mb-4">
                    <img src="{{asset('storage/'.optional($project)->image_url)}}" width="250" alt=""/>
                </figure>

                <p class="mt-3 mb-3">{!! $project->description !!}</p>
            </div>
            <!-- End posts -->

            <section class="mb-5">
                <div class="card bg-light">
                    <div class="card-body">
                        <!-- Comment form-->
                        @include('layouts.partials.comment_replies', ['comments' => $project->comments, 'post_id' => $project->id])
                        <form class="mb-4" action="{{ route('member.save_project_comment') }}" method="post">
                            @csrf
                            <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                            <input type="hidden" name="project_id" value="{{$project->id }}">
                            <div class="form-group mb-3">
                                <label class="control-label">Comment</label>
                                <textarea id="comment_body" name="comment_body" class="form-control" rows="3"
                                          placeholder="Join the discussion and leave a comment!"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                    </div>
                </div>
            </section>

        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            @if($project->user->id == auth()->user()->id)
                <div class="card mb-4">
                    <div class="card-header">Manage</div>
                    <div class="card-body">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-danger" data-bs-toggle="modal"
                                    data-bs-target="#userMessageModal">Delete
                            </button>
                            <a href="{{ route('member.edit_posts', ['post' => $project->id]) }}"
                               class="btn btn btn-warning">Edit</a>
                            <a href="{{ route('member.edit_posts', ['post' => $project->id]) }}"
                               class="btn btn btn-primary">Users</a>
                        </div>
                    </div>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="userMessageModal" tabindex="-1"
                     aria-labelledby="userMessageModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h1 class="modal-title fs-5" id="userMessageModalLabel">
                                    Contact Manager</h1>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form method="post" id="user_message_form"
                                      action="{{ route('member.project_delete') }}">
                                    @csrf
                                    <input type="hidden" name="project_id" value="{{ $project->id }}">
                                    <div class="col-12">
                                        <button id="user_message_btn" type="submit" class="btn btn-danger">Yes Delete</button>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal end -->

            @endif
            <div class="card mb-4">

                <div class="card-header">My Area</div>
                <div class="card-body">

                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-danger">Join</button>
                        <button type="button" class="btn btn-secondary" data-bs-toggle="modal"
                                data-bs-target="#userMessageModal">Contact
                        </button>
                        <button type="button" class="btn btn-primary" id="follow_model_btn">
                            Follow
                        </button>
                    </div>

                </div>
            </div>

            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Details</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('member.project_view', ['project' => $project->id]) }}">Images</a>
                                </li>
                                <li><a href="{{ route('member.project_view', ['project' => $project->id]) }}">Notes</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('member.project_view', ['project' => $project->id]) }}">Edit</a>
                                </li>
                                <li>
                                    <a href="{{ route('member.project_view', ['project' => $project->id]) }}">Groullow</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="https://www.nlgonline.org/">The Numismatic Literary
                                        Guild</a>
                                </li>
                                <li><a class="external_link" href="https://www.coinbooks.org/index.html">Numismatic
                                        Bibliomania
                                        Society</a></li>
                                <li><a class="external_link" href="https://nnp.wustl.edu/">The Newman Numismatic
                                        Portal</a></li>
                                <li><a class="external_link" href="https://donum.numismatics.org/">Database Of
                                        Numismatic
                                        Materials</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    @push('styles')
        <style>
            .display-comment .display-comment {
                margin-left: 40px
            }

            .ck-editor__editable {
                min-height: 250px;
            }
        </style>
    @endpush
    @push('scripts')
        <script src="https://cdn.ckeditor.com/ckeditor5/35.2.1/classic/ckeditor.js"></script>
        <script>
            (function () {
                try {


                    let follow_model_btn = document.getElementById('follow_model_btn');
                    follow_model_btn.addEventListener('click', function () {

                        fetch('{{ route('user.user_follow') }}', {
                            headers: {"Content-Type": "application/json; charset=utf-8"},
                            method: 'POST',
                            body: JSON.stringify({
                                _token: @json(csrf_token()),
                                model_type: 'project',
                                model_id: {{ $project->id }},
                            })
                        }).then(response => response.json())
                            .then(data => {
                                console.log(data);
                                if (data.result == 'Success') {
                                    follow_model_btn.innerHTML = 'Following';
                                    follow_model_btn.classList.add('btn-success')
                                } else {
                                    follow_model_btn.classList.add('text-danger');
                                    follow_model_btn.innerHTML = 'Error';
                                }
                            })
                    });


                    ClassicEditor.defaultConfig = {
                        toolbar: {
                            items: [
                                'heading',
                                '|',
                                'bold',
                                'italic',
                                '|',
                                'bulletedList',
                                'numberedList',
                                '|',
                                'insertTable',
                                '|',
                                'undo',
                                'redo'
                            ]
                        },
                        image: {
                            toolbar: [
                                'imageStyle:full',
                                'imageStyle:side',
                                '|',
                                'imageTextAlternative'
                            ]
                        },
                        table: {
                            contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells']
                        },
                        language: 'en'
                    };


                    ClassicEditor
                        .create(document.querySelector('#comment_body')).catch(error => {
                        console.error(error);
                    });

                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
