@extends('layouts.user.main')

@section('title', 'American Numismatic Association')

@push('styles')
<style>

    div.hs-featured-image-wrapper a img{
        display: none!important;
        max-width:100%!important;
        max-height:95px!important;
        width: auto!important;
        height: auto!important;
    }
</style>

@endpush

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <h2 class="mt-4">Video Feed</h2>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">All News</a> </li>
    </ol>

    <div class="container mt-2">
        <div class="row">
            <div class="col-lg-8">
                <!-- Post content-->




            </div>
            <!-- Side widgets-->
            @include('layouts.news_links')
        </div>
    </div>

    @push('scripts')
        <script>
            (function () {
                try {

                    let elements = document.querySelectorAll('div.hs-featured-image-wrapper a');
                    for (let elem of elements) {
                        console.log(elem.innerHTML); // "test", "passed"hs-featured-image
                    }
                    jQuery(document).ready(function () {
                        jQuery('img.hs-featured-image').css('display', 'none!important');
                        //jQuery('.hs-featured-image').css('max-width', '400px!important').css('height', 'auto!important');
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                        console.log('error with js');
                    }
                }
            })();
        </script>

    @endpush

@endsection
