@extends('layouts.user.main')

@section('title', 'My Dashboard')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')

    <h1 class="mt-4">Coin News Feeds and Videos</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">All News</a> </li>
    </ol>

    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-8">
                <!-- Post content-->





            </div>
            <!-- Side widgets-->
            @include('layouts.news_links')
        </div>
    </div>

@endsection

