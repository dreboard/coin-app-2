@extends('layouts.user.main')
@section('pageTitle', 'My Public Profile')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">{{ $user->name }}</h3>
        </div>
        <div class="col-6">
            @if($user->id == auth()->user()->id)
                <div class="dropdown mt-4 float-end">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                            data-bs-toggle="dropdown" aria-expanded="false">
                        Profile Pages
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <li><a class="dropdown-item"
                               href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">Back To Settings</a>
                        </li>
                        <li><a class="dropdown-item"
                               href="{{ route('user.edit_public', ['user' => auth()->user()->id]) }}">Edit This
                                Profile</a>
                        </li>
                        <li><a class="dropdown-item"
                               href="{{ route('user.user_edit_avatar', ['user' => auth()->user()->id]) }}">Change Image</a>
                        </li>
                    </ul>
                </div>
            @endif
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.directory') }}">User Directory</a></li>
        @if($user->id !== auth()->user()->id)
            <li class="breadcrumb-item"><a href="{{ route('user.public', ['id' => auth()->user()->id]) }}">My
                    Profile</a></li>
        @endif
    </ol>

    <div class="row">
        <div class="col-lg-8">
            <!-- Post content-->
            <article>
                <!-- Post header-->
                <header class="mb-4">
                    <!-- Post meta content-->
                    <div class="text-muted fst-italic mb-2">

                        <img class="text-center" src="{{asset('storage/'.$user->avatar)}}"
                             width="100px" height="auto">


                        Member Since {{ Carbon\Carbon::parse($user->created_at)->format('F Y') }}
                    </div>
                    <!-- Post categories-->
                    <a class="badge bg-primary text-decoration-none link-light"
                       href="#!">{{ ucfirst($user->user_type) }}</a>
                    <a class="badge bg-secondary text-decoration-none link-light"
                       href="#!">{{ ucfirst($user->user_level) }}</a>
                </header>
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <p class="mb-0">ANA Number</p>
                            </div>
                            <div class="col-sm-9">
                                <p class="text-muted mb-0">
                                    {{ $user->ana ?? 'None' }}
                                </p>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <p class="mb-0">eBay Store</p>
                            </div>
                            <div class="col-sm-9">
                                <p class="text-muted mb-0">
                                    {{ $user->ebay_store ?? 'None' }}
                                </p>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <p class="mb-0">eBay Listings</p>
                            </div>
                            <div class="col-sm-9">
                                <p class="text-muted mb-0">
                                    <a target="_blank" href="https://www.ebay.com/usr/liberty.coin">Check out
                                        my other auctions</a>
                                    {{ $user->ebay_id ?? 'None' }}
                                </p>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <p class="mb-0">YouTube</p>
                            </div>
                            <div class="col-sm-9">
                                <p class="text-muted mb-0">
                                    {{ $user->youtube ?? 'None' }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Post content-->
                <section class="mb-5">
                    <div class="fs-5 mb-4">
                        {!! $user->public_message ?? '' !!}
                    </div>
                </section>
            </article>



        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Search</div>
                <div class="card-body">
                    <form method="post" action="{{ route('user.searchForm') }}"
                          class="row row-cols-lg-auto g-3 align-items-center">
                        @csrf
                        <div class="col-12">
                            <label class="visually-hidden" for="usernameSearchTerm">Username</label>
                            <div class="input-group">
                                <input name="searchTerm" type="text" class="form-control" id="usernameSearchTerm"
                                       placeholder="Username">
                            </div>
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
            @if(auth()->user()->id !== $user->id)
                <!-- user widget-->
                <div class="card mb-4">
                    <div class="card-header">User Actions</div>
                    <div class="card-body">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            @if(auth()->user()->isFollowing($user))
                                <button type="button" class="btn btn-success" id="follow_model_btn">
                                    Following
                                </button>
                            @else
                                <button type="button" class="btn btn-primary" id="follow_model_btn">
                                    Follow
                                </button>
                            @endif


                            <button type="button" class="btn btn-secondary" data-bs-toggle="modal"
                                    data-bs-target="#userMessageModal">Contact
                            </button>

                            <button type="button" class="btn btn-warning">Report</button>

                        </div>

                        @if(!empty($invitedGroups))
                            <div class="input-group mb-3 mt-3">
                                <select id="groupInvite" class="form-select">
                                    <option selected>Add to Group</option>
                                    @foreach($invitedGroups as $group)
                                        <option value="{{$group->id}}">{{$group->name}}</option>
                                    @endforeach
                                </select>
                                <button id="groupInviteBtn" type="button" class="btn btn-secondary">Invite</button>
                            </div>
                        @endif

                        @if(!empty($invitedProjects))
                            <div class="input-group mb-3 mt-3">
                                <select id="projectInvite" class="form-select">
                                    <option selected>Add to Project</option>
                                    @foreach($invitedProjects as $project)
                                        <option value="{{$project->id}}">{{$project->title}}</option>
                                    @endforeach
                                </select>
                                <button id="projectInviteBtn" type="button" class="btn btn-secondary">Invite</button>
                            </div>
                        @endif

                        <!-- Modal -->
                        <div class="modal fade" id="userMessageModal" tabindex="-1"
                             aria-labelledby="userMessageModalLabel"
                             aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h1 class="modal-title fs-5" id="userMessageModalLabel">
                                            Contact {{ $user->name }}</h1>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" id="user_message_form"
                                              action="{{ route('user.user_contact') }}">
                                            @csrf
                                            <input type="hidden" name="user_id" value="{{ $user->id }}">
                                            <div class="mb-3">
                                                <label for="user_message_subject" class="form-label">Subject</label>
                                                <input class="form-control" id="user_message_subject" name="subject"
                                                       type="text"/>
                                            </div>
                                            <div class="mb-3">
                                                <label for="user_message_body" class="form-label">Message</label>
                                                <textarea class="form-control" id="user_message_body" name="body"
                                                          rows="3"></textarea>
                                            </div>
                                            <div class="col-12">
                                                <button id="user_message_btn" type="submit" class="btn btn-primary">Send
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <a target="_blank" href="{{ route('view_policy', ['id' => 4]) }}" class="btn btn-warning">Spam Policy</a>
                                            <button href="{{ route('messages.messages') }}" type="button" class="btn btn-primary">My Messages</button>
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal end -->

                    </div>
                </div>
                <!-- user widget end-->
            @endif
            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">My Groups</div>
                <div class="card-body">
                    <div class="row">
                        @if($groups)
                            @foreach($groups->chunk(3) as $chunk)
                                <ul>
                                    @foreach($chunk as $item)
                                        <div class="col-sm-6">
                                            <ul class="list-unstyled mb-0">
                                                <li>
                                                    <a href="{{ route('group.view', ['group' => $item->id]) }}">{{ $item->name }}</a>
                                                </li>
                                            </ul>
                                        </div>
                                    @endforeach
                                </ul>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="h-50"></div>
    </div>

    @push('scripts')
        <script>
            (function (ENVIRONMENT) {
                try {
                    // Invite to group
                    let groupInviteBtn = document.getElementById('groupInviteBtn');
                    let groupInvite = document.getElementById('groupInvite');
                    groupInviteBtn.addEventListener('click', function () {

                        var group_id = groupInvite.value;
                        groupInviteBtn.innerHTML = 'Invite';
                        groupInviteBtn.classList.add('btn-secondary');
                        console.log(group_id);
                        fetch('{{ route('user.group_invite') }}', {
                            headers: {"Content-Type": "application/json; charset=utf-8"},
                            method: 'POST',
                            body: JSON.stringify({
                                _token: @json(csrf_token()),
                                group: group_id,
                                user_id: {{ $user->id }},
                            })
                        }).then(response => response.json())
                            .then(data => {
                                console.log(data);
                                if (data.result == 'Success') {
                                    groupInviteBtn.innerHTML = 'Added';
                                    groupInviteBtn.classList.add('btn-success')
                                } else {
                                    groupInviteBtn.classList.add('text-danger');
                                    groupInviteBtn.innerHTML = 'Error';
                                }
                            })
                    });
                    // Invite to project
                    let projectInvite = document.getElementById('projectInvite');
                    let projectInviteBtn = document.getElementById('projectInviteBtn');
                    projectInviteBtn.addEventListener('click', function () {

                        var project_id = projectInvite.value;
                        projectInviteBtn.innerHTML = 'Invite';
                        projectInviteBtn.classList.add('btn-secondary');
                        console.log(project_id);
                        fetch('{{ route('user.group_invite') }}', {
                            headers: {"Content-Type": "application/json; charset=utf-8"},
                            method: 'POST',
                            body: JSON.stringify({
                                _token: @json(csrf_token()),
                                project: project_id,
                                user_id: {{ $user->id }},
                            })
                        }).then(response => response.json())
                            .then(data => {
                                console.log(data);
                                if (data.result == 'Success') {
                                    projectInviteBtn.innerHTML = 'Added';
                                    projectInviteBtn.classList.add('btn-success')
                                } else {
                                    projectInviteBtn.classList.add('text-danger');
                                    projectInviteBtn.innerHTML = 'Error';
                                }
                            })
                    });

                    // Follow user
                    let follow_model_btn = document.getElementById('follow_model_btn');
                    follow_model_btn.addEventListener('click', function () {

                        fetch('{{ route('user.user_follow') }}', {
                            headers: {"Content-Type": "application/json; charset=utf-8"},
                            method: 'POST',
                            body: JSON.stringify({
                                _token: @json(csrf_token()),
                                model_type: 'user',
                                model_id: {{ $user->id }},
                            })
                        }).then(response => response.json())
                            .then(data => {
                                console.log(data);
                                if (data.result == 'Success') {
                                    follow_model_btn.innerHTML = 'Following';
                                    follow_model_btn.classList.add('btn-success')
                                } else {
                                    follow_model_btn.classList.add('text-danger');
                                    follow_model_btn.innerHTML = 'Error';
                                }
                            })
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection

