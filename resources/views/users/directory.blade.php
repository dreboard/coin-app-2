@extends('layouts.user.main')
@section('pageTitle', 'User Directory')
@section('content')

    <h3 class="mt-4">User Directory</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('group.index') }}">Dashboard</a> </li>
        <li class="breadcrumb-item"><a href="{{ route('group.mine') }}">My Groups</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.groups_i_follow') }}">Groups Followed</a></li>
    </ol>


    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <form method="post" action="{{ route('user.searchForm') }}" class="row row-cols-lg-auto g-3 align-items-center">
                @csrf
                <div class="col-12">
                    <label class="visually-hidden" for="groupUsernameSearchTerm">Username</label>
                    <div class="input-group">
                        <div class="input-group-text">@</div>
                        <input name="searchTerm" type="text" class="form-control" id="groupUsernameSearchTerm" placeholder="Username">
                    </div>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                </div>
            </form>


            <div class="btn-toolbar mt-2 mb-5">
                <div class="btn-group btn-group-lg mb-5">
                    <a href="{{ route('user.search', ['param' => 'A']) }}" class="btn btn-secondary">A</a>
                    <a href="{{ route('user.search', ['param' => 'B']) }}" class="btn btn-secondary">B</a>
                    <a href="{{ route('user.search', ['param' => 'C']) }}" class="btn btn-secondary">C</a>
                    <a href="{{ route('user.search', ['param' => 'D']) }}" class="btn btn-secondary">D</a>
                    <a href="{{ route('user.search', ['param' => 'E']) }}" class="btn btn-secondary">E</a>
                    <a href="{{ route('user.search', ['param' => 'F']) }}" class="btn btn-secondary">F</a>
                    <a href="{{ route('user.search', ['param' => 'G']) }}" class="btn btn-secondary">G</a>
                    <a href="{{ route('user.search', ['param' => 'H']) }}" class="btn btn-secondary">H</a>
                    <a href="{{ route('user.search', ['param' => 'I']) }}" class="btn btn-secondary">I</a>
                    <a href="{{ route('user.search', ['param' => 'J']) }}" class="btn btn-secondary">J</a>
                    <a href="{{ route('user.search', ['param' => 'K']) }}" class="btn btn-secondary">K</a>
                    <a href="{{ route('user.search', ['param' => 'L']) }}" class="btn btn-secondary">L</a>
                    <a href="{{ route('user.search', ['param' => 'M']) }}" class="btn btn-secondary">M</a>
                </div>
                <div class="btn-group btn-group-lg">
                    <a href="{{ route('user.search', ['param' => 'N']) }}" class="btn btn-secondary">N</a>
                    <a href="{{ route('user.search', ['param' => 'O']) }}" class="btn btn-secondary">O</a>
                    <a href="{{ route('user.search', ['param' => 'P']) }}" class="btn btn-secondary">P</a>
                    <a href="{{ route('user.search', ['param' => 'Q']) }}" class="btn btn-secondary">Q</a>
                    <a href="{{ route('user.search', ['param' => 'R']) }}" class="btn btn-secondary">R</a>
                    <a href="{{ route('user.search', ['param' => 'S']) }}" class="btn btn-secondary">S</a>
                    <a href="{{ route('user.search', ['param' => 'T']) }}" class="btn btn-secondary">T</a>
                    <a href="{{ route('user.search', ['param' => 'U']) }}" class="btn btn-secondary">U</a>
                    <a href="{{ route('user.search', ['param' => 'V']) }}" class="btn btn-secondary">V</a>
                    <a href="{{ route('user.search', ['param' => 'W']) }}" class="btn btn-secondary">W</a>
                    <a href="{{ route('user.search', ['param' => 'X']) }}" class="btn btn-secondary">X</a>
                    <a href="{{ route('user.search', ['param' => 'Y']) }}" class="btn btn-secondary">Y</a>
                    <a href="{{ route('user.search', ['param' => 'Z']) }}" class="btn btn-secondary">Z</a>
                </div>
            </div>

        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->

            <div class="card mb-4">
                <div class="card-header">Your Profile</div>
                <div class="card-body">
                    <div class="input-group">
                        <a href="{{ route('user.public', ['id' => auth()->user()->id]) }}" class="btn btn-primary"
                           id="button-search" type="button">My Profile</a>
                    </div>
                </div>
            </div>
            <!-- Categories widget-->
            @include('groups.group.partials.group_section_side')
        </div>
    </div>
@endsection
