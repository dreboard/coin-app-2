@extends('layouts.user.main')
@section('pageTitle', 'Edit User Profile Image')
@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Edit User Profile Image</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Profile Pages
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.public', ['id' => auth()->user()->id]) }}">Public</a></li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.public', ['id' => auth()->user()->id]) }}">My Profile</a></li>
                </ul>
            </div>
        </div>
    </div>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
    </ol>

        <div class="row mb-5">

            <div class="col-md-4">
                <div class="card" style="width: 18rem;">
                    <img id="avatar" class="card-img-top" src="{{asset('storage/'.auth()->user()->avatar)}}" width="100" alt="" />
                    <div class="card-body">
                        <h5 class="card-title">Current</h5>
                        <a href="{{ route('user.public', ['id' => auth()->user()->id]) }}" class="btn btn-primary">Back To Profile</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">

                <div class="mb-3">
                    <label for="img_upload" class="form-label">Choose An Image</label>
                    <input id="img_upload" class="image form-control" type="file" name="image">
                </div>
                <div class="mb-3">
                    <span id="img_upload_success" class="text-success"></span>
                    <span id="img_upload_errors" class="text-danger"></span>
                </div>
                {{--Start Modal--}}
                <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalLabel">Crop Image Before Upload</h5>
                                <button type="button" class="close imageModalClose" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="img-container">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                                        </div>
                                        <div class="col-md-4">
                                            <div class="preview"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button id="imageModalCloseBtn" type="button" class="btn btn-secondary imageModalClose" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-primary" id="crop">Crop</button>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End Modal--}}
            </div>
            <div class="col-md-4">

                <h4>Image Guidelines</h4>
                <ul>
                    <li>PNG, GIF and JPG files are all acceptable.</li>
                    <li>Acceptable file sizes: File sizes are limited by the upload timeout, so we recommend files that are less than 100 MB per file uploaded</li>
                    <li>Image will be cropped to 250x250 pixels</li>
                </ul>
                <h4>Image Upload Policy</h4>
                <p>Do not upload content that is illegal or prohibited OR that contains material that, in our judgment, is pornographic, sexually explicit, obscene or violent in nature.
                    If we find you doing that, your account will be deleted and we'll take appropriate action, which may include reporting you to the authorities.</p>
            </div>
        </div>





    @push('styles')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.13/cropper.css">
        <style>
            img {
                display: block;
                max-width: 100%;
            }
            .preview {
                overflow: hidden;
                width: 160px;
                height: 160px;
                margin: 10px;
                border: 1px solid red;
            }
            .modal-lg{
                max-width: 1000px !important;
            }
        </style>

    @endpush
    @push('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.13/cropper.min.js"></script>
        <script>
            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });

            var jQuerymodal = jQuery('#modal');
            var image = document.getElementById('image');
            let avatar = document.getElementById('avatar');
            var cropper;
            var imageModalClose = document.getElementById('imageModalClose');
            jQuery('.imageModalClose').click(function() {
                jQuerymodal.modal('hide');
                document.getElementById('img_upload').value = null;
            });

            jQuery('#img_upload').click(function() {
                document.getElementById('img_upload_errors').innerText = '';
                document.getElementById('img_upload_success').innerText = '';
            });



            jQuery("body").on("change", ".image", function(e){
                var files = e.target.files;
                var done = function (url) {
                    image.src = url;
                    jQuerymodal.modal('show');
                };
                var reader;
                var file;
                var url;

                if (files && files.length > 0) {
                    file = files[0];

                    if (URL) {
                        done(URL.createObjectURL(file));
                    } else if (FileReader) {
                        reader = new FileReader();
                        reader.onload = function (e) {
                            done(reader.result);
                        };
                        reader.readAsDataURL(file);
                    }
                }
            });

            jQuerymodal.on('shown.bs.modal', function () {
                cropper = new Cropper(image, {
                    aspectRatio: 1,
                    viewMode: 3,
                    preview: '.preview'
                });
            }).on('hidden.bs.modal', function () {
                cropper.destroy();
                cropper = null;
            });

            jQuery("#crop").click(function(){
                canvas = cropper.getCroppedCanvas({
                    width: 250,
                    height: 250,
                });

                canvas.toBlob(function(blob) {
                    url = URL.createObjectURL(blob);
                    var reader = new FileReader();
                    reader.readAsDataURL(blob);
                    reader.onloadend = function() {
                        var base64data = reader.result;

                        jQuery.ajax({
                            type: "POST",
                            dataType: "json",
                            enctype: 'multipart/form-data',
                            url: "{{ route('user.user_save_avatar') }}",
                            data: {'_token': jQuery('meta[name="_token"]').attr('content'), 'image': base64data, user_id: '{{ auth()->user()->id }}'},
                            success: function(data){
                                jQuerymodal.modal('hide');
                                jQuery("#avatar").attr("src", data.url);
                                document.getElementById('img_upload').value = null;
                                document.getElementById('img_upload_success').innerText = 'Success, New profile image uploaded';
                                console.log(data);
                            }, // https://laracasts.com/discuss/channels/general-discussion/showing-request-validation-errors-when-submitting-form-by-ajax
                            error: function(data){
                                var errors = data.responseJSON;
                                console.log(errors);
                                // Render the errors with js ...
                            },
                            statusCode: {
                                500: function() {
                                    if (ENVIRONMENT === 'local') {
                                        console.error(error);
                                    }
                                    document.getElementById('img_upload_errors').innerText = 'There has been an error uploading your image';
                                },
                                422: function(data) {
                                    $.each(data.responseJSON, function (key, value) {
                                        document.getElementById('img_upload_errors').innerText = 'There has been an error uploading your image';
                                    });
                                }
                            }
                        });
                    }
                });
            })
        </script>

        <script>
            (function () {
                try {


                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
