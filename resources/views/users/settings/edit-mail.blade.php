@extends('layouts.user.main')
@section('pageTitle', 'Change Email')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')

    <h3 class="mt-4">Change Email</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        <li class="breadcrumb-item"><a href="{{ route('user.user_profile') }}">Profile</a> </li>
    </ol>
    <div class="container">
        <div class="main-body">

            <form method="post" action="{{ route('user.user_save_email') }}">
                @csrf
                <div class="row g-3 align-items-center mb-2">
                    <div class="col-auto">
                        <label for="email" class="col-form-label">Email</label>
                    </div>
                    <div class="col-auto">
                        <input name="email" type="text" id="email" class="form-control" aria-describedby="emailHelpInline">
                    </div>
                    <div class="col-auto">
                        <span id="emailHelpInline" class="form-text">
                          Your new email, 8-20 characters long
                        </span>
                    </div>
                </div>
                <div class="row g-3 align-items-center mb-2">
                    <div class="col-auto">
                        <label for="password" class="col-form-label">Confirm</label>
                    </div>
                    <div class="col-auto">
                        <input name="confirm_email" type="text" id="confirm_email" class="form-control" aria-describedby="confirm_emailHelpInline">
                    </div>
                    <div class="col-auto">
                        <span id="confirm_emailHelpInline" class="form-text">
                          Confirm new Email.
                        </span>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form>

        </div>
    </div>

    @push('scripts')


    @endpush
@endsection
