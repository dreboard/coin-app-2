@extends('layouts.user.main')
@section('pageTitle', 'My Profile Settings')
@section('content')
    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">My Profile Settings</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Public Profile
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.public', ['id' => auth()->user()->id]) }}">View</a></li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.edit_public', ['user' => auth()->user()->id]) }}">Edit</a></li>
                </ul>
            </div>
        </div>
    </div>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
    </ol>
            <div class="row gutters-sm">
                <div class="col-md-4 mb-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex flex-column align-items-center text-center">
                                <img src="{{asset('storage/'.auth()->user()->avatar)}}" width="100" alt="" />
                                <div class="mt-3">
                                    <h4>{{ auth()->user()->name }}</h4>
                                    <p class="text-secondary mb-1">Basic Collector</p>
                                    <a class="btn btn-outline-primary" href="{{ route('user.user_edit_avatar', ['user' => auth()->user()->id]) }}"
                                            @if( auth()->user()->status == 'warn') disabled @endif>Change
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="deleteUserModal" tabindex="-1" aria-labelledby="deleteUserModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="deleteUserModalLabel">
                                        Delete {{ auth()->user()->name }}</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    You are about to delete the account for {{ auth()->user()->name }}, and all content.
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close
                                    </button>
                                    <a class="btn btn-danger"
                                       href="{{ route('user.user_delete_account', ['user_id' => auth()->user()->id])  }}"
                                       role="button">YES</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card mt-3">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                <h6 class="mb-0">Member Since</h6>
                                <span
                                    class="text-secondary">{{ date('D jS M Y, g:ia', strtotime(auth()->user()->created_at)) }}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                <h6 class="mb-0">Last Login</h6>
                                <span class="text-secondary">{{ $last_login ?? 'No Info' }}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                <h6 class="mb-0 @if( auth()->user()->status == 'warn') text-danger @endif">Messages</h6>
                                <span class="text-secondary">88: <a
                                        href="{{ route('user.message_all') }}">View</a></span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                <h6 class="mb-0">MemberSince</h6>
                                <span class="text-secondary">httcom</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                <h6 class="mb-0">MemberSince</h6>
                                <span class="text-secondary">httcom</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{ auth()->user()->name }}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    <a class="btn btn-info @if( auth()->user()->status == 'warn') invisible @endif"
                                       href=" {{ route('user.user_change_password') }} ">Change Username</a>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{ auth()->user()->email }}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    <a class="btn btn-info @if( auth()->user()->status == 'warn') invisible @endif"
                                       href=" {{ route('user.user_change_email') }} ">Change Email</a>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">Visibility</h6>
                                </div>
                                <div class="col-sm-9 text-secondary" id="profile_visibility"
                                     data-visibility="{{ auth()->user()->profile_visibility }}">
                                    <!-- Checked switch -->
                                    <div class="form-check form-switch" id="profile_visibility_on">
                                        <input
                                            class="form-check-input @if( auth()->user()->status == 'warn') invisible @endif"
                                            type="checkbox" role="switch" id="profile_visibility_check"
                                            value="{{ auth()->user()->profile_visibility }}"
                                            {{ (auth()->user()->profile_visibility == 1 ? 'checked' : '')}}
                                        />
                                        <label
                                            class="form-check-label {{ (auth()->user()->profile_visibility == 1 ? 'text-success' : 'text-danger')}}"
                                            for="profile_visibility_check" id="profile_visibility_label">
                                            {{ (auth()->user()->profile_visibility == 1 ? 'Public' : 'Private')}}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">Security</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    <a class="btn btn-info @if( auth()->user()->status == 'warn') invisible @endif"
                                       href=" {{ route('user.user_change_password') }} ">Change Password</a>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">Account Details</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    <a class="btn btn-info @if( auth()->user()->status == 'warn') invisible @endif"
                                       href="{{ route('user.edit_public', ['user' => auth()->user()->id]) }}">Edit</a>
                                </div>
                            </div>

                            <hr>
                            <div class="row">
                                <div class="col-sm-12">
                                    <button id="delete_user" class="btn btn-danger" data-bs-toggle="modal"
                                            data-bs-target="#deleteUserModal">Delete Account
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                    </div>
                </div>
            </div>


    @push('scripts')
        <script>
            (function () {
                try {
                    // profile_visibility_on
                    const pro_vis = document.getElementById("profile_visibility_check").value;
                    const profile_visibility_label = document.getElementById("profile_visibility_label");
                    document.getElementById("profile_visibility_check").addEventListener('click', getData);
                    if (typeof pro_vis !== 'undefined') {
                        console.log(pro_vis);
                    } else {
                        console.log('nope');
                    }
                    console.log(SITE_URL);
                    console.log(ENVIRONMENT);
                    console.log(CSRF_TOKEN);
                    console.log(USER_ID);

                    function getData() {
                        fetch(SITE_URL + '/user/user_change_visibility', {
                            headers: {"Content-Type": "application/json; charset=utf-8"},
                            method: 'POST',
                            body: JSON.stringify({
                                _token: @json(csrf_token()),
                                id: {{ auth()->user()->id }},
                                profile_visibility: document.getElementById("profile_visibility_check").value
                            })
                        }).then(response => response.json())
                            .then(data => {
                                document.getElementById("profile_visibility_check").value = data;
                                if (data == 1) {
                                    profile_visibility_label.innerHTML = 'Public';
                                    profile_visibility_label.classList.add('text-success');
                                    profile_visibility_label.classList.remove('text-danger');
                                } else {
                                    profile_visibility_label.innerHTML = 'Private';
                                    profile_visibility_label.classList.add('text-danger');
                                    profile_visibility_label.classList.remove('text-success');
                                }
                                console.log('Success:', data);
                                console.log(profile_visibility_label.innerHTML);
                            })
                        //.then(data => console.log(data))
                    }
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
