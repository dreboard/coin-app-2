@extends('layouts.user.main')
@section('pageTitle', 'My Area')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">My Area</h3>
        </div>
        <div class="col-6">
                <div class="dropdown mt-4 float-end">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                            data-bs-toggle="dropdown" aria-expanded="false">
                        Profile Pages
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <li><a class="dropdown-item"
                               href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">Back To Settings</a>
                        </li>
                        <li><a class="dropdown-item"
                               href="{{ route('user.edit_public', ['user' => auth()->user()->id]) }}">Edit This
                                Profile</a>
                        </li>
                        <li><a class="dropdown-item"
                               href="{{ route('user.user_edit_avatar', ['user' => auth()->user()->id]) }}">Change Image</a>
                        </li>
                    </ul>
                </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.directory') }}">User Directory</a></li>
            <li class="breadcrumb-item"><a href="{{ route('user.public', ['id' => auth()->user()->id]) }}">My
                    Profile</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            @if($messageCount > -1)
                <div class="alert alert-warning mb-2" role="alert">
                    <a href="{{ route('user.message_all') }}" class="list-group-item list-group-item-action">You Have
                        Messages <span class="badge bg-warning rounded-pill">{{$messageCount}}</span></a>
                </div>
            @endif
            <!-- Featured blog post-->
                @if($featured)
                    <div class="row">
                        <div class="col-sm-3">
                            @if($featured->images[0]->image_url == 'https://via.placeholder.com/150')
                                <a href="#!"><img class="card-img-top" src="https://via.placeholder.com/150"
                                                  alt="..."/></a>
                            @else
                                <figure class="mb-4">
                                    <a href="{{ route('member.view_post', ['post' => $featured->id]) }}">
                                        <img class="card-img-top post_image" src="{{asset('storage/'.$featured->images[0]->image_url)}}" width="250" alt="" />
                                    </a>
                                </figure>
                            @endif
                        </div>
                        <div class="col-sm-9">
                            <div class="card-body">
                                <div
                                    class="small text-muted">{{ Carbon\Carbon::parse($featured->created_at)->diffForHumans() }}</div>
                                <h2 class="card-title">{{ Str::limit($featured->title, 40) }}</h2>
                                <h3 class="card-title">Featured Post</h3>
                                <p class="card-text">{!! Str::limit($featured->body, 50) !!}</p>
                                <a class="btn btn-primary" href="{{ route('member.view_post', ['post' => $featured->id]) }}">Read more →</a>
                            </div>
                        </div>
                    </div>
                @endif

            <!-- Nested row for non-featured blog posts-->
            <div class="row">
                <div class="col-lg-6">
                    <!-- Blog post-->
                    <div class="card mb-4">
                        <a href="#!"><img class="card-img-top" src="https://dummyimage.com/700x350/dee2e6/6c757d.jpg" alt="..." /></a>
                        <div class="card-body">
                            <div class="small text-muted">January 1, 2022</div>
                            <h2 class="card-title h4">Post Title</h2>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla.</p>
                            <a class="btn btn-primary" href="#!">Read more →</a>
                        </div>
                    </div>
                    <!-- Blog post-->
                    <div class="card mb-4">
                        <a href="#!"><img class="card-img-top" src="https://dummyimage.com/700x350/dee2e6/6c757d.jpg" alt="..." /></a>
                        <div class="card-body">
                            <div class="small text-muted">January 1, 2022</div>
                            <h2 class="card-title h4">Post Title</h2>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla.</p>
                            <a class="btn btn-primary" href="#!">Read more →</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <!-- Blog post-->
                    <div class="card mb-4">
                        <a href="#!"><img class="card-img-top" src="https://dummyimage.com/700x350/dee2e6/6c757d.jpg" alt="..." /></a>
                        <div class="card-body">
                            <div class="small text-muted">January 1, 2022</div>
                            <h2 class="card-title h4">Post Title</h2>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla.</p>
                            <a class="btn btn-primary" href="#!">Read more →</a>
                        </div>
                    </div>
                    <!-- Blog post-->
                    <div class="card mb-4">
                        <a href="#!"><img class="card-img-top" src="https://dummyimage.com/700x350/dee2e6/6c757d.jpg" alt="..." /></a>
                        <div class="card-body">
                            <div class="small text-muted">January 1, 2022</div>
                            <h2 class="card-title h4">Post Title</h2>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam.</p>
                            <a class="btn btn-primary" href="#!">Read more →</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Pagination-->
            <nav aria-label="Pagination">
                <hr class="my-0" />
                <ul class="pagination justify-content-center my-4">
                    <li class="page-item disabled"><a class="page-link" href="#" tabindex="-1" aria-disabled="true">Newer</a></li>
                    <li class="page-item active" aria-current="page"><a class="page-link" href="#!">1</a></li>
                    <li class="page-item"><a class="page-link" href="#!">2</a></li>
                    <li class="page-item"><a class="page-link" href="#!">3</a></li>
                    <li class="page-item disabled"><a class="page-link" href="#!">...</a></li>
                    <li class="page-item"><a class="page-link" href="#!">15</a></li>
                    <li class="page-item"><a class="page-link" href="#!">Older</a></li>
                </ul>
            </nav>
        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            @if(is_null($my_group))
                <div class="card mb-4">
                    <div class="card-header">Create A Group</div>
                    <div class="card-body">
                        <div class="input-group">
                            <a href="{{ route('group.start') }}" class="btn btn-primary" id="button-search" type="button">Go!</a>
                        </div>
                    </div>
                </div>
            @else
                <div class="card mb-4">
                    <div class="card-header">Your Group</div>
                    <div class="card-body">
                        <div class="input-group">
                            <a href="{{ route('group.view', ['group' => $my_group->id]) }}" class="btn btn-primary" id="button-search" type="button">{{ $my_group->name }}</a>
                        </div>
                    </div>
                </div>
            @endif
            <!-- Categories widget-->
            @if(optional(auth()->user())->status !== 'warn')
            <div class="card mb-4">

                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.view_directory') }}">User Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

            </div>
            <div class="card mb-4">
                <div class="card-header">My....</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('collected.index') }}">Collection</a></li>
                                <li><a href="{{ route('group.all') }}">Posts</a></li>
                                <li><a href="{{ route('group.all') }}">Images</a></li>
                                <li><a href="{{ route('group.all') }}">Messages</a></li>
                                <li><a href="{{ route('group.all') }}">Comments</a></li>
                                <li><a href="{{ route('group.all') }}">Likes</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.users_i_follow') }}">Discussions</a></li>
                                <li><a href="{{ route('user.groups_i_follow') }}">Forum</a></li>
                                <li><a href="{{ route('group.all') }}">Wishlist</a></li>
                                <li><a href="{{ route('group.all') }}">Dealers</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>

@endsection

