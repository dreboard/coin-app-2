@extends('layouts.user.main')
@section('pageTitle', 'Site Policies')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')

    <h3 class="mt-4">Site Policies</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
    </ol>
    <div class="container">
        <div class="main-body">

            <div class="row">
                <div class="col-4">
                    <h6>Policies</h6>
                    <ul>
                        <li><a href="{{ route('view_policy', ['id' => 1]) }}">Harassment</a></li>
                        <li><a href="{{ route('view_policy', ['id' => 2]) }}">Illegal Activity</a></li>
                        <li><a href="{{ route('view_policy', ['id' => 3]) }}">Terms of Services</a></li>
                        <li><a href="{{ route('view_policy', ['id' => 4]) }}">Anti Spam Policy</a></li>
                        <li><a href="{{ route('view_policy', ['id' => 5]) }}">Prohibited Activities</a></li>
                        <li><a href="{{ route('view_policy', ['id' => 6]) }}">Uploaded Images Policy</a></li>
                    </ul>
                </div>
                <div class="col-8">


                </div>
            </div>







        </div>
    </div>


@endsection

