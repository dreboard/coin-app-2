@extends('layouts.user.main')
@section('pageTitle', 'Violations Reported Against You')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')

    <h3 class="mt-4">Violations Reported Against You</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        <li class="breadcrumb-item"><a href="{{ route('user.message_all') }}">Messages</a> </li>
    </ol>
    <div class="container">
        <div class="main-body">
            @admin


            @endadmin
            <table id="user_datatable" class="table datatable">
                <thead>
                <tr>
                    <th>ID #</th>
                    <th>Type</th>
                    <th>Action</th>
                    <th>Created</th>
                    <th>Link</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID #</th>
                    <th>Type</th>
                    <th>Action</th>
                    <th>Created</th>
                    <th>Link</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach ($violations as $violation)
                    <tr>
                        <td class="text-start">{{ $violation->id }}</td>
                        <td class="text-start">{{$violation->violation_type}}</td>
                        <td class="text-start">{{$violation->action}}</td>
                        <td class="text-start">{{$violation->created_at->format('l F jS Y, g:ia')}}</td>
                        <td class="text-start"><a href="{{ route('user.violation_view', ['id' => $violation->id]) }}">View</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>



        </div>
    </div>


@endsection
