@extends('layouts.user.main')
@section('pageTitle', 'Report User')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')

    <h3 class="mt-4">Report User</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
    </ol>
    <div class="container">
        <div class="main-body">

            <div class="row">
                <div class="col-4">
                    <h6>Policies</h6>
                    <ul>
                        <li><a class="policyLink" href="{{ route('user.dashboard') }}">Harassment</a></li>
                        <li><a class="policyLink" href="{{ route('user.dashboard') }}">Illegal Activity</a></li>
                        <li><a class="policyLink" href="{{ route('user.dashboard') }}">Terms of Services</a></li>
                        <li><a class="policyLink" href="{{ route('user.dashboard') }}">Anti Spam Policy</a></li>
                        <li><a class="policyLink" href="{{ route('user.dashboard') }}">Prohibited Activities</a></li>
                        <li><a class="policyLink" href="{{ route('user.dashboard') }}">Uploaded Images Policy</a></li>
                    </ul>

                    <div class="mt-5" id="policyText"></div>

                </div>
                <div class="col-8">
                    <form action="{{ route('user.violation_save') }}" method="post" id="violation_save_form" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <select class="form-select" aria-label="Violation Type" name="violation_type">
                                <option selected>Violation Type</option>
                                <option value="Harassment">Harassment</option>
                                <option value="Illegal Activity">Illegal Activity</option>
                                <option value="Terms of Services">Terms of Services</option>
                                <option value="Anti Spam Policy">Anti Spam Policy</option>
                                <option value="Prohibited Activities">Prohibited Activities</option>
                                <option value="Uploaded Images Policy">Uploaded Images Policy</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>
                        @if(isset($user))
                            <input type="hidden" value="{{ optional($user)->id }}" name="violator_id">
                            <div class="mb-3">
                                <label for="violator" class="form-label">Username or Email of Violator</label>
                                <input disabled type="text" class="form-control " id="violator" name="violator" placeholder="name@example.com"
                                       value="{{ old('violator', optional($user)->email) }}">
                            </div>
                        @else
                            <div class="mb-3">
                                <label for="violator" class="form-label">Username or Email of Violator</label>
                                <input type="text" class="form-control" id="violator" name="violator" placeholder="name@example.com" value="{{ old('violator') }}">
                            </div>
                        @endif

                        <div class="mb-3">
                            <label for="url" class="form-label">Paste URL of Incident</label>
                            <input type="text" class="form-control" name="url" id="url" placeholder="name@example.com" value="{{ old('url') }}">
                        </div>
                        <div class="mb-3">
                            <label for="details" class="form-label">Details</label>
                            <textarea class="form-control" id="details" name="details" rows="3" value="{{ old('details') }}"></textarea>
                        </div>
                        <div class="mb-3">
                            <label for="screenshot" class="form-label">Screenshot</label>
                            <input class="form-control" type="file" id="screenshot" name="screenshot">
                        </div>
                        <button type="submit" class="btn btn-primary" id="violation_save_form_btn">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            (function (ENVIRONMENT) {
                try {
                    let follow_user_btn = document.getElementById('follow_user_btn');
                    follow_user_btn.addEventListener('click', function () {

                        fetch('{{ route('user.user_follow') }}', {
                            headers: {"Content-Type": "application/json; charset=utf-8"},
                            method: 'POST',
                            body: JSON.stringify({
                                _token: @json(csrf_token()),
                                user_id: {{ $user->id }},
                            })
                        }).then(response => response.json())
                            .then(data => {
                                console.log(data);
                                if (data.result == 'Success') {
                                    follow_user_btn.innerHTML = 'Following';
                                    follow_user_btn.classList.add('btn-success')
                                } else {
                                    follow_user_btn.classList.add('text-danger');
                                    follow_user_btn.innerHTML = 'Error';
                                }
                            })
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection

