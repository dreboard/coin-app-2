@extends('layouts.user.main')
@section('pageTitle', 'View Violation')
@section('content')

    <h3 class="mt-4">View Violation #{{$violation->id}}
        @admin
        <a href="{{ route('admin.reported_type', ['violation_type' => $violation->violation_type]) }}">{{$violation->violation_type}}</a>
        @else
            {{$violation->violation_type}}
            @endadmin
    </h3>
    <ol class="breadcrumb mb-4">
        @admin
        <li class="breadcrumb-item"><a href="{{ route('admin.violation_all') }}">All Violations</a></li>
        @else
            <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('user.violation_reported') }}">My Violations</a></li>
            <li class="breadcrumb-item"><a href="{{ route('user.violation_create') }}">New Violation</a></li>
            @endadmin
    </ol>
    <div class="row">
        <div class="col-8">
            <div class="card mb-4">
                <div class="card-body">
                    <ul>
                        <li><strong>Current Action: </strong>{{$violation->action}}</li>
                        <li><strong>Created: </strong>{{$violation->created_at->format('l F jS Y, g:ia')}}</li>
                        <li><strong>Last
                                Action: </strong>{{$violation->updated_at->format('l F jS Y, g:ia') ?? $violation->created_at->format('l F jS Y, g:ia')}}
                        </li>
                        @admin
                        <li><strong>Reporter: </strong>
                            <a href="{{ route('admin.view_user', ['user_id' => $reporter->id]) }}">{{$reporter->name}}</a>
                        </li>
                        <li><strong>Violator: </strong>
                            @if ( !empty($violator) )
                                <a href="{{ route('admin.view_user', ['user_id' => $violator->id]) }}">{{$violator->name}}</a>
                            @endif
                        </li>
                        @endadmin
                    </ul>
                    @admin
                    <form method="post" action="{{ route('user.violation_update') }}">
                        @csrf
                        <input type="hidden" value="{{$violation->id}}" name="violation_id">
                        <input type="hidden" value="{{$violation->reporter}}" name="reporter">
                        <div class="mb-3">
                            <select name="action" class="form-select" aria-label="Default select example">
                                <option>Choose An Action</option>
                                @foreach (Config::get('constants.violation_actions') as $action)
                                    <option
                                        {{($violation->action == $action) ? 'selected' : ''}} value="{{$action}}">{{ $action }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="violator_id" class="form-label">Culpits</label>
                            <select name="violator_id" id="violator_id" class="form-select"
                                    aria-label="Default select example">
                                <option value="">Choose A User</option>
                                @foreach ($user_list as $user)
                                    <option
                                        {{($violation->violator_id == $user->id) ? 'selected' : ''}} value="{{ $user->id }}">{{ $user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3 form-check">
                            <input type="checkbox" class="form-check-input" name="isMultiple" id="isMultiple" value="1">
                            <label class="form-check-label" for="isMultiple">Has Multiple</label>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    @endadmin
                </div>
            </div>
        </div>
        <div class="col-4">
            <h5>Details</h5>
            <ul>
                <li><strong>URL: </strong>{{ $violation->url }}</li>
                <li><strong>Screenshot: </strong>
                    <figure class="mb-4">
                        <img src="{{asset('storage/'.$violation->screenshot)}}" class="w-50" width="250" alt=""/>
                    </figure>
                </li>
            </ul>
            <p>{{ $violation->details }}</p>
        </div>

        <div class="table-responsive">

            @admin
            <hr/>
            <h4>Culpits</h4>
            <table id="user_datatable" class="table datatable">
                <thead>
                <tr>
                    <th class="fw-bold">ID #</th>
                    <th class="fw-bold">Name</th>
                    <th class="fw-bold">Status</th>
                    <th class="fw-bold">View</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th class="fw-bold">ID #</th>
                    <th class="fw-bold">Name</th>
                    <th class="fw-bold">Status</th>
                    <th class="fw-bold">View</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach ($user_list as $user)
                    <tr>
                        <td class="text-start">{{ $user->id }}</td>
                        <td class="text-start">{{ $user->name }}</td>
                        <td class="text-start">
                            <span class="">{{ $user->banned_at }}</span>
                            @if ($user->banned_at == null)
                                <span class="text-success">Active</span>
                            @else
                                <span
                                    class="text-danger"> Banned {{ $user->banned_at->format('l F jS Y, g:ia') }}</span>
                            @endif
                        </td>
                        <td class="text-start"><a
                                href="{{ route('admin.view_user', ['user_id' => $user->id]) }}">View</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @endadmin
        </div>
    </div>

@endsection
