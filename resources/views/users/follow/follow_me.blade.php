@extends('layouts.user.main')
@section('pageTitle', 'Users Who Follow Me')
@section('content')
    @push('header')
        <h3 class="mt-4">Users Who Follow Me</h3>
    @endpush

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.directory') }}">User Directory</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.users_i_follow') }}">User I Follow</a></li>
    </ol>
    @if(count($users) > 0)

        <div class="btn-group" role="group" aria-label="Basic example">
            <button id="messageUsersBtn" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#userMessageModal">Message</button>
            <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#unfollowModal">Remove All</button>
        </div>

        <span id="result_span"></span>
        <!-- Modal -->
        <div class="modal fade" id="unfollowModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="exampleModalLabel">Unfollow All Users</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="{{ route('user.remove_all') }}" class="row row-cols-lg-auto g-3 align-items-center">
                            @csrf
                            <input type="hidden" name="model_type" value="App\Models\User">
                            <div class="col-12">
                                <button type="submit" class="btn btn-danger">Yes Unfollow</button>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="userMessageModal" tabindex="-1"
             aria-labelledby="userMessageModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="userMessageModalLabel">
                            Contact Followers</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            @csrf
                            <div class="mb-3">
                                <label for="user_message_subject" class="form-label">Subject</label>
                                <input class="form-control" id="user_message_subject" name="subject"
                                       type="text"/>
                            </div>
                            <div class="mb-3">
                                <label for="user_message_body" class="form-label">Message</label>
                                <textarea class="form-control" id="user_message_body" name="body"
                                          rows="3"></textarea>
                            </div>
                            <div class="col-12">
                                <button id="user_message_btn" type="button" class="btn btn-primary">Send
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a target="_blank" href="{{ route('view_policy', ['id' => 4]) }}" class="btn btn-warning">Spam Policy</a>
                            <button href="{{ route('messages.messages') }}" type="button" class="btn btn-primary">My Messages</button>
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="unfollowModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="exampleModalLabel">Unfollow All Users</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="{{ route('user.unfollow_all') }}" class="row row-cols-lg-auto g-3 align-items-center">
                            @csrf
                            <input type="hidden" name="model_type" value="App\Models\User">
                            <div class="col-12">
                                <button type="submit" class="btn btn-danger">Yes Unfollow</button>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-body">

            <div class="table-responsive">
                <table id="user_datatable" class="table datatable table-hover table-striped">
                    <thead>
                    <tr>
                        <th class="fw-bold w-50">Name</th>
                        <th class="fw-bold">Type</th>
                        <th class="fw-bold">Specialty</th>
                        <th class="fw-bold"></th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th class="fw-bold">Name</th>
                        <th class="fw-bold">Type</th>
                        <th class="fw-bold">Specialty</th>
                        <th class="fw-bold text-end"></th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach ($users as $user)
                        <tr id="userRow{{$user->id}}">
                            <td class="text-start"><a href="{{ route('user.public', ['id' => $user->id]) }}">{{ $user->name }}</a></td>
                            <td class="text-start">{{ ucfirst($user->user_type) }}</td>
                            <td class="text-start">{{ $user->specialty }}</td>
                            <td class="text-end">
                                <div class="btn-group" role="group" aria-label="View User">
                                    <a href="{{ route('user.public', ['id' => $user->id]) }}" class="btn btn-primary">View</a>
                                    <button id="user_{{$user->id}}"
                                            type="button"
                                            data-user_id="{{$user->id}}"
                                            data-model_type="App\Models\User"
                                            class="btn btn-danger removeUserBtns">
                                        Remove
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @else
        <p>No users are following you.</p>
    @endif

    @push('scripts')
        <script>
            (function () {
                try {

                    let removeUserBtns = document.getElementsByClassName("removeUserBtns");
                    let result_span = document.getElementById('result_span');

                    for (var i = 0; i < removeUserBtns.length; i++) {
                        removeUserBtns[i].addEventListener('click', function () {
                            console.log(this.dataset.user_id);
                            fetch('{{ route('user.remove_user') }}', {
                                headers: {"Content-Type": "application/json; charset=utf-8"},
                                method: 'POST',
                                body: JSON.stringify({
                                    _token: @json(csrf_token()),
                                    model_type: this.dataset.model_type,
                                    model_id: this.dataset.user_id,
                                })
                            }).then(response => response.json())
                                .then(data => {
                                    console.log(data);
                                    if (data.result == 'Success') {
                                        result_span.innerHTML = 'Removed';
                                        result_span.classList.add('text-success')
                                        document.getElementById('userRow'+this.dataset.user_id).style.display = "none"
                                    } else {
                                        result_span.classList.add('text-danger');
                                        result_span.innerHTML = 'Error In Removing';
                                    }
                                })
                        });
                    }

                    // send messages

                    let user_message_btn = document.getElementById("user_message_btn");
                    let user_message_body = document.getElementById("user_message_body");
                    let user_message_subject = document.getElementById("user_message_subject");

                    user_message_btn.addEventListener('click', function () {
                        fetch('{{ route('user.user_contact_followers') }}', {
                            headers: {"Content-Type": "application/json; charset=utf-8"},
                            method: 'POST',
                            body: JSON.stringify({
                                _token: @json(csrf_token()),
                                subject: user_message_body.value,
                                body: user_message_subject.value,
                                model_id: this.dataset.user_id,
                            })
                        }).then(response => response.json())
                            .then(data => {
                                console.log(data);
                                if (data.result == 'Success') {
                                    result_span.innerHTML = 'Message Sent';
                                    result_span.classList.add('text-success');
                                    user_message_subject.value = '';
                                    user_message_body.value = '';
                                    jQuery('#userMessageModal').modal('hide');
                                } else {
                                    result_span.classList.add('text-danger');
                                    result_span.innerHTML = 'Error Sending Message';
                                }
                            })
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
