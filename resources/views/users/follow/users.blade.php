@extends('layouts.user.main')
@section('pageTitle', 'Users I Follow')
@section('content')
    @push('header')
        <h3 class="mt-4">Users I Follow</h3>
    @endpush

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.directory') }}">User Directory</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.user_who_follow_me') }}">Following Me</a></li>
    </ol>
    @if(count($users) > 0)

        <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#unfollowModal">Unfollow All</button>

        <span id="result_span"></span>
        <!-- Modal -->
        <div class="modal fade" id="unfollowModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="exampleModalLabel">Unfollow All Users</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="{{ route('user.unfollow_all') }}" class="row row-cols-lg-auto g-3 align-items-center">
                            @csrf
                            <input type="hidden" name="model_type" value="App\Models\User">
                            <div class="col-12">
                                <button type="submit" class="btn btn-danger">Yes Unfollow</button>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-body">

            <div class="table-responsive">
                <table id="user_datatable" class="table datatable table-hover table-striped">
                    <thead>
                    <tr>
                        <th class="fw-bold w-50">Name</th>
                        <th class="fw-bold">Type</th>
                        <th class="fw-bold">Specialty</th>
                        <th class="fw-bold"></th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th class="fw-bold">Name</th>
                        <th class="fw-bold">Type</th>
                        <th class="fw-bold">Specialty</th>
                        <th class="fw-bold text-end"></th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach ($users as $user)
                        <tr id="groupRow{{$user->id}}">
                            <td class="text-start"><a href="{{ route('user.public', ['id' => $user->id]) }}">{{ $user->name }}</a></td>
                            <td class="text-start">{{ ucfirst($user->user_type) }}</td>
                            <td class="text-start">{{ $user->specialty }}</td>
                            <td class="text-end">
                                <div class="btn-group" role="group" aria-label="View Group">
                                    <a href="{{ route('user.public', ['id' => $user->id]) }}" class="btn btn-primary">View</a>
                                    <button id="group_{{$user->id}}"
                                            type="button"
                                            data-user_id="{{$user->id}}"
                                            class="btn btn-warning removeGroupBtns">
                                        Unfollow
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @else
        <p>You are not following any groups</p>
    @endif

    @push('scripts')
        <script>
            (function () {
                try {
                    $(document).ready(function () {
                        $('#user_directory_tbl').DataTable({
                            "pageLength": 100
                        });
                    });
                    let removeGroupBtns = document.getElementsByClassName("removeGroupBtns");
                    let result_span = document.getElementById('result_span');

                    for (var i = 0; i < removeGroupBtns.length; i++) {
                        removeGroupBtns[i].addEventListener('click', function () {
                            console.log(this.dataset.group_name,this.dataset.group_id);
                            fetch('{{ route('user.unfollow_model') }}', {
                                headers: {"Content-Type": "application/json; charset=utf-8"},
                                method: 'POST',
                                body: JSON.stringify({
                                    _token: @json(csrf_token()),
                                    model_type: 'user',
                                    model_id: this.dataset.group_id,
                                })
                            }).then(response => response.json())
                                .then(data => {
                                    console.log(data);
                                    if (data.result == 'Success') {
                                        result_span.innerHTML = 'Upfollowed';
                                        result_span.classList.add('text-success')
                                        document.getElementById('groupRow'+this.dataset.group_id).style.display = "none"

                                    } else {
                                        result_span.classList.add('text-danger');
                                        result_span.innerHTML = 'Error In Unfollowing';
                                    }
                                })
                        });
                    }

                    // send messages

                    let user_message_btn = document.getElementsByClassName("user_message_btn");
                    let user_message_body = document.getElementsByClassName("user_message_body");
                    let user_message_subject = document.getElementsByClassName("user_message_subject");
                    var userMessageModal = document.getElementById('userMessageModal');
                    var modal = bootstrap.Modal.getInstance(userMessageModal);

                    user_message_btn.addEventListener('click', function () {
                        console.log(this.dataset.group_name,this.dataset.group_id);
                        fetch('{{ route('user.user_contact_followers') }}', {
                            headers: {"Content-Type": "application/json; charset=utf-8"},
                            method: 'POST',
                            body: JSON.stringify({
                                _token: @json(csrf_token()),
                                subject: user_message_body.value,
                                body: user_message_subject.value,
                                model_id: this.dataset.group_id,
                            })
                        }).then(response => response.json())
                            .then(data => {
                                console.log(data);
                                if (data.result == 'Success') {
                                    result_span.innerHTML = 'Upfollowed';
                                    result_span.classList.add('text-success');
                                    user_message_subject.value = '';
                                    user_message_body.value = '';
                                    modal.hide();
                                } else {
                                    result_span.classList.add('text-danger');
                                    result_span.innerHTML = 'Error In Unfollowing';
                                }
                            })
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
