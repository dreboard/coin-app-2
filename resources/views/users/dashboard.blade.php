@extends('layouts.user.main')
@section('pageTitle', 'My Dashboard')
@section('sidebar')
    @parent
    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">My Dashboard</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Profile
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li>
                        <a class="dropdown-item" href="{{ route('user.public', ['id' => auth()->user()->id]) }}">Public
                            Page</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">Profile Settings</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.edit_public', ['user' => auth()->user()->id]) }}">Edit Profile</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_edit_avatar', ['user' => auth()->user()->id]) }}">Change Image</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Home</li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            @if($systemMessageCount > 0)
                <div class="alert alert-danger mb-2" role="alert">
                    <a href="{{ route('user.message_all') }}" class="list-group-item list-group-item-action">You
                        Have System Messages <span class="badge bg-danger rounded-pill">{{$messageCount}}</span></a>
                </div>
            @endif

            @if($messageCount > 0)
                <div class="alert alert-warning mb-2" role="alert">
                    <a href="{{ route('messages.messages') }}" class="list-group-item list-group-item-action">You Have
                        Messages <span class="badge bg-warning rounded-pill">{{$messageCount}}</span></a>
                </div>
            @endif

            <div class="row">

                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>My Collection</h4>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Do Something
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('member.view_books') }}">Add Coin</a></li>
                                <li><a class="dropdown-item" href="{{ route('member.view_books') }}">Collection Home</a>
                                </li>
                                <li><a class="dropdown-item" href="{{ route('member.view_books') }}">Image Gallery</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                @if(optional(auth()->user())->status !== 'warn')
                    <div class="col-4">
                        <div class="callout callout-primary">
                            <h4>Articles</h4>
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button"
                                        data-bs-toggle="dropdown" aria-expanded="false">
                                    Do Something
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="{{ route('member.posts_index') }}">All</a></li>
                                    <li><a class="dropdown-item"
                                           href="{{ route('member.user_posts', ['id' => auth()->user()->id]) }}">My
                                            Posts</a></li>
                                    <li><a class="dropdown-item" href="{{ route('member.create_posts') }}">Create</a>
                                    </li>
                                    <li><a class="dropdown-item" href="{{ route('member.view_books') }}">Reviews</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif


                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Messages</h4>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Do Something
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('user.message_all') }}">User Messages</a>
                                </li>
                                <li><a class="dropdown-item" href="{{ route('user.message_all') }}">System Messages</a>
                                </li>
                                <li><a class="dropdown-item" href="{{ route('member.create_posts') }}">Create</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- ---------------------------------------------------------------------------------------------------------->
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->

            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">
                    <div class="card-header">My Area</div>
                    <div class="card-body">
                        <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Go Somewhere
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('member.my_posts') }}">My Articles</a></li>
                                <li><a class="dropdown-item" href="{{ route('member.create_posts') }}">My Forum
                                        Posts</a></li>
                                <li><a class="dropdown-item" href="{{ route('member.create_posts') }}">Book</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            @endif


            <!-- Categories widget-->
            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">
                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.user_types', ['type' => 'Writer']) }}">User
                                            Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            @endif

            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('news.all') }}">News</a>
                                </li>
                                <li><a class="external_link" href="https://www.coinbooks.org/index.html">Numismatic
                                        Bibliomania
                                        Society</a></li>
                                <li><a class="external_link" href="https://nnp.wustl.edu/">The Newman Numismatic
                                        Portal</a></li>
                                <li><a class="external_link" href="https://donum.numismatics.org/">Database Of
                                        Numismatic
                                        Materials</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

