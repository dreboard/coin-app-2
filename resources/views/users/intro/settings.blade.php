@extends('layouts.user.main')
@section('pageTitle', 'User Intro Page')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Edit My Details</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Profile Pages
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.public', ['id' => auth()->user()->id]) }}">Public</a></li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.public', ['id' => auth()->user()->id]) }}">My Profile</a></li>
                </ul>
            </div>
        </div>
    </div>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
    </ol>
    <form method="post" action="{{ route('user.save_edit_public') }}" enctype="multipart/form-data">
        <div class="row mb-5">

            <div class="col-6">
                @csrf
                <div class="mb-3">
                    <label for="ana" class="form-label">User Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="username"
                           value="{{ old('name', auth()->user()->name) }}">
                </div>
                <div class="mb-3">
                    <label for="profile_visibility" class="form-label">Visibility</label>
                    <select class="form-select" aria-label="Default select example" id="profile_visibility" name="profile_visibility">
                        <option selected>Open this select menu</option>
                        <option value="0"@if(auth()->user()->profile_visibility == 0) selected @endif>Private</option>
                        <option value="1"@if(auth()->user()->profile_visibility == 1) selected @endif>Public</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="avatar" class="form-label">Avatar</label>
                    <input class="form-control" type="file" id="avatar" name="avatar" onchange="preview()">
                    <div id="imagesPreview" class="row mt-2">
                        <div class="col-3">
                            <div class="card text-center w-100">
                                <img class="text-center" src="{{asset('storage/'.auth()->user()->avatar)}}" width="100px" height="auto">
                                <div class="card-body">
                                    <p class="card-title">Current</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-3 text-center">
                            <div class="card text-center w-100">
                                <img id="image_preview_frame" width="100px" height="auto">
                                <div class="card-body">
                                    <p id="image_preview_text" class="card-title">Preview</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 text-center">
                            <div class="card text-center w-100">
                                <div>
                                    <button id="image_preview_btn" type="button" onclick="clearImage()" class="btn btn-primary mt-3">Clear Preview</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="mb-3">
                    <label for="specialty" class="form-label">Specialty</label>
                    <input type="text" class="form-control" id="specialty" name="specialty" placeholder="#1234567"
                           value="{{ old('specialty', auth()->user()->specialty) }}">
                </div>
                <div class="mb-3">
                    <label for="user_level" class="form-label">User Level</label>
                    <select class="form-select" aria-label="Default select example" id="user_level" name="user_level">
                        <option value="Beginner"@if(auth()->user()->user_level == 'Beginner') selected @endif>Beginner </option>
                        <option value="Advanced"@if(auth()->user()->user_level == 'Advanced') selected @endif>Advanced</option>
                        <option value="Expert"@if(auth()->user()->user_level == 'Expert') selected @endif>Expert</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="user_type" class="form-label">User Type</label>
                    <select class="form-select" aria-label="Default select example" id="user_type" name="user_type">
                        <option value="Collector"@if(auth()->user()->user_type == 'Collector') selected @endif>Collector </option>
                        <option value="Dealer"@if(auth()->user()->user_type == 'Dealer') selected @endif>Dealer</option>
                        <option value="Research"@if(auth()->user()->user_type == 'Research') selected @endif>Research</option>
                        <option value="Investor"@if(auth()->user()->user_type == 'Investor') selected @endif>Investor</option>
                    </select>
                </div>

            </div>
            <div class="col-6">
                <div class="mb-3">
                    <label for="ana" class="form-label">American Numismatic Association (ANA)#</label>
                    <input type="text" class="form-control" id="ana" name="ana" placeholder="#1234567"
                           value="{{ old('ana', auth()->user()->ana) }}">
                </div>
                <div class="mb-3">
                    <label for="png" class="form-label">Professional Numismatists Guild (PMG)#</label>
                    <input type="text" class="form-control" id="png" name="png" placeholder="#1234567"
                           value="{{ old('png', auth()->user()->png) }}">
                </div>
                <div class="mb-3">
                    <label for="cac" class="form-label">Certified Acceptance Corporation (CAC)#</label>
                    <input type="text" class="form-control" id="cac" name="cac" placeholder="#1234567"
                           value="{{ old('cac', auth()->user()->cac) }}">
                </div>
                <div class="mb-3">
                    <label for="pgcs" class="form-label">PCGS Collectors Club #</label>
                    <input type="text" class="form-control" id="pcgs" name="pcgs" placeholder="#1234567"
                           value="{{ old('pcgs', auth()->user()->pcgs) }}">
                </div>
                <div class="mb-3">
                    <label for="ebay_id" class="form-label">eBay ID</label>
                    <input type="text" class="form-control" id="ebay_id" name="ebay_id" placeholder="#1234567"
                           value="{{ old('ebay_id', auth()->user()->ebay_id) }}">
                </div>
                <div class="mb-3">
                    <label for="ebay_store" class="form-label">eBay Store Linktore</label>
                    <input type="text" class="form-control" id="ebay_store" name="ebay_store" placeholder="#1234567"
                           value="{{ old('ebay_store', auth()->user()->ebay_store) }}">
                </div>
                <div class="mb-3">
                    <label for="youtube" class="form-label">YouTube Channel Link</label>
                    <input type="text" class="form-control" id="youtube" name="youtube" placeholder="#1234567"
                           value="{{ old('youtube', auth()->user()->youtube) }}">
                </div>
            </div>
            <div class="col-12">
                <div class="row g-3 align-items-center mb-2">
                    <div class="mb-3">
                        <label for="public_message" class="form-label">Public Page Intro Text</label>
                        <textarea class="form-control" id="public_message" name="public_message"
                                  rows="3">{{ old('public_message', auth()->user()->public_message)}}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </form>
    @push('styles')
        <style>
            .display-comment .display-comment {
                margin-left: 40px
            }

            .ck-editor__editable {
                min-height: 250px;
            }
            #image_preview_frame {
                border: none!important;
                outline: none!important;
                display: none;
            }
            #imagesPreview img{
                display:block;
            }
            .card {
                border: none!important;
            }
            label {
                font-weight: bold;
            }

        </style>
    @endpush
    @push('scripts')
        <script src="https://cdn.ckeditor.com/ckeditor5/35.2.1/classic/ckeditor.js"></script>
        <script>
            let image_preview_frame = document.getElementById('image_preview_frame');

            function preview() {
                image_preview_frame.style.display = "inline";
                image_preview_frame.src=URL.createObjectURL(event.target.files[0]);
            }
            function clearImage() {
                document.getElementById('avatar').value = null;
                image_preview_frame.src = "";
            }
            (function () {
                try {
                    ClassicEditor.defaultConfig = {
                        toolbar: {
                            items: [
                                'heading',
                                '|',
                                'bold',
                                'italic',
                                '|',
                                'bulletedList',
                                'numberedList',
                                '|',
                                'insertTable',
                                '|',
                                'undo',
                                'redo'
                            ]
                        },
                        image: {
                            toolbar: [
                                'imageStyle:full',
                                'imageStyle:side',
                                '|',
                                'imageTextAlternative'
                            ]
                        },
                        table: {
                            contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells']
                        },
                        language: 'en'
                    };

                    ClassicEditor
                        .create(document.querySelector('#public_message')).catch(error => {
                        console.error(error);
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
