@extends('layouts.user.main')
@section('pageTitle', 'User Intro Page')
@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h4 class="mt-4">Welcome to {{ config('app.name', 'Collecting United') }}</h4>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Profile Pages
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.public', ['id' => auth()->user()->id]) }}">Public</a></li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.public', ['id' => auth()->user()->id]) }}">My Profile</a></li>
                </ul>
            </div>
        </div>
    </div>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
    </ol>
        <div class="row mb-5">

            <div class="col-8">
                <h3>Welcome to {{ config('app.name', 'Collecting United') }}</h3>


            </div>
            <div class="col-6">


            </div>
        </div>

    @push('styles')
        <style>
            .display-comment .display-comment {
                margin-left: 40px
            }

            .ck-editor__editable {
                min-height: 250px;
            }
            #image_preview_frame {
                border: none!important;
                outline: none!important;
                display: none;
            }
            #imagesPreview img{
                display:block;
            }
            .card {
                border: none!important;
            }
            label {
                font-weight: bold;
            }

        </style>
    @endpush
    @push('scripts')
        <script>

            (function (ENVIRONMENT) {
                try {


                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
