@extends('layouts.user.main')
@section('title', 'User Search Results')
@section('content')

    <h3 class="mt-4">{{ $type }} Users ()</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">


            @if($type == 'Research')
                <div class="alert alert-primary" role="alert">
                    You can also visit the <a href="{{ route('user.research') }}">Research</a> section for more resources.
                </div>
            @endif
            @if($type == 'Writer')
                <div class="alert alert-primary" role="alert">
                    You can also visit the <a href="{{ route('user.writers') }}">Writer</a> section for more resources.
                </div>
            @endif



            <form method="post" action="{{ route('user.searchForm') }}"
                  class="row row-cols-lg-auto g-3 align-items-center">
                @csrf
                <div class="col-12">
                    <label class="visually-hidden" for="usernameSearchTerm">Username</label>
                    <div class="input-group">
                        <div class="input-group-text">@</div>
                        <input name="searchTerm" type="text" class="form-control" id="usernameSearchTerm"
                               placeholder="Username">
                    </div>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                </div>
            </form>


            <div id="paramList" class="btn-toolbar mt-2 mb-5">
                <div class="btn-group btn-group-lg mb-5">
                    <a id="A" href="{{ route('user.search', ['param' => 'A']) }}" class="btn btn-secondary">A</a>
                    <a id="B" href="{{ route('user.search', ['param' => 'B']) }}" class="btn btn-secondary">B</a>
                    <a id="C" href="{{ route('user.search', ['param' => 'C']) }}" class="btn btn-secondary">C</a>
                    <a id="D" href="{{ route('user.search', ['param' => 'D']) }}" class="btn btn-secondary">D</a>
                    <a id="E" href="{{ route('user.search', ['param' => 'E']) }}" class="btn btn-secondary">E</a>
                    <a id="F" href="{{ route('user.search', ['param' => 'F']) }}" class="btn btn-secondary">F</a>
                    <a id="G" href="{{ route('user.search', ['param' => 'G']) }}" class="btn btn-secondary">G</a>
                    <a id="H" href="{{ route('user.search', ['param' => 'H']) }}" class="btn btn-secondary">H</a>
                    <a id="I" href="{{ route('user.search', ['param' => 'I']) }}" class="btn btn-secondary">I</a>
                    <a id="J" href="{{ route('user.search', ['param' => 'J']) }}" class="btn btn-secondary">J</a>
                    <a id="K" href="{{ route('user.search', ['param' => 'K']) }}" class="btn btn-secondary">K</a>
                    <a id="L" href="{{ route('user.search', ['param' => 'L']) }}" class="btn btn-secondary">L</a>
                    <a id="M" href="{{ route('user.search', ['param' => 'M']) }}" class="btn btn-secondary">M</a>
                </div>
                <div class="btn-group btn-group-lg">
                    <a id="N" href="{{ route('user.search', ['param' => 'N']) }}" class="btn btn-secondary">N</a>
                    <a id="O" href="{{ route('user.search', ['param' => 'O']) }}" class="btn btn-secondary">O</a>
                    <a id="P" href="{{ route('user.search', ['param' => 'P']) }}" class="btn btn-secondary">P</a>
                    <a id="Q" href="{{ route('user.search', ['param' => 'Q']) }}" class="btn btn-secondary">Q</a>
                    <a id="R" href="{{ route('user.search', ['param' => 'R']) }}" class="btn btn-secondary">R</a>
                    <a id="S" href="{{ route('user.search', ['param' => 'S']) }}" class="btn btn-secondary">S</a>
                    <a id="T" href="{{ route('user.search', ['param' => 'T']) }}" class="btn btn-secondary">T</a>
                    <a id="U" href="{{ route('user.search', ['param' => 'U']) }}" class="btn btn-secondary">U</a>
                    <a id="V" href="{{ route('user.search', ['param' => 'V']) }}" class="btn btn-secondary">V</a>
                    <a id="W" href="{{ route('user.search', ['param' => 'W']) }}" class="btn btn-secondary">W</a>
                    <a id="X" href="{{ route('user.search', ['param' => 'X']) }}" class="btn btn-secondary">X</a>
                    <a id="Y" href="{{ route('user.search', ['param' => 'Y']) }}" class="btn btn-secondary">Y</a>
                    <a id="Z" href="{{ route('user.search', ['param' => 'Z']) }}" class="btn btn-secondary">Z</a>
                </div>
            </div>

            <div class="table-responsive">
                <table id="user_directory_tbl" class="table datatable">
                    <thead>
                    <tr>
                        <th class="fw-bold">Name</th>
                        <th class="fw-bold">Profile</th>
                        <th class="fw-bold">Type</th>
                        <th class="fw-bold">Specialty</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th class="fw-bold">Name</th>
                        <th class="fw-bold">Profile</th>
                        <th class="fw-bold">Type</th>
                        <th class="fw-bold">Specialty</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td class="text-start" style="width: 40%"><a href="{{ route('user.public', ['id' => $user->id]) }}">{{ $user->name }}</a></td>
                            <td class="text-start"><a href="{{ route('user.public', ['id' => $user->id]) }}">View</a></td>
                            <td class="text-start">
                                <a href="{{ route('user.user_types', ['type' => $user->user_type]) }}">{{ $user->user_type }}</a>
                            </td>
                            <td class="text-start">
                                <a href="{{ route('user.user_specialties', ['specialty' => $user->user_type]) }}">{{ $user->specialty }}</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Your Profile</div>
                <div class="card-body">
                    <div class="input-group">
                        <a href="{{ route('user.public', ['id' => auth()->user()->id]) }}" class="btn btn-primary"
                           id="button-search" type="button">My Profile</a>
                    </div>
                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Categories</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                <li><a href="{{ route('group.all') }}">See All</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('group.view_directory') }}">Directory</a></li>
                                <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Side widget-->
            <div class="card mb-4">
                <div class="card-header">By Speciality</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.user_types', ['type' => 'Writer']) }}">Writers</a></li>
                                <li><a href="{{ route('user.user_types', ['type' => 'Investor']) }}">Investors</a></li>
                                <li><a href="{{ route('user.user_types', ['type' => 'Dealer']) }}">Dealers</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.user_types', ['type' => 'Research']) }}">Researchers</a></li>
                                <li><a href="{{ route('user.user_types', ['type' => 'Investor']) }}">Investors</a></li>
                                <li><a href="{{ route('user.user_types', ['type' => 'Dealer']) }}">Dealers</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            (function () {
                try {
                    jQuery(document).ready(function () {
                        jQuery('#paramList a').each(function () {
                            if (jQuery(this).is(":contains('{{ request()->param }}')")) {
                                jQuery(this).addClass("btn-outline-secondary");
                                jQuery(this).removeClass("btn-secondary");
                            }
                        });
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
