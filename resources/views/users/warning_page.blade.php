@extends('layouts.user.main')
@section('pageTitle', 'Warning Page')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')

    <h3 class="mt-4">Warning Page</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        <li class="breadcrumb-item"><a href="{{ route('user.message_all') }}">Messages</a> </li>
    </ol>
    <div class="container">
        <div class="main-body">
            <p> Your account has been issued {{ $warnings }} warning(s), <a href="{{ route('user.violation_against_user') }}">VIEW REPORTED VIOLATIONS</a></p>
            <ul>
                <li>Your site behavior will be monitored</li>
                <li>You will not have access to multiple user features, groups, clubs etc </li>
                <li>Please visit the <a href="{{ route('policies') }}">Terms of Use</a></li>
            </ul>
            <h4 class="text-danger"> Three warnings will result in your account being terminated.</h4>



        </div>
    </div>


@endsection

