@extends('layouts.user.main')
@section('pageTitle', 'My Messages')
@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')

    <h3 class="mt-4">My Messages</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        <li class="breadcrumb-item"><a href="{{ route('user.message_all') }}">Messages</a> </li>
    </ol>
    <div class="container">
        <div class="main-body">

            <div class="card mb-4">
                <div class="card-body">
                    <div class="small text-muted">{{optional($notification->read_at)->format('F jS Y, g:ia') ?? $notification->created_at->format('F jS Y, g:ia') }}</div>
                    <h2 class="card-title">{{$notification->data['subject']}}</h2>
                    <p class="card-text">{!!$notification->data['body']!!}</p>

                    <form class="row row-cols-lg-auto g-3 align-items-center" method="post" action="{{ route('user.system_message_delete') }}">
                        @csrf
                        <input type="hidden" name="id" value="{{ $notification->id }}">
                        <div class="col-12">
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </div>
                    </form>
                </div>
            </div>


        </div>
    </div>


@endsection

