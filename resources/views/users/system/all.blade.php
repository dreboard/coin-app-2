@extends('layouts.user.main')
@section('pageTitle', 'All Messages')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')

    <h3 class="mt-4">My Messages</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
    </ol>
    <div class="container">

        <div class="mb-2">
            <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                <form class="d-flex flex-row align-items-center flex-wrap" method="POST" action="{{ route('user.system_message_delete_all')  }}">
                    @csrf
                    <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                    <button type="submit" class="btn btn-danger">Delete All</button>
                </form>
                <form class="d-flex flex-row align-items-center flex-wrap" method="POST" action="{{ route('user.system_message_mark_all')  }}">
                    @csrf
                    <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                    <button type="submit" class="btn btn-warning">Mark All As Read</button>
                </form>
                <a class="btn btn-primary" href="{{ route('user.system_message_create') }}" role="button">Create</a>
            </div>
        </div>

        <div class="main-body">
            <table class="table table-hover datatable">
                <thead>
                <tr>
                    <th scope="col">From</th>
                    <th scope="col">Subject</th>
                    <th scope="col">Posted</th>
                </tr>
                </thead>
                <tbody>
                @forelse (auth()->user()->notifications as $notification)
                    <tr>
                        <td>{{$notification->data['from']}}</td>
                        <td>
                            <span class="@if($notification->read_at) fw-lighter @else fw-bold @endif">
                                <a href="{{ route('user.system_message_view', ['id' => $notification->id]) }}">{{$notification->data['subject']}}</a>
                            </span>
                        </td>
                        <td>{{ optional($notification->read_at)->format('F jS Y, g:ia') ?? $notification->created_at->format('F jS Y, g:ia')}}</td>
                    </tr>
                @empty
                    <tr>
                        <td></td>
                        <td>There are no new notifications</td>
                        <td></td>
                    </tr>
                @endforelse
                </tbody>
            </table>

        </div>
    </div>

    @push('scripts')

    @endpush
@endsection

