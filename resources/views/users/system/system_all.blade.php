@extends('layouts.user.main')
@section('pageTitle', 'My Messages')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')

    <h3 class="mt-4">My Messages</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
    </ol>
    <div class="container">

        <div class="mb-2">
            <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#delMsgModal">Delete All</button>
                <button type="button" class="btn btn-warning">Mark All As Read</button>
                @if (auth()->user()->is_admin)
                    <a class="btn btn-primary" href="{{ route('admin.message_new') }}" role="button">Create</a>
                @endif

            </div>
        </div>

        <div class="modal fade" id="delMsgModal" tabindex="-1" aria-labelledby="delMsgModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="delMsgModalLabel">Delete All Messages</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        This can not be undone
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <a class="btn btn-danger" href="{{ route('user.system_message_delete_all') }}" role="button">Yes Delete</a>
                    </div>
                </div>
            </div>
        </div>




        <div class="main-body">
            <table class="table table-hover datatable">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">From</th>
                    <th scope="col">Subject</th>
                    <th scope="col">Posted</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>Admin</td>
                    <td><a href="{{ route('user.message_view', ['id' => 1]) }}">Warning status</a> </td>
                    <td>08:30 PM</td>
                </tr>

                @forelse (auth()->user()->notifications as $notification)
                    <tr>
                        <th scope="row">1</th>
                        <td>{{$notification->data['from']}}</td>
                        <td><a href="{{ route('user.message_view', ['id' => $notification->id]) }}">{{$notification->data['subject']}}</a></td>
                        <td>{{$notification->created_at ?? $notification->read_at}}</td>
                    </tr>
                @empty
                    <tr>
                        <th scope="row"></th>
                        <td></td>
                        <td>There are no new notifications</td>
                        <td></td>
                    </tr>
                @endforelse
                </tbody>
            </table>

        </div>
    </div>

    @push('scripts')

    @endpush
@endsection

