@extends('layouts.user.main')
@section('pageTitle', 'Send A Message to The Administrator')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')

    <h3 class="mt-4">Send A Message to The Administrator</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.message_all') }}">Messages</a></li>
    </ol>
    <div class="container">
        <div class="main-body">
            <form id="system_message_form" method="post" action="{{ route('user.system_message_save') }}">
                @csrf
                <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                <div class="mb-3">
                    <label for="system_message_subject" class="form-label">Subject</label>
                    <input type="text" class="form-control" id="system_message_subject" name="subject">
                </div>
                <div class="mb-3">
                    <label for="system_message_body" class="form-label">Message</label>
                    <textarea class="form-control" id="system_message_body" name="body" rows="3"></textarea>
                </div>
                <button type="submit" id="system_message_btn" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

@endsection

