<div class="table-responsive">
    <table class="table table-hover datatable mt-3 mb-3">
        <thead>
        <tr>
            <th scope="col">User</th>
            <th scope="col">Type</th>
            <th class="text-end" scope="col">Include</th>
        </tr>
        </thead>
        <tbody>

        @foreach($users as $user)
            <tr>
                <td class="text-start">
                    <label for="user_{{ $user->id }}" title="{{ $user->name }}">
                        {{ $user->name }}
                    </label>
                </td>
                <td>{{ucfirst($user->user_type)}}</td>
                <td style="padding-right: 45px;" class="text-end"><input id="user_{{ $user->id }}" type="checkbox" name="recipients[]"
                                                                         value="{{ $user->id }}" @if(old('recipients')) checked @endif></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
