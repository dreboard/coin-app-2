@extends('layouts.user.main')

@section('title', 'My Dashboard')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')

    <h1 class="mt-4">My Dashboard</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Home</li>
    </ol>
    <p>My Collection</p>

    <div class="table-responsive">
        <table class="table table-sm table-borderless">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">First</th>
                <th scope="col">Last</th>
                <th scope="col">Handle</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">1</th>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
            </tr>
            <tr>
                <th scope="row">3</th>
                <td colspan="2">Larry the Bird</td>
                <td>@twitter</td>
            </tr>
            </tbody>
        </table>
    </div>



    <div class="card-group">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">My Projects</h5>
                <ul class="list-group list-group-flush">
                    <a href="#" class="list-group-item list-group-item-action">Project 1</a>
                    <a href="#" class="list-group-item list-group-item-action">Project 2</a>
                    <a href="#" class="list-group-item list-group-item-action">Project 3</a>
                </ul>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Groups</h5>
                <p class="card-text"><a href="{{ route('group.all') }}" class="list-group-item list-group-item-action">See All</a></p>
                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                @if(is_null($group))
                    <a href="{{ route('group.start') }}" class="list-group-item list-group-item-action">Create</a>
                @else
                    <a href="{{ route('group.mine') }}" class="list-group-item list-group-item-action">My Groups</a>
                @endif
            </div>
        </div>
        <div class="card" style="width: 18rem;">
            <ul class="list-group list-group-flush">
                <a href="{{ route('user.message_all') }}" class="list-group-item list-group-item-action">
                    <span class="@if($systemMessageCount > 0) text-danger @endif @if(auth()->user()->status === 'warn') text-danger @endif">
                        System Messages
                    </span>
                    <span class="badge @if($systemMessageCount > 0) bg-danger @else bg-primary @endif rounded-pill">{{ $systemMessageCount }}</span>
                </a>
                @good
                    <a href="{{ route('messages.messages') }}" class="list-group-item list-group-item-action">User Messages <span class="badge bg-primary rounded-pill">{{$messageCount}}</span></a>
                    <a href="{{ route('user.view_directory') }}" class="list-group-item list-group-item-action">User Directory</a>
                @endgood
                <a href="{{ route('user.view_directory') }}" class="list-group-item list-group-item-action">Shop</a>
                <a href="{{ route('user.view_directory') }}" class="list-group-item list-group-item-action">News</a>
            </ul>
            <div class="card-footer">
                Card footer
            </div>
        </div>
    </div>


    <div class="card-group">
        <div class="card">
            <img src="..." class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Shop</h5>
                <p class="card-text">This is a widontent is a little bit longer.</p>
                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
            </div>
        </div>
        <div class="card">
            <img src="..." class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">News</h5>
                <p class="card-text">This card has supporting text onal content.</p>
                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
            </div>
        </div>
        <div class="card">
            <img src="..." class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">This is a wider car card haeight action.</p>
                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
            </div>
        </div>
    </div>

@endsection
