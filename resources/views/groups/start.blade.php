@extends('layouts.user.main')

@section('content')

    <h3 class="mt-4">Create A Group</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('group.index') }}">Group Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('group.all') }}">All Groups</a></li>
        <li class="breadcrumb-item"><a href="{{ route('group.mine') }}">My Groups</a></li>
    </ol>

    @if(isset($my_group))
        <div class="alert alert-success" role="alert">
            You have already created a group.  Visit your group page, <a href='{{route('group.view', ['group' => $my_group->id])}}'>{{$my_group->name}}</a>
        </div>
    @else
        <div class="mb-4">
            <p class="card-text">All users are allowed to create one group.  The user who creates the group, is the primary administrator and will control all aspects of the group/club/association.  Ownership can be transferred to another user after creating.</p>
            <a href="#" class="card-link" data-bs-toggle="modal" data-bs-target="#GroupTypeModal">Group Type</a>
            <a href="#" class="card-link" data-bs-toggle="modal" data-bs-target="#exampleModal" 2>Specialty</a>
        </div>
    @endif



    <div class="table-responsive">
        <table class="table table-striped text-successtable-border border-light text-center table-hover">
            <thead class="border-light">
            <tr>
                <th scope="col"></th>
                <th scope="col"><strong>User Group</strong></th>
                <th scope="col"><strong>Professional Coin Club</strong></th>
                <th scope="col"><strong>Professional Association</strong></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">Online Only</th>
                <td>Yes</td>
                <td>Has An Address</td>
                <td>Has An Address</td>
            </tr>
            <tr>
                <th scope="row">Allow Members</th>
                <td>Open</td>
                <td>Club Approval</td>
                <td>Association Approval</td>
            </tr>
            <tr>
                <th scope="row">Assign Roles</th>
                <td>Limited</td>
                <td><i class="fas fa-check"></i></td>
                <td><i class="fas fa-check"></i></td>
            </tr>
            <tr>
                <th scope="row">Event Calendar</th>
                <td><i class="fas fa-check"></i></td>
                <td><i class="fas fa-check"></i></td>
                <td><i class="fas fa-check"></i></td>
            </tr>
            <tr>
                <th scope="row">Newsletters</th>
                <td>No</td>
                <td><i class="fas fa-check"></i></td>
                <td><i class="fas fa-check"></i></td>
            </tr>
            <tr>
                <th scope="row">Photo/Video Galleries</th>
                <td><i class="fas fa-check"></i></td>
                <td><i class="fas fa-check"></i></td>
                <td><i class="fas fa-check"></i></td>
            </tr>
            <tr>
                <th scope="row">Address/Phone Req.</th>
                <td>No</td>
                <td><i class="fas fa-check"></i></td>
                <td><i class="fas fa-check"></i></td>
            </tr>
            @if(!isset($my_group))
            <tr>
                <th class=""></th>
                <th class=""><a href="{{ route('group.create') }}" class="btn btn-primary">Create</a></th>
                <th class=""><a href="{{ route('group.create_club') }}" class="btn btn-primary">Create</a></th>
                <th class=""><a href="{{ route('group.create_association') }}" class="btn btn-primary">Create</a></th>
            </tr>
            @endif
            </tbody>
        </table>
    </div>

    <div class="mb-5">
        <p class="card-text">All users are allowed to create one group.  The user who creates the group, is the primary administrator and will control all aspects of the group/club/association.  Ownership can be transferred to another user after creating.</p>
        <a href="#" class="card-link" data-bs-toggle="modal" data-bs-target="#GroupTypeModal">Group Type</a>
        <a href="#" class="card-link" data-bs-toggle="modal" data-bs-target="#exampleModal" 2>Specialty</a>
    </div>



    <div class="modal fade" id="GroupTypeModal" tabindex="-1" aria-labelledby="GroupTypeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="GroupTypeModalLabel">Group Type</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel2" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel2">Specialty</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


@endsection
