@extends('layouts.user.main')
@section('title', 'Group Search Results')
@section('content')

    <h3 class="mt-4">Group Search Results ({{ $search }})</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('group.index') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('group.mine') }}">My Groups</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.groups_i_follow') }}">Groups Followed</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <form method="post" action="{{ route('group.searchForm') }}"
                  class="row row-cols-lg-auto g-3 align-items-center">
                @csrf
                <div class="col-12">
                    <label class="visually-hidden" for="groupUsernameSearchTerm">Username</label>
                    <div class="input-group">
                        <div class="input-group-text">@</div>
                        <input name="searchTerm" type="text" class="form-control" id="groupUsernameSearchTerm"
                               placeholder="Group name">
                    </div>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                </div>
            </form>


            <div id="paramList" class="btn-toolbar mt-2 mb-5">
                <div class="btn-group btn-group-lg mb-5">
                    <a id="A" href="{{ route('group.search', ['param' => 'A']) }}" class="btn btn-secondary">A</a>
                    <a id="B" href="{{ route('group.search', ['param' => 'B']) }}" class="btn btn-secondary">B</a>
                    <a id="C" href="{{ route('group.search', ['param' => 'C']) }}" class="btn btn-secondary">C</a>
                    <a id="D" href="{{ route('group.search', ['param' => 'D']) }}" class="btn btn-secondary">D</a>
                    <a id="E" href="{{ route('group.search', ['param' => 'E']) }}" class="btn btn-secondary">E</a>
                    <a id="F" href="{{ route('group.search', ['param' => 'F']) }}" class="btn btn-secondary">F</a>
                    <a id="G" href="{{ route('group.search', ['param' => 'G']) }}" class="btn btn-secondary">G</a>
                    <a id="H" href="{{ route('group.search', ['param' => 'H']) }}" class="btn btn-secondary">H</a>
                    <a id="I" href="{{ route('group.search', ['param' => 'I']) }}" class="btn btn-secondary">I</a>
                    <a id="J" href="{{ route('group.search', ['param' => 'J']) }}" class="btn btn-secondary">J</a>
                    <a id="K" href="{{ route('group.search', ['param' => 'K']) }}" class="btn btn-secondary">K</a>
                    <a id="L" href="{{ route('group.search', ['param' => 'L']) }}" class="btn btn-secondary">L</a>
                    <a id="M" href="{{ route('group.search', ['param' => 'M']) }}" class="btn btn-secondary">M</a>
                </div>
                <div class="btn-group btn-group-lg">
                    <a id="N" href="{{ route('group.search', ['param' => 'N']) }}" class="btn btn-secondary">N</a>
                    <a id="O" href="{{ route('group.search', ['param' => 'O']) }}" class="btn btn-secondary">O</a>
                    <a id="P" href="{{ route('group.search', ['param' => 'P']) }}" class="btn btn-secondary">P</a>
                    <a id="Q" href="{{ route('group.search', ['param' => 'Q']) }}" class="btn btn-secondary">Q</a>
                    <a id="R" href="{{ route('group.search', ['param' => 'R']) }}" class="btn btn-secondary">R</a>
                    <a id="S" href="{{ route('group.search', ['param' => 'S']) }}" class="btn btn-secondary">S</a>
                    <a id="T" href="{{ route('group.search', ['param' => 'T']) }}" class="btn btn-secondary">T</a>
                    <a id="U" href="{{ route('group.search', ['param' => 'U']) }}" class="btn btn-secondary">U</a>
                    <a id="V" href="{{ route('group.search', ['param' => 'V']) }}" class="btn btn-secondary">V</a>
                    <a id="W" href="{{ route('group.search', ['param' => 'W']) }}" class="btn btn-secondary">W</a>
                    <a id="X" href="{{ route('group.search', ['param' => 'X']) }}" class="btn btn-secondary">X</a>
                    <a id="Y" href="{{ route('group.search', ['param' => 'Y']) }}" class="btn btn-secondary">Y</a>
                    <a id="Z" href="{{ route('group.search', ['param' => 'Z']) }}" class="btn btn-secondary">Z</a>
                </div>
            </div>

            <div class="table-responsive mb-5">
                <table id="user_datatable" class="table datatable">
                    <thead>
                    <tr>
                        <th class="fw-bold">Name</th>
                        <th class="fw-bold">Type</th>
                        <th class="fw-bold">Specialty</th>
                        <th class="fw-bold">View</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th class="fw-bold">Name</th>
                        <th class="fw-bold">Type</th>
                        <th class="fw-bold">Specialty</th>
                        <th class="fw-bold">View</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach ($groups as $group)
                        <tr>
                            <td class="text-start"><a
                                    href="{{ route('group.view', ['group' => $group->id]) }}">{{ $group->name }}</a>
                            </td>
                            <td class="text-start">{{ ucfirst($group->group_type) }}</td>
                            <td class="text-start">{{ $group->specialty }}</td>
                            <td class="text-start"><a
                                    href="{{ route('group.public', ['group' => $group->id]) }}">View</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            @if(is_null($my_group))
                <div class="card mb-4">
                    <div class="card-header">Create A Group</div>
                    <div class="card-body">
                        <div class="input-group">
                            <a href="{{ route('group.start') }}" class="btn btn-primary" id="button-search"
                               type="button">Go!</a>
                        </div>
                    </div>
                </div>
            @else
                <div class="card mb-4">
                    <div class="card-header">Your Group</div>
                    <div class="card-body">
                        <div class="input-group">
                            <a href="{{ route('group.view', ['group' => $my_group->id]) }}" class="btn btn-primary"
                               id="button-search" type="button">{{ $my_group->name }}</a>
                        </div>
                    </div>
                </div>
            @endif
            <!-- Categories widget-->
            @include('groups.group.partials.group_section_side')
        </div>
    </div>

    @push('scripts')
        <script>
            (function () {
                try {
                    jQuery(document).ready(function () {
                        jQuery('#paramList a').each(function () {
                            if (jQuery(this).is(":contains('{{ request()->param }}')")) {
                                jQuery(this).addClass("btn-outline-secondary");
                                jQuery(this).removeClass("btn-secondary");
                            }
                        });
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
