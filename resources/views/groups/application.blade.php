@extends('layouts.groups.main')
@section('pageTitle', $group->name.' Application Page')

@section('content')

    <h3 class="mt-4">Request Group Membership</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('group.all') }}">All Groups</a></li>
        <li class="breadcrumb-item"><a href="{{ route('group.mine') }}">My Groups</a></li>
    </ol>

        <h3 class="card-title mb-2">{{ $group->name }} Application </h3>

    <span class="text-danger"><small>*All Required</small></span>
    <div class="row">
        <div class="col-6">
            <form class="mb-7" action="{{ route('group.request_store') }}" method="post" id="groupForm">
                @csrf
                <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                <input type="hidden" name="group_type" value="{{ $group->group_type }}">
                <input type="hidden" name="group_id" value="{{ $group->id }}">


                <div class="mb-3">
                    <label for="first_name" class="form-label">First Name</label>
                    <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First"
                           value="{{ old('first_name') }}">
                </div>
                <div class="mb-3">
                    <label for="first_name" class="form-label">Last Name</label>
                    <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last"
                           value="{{ old('last_name') }}">
                </div>



                <div class="mb-3">
                    <label for="address" class="col-sm-2 col-form-label">Address</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="address" id="address" placeholder="123 Main"
                               value="{{ old('address') }}">
                    </div>
                </div>
                <div class="mb-3">
                    <label for="city" class="col-sm-2 col-form-label">City</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="city" id="city" placeholder="Subject"
                               value="{{ old('city') }}">
                    </div>
                </div>
                <div class="mb-3">
                    <label for="state" class="col-sm-2 col-form-label">State</label>
                    <div class="col-sm-10">
                        @include('components.forms.state_select')
                    </div>
                </div>
                <div class="mb-3">
                    <label for="postalcode" class="col-sm-2 col-form-label">Zip</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="postalcode" id="postalcode" placeholder="55555"
                               value="{{ old('postalcode') }}">
                    </div>
                </div>
                <div class="mb-3">
                    <label for="phone" class="col-sm-2 col-form-label">Phone</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="phone" id="phone" placeholder="555-555-5555"
                               value="{{ old('phone') }}">
                    </div>
                </div>
                <div class="w-10">
                    <button style="max-width: 100px;" type="submit" class="btn btn-primary form-control w-10">Apply</button>
                </div>

            </form>
        </div>
        <div class="col-6 card">
            <div class="card-body">
                <h5 class="card-title">This group requires personal data to join</h5>
                <p class="card-text">By completing this membership form you knowingly and voluntarily provide
                    personal information to {{ $group->name }}. Such information will
                    only be used for club business.</p>
                <ul>
                    <li>Billing of club dues (if applicable)</li>
                    <li>Mailings (calendars, newsletters)</li>
                    <li>Purchases</li>
                </ul>
            </div>


        </div>

        <div class="mt-5 mb-5"></div>
    </div>



@endsection
