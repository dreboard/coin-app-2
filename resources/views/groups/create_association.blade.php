@extends('layouts.user.main')

@section('content')

    <h3 class="mt-4">Create A Group</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('group.all') }}">My Groups</a></li>
    </ol>
    <div class="container">

        <div class="row">
            <div class="col-md-8">
                <form action="{{ route('group.store_club') }}" method="post" id="groupForm" enctype="multipart/form-data">
                    @csrf
                    @include('groups.group.partials.group_form', ['create' => true])
                </form>
            </div>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">About Groups</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
                        Group Type
                        <p class="card-text">All users are allowed to create one group.</p>
                        <a href="#" class="card-link" data-bs-toggle="modal" data-bs-target="#GroupTypeModal">Group Type</a>
                        <a href="#" class="card-link" data-bs-toggle="modal" data-bs-target="#exampleModal"2>Specialty</a>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="GroupTypeModal" tabindex="-1" aria-labelledby="GroupTypeModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="GroupTypeModalLabel">Group Type</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            ...
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel2" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="exampleModalLabel2">Specialty</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            ...
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>









        </div>


    </div>

@endsection
