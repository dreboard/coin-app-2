@extends('layouts.user.main')
@section('pageTitle', 'My Groups')
@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    @push('header')
        <h3 class="mt-4">My Groups</h3>
    @endpush

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        <li class="breadcrumb-item"><a href="{{ route('group.all') }}">All Groups</a> </li>
        <li class="breadcrumb-item"><a href="{{ route('user.groups_i_follow') }}">Groups Followed</a></li>
    </ol>

    <div class="card-body">
        <span id="result_span"></span>
        <div class="table-responsive">
            <table id="user_datatable" class="table datatable">
                <thead>
                <tr>
                    <th class="fw-bold">Name</th>
                    <th class="fw-bold">Type</th>
                    <th class="fw-bold">Specialty</th>
                    <th class="fw-bold">View</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th class="fw-bold">Name</th>
                    <th class="fw-bold">Type</th>
                    <th class="fw-bold">Specialty</th>
                    <th class="fw-bold">View</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach ($groups as $group)
                    <tr class="dataSpans" id="groupRow{{$group->id}}">
                        <td class="text-start"><a href="{{ route('group.view', ['group' => $group->id]) }}">{{ $group->name }}</a></td>
                        <td class="text-start">{{ ucfirst($group->group_type) }}</td>
                        <td class="text-start">{{ $group->specialty }}</td>
                        <td class="text-start">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <a href="{{ route('group.view', ['group' => $group->id]) }}"
                                   class="btn btn-primary" data-group="{{$group->id}}">View</a>
                                <a class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#groupDeleteModal{{$group->id}}"
                                   data-group_name="{{ $group->name }}" data-group_id="{{ $group->id }}">X</a>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="groupDeleteModal{{$group->id}}" tabindex="-1" aria-labelledby="groupDeleteModalLabel{{$group->id}}" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h1 class="modal-title fs-5" id="groupDeleteModalLabel{{$group->id}}">{{$group->name}}</h1>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            Yes, remove me from {{$group->name}}.
                                            <a class="btn btn-danger removeGroupBtns"
                                               data-bs-toggle="modal" data-bs-target="#groupDeleteModal"
                                               data-group_name="{{ $group->name }}" data-group_id="{{ $group->id }}">X</a>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @push('scripts')
        <script>
            (function () {
                try {
                    let removeGroupBtns = document.getElementsByClassName("removeGroupBtns");
                    let result_span = document.getElementById('result_span');

                    for (var i = 0; i < removeGroupBtns.length; i++) {
                        removeGroupBtns[i].addEventListener('click', function () {
                            console.log(this.dataset.group_name,this.dataset.group_id);
                            fetch('{{ route('group.remove_myself') }}', {
                                headers: {"Content-Type": "application/json; charset=utf-8"},
                                method: 'POST',
                                body: JSON.stringify({
                                    _token: @json(csrf_token()),
                                    remove_type: 'self',
                                    group_id: this.dataset.group_id,
                                })
                            }).then(response => response.json())
                                .then(data => {
                                    console.log(data);
                                    if (data.result == 'Success') {
                                        result_span.innerHTML = 'Updated';
                                        result_span.classList.add('text-success')
                                        document.getElementById('groupRow'+this.dataset.group_id).style.display = "none"

                                    } else {
                                        result_span.classList.add('text-danger');
                                        result_span.innerHTML = 'Error In saving';
                                    }
                                })
                        });
                    }
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
