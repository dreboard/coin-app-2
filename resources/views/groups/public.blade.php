@extends('layouts.groups.main')
@section('pageTitle', $group->name.' Public Page')

@section('content')

    <h3 class="mt-4">{{ $group->name }}</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('group.all') }}">All Groups</a></li>
        <li class="breadcrumb-item"><a href="{{ route('group.mine') }}">My Groups</a></li>
    </ol>

    <div class="row">
        <div class="col-lg-8">
            <!-- Post content-->
            <article>
                <!-- Post header-->
                <header class="mb-4">
                    <div class="text-muted fst-italic mb-2">Founded {{ $group->formed }}</div>
                    <!-- Post categories-->
                    <a class="badge bg-primary text-decoration-none link-light" href="#!">{{ $group->specialty }}</a>
                </header>
                <figure class="mb-4">
                    <img src="{{asset('storage/'.$group->image_url)}}" width="250" alt="" />
                </figure>
                <!-- Post content-->
                <section class="mb-5">
                    <p class="fs-5 mb-4">{{ $group->short_description }}</p>
                    <p class="fs-5 mb-4">{{ $group->description }}</p>
                </section>
            </article>


        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            @if(!isset($is_member))
                <div class="card mb-4">
                <div class="card-header">Join</div>
                <div class="card-body">

                    @if ($group->private == 0)
                        @isset(auth()->user()->first_name)
                            <div class="card mb-7">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $group->name }} is open</h5>
                                    <p class="card-text">We process immediate approval for this group.</p>

                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <form class="row row-cols-lg-auto g-3 align-items-center" method="post" action="{{ route('group.request_join') }}">
                                            @csrf
                                            <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                                            <input type="hidden" name="group_type" value="{{ $group->group_type }}">
                                            <input type="hidden" name="group_id" value="{{ $group->id }}">
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary">Join</button>
                                            </div>
                                        </form>
                                        <a class="btn btn-secondary" href="{{ route('group.group_contact_form', ['group' => $group->id]) }}">Contact</a>
                                    </div>

                                </div>
                            </div>
                        @endisset

                        <br class="mt-4" />
                    @else
                        <div class="card mb-7">
                            <div class="card-body">
                                <h5 class="card-title">Group Application</h5>
                                <a class="btn btn-primary" href="{{ route('group.request_join', ['group' => $group->id]) }}">Apply</a>
                                <a class="btn btn-secondary" href="{{ route('group.group_contact_form', ['group' => $group->id]) }}">Contact</a>
                                @if(auth()->user()->isFollowing($group))
                                    <button type="button" class="btn btn-success" id="follow_model_btn">
                                        Following
                                    </button>
                                @else
                                    <button type="button" class="btn btn-primary" id="follow_model_btn">
                                        Follow
                                    </button>
                                @endif
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            @else
                <div class="card mb-4">
                    <div class="card-header">Public Preview</div>
                    <div class="card-body">
                        <a class="btn btn-outline-dark" href="{{ route('group.view', ['group' => $group->id]) }}">Back
                            to group</a>
                    </div>
                </div>
            @endif
            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Details</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><span class="fw-bold"> ANA: </span> {{ $group->ana ?? 'No' }}</li>
                                <li><span class="fw-bold"> Meets: </span> {{ $group->meets }}</li>
                                <li><span class="fw-bold"> Type: </span> {{ ucfirst($group->group_type) }}</li>
                                <li><span class="fw-bold"> Contact: </span> <a href="{{ route('group.group_contact_form', ['group' => $group->id]) }}">Send Message</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    @push('scripts')
        <script>
            (function (ENVIRONMENT) {
                try {
                    let follow_model_btn = document.getElementById('follow_model_btn');
                    follow_model_btn.addEventListener('click', function () {

                        fetch('{{ route('user.user_follow') }}', {
                            headers: {"Content-Type": "application/json; charset=utf-8"},
                            method: 'POST',
                            body: JSON.stringify({
                                _token: @json(csrf_token()),
                                model_type: 'group',
                                model_id: {{ $group->id }},
                            })
                        }).then(response => response.json())
                            .then(data => {
                                console.log(data);
                                if (data.result == 'Success') {
                                    follow_model_btn.innerHTML = 'Following';
                                    follow_model_btn.classList.add('btn-success')
                                } else {
                                    follow_model_btn.classList.add('text-danger');
                                    follow_model_btn.innerHTML = 'Error';
                                }
                            })
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
