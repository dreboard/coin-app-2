@extends('layouts.groups.main')
@section('pageTitle', $group->name.' Contact Page')

@section('content')

    <h3 class="mt-4">{{ $group->name }}</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('group.all') }}">All Groups</a></li>
        <li class="breadcrumb-item"><a href="{{ route('group.mine') }}">My Groups</a></li>
    </ol>

    <div class="row">
        <div class="col-lg-8">
            <!-- Post content-->
            <form method="post"
                  action="{{ route('group.group_contact') }}">
                @csrf
                <input type="hidden" name="group_id" value="{{ $group->id }}">
                <div class="mb-3">
                    <label for="group_message_subject" class="form-label">Subject</label>
                    <input class="form-control" id="group_message_subject" name="subject" type="text" />
                </div>
                <div class="mb-3">
                    <label for="group_message_body" class="form-label">Message</label>
                    <textarea class="form-control" id="group_message_body" name="body" rows="3"></textarea>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-primary">Send</button>
                </div>
            </form>


        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Join</div>
                <div class="card-body">
                    <div class="mb-3">
                        <a href="{{ route('group.public', ['group' => $group->id]) }}">Back to group page</a>
                    </div>

                    @if(isset($is_member))
                        <a href="{{ route('group.view', ['group' => $group->id]) }}">Back to group</a>
                    @endif
                    @if ($group->private == 0)
                        @isset(auth()->user()->first_name)
                            <div class="card mb-7">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $group->name }} is open</h5>
                                    <p class="card-text">We process immediate approval for this group.</p>
                                    <form class="row row-cols-lg-auto g-3 align-items-center" method="post" action="{{ route('group.request_join') }}">
                                        @csrf
                                        <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                                        <input type="hidden" name="group_type" value="{{ $group->group_type }}">
                                        <input type="hidden" name="group_id" value="{{ $group->id }}">
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-primary">Join</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        @endisset

                        <br class="mt-4" />
                    @else
                        <div class="card mb-7">
                            <div class="card-body">
                                <h5 class="card-title">Group Application</h5>
                                <a class="btn btn-primary" href="{{ route('group.request_join', ['group' => $group->id]) }}">Apply</a>
                            </div>
                        </div>

                    @endif
                </div>
            </div>


        </div>
    </div>


@endsection
