@extends('layouts.admin.main')
@section('title', 'View All Groups')
@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    @push('header')
        <h3 class="mt-4">Group Directory</h3>
    @endpush

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        <li class="breadcrumb-item"><a href="{{ route('group.mine') }}">My Groups</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.groups_i_follow') }}">Groups Followed</a></li>
    </ol>

    <div class="card-body">

        <div class="table-responsive">
            <table id="user_datatable" class="table datatable">
                <thead>
                <tr>
                    <th class="fw-bold">Name</th>
                    <th class="fw-bold">Type</th>
                    <th class="fw-bold">Specialty</th>
                    <th class="fw-bold">View</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th class="fw-bold">Name</th>
                    <th class="fw-bold">Type</th>
                    <th class="fw-bold">Specialty</th>
                    <th class="fw-bold">View</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach ($groups as $group)
                    <tr>
                        <td class="text-start"><a href="{{ route('group.view', ['group' => $group->id]) }}">{{ $group->name }}</a></td>
                        <td class="text-start">{{ ucfirst($group->group_type) }}</td>
                        <td class="text-start">{{ $group->specialty }}</td>
                        <td class="text-start"><a href="{{ route('group.public', ['group' => $group->id]) }}">View</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
