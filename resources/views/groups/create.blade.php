@extends('layouts.user.main')
@section('title', 'Create Club')
@section('content')

    <h3 class="mt-4">Create A Group</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('group.all') }}">My Groups</a></li>
    </ol>
    <div class="container">

        <div class="row">
            <div class="col-md-8">
                <form action="{{ route('group.store_club') }}" method="post" id="groupForm" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                    <input type="hidden" name="group_type" value="club">
                    <div class="form-group mb-3">
                        <label for="name" class="control-label">Club Name</label>
                        <input type="text" class="form-control" name="name" placeholder="Club Name"
                               value="{{ old('name') }}">
                    </div>

                    <div class="form-group mb-3">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="private" id="private1" value="1" checked>
                            <label class="form-check-label" for="private1">
                                Open (No Approval Required)
                            </label>
                        </div>
                        <div class="form-check">
                            <input data-bs-toggle="modal" data-bs-target="#GroupTypeModal" class="form-check-input" type="radio" name="private" id="private2" value="0">
                            <label class="form-check-label" for="private2">
                                Private (Must Approve Each Member)
                            </label>
                        </div>
                    </div>

                    <div class="form-group mb-3">
                        <label for="specialty" class="control-label">Specialty</label>
                        <select  name="specialty" id="specialty" class="form-select" aria-label="Select....">
                            <option selected>Open this select menu</option>
                            <option value="General Numismatics">General Numismatics</option>
                            <option value="club">Coin Club</option>
                        </select>
                    </div>

                    <div class="form-group mb-3">
                        <label for="image_url" class="control-label">Club Logo</label>
                        <input class="form-control" type="file" name="image_url" id="image_url">
                    </div>

                    <div class="form-group mb-3">
                        <label for="formed" class="control-label">Year Formed</label>
                        <input type="text" class="form-control" id="formed" name="formed" placeholder="2000"
                               value="{{ old('formed') }}">
                    </div>

                    <div class="form-group mb-3">
                        <label for="meets" class="control-label">Meeting Days</label>
                        <input type="text" class="form-control" id="meets" name="meets" placeholder="Every 3rd monday"
                               value="{{ old('meets') }}">
                    </div>

                    <div class="form-group mb-3">
                        <label for="url" class="control-label">Website</label>
                        <input type="text" class="form-control" id="url" name="url" placeholder="www.myclub.com"
                               value="{{ old('url') }}">
                    </div>

                    <div class="form-group mb-3">
                        <label for="short_description" class="control-label">Short Description</label>
                        <textarea name="short_description" id="short_description" class="form-control" placeholder="Short Description">{{ old('short_description') }}</textarea>
                    </div>

                    <div class="form-group mb-3">
                        <label for="description" class="control-label">Description</label>
                        <textarea name="description" id="description" class="form-control" placeholder="Detailed Description">{{ old('description') }}</textarea>
                    </div>


                    <div class="form-group mb-3">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="require_info" id="require_info1" value="1" checked>
                            <label class="form-check-label" for="private1">
                                Require Applicants name, address, phone
                            </label>
                        </div>
                        <div class="form-check">
                            <input data-bs-toggle="modal" data-bs-target="#GroupTypeModal" class="form-check-input" type="radio" name="require_info" id="require_info2" value="0">
                            <label class="form-check-label" for="private2">
                                Just Username
                            </label>
                        </div>
                    </div>

                    <div class="form-group mt-3">
                        <button type="submit" class="btn btn-primary form-control">Save</button>
                    </div>
                </form>
            </div>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">About Groups</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
                        Group Type
                        <p class="card-text">All users are allowed to create one group.</p>
                        <a href="#" class="card-link" data-bs-toggle="modal" data-bs-target="#GroupTypeModal">Group Type</a>
                        <a href="#" class="card-link" data-bs-toggle="modal" data-bs-target="#exampleModal"2>Specialty</a>
                    </div>
                </div>
            </div>

            <div class="mt-5"></div>
            <div class="modal fade" id="GroupTypeModal" tabindex="-1" aria-labelledby="GroupTypeModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="GroupTypeModalLabel">Group Type</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            ...
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel2" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="exampleModalLabel2">Specialty</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            ...
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
