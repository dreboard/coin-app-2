@extends('layouts.groups.main')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')
        @if(intval(auth()->user()->userManageLevel($group->id))  > 2)
            <li class="breadcrumb-item"><a href="{{ route('group.create_election', ['group' => $group->id]) }}">Create Election</a> </li>
        @endif

    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <h4>Create An Issue For</h4>
            <form action="{{ route('group.save_issue') }}" method="post" class="mt-4">
                @csrf
                <input type="hidden" name="group_id" value="{{ $group->id }}">
                <div class="mb-3">
                    <label for="issue" class="form-label">Issue</label>
                    <input type="text" class="form-control @error('issue') is-invalid @enderror"
                           value="{{ old('issue') }}" id="issue" name="issue" aria-describedby="issueHelp">
                    @error('issue')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div id="issueHelp" class="form-text">The issue to vote on</div>
                </div>
                <div class="mb-3">
                    <select name="event_id" id="event_id" class="form-select" aria-label="Select A Meeting">
                        <option value="0" selected>Create Agenda For Meeting</option>
                        @if(!empty($events))
                            @foreach($events as $issue)
                                <option value="{{ $issue->id }}">{{ $issue->title }}</option>
                            @endforeach
                        @endif
                    </select>
                    <div id="issueHelp" class="form-text">Attach to Event</div>
                </div>
                <div class="mb-3">
                    <select name="transparency" id="transparency" class="form-select" aria-label="Default select example">
                        <option value="0" selected>Just Votes</option>
                        <option value="1">Votes and Names (Who voted for what)</option>
                    </select>
                    <div id="issueHelp" class="form-text">Transparency</div>
                </div>
                <div class="mb-3">
                    <select name="vote_level" id="vote_level" class="form-select" aria-label="Default select example">
                        <option value="0" selected>Majority</option>
                        <option value="1">Unanimous</option>
                    </select>
                    <div id="issueHelp" class="form-text">Vote Level</div>
                </div>
                <div class="mb-3 form-check">
                    <input type="checkbox" class="form-check-input" name="create_announcement" id="create_announcement" checked>
                    <label class="form-check-label" for="create_announcement">Create Announcement</label>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>

        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>
    @push('styles')
        <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
        <link href="https://cdn.jsdelivr.net/gh/Eonasdan/tempus-dominus@master/dist/css/tempus-dominus.css"
              rel="stylesheet" crossorigin="anonymous">
    @endpush
    @push('scripts')
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
                crossorigin="anonymous"></script>
        <!-- Tempus Dominus JavaScript -->
        <script src="https://cdn.jsdelivr.net/gh/Eonasdan/tempus-dominus@master/dist/js/tempus-dominus.js"
                crossorigin="anonymous"></script>

        <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
        <script>
            (function () {
                try {
                    // https://bootstrap-datepicker.readthedocs.io/en/latest/
                    $('.datepicker').datepicker({
                        format: 'yyyy-mm-dd',
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
