@extends('layouts.groups.main')
@section('title', 'Manage Club')
@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        @include('groups.group.partials.breadcrumb')

    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <h5>Group Issues</h5>
            <a class="btn btn-primary btn-sm"
               href="{{ route('group.create_issue', ['group' => $group->id]) }}">Create An Issue</a>

            <table class="table table-hover table-striped dataTable">
                @if(!empty($issues))
                    @foreach($issues as $issue)
                        <tr>
                            <td class="text-start">
                                <span class="text-danger fw-bold">{{ $issue->name }}</span>
                            </td>
                            <td class="text-end">
                                <a href="{{ route('group.view_issue', ['group' => $group->id, 'issue' => $issue->id]) }}">View</a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td class="text-start">No Current Issues</td>
                        <td class="text-end">
                        </td>
                    </tr>
                @endif

            </table>
        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>

@endsection
