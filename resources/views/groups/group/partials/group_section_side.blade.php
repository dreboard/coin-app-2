<div class="card mb-4">
    <div class="card-header">Categories</div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-6">
                <ul class="list-unstyled mb-0">
                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                    <li><a href="{{ route('group.all') }}">See All</a></li>
                </ul>
            </div>
            <div class="col-sm-6">
                <ul class="list-unstyled mb-0">
                    <li><a href="{{ route('group.view_directory') }}">Directory</a></li>
                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

