@push('scripts')
    <script src="https://cdn.ckeditor.com/ckeditor5/35.2.1/classic/ckeditor.js"></script>
    <script>
        let image_preview_frame = document.getElementById('image_preview_frame');

        function preview() {
            image_preview_frame.src=URL.createObjectURL(event.target.files[0]);
        }
        (function () {
            try {

                function preview() {
                    frame.src=URL.createObjectURL(event.target.files[0]);
                }

                function onlyOneCheckBox() {
                    let checkboxgroupText = document.getElementById('checkboxgroupText');
                    let checkboxgroup = document.getElementById('checkboxgroup').getElementsByTagName("input");
                    let limit = 3;
                    for (var i = 0; i < checkboxgroup.length; i++) {
                        checkboxgroup[i].onclick = function() {
                            var checkedcount = 0;
                            for (var i = 0; i < checkboxgroup.length; i++) {
                                checkedcount += (checkboxgroup[i].checked) ? 1 : 0;
                            }
                            if (checkedcount > limit) {
                                console.log("You can select maximum of " + limit + " checkbox.");
                                checkboxgroupText.innerText = "You can select maximum of " + limit + " checkbox.";
                                this.checked = false;
                            }
                        }
                    }
                }
                onlyOneCheckBox();


                ClassicEditor.defaultConfig = {
                    toolbar: {
                        items: [
                            'heading',
                            '|',
                            'bold',
                            'italic',
                            '|',
                            'bulletedList',
                            'numberedList',
                            '|',
                            'insertTable',
                            '|',
                            'undo',
                            'redo'
                        ]
                    },
                    image: {
                        toolbar: [
                            'imageStyle:full',
                            'imageStyle:side',
                            '|',
                            'imageTextAlternative'
                        ]
                    },
                    table: {
                        contentToolbar: [ 'tableColumn', 'tableRow', 'mergeTableCells' ]
                    },
                    language: 'en'
                };



                ClassicEditor
                    .create( document.querySelector( '#post_body' )).catch( error => {
                    console.error( error );
                } );


            } catch (error) {
                if (ENVIRONMENT === 'local') {
                    console.error(error);
                }
            }
        })();
    </script>
@endpush
