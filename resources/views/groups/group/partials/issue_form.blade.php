<input type="hidden" name="group_id" value="{{ $group->id }}">
<div class="mb-3">
    <label for="issue" class="form-label">Issue</label>
    <input type="text" class="form-control @error('issue') is-invalid @enderror"
           value="{{ old('issue') }}" id="issue" name="issue" aria-describedby="issueHelp">
    @error('issue')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div id="issueHelp" class="form-text">The issue to vote on</div>
</div>

<div class="mb-3">
    <label for="exampleFormControlTextarea1" class="form-label">Overview</label>
    <textarea class="form-control" name="overview" id="overview" rows="3"></textarea>
</div>
<div class="mb-3">
    <select name="event_id" id="event_id" class="form-select" aria-label="Select A Meeting">
        <option value="0" selected>Create Agenda For Meeting</option>
        @if(!empty($events))
            @foreach($events as $event)
                <option value="{{ $event->id }}">{{ $event->title }}</option>
            @endforeach
        @endif
    </select>
    <div id="issueHelp" class="form-text">Attach to Event</div>
</div>
<div class="mb-3">
    <select name="transparency" id="transparency" class="form-select" aria-label="Default select example">
        <option value="0" selected>Just Votes</option>
        <option value="1">Votes and Names (Who voted for what)</option>
    </select>
    <div id="issueHelp" class="form-text">Transparency</div>
</div>
<div class="mb-3">
    <select name="vote_level" id="vote_level" class="form-select" aria-label="Default select example">
        <option value="0" selected>Majority</option>
        <option value="1">Unanimous</option>
    </select>
    <div id="issueHelp" class="form-text">Vote Level</div>
</div>
<div class="mb-3 form-check">
    <input type="checkbox" class="form-check-input" name="create_announcement" id="create_announcement" checked>
    <label class="form-check-label" for="create_announcement">Create Announcement</label>
</div>
