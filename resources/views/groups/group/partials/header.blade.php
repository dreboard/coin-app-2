<div class="row">
    <div class="col-6 float-start">
        <h3 class="mt-4">{{ Str::limit($group->name, 35) }}</h3>
    </div>
    <div class="col-6">
        <div class="dropdown mt-4 float-end">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                My Actions
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                <li><a class="dropdown-item group_link" id="members_area_link" href="{{ route('group.members_home', ['group' => $group->id]) }}">Members Area</a></li>
                <li><a class="dropdown-item group_link" id="members_area_link" href="{{ route('group.members_home', ['group' => $group->id]) }}">Rules Area?? </a></li>
                @if($group->user_id !== auth()->user()->id)
                    <li><a class="dropdown-item text-danger group_link" id="leave_link" href="#" data-bs-toggle="modal" data-bs-target="#removeModal">Quit Group</a></li>
                @endif
            </ul>
        </div>
    </div>
</div>
<!-- removal modal -->
<div class="modal" tabindex="-1" id="removeModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('group.remove_user') }}" method="post">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Leave Group</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>You are about to remove yourself from this group</p>
                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}" />
                    <input type="hidden" name="group_id" value="{{ $group->id }}" />
                    <input type="hidden" name="remove_type" value="self" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-bs-dismiss="modal">Never-mind</button>
                    <button type="submit" class="btn btn-danger">Yes Remove Me</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- removal modal end-->
