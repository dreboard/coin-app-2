<div class="col-lg-4">
    <!-- Categories widget-->
    <div class="card mb-4">
        <div class="card-header">Categories</div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <ul class="list-unstyled mb-0 group_links">
                        <li><a class="group_link" id="members_link" href="{{ route('group.view_users', ['group' => $group->id]) }}" title="View Club Members">Members</a></li>
                        <li><a class="group_link" id="officer_link" href="{{ route('group.view', ['group' => $group->id]) }}" title="Assign Group Officers">Officers</a></li>
                        <li><a class="group_link" id="committee_link" href="{{ route('group.committee_index', ['group' => $group->id]) }}" title="View/Create Committees">Committees</a></li>
                        <li><a class="group_link" id="message_link" href="{{ route('group.message_index', ['group' => $group->id]) }}" title="Create Group Messages">Group Messages</a></li>
                        <li><a class="group_link" id="board_link" href="{{ route('group.view', ['group' => $group->id]) }}" title="View/Create Message Board">Message Board</a></li>
                        <li><a class="group_link" id="library_link" href="{{ route('group.library_index', ['group' => $group->id]) }}" title="Maintain Group Library">Library</a></li>
                    </ul>
                </div>
                <div class="col-sm-6">
                    <ul class="list-unstyled mb-0 group_links">
                        <li><a class="group_link" id="vote_link" href="{{ route('group.vote_home', ['group' => $group->id]) }}" title="Create/View Voting Issues">Voting</a></li>
                        <li><a class="group_link" id="event_link" href="{{ route('group.events_home', ['group' => $group->id]) }}" title="View/Create Events">Events</a></li>
                        <li><a class="group_link" id="blog_link" href="{{ route('group.posts_home', ['group' => $group->id]) }}" title="Read/Create Blog Posts">Blog</a></li>
                        <li><a class="group_link" id="media_link" href="{{ route('group.view', ['group' => $group->id]) }}" title="Images and Video">Media</a></li>
                        <li><a class="group_link" id="announce_link" href="{{ route('group.show_announcement_all', ['group' => $group->id]) }}" title="Create Announcements">Announcements</a></li>
                        <li><a class="group_link" id="announce_link" href="{{ route('group.public_preview', ['group' => $group->id]) }}" title="Create Announcements">Public Page</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
    <!-- Side widget-->
    <div class="card mb-4">
        <div class="card-header">{{ date('M Y') }} Events</div>
        <div class="card-body">
            <ul class="list-group list-group-flush">
                @if(!empty($events))

                    @foreach($events as $k => $event)
                        <li class="list-group-item">
                            <a href="{{ route('group.view', ['group' => $group->id]) }}">
                                {{ Carbon\Carbon::parse($event->start_at)->format('jS') }} | {{ $event->title }}
                            </a></li>
                    @endforeach
                @else
                    <li class="list-group-item">No events for {{ date('M Y') }}, <a href="{{ route('group.view', ['group' => $group->id]) }}">Full Schedule</a></li>
                @endif
            </ul>
        </div>
    </div>
</div>
