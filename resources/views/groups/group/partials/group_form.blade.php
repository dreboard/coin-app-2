<input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
<input type="hidden" name="group_type" value="club">

<div class="form-group mb-3">
    <label for="name" class="control-label">Club Name</label>
    <input type="text" class="form-control" name="name" placeholder="Club Name"
           value="{{ old('name', $group->name) }} @isset($group) {{ $group->name }} @endisset">
</div>

<div class="form-group mb-3">
    <div class="form-check">
        <input class="form-check-input" type="radio" name="private" id="private1" value="1"
               @isset($group)  @if($group->private == 1)  checked @endif @endisset />
        <label class="form-check-label" for="private1">
            Open
        </label>
    </div>
    <div class="form-check">
        <input data-bs-toggle="modal" data-bs-target="#GroupTypeModal" class="form-check-input" type="radio"
               name="private" id="private2" value="0" @isset($group)  @if($group->private == 0)  checked @endif @endisset />
        <label class="form-check-label" for="private2">
            Private
        </label>
    </div>
</div>

<div class="form-group mb-3">
    <label for="specialty" class="control-label">Specialty</label>
    <select  name="specialty" id="specialty" class="form-select" aria-label="Select....">

        <option>Choose....</option>
        <option value="General Numismatics"@isset($group)  @if($group->specialty == 'General Numismatics')  selected @endif @endisset>General Numismatics</option>
        <optgroup label="By Design">
            <option value="Seated Liberty">Seated Liberty</option>
            <option value="Barber">Barber</option>
        </optgroup>
        <optgroup label="By Denomination">
            <option value="Silver Dollars">Silver Dollars</option>
            <option value="Early American Coppers">Large/Small Cents</option>
        </optgroup>
        <option value="Error Collectors">Error Collectors</option>
        <option value="Variety Collectors">Variety Collectors</option>
    </select>
</div>

<div class="form-group mb-3">
    <label for="image_url" class="control-label">Club Logo</label>
    <input class="form-control" type="file" name="image_url" id="image_url">
</div>

<div class="form-group mb-3">
    <label for="formed" class="control-label">Year Formed</label>
    <input type="text" class="form-control" id="formed" name="formed" placeholder="2000"
           value="{{ old('formed') }} @isset($group) {{ $group->formed }} @endisset">
</div>

<div class="form-group mb-3">
    <label for="formed" class="control-label">ANA Number</label>
    <input type="text" class="form-control" id="ana" name="ana" placeholder="123456"
           value="{{ old('ana') }} @isset($group) {{ $group->ana }} @endisset">
</div>

<div class="form-group mb-3">
    <label for="meets" class="control-label">Meeting Days</label>
    <input type="text" class="form-control" id="meets" name="meets" placeholder="Every 3rd monday"
           value="{{ old('meets') }}@isset($group){{ $group->meets }}@endisset">
</div>

<div class="form-group mb-3">
    <label for="url" class="control-label">Website</label>
    <input type="text" class="form-control" id="url" name="url" placeholder="www.myclub.com"
           value="{{ old('url') }} @isset($group){{ $group->url }}@endisset">
</div>

<div class="form-group mb-3">
    <label for="short_description" class="control-label">Short Description</label>
    <textarea name="short_description" id="short_description" class="form-control" placeholder="Short Description">
        {{ old('short_description') }} @isset($group){{$group->short_description }} @endisset
    </textarea>
</div>

<div class="form-group mb-3">
    <label for="description" class="control-label">Description</label>
    <textarea name="description" id="description" class="form-control" placeholder="Detailed Description">
        {{ old('description') }}
        @isset($group) {{ $group->description }} @endisset
    </textarea>
</div>

<div class="card">
    <div class="card-body">
        <h5 class="card-title mb-2">Club Details </h5>
        <span class="text-danger"><small>*All Required</small></span>
        <div class="mb-3 row">
            <label for="address" class="col-sm-2 col-form-label">Club Address</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="address" id="address" placeholder="123 Main"
                       value="{{ old('address') }} @isset($group){{$group->address }} @endisset">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="first_name" class="col-sm-2 col-form-label">First Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First"
                       value="{{ old('first_name') }} @isset($group){{$group->first_name }} @endisset">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="last_name" class="col-sm-2 col-form-label">Last Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="last_name" id="last_name" placeholder="123 Main"
                       value="{{ old('last_name') }} @isset($group){{$group->last_name }} @endisset">
            </div>
        </div>


        <div class="mb-3 row">
            <label for="city" class="col-sm-2 col-form-label">Club City</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="city" id="city" placeholder="Subject"
                       value="{{ old('city') }} @isset($group){{$group->city }} @endisset">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="state" class="col-sm-2 col-form-label">Club State</label>
            <div class="col-sm-10">
                <select class="form-select" aria-label="Default select example" name="state" id="state">
                    <option selected>Choose...</option>
                    <option value="AL">Alabama</option>
                    <option value="AK">Alaska</option>
                    <option value="AZ">Arizona</option>
                    <option value="AR">Arkansas</option>
                    <option value="CA">California</option>
                    <option value="CO">Colorado</option>
                    <option value="CT">Connecticut</option>
                    <option value="DE">Delaware</option>
                    <option value="DC">District Of Columbia</option>
                    <option value="FL">Florida</option>
                    <option value="GA">Georgia</option>
                    <option value="HI">Hawaii</option>
                    <option value="ID">Idaho</option>
                    <option value="IL">Illinois</option>
                    <option value="IN">Indiana</option>
                    <option value="IA">Iowa</option>
                    <option value="KS">Kansas</option>
                    <option value="KY">Kentucky</option>
                    <option value="LA">Louisiana</option>
                    <option value="ME">Maine</option>
                    <option value="MD">Maryland</option>
                    <option value="MA">Massachusetts</option>
                    <option value="MI">Michigan</option>
                    <option value="MN">Minnesota</option>
                    <option value="MS">Mississippi</option>
                    <option value="MO">Missouri</option>
                    <option value="MT">Montana</option>
                    <option value="NE">Nebraska</option>
                    <option value="NV">Nevada</option>
                    <option value="NH">New Hampshire</option>
                    <option value="NJ">New Jersey</option>
                    <option value="NM">New Mexico</option>
                    <option value="NY">New York</option>
                    <option value="NC">North Carolina</option>
                    <option value="ND">North Dakota</option>
                    <option value="OH">Ohio</option>
                    <option value="OK">Oklahoma</option>
                    <option value="OR">Oregon</option>
                    <option value="PA">Pennsylvania</option>
                    <option value="RI">Rhode Island</option>
                    <option value="SC">South Carolina</option>
                    <option value="SD">South Dakota</option>
                    <option value="TN">Tennessee</option>
                    <option value="TX">Texas</option>
                    <option value="UT">Utah</option>
                    <option value="VT">Vermont</option>
                    <option value="VA">Virginia</option>
                    <option value="WA">Washington</option>
                    <option value="WV">West Virginia</option>
                    <option value="WI">Wisconsin</option>
                    <option value="WY">Wyoming</option>
                </select>
            </div>
        </div>
        <div class="mb-3 row">
            <label for="postalcode" class="col-sm-2 col-form-label">Club Zip</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="postalcode" id="postalcode" placeholder="55555"
                       value="{{ old('postalcode') }} @isset($group){{$group->postalcode }} @endisset">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="phone" class="col-sm-2 col-form-label">Club Phone</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="phone" id="phone" placeholder="555-555-5555"
                       value="{{ old('phone') }} @isset($group){{$group->phone }} @endisset">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="email" class="col-sm-2 col-form-label">Club Email</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="email" id="email" placeholder="club@club.com"
                       value="{{ old('email') }} @isset($group){{$group->email }} @endisset">
            </div>
        </div>

    </div>
</div>
<!-- Submit Form Input -->
<div class="form-group mt-3">
    <button type="submit" class="btn btn-primary form-control">Save</button>
</div>
