@extends('layouts.groups.main')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')
        @if(intval(auth()->user()->userManageLevel($group->id))  > 2)
            <li class="breadcrumb-item"><a href="{{ route('group.create_election', ['group' => $group->id]) }}">Create
                    Election</a></li>
        @endif

    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <h4>{{$issue->issue}}</h4>

            <form action="{{ route('group.save_issue_vote') }}" method="post">
                @csrf
                <input type="hidden" name="issue_id" value="{{ $issue->id }}">
                <input type="hidden" name="voter" value="{{ auth()->user()->id }}">
                <div class="col-md-4 card-body">
                    @if($issue_voted_on === false)
                        <div class="mb-3 form-check">
                            <input class="form-check-input" type="checkbox" value="1" id="vote_yes" name="vote[]">
                            <label class="form-check-label" for="vote_yes">
                                Vote For
                            </label>
                        </div>
                        <div class="mb-3 form-check">
                            <input class="form-check-input" type="checkbox" value="0" id="vote_no" name="vote[]">
                            <label class="form-check-label" for="vote_no">
                                Vote Against
                            </label>
                        </div>
                    @else
                        <p>Thank You For Voting</p>
                    @endif
                        <button type="submit" class="btn btn-primary form-control">Vote</button>

                </div>

            </form>

            <hr class="mt-3" />
            <h4>Results</h4>

            <div class="row">
                @if(!empty($groupwithcount))
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-success text-white mb-4">
                            <div class="card-body">For {{$groupwithcount[1]['for']}}</div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-danger text-white mb-4">
                            <div class="card-body">Against  {{$groupwithcount[0]['against']}}</div>
                        </div>
                    </div>
                @endif
            </div>

            @if($issue->transparency == 1)
                <div class="table-responsive">
                    <table class="table table-hover table-striped datatable">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Vote</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Vote</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($votes as $voter)
                            <tr>
                                <td class="text-start alert">
                                    <span>
                                        {{ $voter->user->first_name }}{{ $voter->user->last_name }} / {{ $voter->user->name }}
                                    </span>
                                </td>
                                <td class="text-end @if($voter->vote == 0)text-danger @else text-success @endif">
                                    {{ Config::get('constants.translate_vote')[$voter->vote] }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            @else

            @endif

        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>

@endsection
