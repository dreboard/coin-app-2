@extends('layouts.groups.main')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        @include('groups.group.partials.breadcrumb')

    </ol>

    <p><span class="fw-bold">Founder: </span> <a href="{{ route('group.view_user', ['group' => $group->id, 'user_id' => $founder->user_id]) }}">
            {{ $founder->user_first_name }} {{ $founder->user_last_name }} / {{ $founder->user_name }}
        </a>
    </p>

    <a href="{{ route('group.view_positions', ['group' => $group->id]) }}">Assign Positions</a>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Position</th>
            <th scope="col">Name</th>
            <th scope="col">Since</th>
            <th scope="col">Expires</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">President</th>
            @isset($board_members[1])
                <td><a href="{{ route('group.view_user', ['group' => $group->id, 'user_id' => $board_members[1]->board_member_user_id]) }}">
                        {{$board_members[1]->board_member_first_name}} {{$board_members[1]->board_member_last_name}}
                    </a>
                </td>
                <td>{{Carbon\Carbon::parse($board_members[1]->position_approved_at)->format('F jS Y')}}</td>
                <td>{{Carbon\Carbon::parse($board_members[1]->position_expired_at)->format('F jS Y')}}</td>
            @else
                <td>Position Vacant @if($user_manage_level > 2)<a href="">Assign</a>@endif </td>
                <td></td>
                <td></td>
            @endisset

        </tr>
        <tr>
            <th scope="row">Vice President</th>
            @isset($board_members[2])
                <td><a href="{{ route('group.view_user', ['group' => $group->id, 'user_id' => $board_members[2]->board_member_user_id]) }}">
                        {{$board_members[2]->board_member_first_name}} {{$board_members[2]->board_member_last_name}}
                    </a>
                </td>
                <td>{{Carbon\Carbon::parse($board_members[2]->position_approved_at)->format('F jS Y')}}</td>
                <td>{{Carbon\Carbon::parse($board_members[2]->position_expired_at)->format('F jS Y')}}</td>
            @else
                <td>Position Vacant @if($user_manage_level > 2)<a href="">Assign</a>@endif </td>
                <td></td>
                <td></td>
            @endisset
        </tr>
        <tr>
            <th scope="row">2nd Vice President</th>
            @isset($board_members[3])
                <td><a href="{{ route('group.view_user', ['group' => $group->id, 'user_id' => $board_members[3]->board_member_user_id]) }}">
                        {{$board_members[3]->board_member_first_name}} {{$board_members[3]->board_member_last_name}}
                    </a>
                </td>
                <td>{{Carbon\Carbon::parse($board_members[3]->position_approved_at)->format('F jS Y')}}</td>
                <td>{{Carbon\Carbon::parse($board_members[3]->position_expired_at)->format('F jS Y')}}</td>
            @else
                <td>Position Vacant <a class="" href="">Assign</a> </td>
                <td></td>
                <td></td>
            @endisset
        </tr>
        <tr>
            <th scope="row">Treasurer</th>
            @isset($board_members[4])
                <td><a href="{{ route('group.view_user', ['group' => $group->id, 'user_id' => $board_members[4]->board_member_user_id]) }}">
                        {{$board_members[4]->board_member_first_name}} {{$board_members[4]->board_member_last_name}}
                    </a>
                </td>
                <td>{{Carbon\Carbon::parse($board_members[4]->position_approved_at)->format('F jS Y')}}</td>
                <td>{{Carbon\Carbon::parse($board_members[4]->position_expired_at)->format('F jS Y')}}</td>
            @else
                <td>Position Vacant <a href="">Assign</a> </td>
                <td></td>
                <td></td>
            @endisset

        </tr>
        <tr>
            <th scope="row">Secretary</th>
            @isset($board_members[5])
                <td><a href="{{ route('group.view_user', ['group' => $group->id, 'user_id' => $board_members[5]->board_member_user_id]) }}">
                        {{$board_members[1]->board_member_first_name}} {{$board_members[5]->board_member_last_name}}
                    </a>
                </td>
                <td>{{Carbon\Carbon::parse($board_members[5]->position_approved_at)->format('F jS Y')}}</td>
                <td>{{Carbon\Carbon::parse($board_members[5]->position_expired_at)->format('F jS Y')}}</td>
            @else
                <td>Position Vacant @if($user_manage_level > 2)<a href="">Assign</a>@endif </td>
                <td></td>
                <td></td>
            @endisset
        </tr>
        </tbody>
    </table>


    <div class="table-responsive">
        <table id="user_datatable" class="table datatable">
            <thead>
            <tr>
                <th>Name</th>
                <th>Member Type</th>
                <th>Joined</th>
                <th>View</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Name</th>
                <th>Member Type</th>
                <th>Joined</th>
                <th>View</th>
            </tr>
            </tfoot>
            <tbody>
            @foreach ($members as $member)
                <tr>
                    <td class="text-start">{{ optional($member)->user_first_name }} {{ optional($member)->user_last_name }} | {{ $member->name }}</td>
                    <td class="text-start">
                        <a href="{{ route('group.view_type', ['group' => $group->id, 'type' => $member->member_type]) }}">{{ ucfirst($member->member_type) }}</a>
                         / {{ $member->club_position }}
                    </td>
                    <td class="text-start">{{ Carbon\Carbon::parse($member->approved_at)->format('F jS Y') }}</td>
                    <td class="text-start">
                        <a href="{{ route('group.view_user', ['group' => $group->id, 'user_id' => $member->user_id]) }}">Profile</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
