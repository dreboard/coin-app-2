@extends('layouts.groups.main')
@section('pageTitle', 'Group User Profile')
@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        @include('groups.group.partials.breadcrumb')
        <li class="breadcrumb-item"><a href="{{ route('group.view_users', ['group' => $group->id]) }}">Members</a> </li>
    </ol>

    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <!-- Post content-->
                <article>
                    <!-- Post header-->
                    <header class="mb-4">
                        <!-- Post title-->
                        <h3 class="fw-bolder mb-1">{{ $user[0]->user_first_name }} {{ $user[0]->user_last_name }} | {{ $user[0]->user_name }}</h3>
                        <!-- Post meta content-->
                        <div class="text-muted fst-italic mb-2">Joined on {{ Carbon\Carbon::parse($user[0]->the_group_start_date)->format('F jS Y') }}</div>
                        <!-- Post categories-->
                        <a class="badge bg-primary text-decoration-none link-light" href="{{ route('group.view_users', ['group' => $group->id]) }}">
                            {{ ucfirst($user[0]->member_type) }}
                        </a>
                        <a class="badge bg-secondary text-decoration-none link-light" href="{{ route('group.view_users', ['group' => $group->id]) }}">
                            {{ ucfirst($user[0]->user_type) }}
                        </a>

                        @isset($roles)
                            @foreach ($roles as $role)
                                <a class="badge bg-primary text-decoration-none link-light" href="{{ route('group.view_users', ['group' => $group->id]) }}">
                                    {{ ucfirst($role->role_name) }}
                                </a>
                            @endforeach
                        @endisset

                    </header>



                <section class="mb-5">
                    <div class="card bg-light">
                        <div class="card-body">
                            <!-- Comment form-->
                            <h3>Send Message</h3>
                            <form method="post" action="{{ route('group.view_users', ['group' => $group->id]) }}" class="mb-4">
                                @csrf
                                <input type="hidden" name="sender" value="{{ auth()->user()->id }}" />
                                <input type="hidden" name="recipient" value="{{ $user[0]->the_users_id }}" />

                                <div class="mb-3">
                                    <label for="exampleFormControlInput1" class="form-label">Subject</label>
                                    <input type="text" name="subject" class="form-control" id="exampleFormControlInput1">
                                </div>
                                <div class="mb-3">
                                    <label for="exampleFormControlTextarea1" class="form-label">Message</label>
                                    <textarea class="form-control" name="body" id="exampleFormControlTextarea1" rows="3"></textarea>
                                </div>
                                <button class="btn btn-primary" type="submit">Send</button>
                            </form>

                        </div>
                    </div>
                </section>
            </div>
            <!-- Side widgets-->
            <div class="col-lg-4">
                <!-- Categories widget-->
                <div class="card mb-4">
                    <div class="card-header">View</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.public', ['id' => auth()->user()->id]) }}">Public Profile</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="#!">Collection</a></li>
                                </ul>
                            </div>

                            @if(intval(auth()->user()->userManageLevel($group->id))  > 2)

                                    <div class="col-8 mt-3">

                                        <span id="result_span"></span>
                                        <select class="form-select member_positions" id="member_manage_level"
                                                data-user="{{ $user[0]->the_users_id }}" name="manage">
                                            <option>Assign A Manage Level</option>
                                            <option value="1" @if($user[0]->manage == 1) selected @endif>1 Member Level</option>
                                            <option value="2" @if($user[0]->manage == 2) selected @endif>2 Staff Level</option>
                                            <option value="3"@if($user[0]->manage == 3) selected @endif>3 Director Level</option>
                                            <option value="4"@if($user[0]->manage == 4) selected @endif>4 Executive Level</option>
                                        </select>
                                    </div>


                                @if(!in_array($user[0]->club_position, \App\Helpers\ManageGroupHelper::GROUP_POSITIONS))
                                    <form class="row row-cols-lg-auto g-3 align-items-center"
                                          action="{{ route('group.user_change_position') }}" method="post">
                                        @csrf
                                        <input id="member_id_{{ $user[0]->the_users_id }}" type="hidden" name="user_id" value="{{ $user[0]->the_users_id }}">
                                        <input type="hidden" name="group_id" value="{{ $group->id }}">
                                        <div class="col-6">
                                            <label class="visually-hidden" for="member_position_{{ $user[0]->the_users_id }}">Preference</label>
                                            <select class="form-select member_positions" id="member_position_{{ $user[0]->the_users_id }}"
                                                    data-user="{{ $user[0]->the_users_id }}" name="role_id">
                                                <option selected>Choose A Title</option>
                                                @foreach ($positions as $position)
                                                    @if($user[0]->club_position !== $position->name)
                                                        <option value="{{ $position->id }}">{{ $position->name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-6">
                                            <button type="submit" class="btn btn-primary">Assign Position</button>
                                        </div>
                                    </form>
                                @else
                                    @isset($roles)
                                        @foreach ($roles as $role)
                                            <a class="badge bg-primary text-decoration-none link-light" href="{{ route('group.view_users', ['group' => $group->id]) }}">
                                                {{ ucfirst($role->role_name) }}
                                            </a>
                                        @endforeach
                                    @endisset
                                @endif


                            @endif
                            @if(intval(auth()->user()->userManageLevel($group->id))  > 2)
                                <div class="col-sm-12 mt-3">
                                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#exampleModal">Remove User</button>
                                </div>
                            @endif
                            <!-- removal modal -->
                            <div class="modal" tabindex="-1" id="exampleModal">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="{{ route('group.remove_user') }}" method="post">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Remove</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <p>You are about to remove {{ $user[0]->user_name }} from this group</p>
                                                @csrf
                                            <input type="hidden" name="user_id" value="{{ $user[0]->the_users_id }}" />
                                            <input type="hidden" name="group_id" value="{{ $group->id }}" />
                                            <input type="hidden" name="remove_type" value="group" />
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-danger">Remove User</button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            (function (ENVIRONMENT) {
                try {
                    let result_span = document.getElementById('result_span');
                    let member_manage_level = document.getElementById('member_manage_level');
                        member_manage_level.addEventListener('change', function () {

                            result_span.innerHTML = '';
                            console.log(this.value);
                            fetch('{{ route('group.user_manage_level') }}', {
                                headers: {"Content-Type": "application/json; charset=utf-8"},
                                method: 'POST',
                                body: JSON.stringify({
                                    _token: @json(csrf_token()),
                                    manage: this.value,
                                    user_id: {{ $user[0]->the_users_id }},
                                    group_id: {{ $group->id }},
                                })
                            }).then(response => response.json())
                                .then(data => {
                                    console.error(data);
                                    if (data.result == 'Success') {
                                        result_span.innerHTML = 'Updated';
                                        result_span.classList.add('text-success')
                                    } else {
                                        result_span.classList.add('text-danger');
                                        result_span.innerHTML = 'Error In saving';
                                    }
                                })
                        });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
@if(config('app.env') == 'local')
{{--{{ dd($user[0]) }}--}}
@endif
