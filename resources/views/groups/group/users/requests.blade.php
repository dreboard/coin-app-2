@extends('layouts.groups.main')
@section('title', 'Manage Club')
@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        @include('groups.group.partials.breadcrumb')

    </ol>
    <div>
        @if(count($requests) == 0)
            <h4>No Requests To Process</h4>
        @else
        <div class="col-md-12 mb-5">

            <div class="row">
                <div class="col-8">
                    <h4>Process All Requests</h4>
                    <form class="row row-cols-lg-auto g-3 align-items-center"
                          action="{{ route('group.request_process_all') }}"
                          method="post">
                        @csrf
                        <input type="hidden" name="group_id" value="{{ $group->id }}">
                        <div class="col-12">
                            <label class="visually-hidden" for="inlineFormSelectPref">Preference</label>
                            <select class="form-select" name="member_type">
                                <option selected>Choose...</option>
                                <option value="deny">Deny All Memberships</option>
                                <option value="associate">Make All Associate Members</option>
                                <option value="full">Make All Full Members</option>
                            </select>
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary">Process</button>
                        </div>
                        <div class="col-12">
                            <span>All in one shot</span>
                        </div>
                    </form>
                </div>
                <div class="col-4">
                    @if($member_director == 'None')
                        <p>No Membership Director Assigned</p>
                        <form class="row row-cols-lg-auto g-3 align-items-center"
                              action="{{ route('group.assign_director') }}"
                              method="post">
                            @csrf
                            <input type="hidden" name="group_id" value="{{ $group->id }}">
                            <input type="hidden" name="role_id" value="{{ $director }}">
                            <div class="col-12">
                                <label class="visually-hidden" for="inlineFormSelectPref">Preference</label>
                                <select class="form-select" name="user_id">
                                    <option selected>Choose A Member</option>
                                    @foreach ($users as $user)
                                        <option value="{{ $user->uid }}">{{ $user->last_name }} {{ $user->first_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary">Assign</button>
                            </div>
                        </form>
                    @else
                        <p><span class="fw-bold"> Membership Director is: </span>{{$member_director->uname}}</p>
                    @endif

                </div>
            </div>



        </div>
        <h4>Process Individual Requests</h4>
        <div class="col-md-12">
            <table id="user_datatable" class="table datatable">
                <thead>
                <tr>
                    <th>Request #</th>
                    <th>User</th>
                    <th>Date Requested</th>
                    <th>Action</th>
                    <th>View</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Request #</th>
                    <th>User</th>
                    <th>Date Requested</th>
                    <th>Action</th>
                    <th>View</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach ($requests as $request)
                    <tr>
                        <td class="text-start">{{ $request->gid }}</td>
                        <td class="text-start">{{ $request->uname }}</td>
                        <td class="text-start">{{ Carbon\Carbon::parse($request->gr_created_at)->format('F jS Y, g:ia') }}</td>
                        <td class="text-start">
                            <form class="row row-cols-lg-auto g-3 align-items-center"
                                  action="{{ route('group.request_process') }}" method="post">
                                @csrf
                                <input type="hidden" name="uid" value="{{ $request->uid }}">
                                <input type="hidden" name="gid" value="{{ $request->gid }}">
                                <div class="col-12">
                                    <label class="visually-hidden" for="inlineFormSelectPref">Preference</label>
                                    <select class="form-select" name="member_type">
                                        <option selected>Choose...</option>
                                        <option value="deny">Deny Membership</option>
                                        <option value="associate">Associate Member</option>
                                        <option value="full">Full Member</option>
                                        <option value="executive">Executive Member</option>
                                    </select>
                                </div>
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary">Process</button>
                                </div>
                            </form>
                        </td>
                        <td class="text-start"><a href="{{ route('group.view', ['group' => $request->gid]) }}">View Request</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @endif
    </div>
@endsection
