@extends('layouts.groups.main')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')

    </ol>
    <h5>Assign Users By Position</h5>
    <a href="{{ route('group.view_user_positions', ['group' => $group->id]) }}">Assign Positions (By User)</a>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Position</th>
            <th scope="col">Name</th>
            <th scope="col">Term Start</th>
            <th scope="col">Term End</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($roles as $role => $r)


            <tr>
                <th scope="row">{{ $role }}</th>
                @isset($r['user_info'][0])
                    <td>

                        <div class="row">
                            <div class="col-6">
                                <a href="{{ route('group.view_user', ['group' => $group->id, 'user_id' => $r['user_info'][0]->uid]) }}">
                                    {{ $r['user_info'][0]->uname}} {{ $r['user_info'][0]->uname}}
                                </a>
                            </div>
                            <div class="col-6">
                                @if(intval(auth()->user()->userManageLevel($group->id))  > 2)
                                    <form class="row row-cols-lg-auto g-3 align-items-end"
                                          action="{{ route('group.user_change_position') }}" method="post">
                                        @csrf
                                        <input type="hidden" name="role_id" value="{{ $r['role_id'] }}">
                                        <input type="hidden" name="group_id" value="{{ $group->id }}">
                                        <div class="col-6 position-end">
                                            <button type="submit" class="btn btn-danger">Remove From Office</button>
                                        </div>
                                    </form>
                                @endif
                            </div>
                        </div>

                    </td>
                    <td>{{ Carbon\Carbon::parse($r['user_info'][0]->role_approved_at)->format('F jS Y, g:ia') ?? '----' }}</td>
                    <td>{{ Carbon\Carbon::parse($r['user_info'][0]->role_expired_at)->format('F jS Y, g:ia') ?? '----'}}</td>
                @else
                    <td>
                        <form class="row row-cols-lg-auto g-3 align-items-center"
                              action="{{ route('group.user_change_position') }}" method="post">
                            @csrf
                            <input type="hidden" name="role_id" value="{{ $r['role_id'] }}">
                            <input type="hidden" name="group_id" value="{{ $group->id }}">
                            <div class="col-6">
                                <label class="visually-hidden" for="member_position">Preference</label>
                                @if(in_array($role, \App\Helpers\ManageGroupHelper::MULTIPLE_MEMBER_POSITIONS))
                                    <a>Assign Multiple Users{{ $r['role_id'] }}</a>

                                @else
                                    <select class="form-select" id="user_id" name="user_id">
                                        <option selected>Position Vacant</option>
                                        @foreach ($members as $member)
                                            <option
                                                value="{{ $member->uid }}">{{ $member->first_name }}  {{ $member->last_name }}
                                                | {{ $member->uname }}</option>
                                        @endforeach
                                    </select>
                                @endif

                            </div>
                            @if(in_array($role, \App\Helpers\ManageGroupHelper::MULTIPLE_MEMBER_POSITIONS))


                            @else
                                <div class="col-6">
                                    <button type="submit" class="btn btn-primary">Assign Position</button>
                                </div>
                            @endif

                        </form>
                    </td>
                    <td></td>
                    <td></td>
                @endisset


            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
