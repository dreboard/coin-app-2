@extends('layouts.groups.main')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        @include('groups.group.partials.breadcrumb')
        <li class="breadcrumb-item"><a href="{{ route('group.view_users', ['group' => $group->id]) }}">Members</a> </li>
    </ol>

    <a href="{{ route('group.view_positions', ['group' => $group->id]) }}">Assign Positions</a>

    <div class="table-responsive">
        <table id="user_datatable" class="table datatable">
            <thead>
            <tr>
                <th>Name</th>
                <th>Member Type</th>
                <th>Joined</th>
                <th>View</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Name</th>
                <th>Member Type</th>
                <th>Joined</th>
                <th>View</th>
            </tr>
            </tfoot>
            <tbody>
            @foreach ($members as $member)
                <tr>
                    <td class="text-start">
                        <a href="{{ route('group.view_user', ['group' => $group->id, 'user_id' => $member->user_id]) }}">
                            {{ optional($member)->user_first_name }} {{ optional($member)->user_last_name }} | {{ $member->name }}
                        </a>
                    </td>
                    <td class="text-start">{{ ucfirst($member->member_type) }} / {{ $member->club_position }}</td>
                    <td class="text-start">{{ Carbon\Carbon::parse($member->approved_at)->format('F jS Y') }}</td>
                    <td class="text-start">
                        <a href="{{ route('group.view_user', ['group' => $group->id, 'user_id' => $member->user_id]) }}">Profile</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
