@extends('layouts.groups.main')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')

    </ol>
    <div class="mb-3">
        <h4>Assign Positions By User</h4>
        <a href="{{ route('group.positions', ['group' => $group->id]) }}">Assign Positions (By Role)</a>
        <span id="updateResponse"></span>
    </div>


    <div class="table-responsive">
        <table id="user_datatable" class="table table-hover datatable">
            <thead>
            <tr class="fw-bold">
                <th class="fw-bold">Member</th>
                <th class="fw-bold">Current Position</th>
                <th class="fw-bold">Add/Change</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th class="fw-bold">Member</th>
                <th class="fw-bold">Current Position</th>
                <th class="fw-bold">Add/Change</th>
            </tr>
            </tfoot>
            <tbody>
{{--            {{ dd($members) }}--}}
            @foreach ($members as $member)
                <tr>
                    <td class="text-start">
                        <a href="{{ route('group.view_user', ['group' => $group->id, 'user_id' => $member->uid]) }}">
                            {{ optional($member)->first_name ?? '' }}  {{ optional($member)->last_name }} | {{ $member->name }}
                        </a>
                    </td>
                    <td class="text-start"><span
                            id="current_position_{{ $member->uid }}
                            "data-user="{{ $member->uid }}"
                            class="current_positions">{{--{{ $member->club_position }}--}} {{ \App\Helpers\ManageGroupHelper::GROUP_POSITION_NUMBERS[$member->role_name] ?? 'None' }}</span>
                    </td>
                    <td class="text-end">
                        <form class="row row-cols-lg-auto g-3 align-items-center"
                              action="{{ route('group.user_change_position') }}" method="post">
                            @csrf
                            <input id="member_id_{{ $member->uid }}" type="hidden" name="user_id" value="{{ $member->uid }}">
                            <input type="hidden" name="group_id" value="{{ $member->gid }}">
                            <div class="col-6">
                                <label class="visually-hidden" for="member_position_{{ $member->uid }}">Preference</label>
                                <select class="form-select member_positions" id="member_position_{{ $member->uid }}"
                                        data-user="{{ $member->uid }}" name="role_id">
                                    <option selected>Choose A Title</option>
                                    @foreach ($positions as $position)
                                        <option value="{{ $position->id }}">{{ $position->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-6">
                                <button type="submit" class="btn btn-primary">Assign Position</button>
                            </div>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    @push('scripts')
        <script>
            (function() {
                try{

                    // https://stackoverflow.com/questions/3871547/iterating-over-result-of-getelementsbyclassname-using-array-foreach
                    /*Array.from(els).forEach((el) => {
                        // Do stuff here
                        console.log(el.tagName);
                    });

                    var els = document.getElementsByClassName("current_positions");

                    Array.prototype.forEach.call(els, function(el) {
                        try{
                            const user_id = el.dataset.user;
                            const updateResponse = document.getElementById("updateResponse");
                            updateResponse.innerHTML = 'Processing';
                            updateResponse.classList.add('text-danger');
                            fetch('http://localhost/example-app/public/group/get_user_roles', {
                                headers: { "Content-Type": "application/json; charset=utf-8" },
                                method: 'POST',
                                body: JSON.stringify({
                                    _token: @json(csrf_token()),
                                    group_id: {{ $group->id }},
                                    user_id: user_id,
                                    //position: this.value
                                })
                            }).then(response => response.json())
                                .then(data => {
                                    //var jsonData = JSON.parse(data);
                                    // console.log(data);
                                    // console.log(data.body);
                                    // console.log(data.message);
                                    // console.log(data.errors);
                                    var role = JSON.stringify(data);
                                    var role2 = JSON.stringify(data);


                                    console.log(role[0]);
                                    if (role) {
                                        //console.log(role);
                                        //el.innerHTML = role;
                                    }

                                    updateResponse.classList.remove('text-danger');
                                    updateResponse.classList.add('text-success');
                                    updateResponse.innerHTML = 'Done';
                                })
                        }catch (error){
                            if (ENVIRONMENT === 'local'){
                                console.error(error);
                            }
                        }
                    });

                    function getCurrentUserPosition(){

                        console.log('fired');
                        var updateResponse = document.getElementById("updateResponse");
                        updateResponse.innerHTML = 'Processing';
                        updateResponse.classList.add('text-danger');
                        fetch(SITE_URL+'/group/get_user_roles', {
                            headers: { "Content-Type": "application/json; charset=utf-8" },
                            method: 'POST',
                            body: JSON.stringify({
                                _token: @json(csrf_token()),
                                group_id: {{ $group->id }},
                                user_id: 1, //this.dataset.user,
                                //position: this.value
                            })
                        }).then(response => response.json())
                            .then(data => {
                                updateResponse.classList.add('text-success');
                                updateResponse.innerHTML = 'Done';

                                console.log('Success:', data);
                                console.log(updateResponse.innerHTML);
                            })
                        //.then(data => console.log(data))
                    }


                    function getUser()
                    {
                        const user_id = document.getElementById("member_id_"+this.dataset.user);
                        const position_id = this.value;
                        console.log('position '+ position_id, 'user '+ this.dataset.user);

                    };

                    const member_positions = document.getElementsByClassName('member_positions');

                    for (i = 0; i < member_positions.length; i++) {
                        member_positions[i].addEventListener("change", getUser);
                    }
                    function getData(){
                        var updateResponse = document.getElementById("updateResponse");
                        updateResponse.innerHTML = 'Processing';
                        updateResponse.classList.add('text-danger');
                        fetch(SITE_URL+'/group/user_change_position', {
                            headers: { "Content-Type": "application/json; charset=utf-8" },
                            method: 'POST',
                            body: JSON.stringify({
                                _token: @json(csrf_token()),
                                group_id: {{ $group->id }},
                                user_id: this.dataset.user,
                                position: this.value
                            })
                        }).then(response => response.json())
                            .then(data => {
                                updateResponse.classList.add('text-success');
                                updateResponse.innerHTML = 'Done';

                                console.log('Success:', data);

                            })
                        //.then(data => console.log(data))
                    }



                    for (var i = 0 ; i < member_positions.length; i++) {
                        member_positions[i].addEventListener('change' , function handleChange(event) {
                            const user_id = document.getElementById("member_id_"+this.dataset.user);
                            const position_id = this.value;
                            //console.log('position '+ position_id, 'user '+ user_id.id);

                            //console.log(event.target.value, user_id); // get selected VALUE

                            // get selected VALUE even outside event handler
                            //console.log(member_positions.options[member_positions.selectedIndex].value);

                            // get selected TEXT in or outside event handler
                            //console.log(member_positions.options[member_positions.selectedIndex].text);
                        });
                    }






                    console.log(SITE_URL);
                    // console.log(ENVIRONMENT);
                    // console.log(CSRF_TOKEN);
                    // console.log(USER_ID);


                }catch (error){
                    if (ENVIRONMENT === 'local'){
                        console.error(error);
                    }
                }
            })();

        </script>

    @endpush
@endsection
