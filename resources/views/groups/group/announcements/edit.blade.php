@extends('layouts.groups.main')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        @include('groups.group.partials.breadcrumb')

    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <h4>Create Announcement</h4>
            <form action="{{ route('group.update_announcement') }}" method="post">
                @csrf
                <div class="col-md-12">
                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                    <input type="hidden" name="group_id" value="{{ $group->id }}">
                    <input type="hidden" name="announcement_id" value="{{ $announcement->id }}">
                    <div class="form-group mb-3">
                        <label class="control-label">Message</label>
                        <textarea id="message" name="message" class="form-control">{{ $announcement->message }}</textarea>
                    </div>

                    <!-- Submit Form Input -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary form-control">Update</button>
                    </div>
                </div>
            </form>

        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>

@endsection
