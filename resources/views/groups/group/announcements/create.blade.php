@extends('layouts.groups.main')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        @include('groups.group.partials.breadcrumb')

    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <h4>Create Announcement</h4>
            <form action="{{ route('group.save_announcement') }}" method="post">
                @csrf
                <div class="col-md-12">
                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                    <input type="hidden" name="group_id" value="{{ $group->id }}">
                    <div class="form-group mb-3">
                        <label class="control-label">Message</label>
                        <textarea id="editor" name="message" class="form-control">{{ old('message') }}</textarea>
                    </div>

                    <!-- Submit Form Input -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary form-control">Create</button>
                    </div>
                </div>
            </form>

        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>

    @push('scripts')
        <script src="https://cdn.ckeditor.com/ckeditor5/35.2.1/classic/ckeditor.js"></script>
        <script>
            (function () {
                try {
                    ClassicEditor
                        .create( document.querySelector( '#editor' ) )
                        .catch( error => {
                            console.error( error );
                        } );
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
