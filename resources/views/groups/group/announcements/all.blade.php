@extends('layouts.groups.main')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        @include('groups.group.partials.breadcrumb')


    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <div class="col-lg-12">
                <h5>Club Announcements</h5>
                <div class="mb-3">
                    <a class="btn btn-primary" href="{{ route('group.create_announcement', ['group' => $group->id]) }}">Create Announcement</a>
                </div>

                @foreach ($announcements as $announcement)
                    <!-- Blog post-->
                    <div class="card mb-4">
                        <div class="card-body">
                            <div
                                class="small text-muted">Created by {{ $announcement->user->name }}: {{ Carbon\Carbon::parse($announcement->created_at)->format('F jS Y') }}</div>
                            <p class="card-text">{{ $announcement->message }}</p>
                            <a class="btn btn-primary" href="{{ route('group.edit_announcement', ['group' => $group->id, 'announcement' => $announcement->id]) }}">Edit</a>
                        </div>
                    </div>
                    <!-- Blog post-->
                @endforeach
            </div>
        </div>

        @include('groups.group.partials.side')
    </div>
@endsection
