@extends('layouts.groups.main')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')


    </ol>
    <div class="row">
        <div class="col-lg-8">
            <div class="col-lg-6">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="small text-muted">{{ Carbon\Carbon::parse($announcement->created_at)->format('F jS Y') }}</div>
                        <p class="card-text">{{ $announcement->message }}</p>

                        <a class="btn btn-primary" href="{{ route('group.edit_announcement') }}">Edit</a>

                    </div>
                </div>
            </div>
        </div>

        @include('groups.group.partials.side')
    </div>
@endsection
