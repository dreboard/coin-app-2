@extends('layouts.groups.main')
@section('title', 'Manage Club')
@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        @include('groups.group.partials.breadcrumb')

    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <h4>Group Management Request</h4>
            <form action="{{ route('group.manage_request_save') }}" method="post">
                @csrf
                <input type="hidden" name="group_id" value="{{ $group->id }}">
                <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                <div class="col-md-6">
                    <!-- Subject Form Input -->
                    <div class="form-group mb-3">
                        <label class="control-label">Level Requested</label>
                        <select class="form-select member_positions" name="manage">
                            <option selected>Choose....</option>
                            @foreach (Config::get('constants.manage_levels') as $k => $position)
                                <option value="{{ $position }}">{{ $k }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mt-3">
                        <button type="submit" class="btn btn-primary form-control">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>

@endsection
