@extends('layouts.groups.main')
@section('title', 'Edit Club')
@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        @include('groups.group.partials.breadcrumb')

    </ol>
    <div class="row">

        <div class="col-lg-8">

            <div>
                <form action="{{ route('group.store_club') }}" method="post" id="groupForm" enctype="multipart/form-data">
                    @include('groups.group.partials.group_form', ['create' => true])
                </form>
            </div>


        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>

@endsection
