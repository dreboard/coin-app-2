@extends('layouts.groups.main')
@section('title', 'Manage Club')
@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        @include('groups.group.partials.breadcrumb')

    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <h4>Group Management</h4>
            <table class="table table-hover table-striped">
                <tr>
                    <th class="w-15 text-center">Level Req.</th>
                    <th class="text-start w-75">Action</th>
                </tr>
                <tr>
                    <th class="w-15 text-center">1</th>
                    <td class="text-start"><a class=""
                                              href="{{ route('group.manage_levels', ['group' => $group->id]) }}">Who Can Manage What</a></td>
                    <td class="text-end"> </td>
                </tr>
                <tr>
                    <th class="w-15 text-center">1</th>
                    <td class="text-start"><a class="" href="{{ route('group.manage_request', ['group' => $group->id]) }}">Assign Positions (By User)</a></td>
                    <td class="text-end"> </td>
                </tr>
                <tr>
                    <th class="w-15 text-center">2</th>
                    <td class="text-start">Edit Group Details</td>
                    <td class="text-end">
                        @if($user_manage_level  > 1)

                        @endif
                        <a class="btn btn-primary btn-sm" href="{{ route('group.edit', ['group' => $group->id]) }}">Go</a> </td>
                </tr>
                <tr>
                    <th class="w-15 text-center">2</th>
                    <td class="text-start">Edit Group Public Page</td>
                    <td class="text-end">
                        @if($user_manage_level  > 1)

                        @endif
                        <a class="btn btn-primary btn-sm"
                           href="{{ route('group.edit', ['group' => $group->id]) }}">Go</a> </td>
                </tr>
                <tr>
                    <th class="w-15 text-center">2</th>
                    <td>Assign Positions (By Role)</td>
                    <td class="text-end">
                        @if($user_manage_level  > 1)

                        @endif
                        <a class="btn btn-primary btn-sm"
                                            href="{{ route('group.positions', ['group' => $group->id]) }}">Go</a> </td>
                </tr>
                <tr>
                    <th class="w-15 text-center">2</th>
                    <td>Assign Positions (By User)</td>
                    <td class="text-end">
                        @if($user_manage_level  > 1)
                            <a class="btn btn-primary btn-sm"
                               href="{{ route('group.view_user_positions', ['group' => $group->id]) }}">Go</a>
                        @endif
                        </td>
                </tr>
                <tr>
                    <th class="w-15 text-center">2</th>
                    <td>Promote Associates</td>
                    <td class="text-end">
                        @if($user_manage_level  > 1)
                            <a class="btn btn-primary btn-sm"
                               href="{{ route('group.edit', ['group' => $group->id]) }}">Go</a>
                        @endif
                         </td>
                </tr>
                <tr>
                    <th class="w-15 text-center">2</th>
                    <td>Create Committee</td>
                    <td class="text-end">
                        @if($user_manage_level  > 2)
                            <a class="btn btn-primary btn-sm"
                               href="{{ route('group.edit', ['group' => $group->id]) }}">Go</a>
                        @endif
                        </td>
                </tr>
                <tr>
                    <th class="w-15 text-center">4</th>
                    <td>Transfer Ownership</td>
                    <td class="text-end">
                        @if($user_manage_level  > 2)
                            <a class="btn btn-primary btn-sm"
                               href="{{ route('group.transfer_user', ['group' => $group->id]) }}">Go</a>
                        @endif
                         </td>
                </tr>
                <tr>
                    <th class="w-15 text-center">4</th>
                    <td><span class="text-danger">Delete Group</span></td>
                    <td class="text-end">
                        @if(auth()->user()->id == $group->user_id)
                            <a class="btn btn-primary btn-sm"
                               href="{{ route('group.delete_group_page', ['group' => $group->id]) }}">Go</a>
                        @endif
                         </td>
                </tr>
            </table>
        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>

@endsection
