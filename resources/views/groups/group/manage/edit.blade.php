@extends('layouts.groups.main')
@section('pageTitle', $group->name.' Edit Group')
@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        @include('groups.group.partials.breadcrumb')

    </ol>
    <div class="row">

        <div class="col-lg-8">

            <div>
                <form id="edit_club_form" action="{{ route('group.save_group_edit') }}"
                      method="post" id="groupForm" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="group_id" value="{{ $group->id }}">
                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                    <input type="hidden" name="group_type" value="{{ $group->group_type }}">

                    <div class="form-group mb-3">
                        <label for="group_name" class="control-label fw-bold">Club Name</label>
                        <input type="text" class="form-control" name="group_name" placeholder="Club Name"
                               value="{{ old('name', $group->name) }}">
                    </div>

                    <div class="form-group mb-3">
                        <label for="group_name" class="control-label fw-bold">Private or Public Access</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="private" id="private1" value="1"
                                   @if($group->private == 1)  checked @endif />
                            <label class="form-check-label" for="private1">
                                Open (Immediate Approval)
                            </label>
                        </div>
                        <div class="form-check">
                            <input data-bs-toggle="modal" data-bs-target="#GroupTypeModal" class="form-check-input" type="radio"
                                   name="private" id="private2" value="0" @if($group->private == 0)  checked @endif />
                            <label class="form-check-label" for="private2">
                                Private (Requires Club Approval)
                            </label>
                        </div>
                    </div>

                    <div class="form-group mb-3">
                        <label for="specialty" class="control-label fw-bold">Specialty</label>
                        <select  name="specialty" id="specialty" class="form-select" aria-label="Select....">

                            <option>Choose....</option>
                            <option value="General Numismatics"@isset($group)  @if($group->specialty == 'General Numismatics')  selected @endif @endisset>General Numismatics</option>
                            <optgroup label="By Design">
                                <option value="Seated Liberty">Seated Liberty</option>
                                <option value="Barber">Barber</option>
                            </optgroup>
                            <optgroup label="By Denomination">
                                <option value="Silver Dollars">Silver Dollars</option>
                                <option value="Early American Coppers">Large/Small Cents</option>
                            </optgroup>
                            <option value="Error Collectors">Error Collectors</option>
                            <option value="Variety Collectors">Variety Collectors</option>
                        </select>
                    </div>

                    <div class="form-group mb-3">
                        <label for="image_url" class="control-label fw-bold">Club Logo</label>
                        <input class="form-control" type="file" name="image_url" id="image_url">
                    </div>

                    <div class="form-group mb-3">
                        <label for="formed" class="control-label fw-bold">Year Formed</label>
                        <input type="text" class="form-control" id="formed" name="formed" placeholder="2000"
                               value="{{ old('formed', $group->formed) }}">
                    </div>

                    <div class="form-group mb-3">
                        <label for="formed" class="control-label fw-bold">ANA Number</label>
                        <input type="text" class="form-control" id="ana" name="ana" placeholder="123456"
                               value="{{ old('ana', $group->ana) }}">
                    </div>

                    <div class="form-group mb-3">
                        <label for="meets" class="control-label fw-bold">Meeting Days</label>
                        <input type="text" class="form-control" id="meets" name="meets" placeholder="Every 3rd monday"
                               value="{{ old('meets', $group->meets) }}">
                    </div>

                    <div class="form-group mb-3">
                        <label for="url" class="control-label fw-bold">Website</label>
                        <input type="text" class="form-control" id="url" name="url" placeholder="www.myclub.com"
                               value="{{ old('url', $group->url) }}">
                    </div>

                    <div class="form-group mb-3">
                        <label for="short_description" class="control-label fw-bold">Short Description</label>
                        <textarea name="short_description" id="short_description" class="form-control">{{ old('short_description', $group->short_description) }}</textarea>
                    </div>

                    <div class="form-group mb-3">
                        <label for="description" class="control-label fw-bold">Description</label>
                        <textarea name="description" id="description" class="form-control" placeholder="Detailed Description">{{old('description', $group->description)}}</textarea>
                    </div>
                    @if(in_array(strtolower($group->group_type), ['club','association']))

                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title mb-2">Club Details </h5>
                            <span class="text-danger"><small>*All Required</small></span>
                            <div class="mb-3 row">
                                <label for="address" class="col-sm-2 col-form-label fw-bold">Club Address</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="address" id="address" placeholder="123 Main"
                                           value="{{ old('address', $group->address) }}">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="first_name" class="col-sm-2 col-form-label fw-bold">First Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First"
                                           value="{{ old('first_name', $group->first_name) }}">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="last_name" class="col-sm-2 col-form-label fw-bold">Last Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="last_name" id="last_name" placeholder="123 Main"
                                           value="{{ old('last_name', $group->last_name) }}">
                                </div>
                            </div>


                            <div class="mb-3 row">
                                <label for="city" class="col-sm-2 col-form-label fw-bold">Club City</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="city" id="city" placeholder="Subject"
                                           value="{{ old('city', $group->city) }}">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="state" class="col-sm-2 col-form-label fw-bold">Club State</label>
                                <div class="col-sm-10">
                                    <select class="form-select" aria-label="Default select example" name="state" id="state">
                                        <option selected>Choose...</option>
                                        <option value="AL">Alabama</option>
                                        <option value="AK">Alaska</option>
                                        <option value="AZ">Arizona</option>
                                        <option value="AR">Arkansas</option>
                                        <option value="CA">California</option>
                                        <option value="CO">Colorado</option>
                                        <option value="CT">Connecticut</option>
                                        <option value="DE">Delaware</option>
                                        <option value="DC">District Of Columbia</option>
                                        <option value="FL">Florida</option>
                                        <option value="GA">Georgia</option>
                                        <option value="HI">Hawaii</option>
                                        <option value="ID">Idaho</option>
                                        <option value="IL">Illinois</option>
                                        <option value="IN">Indiana</option>
                                        <option value="IA">Iowa</option>
                                        <option value="KS">Kansas</option>
                                        <option value="KY">Kentucky</option>
                                        <option value="LA">Louisiana</option>
                                        <option value="ME">Maine</option>
                                        <option value="MD">Maryland</option>
                                        <option value="MA">Massachusetts</option>
                                        <option value="MI">Michigan</option>
                                        <option value="MN">Minnesota</option>
                                        <option value="MS">Mississippi</option>
                                        <option value="MO">Missouri</option>
                                        <option value="MT">Montana</option>
                                        <option value="NE">Nebraska</option>
                                        <option value="NV">Nevada</option>
                                        <option value="NH">New Hampshire</option>
                                        <option value="NJ">New Jersey</option>
                                        <option value="NM">New Mexico</option>
                                        <option value="NY">New York</option>
                                        <option value="NC">North Carolina</option>
                                        <option value="ND">North Dakota</option>
                                        <option value="OH">Ohio</option>
                                        <option value="OK">Oklahoma</option>
                                        <option value="OR">Oregon</option>
                                        <option value="PA">Pennsylvania</option>
                                        <option value="RI">Rhode Island</option>
                                        <option value="SC">South Carolina</option>
                                        <option value="SD">South Dakota</option>
                                        <option value="TN">Tennessee</option>
                                        <option value="TX">Texas</option>
                                        <option value="UT">Utah</option>
                                        <option value="VT">Vermont</option>
                                        <option value="VA">Virginia</option>
                                        <option value="WA">Washington</option>
                                        <option value="WV">West Virginia</option>
                                        <option value="WI">Wisconsin</option>
                                        <option value="WY">Wyoming</option>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="postalcode" class="col-sm-2 col-form-label fw-bold">Club Zip</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="postalcode" id="postalcode" placeholder="55555"
                                           value="{{ old('postalcode', $group->postalcode) }}">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="phone" class="col-sm-2 col-form-label fw-bold">Club Phone</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="phone" id="phone" placeholder="555-555-5555"
                                           value="{{ old('phone', $group->phone) }}">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="email" class="col-sm-2 col-form-label fw-bold">Club Email</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="email" id="email" placeholder="club@club.com"
                                           value="{{ old('email', $group->email) }}">
                                </div>
                            </div>

                        </div>
                    </div>

                    @endif
                    <!-- Submit Form Input -->
                    <div class="form-group mt-3">
                        <button type="submit" class="btn btn-primary form-control">Save</button>
                    </div>

                </form>
            </div>


        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Club Details</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">

                            <figure class="mb-4">
                                <p>Current Logo</p>
                                <img src="{{asset('storage/'.$group->image_url)}}" width="250" alt="" />
                            </figure>
                            <p>All Group details will be shown on your club <a class="group_link" id="public_page_link" href="{{ route('group.public_preview', ['group' => $group->id]) }}" title="club public page">public page</a></p>
                            <ul class="list-unstyled mb-0 group_links">
                                <li><a class="group_link" href="{{ route('group.manage_levels', ['group' => $group->id]) }}">Who Can Edit This</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

@endsection
