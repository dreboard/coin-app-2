@extends('layouts.groups.main')
@section('pageTitle', 'Group Management Levels')
@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        @include('groups.group.partials.breadcrumb')

    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-12">
            <h4>Group Management Levels</h4>
            <span id="change_result" class="fw-bold"></span>
            <div class="table-responsive">

                @if(isset($settings))
                    <table class="table table-striped text-successtable-border border-light text-center table-hover">
                        <thead class="border-light">
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">2 Staff Level</th>
                            <th scope="col">3 Director Level</th>
                            <th scope="col">4 Executive Level</th>
                            <th scope="col">Change</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row"><span id="members_label">Approve Members</span></th>
                            <td>No</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td>
                                @if($user_manage_level > 2)
                                    <select name="members" id="members" data-setting="{{$settings->id}}" class="manage_setting">
                                        <option value="3" @if($settings->members > 0) selected @endif>Only Level 3+</option>
                                        <option value="0" @if($settings->members == 0) selected @endif>Anyone</option>
                                    </select>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Edit Group Details</th>
                            <td>No</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td>
                                @if($user_manage_level > 2)
                                    <select name="edit" id="edit" data-setting="{{$settings->id}}" class="manage_setting">
                                        <option value="2" @if($settings->edit > 1) selected @endif>Only Level 3+</option>
                                        <option value="0" @if($settings->edit == 0) selected @endif>Anyone</option>
                                    </select>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Assign Positions</th>
                            <td>No</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td>
                                @if($user_manage_level > 2)
                                    <select name="positions" id="positions" data-setting="{{$settings->id}}" class="manage_setting">
                                        <option value="2" @if($settings->positions > 1) selected @endif>Only Level 3+</option>
                                        <option value="0" @if($settings->positions == 0) selected @endif>Anyone</option>
                                    </select>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Promote Associates</th>
                            <td>No</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td>
                                @if($user_manage_level > 2)
                                    <select name="members" id="members" data-setting="{{$settings->id}}" class="manage_setting">
                                        <option value="2" @if($settings->members > 1) selected @endif>Only Level 3+</option>
                                        <option value="0" @if($settings->members == 0) selected @endif>Anyone</option>
                                    </select>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Create Committees</th>
                            <td>Yes</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td>
                                @if($user_manage_level > 2)
                                    <select name="committees" id="committees" data-setting="{{$settings->id}}" class="manage_setting">
                                        <option value="2" @if($settings->committees > 1) selected @endif>Only Level 3+</option>
                                        <option value="0" @if($settings->committees == 0) selected @endif>Anyone</option>
                                    </select>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Create Announcements</th>
                            <td>Yes</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td>
                                @if($user_manage_level > 2)
                                    <select name="announcements" id="announcements" data-setting="{{$settings->id}}" class="manage_setting">
                                        <option value="2" @if($settings->announcements > 1) selected @endif>Only Level 3+</option>
                                        <option value="0" @if($settings->announcements == 0) selected @endif>Anyone</option>
                                    </select>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Create Elections</th>
                            <td>No</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td>
                                @if($user_manage_level > 2)
                                    <select name="elections" id="elections" data-setting="{{$settings->id}}" class="manage_setting">
                                        <option value="2" @if($settings->elections > 1) selected @endif>Only Level 3+</option>
                                        <option value="0" @if($settings->elections == 0) selected @endif>Anyone</option>
                                    </select>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><span id="posts_label">Create Posts</span></th>
                            <td>Editor/Writers</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td>
                                @if($user_manage_level > 2)
                                    <select name="posts" id="posts" data-setting="{{$settings->id}}" class="manage_setting">
                                        <option value="2" @if($settings->posts > 1) selected @endif>ONLY Writers and Editors</option>
                                        <option value="0" @if($settings->posts == 0) selected @endif>Anyone</option>
                                    </select>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><span id="posts_label">Create Events</span></th>
                            <td>Yes</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td>
                                @if($user_manage_level > 2)
                                    <select name="events" id="events" data-setting="{{$settings->id}}" class="manage_setting">
                                        <option value="2" @if($settings->events > 1) selected @endif>Only Level 2+</option>
                                        <option value="0" @if($settings->events == 0) selected @endif>Anyone</option>
                                    </select>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                @else
                    <p> You do not have any group settings saved</p>
                @endif




                <h4 class="mt-5">Group Management By Position</h4>
                <table class="table table-hover table-striped">
                    <thead>
                    <tr>
                        <th scope="col">Position</th>
                        <th scope="col">Manage Level</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach (Config::get('constants.manage_position_levels') as $k => $position)
                        <tr>
                            <td>{{ $k }}</td>
                            <td>{{ $position }}</td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>





                <p><a class=""
                      href="{{ route('group.manage_request', ['group' => $group->id]) }}">Request Manage Access</a></p>
            </div>
        </div>

    </div>
    @push('scripts')
        <script>
            (function () {
                try {
                    var elements = document.getElementsByClassName("manage_setting");
                    var dataSpans = document.getElementsByClassName("data_spans");
                    for (var i = 0; i < elements.length; i++) {
                        elements[i].addEventListener('change', function () {

                            let result_span = document.getElementById('change_result');
                            result_span.innerHTML = '';

                            fetch('{{ route('group.change_manage_settings') }}', {
                                headers: {"Content-Type": "application/json; charset=utf-8"},
                                method: 'POST',
                                body: JSON.stringify({
                                    _token: @json(csrf_token()),
                                    manage: this.name,
                                    setting: this.value,
                                    setting_id: {{ optional($settings)->id }},
                                    group_id: {{ $group->id }},
                                })
                            }).then(response => response.json())
                                .then(data => {
                                    if (data.result == 'Success') {
                                        result_span.innerHTML = 'Updated';
                                        result_span.classList.add('text-success')
                                    } else {
                                        result_span.classList.add('text-danger');
                                        result_span.innerHTML = 'Error In saving';
                                    }
                                })
                        });
                    }
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
