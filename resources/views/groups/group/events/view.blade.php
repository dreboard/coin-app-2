@extends('layouts.groups.main')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')
        <li class="breadcrumb-item"><a href="{{ route('group.events_home', ['group' => $group->id]) }}">All Events</a>
        </li>

    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <h4>{{$event->title}}</h4>
            <div class="callout callout-primary mb-4">
                <h5 class="card-title">{{ Carbon\Carbon::parse($event->start_at)->format('F jS Y h:i') }}</h5>
                <p class="card-text">{{$event->description}}</p>
                @if(!$present)
                    <a href="#" class="btn btn-primary" id="checkin_btn" data-event="{{$event->id}}">Check In</a>
                @endif
            </div>

            <hr class="mt-3"/>
            <h5>Agendas</h5>
            @if($event->agendas->isEmpty())
                <p>There are no agendas for this meeting</p>
                <a href="#" class="btn btn-primary btn-sm">Create An Agenda</a>
            @else
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Issue</th>
                        <th scope="col">Action</th>
                        <th scope="col">Disposition</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($event->agendas as $agenda)
                        <tr>
                            <th scope="row">{{ $agenda->topic }}</th>
                            <td>
                                @if($user_manage_level == 4)
                                    <div class="col-12">
                                        <select data-issue="{{ $agenda->id }}" class="form-select issue_actions"
                                                name="action" id="action_{{ $agenda->id }}">
                                            <option>Choose...</option>
                                            <option value="Completed" @if($agenda->action == 'Completed') selected @endif>
                                                Completed
                                            </option>
                                            <option value="Skipped" @if($agenda->action == 'Skipped') selected @endif>
                                                Skipped
                                            </option>
                                            <option value="Pending" @if($agenda->action == 'Pending') selected @endif>
                                                Pending
                                            </option>
                                        </select>
                                    </div>
                                @endif

                            </td>
                            <td><span id="action_span_{{ $agenda->id }}">{{ $agenda->action }}</span></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
            @if($event->participants->isEmpty())

            @else
                <hr class="mt-3"/>
                <h5>Participants</h5>
                <div class="table-responsive">
                    <table class="table table-hover table-striped datatable">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Vote</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Vote</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($event->participants as $participant)
                            <tr>
                                <td class="text-start alert">
                                    <span>
                                        {{ $participant->first_name }}{{ $participant->last_name }} / {{ $participant->name }}
                                    </span>
                                </td>
                                <td>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif

        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>
    @push('scripts')
        <script>
            (function () {
                try {
                    var myFunction = function () {
                        // @todo URL update
                        let action_span = document.getElementById("action_span_" + this.getAttribute("data-issue"));
                        action_span.innerHTML = 'Processing....';
                        fetch('{{ route('group.change_agenda_action') }}', {
                            headers: {"Content-Type": "application/json; charset=utf-8"},
                            method: 'POST',
                            body: JSON.stringify({
                                _token: @json(csrf_token()),
                                id: this.getAttribute("data-issue"),
                                agenda_action: this.value
                            })
                        }).then(response => response.json())
                            .then(data => {
                                console.log(data);
                                action_span.innerHTML = data.agenda_action;
                                if (data.agenda_action == 'Completed') {
                                    action_span.classList.remove('text-danger');
                                    action_span.classList.add('text-success');
                                } else {
                                    action_span.classList.add('text-danger');
                                    action_span.classList.remove('text-success');
                                }
                                console.log('Success:', data);
                                console.log(action_span.innerHTML);
                            })
                        //.then(data => console.log(data))
                    }
                    var elements = document.getElementsByClassName("issue_actions");
                    for (var i = 0; i < elements.length; i++) {
                        elements[i].addEventListener('change', myFunction, false);
                    }

                    // Update user ------------------
                    var checkInUser = function () {
                        // @todo URL update
                        fetch('{{ route('group.add_participant_to_event') }}', {
                            headers: {"Content-Type": "application/json; charset=utf-8"},
                            method: 'POST',
                            body: JSON.stringify({
                                _token: @json(csrf_token()),
                                user_id: {{ auth()->user()->id }},
                                event_id: {{ $event->id }},
                            })
                        }).then(response => response.json())
                            .then(data => {
                                console.log(data);
                                if (data.checked == 'has checked') {
                                    checkin_btn.disabled = true;
                                }

                            })
                    }
                    let checkin_btn = document.getElementById("checkin_btn");

                    checkin_btn.addEventListener("click", checkInUser);

                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
