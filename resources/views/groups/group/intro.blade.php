@extends('layouts.groups.main')
@section('pageTitle', $group->name.' Intro Page')
@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')

    </ol>
    <div class="row">
        <h5>Welcome to Your New Group</h5>
        <div class="col-8">

            <div class="alert alert-danger" role="alert">
                <h4>Club Roles and Access Rights</h4>
                You are the first member of the group and have been assigned level 4 manage rights.  This level will allow you to control the entire group and all activities.
                To allow other members of the group to perform actions, You have 2 options
                <ul>
                    <li>Assign global rights to all members, see <a href="{{ route('group.manage_levels', ['group' => $group->id]) }}">Who Can Manage What</a></li>
                    <li>Assign rights to specific members, see <a href="{{ route('group.view_user_positions', ['group' => $group->id]) }}">Assign Positions (By User)</a></li>
                </ul>
                <p>

                </p>


            </div>
            <div class="accordion" id="accordionExample">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingOne">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Approving Members
                        </button>
                    </h2>
                    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            @if($group->private == 0)
                                <p>Your group is set up to accept members upon approval.  If you wish to change this, you can edit this at the
                                    <a href="{{ route('group.edit', ['group' => $group->id]) }}">edit club details</a> page.  </p>

                                   <p> You as the group owner along with the Membership Director or any
                            board position (President, Vice President, Secretary or Treasurer), can approve or deny any requesting member.
                                All request can be found with the link on the left <a href="{{ route('group.view_requests', ['group' => $group->id]) }}">(User Management -> Requests)</a>.</p>
                            @else
                                Your group is set up to accept members immediately.  If you wish to change this, you can edit this at the
                                <a href="{{ route('group.edit', ['group' => $group->id]) }}">edit club details</a> page by setting
                            @endif
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingTwo">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Committees, Events and The Blog
                        </button>
                    </h2>
                    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <p>
                                The links to the left, are the club activity sections.  Committees, Events and The Blog are accessible to everyone, but creating new items in these
                                sections also require elevated roles.</p><p>  <a href="{{ route('group.view_user_positions', ['group' => $group->id]) }}">Assigning members</a> in any Board or Director role satisfies election, committee or event creation.  The roles of
                                Writer, Editor or Webmaster, allows access to the blog.  Again you can assign any member any specific level on there profile page
                            </p>
                            <p>View your <a href="{{ route('group.view_user', ['group' => $group->id, 'user_id' => auth()->user()->id]) }}">group profile</a></p>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingThree">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Members Area
                        </button>
                    </h2>
                    <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            Message Boards, post cards, media (images and videos) and comments on blog posts, can be accessed by all in the
                            <a href="{{ route('group.members_home', ['group' => $group->id]) }}">members area</a>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingThree">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Quit/Transfer Group Ownership
                        </button>
                    </h2>
                    <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            Once you create a group, you can not quit the group, you can only delete the group <a href="{{ route('group.delete_group_page', ['group' => $group->id]) }}"> here</a>. Ownership can be transferred to another user.  Go to Manage ->
                            <a href="{{ route('group.transfer_user', ['group' => $group->id]) }}">Transfer Ownership</a>
                        </div>
                    </div>
                </div>
            </div>



        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Categories</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0 group_links">
                                <li><a class="group_link" id="members_link" href="{{ route('group.view_users', ['group' => $group->id]) }}" title="View Club Members">Members</a></li>
                                <li><a class="group_link" id="officer_link" href="{{ route('group.view', ['group' => $group->id]) }}" title="Assign Group Officers">Officers</a></li>
                                <li><a class="group_link" id="committee_link" href="{{ route('group.committee_index', ['group' => $group->id]) }}" title="View/Create Committees">Committees</a></li>
                                <li><a class="group_link" id="message_link" href="{{ route('group.message_index', ['group' => $group->id]) }}" title="Create Group Messages">Group Messages</a></li>
                                <li><a class="group_link" id="board_link" href="{{ route('group.view', ['group' => $group->id]) }}" title="View/Create Message Board">Message Board</a></li>
                                <li><a class="group_link" id="library_link" href="{{ route('group.library_index', ['group' => $group->id]) }}" title="Maintain Group Library">Library</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0 group_links">
                                <li><a class="group_link" id="vote_link" href="{{ route('group.vote_home', ['group' => $group->id]) }}" title="Create/View Voting Issues">Voting</a></li>
                                <li><a class="group_link" id="event_link" href="{{ route('group.events_home', ['group' => $group->id]) }}" title="View/Create Events">Events</a></li>
                                <li><a class="group_link" id="blog_link" href="{{ route('group.posts_home', ['group' => $group->id]) }}" title="Read/Create Blog Posts">Blog</a></li>
                                <li><a class="group_link" id="media_link" href="{{ route('group.view', ['group' => $group->id]) }}" title="Images and Video">Media</a></li>
                                <li><a class="group_link" id="announce_link" href="{{ route('group.show_announcement_all', ['group' => $group->id]) }}" title="Create Announcements">Announcements</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Side widget-->
            <div class="card mb-4">
                <div class="card-header">{{ date('M Y') }} Events</div>
                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        @if(!empty($events))

                            @foreach($events as $k => $event)
                                <li class="list-group-item">
                                    <a href="{{ route('group.view', ['group' => $group->id]) }}">
                                        {{ Carbon\Carbon::parse($event->start_at)->format('jS') }} | {{ $event->title }}
                                    </a></li>
                            @endforeach
                        @else
                            <li class="list-group-item">No events for {{ date('M Y') }}, <a href="{{ route('group.view', ['group' => $group->id]) }}">Full Schedule</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>

    </div>
    @push('scripts')
        <script>
            (function (ENVIRONMENT) {
                try {


                } catch (error) {
                    console.error(error);
                    if (ENVIRONMENT === 'local') {

                    }
                }
            })();
        </script>
    @endpush
@endsection
