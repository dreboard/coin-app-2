@extends('layouts.groups.main')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        @include('groups.group.partials.breadcrumb')

    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <!-- Featured election-->
            @if (!empty($election))
                <div class="election_block">
                    <h3 id="headline">{{ $election->name }} Voting Ends</h3>
                    <p>Please
                        <a href="{{ route('group.vote_election', ['group' => $group->id, 'election' => $election->id]) }}">VOTE</a>
                    </p>
                    <div id="countdown">
                        <ul>
                            <li><span id="days"></span>days</li>
                            <li><span id="hours"></span>Hours</li>
                            <li><span id="minutes"></span>Minutes</li>
                            <li><span id="seconds"></span>Seconds</li>
                        </ul>
                    </div>
                </div>
            @else

            @endif

            <!-- Featured announcement-->
            @isset($announcement)
                <div class="callout callout-primary mb-4" id="group_announcement_banner">
                    <div
                        class="small text-muted">Created by {{ $announcement->user->name }}: {{ Carbon\Carbon::parse($announcement->created_at)->format('F jS Y') }}</div>
                    <h4 class="card-title h4">Latest Announcement</h4>
                    <p class="card-text">{{ $announcement->message }}</p>
                    <a class="btn btn-primary" href="{{ route('group.show_announcement_all', ['group' => $group->id]) }}">See All →</a>
                </div>
            @else
                <div class="callout callout-primary mb-4" id="group_announcement_banner">
                    <h4 class="card-title h4">No Group Announcements Yet.</h4>
                    <a class="btn btn-primary" href="{{ route('group.create_announcement', ['group' => $group->id]) }}">Create One →</a>
                    <a class="btn btn-primary" href="{{ route('group.show_announcement_all', ['group' => $group->id]) }}">See All →</a>
                </div>
            @endisset

            <div class="row">
                <!-- Nested row for non-featured blog posts-->
                @isset($posts)
                    @foreach($posts as $post)
                        <div class="card mb-4 col-lg-6">
                            <div class="card-body">
                                <div
                                    class="small text-muted">{{ Carbon\Carbon::parse($post->created_at)->format('F jS Y') }}</div>
                                <h5 class="card-title">{{ $post->title }}</h5>
                                <p class="card-text">{{Str::limit($post->body, 30)}}</p>
                                <a class="btn btn-primary"
                                   href="{{ route('group.view_post', ['group' => $group->id, 'post' => $post->id]) }}">Read
                                    more →</a>
                            </div>
                        </div>
                    @endforeach

                @else
                    <div class="callout callout-primary mb-4" id="group_announcement_banner">
                        <h4 class="card-title h4">No Group Posts Yet.</h4>
                        <a class="btn btn-primary" href="{{ route('group.create_announcement', ['group' => $group->id]) }}">Create One →</a>
                        <a class="btn btn-primary" href="{{ route('group.show_announcement_all', ['group' => $group->id]) }}">See All →</a>
                    </div>
                @endisset

            </div>

        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>
    @push('styles')

        <style>
            .election_block {
                margin: 0 auto;
                text-align: center;
            }

            .election_block h1 {
                font-weight: normal;
                letter-spacing: .125rem;
                text-transform: uppercase;
            }

            .election_block li {
                display: inline-block;
                list-style-type: none;
                padding: 1em;
                text-transform: uppercase;
            }

            .election_block li span {
                display: block;
            }

        </style>

    @endpush
    @push('scripts')
        <script>
            (function () {

                try {
                    (function () {
                        const second = 1000,
                            minute = second * 60,
                            hour = minute * 60,
                            day = hour * 24;

                        //I'm adding this section so I don't have to keep updating this pen every year :-)
                        //remove this if you don't need it
                        let today = new Date(),
                            dd = String(today.getDate()).padStart(2, "0"),
                            mm = String(today.getMonth() + 1).padStart(2, "0"),
                            yyyy = today.getFullYear(),
                            nextYear = yyyy + 1,
                            dayMonth = "09/30/",
                            birthday = dayMonth + yyyy,
                            election_end = '{{ Carbon\Carbon::parse(optional($election)->expired_at)->format('Y/m/d') }}'; //"11/01/2022";  $election_end

                        today = mm + "/" + dd + "/" + yyyy;
                        if (today > election_end) {
                            birthday = dayMonth + nextYear;
                        }
                        //end

                        const countDown = new Date(election_end).getTime(),
                            x = setInterval(function() {

                                const now = new Date().getTime(),
                                    distance = countDown - now;

                                document.getElementById("days").innerText = Math.floor(distance / (day)),
                                    document.getElementById("hours").innerText = Math.floor((distance % (day)) / (hour)),
                                    document.getElementById("minutes").innerText = Math.floor((distance % (hour)) / (minute)),
                                    document.getElementById("seconds").innerText = Math.floor((distance % (minute)) / second);

                                //do something later when date is reached
                                if (distance < 0) {
                                    document.getElementById("election_block").style.display = "none";
                                    document.getElementById("headline").style.display = "none";
                                    document.getElementById("headline").innerText = "";
                                    document.getElementById("countdown").style.display = "none";
                                    document.getElementById("content").style.display = "block";
                                    clearInterval(x);
                                }
                                //seconds
                            }, 0)
                    }());
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>

    @endpush
@endsection
