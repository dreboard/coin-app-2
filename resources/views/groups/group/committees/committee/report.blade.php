@extends('layouts.groups.main')
@section('pageTitle', $group->name.' View Report')
@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')
        <li class="breadcrumb-item"><a
                href="{{ route('group.committee_index', ['group' => $group->id]) }}">Committees</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('group.view_committee', ['group' => $group->id, 'committee' => $committee->id]) }}">{{$committee->title}}</a>
        </li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <h5>{{ Str::limit($report->title, 50) }}</h5>
            <a class="btn btn-secondary btn-sm"
               href="{{ route('group.new_committee_report', ['group' => $group->id, 'committee' => $committee->id]) }}">Create A Report</a>

            <br class="mb-5"/>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">Created</th>
                    <th scope="col">Last Update</th>
                    <th class="text-end" scope="col">Published</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="text-start">
                        {{ Carbon\Carbon::parse($report->created_at)->format('F jS Y h:i:s') }}
                    </td>
                    <td class="text-start">
                        {{ Carbon\Carbon::parse($report->updated_at)->format('F jS Y h:i:s') }}
                    </td>
                    <td class="text-end">
                        {{ boolval($report->published) ? 'Yes' : 'No' }}
                    </td>
                </tr>
                </tbody>
            </table>



            <form action="{{ route('group.update_committee_report') }}" method="post">
                @csrf
                <input type="hidden" name="report_id" value="{{ $report->id }}">
                <input type="hidden" name="group_id" value="{{ $group->id }}">
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Title</label>
                    <input type="text" class="form-control" name="title" id="title" aria-describedby="titleHelp" value="{{ $report->title }}">
                    <div id="titleHelp" class="form-text">Title of report</div>
                </div>


                <div class="mb-3">
                    <label for="type" class="form-label">Report Type</label>
                    <select class="form-select" name="type" id="type">
                        <option selected>Select Type....</option>
                        @foreach (Config::get('constants.report_types') as $k => $position)
                            <option value="{{ $position }}" @if($report->type == $position) selected @endif>{{ $position }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlTextarea1" class="form-label">Overview</label>
                    <textarea class="form-control" name="overview" id="overview" rows="3">{{ $report->overview }}</textarea>
                </div>
                <div class="mb-3">
                    <label for="background" class="form-label">Background</label>
                    <textarea class="form-control" name="background" id="background" rows="3">{{ $report->background }}</textarea>
                </div>
                <div class="mb-3">
                    <label for="discussion" class="form-label">Discussion</label>
                    <textarea class="form-control" name="discussion" id="discussion" rows="3">{{ $report->discussion }}</textarea>
                </div>
                <div class="mb-3">
                    <label for="budget" class="form-label">Budget</label>
                    <textarea class="form-control" name="budget" id="budget" rows="3">{{ $report->budget }}</textarea>
                </div>
                <div class="mb-3">
                    <label for="conclusion" class="form-label">Conclusion</label>
                    <textarea class="form-control" name="conclusion" id="conclusion" rows="3">{{ $report->conclusion }}</textarea>
                </div>
                <div class="mb-3">
                    <label for="recommendations" class="form-label">Recommendations</label>
                    <textarea class="form-control" name="recommendations" id="recommendations" rows="3">{{ $report->recommendations }}</textarea>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" value="1" name="published" id="published_1"
                    @if($report->published == 1) checked @endif>
                    <label class="form-check-label" for="published_1">
                        Publish
                    </label>
                </div>
                <div class="form-check mb-3">
                    <input class="form-check-input" type="radio" value="0" name="published" id="published_0" @if($report->published == 0) checked @endif>
                    <label class="form-check-label" for="published_0">
                        Draft
                    </label>
                </div>
                <button type="submit" class="btn btn-primary">Save Report</button>
            </form>
            <hr />
            <div class="mt-5 mb-5">
                <div class="btn-group">
                    <a href="{{ route('group.new_committee_report', ['group' => $group->id, 'committee' => $committee->id]) }}" class="btn btn-primary active" aria-current="page">Create New Report</a>
                    <a href="{{ route('group.committee_reports', ['group' => $group->id, 'committee' => $committee->id]) }}" class="btn btn-secondary">All Reports</a>
                    <a href="{{ route('group.view_committee', ['group' => $group->id, 'committee' => $committee->id]) }}" class="btn btn-primary">Committee Home</a>
                </div>
            </div>


        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>
    @push('scripts')
        <script src="https://cdn.ckeditor.com/ckeditor5/35.2.1/classic/ckeditor.js"></script>
        <script>
            (function () {
                try {
                    ClassicEditor
                        .create( document.querySelector( '#discussion' ) )
                        .catch( error => {
                            console.error( error );
                        } );
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>

    @endpush
@endsection
