@extends('layouts.groups.main')
@section('pageTitle', $group->name.' Committee Members')
@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')
        <li class="breadcrumb-item"><a href="{{ route('group.committee_index', ['group' => $group->id]) }}">Committees</a></li>
        <li class="breadcrumb-item"><a href="{{ route('group.view_committee', ['group' => $group->id, 'committee' => $committee->id]) }}">{{$committee->title}}</a></li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <h4>{{$committee->title}} Members</h4>
            @if(!empty($committee->members))
                <div class="table-responsive">
                    <table class="table table-hover">
                        @foreach($committee->members as $member)
                            <tr>
                                <td class="text-start alert w-75">
                                    <span>
                                        {{ $member->first_name }}{{ $member->last_name }} / {{ $member->name }}
                                    </span>
                                </td>
                                <td class="w-5">
                                    <a href="{{ route('group.view_user', ['group' => $group->id, 'user_id' => $member->id]) }}">Profile</a>
                                </td>

                                <td class="w-5">
                                    @if($committee->chair !== $member->id)
                                    <form class="row row-cols-lg-auto g-3 align-items-center" method="post" action="{{route('group.remove_committee_member')}}">
                                        @csrf
                                        <input type="hidden" name="committee_id" value="{{ $committee->id }}">
                                        <input type="hidden" name="user_id" value="{{ $member->id }}">
                                        @if($committee->chair == auth()->user()->id || $member->id == auth()->user()->id || $userIsBoardMember)
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-danger btn-sm">Remove</button>
                                        </div>
                                        @endif
                                    </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            @else
                <p>No Members</p>
            @endif

            @if($committee->chair == auth()->user()->id || $userIsBoardMember)

            @endif

            <hr class="mt-5" />
            <h4>Select/Replace Committee Chair</h4>
            <form class="row row-cols-lg-auto g-3 align-items-center" action="{{ route('group.committee_new_chair') }}" method="post">
                @csrf
                <input type="hidden" name="committee_id" value="{{ $committee->id }}">
                <div class="col-12">
                    <select name="chair" id="chair" class="form-select" aria-label="Select A Member">
                        <option selected>Choose Member....</option>
                        @if(!empty($candidates))
                            @foreach($candidates as $user => $u)
                                <option value="{{ $u->user_id }}">{{ $u->user_first_name }}{{ $u->user_last_name }} / {{ $u->club_position }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>

            @if($committee->chair == auth()->user()->id || $userIsBoardMember)
                <hr class="mt-5" />
                <h4>Assign Members To This Committee</h4>
                <form action="{{ route('group.committee_attach_members') }}" method="post">
                    @csrf
                    <input type="hidden" name="committee_id" value="{{ $committee->id }}">
                    <input type="hidden" name="group_id" value="{{ $group->id }}">
                    <table class="table table-hover datatable">
                        <thead>
                        <tr>
                            <th>User</th>
                            <th>Type</th>
                            <th>Include</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($candidates as $user => $u)
                            <tr>
                                <td>
                                    <label for="user_{{ $u->user_id }}" title="{{ $u->name }}">
                                        {{ $u->user_first_name }}{{ $u->user_last_name }} / {{ $u->club_position }}
                                    </label>
                                </td>
                                <td>{{ucfirst($u->member_type)}}</td>
                                <td><input id="user_{{ $u->user_id }}" type="checkbox" name="members[]"
                                           value="{{ $u->user_id }}"></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="mt-5">
                        <button type="submit" class="btn btn-primary form-control w-25">Assign</button>
                    </div>


                </form>

            @endif

        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>

@endsection
