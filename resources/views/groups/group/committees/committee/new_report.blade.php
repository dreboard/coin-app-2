@extends('layouts.groups.main')
@section('pageTitle', $group->name.' Create Report')
@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')
        <li class="breadcrumb-item"><a
                href="{{ route('group.committee_index', ['group' => $group->id]) }}">Committees</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('group.view_committee', ['group' => $group->id, 'committee' => $committee->id]) }}">{{$committee->title}}</a>
        </li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <h5>Create A New Report</h5>
            <a class="btn btn-secondary btn-sm"
               href="{{ route('group.committee_reports', ['group' => $group->id, 'committee' => $committee->id]) }}">All Reports</a>

            <form class="mt-5" action="{{ route('group.save_committee_report') }}" method="post">
                @csrf
                <input type="hidden" name="committee_id" value="{{ $committee->id }}">
                <input type="hidden" name="group_id" value="{{ $group->id }}">
                <input type="hidden" name="new_report" value="1">
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Report Title</label>
                    <input type="text" class="form-control" name="title" id="title" aria-describedby="titleHelp" value="{{ old('title') }}">
                    <div id="titleHelp" class="form-text">Title of report</div>
                </div>

                <div class="mb-3">
                    <label for="type" class="form-label">Report Type</label>
                    <select class="form-select" name="type" id="type">
                        <option>Select Type....</option>
                        @foreach (Config::get('constants.report_types') as $k => $position)
                            <option value="{{ $position }}"@if(old('title') == $position) selected @endif>{{ $position }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Save Report</button>
            </form>
            <br />
            <div class="mt-5 mb-5">
                <div class="btn-group">
                    <a href="{{ route('group.committee_reports', ['group' => $group->id, 'committee' => $committee->id]) }}" class="btn btn-secondary">All Reports</a>
                    <a href="{{ route('group.view_committee', ['group' => $group->id, 'committee' => $committee->id]) }}" class="btn btn-primary">Committee Home</a>
                </div>
            </div>
        </div>


        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>

@endsection
