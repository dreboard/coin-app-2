@extends('layouts.groups.main')
@section('pageTitle', $group->name.' Create Issue')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')
        <li class="breadcrumb-item"><a
                href="{{ route('group.committee_index', ['group' => $group->id]) }}">Committees</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('group.view_committee', ['group' => $group->id, 'committee' => $committee->id]) }}">{{$committee->title}}</a>
        </li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <h4>Create An Issue For</h4>
            <form action="{{ route('group.save_issue') }}" method="post" class="mt-4">
                @csrf
                <input type="hidden" name="committee_id" value="{{ $committee->id }}">
                @include('groups.group.partials.issue_form')
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>

        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>
    @push('styles')
        <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
        <link href="https://cdn.jsdelivr.net/gh/Eonasdan/tempus-dominus@master/dist/css/tempus-dominus.css"
              rel="stylesheet" crossorigin="anonymous">
    @endpush
    @push('scripts')
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
                crossorigin="anonymous"></script>
        <!-- Tempus Dominus JavaScript -->
        <script src="https://cdn.jsdelivr.net/gh/Eonasdan/tempus-dominus@master/dist/js/tempus-dominus.js"
                crossorigin="anonymous"></script>

        <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
        <script>
            (function () {
                try {
                    // https://bootstrap-datepicker.readthedocs.io/en/latest/
                    $('.datepicker').datepicker({
                        format: 'yyyy-mm-dd',
                    });

                    ClassicEditor
                        .create( document.querySelector('#overview'))
                        .catch( error => {
                            console.error(error);
                        });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
