@extends('layouts.groups.main')
@section('pageTitle', $group->name.' Committee Issues')
@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')
        <li class="breadcrumb-item"><a
                href="{{ route('group.committee_index', ['group' => $group->id]) }}">Committees</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('group.view_committee', ['group' => $group->id, 'committee' => $committee->id]) }}">{{$committee->title}}</a>
        </li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <h5>Committee Issues</h5>
            <a class="btn btn-secondary btn-sm"
               href="{{ route('group.new_committee_report', ['group' => $group->id, 'committee' => $committee->id]) }}">Create A Report</a>

            <br class="mb-5" />
            <table class="table table-hover table-striped dataTable">
                @if(!empty($issues))
                    @foreach($issues as $issue)
                        <tr>
                            <td class="text-start">
                                <span class="text-danger fw-bold">{{ $issue->name }}</span>
                            </td>
                            <td class="text-end">
                                <a href="{{ route('group.view_issue', ['group' => $group->id, 'issue' => $issue->id]) }}">View</a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td class="text-start">No Current Issues</td>
                        <td class="text-end">
                        </td>
                    </tr>
                @endif

            </table>
            <div class="mb-5"></div>
        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>

@endsection
