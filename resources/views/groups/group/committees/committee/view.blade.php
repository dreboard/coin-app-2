@extends('layouts.groups.main')
@section('pageTitle', $group->name.' View Committee')

@section('content')



    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')
        <li class="breadcrumb-item"><a
                href="{{ route('group.committee_index', ['group' => $group->id]) }}">Committees</a></li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <h4>{{$committee->title}}</h4>
            <table class="table table-hover table-striped">
                <tr>
                    <th>Chair</th>
                    <td>
                        <a href="{{ route('group.view_user', ['group' => $group->id, 'user_id' => $chair]) }}">{{ $chair->first_name ?? '' }} {{ $chair->last_name ?? '' }} / {{ $chair->name ?? '' }}</a>
                    </td>
                </tr>
                <tr>
                    <th>Created</th>
                    <td>{{ Carbon\Carbon::parse($committee->created_at)->format('F jS Y') }}</td>
                </tr>
            </table>
            <p>{{$committee->details}}</p>

            <h4>Members</h4>
            @if(!empty($committee->members))
                <div class="table-responsive">
                    <table class="table table-hover">
                        @foreach($committee->members as $member)
                            <tr>
                                <td class="text-start alert">
                                    <span>
                                        {{ $member->first_name }}{{ $member->last_name }} / {{ $member->name }}
                                    </span>
                                </td>
                                <td>
                                    <a href="{{ route('group.view_user', ['group' => $group->id, 'user_id' => $member]) }}">Profile</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            @else
                <p>No Members</p>
            @endif

            @if($committee->type == 'Membership' )
            <div class="callout callout-primary mb-4" id="committee_reports_banner">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title h4">Memberships</h4>
                        </div>
                        <div class="col">
                            <a class="btn btn-primary"
                               href="{{ route('group.new_committee_report', ['group' => $group->id, 'committee' => $committee->id]) }}">Requests →</a>
                        </div>
                        <div class="col">
                            <a class="btn btn-primary"
                               href="{{ route('group.committee_reports', ['group' => $group->id, 'committee' => $committee->id]) }}">See All →</a>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="callout callout-primary mb-4" id="committee_reports_banner">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title h4">Meetings</h4>
                        </div>
                        <div class="col">
                            <a class="btn btn-primary"
                               href="{{ route('group.new_committee_report', ['group' => $group->id, 'committee' => $committee->id]) }}">Create →</a>
                        </div>
                        <div class="col">
                            <a class="btn btn-primary"
                               href="{{ route('group.committee_reports', ['group' => $group->id, 'committee' => $committee->id]) }}">See All →</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="callout callout-primary mb-4" id="committee_issues_banner">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title h4">Issues</h4>
                        </div>
                        <div class="col">
                            <a class="btn btn-primary"
                               href="{{ route('group.committee_issues', ['group' => $group->id, 'committee' => $committee->id]) }}">Create →</a>
                        </div>
                        <div class="col">
                            <a class="btn btn-primary"
                               href="{{ route('group.committee_issues', ['group' => $group->id, 'committee' => $committee->id]) }}">See All →</a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="callout callout-primary mb-4" id="committee_reports_banner">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title h4">Reports</h4>
                        </div>
                        <div class="col">
                            <a class="btn btn-primary"
                               href="{{ route('group.new_committee_report', ['group' => $group->id, 'committee' => $committee->id]) }}">Create →</a>
                        </div>
                        <div class="col">
                            <a class="btn btn-primary"
                               href="{{ route('group.committee_reports', ['group' => $group->id, 'committee' => $committee->id]) }}">See All →</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="callout callout-primary mb-5" id="committee_members_banner">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title h4">Members</h4>
                        </div>
                        <div class="col">
                            <a class="btn btn-primary"
                               href="{{ route('group.committee_assign_members', ['group' => $group->id, 'committee' => $committee->id]) }}">Assign →</a>
                        </div>
                        <div class="col">
                            <a class="btn btn-primary"
                               href="{{ route('group.show_announcement_all', ['group' => $group->id]) }}">See All →</a>
                        </div>
                    </div>
                </div>
            </div>

            @if($committee->chair == auth()->user()->id )
                <div class="callout callout-danger mb-5" id="committee_members_banner">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <h4 class="card-title h4">Delete Committee</h4>
                            </div>
                            <div class="col">
                                <a class="btn btn-danger" href="" data-bs-toggle="modal" data-bs-target="#deleteModal">Delete →</a>
                            </div>
                            <div class="col">

                            </div>
                        </div>
                    </div>
                </div>
                <!-- removal modal -->
                <div class="modal" tabindex="-1" id="deleteModal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="{{ route('group.remove_user') }}" method="post">
                                @csrf
                                <div class="modal-header">
                                    <h5 class="modal-title">Delete Committee</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <p>You are about to delete this committee</p>
                                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}" />
                                    <input type="hidden" name="committee_id" value="{{ $committee->id }}" />
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-success" data-bs-dismiss="modal">Never-mind</button>
                                    <button type="submit" class="btn btn-danger">Yes, do it</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


            @endif

            <div class="mt-5 mb-5"></div>
        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>

@endsection
