@extends('layouts.groups.main')
@section('pageTitle', $group->name.' All Reports')
@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')
        <li class="breadcrumb-item"><a
                href="{{ route('group.committee_index', ['group' => $group->id]) }}">Committees</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('group.view_committee', ['group' => $group->id, 'committee' => $committee->id]) }}">{{$committee->title}}</a>
        </li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <h5>Committee Reports</h5>
            <a class="btn btn-secondary btn-sm"
               href="{{ route('group.new_committee_report', ['group' => $group->id, 'committee' => $committee->id]) }}">Create A Report</a>

            <br class="mb-5" />
            <table class="table table-hover datatable">
                <thead>
                <tr>
                    <th scope="col">Title</th>
                    <th scope="col">Created</th>
                    <th scope="col">Published</th>
                    <th class="text-end"></th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($reports))
                    @foreach($reports as $report)
                        <tr>
                            <td class="text-start">
                                {{ Str::limit($report->title, 30) }}
                            </td>
                            <td class="text-start">
                                {{ Carbon\Carbon::parse($report->created_at)->format('F jS Y h:i:s') }}
                            </td>
                            <td class="text-start">
                                {{ boolval($report->published) ? 'Yes' : 'No' }}
                            </td>
                            <td class="text-end">
                                <a href="{{ route('group.view_committee_report', ['group' => $group->id, 'committee' => $committee->id, 'report' => $report->id]) }}">View</a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td class="text-start">No Current Reports</td>
                        <td class="text-end"></td>
                        <td class="text-end"></td>
                        <td class="text-end"></td>
                    </tr>
                @endif
                </tbody>
            </table>
            <div class="mb-5"></div>
        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>

@endsection
