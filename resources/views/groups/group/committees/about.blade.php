@extends('layouts.groups.main')
@section('pageTitle', ' About Committees')
@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        @include('groups.group.partials.breadcrumb')

    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <h5>About Committees</h5>
            <a class="btn btn-secondary btn-sm"
               href="{{ route('group.create_committee', ['group' => $group->id]) }}">Create A Committee</a>

            <p>The primary function of a committee is to contribute to the efficient operation of an organization.  In most cases, a committee is concerned with the communication of information and with assisting the leadership in the decision-making process by providing needed information.</p>

            <div class="row mt-5">
                <div class="col-xl-3 col-md-6">
                    <div class="card bg-primary text-white mb-4">
                        <div class="card-body"></div>
                        <div class="card-footer d-flex align-items-center justify-content-between">
                            <a class="small text-white stretched-link"
                               href="{{ route('group.create_committee', ['group' => $group->id]) }}">Create A Committee</a>
                            <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>

@endsection
