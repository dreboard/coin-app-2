@extends('layouts.groups.main')
@section('pageTitle', $group->name.' Create Committee')

@section('content')

    <h3 class="mt-4">{{ $group->name }}</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')
        <li class="breadcrumb-item"><a href="{{ route('group.committee_index', ['group' => $group->id]) }}">Committees</a></li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <h4>Create A Committee</h4>
            <form action="{{ route('group.save_committee') }}" method="post" class="mt-4">
                @csrf
                <input type="hidden" name="group_id" value="{{ $group->id }}">
                <div class="mb-3">
                    <label for="issue" class="form-label">Title</label>
                    <input type="text" class="form-control @error('title') is-invalid @enderror"
                           value="{{ old('title') }}" id="title" name="title" aria-describedby="issueHelp">
                    @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div id="issueHelp" class="form-text">Title/Name of Committee</div>
                </div>
                <!-- Message Form Input -->
                <div class="form-group mb-3">
                    <label class="control-label">Description</label>
                    <textarea id="details" name="details" class="form-control">{{ old('details') }}</textarea>
                </div>
                <div class="mb-3">
                    <select name="type" id="type" class="form-select" aria-label="Default select example">
                        <option selected>Select A Type</option>
                        @foreach (Config::get('constants.committee_types') as $k => $position)
                            <option value="{{ $position }}">{{ $position }}</option>
                        @endforeach
                    </select>
                    <div id="issueHelp" class="form-text">Committee Type</div>
                </div>

                <fieldset>
                <div class="mb-3">
                    <select name="chair" id="chair" class="form-select" aria-label="Select A Meeting">
                        <option value="0" selected>Select A Committee Chair</option>
                        @if(!empty($candidates))
                                @foreach($candidates as $user => $u)
                                    <option value="{{ $u->user_id }}">{{ $u->user_first_name }}{{ $u->user_last_name }} / {{ $u->club_position }}</option>
                                @endforeach
                        @endif
                    </select>
                    <div id="issueHelp" class="form-text">Attach to Event</div>
                </div>
                    <div class="mb-3 form-check">
                        <input type="checkbox" class="form-check-input" name="create_announcement" id="create_announcement">
                        <label class="form-check-label" for="create_announcement">Create Announcement</label>
                    </div>
                    <br class="mt-3" />

                <div class="mb-3 table-responsive">
                    <h4>Appoint Members</h4>
                    <div class="small text-muted mb-3 text-success">Members can be assigned later.</div>
                    <table class="table table-hover datatable">
                        <thead>
                        <tr>
                            <th>User</th>
                            <th>Type</th>
                            <th>Include</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($candidates as $user => $u)
                            <tr>
                                <td>
                                    <label for="user_{{ $u->user_id }}" title="{{ $u->name }}">
                                        {{ $u->user_first_name }}{{ $u->user_last_name }} / {{ $u->club_position }}
                                    </label>
                                </td>
                                <td>{{ucfirst($u->member_type)}}</td>
                                <td><input id="member_{{ $u->user_id }}" type="checkbox" name="members[]"
                                           value="{{ $u->user_id }}"></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                </fieldset>


                <button type="submit" class="btn btn-primary">Save</button>
            </form>

            <div class="h-25"></div>

        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>
    @push('styles')
        <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
        <link href="https://cdn.jsdelivr.net/gh/Eonasdan/tempus-dominus@master/dist/css/tempus-dominus.css"
              rel="stylesheet" crossorigin="anonymous">
    @endpush
    @push('scripts')
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
                crossorigin="anonymous"></script>
        <!-- Tempus Dominus JavaScript -->
        <script src="https://cdn.jsdelivr.net/gh/Eonasdan/tempus-dominus@master/dist/js/tempus-dominus.js"
                crossorigin="anonymous"></script>

        <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
        <script>
            (function () {
                try {
                    // https://bootstrap-datepicker.readthedocs.io/en/latest/
                    $('.datepicker').datepicker({
                        format: 'yyyy-mm-dd',
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
