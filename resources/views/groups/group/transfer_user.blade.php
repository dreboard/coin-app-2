@extends('layouts.groups.main')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')

    </ol>
    @if($group->user_id === auth()->user()->id)
        <h5>Transfer Group Ownership</h5>
    @else
        <h5>Request Transfer Group Ownership</h5>
    @endif
    <div class="card">
        <div class="card-body">
            <p>Request that YOU obtain ownership</p>
            <form class="row row-cols-lg-auto g-3 align-items-center"
                  action="{{ route('group.transfer_request') }}" method="post">
                @csrf
                <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                <input type="hidden" name="group_id" value="{{ $group->id }}">
                <div class="col-6">
                    <button type="submit" class="btn btn-primary">I Request Ownership</button>
                </div>
            </form>
        </div>
    </div>


    <div class="row mt-5">
        <div class="col">
            <h5>Request Administrator Transfer Ownership</h5>
            <form method="post" action="{{ route('group.view_users', ['group' => $group->id]) }}" class="mb-4">
                @csrf
                <input type="hidden" name="sender" value="{{ auth()->user()->id }}" />
                <input type="hidden" name="recipient" value="admin" />

                <div class="mb-3">
                    <select class="form-select" aria-label="Default select example" name="reason">
                        <option selected>Choose a reason</option>
                        <option value="1">User is no longer a user of this site</option>
                        <option value="2">User is currently banned or in warning status</option>
                        <option value="3">User is unable to use this site</option>
                        <option value="3">Members Have voted to remove user</option>
                        <option value="2">Other.  Please elaborate</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label class="visually-hidden" for="member_position">Preference</label>
                    <select class="form-select" id="user_id" name="user_id">
                        <option selected>Request A Successor</option>
                        @foreach ($members as $member)
                            <option value="{{ $member->uid }}">{{ $member->first_name }}  {{ $member->last_name }} | {{ $member->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="mb-3">
                    <label for="exampleFormControlTextarea1" class="form-label">Explanation</label>
                    <textarea class="form-control" name="body" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
                <button class="btn btn-primary" type="submit">Send</button>
            </form>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Reasons</h5>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">User is no longer a user of this site</li>
                        <li class="list-group-item">User is currently banned or in warning status</li>
                        <li class="list-group-item">User is unable to use this site</li>
                        <li class="list-group-item">
                            Members Have voted to remove user <a href="{{ route('group.create_vote', ['group' => $group->id]) }}">(Please create a vote)</a>
                        </li>
                        <li class="list-group-item">Other.  Please elaborate</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <hr class="mt-5 mb-5" />

    <form class="row row-cols-lg-auto g-3 align-items-center"
          action="{{ route('group.transfer_request') }}" method="post">
        @csrf
        <input type="hidden" name="group_id" value="{{ $group->id }}">
        <div class="col-6">
            <label class="visually-hidden" for="member_position">Preference</label>
            <select class="form-select" id="user_id" name="user_id">
                <option selected>Request A Successor</option>
                @foreach ($members as $member)
                    <option value="{{ $member->uid }}">{{ $member->first_name }}  {{ $member->last_name }} | {{ $member->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-6">
            <button type="submit" class="btn btn-primary">Process</button>
        </div>
    </form>


    <br />

    @if($group->user_id === auth()->user()->id)
    <form class="row row-cols-lg-auto g-3 align-items-center"
          action="{{ route('group.transfer') }}" method="post">
        @csrf
        <input type="hidden" name="group_id" value="{{ $group->id }}">
        <div class="col-6">
            <label class="visually-hidden" for="member_position">Preference</label>
            <select class="form-select" id="user_id" name="user_id">
                <option selected>Choose A Successor</option>
                @foreach ($members as $member)
                    <option value="{{ $member->uid }}">{{ $member->first_name }}  {{ $member->last_name }} | {{ $member->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-6">
            <button type="submit" class="btn btn-primary">Process</button>
        </div>
    </form>

    @endif




@endsection
