@extends('layouts.groups.main')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')
        @if(intval(auth()->user()->userManageLevel($group->id))  > 2)
            <li class="breadcrumb-item"><a href="{{ route('group.create_election', ['group' => $group->id]) }}">Create
                    Election</a></li>
        @endif

    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <h4>{{$election->name}}</h4>
            <p><a href="{{ route('group.election_progress', ['group' => $group->id, 'election' => $election->id]) }}">Results</a></p>

            @empty($positions)
                <p>
                    There are no nominees for this election <a href="{{ route('group.nominate', ['group' => $group->id, 'election' => $election->id]) }}">Please nominate a member</a>
                </p>
            @else
                @foreach($positions as $k => $position_array)

                    @if(in_array($position_array[0]['role_id'] ,$role_voted_on))
                        <div class="card">
                            <div class="card-body alert alert-success">
                                You Have Voted For {{ Config::get('constants.board_positions')[$position_array[0]['role_id']] }}
                            </div>
                        </div>
                    @else
                        <form action="{{ route('group.save_vote') }}" method="post">
                            @csrf
                            <input type="hidden" name="role_id" value="{{ $position_array[0]['role_id'] }}">
                            <input type="hidden" name="election_id" value="{{ $election->id }}">
                            <input type="hidden" name="voter" value="{{ auth()->user()->id }}">
                            <div class="col-md-12">
                                <!-- Subject Form Input -->
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th scope="col">User</th>
                                        <th scope="col">Position</th>
                                        <th scope="col">Vote</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($position_array as $position)

                                        <tr>
                                            <td>
                                                <label for="user_{{ $position['user']['id'] }}"
                                                       title="{{ $position['user']['id'] }}">
                                                    {{ $position['user']['first_name'] }} {{ $position['user']['last_name'] }}
                                                    / {{ $position['user']['name'] }}
                                                </label>
                                            </td>
                                            <td>{{ Config::get('constants.board_positions')[$position['role_id']] }}</td>
                                            <td><input id="user_{{ $position['user']['id'] }}" type="radio" name="candidate[]"
                                                       value="{{ $position['user']['id'] }}">

                                            </td>
                                        </tr>


                                    @endforeach
                                    <tr>
                                        <td scope="col"></td>
                                        <td scope="col"></td>
                                        <td scope="col">
                                            @if(!in_array($position_array[0]['role_id'] ,$role_voted_on ))
                                                <button type="submit" class="btn btn-primary form-control">Vote</button>
                                            @endif
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <br/>

                            </div>
                        </form>
                    @endif
                @endforeach
            @endempty


        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>

@endsection
