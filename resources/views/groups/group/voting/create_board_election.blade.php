@extends('layouts.groups.main')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')
        @if(intval(auth()->user()->userManageLevel($group->id))  > Config::get('constants.manage_levels')['Staff Level'])
            <li class="breadcrumb-item"><a href="{{ route('group.create_election', ['group' => $group->id]) }}">Create Election</a> </li>
        @endif

    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <h4>Create Election For</h4>
            <form action="{{ route('group.save_board_election') }}" method="post" class="mt-4">
                @csrf
                <input type="hidden" name="group_id" value="{{ $group->id }}">
                <div class="col-md-12">
                    <!-- Subject Form Input -->
                    @foreach ($positions as $k => $position)
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="{{ $k }}" name="roles[]" id="roles{{ $k }}">
                            <label class="form-check-label" for="roles{{ $k }}">
                                {{ $position }}
                            </label>
                        </div>
                    @endforeach

                    <br />
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col">Start</th>
                            <th scope="col">End</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td scope="col">
                                <div class="input-group date" data-provide="datepicker">
                                    <input type="text" class="form-control" name="start_at">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </td>
                            <td scope="col">
                                <div class="input-group date" data-provide="datepicker">
                                    <input type="text" class="form-control" name="expired_at">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </td>
                            <td scope="col"></td>
                        </tr>

                        <tr>
                            <td scope="col">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="1" name="create_announcement" id="create_announcement">
                                    <label class="form-check-label" for="create_announcement">
                                       Create Announcement
                                    </label>
                                </div>
                            </td>
                            <td scope="col">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="1" name="send_message" id="send_message">
                                    <label class="form-check-label" for="send_message">
                                        Send Group Message
                                    </label>
                                </div>
                            </td>
                            <th scope="col"><button type="submit" class="btn btn-primary form-control">Create</button></th>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </form>

        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>
    @push('styles')
        <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
    @endpush
    @push('scripts')
        <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
        <script>
            (function () {
                try {
                    // https://bootstrap-datepicker.readthedocs.io/en/latest/
                    $('.datepicker').datepicker({
                        format: 'yyyy-mm-dd',
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
