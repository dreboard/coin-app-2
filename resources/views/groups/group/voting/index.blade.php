@extends('layouts.groups.main')
@section('title', 'Manage Club')
@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        @include('groups.group.partials.breadcrumb')

    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <h5>Group Voting</h5>
            <table class="table table-hover table-striped">

                @if(!empty($election))
                    <tr>
                        <td class="text-start"><span class="text-danger fw-bold">{{ $election->name }} Voting</span></td>
                        <td class="text-end"><a class="btn btn-primary btn-sm"
                                                href="{{ route('group.vote_election', ['group' => $group->id, 'election' => $election->id]) }}">Go</a> </td>
                    </tr>
                    <tr>
                        <td class="text-start"><span class="text-danger">{{ $election->name }} Results</span></td>
                        <td class="text-end"><a class="btn btn-primary btn-sm"
                                                href="{{ route('group.election_progress', ['group' => $group->id, 'election' => $election->id]) }}">Go</a> </td>
                    </tr>
                    <tr>
                        <td class="text-start">Nominations</td>
                        <td class="text-end"><a class="btn btn-primary btn-sm"
                                                href="{{ route('group.nominate', ['group' => $group->id, 'election' => $election->id]) }}">Go</a> </td>
                    </tr>
                @else
                    <tr>
                        <td class="text-start"><span class="text-danger">No Current Elections</span></td>
                        <td class="text-end">
                            <a class="btn btn-primary btn-sm"
                               href="{{ route('group.create_election', ['group' => $group->id]) }}">Create</a>
                        </td>
                    </tr>
                @endif
                @if(intval(auth()->user()->userManageLevel($group->id))  > 2)
                    <tr>
                        <td class="text-start">Create Board Member Election</td>
                        <td class="text-end"><a class="btn btn-primary btn-sm"
                                                href="{{ route('group.create_election', ['group' => $group->id]) }}">Go</a> </td>
                    </tr>
                        <tr>
                            <td class="text-start">Create An Issue</td>
                            <td class="text-end"><a class="btn btn-primary btn-sm"
                                                    href="{{ route('group.create_issue', ['group' => $group->id]) }}">Go</a> </td>
                        </tr>
                        <tr>
                            <td class="text-start">All Issues</td>
                            <td class="text-end"><a class="btn btn-primary btn-sm"
                                                    href="{{ route('group.issue_home', ['group' => $group->id]) }}">Go</a> </td>
                        </tr>
                    <tr>
                        <td>Assign Positions (No Vote)</td>
                        <td class="text-end"><a class="btn btn-primary btn-sm"
                                                href="{{ route('group.positions', ['group' => $group->id]) }}">Go</a> </td>
                    </tr>

                @endif


                    <tr>
                        <td>Current Elections</td>
                        <td class="text-end"><a class="btn btn-primary btn-sm"
                                                href="{{ route('group.edit', ['group' => $group->id]) }}">Go</a> </td>
                    </tr>
                    <tr>
                        <td>Past Elections</td>
                        <td class="text-end"><a class="btn btn-primary btn-sm"
                                                href="{{ route('group.past_elections', ['group' => $group->id]) }}">Go</a> </td>
                    </tr>
                    <tr>
                        <td>My Voting History</td>
                        <td class="text-end"><a class="btn btn-primary btn-sm"
                                                href="{{ route('group.my_voting', ['group' => $group->id]) }}">Go</a> </td>
                    </tr>

            </table>
        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>

@endsection
