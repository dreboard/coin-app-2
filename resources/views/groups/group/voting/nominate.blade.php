@extends('layouts.groups.main')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')
        @if(intval(auth()->user()->userManageLevel($group->id))  > 2)
            <li class="breadcrumb-item"><a href="{{ route('group.create_election', ['group' => $group->id]) }}">Create Election</a> </li>
        @endif

    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <h4>Nominations for {{$election->name}}</h4>
            <form action="{{ route('messages.store') }}" method="post">
                @csrf
                <input type="hidden" name="group_id" value="{{ $group->id }}">
                <div class="col-md-12">
                    <!-- Subject Form Input -->
                    <div class="form-group mb-3">
                        <label class="control-label">Nominate for Position</label>
                        <select class="form-select member_positions" name="role_id">
                            <option selected>Choose A Title</option>
                            @foreach ($positions as $position)
                                <option value="{{ $position->id }}">{{ $position->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <br />
                    <table class="table table-hover datatable">
                        <thead>
                        <tr>
                            <th scope="col">User</th>
                            <th scope="col">Type</th>
                            <th scope="col">Nominate</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($users as $user => $u)
                            <tr>
                                <td>
                                    <label for="user_{{ $u->uid }}" title="{{ $u->uname }}">
                                        {{ optional($u)->first_name }} {{ optional($u)->last_name }} {{ $u->uname }}
                                    </label>
                                </td>
                                <td>{{ucfirst($u->member_type)}}</td>
                                <td><input id="user_{{ $u->uid }}" type="radio" name="recipients[]"
                                           value="{{ $u->uid }}"></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>


                    <br />
                    <!-- Submit Form Input -->
                    <div class="form-group mt-3">
                        <button type="submit" class="btn btn-primary form-control">Submit</button>
                    </div>
                </div>
            </form>

        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>

@endsection
