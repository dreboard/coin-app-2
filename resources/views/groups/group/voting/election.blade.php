@extends('layouts.groups.main')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')
        <li class="breadcrumb-item"><a href="{{ route('group.vote_home', ['group' => $group->id]) }}">
                Election Home</a></li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <h4>{{ $election->name }}</h4>
            <div class="card mb-5">
                <div class="card-body">
                    @empty($positions)
                        <p>
                            There are no nominees for this election <a
                                href="{{ route('group.nominate', ['group' => $group->id, 'election' => $election->id]) }}">Please
                                nominate a member</a>
                        </p>
                    @else
                        <div class="election_block">
                            <h3 id="headline">{{ $election->name }} Voting Ends</h3>
                            <p>Please
                                <a href="{{ route('group.vote_election', ['group' => $group->id, 'election' => $election->id]) }}"
                                   class="card-link">VOTE</a>
                            </p>
                            <div id="countdown">
                                <ul>
                                    <li><span id="days"></span>days</li>
                                    <li><span id="hours"></span>Hours</li>
                                    <li><span id="minutes"></span>Minutes</li>
                                    <li><span id="seconds"></span>Seconds</li>
                                </ul>
                            </div>
                        </div>
                    @endempty
                </div>
            </div>

            @foreach($results_by_role as $key => $result)
                <div class="card mb-5">
                    <div class="card-body">
                        <h5 class="card-title">{{ Config::get('constants.board_positions')[$key] }} Results</h5>
                        @foreach($result as $r)
                            <h6 class="card-subtitle mb-2 text-muted mt-3">{{ $r->user->first_name }} {{ $r->user->last_name }}/ {{ $r->user->id }}
                                ({{ $r->candidate_votes }}) Votes</h6>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar"  style="width: {{ ($r->candidate_votes * 2) }}%; background-color: rgb(52, 125, 241);"
                                     aria-label="Election Results" aria-valuenow="{{ $r->candidate_votes }}"
                                     aria-valuemin="0" aria-valuemax="100">{{ $r->candidate_votes }}</div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endforeach

        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>
    @push('styles')

        <style>
            .election_block {
                margin: 0 auto;
                text-align: center;
            }

            .election_block h1 {
                font-weight: normal;
                letter-spacing: .125rem;
                text-transform: uppercase;
            }

            .election_block li {
                display: inline-block;
                list-style-type: none;
                padding: 1em;
                text-transform: uppercase;
            }

            .election_block li span {
                display: block;
            }

        </style>

    @endpush
    @push('scripts')
        <script>
            (function () {

                try {
                    (function () {
                        const second = 1000,
                            minute = second * 60,
                            hour = minute * 60,
                            day = hour * 24;

                        let today = new Date(),
                            dd = String(today.getDate()).padStart(2, "0"),
                            mm = String(today.getMonth() + 1).padStart(2, "0"),
                            yyyy = today.getFullYear(),
                            nextYear = yyyy + 1,
                            dayMonth = "09/30/",
                            birthday = dayMonth + yyyy,
                            election_end = '{{ Carbon\Carbon::parse(optional($election)->expired_at)->format('Y/m/d') }}'; //"11/01/2022";  $election_end

                        today = mm + "/" + dd + "/" + yyyy;
                        if (today > election_end) {
                            //birthday = dayMonth + nextYear;
                        }
                        //end

                        const countDown = new Date(election_end).getTime(),
                            x = setInterval(function () {

                                const now = new Date().getTime(),
                                    distance = countDown - now;

                                document.getElementById("days").innerText = Math.floor(distance / (day)),
                                    document.getElementById("hours").innerText = Math.floor((distance % (day)) / (hour)),
                                    document.getElementById("minutes").innerText = Math.floor((distance % (hour)) / (minute)),
                                    document.getElementById("seconds").innerText = Math.floor((distance % (minute)) / second);

                                //do something later when date is reached
                                if (distance < 0) {

                                    document.getElementById("headline").innerText = "{{ $election->name }}";
                                    document.getElementById("headline").style.display = "none";
                                    document.getElementById("content").style.display = "none";
                                    clearInterval(x);
                                }
                                //seconds
                            }, 0)
                    }());
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>

    @endpush
@endsection
