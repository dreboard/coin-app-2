@extends('layouts.groups.main')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')
        <li class="breadcrumb-item"><a href="{{ route('group.vote_home', ['group' => $group->id]) }}">
                Election Home</a></li>

    </ol>
    <table class="table table-hover table-striped dataTable">
        <thead>
            <th>Election</th>
            <th>View</th>
        </thead>
        <tbody>
        @if(!empty($past_elections))
            @foreach($past_elections as $election)
                <tr>
                    <td class="text-start">{{ $election->name }}</td>
                    <td class="text-end"><a class="btn btn-primary btn-sm"
                                            href="{{ route('group.past_election', ['group' => $group->id, 'election' => $election->id]) }}">Go</a>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td class="text-start">There are no past elections</td>
                <td class="text-end"></td>
            </tr>
        @endif
        </tbody>
    </table>

@endsection
