@extends('layouts.groups.main')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        @include('groups.group.partials.breadcrumb')
        <li class="breadcrumb-item">
            <a href="{{ route('group.posts_home', ['group' => $group->id]) }}">All Posts</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('group.create_post', ['group' => $group->id]) }}">Create An Article</a>
        </li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <h4>Create Article</h4>
            <form id="edit_club_form" action="{{ route('group.save_post_edit') }}"
                  method="post" id="groupForm" enctype="multipart/form-data">
                @csrf
                <div class="col-md-12">
                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                    <input type="hidden" name="group_id" value="{{$post->group_id }}">
                    <input type="hidden" name="post_id" value="{{$post->id }}">
                    <div class="mb-3">
                        <label for="text" class="form-label">Title</label>
                        <input type="text" class="form-control" name="title" id="title" value="{{ old('title', $post->title) }}">
                        <div id="titleHelp" class="form-text">Article Title</div>
                        @error('title')
                        <div class="text-sm text-red-600">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-3">
                        @if ($post->image_url != 'None')
                        <figure class="mb-4">
                            <img src="{{asset('storage/'.$post->image_url)}}" width="250" alt="" />
                        </figure>
                        @endif
                        <label for="image_path" class="form-label">Change image</label>
                            <div class="mt-2">
                                <input class="form-control" type="file" name="image_url" id="image_url" onchange="preview()">
                            </div>
                            <img id="image_preview_frame" src="" width="100px" height="100px"/>
                    </div>

                    <div class="form-group mb-3">
                        <label class="control-label">Message</label>
                        <textarea id="post_body" name="body" class="form-control">{{ old('body', $post->body) }}</textarea>
                    </div>

                    <div class="form-group mb-3">
                        <div class="accordion" id="accordionExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <span id="checkboxgroupText">Tags (Up to 3)</span>
                                    </button>
                                </h2>
                                <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                    <div id="checkboxgroup" class="accordion-body">

                                        @foreach($tags->chunk(3) as $tagGroup)
                                            <div class="row">
                                                @foreach($tagGroup as $tag)
                                                    <div class="col-md-3 form-check">
                                                        <input class="form-check-input" name="tags[]"
                                                               type="checkbox" value="{{$tag->id}}" id="tag_{{$tag->id}}" @if(in_array($tag->id, $saved_tags)) checked @endif>
                                                        <label class="form-check-label" for="tag_{{$tag->id}}">
                                                            {{$tag->name}}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mb-3 form-check">
                        <input type="checkbox" class="form-check-input" name="publish" id="publish">
                        <label class="form-check-label" for="publish">Publish</label>
                    </div>

                    <!-- Submit Form Input -->
                    <div class="col-3">
                        <button type="submit" class="btn btn-primary form-control">Create</button>
                    </div>
                </div>
            </form>
            <div class="mh-100"></div>
        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Your Unpublished</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">

                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">First</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($unpublished as $unpublishedPost)
                                    <tr>
                                        <td>
                                            <a class="group_link"
                                               href="{{ route('group.edit_post', ['group' => $group->id, 'post' => $unpublishedPost->id]) }}"
                                               title="{{$unpublishedPost->title}}">{{ Str::limit($unpublishedPost->title, 20) }}</a>
                                        </td>
                                        <td>
                                            {{ \Carbon\Carbon::parse($unpublishedPost->created_at)->diffForHumans() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Side widget-->
            <div class="card mb-4">
                <div class="card-header">{{ date('M Y') }} Events</div>
                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        @if(!empty($events))

                            @foreach($events as $k => $event)
                                <li class="list-group-item">
                                    <a href="{{ route('group.view', ['group' => $group->id]) }}">
                                        {{ Carbon\Carbon::parse($event->start_at)->format('jS') }} | {{ $event->title }}
                                    </a></li>
                            @endforeach
                        @else
                            <li class="list-group-item">No events for {{ date('M Y') }}, <a href="{{ route('group.view', ['group' => $group->id]) }}">Full Schedule</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>

    </div>
    @push('styles')
        <style>
            .ck-editor__editable {
                min-height: 300px;
            }
        </style>
    @endpush
    @include('groups.group.partials.post_script')
@endsection
