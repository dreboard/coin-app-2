@extends('layouts.groups.main')
@section('pageTitle', $group->name.' Group Articles')
@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        @include('groups.group.partials.breadcrumb')

    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <h5>Group Articles</h5>
            <div class="mb-2">
                <a class="btn btn-primary btn-sm"
                   href="{{ route('group.create_post', ['group' => $group->id]) }}">Create An Article</a>
            </div>

            <!-- start posts -->
            @if(!empty($posts))
                    @foreach($posts as $post)
                        <div class="card row-hover pos-relative py-3 px-3 mb-3 border-primary border-top-0 border-right-0 border-bottom-0 rounded-0">
                            <div class="row align-items-center">
                                <div class="col-md-8 mb-3 mb-sm-0">
                                    <h5>
                                        <a href="{{ route('group.view_post', ['group' => $group->id, 'post' => $post->id]) }}" class="text-primary">{{ $post->title }}</a>
                                    </h5>
                                    <p class="text-sm"><span class="op-6">Posted</span> <a class="text-black" href="{{ route('group.view_post', ['group' => $group->id, 'post' => $post->id]) }}">{{ Carbon\Carbon::parse($post->created_at)->diffForHumans() }}</a> <span class="op-6">ago by</span>
                                        <a class="text-black" href="{{ route('group.view_editor', ['group' => $group->id]) }}">The Editor</a></p>
                                    <div class="text-sm op-5">
                                        @foreach($post->tags as $tag)
                                            <a class="text-black mr-2" href="#">#{{$tag->name}}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-md-4 op-7">
                                    <div class="row text-center op-7">
                                        <div class="col px-1"> <i class="ion-ios-chatboxes-outline icon-1x"></i> <span class="d-block text-sm">{{$post->comments->count()}} Replys</span> </div>
                                        <div class="col px-1"> <i class="ion-ios-eye-outline icon-1x"></i> <span class="d-block text-sm">{{$post->views}} Views</span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                {{ $posts->links('pagination::bootstrap-5') }}
            @else
                <p>No Current Posts</p>
            @endif


            <!-- End posts -->




        </div>

        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>

@endsection
