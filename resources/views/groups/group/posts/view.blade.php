@extends('layouts.groups.main')
@section('pageTitle', $group->name.' View Post')
@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        @include('groups.group.partials.breadcrumb')
        <li class="breadcrumb-item">
            <a href="{{ route('group.posts_home', ['group' => $group->id]) }}">All Posts</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('group.create_post', ['group' => $group->id]) }}">Create An Article</a>
        </li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            @if($post->user_id == auth()->user()->id)
                <div  class="mb-4">
                    <a class="group_link btn btn-primary"
                       href="{{ route('group.edit_post', ['group' => $group->id, 'post' => $post->id]) }}" >
                        Edit Post</a>
                </div>
            @endif
            <!-- start posts -->
            <div class="card row-hover pos-relative py-3 px-3 mb-3 border-primary border-top-0 border-right-0 border-bottom-0 rounded-0">
                <div class="row align-items-center">
                    <div class="col-md-8 mb-3 mb-sm-0">
                        <h5>
                            {{$post->title}}
                        </h5>
                        <p class="text-sm"><span class="op-6">Posted</span> <a class="text-black" href="#">{{ Carbon\Carbon::parse($post->created_at)->diffForHumans() }}</a>
                            By <a class="text-black" href="{{ route('group.view_editor', ['group' => $group->id]) }}">The Editor</a></p>
                        <div class="text-sm op-5">

                            @foreach($post->tags as $tag)
                                <a class="text-black mr-2" href="#">#{{$tag->name}}</a>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-4 op-7">
                        <div class="row text-center op-7">
                            <div class="col px-1">  <span class="d-block text-sm">{{$post->comments_count}} Replys</span> </div>
                            <div class="col px-1"> <span class="d-block text-sm">{{$post->views}} Views</span> </div>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <figure class="mb-4">
                    <img src="{{asset('storage/'.$post->image_url)}}" width="250" alt="" />
                </figure>

                <p class="mt-3 mb-3">{!! $post->body !!}</p>
            </div>
            <!-- End posts -->

            <section class="mb-5">
                <div class="card bg-light">
                    <div class="card-body">
                        <!-- Comment form-->
                        @include('groups.group.partials.comment_replies', ['comments' => $post->comments, 'post_id' => $post->id])
                        <form class="mb-4" action="{{ route('group.save_post_comment') }}" method="post">
                            @csrf
                            <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                            <input type="hidden" name="post_id" value="{{$post->id }}">
                            <input type="hidden" name="group_id" value="{{$post->group_id }}">

                            <div class="form-group mb-3">
                                <label class="control-label">Comment</label>
                                <textarea id="comment_body" name="comment_body" class="form-control" rows="3"
                                          placeholder="Join the discussion and leave a comment!"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                    </div>
                </div>
            </section>

        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>
    @push('styles')
        <style>
            .display-comment .display-comment {
                margin-left: 40px
            }
            .ck-editor__editable {
                min-height: 250px;
            }
        </style>
    @endpush
    @push('scripts')
        <script src="https://cdn.ckeditor.com/ckeditor5/35.2.1/classic/ckeditor.js"></script>
        <script>
            (function () {
                try {
                    ClassicEditor.defaultConfig = {
                        toolbar: {
                            items: [
                                'heading',
                                '|',
                                'bold',
                                'italic',
                                '|',
                                'bulletedList',
                                'numberedList',
                                '|',
                                'insertTable',
                                '|',
                                'undo',
                                'redo'
                            ]
                        },
                        image: {
                            toolbar: [
                                'imageStyle:full',
                                'imageStyle:side',
                                '|',
                                'imageTextAlternative'
                            ]
                        },
                        table: {
                            contentToolbar: [ 'tableColumn', 'tableRow', 'mergeTableCells' ]
                        },
                        language: 'en'
                    };



                    ClassicEditor
                        .create( document.querySelector( '#comment_body' )).catch( error => {
                        console.error( error );
                    } );

                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
