@extends('layouts.groups.main')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Manage</a> </li>

    </ol>
    <div class="row">
        <div class="col-8">
            <div class="alert alert-primary" role="alert">
                Announcements
            </div>

        </div>
        <div class="col-4">

            <h3>{{ date('M') }} Events</h3>
            <ul class="list-group">
                <li class="list-group-item">meet Events</li>
                <li class="list-group-item">A second item</li>
                <li class="list-group-item">A third item</li>
                <li class="list-group-item">A fourth item</li>
                <li class="list-group-item"><a href="{{ route('group.calendar', ['group' => $group->id]) }}">Club Calendar</a></li>
            </ul>
        </div>
    </div>
    @push('scripts')
        <script>

            (function() {

                try{
                    // profile_visibility_on
                    const pro_vis = document.getElementById("profile_visibility_check").value;
                    const profile_visibility_label = document.getElementById("profile_visibility_label");
                    document.getElementById("profile_visibility_check").addEventListener('click', getData);
                    if (typeof pro_vis !== 'undefined') {
                        console.log(pro_vis);
                    } else {
                        console.log('nope');
                    }
                    console.log(SITE_URL);
                    console.log(ENVIRONMENT);
                    console.log(CSRF_TOKEN);
                    console.log(USER_ID);

                    function getData(){
                        fetch(SITE_URL+'/user/user_change_visibility', {
                            headers: { "Content-Type": "application/json; charset=utf-8" },
                            method: 'POST',
                            body: JSON.stringify({
                                _token: @json(csrf_token()),
                                id: {{ auth()->user()->id }},
                                profile_visibility: document.getElementById("profile_visibility_check").value
                            })
                        }).then(response => response.json())
                            .then(data => {
                                document.getElementById("profile_visibility_check").value = data;
                                if(data == 1){
                                    profile_visibility_label.innerHTML = 'Public';
                                    profile_visibility_label.classList.add('text-success');
                                    profile_visibility_label.classList.remove('text-danger');
                                } else {
                                    profile_visibility_label.innerHTML = 'Private';
                                    profile_visibility_label.classList.add('text-danger');
                                    profile_visibility_label.classList.remove('text-success');
                                }
                                console.log('Success:', data);
                                console.log(profile_visibility_label.innerHTML);
                            })
                        //.then(data => console.log(data))
                    }

                }catch (error){
                    if (ENVIRONMENT === 'local'){
                        console.error(error);
                    }

                }




            })();



        </script>

    @endpush
@endsection
