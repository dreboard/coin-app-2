@extends('layouts.groups.main')

@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        @include('groups.group.partials.breadcrumb')

    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <h4>Create Article</h4>
            <form id="edit_club_form" action="{{ route('group.save_post') }}"
                  method="post" id="groupForm" enctype="multipart/form-data">
                @csrf
                <div class="col-md-12">
                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                    <input type="hidden" name="group_id" value="{{ $group->id }}">
                    <div class="mb-3">
                        <label for="text" class="form-label">Title</label>
                        <input type="text" class="form-control" name="title" id="title" value="{{ old('title') }}">
                        <div id="titleHelp" class="form-text">Article Title</div>
                        @error('title')
                        <div class="text-sm text-red-600">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="image_path" class="form-label">Attach an image</label>
                        <input class="form-control" type="file" name="image_url" id="image_url" onchange="preview()">
                        <img id="image_preview_frame" src="" width="100px" height="100px"/>
                    </div>

                    <div class="form-group mb-3">
                        <label class="control-label">Article Text</label>
                        <textarea id="post_body" name="body" class="form-control">{{ old('body') }}</textarea>
                    </div>

                    <div class="form-group mb-3">
                        <div class="accordion" id="accordionExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <span id="checkboxgroupText">Tags (Up to 3)</span>
                                    </button>
                                </h2>
                                <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                    <div id="checkboxgroup" class="accordion-body">

                                        @foreach($tags->chunk(3) as $tagGroup)
                                            <div class="row">
                                                @foreach($tagGroup as $tag)
                                                    <div class="col-md-3 form-check">
                                                        <input class="form-check-input" name="tags[]" type="checkbox" value="{{$tag->id}}" id="tag_{{$tag->id}}">
                                                        <label class="form-check-label" for="tag_{{$tag->id}}">
                                                            {{$tag->name}}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mb-3 form-check">
                        <input type="checkbox" class="form-check-input" name="publish" id="publish">
                        <label class="form-check-label" for="publish">Publish</label>
                    </div>

                    <!-- Submit Form Input -->
                    <div class="col-3">
                        <button type="submit" class="btn btn-primary form-control">Create</button>
                    </div>
                </div>
            </form>
            <div class="mh-100"></div>
        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Your Unpublished</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">

                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">First</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($unpublished as $unpublishedPost)
                                    <tr>
                                        <td>
                                            <a class="group_link"
                                               href="{{ route('group.edit_post', ['group' => $group->id, 'post' => $unpublishedPost->id]) }}"
                                               title="{{$unpublishedPost->title}}">{{ Str::limit($unpublishedPost->title, 20) }}</a>
                                        </td>
                                        <td>
                                            {{ \Carbon\Carbon::parse($unpublishedPost->created_at)->diffForHumans() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>



                            <ul class="list-unstyled mb-0 group_links">
                                <li><a class="group_link" id="members_link" href="{{ route('group.view_users', ['group' => $group->id]) }}" title="View Club Members">Members</a></li>
                                <li><a class="group_link" id="officer_link" href="{{ route('group.view', ['group' => $group->id]) }}" title="Assign Group Officers">Officers</a></li>
                                <li><a class="group_link" id="committee_link" href="{{ route('group.committee_index', ['group' => $group->id]) }}" title="View/Create Committees">Committees</a></li>
                                <li><a class="group_link" id="message_link" href="{{ route('group.message_index', ['group' => $group->id]) }}" title="Create Group Messages">Group Messages</a></li>
                                <li><a class="group_link" id="board_link" href="{{ route('group.view', ['group' => $group->id]) }}" title="View/Create Message Board">Message Board</a></li>
                                <li><a class="group_link" id="library_link" href="{{ route('group.library_index', ['group' => $group->id]) }}" title="Maintain Group Library">Library</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Side widget-->
            <div class="card mb-4">
                <div class="card-header">{{ date('M Y') }} Events</div>
                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        @if(!empty($events))

                            @foreach($events as $k => $event)
                                <li class="list-group-item">
                                    <a href="{{ route('group.view', ['group' => $group->id]) }}">
                                        {{ Carbon\Carbon::parse($event->start_at)->format('jS') }} | {{ $event->title }}
                                    </a></li>
                            @endforeach
                        @else
                            <li class="list-group-item">No events for {{ date('M Y') }}, <a href="{{ route('group.view', ['group' => $group->id]) }}">Full Schedule</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>

    </div>
    @push('styles')
        <style>
            .ck-editor__editable {
                min-height: 300px;
            }
        </style>
    @endpush
    @include('groups.group.partials.post_script')
@endsection
