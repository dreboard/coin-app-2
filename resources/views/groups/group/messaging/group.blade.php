@extends('layouts.groups.main')
@section('pageTitle', $group->name.' User Messages')
@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        <li class="breadcrumb-item"><a href="{{ route('group.view', ['group' => $group->id]) }}">Manage</a> </li>

    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <h4>User Messages</h4>
            <form action="{{ route('messages.store') }}" method="post">
                @csrf
                <input type="hidden" name="group_id" value="{{ $group->id }}">
                <div class="col-md-12">
                    <!-- Subject Form Input -->
                    <div class="form-group mb-3">
                        <label class="control-label">Subject</label>
                        <input type="text" class="form-control" name="subject" placeholder="Subject"
                               value="{{ old('subject') }}">
                    </div>

                    <!-- Message Form Input -->
                    <div class="form-group mb-3">
                        <label class="control-label">Message</label>
                        <textarea id="message" name="message" class="form-control">{{ old('message') }}</textarea>
                    </div>
                    <br />
                    <table class="table table-hover datatable">
                        <thead>
                        <tr>
                            <th scope="col">User</th>
                            <th scope="col">Type</th>
                            <th scope="col">Include</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($users as $user => $u)
                            <tr>
                                <td>
                                    <label for="user_{{ $u->uid }}" title="{{ $u->uname }}">
                                        {{ $u->uname }}
                                    </label>
                                </td>
                                <td>{{ucfirst($u->member_type)}}</td>
                                <td><input id="user_{{ $u->uid }}" type="checkbox" name="recipients[]"
                                           value="{{ $u->uid }}"></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>


                    <br />
                    <!-- Submit Form Input -->
                    <div class="form-group mt-3">
                        <button type="submit" class="btn btn-primary form-control">Submit</button>
                    </div>
                </div>
            </form>

        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>

@endsection
