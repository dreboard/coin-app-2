@extends('layouts.groups.main')
@section('pageTitle', $group->name.' Inbox')
@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        <li class="breadcrumb-item"><a href="{{ route('group.view', ['group' => $group->id]) }}">Manage</a> </li>

    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <h4>Inbox</h4>

            <div class="mb-2">
                <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#delMsgModal">Delete All</button>
                    <button type="button" class="btn btn-warning">Mark All As Read</button>
                    <a class="btn btn-primary" href="{{ route('group.message_group', ['group' => $group->id]) }}" role="button">Create</a>
                    @if (auth()->user()->is_admin)

                    @endif

                </div>
            </div>

            <div class="modal fade" id="delMsgModal" tabindex="-1" aria-labelledby="delMsgModal" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="delMsgModalLabel">Delete All Messages</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            This can not be undone

                            <form class="row row-cols-lg-auto g-3 align-items-center"
                                  action="{{ route('group.group_message_delete_all') }}" method="post">
                                @csrf
                                <input type="hidden" name="group_id" value="{{ $group->id }}">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-danger">Yes Delete</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="main-body">
                <table class="table table-hover datatable">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">From</th>
                        <th scope="col">Subject</th>
                        <th scope="col">Posted</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($group->notifications as $notification)
                        <tr>
                            <th scope="row">1</th>
                            <td>{{$notification->data['from']}}</td>
                            <td><a href="{{ route('group.message_view', ['group' => $group, 'id' => $notification->id]) }}">{{$notification->data['subject']}}</a></td>
                            <td>{{$notification->created_at ?? $notification->read_at}}</td>
                        </tr>
                    @empty
                        <tr>
                            <th scope="row"></th>
                            <td></td>
                            <td>There are no new notifications</td>
                            <td></td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

            </div>



        </div>
        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>

@endsection
