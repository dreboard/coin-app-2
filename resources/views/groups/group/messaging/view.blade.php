@extends('layouts.groups.main')
@section('pageTitle', $group->name.' View Message')
@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')

    <h3 class="mt-4">My Messages</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        <li class="breadcrumb-item"><a href="{{ route('user.message_all') }}">Messages</a> </li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-12">

                    <div class="small text-muted">
                        {{optional($notification->read_at)->format('F jS Y, g:ia') ?? $notification->created_at->format('F jS Y, g:ia') }}
                    </div>
                    <h2 class="card-title">{{$notification->data['subject']}}</h2>
                    <p class="card-text">{!!$notification->data['body']!!}</p>

                    <form class="row row-cols-lg-auto g-3 align-items-center" method="post"
                          action="{{ route('group.group_message_delete') }}">
                        @csrf
                        <input type="hidden" name="notification_id" value="{{ $notification->id }}">
                        <input type="hidden" name="group_id" value="{{ $group->id }}">
                        <div class="col-12">
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </div>
                    </form>

            <hr />
            <h3>Reply</h3>
            <div class="card mb-4">
                <div class="card-body">
                    <div class="small text-muted">
                        {{optional($notification->read_at)->format('F jS Y, g:ia') ?? $notification->created_at->format('F jS Y, g:ia') }}
                    </div>
                    <h2 class="card-title">{{$notification->data['subject']}}</h2>
                    <p class="card-text">{!!$notification->data['body']!!}</p>

                    <form method="post"
                          action="{{ route('group.group_message_delete', ['group' => $group, 'id' => $notification->id]) }}">
                        @csrf
                        <input type="hidden" name="notification_id" value="{{ $notification->id }}">
                        <input type="hidden" name="group_id" value="{{ $group->id }}">
                        <input type="hidden" name="recipient" value="{{ $notification->data['from'] }}">
                        <div class="mb-3">
                            <label for="group_message_body" class="form-label">Message</label>
                            <textarea class="form-control" id="group_message_body" name="body" rows="3"></textarea>
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary">Delete</button>
                        </div>
                    </form>
                </div>
            </div>


        </div>
    </div>


@endsection

