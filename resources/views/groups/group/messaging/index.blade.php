@extends('layouts.groups.main')
@section('pageTitle', $group->name.' Group Message Home')
@section('content')

    @include('groups.group.partials.header')
    <ol class="breadcrumb mb-4">
        @include('groups.group.partials.breadcrumb')

    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <h5>Group Messages</h5>
            <div class="row">
                <div class="col-6">
                    <div class="callout callout-primary mb-4" id="group_announcement_banner">
                        <h4 class="card-title h4">Create a Group Message</h4>
                        <p class="card-text">Send a message to one or all group members</p>
                        <a class="btn btn-primary btn-sm"
                           href="{{ route('group.message_group', ['group' => $group->id]) }}">Create An Message</a>
                    </div>
                </div>
                <div class="col-6">
                    <div class="callout callout-primary mb-4" id="group_announcement_banner">
                        <h4 class="card-title h4">Group Inbox</h4>
                        <p class="card-text">Inbox for the group</p>
                        <a class="btn btn-primary btn-sm"
                           href="{{ route('group.group_inbox', ['group' => $group->id]) }}">Group Inbox</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Side widgets-->
        @include('groups.group.partials.side')
    </div>

@endsection
