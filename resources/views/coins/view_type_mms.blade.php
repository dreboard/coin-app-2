@extends('layouts.user.main')
@section('pageTitle', "View Type Mintmark Style")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">{{ $coinType->coinType }} Mintmark Style</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    View
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Coin</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_category', ['id' => $coinType->coincats_id]) }}">{{ $coinType->coinCategory }}</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">{{ $coinType->coinType }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('coin.view_category', ['id' => $coinType->coincats_id]) }}">{{ $coinType->coinCategory }}</a>
        </li>
        <li class="breadcrumb-item"><a
                href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">{{ $coinType->coinType }}</a>
        </li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                {{ $coinType->coinType }} <span class="fw-bold">{{$mint}} {{$mms}}</span> Mintmark Style Coins
            </div>


            <div class="card mb-3">
                <div class="card-body">

                    <div class="row">
                        <div class="col-sm-4">
                            <img src="{{config('app.image_url')}}{{ str_replace(' ', '_', $coinType->coinType) }}.jpg"/>
                        </div>
                        <div class="col-sm-8">
                            <h5 class="card-title">Details</h5>
                            <table class="table">
                                <tr>
                                    <td><span class="fw-bold">Style:</span></td>
                                    <td class="w-75"><a href="{{ route('coin.view_type_mint', ['coinType' => $coinType->id, 'mint' => str_replace(' ', '_', $mint)]) }}">
                                            {{ $mint }}
                                        </a> {{$mms}}</td>
                                </tr>

                                <tr>
                                    <td><span class="fw-bold">Switch:</span></td>
                                    <td class="w-75">
                                        <div class="dropdown">
                                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                                    aria-expanded="false">
                                               Switch Style
                                            </button>
                                            <ul class="dropdown-menu">
                                                @foreach($mm_styles as $mm_style)
                                                    <li><a class="dropdown-item" href="{{ route('coin.view_type_mms', [
                                                    'coinType' => $coinType->id,
                                                    'mint' => $mint,
                                                    'mms' => $mm_style,
                                                    ]) }}">{{$mint}} {{$mm_style}}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </td>
                                </tr>


                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Varieties</h4>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Do Something
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('member.posts_index') }}">See All</a></li>
                                <li><a class="dropdown-item" href="{{ route('member.create_posts') }}">Create (Found A
                                        new variety)</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Errors</h4>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Do Something
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('member.view_books') }}">See All</a></li>
                                <li><a class="dropdown-item" href="{{ route('member.create_posts') }}">Create (Found A
                                        new error)</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Discussion Area</h4>
                        <a class="btn btn-secondary" href="{{ route('member.forum') }}">Go →</a>
                    </div>
                </div>
            </div>


            <hr/>
            <!-- Featured blog post-->


            <!-- Nested row for non-featured blog posts-->
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-hover datatable">
                        <thead>
                        <tr>
                            <th scope="col">Type</th>
                            <th scope="col">Collected</th>
                            <th scope="col">Investment</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($coins as $key => $coin)
                            <tr>
                                <td class="w-75">
                                    <a href="{{ route('coin.view_coin', ['coin' => $key]) }}">{{ Str::limit($coin['coin']->coinName, 55) }}</a>
                                </td>
                                <td class="text-center">
                                    {{$coin['collected']}}
                                </td>
                                <td class="text-center">
                                    {{$coin['investment']}}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td></td>
                                <td>No {{ $coinType->coinType }} {{$mint}} {{$mms}} coins saved</td>
                                <td></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>

                <div class="col-lg-12">
                    <a class="btn btn-secondary" href="#!">See All Projects</a>
                </div>
            </div>
        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Switch Type</div>
                <div class="card-body">
                    <div class="input-group">
                        <select class="form-select" id="coin_id_select"
                                aria-label="Example select with button addon">
                            <option selected>Choose...</option>
                            @foreach(Config::get('constants.coins.types_list')[$coinType->coincats_id] as $key => $type)
                                <option value="{{ $key }}">{{ $type }}</option>
                            @endforeach
                        </select>
                        <button id="coin_id_select_btn" class="btn btn-outline-primary" type="button">Load</button>
                    </div>
                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Reports</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                @if(in_array($coinType->coinType, Config::get('constants.coins.lincoln_reverse')))
                                    <li><a href="{{ route('coin.view_type_group', ['type' => 'lincoln_reverse']) }}">All
                                            Lincoln Cents</a></li>

                                @endif
                                @if(in_array($coinType->coinType, Config::get('constants.coins.jefferson_reverse')))
                                    <li><a href="{{ route('coin.view_type_group', ['type' => 'jefferson_reverse']) }}">All
                                            Jefferson Nickels</a></li>
                                @endif

                                    @if(str_contains($coinType->coinType, 'Seated Liberty') || $coinType->id == 44)
                                        <li><a href="{{ route('coin.seated_index') }}">Seated Liberty</a></li>

                                    @endif
                                <li><a href="{{ route('coin.type_grades', ['coinType' => $coinType->id]) }}">Grade
                                        Report</a></li>
                                <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">Proof Report</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                @if(in_array($coinType->category->id, Config::get('constants.coins.color_categories')))
                                    <li><a href="{{ route('coin.view_type_color', ['id' => $coinType->id]) }}">Color
                                            Report</a></li>
                                @endif

                                @if($coinType->coinType == 'Lincoln Wheat')
                                    <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">Color
                                            Report</a></li>
                                    <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">BIE Errors</a>
                                    </li>
                                    <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">Coppercoins
                                            List</a></li>
                                @endif



                                @if($coinType->coinType == 'Indian Head Cent' || $coinType->coinType == 'Flying Eagle')
                                    <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">By Snow
                                            Numbers</a></li>
                                @endif

                                @if($coinType->coinType == 'Morgan Dollar' || $coinType->coinType == 'Peace Dollar')
                                    <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">VAMs</a></li>
                                @endif

                                @if($coinType->coinType == 'Standing Liberty')
                                    <li><a href="{{ route('coin.view_type_head', ['id' => $coinType->id]) }}">Full Head
                                            Report</a></li>
                                @endif
                                @if($coinType->coinType == 'Jefferson Nickel')
                                    <li><a href="{{ route('coin.view_type_steps', ['id' => $coinType->id]) }}">Full
                                            Steps Report</a></li>
                                @endif
                                @if($coinType->coinType == 'Franklin Half Dollar')
                                    <li><a href="{{ route('coin.view_type_bell', ['id' => $coinType->id]) }}">Full Bell
                                            Lines Report</a></li>
                                @endif

                                @if(in_array($coinType->coinType, Config::get('constants.coins.full_band_types')))
                                    <li><a href="{{ route('coin.view_type_bands', ['id' => $coinType->id]) }}">Full
                                            Bands Report</a></li>
                                @endif

                                @if($coinType->coinType == 'Lincoln Wheat' || $coinType->coinType == 'Lincoln Memorial')
                                    <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">Coppercoins
                                            List</a></li>
                                @endif


                                <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">Damage Report</a>
                                </li>
                                <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">Variety Report</a>
                                </li>
                                <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">Error Report</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        @if(!empty($typeLinks))
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">

                                    @foreach($typeLinks as $src => $link)
                                        <li><a class="external_link" href="{{$link}}">{{$src}}</a></li>
                                    @endforeach

                                </ul>
                            </div>
                        @endif
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                @if($coinType->category->id === 63)
                                    <li><a class="external_link" href="http://www.smalldollars.com/index.html">SmallDollars.com</a>
                                    </li>
                                @endif
                                @if(!empty($typeSpecificLinks))

                                    @foreach($typeSpecificLinks as $src => $link)
                                        <li><a class="external_link" href="{{$link}}">{{$src}}</a></li>
                                    @endforeach

                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @push('scripts')
        <script>
            (function (ENVIRONMENT) {
                try {
                    //const coin_type_id = {{$coinType->id}};

                    const coin_id_select = document.getElementById("coin_id_select");
                    const coin_id_select_btn = document.getElementById("coin_id_select_btn");

                    coin_id_select_btn.addEventListener("click", function () {
                        let coin_type_id = coin_id_select.options[coin_id_select.selectedIndex].value;
                        window.location.href = "{{ route('coin.view_type', [':id']) }}".replace(':id', coin_type_id);
                        //window.location.href = url;
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection

