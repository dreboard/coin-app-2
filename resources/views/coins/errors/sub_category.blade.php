@extends('layouts.user.main')
@section('pageTitle', 'Coin Errors Home')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Coin Errors, {{$subcategory}}</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Profile Pages
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">Coins</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.edit_public', ['user' => auth()->user()->id]) }}">Currency</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_edit_avatar', ['user' => auth()->user()->id]) }}">Changmage</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.error_index') }}">Error Home</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                View/Save U.S. Error Coins.  Info obtained from the <a class="external_link" href="https://www.error-ref.com/error_and_variety_check_list/">Comprehensive Error-Variety Checklist</a>
            </div>

            <h4>Subcategories for {{$subcategory}} Errors</h4>

            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="fw-bold">Subcategory</th>
                        <th class="fw-bold">Collected</th>
                        <th class="fw-bold">Forum</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($master_list as $error)
                        <tr>
                            <th class="fw-bold">
                                <a href="{{ route('coin.error_view', ['error' => $error->id]) }}">
                                    @if($error->type == 'None') All {{$subcategory}} @else {{$error->type}} @endif
                                     @if($error->sub_type !== 'None') / {{$error->sub_type}} @endif
                                </a>
                            </th>
                            <td><a href="{{ route('member.forum') }}">View</a></td>
                            <td><a href="{{ route('member.forum') }}">View</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>


            <div id="set_type_div" class="row">
                <div class="col-sm-6 mb-3">
                    <div class="card text-center">
                        <div class="card-body">
                            <h5 class="card-title">Discussions</h5>
                            <p class="card-text">There are currently {{count($forum_posts)}} posts.</p>
                            <a href="{{ route('member.forum') }}" class="btn btn-primary">View</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 mb-3">
                    <div class="card text-center">
                        <div class="card-body">
                            <h5 class="card-title">Collected {{$subcategory}} Errors</h5>
                            <p class="card-text">Collected {{count($forum_posts)}}.</p>
                            <a href="{{ route('coin.design_type_set', ['design' => 'lincoln']) }}" class="btn btn-primary">View</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 mb-3">
                    <div class="card text-center">
                        <div class="card-body">
                            <h5 class="card-title">Articles</h5>
                            <p class="card-text">There are currently {{count($forum_posts)}} articles.</p>
                            <a href="{{ route('coin.design_type_set', ['design' => 'lincoln']) }}" class="btn btn-primary">View</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Search For Error</div>
                <div class="card-body">
                    <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0"
                          id="error_search_form" method="post" action="{{ route('coin.search_error') }}">
                        @csrf
                        <div class="input-group">
                            <input class="form-control" type="text" placeholder="Find Error..." aria-label="Search for..." aria-describedby="errorSearch"
                                   name="find_error"/>
                            <button class="btn btn-primary" id="errorSearch" type="submit"><i class="fas fa-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Switch Error Type</div>
                <div class="card-body">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                data-bs-toggle="dropdown" aria-expanded="false">
                            Go
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            @foreach($error_categories as $key => $value)
                                <li><a class="dropdown-item" href="{{ route('coin.error_category', ['category' => str_replace(' ', '_', htmlentities($value))]) }}">{{$value}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link"
                                       href="https://www.error-ref.com/">The Error – Variety Ready Reference</a>
                                </li>
                                <li><a class="external_link" href="https://minterrornews.com/">Mint Error News</a></li>
                                <li><a class="external_link" href="https://conecaonline.org/">CONECA</a></li>
                                <li><a class="external_link" href="http://www.doubleddie.com/1801.html">Wexler’s Die
                                        Varieties</a></li>
                                <li><a class="external_link" href="http://varietyvista.com/Attribution%20Services.htm">Variety
                                        Vista Attribution Services</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="http://www.maddieclashes.com/">MAD Die Clashes</a>
                                </li>
                                <li><a class="external_link" href="http://www.cuds-on-coins.com/">Cuds-On-Coins</a></li>
                                <li><a class="external_link" href="http://www.traildies.com/">Trail Dies</a></li>
                                <li><a class="external_link"
                                       href="https://www.error-ref.com/">The Error – Variety Ready Reference</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">

                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.view_directory') }}">User Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            @endif

        </div>
    </div>

@endsection

