@extends('layouts.user.main')
@section('pageTitle', "View Type - $coinType->coinType")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">{{ $coinType->coinType }}, {{$design}}</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    View
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Coin</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Mintset</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Other</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('coin.view_category', ['id' => $coinType->coincats_id]) }}">{{ $coinType->coinCategory }}</a>
        </li>
        <li class="breadcrumb-item"><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">{{$coinType->coinType}}</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                {{ $coinType->coinType }} {{$design}} collected coins and resources.
            </div>


            <div class="card mb-3">
                <div class="card-body">

                    <div class="row">
                        <div class="col-sm-4">
                            <img src="{{config('app.image_url')}}{{ str_replace(' ', '_', $design) }}.jpg"/>
                        </div>
                        <div class="col-sm-8">
                            <h5 class="card-title">Details</h5>
                            <table class="table">
                                <tr>
                                    <td><span class="fw-bold">Design:</span></td>
                                    <td class="w-75">{{$design}}</td>
                                </tr>
                                <tr>
                                    <td><span class="fw-bold">Minted:</span></td>
                                    <td class="w-75">{{$year}}</td>
                                </tr>
                                <tr>
                                    <td><span class="fw-bold">Metals:</span></td>
                                    <td class="w-75">
                                        @foreach (explode(', ', $coinType->metals) as $metal)
                                            <a href="{{ route('coin.view_type_mint', ['coinType' => $coinType->id, 'mint' => str_replace(' ', '_', $metal)]) }}">
                                                {{ $metal }},
                                            </a>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="fw-bold">Mints:</span></td>
                                    <td class="w-75">
                                        @foreach (explode(', ', $coinType->mints) as $mint)
                                            <a href="{{ route('coin.view_type_mint', ['coinType' => $coinType->id, 'mint' => str_replace(' ', '_', $mint)]) }}">
                                                {{ $mint }},
                                            </a>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="fw-bold">Mintmark:</span></td>
                                    <td>{{$coinType->mark_place}}</td>
                                </tr>
                                <tr>
                                    <td><span class="fw-bold">Strikes:</span></td>
                                    <td>
                                        @foreach($strikes as $strike)
                                            <a href="{{ route('coin.view_type_strike', ['coinType' => $coinType->id, 'strike' => str_replace(' ', '_', $strike)]) }}">{{$strike}}</a>
                                            @if(!$loop->last)
                                                ,
                                            @endif
                                        @endforeach
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <div class="card">
                <div class="card-body">
                    <table class="table">
                        @if(isset($design_varieties['obverse_coneca'][0]))
                            <tr>
                                <td><span class="fw-bold">Obverse Designs:</span></td>
                                <td>
                                    @foreach($design_varieties['obverse_coneca'] as $obverse)
                                        <a href="{{ route('coin.view_type_design_detail', [
                                                    'coinType' => $coinType->id,
                                                    'detail' => 'obverse',
                                                    'variety' => urlencode(urlencode($obverse)),
                                                    ]) }}">{{$obverse}}</a>
                                        @if(!$loop->last)
                                            ,
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                        @endif

                        @if(isset($design_varieties['reverse_coneca'][0]))
                            <tr>
                                <td><span class="fw-bold">Reverse Designs:</span></td>
                                <td>
                                    @foreach($design_varieties['reverse_coneca'] as $reverse)
                                        <a href="{{ route('coin.view_type_design_detail', [
                                                    'coinType' => $coinType->id,
                                                    'detail' => 'reverse',
                                                    'variety' => urlencode(urlencode($reverse)),
                                                    ]) }}">{{$reverse}}</a>
                                        @if(!$loop->last)
                                            ,
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                        @endif
                    </table>

                    @if(isset($mintmarkStyles['Philadelphia'][1]) || isset($mintmarkStyles['Denver'][1]) || isset($mintmarkStyles['San Francisco'][1]))
                        <h5>Mintmark Styles</h5>
                    @endif

                    <table>
                        @if(isset($mintmarkStyles['Denver'][1]))
                            <tr>
                                <td><span class="fw-bold">Denver: </span> </td>
                                <td>
                                    @foreach($mintmarkStyles['Denver'] as $mintmark)
                                        <a href="{{ route('coin.view_type_mms', [
                                                    'coinType' => $coinType->id,
                                                    'mint' => 'Denver',
                                                    'mms' => $mintmark,
                                                    ]) }}">{{$mintmark}}</a>
                                        @if(!$loop->last)
                                            ,
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                        @endif
                        @if(isset($mintmarkStyles['San Francisco'][1]))
                            <tr>
                                <td><span class="fw-bold">San Francisco: </span> </td>
                                <td>
                                    @foreach($mintmarkStyles['San Francisco'] as $mintmark)
                                        <a href="{{ route('coin.view_type_mms', [
                                                    'coinType' => $coinType->id,
                                                    'mint' => 'San_Francisco',
                                                    'mms' => $mintmark,
                                                    ]) }}">{{$mintmark}}</a>
                                        @if(!$loop->last)
                                            ,
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                        @endif
                            @if(isset($mintmarkStyles['Philadelphia'][1]))
                                <tr>
                                    <td><span class="fw-bold">Philadelphia: </span> </td>
                                    <td>
                                        @foreach($mintmarkStyles['Philadelphia'] as $mintmark)
                                            <a href="{{ route('coin.view_type_mms', [
                                                    'coinType' => $coinType->id,
                                                    'mint' => 'Philadelphia',
                                                    'mms' => $mintmark,
                                                    ]) }}">{{$mintmark}}</a>
                                            @if(!$loop->last)
                                                ,
                                            @endif
                                        @endforeach
                                    </td>
                                </tr>
                            @endif

                    </table>
                </div>
            </div>


            <p class="mt-3">
                <a class="btn btn-primary" data-bs-toggle="collapse" href="#collapseExample" role="button"
                   aria-expanded="false" aria-controls="collapseExample">
                    Bulk Data
                </a>

            </p>
            <div class="collapse" id="collapseExample">
                <div class="card">
                    <div class="card-body">
                        <P class="card-title"><a href="{{ route('coin.coin_index') }}">{{ $coinType->coinType }} Bulk
                                Report</a></P>
                        <table class="table">
                            <tr>
                                <td><span class="fw-bold">Coins Per Roll:</span> {{$coinType->rollCount}}</td>
                                <td><span class="fw-bold">Roll Value:</span> {{$coinType->rollVal}}</td>
                            </tr>
                            <tr>
                                <td><span class="fw-bold">Coins Per Box:</span> {{$coinType->rollLgBoxCount}}</td>
                                <td><span class="fw-bold">Box Value:</span> {{$coinType->rollLgBoxVal}}</td>
                            </tr>
                            <tr>
                                <td><span class="fw-bold">Coins Per Bag:</span> {{$coinType->bagCount}}</td>
                                <td><span class="fw-bold">Bag Value:</span> {{$coinType->bagVal}}</td>
                            </tr>
                            @if(in_array($coinType->id, [101,99]))
                                <tr>
                                    <td><span
                                            class="fw-bold">Coins Per Monster Box:</span> {{$coinType->monster_box_count}}
                                    </td>
                                    <td><span class="fw-bold">Monster Box Value:</span> {{$coinType->monster_box_val}}
                                    </td>
                                </tr>
                            @endif

                        </table>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Saved</h5>
                    <div class="card card-body">
                        <table class="table">
                            <td><span class="fw-bold">Business:</span> {{$business}}</td>
                            <td><span class="fw-bold">Proofs:</span> {{$proofs}}</td>
                            <td><span class="fw-bold">Investment:</span> {{$proofs}}</td>
                        </table>
                    </div>
                </div>
            </div>


            @if(in_array($coinType->id, [3]))
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">By Composition</h5>
                        <div class="card card-body">
                            <table class="table">
                                <tr>
                                    <td><span class="fw-bold">1959-1982:</span>  95% Copper - 5% Tin and Zinc</td>
                                    <td><span class="fw-bold">1982-2008:</span> 97.5% Zinc - 2.5% Copper</td>
                                </tr>
                                <tr>
                                    <td><span class="fw-bold">Saved:</span>  <span id="copper_span"></span></td>
                                    <td><span class="fw-bold">Saved:</span>  <span id="zinc_span"></span></td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
            @endif

            <!-- Nested row for non-featured blog posts-->
                <div class="table-responsive">
                    <table class="table table-hover datatable">
                        <thead>
                        <tr>
                            <th scope="col">Type</th>
                            <th scope="col">Collected</th>
                            <th scope="col">Investment</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($coins as $key => $coin)
                            <tr>
                                <td class="w-75">
                                    <a href="{{ route('coin.view_coin', ['coin' => $key]) }}">{{ Str::limit($coin['coin']->coinName, 55) }}</a>
                                </td>
                                <td class="text-center">
                                    {{$coin['collected']}}
                                </td>
                                <td class="text-center">
                                    {{$coin['investment']}}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td></td>
                                <td>No {{ $coin->coinName }} saved</td>
                                <td></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>

                <div class="col-lg-12">
                    <a class="btn btn-secondary" href="#!">See All Projects</a>
                </div>
            </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Switch Design</div>
                <div class="card-body">
                    <div class="input-group">
                        <select class="form-select" id="coin_id_select"
                                aria-label="Example select with button addon">
                            <option selected>Choose...</option>
                            @foreach($releaseList as $year => $value)
                                @foreach($value as $release)
                                    <option value="{{ str_replace(' ', '_', $release) }}">{{ $release }}</option>
                                @endforeach
                            @endforeach
                        </select>
                        <button id="coin_id_select_btn" class="btn btn-outline-primary" type="button">Load</button>
                    </div>
                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Reports</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                @if(in_array($coinType->coinType, Config::get('constants.coins.lincoln_reverse')))
                                    <li><a href="{{ route('coin.lincoln_index') }}">All Lincoln Cents</a></li>
                                @endif
                                @if(str_contains($coinType->coinType, 'Seated Liberty') || $coinType->id == 44)
                                    <li><a href="{{ route('coin.seated_index') }}">Seated Liberty</a></li>

                                @endif
                                @if(in_array($coinType->coinType, Config::get('constants.coins.jefferson_reverse')))
                                    <li><a href="{{ route('coin.view_type_group', ['type' => 'jefferson_reverse']) }}">All
                                            Jefferson Nickels</a></li>
                                @endif

                                <li><a href="{{ route('coin.type_grades', ['coinType' => $coinType->id]) }}">Grade
                                        Report</a></li>
                                <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">Proof Report</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                @if(in_array($coinType->category->id, Config::get('constants.coins.color_categories')))
                                    <li><a href="{{ route('coin.view_type_color', ['id' => $coinType->id]) }}">Color
                                            Report</a></li>
                                @endif

                                @if($coinType->coinType == 'Lincoln Wheat')
                                    <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">Color
                                            Report</a></li>
                                    <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">BIE Errors</a>
                                    </li>
                                    <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">Coppercoins
                                            List</a></li>
                                @endif



                                @if($coinType->coinType == 'Indian Head Cent' || $coinType->coinType == 'Flying Eagle')
                                    <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">By Snow
                                            Numbers</a></li>
                                @endif

                                @if($coinType->coinType == 'Morgan Dollar' || $coinType->coinType == 'Peace Dollar')
                                    <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">VAMs</a></li>
                                @endif

                                @if($coinType->coinType == 'Standing Liberty')
                                    <li><a href="{{ route('coin.view_type_head', ['id' => $coinType->id]) }}">Full Head
                                            Report</a></li>
                                @endif
                                @if($coinType->coinType == 'Jefferson Nickel')
                                    <li><a href="{{ route('coin.view_type_steps', ['id' => $coinType->id]) }}">Full
                                            Steps Report</a></li>
                                @endif
                                @if($coinType->coinType == 'Franklin Half Dollar')
                                    <li><a href="{{ route('coin.view_type_bell', ['id' => $coinType->id]) }}">Full Bell
                                            Lines Report</a></li>
                                @endif

                                @if(in_array($coinType->coinType, Config::get('constants.coins.full_band_types')))
                                    <li><a href="{{ route('coin.view_type_bands', ['id' => $coinType->id]) }}">Full
                                            Bands Report</a></li>
                                @endif

                                @if($coinType->coinType == 'Lincoln Wheat' || $coinType->coinType == 'Lincoln Memorial')
                                    <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">Coppercoins
                                            List</a></li>
                                @endif


                                <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">Damage Report</a>
                                </li>
                                <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">Variety Report</a>
                                </li>
                                <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">Error Report</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        @if(!empty($typeLinks))
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">

                                    @foreach($typeLinks as $src => $link)
                                        <li><a class="external_link" href="{{$link}}">{{$src}}</a></li>
                                    @endforeach

                                </ul>
                            </div>
                        @endif
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                @if($coinType->category->id === 63)
                                    <li><a class="external_link" href="http://www.smalldollars.com/index.html">SmallDollars.com</a>
                                    </li>
                                @endif
                                @if(!empty($typeSpecificLinks))

                                    @foreach($typeSpecificLinks as $src => $link)
                                        <li><a class="external_link" href="{{$link}}">{{$src}}</a></li>
                                    @endforeach

                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">
                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.view_directory') }}">User Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    @push('scripts')
        <script>
            (function (ENVIRONMENT) {
                try {
                    //const coin_type_id = {{$coinType->id}};

                    const coin_id_select = document.getElementById("coin_id_select");
                    const coin_id_select_btn = document.getElementById("coin_id_select_btn");

                    coin_id_select_btn.addEventListener("click", function () {
                        let coin_type_design = coin_id_select.options[coin_id_select.selectedIndex].value;
                        window.location.href = "{{ route('coin.design_index', [':design']) }}".replace(':design', coin_type_design);
                        //window.location.href = url;
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection

