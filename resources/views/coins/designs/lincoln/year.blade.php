@extends('layouts.user.main')
@section('pageTitle', "Releases for Year $year")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Releases for Year: {{ $year }} </h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    My Sets
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item" href="{{ route('coin.coin_index') }}">My Coins</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Sets</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Folders</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Rolls</a></li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.lincoln_index') }}">All Lincoln Cents</a></li>
    </ol>


    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                Lincoln Cent Mint releases for {{ $year }}.
            </div>

            <div class="row">
                <div class="col-sm-6 float-start">
                    <div class="input-group mb-3">
                        @if($year > 1909)<a class="btn btn-secondary" href="{{ route('coin.lincoln_index_year_view', ['year' => $year - 1]) }}">{{ $year -1 }}</a> @endif
                        @if($year < date('Y'))<a class="btn btn-secondary float-end" href="{{ route('coin.lincoln_index_year_view', ['year' => $year + 1]) }}">{{ $year +1 }}</a> @endif
                    </div>



                </div>
                <div class="col-sm-6">
                    <div class="input-group mb-3">
                        <select class="form-select" id="coin_date">
                            @php $output = range(1909,date('Y')); @endphp
                            @foreach($output as $date)
                                <option value="{{sprintf("%02d", $date)}}">{{sprintf("%02d", $date)}}</option>
                            @endforeach
                        </select>
                        <button class="btn btn-outline-secondary" type="button" id="coin_date_btn">Load</button>
                    </div>

                </div>
            </div>
            <table class="table">
                <tr>
                    <td><span class="fw-bold">Saved: </span> ({{$count}})</td>
                    <td><span class="fw-bold">Invested: </span> ({{$cost}})</td>
                    <td><span class="fw-bold">Face Val: </span> ({{$count}})</td>
                </tr>
            </table>

            <table class="table table-striped table-hover">
                    <tr>
                        <th class="fw-bold fs-6">Coins</th>
                        <th class="fw-bold">Collected</th>
                    </tr>
                    @foreach($coins as $coin)
                        <tr>
                            <td>
                                <a href="{{ route('coin.view_coin', ['coin' => $coin->id]) }}">{{ $coin->coinName }} {{ $coin->coinType }}</a>
                            </td>
                            <td id="coin_count_row">
                                <span class="coin_id_count" data-coin="{{$coin->id}}">Loading...</span>
                            </td>
                        </tr>
                    @endforeach
                </table>

            <div class="card">
                <div class="card-body">
                    <table class="table">
                        @if(isset($design_varieties['obverse_coneca2'][0]))
                            <tr>
                                <td class="w-50"><span class="fw-bold">{{$year}} Obverse Designs:</span></td>
                                <td>
                                    @foreach($design_varieties['obverse_coneca2'] as $obverse)
                                        @if($obverse !== 'None')
                                        <a class="design_link" href="{{ route('coin.view_lincoln_design_detail', [
                                                    'detail' => 'obverse',
                                                    'variety' => urlencode(urlencode($obverse)),
                                                    ]) }}">{{$obverse}}</a>
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                        @endif

                        @if(isset($design_varieties['reverse_coneca'][0]))
                            <tr>
                                <td><span class="fw-bold">{{$year}} Reverse Designs:</span></td>
                                <td>
                                    @foreach(explode(' ',$design_varieties['reverse_coneca'][0]->reverses) as $reverse)
                                        @if($reverse !== 'None')
                                            <a class="design_link" href="{{ route('coin.view_lincoln_design_detail', [
                                                    'detail' => 'reverse',
                                                    'variety' => urlencode(urlencode($reverse)),
                                                    ]) }}">{{$reverse}}</a>
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                        @endif

                        @if(isset($mintmarkStyles['Denver'][0]))
                            <tr>
                                <td><span class="fw-bold">{{$year}} Denver Mintmark Styles: </span> </td>
                                <td>
                                    @foreach($mintmarkStyles['Denver'] as $mintmark)
                                        @if($mintmark !== 'None')
                                            <a href="{{ route('coin.view_lincoln_mms', [
                                                    'mint' => 'Denver',
                                                    'mms' => $mintmark,
                                                    ]) }}">{{$mintmark}}</a>
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                        @endif
                        @if(isset($mintmarkStyles['San Francisco'][0]))
                            <tr>
                                <td><span class="fw-bold">{{$year}} San Francisco Mintmark Styles: </span> </td>
                                <td>
                                    @foreach($mintmarkStyles['San Francisco'] as $mintmark)
                                        @if($mintmark !== 'None')
                                            <a href="{{ route('coin.view_lincoln_mms', [
                                                    'mint' => 'San_Francisco',
                                                    'mms' => $mintmark,
                                                    ]) }}">{{$mintmark}}</a>
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                        @endif

                    </table>
                </div>
            </div>


        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Add {{ $year }} Coin</div>
                <div class="card-body">

                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                data-bs-toggle="dropdown" aria-expanded="false">
                            Add New
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            @foreach($coins as $coin)
                                <li><a class="dropdown-item"
                                       href="{{ route('collected.save_by_coin_id', ['id' => $coin->id]) }}">{{ $coin->coinName }} {{ $coin->coinType }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                </div>
            </div>

            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">By Mint</div>
                <div class="card-body">
                    <div class="input-group mb-3">
                        <select class="form-select" id="coin_mints">
                            <option value="{{ route('coin.view_lincoln_by_mint', ['mint' => 'Denver']) }}">Denver</option>
                            <option value="{{ route('coin.view_lincoln_by_mint', ['mint' => 'Philadelphia']) }}">Philadelphia</option>
                            <option value="{{ route('coin.view_lincoln_by_mint', ['mint' => 'San_Francisco']) }}">San Francisco</option>
                            <option value="{{ route('coin.view_lincoln_by_mint', ['mint' => 'West_Point']) }}">West Point</option>
                        </select>
                        <button class="btn btn-outline-secondary" type="button" id="coin_mints_btn">Load</button>
                    </div>
                </div>
            </div>

            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('coin.view_type', ['id' => $year]) }}">Color Report</a></li>

                                @if($year == 'Lincoln Wheat')
                                    <li><a href="{{ route('coin.view_type', ['id' => $year]) }}">Color Report</a></li>

                                @endif
                                @if($year == 'Lincoheat')
                                    <li><a href="{{ route('coin.view_type', ['id' => $year]) }}">Coloport</a>
                                    </li>

                                @endif
                                @if($year == 'Lincoheat')
                                    <li>
                                        <a href="{{ route('coin.view_type', ['id' => $year]) }}">Coloport</a>
                                    </li>
                                @endif
                                @if($year == 'Lincoheat')
                                    <li>
                                        <a href="{{ route('coin.view_type', ['id' => $year]) }}">Coloport</a>
                                    </li>
                                @endif
                                @if($year == 'Lincoheat')
                                    <li>
                                        <a href="{{ route('coin.view_type', ['id' => $year]) }}">Coloport</a>
                                    </li>
                                @endif
                                @if($year == 'Lincoheat')
                                    <li>
                                        <a href="{{ route('coin.view_type', ['id' => $year]) }}">Coloport</a>
                                    </li>
                                @endif

                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('coin.year_view', ['year' => $year]) }}">All {{ $year }}</a></li>
                                <li><a class="external_link" href="http://www.cuds-on-coins.com/">Cuds-On-Coins</a></li>
                                <li><a class="external_link" href="http://www.traildies.com/">Trail Dies</a></li>
                                <li><a class="external_link"
                                       href="http://ec2-13-58-222-16.us-east-2.compute.amazonaws.com/wiki/Home">VAMWorld</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">

                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.view_directory') }}">User Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            @endif
        </div>
    </div>

@endsection
@push('scripts')
    <script>


        (function (ENVIRONMENT) {
            try {

                const coin_counts = document.querySelectorAll('.coin_id_count');
                const set_counts = document.querySelectorAll('.set_id_count');
                Array.from(coin_counts)
                    .forEach(function (coin_count) {
                        //console.log(coin_count.dataset.coin);
                        window.addEventListener('load', function (event) {
                            let url = "{{ route('coin.lincoln_year_count_by_coin', [':coin_id', ':year']) }}".replace(':coin_id', coin_count.dataset.coin).replace(':year', {{$year}});
                            fetch(url, {
                                "method": "GET",
                            }).then(
                                response => {
                                    response.json().then(
                                        data => {
                                            console.log(data);
                                            coin_count.innerHTML = data.count;
                                        }
                                    )
                                })
                        }, false)
                    })

                let coin_mints = document.getElementById("coin_mints");
                document.getElementById("coin_mints_btn").addEventListener("click", function () {
                    let mint_url = coin_mints.options[coin_mints.selectedIndex].value;
                    window.location.href = mint_url;
                });

                let coin_date = document.getElementById("coin_date");
                document.getElementById("coin_date_btn").addEventListener("click", function () {
                    let date_year = coin_date.options[coin_date.selectedIndex].value;
                    let date_url = "{{ route('coin.lincoln_index_year_view', [':year']) }}".replace(':year', date_year);
                    //console.log(url);
                    window.location.href = date_url;
                });

            } catch (error) {
                if (ENVIRONMENT === 'local') {
                    console.error(error);
                }
            }
        })();
    </script>
@endpush
