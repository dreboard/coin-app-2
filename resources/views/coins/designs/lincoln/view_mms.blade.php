@extends('layouts.user.main')
@section('pageTitle', "View Type Mintmark Style")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Mintmark Style</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    View
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Coin</a>
                    </li>

                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.lincoln_index') }}">All Lincoln Cents</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                Lincoln Cent <span class="fw-bold">{{$mint}} {{$mms}}</span> Mintmark Style Coins
            </div>


            <div class="card mb-3">
                <div class="card-body">

                    <div class="row">
                        <div class="col-sm-4">
                            <img src="{{config('app.image_url')}}{{ str_replace(' ', '_', 'Lincoln Wheat') }}.jpg"/>
                        </div>
                        <div class="col-sm-8">
                            <h5 class="card-title">Details</h5>
                            <table class="table">
                                <tr>
                                    <td><span class="fw-bold">Style:</span></td>
                                    <td class="w-75"><a href="{{ route('coin.view_lincoln_by_mint', ['mint' => str_replace(' ', '_', $mint)]) }}">
                                            {{ $mint }}
                                        </a> {{$mms}}</td>
                                </tr>

                                <tr>
                                    <td><span class="fw-bold">Switch:</span></td>
                                    <td class="w-75">
                                        <div class="dropdown">
                                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                                    aria-expanded="false">
                                               Switch Style
                                            </button>
                                            <ul class="dropdown-menu">
                                                @foreach($mm_styles as $mm_style)
                                                    <li><a class="dropdown-item" href="{{ route('coin.view_lincoln_mms', [
                                                    'mint' => $mint,
                                                    'mms' => $mm_style,
                                                    ]) }}">{{$mint}} {{$mm_style}}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </td>
                                </tr>


                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Varieties</h4>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Do Something
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('member.posts_index') }}">See All</a></li>
                                <li><a class="dropdown-item" href="{{ route('member.create_posts') }}">Create (Found A
                                        new variety)</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Errors</h4>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Do Something
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('member.view_books') }}">See All</a></li>
                                <li><a class="dropdown-item" href="{{ route('member.create_posts') }}">Create (Found A
                                        new error)</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Discussion Area</h4>
                        <a class="btn btn-secondary" href="{{ route('member.forum') }}">Go →</a>
                    </div>
                </div>
            </div>


            <hr/>
            <!-- Featured blog post-->


            <!-- Nested row for non-featured blog posts-->
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-hover datatable">
                        <thead>
                        <tr>
                            <th scope="col">Type</th>
                            <th scope="col">Collected</th>
                            <th scope="col">Investment</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($coins as $key => $coin)
                            <tr>
                                <td class="w-75">
                                    <a href="{{ route('coin.view_coin', ['coin' => $key]) }}">{{ Str::limit($coin['coin']->coinName, 55) }}</a>
                                </td>
                                <td class="text-center">
                                    {{$coin['collected']}}
                                </td>
                                <td class="text-center">
                                    {{$coin['investment']}}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td></td>
                                <td>No coins saved</td>
                                <td></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>

                <div class="col-lg-12">
                    <a class="btn btn-secondary" href="#!">See All Projects</a>
                </div>
            </div>
        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">By Year</div>
                <div class="card-body">
                    <div class="input-group mb-3">
                        <select class="form-select" id="coin_date">
                            @php $output = range(1909,date('Y')); @endphp
                            @foreach($output as $date)
                                <option value="{{sprintf("%02d", $date)}}">{{sprintf("%02d", $date)}}</option>
                            @endforeach
                        </select>
                        <button class="btn btn-outline-secondary" type="button" id="coin_date_btn">Load</button>
                    </div>
                </div>
            </div>

            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">By Mint</div>
                <div class="card-body">
                    <div class="input-group mb-3">
                        <select class="form-select" id="coin_date">
                            <option value="{{ route('coin.view_lincoln_by_mint', ['mint' => 'Denver']) }}">Denver</option>
                            <option value="{{ route('coin.view_lincoln_by_mint', ['mint' => 'Philadelphia']) }}">Philadelphia</option>
                            <option value="{{ route('coin.view_lincoln_by_mint', ['mint' => 'San_Francisco']) }}">San Francisco</option>
                            <option value="{{ route('coin.view_lincoln_by_mint', ['mint' => 'West_Point']) }}">West Point</option>
                        </select>
                        <button class="btn btn-outline-secondary" type="button" id="coin_date_btn">Load</button>
                    </div>
                </div>
            </div>
            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">

                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.view_directory') }}">User Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            @endif
        </div>
    </div>
    @push('scripts')
        <script>
            (function (ENVIRONMENT) {
                try {
                    const coin_id_select = document.getElementById("coin_id_select");
                    const coin_id_select_btn = document.getElementById("coin_id_select_btn");

                    coin_id_select_btn.addEventListener("click", function () {
                        let coin_type_id = coin_id_select.options[coin_id_select.selectedIndex].value;
                        window.location.href = "{{ route('coin.view_type', [':id']) }}".replace(':id', coin_type_id);
                        //window.location.href = url;
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection

