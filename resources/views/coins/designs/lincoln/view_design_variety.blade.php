@extends('layouts.user.main')
@section('pageTitle', "View Lincoln Design Variety")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">View Lincoln Design Variety {{$variety_name}}</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    View
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item" href="{{ route('coin.coin_index') }}">My Coins</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Sets</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Folders</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Rolls</a></li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.lincoln_index') }}">All Lincoln Cents</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                Design Variety <span class="fw-bold">{{$variety}}</span> Coins
            </div>


            <div class="card mb-3">
                <div class="card-body">

                    <div class="row">
                        <div class="col-sm-4">
                            <img src="{{config('app.image_url')}}{{ str_replace(' ', '_', 'Lincoln Wheat') }}.jpg"/>
                        </div>
                        <div class="col-sm-8">
                            <h5 class="card-title">Details</h5>
                            <table class="table">
                                <tr>
                                    <td><span class="fw-bold">Design Variety:</span></td>
                                    <td class="w-75">{{$variety}}</td>
                                </tr>

                                <tr>
                                    <td><span class="fw-bold">Switch:</span></td>
                                    <td class="w-75">
                                        <div class="dropdown">
                                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                                    aria-expanded="false">
                                                {{$variety_name}}
                                            </button>
                                            <ul class="dropdown-menu">
                                                @foreach($variety_list as $link)
                                                    <li><a class="dropdown-item" href="{{ route('coin.view_lincoln_design_detail', [
                                                    'detail' => $detail,
                                                    'variety' => urlencode(urlencode($link)),
                                                    ]) }}">{{$link}}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="related_design_div">
                                    <td><span class="fw-bold">Design Variety:</span></td>
                                    <td class="w-75">{{$variety}}</td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Varieties</h4>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Do Something
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('member.posts_index') }}">See All</a></li>
                                <li><a class="dropdown-item" href="{{ route('member.create_posts') }}">Create (Found A
                                        new variety)</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Errors</h4>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Do Something
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('member.view_books') }}">See All</a></li>
                                <li><a class="dropdown-item" href="{{ route('member.create_posts') }}">Create (Found A
                                        new error)</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Discussion Area</h4>
                        <a class="btn btn-secondary" href="{{ route('member.forum') }}">Go →</a>
                    </div>
                </div>
            </div>


            <hr/>
            <!-- Featured blog post-->


            <!-- Nested row for non-featured blog posts-->
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-hover datatable">
                        <thead>
                        <tr>
                            <th scope="col">Type</th>
                            <th scope="col">Collected</th>
                            <th scope="col">Investment</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($coins as $key => $coin)
                            <tr>
                                <td class="w-75">
                                    <a href="{{ route('coin.view_coin', ['coin' => $key]) }}">{{ Str::limit($coin['coin']->coinName, 55) }}</a>
                                </td>
                                <td class="text-center">
                                    {{$coin['collected']}}
                                </td>
                                <td class="text-center">
                                    {{$coin['investment']}}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td></td>
                                <td>No {{$variety_name}} coins saved</td>
                                <td></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>

                <div class="col-lg-12">
                    <a class="btn btn-secondary" href="{{ route('coin.lincoln_index') }}">See All Lincoln Cents</a>
                </div>
            </div>
        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">By Year</div>
                <div class="card-body">
                    <div class="input-group mb-3">
                        <select class="form-select" id="coin_date">
                            @php $output = range(1909,date('Y')); @endphp
                            @foreach($output as $date)
                                <option value="{{sprintf("%02d", $date)}}">{{sprintf("%02d", $date)}}</option>
                            @endforeach
                        </select>
                        <button class="btn btn-outline-secondary" type="button" id="coin_date_btn">Load</button>
                    </div>
                </div>
            </div>

            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">By Mint</div>
                <div class="card-body">
                    <div class="input-group mb-3">
                        <select class="form-select" id="coin_mints">
                            <option value="{{ route('coin.view_lincoln_by_mint', ['mint' => 'Denver']) }}">Denver</option>
                            <option value="{{ route('coin.view_lincoln_by_mint', ['mint' => 'Philadelphia']) }}">Philadelphia</option>
                            <option value="{{ route('coin.view_lincoln_by_mint', ['mint' => 'San_Francisco']) }}">San Francisco</option>
                            <option value="{{ route('coin.view_lincoln_by_mint', ['mint' => 'West_Point']) }}">West Point</option>
                        </select>
                        <button class="btn btn-outline-secondary" type="button" id="coin_mints_btn">Load</button>
                    </div>
                </div>
            </div>
            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">

                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.view_directory') }}">User Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            @endif
        </div>
    </div>
    @push('scripts')
        <script>
            (function (ENVIRONMENT) {
                try {
                    let coin_date = document.getElementById("coin_date");
                    document.getElementById("coin_date_btn").addEventListener("click", function () {
                        let date_year = coin_date.options[coin_date.selectedIndex].value;
                        let url = "{{ route('coin.lincoln_index_year_view', [':year']) }}".replace(':year', date_year);
                        window.location.href = url;
                    });

                    let coin_mints = document.getElementById("coin_mints");
                    document.getElementById("coin_mints_btn").addEventListener("click", function () {
                        let url = coin_mints.options[coin_mints.selectedIndex].value;
                        //console.log(url);
                        window.location.href = url;
                    });


                    window.addEventListener('load', function (event) {
                        fetch('{{ route('coin.get_other_design_variety') }}', {
                            headers: {"Content-Type": "application/json; charset=utf-8"},
                            method: 'POST',
                            body: JSON.stringify({
                                _token: @json(csrf_token()),
                                current_design: "{{$detail}}",
                                design_variety: "{{$variety}}",
                            })
                        }).then(response => response.json())
                            .then(data => {
                                console.log(data);

                            }).catch((error) => {
                            console.log(error)
                        })
                    }, false);


                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection

