@extends('layouts.user.main')
@section('pageTitle', "View Lincoln Design Variety")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Lincoln Cents BIE Die Breaks</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    View
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item" href="{{ route('coin.coin_index') }}">My Coins</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Sets</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Folders</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Rolls</a></li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">Coins Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.lincoln_index') }}">All Lincoln Cents</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                Lincoln Cent Liberty Die Breaks
            </div>


            <div class="card mb-3">
                <div class="card-body">

                    <div class="row">
                        <div class="col-sm-4">
                            <img src="{{config('app.image_url')}}{{ str_replace(' ', '_', 'Lincoln Wheat') }}.jpg"/>
                        </div>
                        <div class="col-sm-8">
                            <h5 class="card-title">Details</h5>
                            <table class="table">
                                <tr>
                                    <td><span class="fw-bold">Error:</span></td>
                                    <td class="w-75"><a href="{{ route('coin.error_view', ['error' => 795]) }}">Die Break </a> (BIE Error)</td>
                                </tr>
                                <tr>
                                    <td><span class="fw-bold">Source:</span></td>
                                    <td class="w-75"><a class="external_link" href="http://cuds-on-coins.com">Cuds On Coins</a> BROKEN DIE ERRORS</td>
                                </tr>

                                <tr>
                                    <td><span class="fw-bold">Switch:</span></td>
                                    <td class="w-75"></td>
                                </tr>


                            </table>
                        </div>
                    </div>
                </div>
            </div>



            <!-- Featured blog post-->


            <!-- Nested row for non-featured blog posts-->
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-hover datatable">
                        <thead>
                        <tr>
                            <th scope="col">Type</th>
                            <th scope="col">Investment</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($bie_labels as $label)
                            <tr>
                                <td class="w-75">
                                    <a href="{{ route('coin.error_view', ['error' => 795]) }}">Die Break, {{ Str::limit($label->description, 55) }}</a>
                                </td>
                                <td class="text-center">
                                    {{$label->designation}}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td>No BIE coins saved</td>
                                <td></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>

                <div class="col-lg-12">
                    <a class="btn btn-secondary" href="{{ route('coin.lincoln_index') }}">See All Lincoln Cents</a>
                </div>
            </div>
        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">By Year</div>
                <div class="card-body">
                    <div class="input-group mb-3">
                        <select class="form-select" id="coin_date">
                            @php $output = range(1909,date('Y')); @endphp
                            @foreach($output as $date)
                                <option value="{{sprintf("%02d", $date)}}">{{sprintf("%02d", $date)}}</option>
                            @endforeach
                        </select>
                        <button class="btn btn-outline-secondary" type="button" id="coin_date_btn">Load</button>
                    </div>
                </div>
            </div>

            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">By Mint</div>
                <div class="card-body">
                    <div class="input-group mb-3">
                        <select class="form-select" id="coin_mints">
                            <option value="{{ route('coin.view_lincoln_by_mint', ['mint' => 'Denver']) }}">Denver</option>
                            <option value="{{ route('coin.view_lincoln_by_mint', ['mint' => 'Philadelphia']) }}">Philadelphia</option>
                            <option value="{{ route('coin.view_lincoln_by_mint', ['mint' => 'San_Francisco']) }}">San Francisco</option>
                            <option value="{{ route('coin.view_lincoln_by_mint', ['mint' => 'West_Point']) }}">West Point</option>
                        </select>
                        <button class="btn btn-outline-secondary" type="button" id="coin_mints_btn">Load</button>
                    </div>
                </div>
            </div>
            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">

                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.view_directory') }}">User Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            @endif
        </div>
    </div>
    @push('scripts')
        <script>
            (function (ENVIRONMENT) {
                try {
                    let coin_date = document.getElementById("coin_date");
                    document.getElementById("coin_date_btn").addEventListener("click", function () {
                        let date_year = coin_date.options[coin_date.selectedIndex].value;
                        let url = "{{ route('coin.lincoln_index_year_view', [':year']) }}".replace(':year', date_year);
                        window.location.href = url;
                    });

                    let coin_mints = document.getElementById("coin_mints");
                    document.getElementById("coin_mints_btn").addEventListener("click", function () {
                        let url = coin_mints.options[coin_mints.selectedIndex].value;
                        //console.log(url);
                        window.location.href = url;
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection

