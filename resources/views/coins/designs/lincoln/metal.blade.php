@extends('layouts.user.main')
@section('pageTitle', "Lincoln Cents Metal Content")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Lincoln Cents: {{ $content }} </h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    My Sets
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item" href="{{ route('coin.coin_index') }}">My Coins</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Sets</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Folders</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Rolls</a></li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.lincoln_index') }}">All Lincoln Cents</a></li>
    </ol>


    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                Lincoln Cents Metal Content: {{ $content }}.
            </div>

            <div class="row mb-3">
                <div class="col-sm-12">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Switch Content
                        </button>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="{{ route('coin.view_lincoln_metal', ['group' => 1]) }}"> 1909–1942 Bronze (95% Copper, 5% Tin and Zinc)</a></li>
                            <li><a class="dropdown-item" href="{{ route('coin.view_lincoln_metal', ['group' => 2]) }}">1943 Zinc-Coated Steel</a></li>
                            <li><a class="dropdown-item" href="{{ route('coin.view_lincoln_metal', ['group' => 3]) }}">1944–1946 Gilding Metal (95% Copper, 5% Zinc)</a></li>
                            <li><a class="dropdown-item" href="{{ route('coin.view_lincoln_metal', ['group' => 4]) }}">1947–1962 Bronze (95% copper, 5% Tin and Zinc)</a></li>
                            <li><a class="dropdown-item" href="{{ route('coin.view_lincoln_metal', ['group' => 5]) }}">1962–1982 Gilding Metal (95% Copper, 5% Zinc)</a></li>
                            <li><a class="dropdown-item" href="{{ route('coin.view_lincoln_metal', ['group' => 6]) }}">1982–Present Copper-Plated Zinc (97.5% Zinc, 2.5% Copper)</a></li>
                        </ul>
                    </div>
                </div>
            </div>


            <table class="table table-hover datatable">
                <thead>
                <tr>
                    <th class="fw-bold fs-6">Coins</th>
                    <th class="fw-bold">Collected</th>
                </tr>
                </thead>
                <tbody>
                @foreach($coins as $coin)
                    <tr>
                        <td>
                            <a href="{{ route('coin.view_coin', ['coin' => $coin->id]) }}">{{ $coin->coinName }}</a>
                        </td>
                        <td id="coin_count_row">
                            <span class="coin_id_count" data-coin="{{$coin->id}}">Loading...</span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Add A Lincoln Cent</div>
                <div class="card-body">

                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                data-bs-toggle="dropdown" aria-expanded="false">
                            Add New
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            @foreach($coins as $coin)
                                <li><a class="dropdown-item"
                                       href="{{ route('collected.save_by_coin_id', ['id' => $coin->id]) }}">{{ $coin->coinName }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">


                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="http://www.cuds-on-coins.com/">Cuds-On-Coins</a></li>
                                <li><a class="external_link" href="http://www.traildies.com/">Trail Dies</a></li>
                                <li><a class="external_link"
                                       href="http://ec2-13-58-222-16.us-east-2.compute.amazonaws.com/wiki/Home">VAMWorld</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">

                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.view_directory') }}">User Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            @endif
        </div>
    </div>

@endsection
@push('scripts')
    <script>


        (function (ENVIRONMENT) {
            try {

                const coin_counts = document.querySelectorAll('.coin_id_count');
                const set_counts = document.querySelectorAll('.set_id_count');
                Array.from(coin_counts)
                    .forEach(function (coin_count) {
                        //console.log(coin_count.dataset.coin);
                        window.addEventListener('load', function (event) {
                            let url = "{{ route('coin.lincoln_year_count_by_metal', [':coin_id']) }}".replace(':coin_id', coin_count.dataset.coin);
                            fetch(url, {
                                "method": "GET",
                            }).then(
                                response => {
                                    response.json().then(
                                        data => {
                                            console.log(data);
                                            coin_count.innerHTML = data.count;
                                        }
                                    )
                                })
                        }, false)
                    })
            } catch (error) {
                if (ENVIRONMENT === 'local') {
                    console.error(error);
                }
            }
        })();
    </script>
@endpush
