@extends('layouts.user.main')
@section('pageTitle', 'Lincoln Cents')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Lincoln Cents</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Switch Type
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('coin.lincoln_index') }}">Lincoln Cents</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.seated_index') }}">Seated Liberty</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.capped_index') }}">Capped Bust</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.draped_index') }}">Draped Bust</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">Coins Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                Lincoln Cents Coins 1909 to Present.
            </div>



            <div class="table-responsive mb-2">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="fw-bold">#</th>
                        <th class="fw-bold">Coin</th>
                        <th class="fw-bold">Grades</th>
                        <th class="fw-bold">Attribution</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($types as $id => $type)
                        <tr>
                            <th><img src="{{config('app.image_url')}}{{ str_replace(' ', '_', $type) }}.jpg" class="img-seated" alt="..."></th>
                            <td scope="row"><a href="{{ route('coin.view_type', ['id' => $id]) }}">{{ $type }}</a></td>
                            <td><a href="{{ route('coin.type_grades', ['coinType' => $id]) }}">Grade Report</a></td>
                            <td><a href="{{ route('coin.lincoln_index') }}">Report</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>


            <div id="set_type_div" class="row">
                <div class="col-sm-3">
                    <div class="card text-center">
                        <div class="card-body">
                            <h5 class="card-title">Type Set</h5>
                            <p class="card-text">One of each type collection.</p>
                        </div>
                        <div class="card-footer text-muted mx-auto">
                            <a href="{{ route('coin.design_type_set', ['design' => 'lincoln']) }}" class="btn btn-primary">View</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card text-center">
                        <div class="card-body">
                            <h5 class="card-title">Year Set</h5>
                            <p class="card-text">One of each type collection.</p>
                        </div>
                        <div class="card-footer text-muted mx-auto">
                            <a href="{{ route('coin.design_type_set', ['design' => 'lincoln']) }}" class="btn btn-primary">View</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card text-center">
                        <div class="card-body">
                            <h5 class="card-title">Sets Including Proofs</h5>
                            <p class="card-text">One of each type collection.</p>
                        </div>
                        <div class="card-footer text-muted mx-auto">
                            <a href="{{ route('coin.design_type_set', ['design' => 'lincoln']) }}" class="btn btn-primary">View</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card text-center">
                        <div class="card-body">
                            <h5 class="card-title">Date and Mintmark Set</h5>
                            <p class="card-text">One of each type collection.</p>
                        </div>
                        <div class="card-footer text-muted mx-auto">
                            <a href="{{ route('coin.design_type_set', ['design' => 'lincoln']) }}" class="btn btn-primary">View</a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="card mb-3 mt-3">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tr class="text-center">
                                <td rowspan="4"><span class="fw-bold">Collecting</span></td>
                            </tr>
                            <tr class="text-center">
                                <td class="w-25"><a href="{{ route('coin.view_lincoln_bie') }}">BIE Errors</a></td>
                                <td class="w-25"><a href="{{ route('coin.design_type_set', ['design' => 'lincoln']) }}">Year Set</a></td>
                                <td class="w-25"><a href="{{ route('coin.design_type_set', ['design' => 'lincoln']) }}">Date and Mintmark Set</a></td>
                                <td class="w-25"><a href="{{ route('coin.design_type_set', ['design' => 'lincoln']) }}">Sets Including Proofs</a></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <table class="table">
                        @if(isset($design_varieties['obverse_coneca'][0]))
                            <tr>
                                <td><span class="fw-bold">Obverse Designs:</span></td>
                                <td>
                                    @foreach($design_varieties['obverse_coneca'] as $obverse)
                                        <a href="{{ route('coin.view_lincoln_design_detail', [
                                                    'detail' => 'obverse',
                                                    'variety' => urlencode(urlencode($obverse)),
                                                    ]) }}">{{$obverse}}</a>
                                        @if(!$loop->last)
                                            ,
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                        @endif

                        @if(isset($design_varieties['reverse_coneca'][0]))
                            <tr>
                                <td><span class="fw-bold">Reverse Designs:</span></td>
                                <td>
                                    @foreach($design_varieties['reverse_coneca'] as $reverse)
                                        <a href="{{ route('coin.view_lincoln_design_detail', [
                                                    'detail' => 'reverse',
                                                    'variety' => urlencode(urlencode($reverse)),
                                                    ]) }}">{{$reverse}}</a>
                                        @if(!$loop->last)
                                            ,
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                        @endif
                    </table>

                    @if(isset($mintmarkStyles['Philadelphia'][1]) || isset($mintmarkStyles['Denver'][1]) || isset($mintmarkStyles['San Francisco'][1]))
                        <h5>Mintmark Styles</h5>
                    @endif

                    <table>
                        @if(isset($mintmarkStyles['Denver'][1]))
                            <tr>
                                <td><span class="fw-bold">Denver: </span> </td>
                                <td>
                                    @foreach($mintmarkStyles['Denver'] as $mintmark)
                                        <a href="{{ route('coin.view_lincoln_mms', [
                                                    'mint' => 'Denver',
                                                    'mms' => $mintmark,
                                                    ]) }}">{{$mintmark}}</a>
                                        @if(!$loop->last)
                                            ,
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                        @endif
                        @if(isset($mintmarkStyles['San Francisco'][1]))
                            <tr>
                                <td><span class="fw-bold">San Francisco: </span> </td>
                                <td>
                                    @foreach($mintmarkStyles['San Francisco'] as $mintmark)
                                        <a href="{{ route('coin.view_lincoln_mms', [
                                                    'mint' => 'San_Francisco',
                                                    'mms' => $mintmark,
                                                    ]) }}">{{$mintmark}}</a>
                                        @if(!$loop->last)
                                            ,
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                        @endif
                        @if(isset($mintmarkStyles['Philadelphia'][1]))
                            <tr>
                                <td><span class="fw-bold">Philadelphia: </span> </td>
                                <td>
                                    @foreach($mintmarkStyles['Philadelphia'] as $mintmark)
                                        <a href="{{ route('coin.view_lincoln_mms', [
                                                    'mint' => 'Philadelphia',
                                                    'mms' => $mintmark,
                                                    ]) }}">{{$mintmark}}</a>
                                        @if(!$loop->last)
                                            ,
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                        @endif

                    </table>
                </div>
            </div>


            <div class="card mt-3">
                <div class="card-body">
                    <table class="table">
                        <tbody>
                        <tr style="text-align: left;">
                            <th>Years</th>
                            <th>Material</th>
                            <th>Weight<br>(grams)</th>
                        </tr>
                        <tr>
                            <td><a href="{{ route('coin.view_lincoln_metal', ['group' => 1]) }}"> 1909–1942</a> </td>
                            <td>Bronze (95% Copper, 5% Tin and Zinc)</td>
                            <td>3.11
                            </td>
                        </tr>
                        <tr>
                            <td><a href="{{ route('coin.view_lincoln_metal', ['group' => 2]) }}">1943</a></td>
                            <td>Zinc-Coated Steel</td>
                            <td>2.72
                            </td>
                        </tr>
                        <tr>
                            <td><a href="{{ route('coin.view_lincoln_metal', ['group' => 3]) }}">1944–1946</a></td>
                            <td>Gilding Metal (95% Copper, 5% Zinc)</td>
                            <td>3.11
                            </td>
                        </tr>
                        <tr>
                            <td><a href="{{ route('coin.view_lincoln_metal', ['group' => 4]) }}">1947–1962</a></td>
                            <td>Bronze (95% copper, 5% Tin and Zinc)</td>
                            <td>3.11
                            </td>
                        </tr>
                        <tr>
                            <td><a href="{{ route('coin.view_lincoln_metal', ['group' => 5]) }}">1962 – September 1982</a></td>
                            <td>Gilding Metal (95% Copper, 5% Zinc)</td>
                            <td>3.11
                            </td>
                        </tr>
                        <tr>
                            <td><a href="{{ route('coin.view_lincoln_metal', ['group' => 6]) }}">October 1982 – Present</a></td>
                            <td>Copper-Plated Zinc (97.5% Zinc, 2.5% Copper)</td>
                            <td>2.5
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>


            <div class="card mb-5 mt-3">
                <div class="card-body">
                    <h5>Album/Folders</h5>
                    <table class="table table-hover table-striped">
                        @foreach ($folders as $folder)
                            <tr>
                                <td>
                                    <a href="{{ route('coin.set_view', ['mintset' => $folder->id]) }}">{{$folder->folderName}}
                                    </a> #{{$folder->folderCode}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>

            </div>



        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">By Year</div>
                <div class="card-body">
                    <div class="input-group mb-3">
                        <select class="form-select" id="coin_date">
                            @php $output = range(1909,date('Y')); @endphp
                            @foreach($output as $date)
                                <option value="{{sprintf("%02d", $date)}}">{{sprintf("%02d", $date)}}</option>
                            @endforeach
                        </select>
                        <button class="btn btn-outline-secondary" type="button" id="coin_date_btn">Load</button>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">By Mint</div>
                <div class="card-body">
                    <div class="input-group mb-3">
                        <select class="form-select" id="coin_mints">
                            <option value="{{ route('coin.view_lincoln_by_mint', ['mint' => 'Denver']) }}">Denver</option>
                            <option value="{{ route('coin.view_lincoln_by_mint', ['mint' => 'Philadelphia']) }}">Philadelphia</option>
                            <option value="{{ route('coin.view_lincoln_by_mint', ['mint' => 'San_Francisco']) }}">San Francisco</option>
                            <option value="{{ route('coin.view_lincoln_by_mint', ['mint' => 'West_Point']) }}">West Point</option>
                        </select>
                        <button class="btn btn-outline-secondary" type="button" id="coin_mints_btn">Load</button>
                    </div>
                </div>
            </div>

            <div class="card mb-4">
                <div class="card-header">Sections</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('coin.seated_index') }}">Seated Liberty</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">Barber</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">Capped Bust</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">19th Century</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('coin.coin_index') }}">Classics ()</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">Commemoratives</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">Morgan Dollars</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">Pre-1933 Gold</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">External References</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link"
                                       href="http://www.lincolncentresource.com/index.html">The Lincoln Cent Resource</a></li>
                                <li><a class="external_link" href="http://www.coppercoins.com/">Coppercoins</a></li>
                                <li><a class="external_link" href="http://www.traildies.com/">Trail and Wavy Step</a></li>

                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="http://www.varietyvista.com/">Variety Vista</a>
                                </li>
                                <li><a class="external_link" href="http://www.cuds-on-coins.com/">Cuds-On-Coins</a></li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">

                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.view_directory') }}">User Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            @endif
        </div>
    </div>
@push('styles')
    <style>
        .img-seated {
            height: 30px; width: auto;
        }
        #set_type_div img {
            width: 50px; height: auto;
        }
        .set_img {
            height: 60px; width: auto;
        }
        #set_type_div div.card {
            min-height: 200px;
        }
    </style>
@endpush
    @push('scripts')
        <script>
            (function (ENVIRONMENT) {
                try {

                    let coin_date = document.getElementById("coin_date");
                    document.getElementById("coin_date_btn").addEventListener("click", function () {
                        let date_year = coin_date.options[coin_date.selectedIndex].value;
                        let url = "{{ route('coin.lincoln_index_year_view', [':year']) }}".replace(':year', date_year);
                        window.location.href = url;
                    });

                    let coin_mints = document.getElementById("coin_mints");
                    document.getElementById("coin_mints_btn").addEventListener("click", function () {
                        let url = coin_mints.options[coin_mints.selectedIndex].value;
                        //console.log(url);
                        window.location.href = url;
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection

