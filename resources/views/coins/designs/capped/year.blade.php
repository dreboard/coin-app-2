@extends('layouts.user.main')
@section('pageTitle', "Releases for Year $year")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Releases for Year: {{ $year }} </h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    My Sets
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item" href="{{ route('coin.coin_index') }}">My Coins</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Sets</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Folders</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Rolls</a></li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.capped_index') }}">Capped Bust</a></li>
    </ol>


    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                Capped Bust Mint releases for {{ $year }}.
            </div>

            <div class="row">
                <div class="col-sm-6 float-start">
                    <div class="input-group mb-3">
                        @if($year > 1807)<a class="btn btn-secondary" href="{{ route('coin.seated_year_view', ['year' => $year - 1]) }}">{{ $year -1 }}</a> @endif
                        @if($year < 1839)<a class="btn btn-secondary float-end" href="{{ route('coin.seated_year_view', ['year' => $year + 1]) }}">{{ $year +1 }}</a> @endif
                    </div>



                </div>
                <div class="col-sm-6">
                    <div class="input-group mb-3">
                        <select class="form-select" id="coin_date">
                            @php $output = range(1807,1839); @endphp
                            @foreach($output as $date)
                                <option value="{{sprintf("%02d", $date)}}">{{sprintf("%02d", $date)}}</option>
                            @endforeach
                        </select>
                        <button class="btn btn-outline-secondary" type="button" id="coin_date_btn">Load</button>
                    </div>

                </div>
            </div>
            <table class="table">
                <tr>
                    <td><span class="fw-bold">Saved: </span> ({{$count}})</td>
                    <td><span class="fw-bold">Invested: </span> ({{$cost}})</td>
                    <td><span class="fw-bold">Face Val: </span> ({{$count}})</td>
                </tr>
            </table>

            <table class="table table-striped table-hover">
                    <tr>
                        <th class="fw-bold fs-6">Coins</th>
                        <th class="fw-bold">Collected</th>
                    </tr>
                    @foreach($coins as $coin)
                        <tr>
                            <td>
                                <a href="{{ route('coin.view_coin', ['coin' => $coin->id]) }}">{{ $coin->coinName }} {{ $coin->coinType }}</a>
                            </td>
                            <td id="coin_count_row">
                                <span class="coin_id_count" data-coin="{{$coin->id}}">Loading...</span>
                            </td>
                        </tr>
                    @endforeach
                </table>

        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Add {{ $year }} Coin</div>
                <div class="card-body">

                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                data-bs-toggle="dropdown" aria-expanded="false">
                            Add New
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            @foreach($coins as $coin)
                                <li><a class="dropdown-item"
                                       href="{{ route('collected.save_by_coin_id', ['id' => $coin->id]) }}">{{ $coin->coinName }} {{ $coin->coinType }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('coin.view_type', ['id' => $year]) }}">Color
                                        Report</a></li>

                                @if($year == 'Lincoln Wheat')
                                    <li><a href="{{ route('coin.view_type', ['id' => $year]) }}">Color
                                            Report</a></li>

                                @endif
                                @if($year == 'Lincoheat')
                                    <li><a href="{{ route('coin.view_type', ['id' => $year]) }}">Coloport</a>
                                    </li>

                                @endif
                                @if($year == 'Lincoheat')
                                    <li>
                                        <a href="{{ route('coin.view_type', ['id' => $year]) }}">Coloport</a>
                                    </li>
                                @endif
                                @if($year == 'Lincoheat')
                                    <li>
                                        <a href="{{ route('coin.view_type', ['id' => $year]) }}">Coloport</a>
                                    </li>
                                @endif
                                @if($year == 'Lincoheat')
                                    <li>
                                        <a href="{{ route('coin.view_type', ['id' => $year]) }}">Coloport</a>
                                    </li>
                                @endif
                                @if($year == 'Lincoheat')
                                    <li>
                                        <a href="{{ route('coin.view_type', ['id' => $year]) }}">Coloport</a>
                                    </li>
                                @endif

                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('coin.year_view', ['year' => $year]) }}">All {{ $year }}</a></li>
                                <li><a class="external_link" href="http://www.cuds-on-coins.com/">Cuds-On-Coins</a></li>
                                <li><a class="external_link" href="http://www.traildies.com/">Trail Dies</a></li>
                                <li><a class="external_link"
                                       href="http://ec2-13-58-222-16.us-east-2.compute.amazonaws.com/wiki/Home">VAMWorld</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Projects</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('user.user_types', ['type' => 'Research']) }}">Scholar
                                        Directory</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
@push('scripts')
    <script>


        (function (ENVIRONMENT) {
            try {

                const coin_counts = document.querySelectorAll('.coin_id_count');
                const set_counts = document.querySelectorAll('.set_id_count');
                Array.from(coin_counts)
                    .forEach(function (coin_count) {
                        //console.log(coin_count.dataset.coin);
                        window.addEventListener('load', function (event) {
                            let url = "{{ route('coin.seated_year_count_by_coin', [':coin_id', ':year']) }}".replace(':coin_id', coin_count.dataset.coin).replace(':year', {{$year}});
                            fetch(url, {
                                "method": "GET",
                            }).then(
                                response => {
                                    response.json().then(
                                        data => {
                                            console.log(data);
                                            coin_count.innerHTML = data.count;
                                        }
                                    )
                                })
                        }, false)
                    })


                let coin_date = document.getElementById("coin_date");
                document.getElementById("coin_date_btn").addEventListener("click", function () {
                    let date_year = coin_date.options[coin_date.selectedIndex].value;
                    let url = "{{ route('coin.seated_year_view', [':year']) }}".replace(':year', date_year);
                    //console.log(url);
                    window.location.href = url;
                });

            } catch (error) {
                if (ENVIRONMENT === 'local') {
                    console.error(error);
                }
            }
        })();
    </script>
@endpush
