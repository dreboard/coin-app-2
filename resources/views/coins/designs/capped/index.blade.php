@extends('layouts.user.main')
@section('pageTitle', 'Capped Bust Coins')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Capped Bust Coins</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Switch Type
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('coin.lincoln_index') }}">Lincoln Cents</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.seated_index') }}">Seated Liberty</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.draped_index') }}">Draped Bust</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">Coins Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                Capped Bust U.S. Coins 1807 to 1839.
            </div>



            <div class="table-responsive mb-2">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="fw-bold">#</th>
                        <th class="fw-bold">Coin</th>
                        <th class="fw-bold">Grades</th>
                        <th class="fw-bold">Attribution</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($types as $id => $type)
                        <tr>
                            <th>
                                    <img src="{{config('app.image_url')}}{{ str_replace(' ', '_', $type) }}.jpg"
                                         class="img-seated" alt="..."></th>
                            <td scope="row"><a href="{{ route('coin.view_type', ['id' => $id]) }}">{{ $type }}</a></td>
                            <td><a href="{{ route('coin.type_grades', ['coinType' => $id]) }}">Grade
                                    Report</a></td>
                            <td><a href="{{ route('coin.seated_type', ['type' => $id]) }}">Report</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>


            <div class="table-responsive mb-2">
                <h4>Attribution/References</h4>
                    <table class="table">
                        <tr>
                            <td><a href="{{ route('coin.type_grades', ['coinType' => 'Valentine']) }}">Valentine</a></td>
                            <td><a href="{{ route('coin.type_grades', ['coinType' => 'Wiley-Bugert']) }}">Wiley-Bugert</a></td>
                            <td><a href="{{ route('coin.type_grades', ['coinType' => 'Brunner-Frost']) }}">Brunner-Frost</a></td>
                            <td><a href="{{ route('coin.type_grades', ['coinType' => 'Fivaz-Stanton']) }}">Fivaz-Stanton</a></td>
                            <td><a href="{{ route('coin.type_grades', ['coinType' => 'PCGS']) }}">PCGS</a></td>
                        </tr>
                    </table>
            </div>


            <h4>Type Sets</h4>
            <div class="row">
                @foreach($types as $id => $type)
                    <div class="col">
                        <div class="text-center">
                            <a href="{{ route('coin.view_type', ['id' => $id]) }}">
                                <img src="{{config('app.image_url')}}{{ str_replace(' ', '_', $type) }}.jpg"
                                     class="set_img" alt="...">
                            </a>
                            <div class="card-body">
                                <p class="card-title">{{ $type }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>










        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Capped By Year</div>
                <div class="card-body">
                    <div class="input-group mb-3">
                        <select class="form-select" id="coin_date">
                            @php $output = range(1807,1839); @endphp
                            @foreach($output as $date)
                                <option value="{{sprintf("%02d", $date)}}">{{sprintf("%02d", $date)}}</option>
                            @endforeach
                        </select>
                        <button class="btn btn-outline-secondary" type="button" id="coin_date_btn">Load</button>
                    </div>
                </div>
            </div> <div class="card mb-4">
                <div class="card-header">Capped By Mint</div>
                <div class="card-body">
                    <div class="input-group mb-3">
                        <select class="form-select" id="coin_date">
                            <option value="{{ route('coin.view_type', ['id' => $id]) }}">Philadelphia</option>
                            <option value="{{ route('coin.view_type', ['id' => $id]) }}">New Orleans</option>
                        </select>
                        <button class="btn btn-outline-secondary" type="button" id="coin_date_btn">Load</button>
                    </div>
                </div>
            </div>

            <div class="card mb-4">
                <div class="card-header">External References</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="https://www.jrcs.org/">John Reich Collectors Society</a></li>
                                <li><a class="external_link" href="https://518616078926146539.weebly.com/">Capped Bust Half-Dime - Attribution Guide</a></li>
                                <li><a class="external_link" href="http://www.byronreed.com/byrons_collections/capped_bust_dimes/">Capped Bust Dimes by Die Marriage</a></li>
                                <li><a class="external_link" href="https://www.jrcs.org/jrcs_late_die_showcase.php">Late Die State Showcase</a></li>
                                <li><a class="external_link" href="http://www.seateddollarvarieties.com/">Register of Liberty Seated Dollar Varieties</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card mb-4">
                <div class="card-header">Sections</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('coin.coin_index') }}">CappedBust</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">Barber</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">Capped Bust</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">19th Century</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('coin.coin_index') }}">Classics ()</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">Commemoratives</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">Morgan Dollars</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">Pre-1933 Gold</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link"
                                       href="https://catalog.usmint.gov/product-schedule/?cm_sp=CLP-_-mintmark-sched-_-040121&scp=SCHED">US
                                        Mint Product Schedule</a></li>
                                <li><a class="external_link" href="https://minterrornews.com/">Mint Error News</a></li>
                                <li><a class="external_link" href="https://conecaonline.org/">CONECA</a></li>
                                <li><a class="external_link" href="http://www.doubleddie.com/1801.html">Wexler’s Die
                                        Varieties</a></li>
                                <li><a class="external_link" href="http://varietyvista.com/Attribution%20Services.htm">Variety
                                        Vista Attribution Services</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="http://www.maddieclashes.com/">MAD Die Clashes</a>
                                </li>
                                <li><a class="external_link" href="http://www.cuds-on-coins.com/">Cuds-On-Coins</a></li>
                                <li><a class="external_link" href="http://www.traildies.com/">Trail Dies</a></li>
                                <li><a class="external_link"
                                       href="http://ec2-13-58-222-16.us-east-2.compute.amazonaws.com/wiki/Home">VAMWorld</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@push('styles')
    <style>
        .img-seated {
            height: 30px; width: auto;
        }
        .set_img {
            height: 60px; width: auto;
        }
    </style>
@endpush
    @push('scripts')
        <script>


            (function (ENVIRONMENT) {
                try {

                    let coin_date = document.getElementById("coin_date");
                    document.getElementById("coin_date_btn").addEventListener("click", function () {
                        let date_year = coin_date.options[coin_date.selectedIndex].value;
                        let url = "{{ route('coin.capped_year_view', [':year']) }}".replace(':year', date_year);
                        //console.log(url);
                        window.location.href = url;
                    });

                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection

