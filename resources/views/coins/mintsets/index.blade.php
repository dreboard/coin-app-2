@extends('layouts.user.main')
@section('pageTitle', 'Mintsets Home')
@section('content')



    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Mintsets</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">Go</button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">Set Home</a></li>
                    <li><a class="dropdown-item" href="{{ route('collected.view_collected_sets') }}">My Sets</a></li>
                    <li><a class="dropdown-item" href="{{ route('collected.start_mintset') }}">Add Set</a></li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('collected.view_collected_sets') }}">My Sets</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                U.S. Mint issued sets and slabbed sets.
            </div>

            <div class="row">
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Mintsets</h4>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Do Something
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('collected.view_collected_sets') }}">See All</a></li>
                                <li><a class="dropdown-item" href="{{ route('collected.view_collected_sets') }}">Add</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Errors</h4>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Do Something
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('member.view_books') }}">See All</a></li>
                                <li><a class="dropdown-item" href="{{ route('member.create_posts') }}">Create (Found A
                                        new error)</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Add Something</h4>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Select
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('collected.start') }}">Coin</a></li>
                                <li><a class="dropdown-item" href="{{ route('collected.start_folder') }}">Folder/Album</a></li>
                                <li><a class="dropdown-item" href="{{ route('collected.start_roll') }}">Rolls</a></li>
                                <li><a class="dropdown-item" href="{{ route('collected.start_mintset') }}">Minset</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <h4>Sets</h4>
            <div class="table-responsive">
                <table class="table table-hover datatable">
                    <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Type</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($mintSets as $set)
                        <tr class="set_display_rows">
                            <td class="w-75"><a href="{{ route('coin.set_view', ['mintset' => $set->id]) }}">{{$set->setName}}</a></td>
                            <td>{{$set->setType}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Add A Mintset</div>
                <div class="card-body">
                    <div class="input-group">
                        <a href="{{ route('member.project_intro') }}" class="btn btn-primary" id="button-search"
                           type="button">Go!</a>
                    </div>
                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="https://www.error-ref.com/">Error-Variety Ready
                                        Reference</a></li>
                                <li><a class="external_link" href="https://minterrornews.com/">Mint Error News</a></li>
                                <li><a class="external_link" href="https://conecaonline.org/">CONECA</a></li>
                                <li><a class="external_link" href="http://www.doubleddie.com/1801.html">Wexler’s Die
                                        Varieties</a></li>
                                <li><a class="external_link" href="http://varietyvista.com/Attribution%20Services.htm">Variety
                                        Vista Attribution Services</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="http://www.maddieclashes.com/">MAD Die Clashes</a>
                                </li>
                                <li><a class="external_link" href="http://www.cuds-on-coins.com/">Cuds-On-Coins</a></li>
                                <li><a class="external_link" href="http://www.traildies.com/">Trail Dies</a></li>
                                <li><a class="external_link"
                                       href="http://ec2-13-58-222-16.us-east-2.compute.amazonaws.com/wiki/Home">VAMWorld</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Projects</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('user.user_types', ['type' => 'Research']) }}">Scholar
                                        Directory</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

