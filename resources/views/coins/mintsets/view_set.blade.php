@extends('layouts.user.main')
@section('pageTitle', 'View Mintset')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">{{ $mintset->setName}}</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    My Sets
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item" href="{{ route('coin.coin_index') }}">My Coins</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Sets</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Folders</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Rolls</a></li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.set_index') }}">Sets Home</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                All {{ $mintset->setName}} sets and related coins. @if(str_contains($mintset->setName, 'American Eagle'))
                    Part of the <a href="{{ route('coin.view_eagle_index') }}">American Eagle Coins</a> Series
                @endif
            </div>


            <div class="card p-3">

                <div class="row">
                    <div class="col-9">
                        <h5>Coins In Set</h5>
                    </div>
                    <div class="col-3">

                        <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                Add This Set
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item"
                                       href="{{ route('collected.create_set_by_id', ['set_id' => $mintset->id]) }}">Regular Issue</a>
                                </li>
                                <li><a class="dropdown-item"
                                       href="{{ route('collected.create_set_slab', ['set_id' => $mintset->id]) }}">Slabbed Set</a>
                                </li>
                                <li><a class="dropdown-item"
                                       href="{{ route('collected.create_set_loose', ['set_id' => $mintset->id]) }}">Loose Set</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <table class="table">
                    @foreach($coins as $coin)
                        <tr>
                            <td><a href="{{ route('coin.view_coin', ['coin' => $coin->id]) }}">{{ $coin->coinName }} {{ $coin->coinType }}</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>


            <h3 class="my-3">Saved Sets</h3>
            <div class="table-responsive">
                <table class="table table-hover datatable">
                    <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Type</th>
                        <th scope="col">Created</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($collected as $collect)
                        <tr>
                            <td class="w-75">
                                <a href="{{ route('collected.view_collected_set', ['set' => $collect->id]) }}">{{ Str::limit($collect->nickname, 55) ?? 'No Name' }}</a>
                            </td>
                            <td>
                                <a href="{{ route('collected.view_collected_set', ['set' => $collect->id]) }}">{{optional($collect)->set_version}}</a>
                            </td>
                            <td>{{ Carbon\Carbon::parse($collect->created_at)->format('M jS Y') }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td></td>
                            <td>No {{ $mintset->setName }} saved</td>
                            <td></td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>

            <div>
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                            data-bs-toggle="dropdown" aria-expanded="false">
                        Add This Set
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <li><a class="dropdown-item"
                               href="{{ route('collected.create_set_by_id', ['set_id' => $mintset->id]) }}">Quick Add</a>
                        </li>
                        <li><a class="dropdown-item"
                               href="{{ route('collected.create_set_slab', ['set_id' => $mintset->id]) }}">Slabbed Set</a>
                        </li>
                        <li><a class="dropdown-item"
                               href="{{ route('collected.create_set_loose', ['set_id' => $mintset->id]) }}">Loose Set</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">This Mintset</div>
                <div class="card-body">
                    <form method="post" action="{{ route('collected.quick_save_set_id') }}" class="row row-cols-lg-auto g-3 align-items-center">
                        @csrf
                        <input type="hidden" name="set_id" value="{{$mintset->id}}">
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary">Quick Add</button>
                        </div>
                    </form>
                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Related</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('coin.year_view', ['year' => $mintset->coinYear]) }}">All {{ $mintset->coinYear }}</a></li>
                                <li><a href="{{ route('coin.set_index') }}">All {{$mintset->strike}} Strikes</a></li>

                            </ul>

                        </div>
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="https://www.greysheet.com/coin-prices/group/united-states-proof-and-mint-sets">Greysheet & CPG® PRICE GUIDE</a></li>
                                <li><a class="external_link" href="http://www.cuds-on-coins.com/">Cuds-On-Coins</a></li>
                                <li><a class="external_link" href="http://www.traildies.com/">Trail Dies</a></li>
                                <li><a class="external_link"
                                       href="http://ec2-13-58-222-16.us-east-2.compute.amazonaws.com/wiki/Home">VAMWorld</a>
                                </li>
                            </ul>


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

