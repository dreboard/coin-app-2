@extends('layouts.user.main')
@section('pageTitle', 'Add To Collection')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Add {{ $mintset->setName }}</h3>
            <small>Slabbed Set</small>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Collection Pages
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">Bacettings</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.edit_public', ['user' => auth()->user()->id]) }}">Edirofile</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_edit_avatar', ['user' => auth()->user()->id]) }}">Chaage</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.research') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('coin.set_index') }}">Mintsets</a>
        </li>
        <li class="breadcrumb-item"><a
                href="{{ route('collected.start_mintset') }}">Back To Start</a></li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <div class="alert alert-primary" role="alert">
                All coins from this set will be added to your collection.
                <button data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" type="button" class="btn-secondary btn-sm">View Coins</button>

            </div>
            <div class="collapse" id="collapseExample">
                <div class="card card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Name</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($coins as $coin)
                            <tr>
                                <td>{{ $coin['coinName'] }} {{ $coin['coinType'] }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>


            <form id="save_slabbed_set_form" action="{{ route('collected.save_coin_id') }}"
                  method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-md-12">
                    <input type="hidden" name="set_id" value="{{ $mintset->id }}">
                    <input type="hidden" name="set_version" value="Slabbed">
                    <div class="mb-3">
                        <label for="text" class="form-label">Nickname</label>
                        <input type="text" class="form-control" name="nickname" id="nickname"
                               value="{{ old('nickname') }}">
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="tpg_service" class="form-label">Grading Service</label>
                                <select class="form-select" name="tpg_service" id="tpg_service">
                                    <option value="None" selected>None</option>
                                    <option value="PCGS">PCGS (Professional Coin Grading Service)</option>
                                    <option value="NGC">NGC (Numismatic Guaranty Corporation of America)</option>
                                    <option value="ANACS">ANACS (American Numismatic Association Certification
                                        Service)
                                    </option>
                                    <option value="ICG">ICG (Independent Coin Grading Company)</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="grade" class="form-label">Grade</label>
                                <select class="form-select" name="grade" id="grade">

                                </select>
                            </div>
                        </div>

                    </div>

                    <div id="pro_div" class="row">
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="tpg_serial_num" class="form-label">Serial Number</label>
                                <input type="text" class="form-control" name="tpg_serial_num" id="tpg_serial_num"
                                       value="{{ old('tpg_serial_num') }}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="slab_condition" class="form-label">Slab Condition</label>
                                <select class="form-select" name="slab_condition" id="slab_condition">
                                    <option selected>None</option>
                                    <option value="Excellent">Excellent</option>
                                    <option value="Scratched Heavy">Scratched Heavy</option>
                                    <option value="Scratched Light">Scratched Light</option>
                                    <option value="Cracked">Cracked</option>
                                    <option value="Cracked Severe">Cracked Severe</option>
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="form-group mb-3">
                        <label class="control-label">Notes</label>
                        <textarea id="note" name="note" class="form-control">{{ old('note') }}</textarea>
                    </div>

                    <div class="mb-3 form-check">
                        <input type="checkbox" class="form-check-input" name="private" id="private">
                        <label class="form-check-label" for="private">Private (NOT viewable)</label>
                    </div>

                    <!-- Submit Form Input -->
                    <div class="col-3">
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </div>
            </form>
            <div class="mh-100"></div>
        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <h6>Quick Add (details later)</h6>
                            <form class="row row-cols-lg-auto g-3 align-items-center" id="add_bulk_coin_form"
                                  action="{{ route('collected.quick_save_set_id') }}">
                                <input type="hidden" name="set_id" value="{{ $mintset->id }}">
                                <div class="col-12">
                                    <label class="visually-hidden" for="inlineFormSelectPref">Preference</label>
                                    <select name="coin_amount" class="form-select" id="inlineFormSelectPref" required>
                                        <option selected>Choose...</option>
                                        <option value="1">1</option>
                                        <option value="10">10</option>
                                    </select>
                                </div>

                                <div class="col-12">
                                    <button id="add_bulk_coin_form_btn" type="submit" class="btn btn-primary">Add
                                    </button>
                                </div>
                            </form>

                            <h6 class="mb-0 mt-5">Recent Additions</h6>
                            @if(count($recents) > 0)
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Name</th>
                                        <th scope="col">Saved</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($recents as $recent)
                                        <tr>
                                            <td><a class="group_link"
                                                   href="{{ route('collected.view_collected_set', ['set' => $recent->id]) }}"
                                                >{{ Str::limit($recent->nickname, 20) ?? 'None'}}</a>
                                            </td>
                                            <td>
                                                {{ \Carbon\Carbon::parse($recent->created_at)->diffForHumans() }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <p>None saved</p>
                            @endif


                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    @push('styles')
        <style>
            .ck-editor__editable {
                min-height: 300px;
            }
        </style>
    @endpush
    @push('scripts')
        <script>
            (function () {
                try {
                    // https://stackoverflow.com/questions/71201157/how-to-display-data-in-an-html-table-using-fetch-api




                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
