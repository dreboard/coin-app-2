@extends('layouts.user.main')
@section('pageTitle', 'View Collected Mintset')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">{{ $collectedSet->nickname ?? 'No Name'}}</h3>
            <p class="mb-2"> <a href="{{ route('coin.set_view', ['mintset' => $collectedSet->mintset->id]) }}">{{$collectedSet->mintset->setName}}</a></p>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    My Sets
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item" href="{{ route('coin.coin_index') }}">My Coins</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Sets</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Folders</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Rolls</a></li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.set_index') }}">All Sets</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                {{$collectedSet->mintset->setName}} details and coins.
            </div>


            <div class="row">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <td><span class="fw-bold">Saved: </span> {{ Carbon\Carbon::parse($collectedSet->created_at)->format('M jS Y') }}</td>
                            <td><span class="fw-bold">Invested: </span> ({{$collectedSet->cost ?? number_format('0.00', 2)}})</td>
                            <td><span class="fw-bold">Condition: </span> ({{$collectedSet->condition}})</td>
                        </tr>
                    </table>
                </div>
                <div class="table-responsive">
                    <h5>Saved Coins In Set</h5>
                    <table class="table">
                        @foreach($coins as $collectedCoin)
                            <tr>
                                <td><a href="{{ route('collected.view_collected', ['collected' => $collectedCoin->id]) }}">{{ $collectedCoin->coin->coinName }} {{ $collectedCoin->coin->coinType }}</a>
                                    </td>
                            </tr>
                        @endforeach
                    </table>
                </div>

                <div class="col-lg-12">
                    <a class="btn btn-secondary" href="{{ route('coin.set_index') }}">See All Sets</a>
                </div>
            </div>
        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Add Mintsets</div>
                <div class="card-body">

                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                data-bs-toggle="dropdown" aria-expanded="false">
                            Add This Set
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.create_set_by_id', ['set_id' => $collectedSet->mintset->id]) }}">Quick Add</a>
                            </li>
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.create_set_by_id', ['set_id' => $collectedSet->mintset->id]) }}">Detailed Add</a>
                            </li>
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.create_set_slab', ['set_id' => $collectedSet->mintset->id]) }}">Slabbed Set</a>
                            </li>
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.create_set_loose', ['set_id' => $collectedSet->mintset->id]) }}">Loose Set</a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Related</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">

                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('coin.year_view', ['year' => $collectedSet->mintset->coinYear]) }}">All {{ $collectedSet->mintset->coinYear }}</a></li>
                                <li><a href="{{ route('coin.set_index') }}">All {{$collectedSet->mintset->strike}} Strikes</a></li>


                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Projects</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('user.user_types', ['type' => 'Research']) }}">Scholar
                                        Directory</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

