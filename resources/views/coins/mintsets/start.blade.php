@extends('layouts.user.main')
@section('pageTitle', 'Add Mintset To Collection')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Add Mintset To Collection</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    My Sets
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item" href="{{ route('coin.coin_index') }}">My Coins</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Sets</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Folders</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Rolls</a></li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.set_index') }}">My Sets</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                You can add sets in 3 ways: <span class="fw-bold">Regular Issue, Loose and Slabbed</span>
            </div>

            <div class="row">
                <div id="set_year_div" class="col-4">
                    <div class="callout callout-primary">
                        <h4>By Year</h4>
                        <div class="input-group mb-3">
                            <select id="set_year_select" class="form-select" aria-label="Default select example">
                                <option selected>Select year</option>
                                @foreach($setYears as $setYear)
                                    <option value="{{$setYear->coinYear}}">{{$setYear->coinYear}}</option>
                                @endforeach
                            </select>
                            <button type="button" id="set_year_btn" class="btn btn-outline-primary">Load</button>
                        </div>
                    </div>
                </div>
                <div id="set_type_div" class="col-4">
                    <div class="callout callout-primary">
                        <h4>By Type</h4>
                        <div class="input-group mb-3">
                            <select id="set_type_select" class="form-select" aria-label="Default select example">
                                <option selected>Select type</option>
                                @foreach($setTypes as $setType)
                                    <option value="{{str_replace(' ', '_', $setType->setType)}}">{{$setType->setType}}</option>
                                @endforeach
                            </select>
                            <button type="button" id="set_type_btn" class="btn btn-outline-primary">Load</button>
                        </div>
                    </div>
                </div>

                <div id="set_type_div" class="col-4">
                    <div class="callout callout-primary">
                        <h4>&nbsp;</h4>
                        <div class="input-group mb-3">
                            <button type="button" id="set_reset_btn" class="btn btn-outline-secondary">Reset</button>
                        </div>
                    </div>
                </div>
            </div>


            <div class="table-responsive">
                <table id="set_type_tbl" class="table">
                    <thead>
                    <tr>
                        <th class="fw-bold">Name</th>
                        <th class="fw-bold">Quick Add</th>
                        <th class="fw-bold">Add</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th class="fw-bold">Name</th>
                        <th class="fw-bold">Quick Add</th>
                        <th class="fw-bold">Add</th>
                    </tr>
                    </tfoot>
                    <tbody id="set_display_row"></tbody>
                </table>
            </div>

        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Add More</div>
                <div class="card-body">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                data-bs-toggle="dropdown" aria-expanded="false">
                            Start Pages
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.start_mintset') }}">Mintset</a>
                            </li>
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.start_folder') }}">Folder</a>
                            </li>
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.start_roll') }}">Rolls</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="https://www.error-ref.com/">Error-Variety Ready
                                        Reference</a></li>
                                <li><a class="external_link" href="https://minterrornews.com/">Mint Error News</a></li>
                                <li><a class="external_link" href="https://conecaonline.org/">CONECA</a></li>
                                <li><a class="external_link" href="http://www.doubleddie.com/1801.html">Wexler’s Die
                                        Varieties</a></li>
                                <li><a class="external_link" href="http://varietyvista.com/Attribution%20Services.htm">Variety
                                        Vista Attribution Services</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="http://www.maddieclashes.com/">MAD Die Clashes</a>
                                </li>
                                <li><a class="external_link" href="http://www.cuds-on-coins.com/">Cuds-On-Coins</a></li>
                                <li><a class="external_link" href="http://www.traildies.com/">Trail Dies</a></li>
                                <li><a class="external_link"
                                       href="http://ec2-13-58-222-16.us-east-2.compute.amazonaws.com/wiki/Home">VAMWorld</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Projects</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('user.user_types', ['type' => 'Research']) }}">Scholar
                                        Directory</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @push('scripts')
        <script>

            (function () {
                try {
                    const csrf = document.querySelector('meta[name="csrf-token"]').content;
                    const csrf_field = '<input type="hidden" name="_token" value="' + csrf + '">';
                    // https://stackoverflow.com/questions/71201157/how-to-display-data-in-an-html-table-using-fetch-api
                    let set_year_btn = document.getElementById("set_year_btn");
                    let set_type_btn = document.getElementById("set_type_btn");
                    let set_year_select = document.getElementById("set_year_select");
                    let set_type_select = document.getElementById("set_type_select");
                    let set_display_row = document.getElementById("set_display_row");

                    document.getElementById("set_reset_btn").addEventListener("click", function () {
                        delete set_data;
                        delete year_data;
                        $('#set_type_tbl').dataTable().fnDestroy();
                        set_display_row.innerHTML = '';
                        set_year_select.selectedIndex = 0;
                        set_type_select.selectedIndex = 0;
                    });

                    set_type_btn.addEventListener("click", function () {
                        delete set_data;
                        delete year_data;

                        $('#set_type_tbl').dataTable().fnDestroy();
                        set_display_row.innerHTML = '';
                        set_year_select.selectedIndex = 0;
                        set_type = set_type_select.options[set_type_select.selectedIndex].value;
                        let url = "{{ route('collected.populate_set_type', [':set_type']) }}".replace(':set_type', set_type);
                        console.log(url);
                        fetch(url, {
                            "method": "GET",
                        }).then(
                            response => {
                                response.json().then(
                                    data => {
                                        if (ENVIRONMENT === 'local') {
                                            console.log(data);
                                        }
                                        set_display_row.innerHTML = '';
                                        set_data = "";

                                        data.sets.forEach((x) => {
                                            let quick_url = "{{ route('collected.quick_save_set_id') }}";
                                            let regular_url = "{{ route('collected.create_set_by_id', [':set_id']) }}".replace(':set_id', x.id);
                                            let loose_url = "{{ route('collected.create_set_loose', [':set_id']) }}".replace(':set_id', x.id);
                                            let slabbed_url = "{{ route('collected.create_set_slab', [':set_id']) }}".replace(':set_id', x.id);
                                            set_data += "<tr>";
                                            set_data += "<td class='text-start w-75'>" + x.setName + "</td>";
                                            set_data += "<td class='text-start'><form id='set_form_" + x.id + "' method=\"POST\" class=\"row row-cols-lg-auto g-3 align-items-center\" action='" + quick_url + "'>";
                                            set_data += "<input type=\"hidden\" name=\"set_id\" value='" + x.id + "'>";
                                            set_data += "<input type=\"hidden\" name=\"_token\" value='" + csrf + "'>";
                                            set_data += "<div class=\"col-12\"><button type=\"submit\" class=\"btn btn-sm btn-primary\">Quick Add</button></div></form></td>";
                                            set_data += "<td><div class=\"dropdown\"><button class=\"btn btn-sm btn-secondary dropdown-toggle\" type=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">More</button>";
                                            set_data += "<ul class=\"dropdown-menu\">"
                                            set_data += "<li><a class=\"dropdown-item\" href='" + regular_url + "'>Regular Set</a></li>";
                                            set_data += "<li><a class=\"dropdown-item\" href='" + loose_url + "'>Loose Set</a></li>";
                                            set_data += "<li><a class=\"dropdown-item\" href='" + slabbed_url + "'>Slabbed Set</a></li></ul></div></td>";
                                            set_data += "</tr>"
                                            jQuery('set_form_' + x.id).append(csrf_field);

                                        });
                                        set_display_row.innerHTML = '';
                                        set_display_row.innerHTML = set_data;
                                        jQuery('#set_type_tbl').dataTable();
                                    }
                                )
                            })
                    });

                    set_year_btn.addEventListener("click", function () {
                        delete year_data;
                        set_display_row.innerHTML = '';
                        set_type_select.selectedIndex = 0;

                        $('#set_type_tbl').dataTable().fnDestroy();

                        set_year = set_year_select.options[set_year_select.selectedIndex].value;
                        let url = "{{ route('collected.populate_set', [':set_year']) }}".replace(':set_year', set_year);
                        console.log(url);

                        fetch(url, {
                            "method": "GET",
                        }).then(
                            response => {
                                response.json().then(
                                    data => {
                                        if (ENVIRONMENT === 'local') {
                                            console.log(data);
                                        }

                                        year_data = "";

                                        data.sets.forEach((x) => {
                                            let quick_url = "{{ route('collected.quick_save_set_id') }}";
                                            let regular_url = "{{ route('collected.create_set_by_id', [':set_id']) }}".replace(':set_id', x.id);
                                            let loose_url = "{{ route('collected.create_set_loose', [':set_id']) }}".replace(':set_id', x.id);
                                            let slabbed_url = "{{ route('collected.create_set_slab', [':set_id']) }}".replace(':set_id', x.id);
                                            year_data += "<tr>";
                                            year_data += "<td class='text-start w-75'>" + x.setName + "</td>";
                                            year_data += "<td class='text-start'><form id='set_form_" + x.id + "' method=\"POST\" class=\"row row-cols-lg-auto g-3 align-items-center\" action='" + quick_url + "'>";
                                            year_data += "<input type=\"hidden\" name=\"set_id\" value='" + x.id + "'>";
                                            year_data += "<input type=\"hidden\" name=\"_token\" value='" + csrf + "'>";
                                            year_data += "<div class=\"col-12\"><button type=\"submit\" class=\"btn btn-sm btn-primary\">Quick Add</button></div></form></td>";
                                            year_data += "<td><div class=\"dropdown\"><button class=\"btn btn-sm btn-secondary dropdown-toggle\" type=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">More</button>";
                                            year_data += "<ul class=\"dropdown-menu\">"
                                            year_data += "<li><a class=\"dropdown-item\" href='" + regular_url + "'>Regular Set</a></li>";
                                            year_data += "<li><a class=\"dropdown-item\" href='" + loose_url + "'>Loose Set</a></li>";
                                            year_data += "<li><a class=\"dropdown-item\" href='" + slabbed_url + "'>Slabbed Set</a></li></ul></div></td>";
                                            year_data += "</tr>"
                                            jQuery('set_form_' + x.id).append(csrf_field);

                                        });
                                        set_display_row.innerHTML = '';
                                        set_display_row.innerHTML = year_data;
                                        jQuery('#set_type_tbl').dataTable();
                                    }
                                )
                            })
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection

