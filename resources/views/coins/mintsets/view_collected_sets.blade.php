@extends('layouts.user.main')
@section('pageTitle', 'View Collected Mintsets')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Collected Mintsets</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    My Sets
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item" href="{{ route('coin.coin_index') }}">My Coins</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Sets</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Folders</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Rolls</a></li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.set_index') }}">All Sets</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                This section is for collectors to document newly discovered coin varieties or errors on U.S. Coins.
            </div>


            <h4>Sets</h4>
            <table class="table">
                <tr>
                    <td class="text-bg-success">Loose</td>
                    <td class="text-bg-danger">Issued</td>
                    <td class="text-bg-info">Slabbed</td>
                </tr>
            </table>
            <div class="table-responsive">
                <table class="table table-hover datatable">
                    <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Type</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($collectedSets as $set)
                        <tr class="set_display_rows">
                            <td class="w-50"><a class=" @if($set->set_version == 'Loose') link-success @endif @if($set->set_version == 'Issued') link-danger @endif @if($set->set_version == 'Slabbed') link-info @endif"
                                                href="{{ route('collected.view_collected_set', ['set' => $set->id]) }}">
                                    {{$set->nickname ?? 'No Name'}}
                                </a>
                            </td>
                            <td>{{$set->mintset->setName}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Add Mintsets</div>
                <div class="card-body">

                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                data-bs-toggle="dropdown" aria-expanded="false">
                            Add New
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.start_mintset') }}">Mintset</a>
                            </li>
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.start_folder') }}">Folder</a>
                            </li>
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.start_roll') }}">Rolls</a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">

                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="http://www.maddieclashes.com/">MAD Die Clashes</a>
                                </li>
                                <li><a class="external_link" href="http://www.cuds-on-coins.com/">Cuds-On-Coins</a></li>
                                <li><a class="external_link" href="http://www.traildies.com/">Trail Dies</a></li>
                                <li><a class="external_link"
                                       href="http://ec2-13-58-222-16.us-east-2.compute.amazonaws.com/wiki/Home">VAMWorld</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Projects</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('user.user_types', ['type' => 'Research']) }}">Scholar
                                        Directory</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

