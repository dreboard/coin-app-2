@extends('layouts.user.main')
@section('pageTitle', "View Category - $category->coinCategory")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">{{ $category->coinCategory }}</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Add New
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Coin</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.edit_public', ['user' => auth()->user()->id]) }}">Edofile</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_edit_avatar', ['user' => auth()->user()->id]) }}">Changmage</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">Coins Home</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                {{ $category->coinCategory }} collected coins and resources.
            </div>


            <div class="card mb-3">
                <div class="card-body">

                    <div class="row">
                        <div class="col-sm-4">
                            <img src="{{config('app.image_url')}}{{ str_replace(' ', '_', $category->coinCategory) }}.jpg"/>
                        </div>
                        <div class="col-sm-8">
                            <h5 class="card-title">Details</h5>
                            <table class="table">
                                <tr>
                                    <td><span class="fw-bold">Collected:</span></td>
                                    <td class="w-75">100</td>
                                </tr>
                                <tr>
                                    <td><span class="fw-bold">Value:</span></td>
                                    <td class="w-75">{{$category->denomination}} U.S. dollar</td>
                                </tr>
                                <tr>
                                    <td><span class="fw-bold">Investment:</span></td>
                                    <td>0.00</td>
                                </tr>
                                <tr>
                                    <td><span class="fw-bold">Face Val:</span></td>
                                    <td>0.00</td>
                                </tr>
                                <tr>
                                    <td><span class="fw-bold">Strikes:</span></td>
                                    <td>

                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            @if($category->id == 63)
                <div class="row text-center">
                    <div class="col-sm-6"><a href="{{ route('coin.coin_index') }}">Large Dollars</a></div>
                    <div class="col-sm-6"><a href="{{ route('coin.coin_index') }}">Small Dollars</a></div>
                </div>
            @endif

            @if($category->id == 46)
                <table class="table">
                    <tr><td colspan="3"></td></tr>
                    <tr>
                        <td><a href="{{ route('coin.coin_index') }}">Early Dates (1793-1814)</a></td>
                        <td><a href="{{ route('coin.coin_index') }}">Middle Dates (1816-1839)</a></td>
                        <td><a href="{{ route('coin.coin_index') }}">Late Dates (1839–1857)</a></td>
                    </tr>
                </table>
            @endif



            <div class="row">
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Varieties</h4>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Do Something
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('member.posts_index') }}">See All</a></li>
                                <li><a class="dropdown-item" href="{{ route('member.create_posts') }}">Create (Found A
                                        new variety)</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Errors</h4>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Do Something
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('member.view_books') }}">See All</a></li>
                                <li><a class="dropdown-item" href="{{ route('member.create_posts') }}">Create (Found A
                                        new error)</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Discussion Area</h4>
                        <a class="btn btn-secondary" href="{{ route('member.forum_cat_index', ['cat_id' => $category->id]) }}">Go →</a>
                    </div>
                </div>
            </div>

            <h4>Type Set Progress</h4>
            <div class="progress" style="height: 20px;">
                <div class="progress-bar" role="progressbar" aria-label="Example 20px high" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>

            <!-- Featured blog post-->
            <h4 class="mt-4 mb-3">Saved</h4>
            <!-- Nested row for non-featured blog posts-->
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-hover datatable">
                        <thead>
                        <tr>
                            <th scope="col">Type</th>
                            <th scope="col">Collected</th>
                            <th scope="col">Investment</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($types_collected as $type => $info)
                            <tr>
                                <td class="w-75">
                                    <a href="{{ route('coin.view_type', ['id' => Config::get('constants.type_id_list')[$type]]) }}">{{ Str::limit($type, 55) }}</a>
                                </td>
                                <td class="text-center">
                                    {{$info['collected']}}
                                </td>
                                <td class="text-center">
                                    {{$info['investment']}}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td>There are no new projects</td>
                                <td></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>

                <div class="col-lg-12">
                    <a class="btn btn-secondary" href="#!">All {{ $category->coinCategory }} History</a>
                </div>
            </div>
        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Switch</div>
                <div class="card-body">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                data-bs-toggle="dropdown" aria-expanded="false">
                            Categories
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            @foreach(Config::get('constants.coins.cat_id_array') AS $id => $cat)
                                <li><a class="dropdown-item"
                                       href="{{ route('coin.view_category', ['id' => $id]) }}">{{$cat}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="https://www.error-ref.com/">Error-Variety Ready
                                        Reference</a></li>
                                <li><a class="external_link" href="https://minterrornews.com/">Mint Error News</a></li>
                                <li><a class="external_link" href="https://conecaonline.org/">CONECA</a></li>
                                <li><a class="external_link" href="http://www.doubleddie.com/1801.html">Wexler’s Die
                                        Varieties</a></li>
                                <li><a class="external_link" href="http://varietyvista.com/Attribution%20Services.htm">Variety
                                        Vista Attribution Services</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="http://www.maddieclashes.com/">MAD Die Clashes</a>
                                </li>
                                <li><a class="external_link" href="http://www.cuds-on-coins.com/">Cuds-On-Coins</a></li>
                                <li><a class="external_link" href="http://www.traildies.com/">Trail Dies</a></li>
                                <li><a class="external_link"
                                       href="http://ec2-13-58-222-16.us-east-2.compute.amazonaws.com/wiki/Home">VAMWorld</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">
                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.view_directory') }}">User Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        </div>
    </div>

@endsection

