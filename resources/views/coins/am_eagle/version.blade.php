@extends('layouts.user.main')
@section('pageTitle', "$eagle_type Coins")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">{{$eagle_type}} Coins</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    View
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Coin</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Mintset</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Other</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">Coins Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.view_eagle_index') }}">American Eagle Coins</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                {{$eagle_type}} collected coins and resources.
            </div>


            <div class="card mb-3">
                <div class="card-body">
                    <div class="row text-center">
                        <div class="col-sm-6 mb-2"><a href="{{ route('coin.view_eagle_type', ['type' => 'Gold']) }}">American Gold Eagle</a></div>
                        <div class="col-sm-6 mb-2"><a href="{{ route('coin.view_eagle_type', ['type' => 'Platinum']) }}">American Platinum Eagle</a></div>
                        <div class="col-sm-6 mb-2"><a href="{{ route('coin.view_eagle_type', ['type' => 'Palladium']) }}">American Palladium Eagle</a></div>
                        <div class="col-sm-6 mb-2"><a href="{{ route('coin.view_eagle_type', ['type' => 'Silver']) }}">American Silver Eagle</a></div>
                    </div>
                </div>
            </div>


            @if($eagle_type == 'Silver American Eagle')
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="row text-center">
                            <div class="col-sm-12 mb-2 fw-bold">By Type</div>
                            <div class="col-sm-6 mb-2"><a href="{{ route('coin.view_type', ['id' => 101]) }}">Type I Reverse</a></div>
                            <div class="col-sm-6 mb-2"><a href="{{ route('coin.view_type', ['id' => 101]) }}">Type II Reverse</a></div>
                            <div class="col-sm-6 mb-2"><a href="{{ route('coin.view_type_strike', ['coinType' => 101, 'strike' => 'Proof']) }}">Proofs</a></div>
                            <div class="col-sm-6 mb-2"><a href="{{ route('coin.view_type_strike', ['coinType' => 101, 'strike' => 'Reverse_Proof']) }}">Reverse Proofs</a></div>
                        </div>
                    </div>
                </div>
            @endif

            @if($eagle_type == 'Platinum American Eagle')
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="row text-center">
                            <div class="col-sm-12 mb-2 fw-bold">All Platinum</div>
                            <div class="col-sm-6 mb-2"><a href="{{ route('coin.view_type', ['id' => 120]) }}">Tenth
                                    Ounce Platinum</a></div>
                            <div class="col-sm-6 mb-2"><a href="{{ route('coin.view_type', ['id' => 94]) }}">Quarter
                                    Ounce Platinum</a></div>
                            <div class="col-sm-6 mb-2"><a href="{{ route('coin.view_type', ['id' => 96]) }}">Half Ounce
                                    Platinum</a></div>
                            <div class="col-sm-6 mb-2"><a href="{{ route('coin.view_type', ['id' => 100]) }}">One Ounce
                                    Platinum</a></div>
                        </div>
                        @if(isset($series_list[0]))
                            <div class="row text-center">
                                <div class="col-sm-12 mb-2 fw-bold">All Series</div>
                                @foreach($series_list as $list)
                                    <div class="col-sm-6 mb-2"><a
                                            href="{{ route('coin.view_type', ['id' => 120]) }}">{{$list}}</a></div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            @endif

            <!-- Nested row for non-featured blog posts-->
            <div class="table-responsive">
                <table id="coins_tbl" class="table table-hover">
                    <thead>
                    <tr>
                        <th class="fw-bold">Type</th>
                        <th class="fw-bold">Collected</th>
                        <th class="fw-bold">Investment</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($coins as $key => $coin)
                        <tr>
                            <td class="w-75">
                                <a href="{{ route('coin.view_coin', ['coin' => $key]) }}">{{ Str::limit($coin['coin']->coinName, 55) }} {{$coin['coin']->coinType}}</a>
                            </td>
                            <td class="text-center">
                                {{$coin['collected']}}
                            </td>
                            <td class="text-center">
                                {{$coin['investment']}}
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td></td>
                            <td>No {{ $genre }} saved</td>
                            <td></td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>

            <div class="col-lg-12">
                <a class="btn btn-secondary" href="{{ route('coin.coin_index') }}">Coins Home</a>
            </div>
        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Save as a set</div>
                <div class="card-body">
                    <div class="input-group">
                        <a href="{{ route('collected.index') }}" class="btn btn-primary" id="button-search"
                           type="button">Go!</a>
                    </div>
                </div>
            </div>

            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Reports</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('coin.coin_index') }}">Precious Metals</a></li>

                                <li><a href="{{ route('coin.coin_index') }}">Grade Report</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">Proof Report</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('coin.coin_index') }}">Damage Report</a>
                                </li>
                                <li><a href="{{ route('coin.coin_index') }}">Variety Report</a>
                                </li>
                                <li><a href="{{ route('coin.coin_index') }}">Error Report</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">

                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="http://www.smalldollars.com/index.html">SmallDollars.com</a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">
                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.view_directory') }}">User Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    @push('scripts')
        <script>
            (function ($) {
                try {
                    $(document).ready( function() {
                        $(".datatable").dataTable().fnDestroy();

                        table = $('#coins_tbl').DataTable( {
                            "iDisplayLength": 50
                        } );
                    } );
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }


            }(window.jQuery));
        </script>
    @endpush
@endsection

