@extends('layouts.user.main')
@section('pageTitle', "American Eagle Coins")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">American Eagle Coins</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    View
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Coin</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Mintset</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Other</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">Coins Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.view_coin_commem_index') }}">Commemoratives</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                American Eagle collected coins and resources.
            </div>

            <div class="card mb-3">
                <div class="card-body">
                    <table class="table table-striped table-hover">
                        <tr>
                            <td><a href="{{ route('coin.view_eagle_type', ['type' => 'Gold']) }}">American Gold Eagle</a></td>
                            <td>Germany</td>
                            <td>Germany</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-sm btn-secondary dropdown-toggle" type="button"
                                            id="dropdownMenuButton1"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                        Types
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        @foreach($types_list['Gold American Eagle'] as $id => $type)
                                            <li><a class="dropdown-item"
                                                   href="{{ route('coin.view_type', ['id' => $id]) }}">{{$type}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </td>
                        <tr>
                            <td><a href="{{ route('coin.view_eagle_type', ['type' => 'Platinum']) }}">American Platinum Eagle</a></td>
                            <td>Germany</td>
                            <td>Germany</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-sm btn-secondary dropdown-toggle" type="button"
                                            id="dropdownMenuButton1"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                        Types
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        @foreach($types_list['American Platinum Eagle'] as $id => $type)
                                            <li><a class="dropdown-item"
                                                   href="{{ route('coin.view_type', ['id' => $id]) }}">{{$type}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="{{ route('coin.view_eagle_type', ['type' => 'Silver']) }}">American Silver Eagle</a></td>
                            <td>Germany</td>
                            <td>Germany</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-sm btn-secondary dropdown-toggle" type="button"
                                            id="dropdownMenuButton1"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                        Types
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        @foreach($types_list['American Silver Eagle'] as $id => $type)
                                            <li><a class="dropdown-item"
                                                   href="{{ route('coin.view_type', ['id' => $id]) }}">{{$type}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="{{ route('coin.view_eagle_type', ['type' => 'Palladium']) }}">American Palladium Eagle</a></td>
                            <td>Germany</td>
                            <td>Germany</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-sm btn-secondary dropdown-toggle" type="button"
                                            id="dropdownMenuButton1"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                        <span class="dropdown_text">Types</span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        @foreach($types_list['American Palladium Eagle'] as $id => $type)
                                            <li><a class="dropdown-item"
                                                   href="{{ route('coin.view_type', ['id' => $id]) }}">{{$type}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 sets_div">
                    <h4>Sets</h4>
                    <table>
                        @foreach($sets as $set)
                            <tr>
                                <td><a href="{{ route('coin.set_view',['mintset' => $set->id]) }}">{{$set->setName}}</a></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <div class="col-sm-6">
                    <h4>Collected</h4>
                    <table>
                        <tr>
                            <th>Sets</th>
                        </tr>
                        <tr>
                            <td><a href="{{ route('collected.start') }}">g65r4ege</a></td>
                        </tr>
                    </table>
                </div>
            </div>



            <div class="col-lg-12">
                <a class="btn btn-secondary" href="{{ route('coin.coin_index') }}">Coins Home</a>
            </div>
        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Save as a set</div>
                <div class="card-body">
                    <div class="input-group">
                        <a href="{{ route('collected.index') }}" class="btn btn-primary" id="button-search"
                           type="button">Go!</a>
                    </div>
                </div>
            </div>

            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Reports</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('coin.coin_index') }}">Grade
                                        Report</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">Proof Report</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('coin.coin_index') }}">Damage Report</a>
                                </li>
                                <li><a href="{{ route('coin.coin_index') }}">Variety Report</a>
                                </li>
                                <li><a href="{{ route('coin.coin_index') }}">Error Report</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">

                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="http://www.smalldollars.com/index.html">SmallDollars.com</a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">
                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.view_directory') }}">User Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    @push('styles')
        <style>
            .sets_div {
                height: 450px;
                overflow: scroll;
            }
        </style>
    @endpush
    @push('scripts')
        <script>
            (function ($) {
                try {
                    $(document).ready( function() {
                        $(".datatable").dataTable().fnDestroy();

                        table = $('#coins_tbl').DataTable( {
                            "iDisplayLength": 50
                        } );
                    } );
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }


            }(window.jQuery));
        </script>
    @endpush
@endsection

