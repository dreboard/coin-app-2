@extends('layouts.user.main')
@section('pageTitle', "Precious Metals Report")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Precious Metals Report</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    View
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Coin</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Mintset</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Other</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">Coins Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('coin.coin_tags')}}">All Tags</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                American Eagle collected coins and resources.
            </div>


            <div class="card mb-3">
                <div class="card-body">

                    <div class="row text-center">
                        <div class="col-sm-6 mb-2"><a href="{{ route('coin.view_eagle_type', ['type' => 'Gold']) }}">American Gold Eagle</a></div>
                        <div class="col-sm-6 mb-2"><a href="{{ route('coin.view_eagle_type', ['type' => 'Platinum']) }}">American Platinum Eagle</a></div>
                        <div class="col-sm-6 mb-2"><a href="{{ route('coin.view_eagle_type', ['type' => 'Palladium']) }}">American Palladium Eagle</a></div>
                        <div class="col-sm-6 mb-2"><a href="{{ route('coin.view_eagle_type', ['type' => 'Silver']) }}">American Silver Eagle</a></div>
                    </div>
                </div>
            </div>




            <div class="row">
                <div class="col-sm-6">
                    <div class="mb-4 text-center pr-5">
                        <div id="apmex-widget-spotprice"
                             style="width:100%;height:auto;background-color:#FFFFFF;border:1px Solid #ffffff;">
                            <div id="apmex-widget-spot-frame-target"><iframe src="http://widgets.apmex.com/widget/spotprice/?w=280&amp;h=180&amp;mtls=GSPL&amp;arf=True&amp;rint=30&amp;srf=False&amp;tId=1&amp;cId=c569305c-f274-4ebf-ab14-28b9dc53b479&amp;wId=1" frameBorder="0" width="280" height="180" scrolling="no" style="display:block;"></iframe></div>
                            <div id="apmex-widget-spot-target-footer" style="text-align: center; text-decoration: none;">
                                <a href="http://www.apmex.com" target="_blank" style="color: #000000; text-decoration: none;">
                                    <img src="https://widgets.apmex.com/content/themes/logos/blue.png" style="border:0px;" /></a></div></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-chart-bar me-1"></i>
                            What I Spend
                        </div>
                        <div class="card-body"><canvas id="myBarChart" width="100%" height="40"></canvas></div>
                    </div>
                </div>
            </div>

            <!-- Nested row for non-featured blog posts-->
            <div class="table-responsive">
                <table id="coins_tbl" class="table table-hover">
                    <thead>
                    <tr>
                        <th class="fw-bold">Type</th>
                        <th class="fw-bold">Weight Collected</th>
                        <th class="fw-bold">App Value</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><a href="{{ route('coin.metal_type', ['type' => 'Silver']) }}">Silver</a></td>
                        <td><span id="silver_weight"></span></td>
                        <td><span id="silver_value"></span></td>
                    </tr>
                    <tr>
                        <td><a href="{{ route('coin.metal_type', ['type' => 'Platinum']) }}">Platinum</a></td>
                        <td><span id="plat_weight"></span></td>
                        <td><span id="plat_value"></span></td>
                    </tr>
                    <tr>
                        <td><a href="{{ route('coin.metal_type', ['type' => 'Gold']) }}">Gold</a></td>
                        <td><span id="gold_weight"></span></td>
                        <td><span id="gold_value"></span></td>
                    </tr>
                    <tr>
                        <td><a href="{{ route('coin.metal_type', ['type' => 'Palladium']) }}">Palladium</a></td>
                        <td><span id="palladium_weight"></span></td>
                        <td><span id="palladium_value"></span></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-lg-12">
                <a class="btn btn-secondary" href="{{ route('coin.coin_index') }}">Coins Home</a>
            </div>
        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Save as a set</div>
                <div class="card-body">
                    <div class="input-group">
                        <a href="{{ route('collected.index') }}" class="btn btn-primary" id="button-search"
                           type="button">Go!</a>
                    </div>
                </div>
            </div>

            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Reports</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('coin.coin_index') }}">Grade
                                        Report</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">Proof Report</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('coin.coin_index') }}">Damage Report</a>
                                </li>
                                <li><a href="{{ route('coin.coin_index') }}">Variety Report</a>
                                </li>
                                <li><a href="{{ route('coin.coin_index') }}">Error Report</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">

                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="http://www.smalldollars.com/index.html">SmallDollars.com</a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">
                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.view_directory') }}">User Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

    @push('styles')
        <style>
            .forum_index_img {
                height: 30px;
                width: auto;
            }
        </style>
    @endpush
    @push('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
        <script>

            (function ($) {
                try {
                    document.getElementById("gold_value").innerHTML = 'hgt56r4eg';
                    var myHeaders = new Headers();
                    myHeaders.append("x-access-token", "goldapi-3ueb5rle6c3c3q-io");
                    myHeaders.append("Content-Type", "application/json");

                    var requestOptions = {
                        method: 'GET',
                        headers: myHeaders,
                        redirect: 'follow'
                    };
                    console.log('')
                    fetch("https://www.goldapi.io/api/XAU/USD", requestOptions)
                        .then(response => response.json())
                        .then(data => {
                            document.getElementById("gold_value").innerHTML = data.price;
                            console.log(data);
                        })
                        .catch(error => console.log('error', error));


                    fetch("https://www.goldapi.io/api/XAG/USD", requestOptions)
                        .then(response => response.json())
                        .then(data => {
                            document.getElementById("silver_value").innerHTML = data.price;
                            console.log(data);
                        })
                        .catch(error => console.log('error', error));


                } catch (error) {
                    console.error(error);

                }

            }(window.jQuery));

            (function ($) {
                try {
                    Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
                    Chart.defaults.global.defaultFontColor = '#292b2c';

// Bar Chart Example
                    var ctx = document.getElementById("myBarChart");
                    var myLineChart = new Chart(ctx, {
                        type: 'pie',
                        data: {
                            labels: ['Platinum', 'Gold', 'Silver', 'Palladuim'],
                            datasets: [{
                                data: [50, 60, 180, 190],
                                backgroundColor: [
                                    'rgb(255, 99, 132)',
                                    'rgb(255, 159, 64)',
                                    'rgb(255, 205, 86)',
                                    'rgb(54, 162, 235)',
                                ],
                            }]
                        }

                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }

                const cat_ids = document.querySelectorAll('.cat_ids');
                Array.from(cat_ids)
                    .forEach(function (cat_id) {
                        window.addEventListener('load', function (event) {
                            let url = "{{ route('member.get_category_info', [':cat_id']) }}".replace(':cat_id', cat_id.dataset.category);
                            fetch(url, {
                                "method": "GET",
                            }).then(
                                response => {
                                    response.json().then(
                                        data => {
                                            document.getElementById("cat_count_" + cat_id.dataset.category).innerHTML = data.data.total;
                                            document.getElementById("cat_date_" + cat_id.dataset.category).innerHTML = data.data.last;
                                        }
                                    )
                                })
                        }, false)
                    })
            }(window.jQuery));


        </script>


    @endpush
@endsection

