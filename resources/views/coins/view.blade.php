@extends('layouts.user.main')
@section('pageTitle', "View Coin $coin->coinName $coin->type->coinType")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">@if($coin->keyDate !== 0) <i class="bi bi-key"></i> @endif @if($coin->semiKeyDate !== 0) <i class="bi bi-key-fill"></i> @endif
                {{ $coin->coinName }} {{ $coin->type->coinType }}
            </h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    View
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">All {{ $coin->coinYear }} {{ $coin->type->coinType }}</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_type', ['id' => $coin->type->id]) }}">{{ $coin->type->coinType }}</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_category', ['id' => $coin->category->id]) }}">{{ $coin->category->coinCategory }}</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('member.forum_coin', ['id' => $coin->id]) }}">Discussions</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">All</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('coin.view_category', ['id' => $coin->category->id]) }}">{{ $coin->category->coinCategory }}</a>
        </li>
        <li class="breadcrumb-item"><a
                href="{{ route('coin.view_type', ['id' => $coin->type->id]) }}">{{ $coin->type->coinType }}</a></li>
        @if($coin->commemorative === 1)
            <li class="breadcrumb-item"><a href="{{ route('coin.view_type', ['id' => $coin->type->id]) }}">All
                    Commemoratives</a></li>
        @endif
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                Information about characteristics, varieties and errors
                for {{ $coin->coinName }} {{ $coin->type->coinType }}. @if($coin->series == "Lincoln Cents")
                    Part of
                    <a class="fw-bold" href="{{ route('coin.lincoln_index') }}">{{$coin->series}}
                        Series</a>
                @endif @if($coin->seriesType !== 'None')
                    {{$coin->seriesType}}
                @endif
            </div>

            <div class="row">
                @if(isset($sub_types[0]))
                    <div class="col-sm-6">
                        <h5>Major Varieties</h5>
                        <p>@foreach($sub_types as $sub_type)
                                @if ($sub_type->sub_type == 'None')
                                    @continue
                                @endif

                                    <a class="fw-bold"
                                       href="{{ route('coin.view_coin_subtype', ['coin' => $coin->id, 'sub_type' => urlencode(urlencode($sub_type->sub_type))]) }}">{{$sub_type->sub_type}}</a>
                                    @if(!$loop->last)
                                        ,
                                    @endif
                            @endforeach</p>
                    </div>
                @endif
                @if($coin->obv !== 'None')
                    <div class="col-sm-6">
                        <h5>Obverse Designs</h5>
                        <p><a href="{{ route('coin.view_type_design_detail', [
                                                    'coinType' => $coin->type->id,
                                                    'detail' => 'obverse',
                                                    'variety' => urlencode(urlencode($coin->obv)),
                                                    ]) }}">{{$coin->obv}}</a>

                            @if($coin->obv2 !== 'None')
                                ,
                                <a href="{{ route('coin.view_type_design_detail', [
                                                    'coinType' => $coin->type->id,
                                                    'detail' => 'obverse',
                                                    'variety' => urlencode(urlencode($coin->obv2)),
                                                    ]) }}">{{$coin->obv2}}</a>
                            @endif @if($coin->obv3 !== 'None')
                                , <a href="{{ route('coin.view_type_design_detail', [
                                                    'coinType' => $coin->type->id,
                                                    'detail' => 'obverse',
                                                    'variety' => urlencode(urlencode($coin->obv3)),
                                                    ]) }}">{{$coin->obv3}}</a>
                            @endif</p>
                    </div>
                @endif
                @if($coin->rev !== 'None')
                    <div class="col-sm-6">
                        <h5>Reverse Designs</h5>
                        <p><a href="{{ route('coin.view_type_design_detail', [
                                                    'coinType' => $coin->type->id,
                                                    'detail' => 'reverse',
                                                    'variety' => urlencode(urlencode($coin->rev)),
                                                    ]) }}">{{$coin->rev}}</a> @if($coin->rev2 !== 'None')
                                , <a href="{{ route('coin.view_type_design_detail', [
                                                    'coinType' => $coin->type->id,
                                                    'detail' => 'reverse',
                                                    'variety' => urlencode(urlencode($coin->rev2)),
                                                    ]) }}">{{$coin->rev2}}</a>
                            @endif @if($coin->rev3 !== 'None')
                                , <a href="{{ route('coin.view_type_design_detail', [
                                                    'coinType' => $coin->type->id,
                                                    'detail' => 'reverse',
                                                    'variety' => urlencode(urlencode($coin->rev3)),
                                                    ]) }}">{{$coin->rev3}}</a>
                            @endif</p>
                    </div>
                @endif
                @if($coin->mms !== 'None')
                    <div class="col-sm-6">
                        <h5>Mintmark Styles</h5>
                        <p>
                            <a href="{{ route('coin.view_type_mms', [
                                                    'coinType' => $coin->type->id,
                                                    'mint' => str_replace(' ', '_', $coin->mint),
                                                    'mms' => $coin->mms,
                                                    ]) }}">{{$coin->mms}}</a>
                            @if($coin->mms2 !== 'None')
                                , <a href="{{ route('coin.view_type_mms', [
                                                    'coinType' => $coin->type->id,
                                                    'mint' => str_replace(' ', '_', $coin->mint),
                                                    'mms' => $coin->mms2,
                                                    ]) }}">{{$coin->mms2}}</a>
                            @endif @if($coin->mms3 !== 'None')
                                , <a href="{{ route('coin.view_type_mms', [
                                                    'coinType' => $coin->type->id,
                                                    'mint' => str_replace(' ', '_', $coin->mint),
                                                    'mms' => $coin->mms3,
                                                    ]) }}">{{$coin->mms3}}</a>
                            @endif</p>
                    </div>
                @endif
            </div>
            @if($coin->commemorativeVersion !== 'None')
                <div class="row">
                    <div class="col-sm-6">
                        <h5>Version</h5>

                        @if($coin->commemorativeType == 'American Eagle')
                            <a href="{{ route('coin.view_eagle_index') }}">American Eagle</a>
                        @else
                            <a href="{{ route('coin.coin_genre', ['genre' => str_replace(' ', '_', $coin->commemorativeVersion)]) }}">
                                {{$coin->commemorativeVersion}}</a>
                        @endif

                    </div>
                    @if($coin->commemorativeCategory !== 'None')
                        <div class="col-sm-6">
                            <h5>Category</h5>
                            <a href="{{ route('coin.coin_genre', ['genre' => str_replace(' ', '_', $coin->commemorativeCategory)]) }}">
                                {{$coin->commemorativeCategory}}</a>
                        </div>
                    @endif
                    @if($coin->commemorativeType !== 'None')
                        <div class="col-sm-6">
                            <h5>Type</h5>
                            @if($coin->commemorativeType == 'American Eagle')
                                <a href="{{ route('coin.view_eagle_index') }}">American Eagle</a>
                            @else
                                <a href="{{ route('coin.view_coin_commem_type', ['type' => str_replace(' ', '_', $coin->commemorativeType)]) }}">
                                    {{$coin->commemorativeType}}</a>
                            @endif

                        </div>
                    @endif
                    @if($coin->commemorativeGroup !== 'None')
                        <div class="col-sm-6">
                            <h5>Group</h5>
                            <a href="{{ route('coin.coin_genre', ['genre' => str_replace(' ', '_', $coin->commemorativeGroup)]) }}">
                                {{$coin->commemorativeGroup}}</a>
                        </div>
                    @endif
                </div>
            @endif
            <div class="mt-3 mb-2">
                @if($coin->genre !== 'None')
                    <button id="genreCollapseBtn" class="btn btn-primary" type="button" data-bs-toggle="collapse"
                            data-bs-target="#genreCollapse" aria-expanded="false" aria-controls="collapseExample">
                        Genres
                    </button>
                @endif
                <button id="seriesCollapseBtn" class="btn btn-primary" type="button" data-bs-toggle="collapse"
                        data-bs-target="#seriesCollapse" aria-expanded="false" aria-controls="seriesCollapse">
                    Series
                </button>

                <button id="varietyCollapseBtn" class="btn btn-primary" type="button" data-bs-toggle="collapse"
                        data-bs-target="#varietyCollapse" aria-expanded="false" aria-controls="varietyCollapse">
                    Known Varieties
                </button>
            </div>
            <div class="collapse" id="varietyCollapse">
                <div class="card card-body">
                    <table class="table">
                        @foreach($varieties_list as $variety)
                            <tr>
                                <td>
                                    <a class="dropdown-item"
                                       href="{{ route('coin.view_variety_list', ['coin_id' => $coin->id, 'variety' => str_replace(' ', '_', $variety)]) }}">{{ $variety }}</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="collapse" id="genreCollapse">
                <div class="card card-body">
                    <table class="table">
                        <tr>
                            @if($coin->genre !== 'None')
                                <td><span class="fw-bold">{{$coin->genre}}: </span> <a
                                        href="{{ route('coin.coin_genre', ['genre' => str_replace(' ', '_', $coin->genre)]) }}">{{$coin->genre}}</a>
                                </td>
                            @endif
                            @if($coin->subGenre !== 'None')
                                <td><span class="fw-bold">{{$coin->subGenre}}: </span> <a
                                        href="{{ route('coin.coin_genre', ['genre' => str_replace(' ', '_', $coin->subGenre)]) }}">{{$coin->subGenre}}</a>
                                </td>
                            @endif
                            @if($coin->subGenre2 !== 'None')
                                <td><span class="fw-bold">{{$coin->subGenre2}}: </span> <a
                                        href="{{ route('coin.coin_genre', ['genre' => str_replace(' ', '_', $coin->subGenre2)]) }}">{{$coin->subGenre2}}</a>
                                </td>
                            @endif
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Varieties</h4>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                View
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item"
                                       href="{{ route('coin.view_variety_for_coin', ['coin' => $coin->id]) }}">See
                                        All</a></li>
                                @foreach($varieties_list as $variety)
                                    <li><a class="dropdown-item"
                                           href="{{ route('coin.view_variety_list', ['coin_id' => $coin->id, 'variety' => str_replace(' ', '_', $variety)]) }}">{{ $variety }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Errors</h4>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Do Something
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('member.view_books') }}">See All</a></li>
                                @if(optional(auth()->user())->status !== 'warn')
                                    <li><a class="dropdown-item" href="{{ route('member.create_posts') }}">Create (Found A
                                            new error)</a></li>
                                @endif

                            </ul>
                        </div>
                    </div>
                </div>
                @if(optional(auth()->user())->status !== 'warn')
                    <div class="col-4">
                        <div class="callout callout-primary">
                            <h4>Discussion Area</h4>
                            <a class="btn btn-secondary" href="{{ route('member.forum_coin', ['id' => $coin->id]) }}">Go→</a>
                        </div>
                    </div>
                @endif
            </div>

            <hr/>

            @if(!empty($mintsets[0]))
                <h5>In Mintsets</h5>
                <table class="table">
                    @foreach ($mintsets as $mintset)
                        <tr>
                            <td>
                                <a href="{{ route('coin.set_view', ['mintset' => $mintset->id]) }}">{{$mintset->setName}}</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
                <hr/>
            @endif
            @if(isset($folders[0]))
                <h5>In Folder</h5>
                <table class="table">
                    @foreach ($folders as $folder)
                        <tr>
                            <td>
                                <a href="{{ route('coin.set_view', ['mintset' => $folder->id]) }}">{{$folder->folderName}}
                                    #{{$folder->folderCode}}</a></td>
                        </tr>
                    @endforeach
                </table>
                <hr/>
            @endif

            @if(in_array($coin->id, [7648,3887,3888]))
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">By Composition</h5>
                        <div class="card card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <td><span class="fw-bold">1959-1982:</span> 95% Copper - 5% Tin and Zinc</td>
                                        <td><span class="fw-bold">1982-2008:</span> 97.5% Zinc - 2.5% Copper</td>
                                    </tr>
                                    <tr>
                                        <td><span class="fw-bold">Saved:</span> <span id="copper_span"></span></td>
                                        <td><span class="fw-bold">Saved:</span> <span id="zinc_span"></span></td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endif


            <!-- Featured blog post-->
            <h4 class="mt-5 mb-3">Saved ({{$count}})</h4>
            <!-- Nested row for non-featured blog posts-->
            <div class="row">
                <div class="col-lg-12">

                    <table class="table">
                        <tr>
                            <td><span class="fw-bold">Saved: </span> ({{$count}})</td>
                            <td><span class="fw-bold">Invested: </span> ({{$cost}})</td>
                            <td><span class="fw-bold">Face Val: </span> ({{$count}})</td>
                        </tr>
                    </table>
                    <div class="table-responsive">
                        <table class="table table-hover datatable">
                            <thead>
                            <tr>
                                <th scope="col">Nickname</th>
                                <th scope="col">Grade/TPG</th>
                                <th scope="col">Created</th>
                                <th scope="col">Purchase</th>
                                <th scope="col">In</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($collected as $collect)
                                <tr>
                                    <td>
                                        <a href="{{ route('collected.view_collected', ['collected' => $collect->id]) }}">{{ Str::limit($collect->nickname, 55) ?? 'None' }}</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('collected.view_collected', ['collected' => $collect->id]) }}">{{ Str::limit($collect->grade, 55) ?? 'None' }}
                                            /{{$collect->tpg_service ?? 'None'}}</a>
                                    </td>
                                    <td>{{ Carbon\Carbon::parse($collect->created_at)->format('M jS Y') }}</td>

                                    <td>
                                        {{$collect->cost}}
                                    </td>
                                    <td>
                                        <a href="{{ route('collected.view_collected', ['collected' => $collect->id]) }}">{{$collect->tpg_service ?? 'None'}}</a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td>No {{ $coin->coinName }} {{ $coin->type->coinType }} coins saved</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>

                </div>

                <div class="col-lg-12 mb-3">
                    <a class="btn btn-secondary" href="{{ route('coin.coin_index') }}">Coins Home</a>
                </div>
            </div>
        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">{{ $coin->coinName }}</div>
                <div class="card-body">

                    <div>
                        <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                Add New
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item"
                                       href="{{ route('collected.save_by_coin_id', ['id' => $coin->id]) }}">Single
                                        Coin</a>
                                </li>
                                <li><a class="dropdown-item"
                                       href="{{ route('collected.save_multi_by_coin_id', ['id' => $coin->id, 'amount' => 10]) }}">Multiple
                                        Coins</a>
                                </li>
                                <li><a class="dropdown-item"
                                       href="{{ route('collected.save_by_coin_id', ['id' => $coin->id]) }}">Roll(s)</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="mt-3">
                        <form class="row row-cols-lg-auto g-3 align-items-center" id="add_bulk_coin_form"
                              action="{{ route('collected.quick_save_coin_id') }}">
                            @csrf
                            <input type="hidden" name="coin_id" value="{{ $coin->id }}">
                            <div class="col-12">
                                <label class="visually-hidden" for="inlineFormSelectPref">Preference</label>
                                <select name="coin_amount" class="form-select" id="inlineFormSelectPref" required>
                                    <option selected>Choose...</option>
                                    <option value="1">1</option>
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="{{$coin->type->rollCount}}">1 Roll</option>
                                    <option value="{{$coin->type->bagCount}}">1 Bag</option>
                                </select>
                            </div>

                            <div class="col-12">
                                <button id="add_bulk_coin_form_btn" type="submit" class="btn btn-primary">Quick Add
                                </button>
                            </div>
                        </form>
                    </div>


                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Reports</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                @if($coin->keyDate !== 0 || $coin->semiKeyDate !== 0)
                                    <li>
                                        <a href="{{ route('coin.coin_key_dates') }}">Key Date Report</a>
                                    </li>
                                @endif
                                <li>
                                    <a href="{{ route('coin.year_view', ['year' => $coin->coinYear]) }}">All {{ $coin->coinYear }}</a>
                                </li>
                                @if($coin->design == 'Seated Liberty')
                                    <li><a href="{{ route('coin.seated_index') }}">Seated Liberty</a></li>

                                @endif

                                @if($coin->series == "Lincoln Cents")
                                    <li><a href="{{ route('coin.lincoln_index') }}">All Lincoln Cents</a></li>
                                @endif

                                @if($coin->series == "Lincoln Cents")
                                    <li>
                                        <a href="{{ route('coin.lincoln_index_year_view', ['year' => $coin->coinYear]) }}">All {{$coin->coinYear}}
                                            Lincoln Cents</a></li>
                                @endif

                                @if($color_cat == 1)
                                    <li><a href="{{ route('coin.view_type', ['id' => $coin->type->id]) }}">Color
                                            Report</a></li>
                                @endif
                                @if($coin->type->coinType == 'Lincoheat')
                                    <li><a href="{{ route('coin.view_type', ['id' => $coin->type->id]) }}">Coloport</a>
                                    </li>
                                @endif
                                @if($coin->type->coinType == 'Lincoheat')
                                    <li>
                                        <a href="{{ route('coin.view_type', ['id' => $coin->type->id]) }}">Coloport</a>
                                    </li>
                                @endif
                                @if($coin->type->coinType == 'Lincoheat')
                                    <li>
                                        <a href="{{ route('coin.view_type', ['id' => $coin->type->id]) }}">Coloport</a>
                                    </li>
                                @endif
                                @if($coin->type->coinType == 'Lincoheat')
                                    <li>
                                        <a href="{{ route('coin.view_type', ['id' => $coin->type->id]) }}">Coloport</a>
                                    </li>
                                @endif
                                @if($coin->type->coinType == 'Lincoheat')
                                    <li>
                                        <a href="{{ route('coin.view_type', ['id' => $coin->type->id]) }}">Coloport</a>
                                    </li>
                                @endif
                                <li><a href="{{ route('coin.coin_id_grades', ['coin' => $coin->id]) }}">Grade Report</a>
                                </li>
                                @if(in_array($coin->strike ,Config::get('constants.coins.proof_strikes')) || in_array($coin->strike ,Config::get('constants.coins.special_strikes')))
                                    <li>
                                        <a href="{{ route('coin.view_type_strike', ['coinType' => $coin->type->id, 'strike' => str_replace(' ', '_', $coin->strike)]) }}">All {{$coin->strike}}
                                            Strikes</a></li>
                                @endif


                            </ul>
                        </div>
                        <div class="col-sm-6">

                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <ul class="list-unstyled mb-0">
                        <li><a class="external_link" href="http://www.cuds-on-coins.com/">Cuds-On-Coins</a></li>
                        <li><a class="external_link" href="http://www.traildies.com/">Trail Dies</a></li>
                        <li><a class="external_link"
                               href="http://ec2-13-58-222-16.us-east-2.compute.amazonaws.com/wiki/Home">VAMWorld</a>
                        </li>
                    </ul>
                </div>
            </div>
            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">

                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.view_directory') }}">User Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            @endif
        </div>
    </div>
    @push('scripts')
        <script>
            (function ($) {
                try {
                    console.log('ready');
                    $('.dropdown-menu li a').click(function () {
                        console.log('tg5res4wgwad');
                        $('button[data-toggle="dropdown"]').text('Loading...');
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }


            }(window.jQuery));

            (function (ENVIRONMENT) {
                try {


                    let coin_date_century = document.getElementById("coin_date_century");
                    let coin_date_year = document.getElementById("coin_date_year");
                    document.getElementById("coin_date_btn").addEventListener("click", function () {
                        let date_century = coin_date_century.options[coin_date_century.selectedIndex].value;
                        let date_year = coin_date_year.options[coin_date_year.selectedIndex].value;
                        let new_date_year = date_century + date_year;
                        let url = "{{ route('coin.year_view', [':year']) }}".replace(':year', new_date_year);
                        //console.log(url);
                        window.location.href = url;
                    });

                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection

