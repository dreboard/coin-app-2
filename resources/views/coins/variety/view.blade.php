@extends('layouts.user.main')
@section('pageTitle', "Variety $variety->variety")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">{{$variety->type}} {{ $variety->label }} {{ $variety->variety }}</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    View
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">All {{ $coin->coinYear }} {{ $coin->type->coinType }}</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_type', ['id' => $coin->type->id]) }}">{{ $coin->type->coinType }}</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_category', ['id' => $coin->category->id]) }}">{{ $coin->category->coinCategory }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4 mt-2">
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">All</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.view_type', ['id' => $coin->type->id]) }}">{{ $coin->type->coinType }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.view_category', ['id' => $coin->category->id]) }}">{{ $coin->category->coinCategory }}</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                {{ $variety->variety }} for {{ $coin->coinName }} {{ $coin->type->coinType }}
            </div>


            <table class="table">
                <tr>
                    <td class="fw-bold w-25">
                        Coin:
                    </td>
                    <td><a href="{{ route('coin.view_coin', ['coin' => $coin->id]) }}"> {{ $coin->coinName }} {{ $coin->type->coinType }}</a></td>
                </tr>
                <tr>
                    <td class="fw-bold">
                        Type:
                    </td>
                    <td><a href="{{ route('coin.view_coin', ['coin' => $coin->id]) }}"> {{ $variety->variety }}</a></td>
                </tr>
                <tr>
                    <td class="fw-bold">Reference:</td>
                    <td>{{$variety->type}}</td>
                </tr>
                <tr>
                    <td class="fw-bold">Designation:</td>
                    <td>{{$variety->label}}/{{$variety->designation}}</td>
                </tr>
                <tr>
                    <td class="fw-bold">Obverse:</td>
                    <td>{{$variety->obv}}</td>
                </tr>
                <tr>
                    <td class="fw-bold">Reverse:</td>
                    <td>{{$variety->rev}}</td>
                </tr>
            </table>

            <!-- Featured blog post-->
            <h4 class="mt-5 mb-3">Saved</h4>
            <!-- Nested row for non-featured blog posts-->
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-hover datatable">
                        <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Created</th>
                            <th scope="col">Purchase</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($collected as $collect)
                            <tr>
                                <td>
                                    <a href="{{ route('collected.view_collected', ['collected' => $collect->id]) }}">{{ Str::limit($collect->grade, 55) }}</a>
                                </td>
                                <td>{{ Carbon\Carbon::parse($collect->created_at)->format('M jS Y') }}</td>
                                <td>
                                    <a href="{{ route('collected.view_collected', ['collected' => $collect->id]) }}">{{optional($collect)->nickName ?? 'No Name'}}</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td></td>
                                <td>No {{ $coin->coinName }} saved</td>
                                <td></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>

                <div class="col-lg-12">
                    <a class="btn btn-secondary" href="#!">See All Projects</a>
                </div>
            </div>
        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">{{ $coin->coinName }}</div>
                <div class="card-body">

                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                data-bs-toggle="dropdown" aria-expanded="false">
                            Add New
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.save_by_coin_id', ['id' => $coin->id]) }}">Single Coin</a>
                            </li>
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.save_by_coin_id', ['id' => $coin->id]) }}">Multiple Coins</a>
                            </li>
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.save_by_coin_id', ['id' => $coin->id]) }}">Roll(s)</a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="https://www.error-ref.com/">Error-Variety Ready
                                        Reference</a></li>
                                <li><a class="external_link" href="https://minterrornews.com/">Mint Error News</a></li>
                                <li><a class="external_link" href="https://conecaonline.org/">CONECA</a></li>
                                <li><a class="external_link" href="http://www.doubleddie.com/1801.html">Wexler’s Die
                                        Varieties</a></li>
                                <li><a class="external_link" href="http://varietyvista.com/Attribution%20Services.htm">Variety
                                        Vista Attribution Services</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="http://www.maddieclashes.com/">MAD Die Clashes</a>
                                </li>
                                <li><a class="external_link" href="http://www.cuds-on-coins.com/">Cuds-On-Coins</a></li>
                                <li><a class="external_link" href="http://www.traildies.com/">Trail Dies</a></li>
                                <li><a class="external_link"
                                       href="http://ec2-13-58-222-16.us-east-2.compute.amazonaws.com/wiki/Home">VAMWorld</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Projects</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('user.user_types', ['type' => 'Research']) }}">Scholar
                                        Directory</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

