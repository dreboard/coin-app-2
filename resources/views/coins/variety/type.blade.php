@extends('layouts.user.main')
@section('pageTitle', 'View Coin')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">{{ $coin->type->coinType }}</h3>
            <p>Varieties</p>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    View
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">All {{ $coin->coinYear }} {{ $coin->type->coinType }}</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_type', ['id' => $coin->type->id]) }}">{{ $coin->type->coinType }}</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_category', ['id' => $coin->category->id]) }}">{{ $coin->category->coinCategory }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">All</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.view_type', ['id' => $coin->type->id]) }}">{{ $coin->type->coinType }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.view_category', ['id' => $coin->category->id]) }}">{{ $coin->category->coinCategory }}</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                This section is for collectors to document newly discovered coin varieties or errors on U.S. Coins.
            </div>

            <div class="row">
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Varieties</h4>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Do Something
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('member.posts_index') }}">See All</a></li>
                                <li><a class="dropdown-item" href="{{ route('member.create_posts') }}">Create (Found A
                                        new variety)</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Errors</h4>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Do Something
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('member.view_books') }}">See All</a></li>
                                <li><a class="dropdown-item" href="{{ route('member.create_posts') }}">Create (Found A
                                        new error)</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Discussion Area</h4>
                        <a class="btn btn-secondary" href="{{ route('member.forum') }}">Go →</a>
                    </div>
                </div>
            </div>


            <hr/>
            <!-- Featured blog post-->
            <h4 class="mt-5 mb-3">Saved</h4>
            <!-- Nested row for non-featured blog posts-->
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-hover datatable">
                        <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Created</th>
                            <th scope="col">Purchase</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($collected as $project)
                            <tr>
                                <td>
                                    <a href="{{ route('member.project_public_view', ['project' => $project->id]) }}">{{ Str::limit($project->title, 55) }}</a>
                                </td>
                                <td>{{ Carbon\Carbon::parse($project->created_at)->format('M jS Y') }}</td>
                                <td>
                                    <a href="{{ route('user.public', ['id' => $project->user->id]) }}">{{$project->user->name}}</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td></td>
                                <td>No {{ $coin->coinName }} saved</td>
                                <td></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>

                <div class="col-lg-12">
                    <a class="btn btn-secondary" href="#!">See All Projects</a>
                </div>
            </div>
        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">{{ $coin->coinName }}</div>
                <div class="card-body">

                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                data-bs-toggle="dropdown" aria-expanded="false">
                            Add New
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.save_by_coin_id', ['id' => $coin->id]) }}">Single Coin</a>
                            </li>
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.save_by_coin_id', ['id' => $coin->id]) }}">Multiple Coins</a>
                            </li>
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.save_by_coin_id', ['id' => $coin->id]) }}">Roll(s)</a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="https://www.error-ref.com/">Error-Variety Ready
                                        Reference</a></li>
                                <li><a class="external_link" href="https://minterrornews.com/">Mint Error News</a></li>
                                <li><a class="external_link" href="https://conecaonline.org/">CONECA</a></li>
                                <li><a class="external_link" href="http://www.doubleddie.com/1801.html">Wexler’s Die
                                        Varieties</a></li>
                                <li><a class="external_link" href="http://varietyvista.com/Attribution%20Services.htm">Variety
                                        Vista Attribution Services</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="http://www.maddieclashes.com/">MAD Die Clashes</a>
                                </li>
                                <li><a class="external_link" href="http://www.cuds-on-coins.com/">Cuds-On-Coins</a></li>
                                <li><a class="external_link" href="http://www.traildies.com/">Trail Dies</a></li>
                                <li><a class="external_link"
                                       href="http://ec2-13-58-222-16.us-east-2.compute.amazonaws.com/wiki/Home">VAMWorld</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Projects</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('user.user_types', ['type' => 'Research']) }}">Scholar
                                        Directory</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

