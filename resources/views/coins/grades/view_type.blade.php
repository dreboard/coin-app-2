@extends('layouts.user.main')
@section('pageTitle', "View Type - $coinType->coinType Grades")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">{{ $coinType->coinType }} Grade Sheet</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Reports
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">

                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Coin</a>
                    </li>

                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_category', ['id' => $coinType->coincats_id]) }}">{{ $coinType->coinCategory }}</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_category', ['id' => $coinType->id]) }}">{{ $coinType->coinType }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.view_category', ['id' => $coinType->coincats_id]) }}">{{ $coinType->coinCategory }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">{{ $coinType->coinType }}</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                Grade Sheet for all collected {{ $coinType->coinType }} coins.
            </div>


            <div class="row">
                <div class="col-lg-6">
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-chart-pie me-1"></i>
                            Raw vs Slabbed
                        </div>
                        <div class="card-body"><canvas id="thirdPartyChart" width="100%" height="50"></canvas></div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-chart-pie me-1"></i>
                            Coins Graded
                        </div>
                        <div class="card-body"><canvas id="myPieChart" width="100%" height="50"></canvas></div>
                    </div>
                </div>
            </div>


            <table id="strike_totals_tbl" class="table">
                <tr class="gradeCell" id="business_strike_row">
                    <td><span class="fw-bold">(MS) Business: </span><span id="business_total">Loading.....</span></td>
                    <td>Graded: <span id="business_graded">Loading.....</span></td>
                    <td>Ungraded: <span id="business_ungraded">Loading.....</span></td>
                    <td>Certified: <span id="business_certified">Loading.....</span></td>
                    <td><a href="{{ route('coin.coin_type_strike_grade', ['strike' => 'business','coinType' => $coinType->id]) }}" class="btn btn-secondary btn-sm">All MS</a> </td>
                </tr>
                <tr class="gradeCell" id="proof_strike_row">
                    <td><span class="fw-bold">(PR) Proof: </span><span id="proof_total">Loading.....</span></td>
                    <td>Graded: <span id="proof_graded">Loading.....</span></td>
                    <td>Ungraded: <span id="proof_ungraded">Loading.....</span></td>
                    <td>Certified: <span id="proof_certified">Loading.....</span></td>
                    <td><a href="{{ route('coin.coin_type_strike_grade', ['strike' => 'proof','coinType' => $coinType->id]) }}" class="btn btn-secondary btn-sm">All PR</a> </td>
                </tr>
                <tr class="gradeCell" id="special_strike_row">
                    <td><span class="fw-bold">(SP) Special: </span><span id="special_total">Loading.....</span></td>
                    <td>Graded: <span id="special_graded">Loading.....</span></td>
                    <td>Ungraded: <span id="special_ungraded">Loading.....</span></td>
                    <td>Certified: <span id="special_certified">Loading.....</span></td>
                    <td><a href="{{ route('coin.coin_type_strike_grade', ['strike' => 'special','coinType' => $coinType->id]) }}" class="btn btn-secondary btn-sm">All SP</a> </td>
                </tr>
            </table>

            <div class="table-responsive mt-5">
                <table class="table" id="strike_grades_tbl">
                    {!! $gradeTable !!}
                </table>
            </div>




        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Grades By Coin</div>
                <div class="card-body">

                    <div class="input-group">
                        <select class="form-select" id="coin_id_select"
                                aria-label="Example select with button addon">
                            <option selected>Choose...</option>
                            @foreach($coinType->coins as $coin)
                                <option value="{{ $coin->id }}">{{ $coin->coinName }}</option>
                            @endforeach
                        </select>
                        <button id="coin_id_select_btn" class="btn btn-outline-primary" type="button">Load</button>
                    </div>

                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="https://www.error-ref.com/">Error-Variety Ready
                                        Reference</a></li>
                                <li><a class="external_link" href="https://minterrornews.com/">Mint Error News</a></li>
                                <li><a class="external_link" href="https://conecaonline.org/">CONECA</a></li>
                                <li><a class="external_link" href="http://www.doubleddie.com/1801.html">Wexler’s Die
                                        Varieties</a></li>
                                <li><a class="external_link" href="http://varietyvista.com/Attribution%20Services.htm">Variety
                                        Vista Attribution Services</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="http://www.maddieclashes.com/">MAD Die Clashes</a>
                                </li>
                                <li><a class="external_link" href="http://www.cuds-on-coins.com/">Cuds-On-Coins</a></li>
                                <li><a class="external_link" href="http://www.traildies.com/">Trail Dies</a></li>
                                <li><a class="external_link"
                                       href="http://ec2-13-58-222-16.us-east-2.compute.amazonaws.com/wiki/Home">VAMWorld</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Projects</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('user.user_types', ['type' => 'Research']) }}">Scholar
                                        Directory</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @push('scripts')
       <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>

        <script>

            (function (ENVIRONMENT) {
                try {

                    // Set new default font family and font color to mimic Bootstrap's default styling
                    Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
                    Chart.defaults.global.defaultFontColor = '#292b2c';

                    const coin_type_id = {{$coinType->id}};

                    const coin_id_select = document.getElementById("coin_id_select");
                    const coin_id_select_btn = document.getElementById("coin_id_select_btn");

                    coin_id_select_btn.addEventListener("click", function () {
                        let coin_id = coin_id_select.options[coin_id_select.selectedIndex].value;
                        let url = "{{ route('coin.coin_id_grades', [':coin']) }}".replace(':coin', coin_id);
                        console.log(url);
                        window.location.href = url;
                    });

                    window.addEventListener('load', (event) => {
                        let url = "{{ route('coin.coin_grade_details', [':coin_type_id']) }}".replace(':coin_type_id', coin_type_id);
                        fetch(url, {
                            "method": "GET",
                        }).then(
                            response => {
                                response.json().then(
                                    data => {
                                        console.log(data);
                                        if (ENVIRONMENT === 'local') {

                                        }

                                        var ctx = document.getElementById("myPieChart");
                                        var myPieChart = new Chart(ctx, {
                                            type: 'pie',
                                            data: {
                                                labels: ["Ungraded", "Graded"],
                                                datasets: [{
                                                    data: [data.coins.ungraded, data.coins.graded],
                                                    backgroundColor: ['#444444','#007bff'],
                                                }],
                                            },
                                        });

                                        var ctx2 = document.getElementById("thirdPartyChart");
                                        var thirdPartyChart = new Chart(ctx2, {
                                            type: 'pie',
                                            data: {
                                                labels: ["Raw "+data.coins.raw, "Slabbed "+data.coins.slabbed],
                                                datasets: [{
                                                    data: [data.coins.raw, data.coins.slabbed],
                                                    backgroundColor: ['#444444', '#007bff'],
                                                }],
                                            },
                                        });

                                        document.getElementById('business_total').innerHTML = data.coins.business.count;
                                        document.getElementById('business_graded').innerHTML = data.coins.business.graded;
                                        document.getElementById('business_ungraded').innerHTML = data.coins.business.ungraded;
                                        document.getElementById('business_certified').innerHTML = data.coins.business.pro_graded;
                                        document.getElementById('proof_total').innerHTML = data.coins.proof.count;
                                        document.getElementById('proof_graded').innerHTML = data.coins.proof.graded;
                                        document.getElementById('proof_ungraded').innerHTML = data.coins.proof.ungraded;
                                        document.getElementById('proof_certified').innerHTML = data.coins.proof.pro_graded;
                                        document.getElementById('special_total').innerHTML = data.coins.special.count;
                                        document.getElementById('special_graded').innerHTML = data.coins.special.graded;
                                        document.getElementById('special_ungraded').innerHTML = data.coins.special.ungraded;
                                        document.getElementById('special_certified').innerHTML = data.coins.special.pro_graded;


                                    }
                                )
                            })
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection

