@extends('layouts.user.main')
@section('pageTitle', "View Type - $coinType->coinType Graded $grade")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">{{ $coinType->coinType }} Graded {{Config::get('constants.coins.ms_grade_prefix')[$grade]}}/{{Config::get('constants.coins.pr_grade_prefix')[$grade]}} </h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    View
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Coin</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_category', ['id' => $coinType->coincats_id]) }}">{{ $coinType->coinCategory }}</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_category', ['id' => $coinType->coincats_id]) }}">{{ $coinType->coinType }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.view_category', ['id' => $coinType->coincats_id]) }}">{{ $coinType->coinCategory }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">{{ $coinType->coinType }}</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                All {{ $coinType->coinType }} coins graded PR/MS{{ $grade }}, see <a href="{{ route('coin.type_grades', ['coinType' => $coinType->id]) }}">Full Grade Report</a>
            </div>

            <div class="table-responsive mt-5">
                <table class="table table-hover datatable">
                    <thead class="alert alert-primary">
                    <tr>
                        <th scope="col">Grade</th>
                        <th scope="col">3rd Party</th>
                        <th scope="col">Created</th>
                        <th scope="col">Purchase</th>
                        <th scope="col">In</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($collected as $collect)
                        <tr>
                            <td><a href="{{ route('collected.view_collected', ['collected' => $collect->id]) }}">{{$collect->coin->coinName ?? 'None'}}</a></td>
                            <td>
                                @if(in_array($collect->coin->strike, Config::get('constants.coins.business_strikes'))) {{Config::get('constants.coins.ms_grade_prefix')[$collect->grade]}}
                                @elseif(in_array($collect->coin->strike, Config::get('constants.coins.proof_strikes'))) {{Config::get('constants.coins.pr_grade_prefix')[$collect->grade]}} @endif
                                   / {{$collect->tpg_service ?? 'None'}}
                            </td>
                            <td>{{ Carbon\Carbon::parse($collect->created_at)->format('M jS Y') }}</td>

                            <td>
                                {{ number_format($collect->cost, 2)}}
                            </td>
                            <td><a href="{{ route('collected.view_collected', ['collected' => $collect->id]) }}">{{$collect->tpg_service ?? 'None'}}</a></td>
                        </tr>
                    @empty
                        <tr>
                            <td></td>
                            <td>No {{ $coinType->coinType }} graded {{ $grade }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>




        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-body">
                    <div class="input-group">
                        <a class="btn btn-secondary btn-sm" href="{{ route('coin.type_grades', ['coinType' => $coinType->id]) }}">Full Grade Report</a>
                    </div>
                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="https://www.error-ref.com/">Error-Variety Ready
                                        Reference</a></li>
                                <li><a class="external_link" href="https://minterrornews.com/">Mint Error News</a></li>
                                <li><a class="external_link" href="https://conecaonline.org/">CONECA</a></li>
                                <li><a class="external_link" href="http://www.doubleddie.com/1801.html">Wexler’s Die
                                        Varieties</a></li>
                                <li><a class="external_link" href="http://varietyvista.com/Attribution%20Services.htm">Variety
                                        Vista Attribution Services</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="http://www.maddieclashes.com/">MAD Die Clashes</a>
                                </li>
                                <li><a class="external_link" href="http://www.cuds-on-coins.com/">Cuds-On-Coins</a></li>
                                <li><a class="external_link" href="http://www.traildies.com/">Trail Dies</a></li>
                                <li><a class="external_link"
                                       href="http://ec2-13-58-222-16.us-east-2.compute.amazonaws.com/wiki/Home">VAMWorld</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Projects</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('user.user_types', ['type' => 'Research']) }}">Scholar
                                        Directory</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

