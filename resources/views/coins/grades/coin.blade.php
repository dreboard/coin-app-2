@extends('layouts.user.main')
@section('pageTitle', "View - $coin->coinName Grades")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">{{ $coin->coinName }} Grade Sheet</h3>
            <small class="text-muted h5"><span class="fw-bold">For:</span>
                <a href="{{ route('coin.view_coin', ['coin' => $coin->id]) }}"> {{ $coin->coinName }} {{ $coin->type->coinType }}</a>
            </small>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    View
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_coin', ['coin' =>$coin->id]) }}">Coin</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_category', ['id' =>$coin->category->id]) }}">{{ $coin->category->coinCategory }}</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_category', ['id' => $coin->type->id]) }}">{{ $coin->type->coinType }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4 mt-2">
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">All Coins</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('coin.view_category', ['id' => $coin->category->id]) }}">{{ $coin->category->coinCategory }}</a>
        </li>
        <li class="breadcrumb-item"><a
                href="{{ route('coin.view_type', ['id' => $coin->type->id]) }}">{{ $coin->type->coinType }}</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                {{ $coin->coinName }} Grade Sheet. Go to <a
                    href="{{ route('coin.type_grades', ['coinType' => $coin->type->id]) }}">Full Grade Report</a>

            </div>


            <div class="card mb-3">
                <div class="card-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <h5>Finest</h5>
                            <table id="strike_finest_tbl" class="table">
                                <tr class="gradeCell p-2" id="slabbed_finest_row">
                                    <td class="w-25"><span class="fw-bold">Slabbed: </span></td>
                                    <td><a href="" id="slabbed_finest_link"> <span id="slabbed_finest_grade">Loading.....</span></a>
                                    </td>
                                </tr>
                                <tr class="gradeCell p-2" id="raw_finest_row">
                                    <td><span class="fw-bold">Raw: </span></td>
                                    <td><a href="" id="raw_finest_link"> <span id="raw_finest_grade">Loading.....</span></a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <h5>Breakdown</h5>
                            <table id="strike_totals_tbl" class="table">
                                @if(in_array($coin->strike, Config::get('constants.coins.business_strikes')))
                                    <tr class="gradeCell" id="business_strike_row">
                                        <td><span class="fw-bold">Collected: </span><span id="business_total">Loading.....</span></td>
                                        <td>Ungraded: <span id="business_ungraded">Loading.....</span></td>
                                        <td>Slabbed: <span id="business_certified">Loading.....</span></td>
                                        <td>Total Graded: <span id="business_graded">Loading.....</span></td>
                                    </tr>
                                @endif
                                @if(in_array($coin->strike,Config::get('constants.coins.proof_strikes')))
                                    <tr class="gradeCell" id="proof_strike_row">
                                        <td><span class="fw-bold">Collected: </span><span id="proof_total">Loading.....</span></td>
                                        <td>Ungraded: <span id="proof_ungraded">Loading.....</span></td>
                                        <td>Slabbed: <span id="proof_certified">Loading.....</span></td>
                                        <td>Total Graded: <span id="proof_graded">Loading.....</span></td>
                                    </tr>
                                @endif
                                @if(in_array($coin->strike,Config::get('constants.coins.special_strikes')))
                                    <tr class="text-center" id="special_strike_row">
                                        <td class="text-center"><span class="fw-bold">Collected: </span><span id="special_total">Loading.....</span></td>
                                        <td class="text-center">Ungraded: <span id="special_ungraded">Loading.....</span></td>
                                        <td class="text-center">Slabbed: <span id="special_certified">Loading.....</span></td>
                                        <td class="text-center">Total Graded: <span id="special_graded">Loading.....</span></td>
                                    </tr>
                                @endif
                            </table>
                        </div>

                        <div class="col-sm-12">

                            <table id="strike_totals_tbl" class="table">
                                <tr class="gradeCell" id="business_strike_row">
                                    <td><span class="fw-bold">PCGS: </span><span id="span_pcgs">Loading.....</span></td>
                                    <td><span class="fw-bold">NGC: </span><span id="span_ngc">Loading.....</span></td>
                                    <td><span class="fw-bold">ANACS: </span><span id="span_anacs">Loading.....</span></td>
                                    <td><span class="fw-bold">IGC: </span><span id="span_igc">Loading.....</span></td>
                                </tr>
                            </table>
                        </div>

                        @if($coin->type->coinType == 'Standing Liberty')
                        <div class="col-sm-12">
                            <h5>Full Head Count</h5>
                            <table id="strike_totals_tbl" class="table">
                                <tr class="gradeCell" id="business_strike_row">
                                    <td><span class="fw-bold">Count: </span><span id="span_full_head">Loading.....</span>
                                        <button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                            Button with data-bs-target
                                        </button>

                                    </td>
                                </tr>
                            </table>
                            <div class="collapse" id="collapseExample">
                                <div class="card card-body">
                                    Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.


                                </div>
                            </div>
                        </div>
                        @endif


                    </div>


                </div>
            </div>


            <div class="row">
                <div class="col-lg-6">
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-chart-bar me-1"></i>
                            Raw vs Slabbed
                        </div>
                        <div class="card-body">
                            <canvas id="thirdPartyChart" width="100%" height="50"></canvas>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-chart-pie me-1"></i>
                            Coins Graded
                        </div>
                        <div class="card-body">
                            <canvas id="myPieChart" width="100%" height="50"></canvas>
                        </div>
                    </div>
                </div>
            </div>






            <div class="table-responsive mt-5">
                <table class="table" id="strike_grades_tbl">
                    {!! $gradeTable !!}
                </table>
            </div>

        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Grades By Coin</div>
                <div class="card-body">

                    <div class="input-group">
                        <select class="form-select" id="coin_id_select"
                                aria-label="Example select with button addon">
                            <option selected>Choose...</option>
                            @foreach($coin->type->coins as $cointypeCoin)
                                <option value="{{ $cointypeCoin->id }}">{{ $cointypeCoin->coinName }}</option>
                            @endforeach
                        </select>
                        <button id="coin_id_select_btn" class="btn btn-outline-primary" type="button">Load</button>
                    </div>

                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">{{ $coin->coinName }} Reports</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('coin.coin_id_grades', ['coin' => $coin->id]) }}">Grade Report</a>
                                </li>
                                <li>
                                    <a href="{{ route('coin.year_view', ['year' => $coin->coinYear]) }}">All {{ $coin->coinYear }}</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="http://www.maddieclashes.com/">MAD Die Clashes</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Projects</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('user.user_types', ['type' => 'Research']) }}">Scholar
                                        Directory</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @push('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>

        <script>

            (function (ENVIRONMENT) {
                try {

                    // Set new default font family and font color to mimic Bootstrap's default styling
                    Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
                    Chart.defaults.global.defaultFontColor = '#292b2c';

                    const coin_id = {{$coin->id}};

                    const coin_id_select = document.getElementById("coin_id_select");
                    const coin_id_select_btn = document.getElementById("coin_id_select_btn");

                    coin_id_select_btn.addEventListener("click", function () {
                        let coin_select_id = coin_id_select.options[coin_id_select.selectedIndex].value;
                        let url = "{{ route('coin.coin_id_grades', [':coin']) }}".replace(':coin', coin_select_id);
                        console.log(url);
                        window.location.href = url;
                    });
                    window.addEventListener('load', (event) => {
                        let url = "{{ route('coin.coin_id_grade_details', [':coin_id']) }}".replace(':coin_id', coin_id);
                        fetch(url, {
                            "method": "GET",
                        }).then(
                            response => {
                                response.json().then(
                                    data => {
                                        console.log(data);
                                        if (ENVIRONMENT === 'local') {

                                        }

                                        var ctx = document.getElementById("myPieChart");
                                        var myPieChart = new Chart(ctx, {
                                            type: 'pie',
                                            data: {
                                                labels: ["Graded " + data.coins.graded, "Ungraded " + data.coins.ungraded],
                                                datasets: [{
                                                    data: [data.coins.graded, data.coins.ungraded],
                                                    backgroundColor: ['#007bff', '#444444'],
                                                }],
                                            },
                                        });

                                        var ctx2 = document.getElementById("thirdPartyChart");
                                        var thirdPartyChart = new Chart(ctx2, {
                                            type: 'pie',
                                            data: {
                                                labels: ["Raw " + data.coins.raw, "Slabbed " + data.coins.slabbed],
                                                datasets: [{
                                                    data: [data.coins.raw, data.coins.slabbed],
                                                    backgroundColor: ['#444444', '#007bff'],
                                                }],
                                            },
                                        });

                                        document.getElementById('span_pcgs').innerHTML = data.coins.tpg.pcgs;
                                        document.getElementById('span_ngc').innerHTML = data.coins.tpg.ngc;
                                        document.getElementById('span_anacs').innerHTML = data.coins.tpg.anacs;
                                        document.getElementById('span_igc').innerHTML = data.coins.tpg.igc;

                                        if (data.coins.finest.slabbed.grade !== null){
                                            document.getElementById('slabbed_finest_link').href = "{{ route('collected.view_collected', [':collected']) }}".replace(':collected', data.coins.finest.slabbed.id);
                                            document.getElementById('slabbed_finest_grade').innerHTML = data.coins.prefix + data.coins.finest.slabbed.grade;
                                        } else {
                                            document.getElementById('slabbed_finest_grade').innerHTML = 'None';
                                        }
                                        if (data.coins.finest.raw.grade !== null){
                                            document.getElementById('raw_finest_grade').innerHTML = data.coins.prefix + data.coins.finest.raw.grade;
                                            document.getElementById('raw_finest_link').href = "{{ route('collected.view_collected', [':collected']) }}".replace(':collected', data.coins.finest.raw.id);
                                        } else {
                                            document.getElementById('raw_finest_grade').innerHTML = 'None';
                                        }


                                        @if(in_array($coin->strike, Config::get('constants.coins.business_strikes')))
                                        document.getElementById('business_total').innerHTML = data.coins.business.count;
                                        document.getElementById('business_graded').innerHTML = data.coins.business.graded;
                                        document.getElementById('business_ungraded').innerHTML = data.coins.business.ungraded;
                                        document.getElementById('business_certified').innerHTML = data.coins.business.pro_graded;
                                        @endif
                                        @if(in_array($coin->strike,Config::get('constants.coins.proof_strikes')))
                                        document.getElementById('proof_total').innerHTML = data.coins.proof.count;
                                        document.getElementById('proof_graded').innerHTML = data.coins.proof.graded;
                                        document.getElementById('proof_ungraded').innerHTML = data.coins.proof.ungraded;
                                        document.getElementById('proof_certified').innerHTML = data.coins.proof.pro_graded;
                                        @endif
                                        @if(in_array($coin->strike,Config::get('constants.coins.special_strikes')))
                                        document.getElementById('special_total').innerHTML = data.coins.special.count;
                                        document.getElementById('special_graded').innerHTML = data.coins.special.graded;
                                        document.getElementById('special_ungraded').innerHTML = data.coins.special.ungraded;
                                        document.getElementById('special_certified').innerHTML = data.coins.special.pro_graded;
                                        @endif
                                    }
                                )
                            })
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection

