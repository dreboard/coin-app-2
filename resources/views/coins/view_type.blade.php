@extends('layouts.user.main')
@section('pageTitle', "View Type - $coinType->coinType")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">{{ $coinType->coinType }}</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    View
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Coin</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Mintset</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Other</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">Coins Home</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('coin.view_category', ['id' => $coinType->coincats_id]) }}">{{ $coinType->coinCategory }}</a>
        </li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                {{ $coinType->coinType }} collected coins and resources.
                @if(array_key_exists($coinType->id, Config::get('constants.coins.american_eagle_types')))
                    Part of the <a href="{{ route('coin.view_eagle_index') }}">American Eagle Coins</a> Series
                @endif
            </div>


            <div class="card mb-3">
                <div class="card-body">

                    <div class="row">
                        <div class="col-sm-4">
                            <img src="{{config('app.image_url')}}{{ str_replace(' ', '_', $coinType->coinType) }}.jpg"/>
                        </div>
                        <div class="col-sm-8">
                            <table class="table">
                                <tr>
                                    <td><span class="fw-bold">Minted:</span></td>
                                    <td class="w-75">
                                        {{$coinType->dates}}
                                        @if($coinType->coinType == 'Morgan Dollar' || $coinType->coinType == 'Peace Dollar')
                                            ,<a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}"> Commemoratives</a>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="fw-bold">Metals:</span></td>
                                    <td class="w-75">
                                        @foreach (explode(', ', $coinType->metals) as $metal)
                                            <a href="{{ route('coin.view_type_mint', ['coinType' => $coinType->id, 'mint' => str_replace(' ', '_', $metal)]) }}">
                                                {{ $metal }},
                                            </a>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="fw-bold">Mints:</span></td>
                                    <td class="w-75">
                                        @foreach (explode(', ', $coinType->mints) as $mint)
                                            <a href="{{ route('coin.view_type_mint', ['coinType' => $coinType->id, 'mint' => str_replace(' ', '_', $mint)]) }}">
                                                {{ $mint }},
                                            </a>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="fw-bold">Edge:</span></td>
                                    <td>{{$coinType->edge}}</td>
                                </tr>
                                <tr>
                                    <td><span class="fw-bold">Mintmark:</span></td>
                                    <td>{{$coinType->mark_place}}</td>
                                </tr>
                                <tr>
                                    <td><span class="fw-bold">Strikes:</span></td>
                                    <td>
                                        @foreach($strikes as $strike)
                                            <a href="{{ route('coin.view_type_strike', ['coinType' => $coinType->id, 'strike' => str_replace(' ', '_', $strike)]) }}">{{$strike}}</a>
                                            @if(!$loop->last)
                                                ,
                                            @endif
                                        @endforeach
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            @if($coinType->coinType == 'Sacagawea Dollar')
                <div class="row">
                    <div class="col-sm-6"><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}"> Type 1, Sacagawea</a></div>
                    <div class="col-sm-6"><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}"> Type 2, Native American</a></div>
                </div>
            @endif


            <div class="card">
                <div class="card-body">
                    <table class="table">
                        @if(isset($design_varieties['obverse_coneca'][0]))
                            <tr>
                                <td><span class="fw-bold">Obverse Designs:</span></td>
                                <td>
                                    @foreach($design_varieties['obverse_coneca'] as $obverse)
                                        <a href="{{ route('coin.view_type_design_detail', [
                                                    'coinType' => $coinType->id,
                                                    'detail' => 'obverse',
                                                    'variety' => urlencode(urlencode($obverse)),
                                                    ]) }}">{{$obverse}}</a>
                                        @if(!$loop->last)
                                            ,
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                        @endif

                        @if(isset($design_varieties['reverse_coneca'][0]))
                            <tr>
                                <td><span class="fw-bold">Reverse Designs:</span></td>
                                <td>
                                    @foreach($design_varieties['reverse_coneca'] as $reverse)
                                        <a href="{{ route('coin.view_type_design_detail', [
                                                    'coinType' => $coinType->id,
                                                    'detail' => 'reverse',
                                                    'variety' => urlencode(urlencode($reverse)),
                                                    ]) }}">{{$reverse}}</a>
                                        @if(!$loop->last)
                                            ,
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                        @endif
                    </table>

                    @if(!empty($coinType->mms['Denver']) || !empty($coinType->mms['San Francisco']))
                        <h5>Mintmark Styles</h5>


                    <table>
                        @if(!empty($coinType->mms['Denver']))
                            <tr>
                                <td class="w-25"><span class="fw-bold">Denver: </span> </td>
                                <td>

                                    @foreach ($coinType->mms['Denver'] as $denver_mms)
                                        <a href="{{ route('coin.view_type_mms', [
                                                    'coinType' => $coinType->id,
                                                    'mint' => 'Denver',
                                                    'mms' => $denver_mms,
                                                    ]) }}">{{$denver_mms}}</a>
                                        @if(!$loop->last)
                                            ,
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                        @endif
                        @if(!empty($coinType->mms['San Francisco']))
                            <tr>
                                <td><span class="fw-bold">San Francisco: </span> </td>
                                <td>
                                    @foreach ($coinType->mms['San Francisco'] as $sf_mms)
                                        <a href="{{ route('coin.view_type_mms', [
                                                    'coinType' => $coinType->id,
                                                    'mint' => 'San_Francisco',
                                                    'mms' => $sf_mms,
                                                    ]) }}">{{$sf_mms}}</a>
                                        @if(!$loop->last)
                                            ,
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                        @endif

                    </table>
                    @endif

                </div>
            </div>

            <div class="row mt-3 mb-2">
                <div class="col-sm-3">
                    <a id="type_bulk_btn" class="btn btn-primary" data-bs-toggle="collapse" href="#collapseBulk" role="button" aria-expanded="false" aria-controls="collapseBulk">Bulk Data</a>
                </div>
                <div class="col-sm-3">
                    <a id="type_sets_btn" class="btn btn-primary" data-bs-toggle="collapse" href="#collapseSets" role="button" aria-expanded="false" aria-controls="collapseSets">Sets</a>
                </div>
                <div class="col-sm-3">
                    <a id="type_key_btn" class="btn btn-primary" data-bs-toggle="collapse" href="#collapseKey" role="button" aria-expanded="false" aria-controls="collapseExample2">Key Dates</a>
                </div>
                <div class="col-sm-3">
                    <a id="type_sell_btn" class="btn btn-primary" data-bs-toggle="collapse" href="#collapseSale" role="button" aria-expanded="false" aria-controls="collapseExample2">For Sale</a>
                </div>
            </div>

            <div class="collapse" id="collapseBulk">
                <div class="card">
                    <div class="card-body">
                        <P class="card-title"><a href="{{ route('coin.coin_index') }}">{{ $coinType->coinType }} Bulk
                                Report</a></P>

                        <table class="table">
                            <tr>
                                <td><span class="fw-bold">Rolls From Loose:</span> <span id="rolls_count"></span></td>
                                <td><span class="fw-bold">Rolls Value:</span> <span id="rolls_value"></span></td>
                            </tr>
                            <tr>
                                <td><span class="fw-bold">Coins Per Box:</span> {{$coinType->rollLgBoxCount}}</td>
                                <td><span class="fw-bold">Box Value:</span> {{$coinType->rollLgBoxVal}}</td>
                            </tr>
                            <tr>
                                <td><span class="fw-bold">Coins Per Bag:</span> {{$coinType->bagCount}}</td>
                                <td><span class="fw-bold">Bag Value:</span> {{$coinType->bagVal}}</td>
                            </tr>
                        </table>


                        <table class="table">
                            <tr>
                                <td><span class="fw-bold">Coins Per Roll:</span> {{$coinType->rollCount}}</td>
                                <td><span class="fw-bold">Roll Value:</span> {{$coinType->rollVal}}</td>
                            </tr>
                            <tr>
                                <td><span class="fw-bold">Coins Per Box:</span> {{$coinType->rollLgBoxCount}}</td>
                                <td><span class="fw-bold">Box Value:</span> {{$coinType->rollLgBoxVal}}</td>
                            </tr>
                            <tr>
                                <td><span class="fw-bold">Coins Per Bag:</span> {{$coinType->bagCount}}</td>
                                <td><span class="fw-bold">Bag Value:</span> {{$coinType->bagVal}}</td>
                            </tr>
                            @if(in_array($coinType->id, [101,99]))
                                <tr>
                                    <td><span
                                            class="fw-bold">Coins Per Monster Box:</span> {{$coinType->monster_box_count}}
                                    </td>
                                    <td><span class="fw-bold">Monster Box Value:</span> {{$coinType->monster_box_val}}
                                    </td>
                                </tr>
                            @endif

                        </table>
                    </div>
                </div>
            </div>
            <div class="collapse" id="collapseSets">
                <div class="card">
                    <div class="card-body">
                        <P class="card-title"><a href="{{ route('coin.coin_index') }}">{{ $coinType->coinType }} Bulk
                                Report</a></P>
                        <table class="table">
                            <tr>
                                <td><span class="fw-bold">Year Set:</span> {{$coinType->rollCount}}</td>
                                <td>
                                    <div class="dropdown float-start">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                                data-bs-toggle="dropdown" aria-expanded="false">
                                            View
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                            <li><a class="dropdown-item"
                                                   href="{{ route('collected.start') }}">Coin</a>
                                            </li>
                                            <li><a class="dropdown-item"
                                                   href="{{ route('collected.start') }}">Mintset</a>
                                            </li>
                                            <li><a class="dropdown-item"
                                                   href="{{ route('collected.start') }}">Other</a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="fw-bold">Date and Mintmark Set:</span> {{$coinType->rollLgBoxCount}}</td>
                                <td><span class="fw-bold">Box Value:</span> {{$coinType->rollLgBoxVal}}</td>
                            </tr>
                            <tr>
                                <td><span class="fw-bold">Sets Including Proofs:</span> {{$coinType->bagCount}}</td>
                                <td><span class="fw-bold">Bag Value:</span> {{$coinType->bagVal}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="collapse mb-3" id="collapseKey">
                <div class="card">
                    <div class="card-body">
                        <P class="card-title"><a href="{{ route('coin.coin_index') }}">{{ $coinType->coinType }} Key Dates Report</a></P>
                        <table class="table">
                            <tr>
                                <td class="fw-bold">Key Dates:</td>
                                @foreach($keyDates as $keyDate)
                                    <td>
                                        <a href="{{ route('coin.view_coin', ['coin' => $keyDate->id]) }}">{{ Str::limit($keyDate->coinName, 55) }}</a>
                                    </td>
                                @endforeach
                            </tr>
                            <tr>
                                <td class="fw-bold">Semi-Key Dates:</td>
                                @foreach($semiKeyDates as $semiKeyDate)
                                    <td>
                                        <a href="{{ route('coin.view_coin', ['coin' => $semiKeyDate->id]) }}">{{ Str::limit($semiKeyDate->coinName, 55) }}</a>
                                    </td>
                                @endforeach
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Saved</h5>
                    <div class="card card-body">
                        <table class="table">
                            <td><span class="fw-bold">Business:</span> {{$business}}</td>
                            <td><span class="fw-bold">Proofs:</span> {{$proofs}}</td>
                            <td><span class="fw-bold">Investment:</span> {{$proofs}}</td>
                        </table>
                    </div>
                </div>
            </div>


            @if(in_array($coinType->id, [3]))
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">By Composition</h5>
                        <div class="card card-body">
                            <table class="table">
                                <tr>
                                    <td><span class="fw-bold">1959-1982:</span>  95% Copper - 5% Tin and Zinc</td>
                                    <td><span class="fw-bold">1982-2008:</span> 97.5% Zinc - 2.5% Copper</td>
                                </tr>
                                <tr>
                                    <td><span class="fw-bold">Saved:</span>  <span id="copper_span"></span></td>
                                    <td><span class="fw-bold">Saved:</span>  <span id="zinc_span"></span></td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
            @endif


            @if(!empty($releaseList))
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">By Release</h5>
                            <div class="card card-body">
                                <table class="table">
                                    @foreach($releaseList as $year => $value)
                                        <tr>
                                            <td><span class="fw-bold">{{$year}}:</span>  </td>
                                            <td>@foreach($value as $release)
                                                    <a href="{{ route('coin.design_index', ['design' => str_replace(' ', '_', $release)]) }}">{{$release}} </a>
                                                    @if(!$loop->last)
                                                    ,
                                                @endif
                                                @endforeach</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
            @endif


            <div class="row">
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Varieties</h4>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Do Something
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('member.posts_index') }}">See All</a></li>
                                <li><a class="dropdown-item" href="{{ route('member.create_posts') }}">Create (Found A
                                        new variety)</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Errors</h4>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Do Something
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('member.view_books') }}">See All</a></li>
                                <li><a class="dropdown-item" href="{{ route('member.view_books') }}">Create (Found A
                                        new error)</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>Discussion Area</h4>
                        <a class="btn btn-secondary" href="{{ route('member.forum_type_index', ['type_id' => $coinType->id]) }}">Go →</a>
                    </div>
                </div>
            </div>


            <hr/>
            <!-- Featured blog post-->


            <!-- Nested row for non-featured blog posts-->
                <div class="table-responsive">
                    <table class="table table-hover datatable">
                        <thead>
                        <tr>
                            <th scope="col">Type</th>
                            <th scope="col">Collected</th>
                            <th scope="col">Investment</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($coins as $key => $coin)
                            <tr>
                                <td class="w-75">
                                    <a href="{{ route('coin.view_coin', ['coin' => $key]) }}">{{ Str::limit($coin['coin']->coinName, 55) }}</a>
                                </td>
                                <td class="text-center">
                                    {{$coin['collected']}}
                                </td>
                                <td class="text-center">
                                    {{$coin['investment']}}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td></td>
                                <td>No {{ $coin->coinName }} saved</td>
                                <td></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>

                <div class="col-lg-12">
                    <a class="btn btn-secondary" href="#!">All {{ $coinType->coinType }} History</a>
                </div>
            </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Switch Type</div>
                <div class="card-body">
                    <div class="input-group">
                        <select class="form-select" id="coin_id_select"
                                aria-label="Example select with button addon">
                            <option selected>Choose...</option>
                            @foreach(Config::get('constants.coins.types_list')[$coinType->coincats_id] as $key => $type)
                                <option value="{{ $key }}">{{ $type }}</option>
                            @endforeach
                        </select>
                        <button id="coin_id_select_btn" class="btn btn-outline-primary" type="button">Load</button>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">By Year</div>
                <div class="card-body">
                    <div class="input-group">
                        <select class="form-select" id="coin_year_select"
                                aria-label="Example select with button addon">
                            <option selected>Choose...</option>
                            @foreach($yearLists as $key => $year)
                                <option value="{{ $year }}">{{ $year }}</option>
                            @endforeach
                        </select>
                        <button id="coin_year_select_btn" class="btn btn-outline-primary" type="button">Load</button>
                    </div>
                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Reports</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                @if(array_key_exists($coinType->id, Config::get('constants.coins.american_eagle_types')))
                                    <a href="{{ route('coin.view_eagle_index') }}">American Eagle Coins</a>
                                @endif

                                @if(str_contains($coinType->metals, 'Gold') || str_contains($coinType->metals, 'Platunim') || str_contains($coinType->metals, 'Palladium'))
                                    <li><a href="{{ route('coin.metal_index') }}">Precious Metals</a></li>
                                @endif

                                @if(in_array($coinType->coinType, Config::get('constants.coins.lincoln_reverse')))
                                    <li><a href="{{ route('coin.lincoln_index') }}">All Lincoln Cents</a></li>
                                @endif
                                @if(str_contains($coinType->coinType, 'Seated Liberty') || $coinType->id == 44)
                                    <li><a href="{{ route('coin.seated_index') }}">Seated Liberty</a></li>

                                @endif
                                @if(in_array($coinType->coinType, Config::get('constants.coins.jefferson_reverse')))
                                    <li><a href="{{ route('coin.view_type_group', ['type' => 'jefferson_reverse']) }}">All
                                            Jefferson Nickels</a></li>
                                @endif

                                <li><a href="{{ route('coin.type_grades', ['coinType' => $coinType->id]) }}">Grade
                                        Report</a></li>
                                <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">Proof Report</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">




                                @if(in_array($coinType->category->id, Config::get('constants.coins.color_categories')))
                                    <li><a href="{{ route('coin.view_type_color', ['id' => $coinType->id]) }}">Color
                                            Report</a></li>
                                @endif

                                @if($coinType->coinType == 'Lincoln Wheat')
                                    <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">Color
                                            Report</a></li>
                                    <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">BIE Errors</a>
                                    </li>
                                    <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">Coppercoins
                                            List</a></li>
                                @endif



                                @if($coinType->coinType == 'Indian Head Cent' || $coinType->coinType == 'Flying Eagle')
                                    <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">By Snow
                                            Numbers</a></li>
                                @endif

                                @if($coinType->coinType == 'Morgan Dollar' || $coinType->coinType == 'Peace Dollar')
                                    <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">VAMs</a></li>
                                @endif

                                @if($coinType->coinType == 'Standing Liberty')
                                    <li><a href="{{ route('coin.view_type_head', ['id' => $coinType->id]) }}">Full Head
                                            Report</a></li>
                                @endif
                                @if($coinType->coinType == 'Jefferson Nickel')
                                    <li><a href="{{ route('coin.view_type_steps', ['id' => $coinType->id]) }}">Full
                                            Steps Report</a></li>
                                @endif
                                @if($coinType->coinType == 'Franklin Half Dollar')
                                    <li><a href="{{ route('coin.view_type_bell', ['id' => $coinType->id]) }}">Full Bell
                                            Lines Report</a></li>
                                @endif

                                @if(in_array($coinType->coinType, Config::get('constants.coins.full_band_types')))
                                    <li><a href="{{ route('coin.view_type_bands', ['id' => $coinType->id]) }}">Full
                                            Bands Report</a></li>
                                @endif

                                @if($coinType->coinType == 'Lincoln Wheat' || $coinType->coinType == 'Lincoln Memorial')
                                    <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">Coppercoins
                                            List</a></li>
                                @endif


                                <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">Damage Report</a>
                                </li>
                                <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">Variety Report</a>
                                </li>
                                <li><a href="{{ route('coin.view_type', ['id' => $coinType->id]) }}">Error Report</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        @if(!empty($typeLinks))
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">

                                    @foreach($typeLinks as $src => $link)
                                        <li><a class="external_link" href="{{$link}}">{{$src}}</a></li>
                                    @endforeach

                                </ul>
                            </div>
                        @endif
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                @if($coinType->category->id === 63)
                                    <li><a class="external_link" href="http://www.smalldollars.com/index.html">SmallDollars.com</a>
                                    </li>
                                @endif
                                @if(!empty($typeSpecificLinks))

                                    @foreach($typeSpecificLinks as $src => $link)
                                        <li><a class="external_link" href="{{$link}}">{{$src}}</a></li>
                                    @endforeach

                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">
                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.view_directory') }}">User Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    @push('scripts')
        <script>
            (function (ENVIRONMENT) {
                try {
                    const coin_type_id = {{$coinType->id}};

                    const coin_year_select = document.getElementById("coin_year_select");
                    const coin_year_select_btn = document.getElementById("coin_year_select_btn");

                    coin_year_select_btn.addEventListener("click", function () {
                        let coin_type_year = coin_year_select.options[coin_year_select.selectedIndex].value;
                        window.location.href = "{{ route('coin.view_type', [':id']) }}".replace(':id', coin_type_year);
                        //window.location.href = url;
                    });
                    const coin_id_select = document.getElementById("coin_id_select");
                    const coin_id_select_btn = document.getElementById("coin_id_select_btn");

                    coin_id_select_btn.addEventListener("click", function () {
                        let coin_type_id = coin_id_select.options[coin_id_select.selectedIndex].value;
                        window.location.href = "{{ route('coin.view_type', [':id']) }}".replace(':id', coin_type_id);
                        //window.location.href = url;
                    });

                    const type_bulk_btn = document.getElementById("type_bulk_btn");
                    const rolls_count = document.getElementById("rolls_count");
                    const rolls_value = document.getElementById("rolls_value");
                    type_bulk_btn.addEventListener('click', (event) => {
                        rolls_count.innerHTML = 'Loading..';
                        rolls_value.innerHTML = 'Loading..';
                        let url = "{{ route('collected.collected_get_bulk_data', [':type']) }}".replace(':type', coin_type_id);
                        fetch(url, {
                            "method": "GET",
                        }).then(
                            response => {
                                response.json().then(
                                    data => {
                                        console.log(data);
                                        rolls_count.innerHTML = data.roll_count;
                                        rolls_value.innerHTML = data.roll_value;
                                    }
                                )
                            })
                    }, { once: true });



                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection

