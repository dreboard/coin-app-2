@extends('layouts.user.main')
@section('pageTitle', "View Commemorative Coins")
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Commemorative Coins</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    View
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Coin</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Mintset</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('collected.start') }}">Other</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">Coins Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('coin.coin_tags')}}">All Tags</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                Commemorative collected coins and resources.
            </div>


            <div class="card mb-3">
                <div class="card-body">

                    <div class="row text-center">
                        <div class="col-sm-6 mb-2"><a href="{{ route('coin.view_coin_commem_type', ['type' => 'Modern']) }}">Early Commemorative Coins</a></div>
                        <div class="col-sm-6 mb-2"><a href="{{ route('coin.view_coin_commem_type', ['type' => 'Modern']) }}">Modern Commemorative Coins</a></div>
                        <div class="col-sm-6 mb-2"><a href="{{ route('coin.view_eagle_index') }}">American Eagle Coins</a></div>
                        <div class="col-sm-6 mb-2"><a href="{{ route('coin.view_coin_commem_type', ['type' => 'Modern']) }}">Modemmemorative Coins</a></div>
                    </div>
                </div>
            </div>


            <!-- Nested row for non-featured blog posts-->
            <div class="table-responsive">
                <table id="coins_tbl" class="table table-hover">
                    <thead>
                    <tr>
                        <th class="fw-bold">Type</th>
                        <th class="fw-bold">Collected</th>
                        <th class="fw-bold">Investment</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($coins as $coin)
                        <tr>
                            <td class="w-75">
                                <a href="{{ route('coin.view_coin', ['coin' => $coin->id]) }}">{{ Str::limit($coin->coinName, 55) }} {{$coin->coinType}}</a>
                            </td>
                            <td class="text-center">
                                <a href="{{ route('coin.view_coin', ['coin' => $coin->id]) }}">View</a>
                            </td>
                            <td class="text-center">
                                <a class="btn btn-secondary btn-sm" href="{{ route('collected.save_by_coin_id', ['id' => $coin->id]) }}">Add</a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td></td>
                            <td>None saved</td>
                            <td></td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>

            <div class="col-lg-12">
                <a class="btn btn-secondary" href="{{ route('coin.coin_index') }}">Coins Home</a>
            </div>
        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Save as a set</div>
                <div class="card-body">
                    <div class="input-group">
                        <a href="{{ route('collected.index') }}" class="btn btn-primary" id="button-search"
                           type="button">Go!</a>
                    </div>
                </div>
            </div>

            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Reports</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('coin.coin_index') }}">Grade
                                        Report</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">Proof Report</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('coin.coin_index') }}">Damage Report</a>
                                </li>
                                <li><a href="{{ route('coin.coin_index') }}">Variety Report</a>
                                </li>
                                <li><a href="{{ route('coin.coin_index') }}">Error Report</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">

                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="http://www.smalldollars.com/index.html">SmallDollars.com</a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">
                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.view_directory') }}">User Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    @push('scripts')
        <script>
            (function ($) {
                try {
                    $(document).ready( function() {
                        $(".datatable").dataTable().fnDestroy();

                        table = $('#coins_tbl').DataTable( {
                            "iDisplayLength": 50
                        } );
                    } );
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }


            }(window.jQuery));
        </script>
    @endpush
@endsection

