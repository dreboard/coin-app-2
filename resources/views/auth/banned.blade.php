@include('layouts.front.head')
<!-- Responsive navbar-->
@include('layouts.front.nav')
<!-- Page header with logo and tagline-->
<header class="py-3 bg-light border-bottom mb-4">
    <div class="container">
        <div class="text-center my-3">
            <h1 class="fw-bolder">Login Restricted</h1>
        </div>
    </div>
</header>
<!-- Page content-->
<div class="container">
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <!-- Featured blog post-->
            <div class="card mb-4">
                <!-- Session Status -->
                @if (session('status'))
                    <div class="alert alert-success">
                        <h5>{{ session('status') }}</h5>
                        <p>You will be notified by the supplied email. </p>
                    </div>
                @else

                <div class='font-medium text-sm alert alert-danger'>
                    <h3>Your Account has been Banned or Suspended</h3>
                    <p>{{ config('app.name', 'Coin App') }} suspends or bans accounts for violations of our Code of Conduct. If you're seeing this message, check your email for details about the violation.</p>
                </div>

                <form class="card-body p-11" method="POST" action="{{ route('restore_ban') }}">
                    @csrf
                    <p class="p-1 text-danger"> Request account restoration</p>
                    <!-- Email input -->
                    <div class="form-outline mb-2">
                        <label class="form-label" for="formEmail">Email address</label>
                        <input type="email" id="formEmail" class="form-control form-control-lg w-75"
                               placeholder="Enter a your email address" name="email" value="{{ old('email')}}"  />
                    </div>

                    <div class="text-center text-lg-start mt-4 pt-2">
                        <button type="submit" class="btn btn-primary btn-lg"
                                style="padding-left: 2.5rem; padding-right: 2.5rem;">Send Request</button>
                    </div>

                </form>
                @endif
            </div>


        </div>
        <!-- Side widgets -->
        @include('layouts.front.side-widget')
    </div>
</div>
<!-- Footer-->
<!-- Footer-->
@include('layouts.front.footer')
