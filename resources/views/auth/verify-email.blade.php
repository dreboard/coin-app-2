@include('layouts.front.head')
<!-- Responsive navbar-->
@include('layouts.front.nav')
<!-- Page header with logo and tagline-->
<header class="py-3 bg-light border-bottom mb-4">
    <div class="container">
        <div class="text-center my-3">
            <h1 class="fw-bolder">{{ $front_msg ?? 'Next Steps' }}</h1>
        </div>
    </div>
</header>
<!-- Page content-->
<div class="container">
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <!-- Featured blog post-->
            <div class="card mb-4">
                <div class="d-flex flex-row align-items-center justify-content-center justify-content-lg-start">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>

                <div class="mb-2 text-gray-600 p-3">
                    {{ __('Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn\'t receive the email, we will gladly send you another.') }}
                </div>

                @if (session('status') == 'verification-link-sent')
                    <div class="mb-4 font-medium text-sm text-green-600">
                        {{ __('A new verification link has been sent to the email address you provided during registration.') }}
                    </div>
                @endif

                <div class="mt-4 flex items-center justify-between">
                    <form method="POST" action="{{ route('verification.send') }}">
                        @csrf
                        <button type="submit" class="btn btn-primary btn-lg"
                                style="padding-left: 2.5rem; padding-right: 2.5rem;">Resend Verification Email</button>
                    </form>
                    <br>
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <button type="submit" class="btn btn-outline-primary btn-sm"
                                style="padding-left: 2.5rem; padding-right: 2.5rem;">Log Out</button>

                    </form>
                </div>
            </div>


        </div>
        <!-- Side widgets -->
        @include('layouts.front.side-widget')
    </div>
</div>
<!-- Footer-->
<!-- Footer-->
@include('layouts.front.footer')
