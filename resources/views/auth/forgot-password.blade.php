@include('layouts.front.head')
<!-- Responsive navbar-->
@include('layouts.front.nav')
<!-- Page header with logo and tagline-->
<header class="py-3 bg-light border-bottom mb-4">
    <div class="container">
        <div class="text-center my-3">
            <h1 class="fw-bolder">{{ $front_msg ?? 'Forgot My Password' }}</h1>
        </div>
    </div>
</header>
<!-- Page content-->
<div class="container">
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <!-- Featured blog post-->
            <div class="card mb-4">
                <!-- Session Status -->
                <x-auth-session-status class="mb-4 p-2" :status="session('status')" />
                <!-- Validation Errors -->
                <x-auth-validation-errors class="mb-4 p-2" :errors="$errors" />

                <div class="card-body">
                    <h2 class="card-title">Enter Email Address</h2>
                    <p class="card-text">
                        {{ __('Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }}
                    </p>
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <!-- Email Address -->
                        <div>
                            <x-label for="email" :value="__('Email')" />

                            <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')"  autofocus />
                        </div>

                        <div class="flex items-center justify-end mt-4">
                            <button type="submit" class="btn btn-primary btn-lg"
                                    style="padding-left: 2.5rem; padding-right: 2.5rem;">Email Password Reset Link</button>
                        </div>
                    </form>
                </div>
            </div>


        </div>
        <!-- Side widgets -->
        @include('layouts.front.side-widget')
    </div>
</div>
<!-- Footer-->
<!-- Footer-->
@include('layouts.front.footer')
