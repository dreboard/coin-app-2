@include('layouts.front.head')
<!-- Responsive navbar-->
@include('layouts.front.nav')
<!-- Page header with logo and tagline-->
<header class="py-3 bg-light border-bottom mb-4">
    <div class="container">
        <div class="text-center my-3">
            <h1 class="fw-bolder">{{ $front_msg ?? 'Create your account' }}</h1>
        </div>
    </div>
</header>
<!-- Page content-->
<div class="container">
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <!-- Featured blog post-->
            <div class="card mb-4">
                <div class="d-flex flex-row align-items-center justify-content-center justify-content-lg-start">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
                <form class="card-body p-11" method="POST" action="{{ route('register') }}">
                    @csrf
                    <!-- Email input -->
                    <div class="form-outline mb-2">
                        <label class="form-label" for="formEmail">Email address</label>
                        <input type="email" id="formEmail" class="form-control form-control-lg w-75"
                               placeholder="Enter a valid email address" name="email" value="{{ old('name')}}" required />
                    </div>
                    <div class="form-outline mb-2">
                        <label class="form-label" for="formName">Username</label>
                        <input type="text" id="formName" class="form-control form-control-lg w-75"
                               placeholder="Enter a valid email address" name="name" value="{{ old('email')}}" required />
                    </div>

                    <!-- Password input -->
                    <div class="form-outline mb-3">
                        <label class="form-label" for="password">Password</label>
                        <input  name="password" type="password" id="password" class="form-control form-control-lg w-75"
                                placeholder="Enter password" required autocomplete="current-password" />
                    </div>
                    <div class="form-outline mb-3">
                        <label class="form-label" for="password_confirmation">Password</label>
                        <input  name="password_confirmation" type="password" id="password_confirmation" class="form-control form-control-lg w-75"
                                placeholder="Enter password" required autocomplete="current-password" />
                    </div>

                    <div class="d-flex justify-content-between align-items-center">
                        <!-- Checkbox -->
                        <div class="form-check mb-0">
                            <input name="remember" class="form-check-input me-2" type="checkbox" value="" id="remember_me" />
                            <label class="form-check-label" for="remember_me">
                                Remember me
                            </label>
                        </div>
                        <a href="{{ route('password.request') }}" class="text-body">Forgot password?</a>
                    </div>

                    <div class="text-center text-lg-start mt-4 pt-2">
                        <button type="submit" class="btn btn-primary btn-lg"
                                style="padding-left: 2.5rem; padding-right: 2.5rem;">Register</button>
                        <p class="small fw-bold mt-2 pt-1 mb-0">Already have an account? <a href="{{ route('login') }}"
                                                                                            class="link-danger">Login</a></p>
                    </div>

                </form>
            </div>


        </div>
        <!-- Side widgets -->
        @include('layouts.front.side-widget')
    </div>
</div>
<!-- Footer-->
<!-- Footer-->
@include('layouts.front.footer')
