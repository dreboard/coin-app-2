<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Core</div>
                <a class="nav-link" href="{{ route('user.dashboard') }}">
                    <div class="sb-nav-link-icon"><i class="bi bi-house-door"></i></div>
                    Dashboard
                </a>
                <div class="sb-sidenav-menu-heading">My Area</div>
                <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseMyArea" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    My Area
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div @if(!in_array(Route::currentRouteName(), Config::get('pages.user_pages'))) class="collapse" @endif id="collapseMyArea" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{ route('user.my_area') }}">My Area</a>
                        <a class="nav-link" href="{{ route('user.user_profile') }}">Profile</a>
                        <a class="nav-link" href="{{ route('user.public', ['id' => auth()->user()->id]) }}">Public Page</a>
                        <a class="nav-link" href="{{ route('user.message_all') }}">Messages</a>
                        <a class="nav-link" href="{{ route('user.edit_public') }}">Settings</a>
                    </nav>
                </div>
                <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
                    <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                    Social
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-bs-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth">
                            Groups
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordionPages">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{ route('group.index') }}">Home</a>
                                <a class="nav-link" href="{{ route('group.mine') }}">Mine</a>
                                <a class="nav-link" href="{{ route('group.all') }}">All</a>
                                <a class="nav-link" href="{{ route('user.groups_i_follow') }}">Following</a>
                            </nav>
                        </div>
                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#pagesCollapseArticles" aria-expanded="false" aria-controls="pagesCollapseError">
                            Articles
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="pagesCollapseArticles" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordionPages">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{ route('member.user_posts', ['id' => auth()->user()->id]) }}">My Articles</a>
                                <a class="nav-link" href="{{ route('member.forum') }}">Forum</a>
                                <a class="nav-link" href="{{ route('user.writers') }}">Writers Area</a>
                                <a class="nav-link" href="{{ route('member.posts_index') }}">All Articles</a>
                            </nav>
                        </div>
                    </nav>
                </div>
                <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseCollection" aria-expanded="false" aria-controls="collapsePages">
                    <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                    Collection
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseCollection" aria-labelledby="headingTwo" data-bs-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#pagesCollapseGroups" aria-expanded="false" aria-controls="pagesCollapseAuth">
                            Coins
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="pagesCollapseGroups" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordionPages">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{ route('collected.index') }}">Collected</a>
                                <a class="nav-link" href="{{ route('group.all') }}">Add</a>
                            </nav>
                        </div>
                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#pagesCollapseError" aria-expanded="false" aria-controls="pagesCollapseError">
                            Currency
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordionPages">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{ route('collected.index') }}">Collected</a>
                                <a class="nav-link" href="{{ route('group.all') }}">Add</a>
                            </nav>
                        </div>
                    </nav>
                </div>
                <div class="sb-sidenav-menu-heading">Addons</div>
                <a class="nav-link" href="{{ route('news.all') }}">
                    <div class="sb-nav-link-icon"><i class="bi bi-newspaper"></i></div>
                    News
                </a>
                <a class="nav-link" href="{{ route('news.all') }}">
                    <div class="sb-nav-link-icon"><i class="bi bi-currency-dollar"></i></div>
                    Marketplace
                </a>
            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Logged in as: {{ auth()->user()->name ?? '' }}</div>
        </div>
    </nav>
</div>
