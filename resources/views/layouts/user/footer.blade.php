<div class="footer-dark mt-5">
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-3 item">
                    <h3>Legal</h3>
                    <ul>
                        <li><a href="{{ route('policies') }}">Site Policies</a></li>
                        <li><a href="#"><a href="{{ route('user.violation_create') }}">Report Violation</a></a></li>
                        <li><a href="#">Hosting</a></li>
                    </ul>
                </div>
                <div class="col-md-3 item">
                    <h3>Services</h3>
                    <ul>
                        <li><a href="#">Web design</a></li>
                        <li><a href="#">Development</a></li>
                        <li><a href="#">Hosting</a></li>
                    </ul>
                </div>
                <div class="col-md-3 item">
                    <h3>About</h3>
                    <ul>
                        <li><a href="#">Company</a></li>
                        <li><a href="#">Team</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </div>
                <div class="col-md-3 item text">
                    <h3>{{ optional(auth()->user())->name }}</h3>
                    <p><a href="{{ route('user.public', ['id' => auth()->user()->id]) }}">Profile</a></p>
                </div>
                <div class="col item social"></div>
            </div>
            <p class="copyright">Copyright &copy; {{ config('app.name', 'Collecting United') }} {{ date('Y') }}</p>
        </div>
    </footer>
</div>
{{--@if(config('app.env') !== 'production') {{ dd(get_defined_vars()) }}@endif--}}
