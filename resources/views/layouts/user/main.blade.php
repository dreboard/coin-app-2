<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <base href="http://cdn.dev-php.site/public/img/coins/"> -->


    <title>@yield('pageTitle') - {{ config('app.name', 'Collecting United') }}</title>
    @include('layouts.styles')

    <script async>
        const ENVIRONMENT = "{{ config('app.env') }}";
        const SITE_URL = "{{ config('app.url') }}";
        const USER_ID = "{{ auth()->user()->id ?? 0 }}";
        const CSRF_TOKEN = "{{ csrf_token() }}";
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
{{--    @include('layouts.ga')--}}

</head>
<body class="sb-nav-fixed">

@include('layouts.user.top-nav')
<div id="layoutSidenav">
    @include('layouts.user.side-nav')
    <div id="layoutSidenav_content">
        <main>
            @if ($errors && $errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @else
                {{ $errors = ''}}
            @endif
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                @if(optional(auth()->user())->status == 'warn')
                    <div class="alert alert-warning">
                        <h2>Your Account is in warning status</h2>
                        <p>The administrator has issued warning(s) for your account.  See <a class="btn btn-warning" href="{{ route('user.warning_page') }}" role="button">Warning status</a></p>
                    </div>
                @endif

                @if(isset($announcement))
                    <div class="alert alert-primary">
                        <p>The administrator has issued warning(s) for your account.  See <a href="{{ route('user.dashboard') }}">Whats New</a></p>
                    </div>
                @endif
            <div id="app" class="container-fluid px-4">
                @stack('header')

                @yield('content')


            </div>
        </main>
        @include('layouts.user.footer')
    </div>
</div>
@include('layouts.scripts')
</body>
</html>
@if(config('app.env') !== 'production') {{ dd(get_defined_vars()) }} @endif
