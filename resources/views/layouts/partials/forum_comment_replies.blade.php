
@foreach($comments as $comment)

{{--  {{ dd($comment->replies) }}  @if($comment->id === 631) {{ dd($comment) }} @endif--}}
    <div class="display-comment card p-3 mb-2">
        <p><strong>{{ $comment->user->name }}</strong> {{ Carbon\Carbon::parse($comment->created_at)->diffForHumans() }}</p>


        @if(isset($comment->images[0]))
            <div class="col-sm-6">
                <figure class="mb-4">
                    <img src="{{asset('storage/'.$comment?->images[0]->image_url)}}" style="width: 80%; height: auto" alt=""/>
                </figure>
            </div>
        @endif
        @if(!is_null($comment->parent_id))

        @endif
        <p>{!! $comment->body !!}</p>
        <form class="commentForm" method="post" action="{{ route('member.save_forum_comment_reply') }}" enctype="multipart/form-data">
            @csrf

            @if(is_null($comment->parent_id))
                <div class="mb-3">
                    <label for="image_path" class="form-label">Attach an image</label>
                    <input class="form-control" type="file" name="image_url" id="image_url">
                </div>
            @endif

            <div class="form-group">
                <input type="text" name="comment_body" class="form-control" />
                <input type="hidden" name="forum_id" value="{{ $forum_id }}" />
                <input type="hidden" name="comment_id" value="{{ $comment->id }}" />
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Reply" />
            </div>
        </form>
        @include('layouts.partials.forum_comment_replies', ['comments' => $comment->replies])
{{--        {{ $comment->links('pagination::bootstrap-5') }}--}}
    </div>
@endforeach
