{{-- <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
   <link rel="stylesheet" href="{{ asset('css/admin.css') }}">--}}

<link rel="stylesheet" href="{{ asset('css/app.css') }}?cache={{time()}}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
{{--<link rel="stylesheet" href="{{ asset('DataTables/datatables.min.css') }}">--}}

@stack('styles')
