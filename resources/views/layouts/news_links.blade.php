<div class="col-lg-4">
    <!-- Categories widget-->
    <div class="card mb-4">
        <div class="card-header">News Feeds</div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <ul class="list-unstyled mb-0">
                        <li><a href="{{ route('news.feed', 'us_mint') }}">US Mint</a></li>
                        <li><a href="{{ route('news.ana', 'ana') }}">ANA</a></li>
                        <li><a href="{{ route('news.feed', 'coin_news') }}">CoinNews</a></li>
                        <li><a href="{{ route('news.feed', 'coin_week') }}">Coin Week</a></li>
                    </ul>
                </div>
                <div class="col-sm-6">
                    <ul class="list-unstyled mb-0">
                        <li><a href="{{ route('news.feed', 'coin_show') }}">The Coin Show</a></li>
                        <li><a href="{{ route('news.feed', 'coin_update') }}">Coin Update</a></li>
                        <li><a href="{{ route('news.feed', 'littleton') }}">Littleton</a></li>
                        <li><a href="{{ route('news.feed', 'davidlawrence') }}">David Lawrence</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Side widget-->
    <div class="card mb-4">
        <div class="card-header">Podcasts & Videos</div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <ul class="list-unstyled mb-0">
                        <li><a href="{{ route('video.channel', 'UCQ-T-XzN7mJQoYdW5zuGBSA') }}">PCGS</a></li>
                        <li><a href="{{ route('video.channel', 'UCQ-T-XzN7mJQoYdW5zuGBSA') }}">American Numismatic Association</a></li>
                        <li><a href="{{ route('video.channel', 'PCGScoin') }}">CoinNews</a></li>
                        <li><a href="{{ route('news.feed', 'coin_week') }}">Coineek</a></li>
                    </ul>
                </div>
                <div class="col-sm-6">
                    <ul class="list-unstyled mb-0">
                        <li><a href="{{ route('news.feed', 'coin_show') }}">TheCoinShow</a></li>
                        <li><a href="{{ route('news.feed', 'coin_update') }}">CoinUpdate</a></li>
                        <li><a href="{{ route('news.feed', 'littleton') }}">Littleton</a></li>
                        <li><a href="{{ route('news.feed', 'davidlawrence') }}">DavidLawrence</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
