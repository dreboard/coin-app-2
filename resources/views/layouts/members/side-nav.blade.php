<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Core</div>
                <a class="nav-link" href="{{ route('user.dashboard') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Dashboard
                </a>
                <div class="sb-sidenav-menu-heading">Interface</div>

                @if(auth()->user()->userManageLevel($group->id) > 2)
                    <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                        <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                        User Management
                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                    </a>
                    <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                        <nav class="sb-sidenav-menu-nested nav">
                            <a class="nav-link" href="{{ route('group.view_users', ['group' => $group->id]) }}">Users</a>
                            <a class="nav-link" href="{{ route('group.view_requests', ['group' => $group->id]) }}">Requests</a>
                        </nav>
                    </div>
                @endif

                <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
                    <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                    Pages
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-bs-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth">
                            Messaging
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordionPages">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{ route('group.view', ['group' => $group->id]) }}">Announcements</a>
                                <a class="nav-link" href="{{ route('group.view', ['group' => $group->id]) }}">Newsletters</a>
                                <a class="nav-link" href="{{ route('group.message_index', ['group' => $group->id]) }}">Messages</a>
                            </nav>
                        </div>
                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#pagesCollapseError" aria-expanded="false" aria-controls="pagesCollapseError">
                            Activities
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordionPages">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{ route('group.view', ['group' => $group->id]) }}">Events</a>
                                <a class="nav-link" href="{{ route('group.view', ['group' => $group->id]) }}">Shows</a>
                                <a class="nav-link" href="{{ route('group.view', ['group' => $group->id]) }}">Meetings</a>
                            </nav>
                        </div>
                    </nav>
                </div>
                <div class="sb-sidenav-menu-heading">Addons</div>
                <a class="nav-link" href="{{ route('group.view', ['group' => $group->id]) }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                    Calendar
                </a>
                <a class="nav-link" href="{{ route('group.view', ['group' => $group->id]) }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Media
                </a>
                <a class="nav-link" href="{{ route('group.intro', ['group' => $group->id]) }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Intro Page
                </a>
            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Logged in as: {{ auth()->user()->name ?? '' }}</div>
        </div>
    </nav>
</div>
