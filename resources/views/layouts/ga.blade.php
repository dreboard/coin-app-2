<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id={{ config('ga.property') }}"></script>
<script>
    (function () {
        try {
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', '{{ config('ga.property') }}', {'user_id': '{{ auth()->user()->id ?? '0000' }}'});
            if (typeof GROUP_ID !== 'undefined') {
                gtag('config', '{{ config('ga.property') }}', {'group_id': GROUP_ID});
                gtag('set', 'group_properties', { 'crm_id' : GROUP_ID });
            }
            gtag('set', 'user_properties', { 'crm_id' : '{{ auth()->user()->id ?? '0000' }}' });
        } catch (error) {
            if (ENVIRONMENT === 'local') {
                console.error(error);
            }
        }
    })();
</script>
{{-- Google Analytics --}}
{{-- https://coin-app.dev-php.site --}}
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-ZM103LD445"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-ZM103LD445');
</script>
