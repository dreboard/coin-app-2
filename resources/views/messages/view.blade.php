@extends('layouts.user.main')
@section('pageTitle', 'View Message')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">View Message</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Profile Pages
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">Back To Settings</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.edit_public', ['user' => auth()->user()->id]) }}">Edit This
                            Profile</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_edit_avatar', ['user' => auth()->user()->id]) }}">Change Image</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.research') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.message_all') }}">My Messages</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                You can send bulk messages to <a href="{{ route('user.user_who_follow_me') }}">users that follow you</a>.  Visit the <a href="{{ route('user.user_types', ['type' => 'Writer']) }}">User Directory</a>
                to send messages to others.
            </div>

            <div class="small text-muted">Sent: {{optional($notification->read_at)->format('F jS Y, g:ia') ?? $notification->created_at->format('F jS Y, g:ia') }}</div>
            <div class="small text-muted">From: {{ $from->name }}</div>
            @if(isset($notification->data['image_url']))
                <figure class="mb-4">
                    <img src="{{asset('storage/'.$notification->data['image_url'])}}" style="height: auto; width: 100%" alt="" />
                </figure>
            @endif
            <h2 class="card-title">{{$notification->data['subject']}}</h2>
            <p class="card-text">{!!$notification->data['body']!!}</p>
            @if(isset($notification->data['forum_id']))
                <a href="{{route('member.view_forum_post', ['forum' => $notification->data['forum_id']])}}">Go to discussion</a>
            @endif
            <form class="row row-cols-lg-auto g-3 align-items-center" method="post" action="{{ route('user.message_delete') }}">
                @csrf
                <input type="hidden" name="notification_id" value="{{ $notification->id }}">
                <div class="col-12">
                    <button type="submit" class="btn btn-danger">Delete</button>
                </div>
            </form>


            @if(!isset($notification->data['forum_id']))
                <h5>Reply</h5>
                <form id="message_reply_form" action="{{ route('user.message_reply') }}"
                      method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-12">
                        <input type="hidden" name="notification_id" value="{{ $notification->id }}">
                        <input type="hidden" name="to_id" value="{{ $from->id }}">

                        <div class="form-group mb-3">
                            <label class="control-label">Article Text</label>
                            <textarea id="message_reply_body" name="body" class="form-control">{{ old('body') }}</textarea>
                        </div>
                        <!-- Submit Form Input -->
                        <div class="col-3 mt-3">
                            <button id="message_reply_form_btn" type="submit" class="btn btn-primary">Send</button>
                        </div>
                    </div>
                </form>
            @else
                <p><a class="btn btn-outline-primary" href="{{route('member.view_forum_post', ['forum' => $notification->data['forum_id']])}}">Go to discussion</a></p>
            @endif


        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Create</div>
                <div class="card-body">

                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Choose
                        </button>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="{{ route('user.message_create') }}">New Message</a></li>
                            <li><a class="dropdown-item" href="{{ route('user.message_create') }}">New Discussion</a></li>
                        </ul>
                    </div>

                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Connect</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.user_types', ['type' => 'Writer']) }}">User Directory</a></li>
                                <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script src="https://cdn.ckeditor.com/ckeditor5/35.2.1/classic/ckeditor.js"></script>
    <script>

        (function () {
            try {

                ClassicEditor.defaultConfig = {
                    toolbar: {
                        items: [
                            'heading',
                            '|',
                            'bold',
                            'italic',
                            '|',
                            'bulletedList',
                            'numberedList',
                            '|',
                            'insertTable',
                            '|',
                            'undo',
                            'redo'
                        ]
                    },
                    image: {
                        toolbar: [
                            'imageStyle:full',
                            'imageStyle:side',
                            '|',
                            'imageTextAlternative'
                        ]
                    },
                    table: {
                        contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells']
                    },
                    language: 'en'
                };


                ClassicEditor
                    .create(document.querySelector('#message_reply_body')).catch(error => {
                    console.error(error);
                });


            } catch (error) {
                if (ENVIRONMENT === 'local') {
                    console.error(error);
                }
            }
        })();
    </script>
@endpush
