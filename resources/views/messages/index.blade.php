@extends('layouts.user.main')
@section('pageTitle', 'My Messages')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">My Messages</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Profile Pages
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">Back To Settings</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.edit_public', ['user' => auth()->user()->id]) }}">Edit This
                            Profile</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_edit_avatar', ['user' => auth()->user()->id]) }}">Change Image</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.research') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.user_types', ['type' => 'Research']) }}">Researchers Directory</a></li>
        <li class="breadcrumb-item"><a href="{{ route('member.user_projects') }}">My Projects</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                You can send bulk messages to <a href="{{ route('user.user_who_follow_me') }}">users that follow you</a>.  Visit the <a href="{{ route('user.user_types', ['type' => 'Writer']) }}">User Directory</a>
                to send messages to others.
            </div>

            <table class="table table-hover datatable">
                <thead>
                <tr>
                    <th class="fw-bold" scope="col">From</th>
                    <th class="fw-bold" scope="col">Subject</th>
                    <th class="fw-bold text-end" scope="col">Posted</th>
                </tr>
                </thead>
                <tbody>
                @forelse (auth()->user()->notifications as $notification)
                    <tr class="{{ ( $notification->read_at == null ) ? 'alert alert-primary' : '' }}"
                        data-read_at="@if(is_null($notification->read_at))unread @endif">
                        <td class="text-start">
                            <a href="{{ route('user.public', ['id' => $notification->data['from_id']]) }}">
                                {{Str::limit($notification->data['from'], 20)}}
                            </a>
                        </td>
                        <td class="text-start">
                            <span class="{{ ( $notification->read_at === null ) ? 'fw-bolder' : 'fw-lighter' }}">
                                <a href="{{ route('user.message_view', ['id' => $notification->id]) }}">
                                    {{Str::limit($notification->data['subject'], 50)}}
                                </a>
                            </span>
                        </td>
                        <td class="text-end">{{ optional($notification->read_at)->format('M jS Y, g:ia') ?? $notification->created_at->format('M jS Y, g:ia')}}</td>
                    </tr>
                @empty
                    <tr>
                        <td></td>
                        <td>There are no messages</td>
                        <td></td>
                    </tr>
                @endforelse
                </tbody>
            </table>


        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Create A Message</div>
                <div class="card-body">
                    <div class="input-group">
                        <a href="{{ route('user.message_create') }}" class="btn btn-primary" id="button-search"
                           type="button">Go!</a>
                    </div>
                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Connect</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.user_types', ['type' => 'Writer']) }}">User Directory</a></li>
                                <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script>
        (function () {
            try {
                var message_row = document.querySelectorAll('.message_row')
                message_row.forEach(function(message_row) {
                    console.log(message_row.dataset.read_at);
                    if (message_row.dataset.read_at == 'unread') {
                        message_row.style.backgroundColor = null;
                        //message_row.classList.add('background-color: transparent');
                        message_row.classList.add('alert alert-primary');
                    }
                });

            } catch (error) {
                if (ENVIRONMENT === 'local') {
                    console.error(error);
                }
            }
        })();
    </script>
@endpush
