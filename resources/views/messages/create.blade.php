@extends('layouts.user.main')
@section('pageTitle', 'Create Message')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Create Message</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Profile Pages
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">Back To Settings</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.edit_public', ['user' => auth()->user()->id]) }}">Edit This
                            Profile</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_edit_avatar', ['user' => auth()->user()->id]) }}">Change Image</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.research') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('user.message_all') }}">My Messages</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                You can send bulk messages to <a href="{{ route('user.user_who_follow_me') }}">users that follow you</a>.  Visit the <a href="{{ route('user.user_types', ['type' => 'Writer']) }}">User Directory</a>
                to send messages to others.
            </div>

            <form class="boldLabel" id="message_reply_form" action="{{ route('user.message_save') }}"
                  method="post" enctype="multipart/form-data">
                @csrf
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Subject</label>
                    <input type="text" class="form-control" name="subject" value="{{ old('subject') }}">
                </div>

                <div class="mb-3">
                    <label for="image_path" class="form-label">Attach an image</label>
                    <input class="form-control" type="file" name="image_url" id="image_url" onchange="preview()">
                    <img id="image_preview_frame" src="" width="100px" height="100px" alt=""/>
                </div>
                <br />

                <div class="table-responsive">
                    <table class="table table-hover datatable mt-3 mb-3">
                        <thead>
                        <tr>
                            <th scope="col">User</th>
                            <th scope="col">Type</th>
                            <th class="text-end" scope="col">Include</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($users as $user)
                            <tr>
                                <td class="text-start">
                                    <label for="user_{{ $user->id }}" title="{{ $user->name }}">
                                        {{ $user->name }}
                                    </label>
                                </td>
                                <td>{{ucfirst($user->user_type)}}</td>
                                <td style="padding-right: 45px;" class="text-end"><input id="user_{{ $user->id }}" type="checkbox" name="recipients[]"
                                           value="{{ $user->id }}" @if(old('recipients')) checked @endif></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>



                <br />

                <div class="form-group mb-3 mt-3">
                    <label class="control-label">Article Text</label>
                    <textarea id="message_body" name="body" class="form-control">{{ old('body') }}</textarea>
                </div>
                <!-- Submit Form Input -->
                <div class="col-3 mt-3">
                    <button id="message_reply_form_btn" type="submit" class="btn btn-primary">Send</button>
                </div>
            </form>

        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Create A Discussion</div>
                <div class="card-body">
                    <div class="input-group">
                        <a href="{{ route('member.forum_create') }}" class="btn btn-primary" id="button-search"
                           type="button">Go!</a>
                    </div>
                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Connect</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.user_types', ['type' => 'Writer']) }}">User Directory</a></li>
                                <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script src="https://cdn.ckeditor.com/ckeditor5/35.2.1/classic/ckeditor.js"></script>
    <script>
        let image_preview_frame = document.getElementById('image_preview_frame');

        function preview() {
            image_preview_frame.src = URL.createObjectURL(event.target.files[0]);
        }

        (function () {
            try {

                ClassicEditor.defaultConfig = {
                    toolbar: {
                        items: [
                            'heading',
                            '|',
                            'bold',
                            'italic',
                            '|',
                            'bulletedList',
                            'numberedList',
                            '|',
                            'insertTable',
                            '|',
                            'undo',
                            'redo'
                        ]
                    },
                    image: {
                        toolbar: [
                            'imageStyle:full',
                            'imageStyle:side',
                            '|',
                            'imageTextAlternative'
                        ]
                    },
                    table: {
                        contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells']
                    },
                    language: 'en'
                };


                ClassicEditor
                    .create(document.querySelector('#message_body')).catch(error => {
                    console.error(error);
                });


            } catch (error) {
                if (ENVIRONMENT === 'local') {
                    console.error(error);
                }
            }
        })();
    </script>
@endpush



