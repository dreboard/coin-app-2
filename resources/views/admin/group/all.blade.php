@extends('layouts.admin.main')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    @push('header')
        <h3 class="mt-4">Group Management</h3>
    @endpush

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('admin.view_users') }}">Users</a></li>
    </ol>

    <div class="table-responsive">

        <table id="user_datatable" class="table datatable">
            <thead>
            <tr>
                <th>ID #</th>
                <th>Name</th>
                <th>Members</th>
                <th>Created</th>
                <th>Type</th>
                <th>View</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>ID #</th>
                <th>Name</th>
                <th>Members</th>
                <th>Created</th>
                <th>Type</th>
                <th>View</th>
            </tr>
            </tfoot>
            <tbody>
            @foreach ($groups as $group)
                <tr>
                    <td class="text-start">{{ $group->id }}</td>
                    <td class="text-start">{{ ucfirst($group->name) }}</td>
                    <td class="text-center">{{ ucfirst($group->user_count) }}</td>
                    <td class="text-start">{{ Carbon\Carbon::parse($group->created_at)->format('F jS Y, g:ia') }}</td>
                    <td class="text-start">
                        <span class="
                        @if ($group->group_type == 'club')
                            text-success
                        @elseif ($group->group_type == 'association')
                            text-warning
                        @else
                            text-info
                        @endif ">{{ ucfirst($group->group_type)}}</span>
                    </td>
                    <td class="text-start"><a href="{{ route('admin.group_view', ['id' => $group->id]) }}">View</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
