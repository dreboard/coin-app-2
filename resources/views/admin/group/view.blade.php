@extends('layouts.admin.main')

@section('title', 'View Group')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <h3 class="mt-4">#{{ $group->id }} {{ $group->name }}</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a> </li>
        <li class="breadcrumb-item active"><a href="{{ route('admin.group_all') }}">Groups</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('admin.view_users') }}">Users</a></li>
    </ol>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Type</th>
            <th scope="col">Specialty</th>
            <th scope="col">Created</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">{{ $group->id }}</th>
            <td>{{ $group->group_type }}</td>
            <td>{{ $group->specialty }}</td>
            <td>{{ Carbon\Carbon::parse($group->created_at)->format('F jS Y, g:ia') }}</td>
        </tr>
        </tbody>
    </table>


    <div class="table-responsive">

        <table id="user_datatable" class="table datatable">
            <thead>
            <tr>
                <th>ID #</th>
                <th>Name</th>
                <th>Status</th>
                <th>View</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>ID #</th>
                <th>Name</th>
                <th>Status</th>
                <th>View</th>
            </tr>
            </tfoot>
            <tbody>
            @foreach ($users as $user)
                <tr>
                    <td class="text-start">{{ $user->id }}</td>
                    <td class="text-start">{{ $user->name }}</td>
                    <td class="text-start">{{ ucfirst($user->member_type) }} | {{ $user->club_position }}</td>
                    <td class="text-start"><a href="{{ route('admin.view_user', ['user_id' => $user->id]) }}">View</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
