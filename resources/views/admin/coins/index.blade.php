@extends('layouts.user.main')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')

    <h3 class="mt-4">Admin Dashboard</h3>


    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Home</li>
    </ol>

    <div class="table-responsive">
        <table class="table table-sm table-borderless">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">New</th>
                <th scope="col">Active</th>
                <th scope="col">Total</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">Users</th>
                <td>{{ $createdToday ?? 0}}</td>
                <td>{{ $allActiveCount ?? 0 }}</td>
                <td>{{ $allUsersCount ?? 0}}</td>
            </tr>
            <tr>
                <th scope="row">Clubs</th>
                <td>11</td>
                <td>111</td>
                <td>1111</td>
            </tr>
            <tr>
                <th scope="row">Groups</th>
                <td>11</td>
                <td>111</td>
                <td>1111</td>
            </tr>
            </tbody>
        </table>
    </div>


    <div class="card-group mt-3">
        <div class="card">
            <div class="card-header">
                Users
            </div>
            <div class="card-body">
                <table class="table">
                    <tr>
                        <td><a href="{{ route('admin.violation_pending') }}">Pending Violations</a></td>
                        <td><a href="{{ route('admin.view_users') }}">Messages</a></td>
                        <td>@mdo</td>
                    </tr>
                    <tr>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                    </tr>
                </table>
                <a href="{{ route('admin.view_users') }}" class="btn btn-primary">Manage</a>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                Featured
            </div>
            <div class="card-body">
                <h5 class="card-title">Clubs</h5>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <a href="{{ route('admin.view_users') }}" class="btn btn-primary">Manage</a>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                Featured
            </div>
            <div class="card-body">
                <h5 class="card-title">Groups</h5>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <a href="{{ route('admin.view_users') }}" class="btn btn-primary">Manage</a>
            </div>
        </div>
    </div>




@endsection
