@extends('layouts.user.main')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')

    <h3 class="mt-4">Create System Message</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        <li class="breadcrumb-item"><a href="{{ route('user.message_all') }}">Messages</a> </li>
    </ol>
    <div class="container">
        <div class="main-body">

            <form action="{{ route('admin.message_system_save') }}" method="post">
                @csrf
                <div class="mb-3">
                    <label for="subject" class="form-label">Email address</label>
                    <input type="text" class="form-control" name="subject" placeholder="name@example.com">
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlTextarea1" class="form-label">Example textarea</label>
                    <textarea class="form-control" name="body" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>

        </div>
    </div>


@endsection

