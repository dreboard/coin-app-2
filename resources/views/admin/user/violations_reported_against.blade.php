@extends('layouts.admin.main')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <h3 class="mt-4">Violations Reported Against {{ $user->name }} </h3>
    <ol class="breadcrumb mb-4">
        @admin
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a> </li>
        <li class="breadcrumb-item active"><a href="{{ route('admin.view_users') }}">Users</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('admin.view_user', ['user_id' => $user->id]) }}">User</a></li>
        @else
            <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
            <li class="breadcrumb-item"><a href="{{ route('user.message_all') }}">Messages</a> </li>
        @endadmin


    </ol>

    <div class="container">
        <div class="main-body">


            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Warning Status</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">

                                <form class="row row-cols-lg-auto g-3 align-items-center" method="POST" action="{{ route('admin.warn_user')  }}">
                                    @csrf
                                    <input type="hidden" name="user_id" value="{{$user->id}}">

                                    @if($user->status == 'good')
                                        <span class="text-success">Good</span>
                                        <input type="hidden" name="status" value="warn">
                                        <div class="col-4">
                                            <button type="submit" class="btn btn-warning">Warn</button>
                                        </div>
                                    @else
                                        <span class="text-warning">Warning</span>
                                        <input type="hidden" name="status" value="good">
                                        <div class="col-4">
                                            <button type="submit" class="btn btn-success">Restore</button>
                                        </div>
                                    @endif
                                </form>

                            </div>
                        </div>

                        @if($user->is_admin != 1)
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">
                                        @if($user->isBanned())
                                            <span class="text-danger">Suspended</span>
                                        @else
                                            <span class="text-success">Active</span>
                                        @endif
                                    </h6>
                                </div>

                                <div class="col-sm-9 text-secondary">
                                    @if($user->isBanned())
                                        @if ($user->banned_at == null)
                                            <span class="text-success">Active</span>
                                        @else
                                            <span class="text-danger"> {{ $user->banned_at->format('F jS Y, g:ia') }}</span>
                                        @endif
                                        <a id="status_user" class="btn btn-success" href="{{ route('admin.unban_user', ['user_id' => $user->id])  }}">Activate </a>
                                    @else
                                        <form class="row row-cols-lg-auto g-3 align-items-center" method="POST" action="{{ route('admin.ban_user')  }}">
                                            @csrf
                                            <input type="hidden" name="user_id" value="{{$user->id}}">
                                            <div class="col-8">
                                                <label class="visually-hidden" for="inlineFormSelectPref">Preference</label>
                                                <select class="form-select" name="length">
                                                    <option selected value=""> Suspend User</option>
                                                    <option value="7">1 Week</option>
                                                    <option value="30">1 Month</option>
                                                    <option value="0">Forever</option>
                                                </select>
                                            </div>

                                            <div class="col-4">
                                                <button type="submit" class="btn btn-danger">Suspend</button>
                                            </div>
                                        </form>
                                    @endif

                                </div>
                            </div>
                        @endif
                    </div>
                </div>

            </div>

            <table id="user_datatable" class="table datatable">
                <thead>
                <tr>
                    <th>ID #</th>
                    <th>Type</th>
                    @admin <th>Reporter</th> @endadmin
                    <th>Created</th>
                    <th>Action</th>
                    <th>Details</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID #</th>
                    <th>Type</th>
                    @admin <th>Reporter</th> @endadmin
                    <th>Created</th>
                    <th>Action</th>
                    <th>Details</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach ($user_list as $violation)
                    <tr>
                        <td class="text-start">{{ $violation->id }}</td>
                        <td class="text-start">{{$violation->violation_type}}</td>
                        @admin <td class="text-start"><a href="{{ route('admin.view_user', ['user_id' => $violation->reportingUser->id]) }}">{{$violation->reportingUser->name}}</a></td> @endadmin
                        <td class="text-start">{{$violation->created_at->format('l F jS Y, g:ia')}}</td>
                        <td class="text-start">{{$violation->action}}</td>
                        <td class="text-start"><a href="{{ route('user.violation_view', ['id' => $violation->id]) }}">View</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
