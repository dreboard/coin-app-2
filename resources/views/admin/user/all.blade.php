@extends('layouts.admin.main')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">User Management</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Management
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('admin.view_users') }}">Baofile</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('admin.view_users') }}">Group Management</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('admin.view_users') }}">Users</a></li>
    </ol>

    <div class="card-body mb-4">
        <div class="btn-group" role="group" aria-label="Basic example">
            <a class="btn btn-danger" href="{{ route('admin.view_banned_users') }}">Suspended Users</a>
            <button type="button" class="btn btn-primary">Send Message</button>
            <button type="button" class="btn btn-warning">Violations</button>
        </div>


        <div class="table-responsive">
            <table class="table table-sm table-borderless table-hover">
                <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col">Today</th>
                    <th scope="col">Month</th>
                    <th scope="col">Year</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">Logins</th>
                    <td>{{ $loginsToday }}</td>
                    <td>{{ $loginsMonth }}</td>
                    <td>{{ $loginsYear }}</td>
                </tr>
                <tr>
                    <th scope="row">Registered</th>
                    <td>{{ $createdToday }}</td>
                    <td>{{ $createdMonth }}</td>
                    <td>{{ $createdYear }}</td>
                </tr>
                <tr>
                    <th scope="row">Groups</th>
                    <td>11</td>
                    <td>111</td>
                    <td>1111</td>
                </tr>
                </tbody>
            </table>
        </div>
        <hr/>


        <table id="user_datatable" class="table datatable">
            <thead>
            <tr>
                <th>ID #</th>
                <th>Name</th>
                <th>Status</th>
                <th>View</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>ID #</th>
                <th>Name</th>
                <th>Status</th>
                <th>View</th>
            </tr>
            </tfoot>
            <tbody>
            @foreach ($users as $user)
                <tr>
                    <td class="text-start">{{ $user->id }}</td>
                    <td class="text-start">{{ $user->name }}</td>
                    <td class="text-start">
                        <span class="">{{ $user->banned_at }}</span>
                        @if ($user->banned_at == null)
                            <span class="text-success">Active</span>
                        @else
                            <span class="text-danger"> Banned {{ $user->banned_at->format('l F jS Y, g:ia') }}</span>
                        @endif
                    </td>
                    <td class="text-end"><a href="{{ route('admin.view_user', ['user_id' => $user->id]) }}">View</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>


    </div>
@endsection
