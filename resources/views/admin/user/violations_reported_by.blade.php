@extends('layouts.admin.main')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <h3 class="mt-4">Violations Reported By {{ $user->name }} </h3>
    <ol class="breadcrumb mb-4">
        @admin
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a> </li>
        <li class="breadcrumb-item active"><a href="{{ route('admin.view_users') }}">Users</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('admin.view_user', ['user_id' => $user->id]) }}">User</a></li>
        @else
            <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
            <li class="breadcrumb-item"><a href="{{ route('user.message_all') }}">Messages</a> </li>
        @endadmin


    </ol>

    <div class="container">
        <div class="main-body">


            <table id="user_datatable" class="table datatable">
                <thead>
                <tr>
                    <th>ID #</th>
                    <th>Type</th>
                    @admin <th>Reporter</th> @endadmin
                    <th>Created</th>
                    <th>Action</th>
                    <th>Details</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID #</th>
                    <th>Type</th>
                    @admin <th>Reporter</th> @endadmin
                    <th>Created</th>
                    <th>Action</th>
                    <th>Details</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach ($user_list as $violation)
                    <tr>
                        <td class="text-start">{{ $violation->id }}</td>
                        <td class="text-start">{{$violation->violation_type}}</td>
                        @admin <td class="text-start"><a href="{{ route('admin.view_user', ['user_id' => $violation->reportingUser->id]) }}">{{$violation->reportingUser->name}}</a></td> @endadmin
                        <td class="text-start">{{$violation->created_at->format('l F jS Y, g:ia')}}</td>
                        <td class="text-start">{{$violation->action}}</td>
                        <td class="text-start"><a href="{{ route('user.violation_view', ['id' => $violation->id]) }}">View</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
