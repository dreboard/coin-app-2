@extends('layouts.admin.main')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <h3 class="mt-4">#{{$user->id}} {{ $user->name }}</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a> </li>
        <li class="breadcrumb-item active"><a href="{{ route('admin.view_users') }}">Users</a></li>
    </ol>

    <div class="container">
        <div class="main-body">

            <div class="row gutters-sm">
                <div class="col-md-4 mb-3">
                    <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                        @if($user->is_admin != 1)
                            <a class="btn btn-outline-primary" href="{{ route('admin.message_user', $user->id) }}" role="button">Message</a>

                            @if (!$user->canBeImpersonated())
                                <a class="btn btn-warning" href="{{ route('admin.clone_user', $user->id) }}" role="button">Clone</a>
                            @endif
                            @canBeImpersonated($user)
                            <a class="btn btn-warning" href="{{ route('admin.clone_user', $user->id) }}" role="button">Clone</a>
                            @endCanBeImpersonated

                            <button id="delete_user" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#deleteUserModal">Delete</button>
                        @endif
                    </div>

                    <div class="modal fade" id="deleteUserModal" tabindex="-1" aria-labelledby="deleteUserModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="deleteUserModalLabel">Delete {{ $user->name }}</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    You are about to delete {{ $user->name }}, and all content.
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                    <a class="btn btn-danger" href="{{ route('admin.delete_user', ['user_id' => $user->id])  }}" role="button">YES</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card mt-3">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                <h6 class="mb-0">Email</h6>
                                <span class="text-secondary">{{ $user->email }}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                <h6 class="mb-0">Logins</h6>
                                <span class="text-secondary"><a href="{{ route('admin.view_user_logins', $user->id) }}" role="button">Logins: {{ $user->logins_count }}</a></span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                <h6 class="mb-0">Bans</h6>
                                <span class="text-secondary">{{ $user->bans_count }}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                <h6 class="mb-0">Warnings</h6>
                                <span class="text-secondary">{{ $user->warnings_count }}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                <h6 class="mb-0">Violations By</h6>
                                <span class="text-secondary"><a href="{{ route('admin.violations_by_user', $user->id) }}" role="button">Violations: {{ $user->violations_count }}</a></span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                <h6 class="mb-0">Violations Against</h6>
                                <span class="text-secondary"><a href="{{ route('admin.violations_against_user', $user->id) }}" role="button">Violations: {{ $reported }} </a></span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card mb-3">
                        <div class="card-body">

                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">Site Status</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    <span class="text-secondary">@if(Cache::has('user-is-online-' . $user->id))
                                            <span class="text-success">Online</span>
                                        @else
                                            <span class="text-danger">Offline</span>
                                        @endif</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">Warning Status</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">

                                        <form class="row row-cols-lg-auto g-3 align-items-center" method="POST" action="{{ route('admin.warn_user')  }}">
                                            @csrf
                                            <input type="hidden" name="user_id" value="{{$user->id}}">

                                            @if($user->status == 'good')
                                                <span class="text-success">Good</span>
                                                <input type="hidden" name="status" value="warn">
                                                <div class="col-4">
                                                    <button type="submit" class="btn btn-warning">Warn</button>
                                                </div>
                                            @else
                                                <span class="text-warning">Warning</span>
                                                <input type="hidden" name="status" value="good">
                                                <div class="col-4">
                                                    <button type="submit" class="btn btn-success">Restore</button>
                                                </div>
                                            @endif
                                        </form>

                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">Last Login</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                @empty($last_login->created_at)
                                        <span class="text-danger">No Login Data</span>
                                    @else
                                        {{$last_login->created_at->format('l jS F Y, g:ia')}}
                                    @endempty
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    @empty($user->email_verified_at)
                                        <span class="text-danger">Unverified</span>
                                    @else
                                        <span class="text-success">Verified</span>
                                    @endempty
                                 </div>
                                <div class="col-sm-9 text-secondary">
                                    @empty($user->email_verified_at)
                                        <form class="row row-cols-lg-auto g-3 align-items-center" method="POST" action="{{ route('admin.verify_user')  }}">
                                            @csrf
                                            <input type="hidden" name="user_id" value="{{$user->id}}">
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-danger">Verify User</button>
                                            </div>
                                        </form>

                                    @else
                                        {{ $user->email_verified_at->format('l jS F Y, g:ia') ?? 'No Data' }}
                                    @endempty

                                </div>
                            </div>

                            @if($user->is_admin != 1)
                                <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">
                                        @if($user->isBanned())
                                            <span class="text-danger">Suspended</span>
                                        @else
                                            <span class="text-success">Active</span>
                                        @endif
                                    </h6>
                                </div>

                                <div class="col-sm-9 text-secondary">
                                    @if($user->isBanned())
                                        @if ($user->banned_at == null)
                                            <span class="text-success">Active</span>
                                        @else
                                            <span class="text-danger"> {{ $user->banned_at->format('F jS Y, g:ia') }}</span>
                                        @endif
                                            <a id="status_user" class="btn btn-success" href="{{ route('admin.unban_user', ['user_id' => $user->id])  }}">Activate </a>
                                    @else
                                        <form class="row row-cols-lg-auto g-3 align-items-center" method="POST" action="{{ route('admin.ban_user')  }}">
                                            @csrf
                                            <input type="hidden" name="user_id" value="{{$user->id}}">
                                            <div class="col-8">
                                                <label class="visually-hidden" for="inlineFormSelectPref">Preference</label>
                                                <select class="form-select" name="length">
                                                    <option selected value=""> Suspend User</option>
                                                    <option value="7">1 Week</option>
                                                    <option value="30">1 Month</option>
                                                    <option value="0">Forever</option>
                                                </select>
                                            </div>

                                            <div class="col-4">
                                                <button type="submit" class="btn btn-danger">Suspend</button>
                                            </div>
                                        </form>
                                    @endif

                                </div>
                            </div>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
            <hr />





        </div>
    </div>
@endsection
