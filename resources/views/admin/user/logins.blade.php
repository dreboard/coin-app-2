@extends('layouts.admin.main')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <h3 class="mt-4">#{{$user->id}} {{ $user->name }}</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a> </li>
        <li class="breadcrumb-item active"><a href="{{ route('admin.view_users') }}">Users</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('admin.view_user', ['user_id' => $user->id]) }}">User</a></li>
    </ol>

    <div class="container">
        <div class="main-body">

            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Status</th>
                    <th scope="col">Logins</th>
                    <th scope="col">Last Login</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>@if(Cache::has('user-is-online-' . $user->id))
                            <span class="text-success">Online</span>
                        @else
                            <span class="text-danger">Offline</span>
                        @endif</td>
                    <td>{{ $user->logins_count }}</td>
                    <td>@empty($last_login->created_at)
                            <span class="text-danger">No Login Data</span>
                        @else
                            {{$last_login->created_at->format('l jS F Y, g:ia')}}
                        @endempty</td>
                </tr>
                </tbody>
            </table>

            <h3>Login History</h3>
            <br />
            <div class="mt-2">
                <table id="user_datatable" class="table datatable mt-2">
                    <thead>
                    <tr>
                        <th>ID #</th>
                        <th>Date</th>
                        <th class="text-end">IP Address</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>ID #</th>
                        <th>Date</th>
                        <th class="text-end">IP Address</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach ($user->logins as $login)
                        <tr>
                            <td class="text-start">{{ $login->id }}</td>
                            <td class="text-start">{{ $login->created_at }}</td>
                            <td class="text-end">{{ $login->ip_address }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection
