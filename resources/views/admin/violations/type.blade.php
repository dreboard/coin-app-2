@extends('layouts.admin.main')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    @push('header')
        <h3 class="mt-4">{{ $violation_type }} Violations</h3>
    @endpush

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('admin.violation_all') }}">All Violations</a></li>
    </ol>

    <div class="card-body">
        <div class="mb-4">
            <a class="btn btn-danger" href="{{ route('admin.view_banned_users') }}">Suspended Users</a>
        </div>

        <hr/>

        <table id="user_datatable" class="table datatable">
            <thead>
            <tr>
                <th>ID #</th>
                <th>Type</th>
                <th>Created</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>ID #</th>
                <th>Type</th>
                <th>Created</th>
                <th>Action</th>
            </tr>
            </tfoot>
            <tbody>
            @forelse ($violations as $violation)
                <tr>
                    <td class="text-start">{{ $violation->id }}</td>
                    <td class="text-start"><a
                            href="{{ route('admin.reported_type', ['violation_type' => $violation->violation_type]) }}">{{$violation->violation_type}}</a>
                    </td>
                    <td class="text-start">{{$violation->created_at->format('l F jS Y, g:ia')}}</td>
                    <td class="text-start"><a
                            href="{{ route('user.violation_view', ['id' => $violation->id]) }}">View</a></td>
                </tr>
            @empty
                <tr>
                    <td class="text-start"></td>
                    <td class="text-start">None Found</td>
                    <td class="text-start"></td>
                    <td class="text-start"></td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection
