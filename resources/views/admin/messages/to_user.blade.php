@extends('layouts.user.main')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')

    <h3 class="mt-4">Send Message to {{$user->name}}</h3>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a> </li>
        <li class="breadcrumb-item"><a href="{{ route('user.message_all') }}">Messages</a> </li>
        <li class="breadcrumb-item"><a href="{{ route('admin.view_user', ['user_id' => $user->id]) }}">User</a> </li>
    </ol>
    <div class="container">
        <div class="main-body">
            <form method="post" action="{{ route('admin.message_user_save') }}">
                @csrf
                <input type="hidden" name="user_id" value="{{$user->id}}">
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Subject</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" name="subject">
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlTextarea1" class="form-label">Example textarea</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" name="body" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>


        </div>
    </div>


@endsection

