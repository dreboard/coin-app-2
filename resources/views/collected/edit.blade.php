@extends('layouts.user.main')
@section('pageTitle', 'Add To Collection')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Add {{ $coin->coinName }} {{ $coin->type->coinType }}</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Collection Pages
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">Bacettings</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.edit_public', ['user' => auth()->user()->id]) }}">Edirofile</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_edit_avatar', ['user' => auth()->user()->id]) }}">Chaage</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('collected.start') }}">Back To Start</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('coin.view_coin', ['coin' => $coin->id]) }}">{{ $coin->coinName }} {{ $coin->type->coinType }}</a>
        </li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <div class="alert alert-primary" role="alert">
                Images, varieties and notes can be added to this coin on the view collected coins page..
            </div>

            <form id="edit_club_form" action="{{ route('collected.save_coin_id') }}"
                  method="post" id="groupForm" enctype="multipart/form-data">
                @csrf
                <div class="col-md-12">
                    <input type="hidden" name="coin_id" value="{{ $coin->id }}">
                    <div class="mb-3">
                        <label for="text" class="form-label">Nickname</label>
                        <input type="text" class="form-control" name="nickname" id="nickname"
                               value="{{ $collected->nickname }}" placeholder="Auto generated if blank">
                    </div>

                    @if($sub_types !== 'None')
                        <div class="mb-3">
                            <label for="sub_type" class="form-label">Variety Type</label>
                            <select class="form-select" name="sub_type" id="sub_type">
                                <option value="Plain" selected>Plain (Regular Issue)</option>
                                @foreach($sub_types as $k => $sub_type)
                                    @if ($sub_type === 'None')
                                        @php
                                            continue;
                                        @endphp
                                    @endif
                                    <option value="{{$sub_type}}">{{$sub_type}}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif

                    @if($circulated == 1)
                        <div class="form-check">
                            <input class="form-check-input" value="1" type="radio" name="circulated" id="circulated1"
                                   @if ($collected->circulated === 1) checked @endif>
                            <label class="form-check-label" for="circulated1">
                                Circulated
                            </label>
                        </div>
                        <div class="form-check mb-3">
                            <input class="form-check-input" value="0"  type="radio" name="circulated" id="circulated2"
                                   @if ($collected->circulated === 0) checked @endif>
                            <label class="form-check-label" for="circulated2">
                                Uncirculated
                            </label>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="grade" class="form-label">Grade</label>
                                <select class="form-select" name="grade" id="grade">
                                    @include($gradeList)
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="tpg_service" class="form-label">Grading Service</label>
                                <select class="form-select" name="tpg_service" id="tpg_service">
                                    <option value="None" @if ($collected->tpg_service === 'None') selected @endif>None</option>
                                    <option value="PCGS" @if ($collected->tpg_service === 'PCGS') selected @endif>PCGS (Professional Coin Grading Service)</option>
                                    <option value="NGC" @if ($collected->tpg_service === 'NGC') selected @endif>NGC (Numismatic Guaranty Corporation of America)</option>
                                    <option value="ANACS" @if ($collected->tpg_service === 'ANACS') selected @endif>ANACS (American Numismatic Association Certification
                                        Service)
                                    </option>
                                    <option value="ICG" @if ($collected->tpg_service === 'IGC') selected @endif>ICG (Independent Coin Grading Company)</option>
                                    <option value="Other" @if ($collected->tpg_service === 'Other') selected @endif>Other</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div id="pro_div" class="row">
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="tpg_serial_num" class="form-label">Serial Number</label>
                                <input type="text" class="form-control" name="tpg_serial_num" id="tpg_serial_num"
                                       value="{{ $collected->tpg_serial_num ?? '' }}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="slab_condition" class="form-label">Slab Condition</label>
                                <select class="form-select" name="slab_condition" id="slab_condition">
                                    <option  @if ($collected->slab_condition === 'None') selected @endif>None</option>
                                    <option value="Excellent" @if ($collected->slab_condition === 'Excellent') selected @endif>Excellent</option>
                                    <option value="Scratched Heavy" @if ($collected->slab_condition === 'Scratched Heavy') selected @endif>Scratched Heavy</option>
                                    <option value="Scratched Light" @if ($collected->slab_condition === 'Scratched Light') selected @endif>Scratched Light</option>
                                    <option value="Cracked" @if ($collected->slab_condition === 'Cracked') selected @endif>Cracked</option>
                                    <option value="Cracked Severe" @if ($collected->slab_condition === 'Cracked Severe') selected @endif>Cracked Severe</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="card mb-3 alert alert-primary" id="pcgs_div">
                        <div class="card-body">
                            <h5 class="card-title">PCGS Specific</h5>
                            <div id="" class="row">
                                <div class="col-sm-6">

                                    <div class="mb-3">
                                        <label for="slab_generation" class="form-label">
                                            Slab Generation <a class="external_link"
                                                               href="https://www.pcgs.com/holdermuseum" target="_blank">View</a>
                                        </label>
                                        <select class="form-select" name="slab_generation" id="pcgs_slab_generation">
                                            <option selected>None</option>
                                            @if($coin->coinYear < 1987)
                                                <option value="Generation 1.0" @if ($collected->slab_generation === 'Generation 1.0') selected @endif>Generation 1.0</option>
                                                <option value="Generation 1.1" @if ($collected->slab_generation === 'Generation 1.1') selected @endif>Generation 1.1</option>
                                            @endif

                                            @if($coin->coinYear < 1989)
                                                <option value="Generation 1.2" @if ($collected->slab_generation === 'Generation 1.2') selected @endif>Generation 1.2</option>
                                                <option value="Generation 2.0" @if ($collected->slab_generation === 'Generation 2.0') selected @endif>Generation 2.0</option>
                                            @endif

                                            @if($coin->coinYear < 1987)

                                            @endif

                                            <option value="Generation 2.1A" @if ($collected->slab_generation === 'Generation 2.1A') selected @endif>Generation 2.1A</option>
                                            <option value="Generation 2.1B" @if ($collected->slab_generation === 'Generation 2.1B') selected @endif>Generation 2.1B</option>
                                            <option value="Generation 2.2" @if ($collected->slab_generation === 'Generation 2.2') selected @endif>Generation 2.2</option>
                                            <option value="Generation 3.0" @if ($collected->slab_generation === 'Generation 3.0') selected @endif>Generation 3.0</option>
                                            <option value="Generation 3.1" @if ($collected->slab_generation === 'Generation 3.1') selected @endif>Generation 3.1</option>
                                            <option value="Generation 4.0A" @if ($collected->slab_generation === 'Generation ') selected @endif>Generation 4.0A</option>
                                            <option value="Generation 4.0A" @if ($collected->slab_generation === 'Generation ') selected @endif>Generation 4.0A</option>
                                            <option value="Generation 4.0B" @if ($collected->slab_generation === 'Generation ') selected @endif>Generation 4.0B</option>
                                            <option value="Generation 4.1" @if ($collected->slab_generation === 'Generation ') selected @endif>Generation 4.1</option>
                                            <option value="Generation 4.2" @if ($collected->slab_generation === 'Generation ') selected @endif>Generation 4.2</option>
                                            <option value="Generation 4.3" @if ($collected->slab_generation === 'Generation ') selected @endif>Generation 4.3</option>
                                            <option value="Generation 4.4" @if ($collected->slab_generation === 'Generation ') selected @endif>Generation 4.4</option>
                                            <option value="Generation 4.4S" @if ($collected->slab_generation === 'Generation ') selected @endif>Generation 4.4S</option>
                                            <option value="Generation 4.5" @if ($collected->slab_generation === 'Generation ') selected @endif>Generation 4.5</option>
                                            <option value="Generation 4.5S" @if ($collected->slab_generation === 'Generation ') selected @endif>Generation 4.5S</option>
                                            <option value="Generation 4.6" @if ($collected->slab_generation === 'Generation ') selected @endif>Generation 4.6</option>
                                            <option value="Generation 4.6S" @if ($collected->slab_generation === 'Generation ') selected @endif>Generation 4.6S</option>
                                            <option value="Generation 5.0" @if ($collected->slab_generation === 'Generation ') selected @endif>Generation 5.0</option>
                                            <option value="Generation 5.0S" @if ($collected->slab_generation === 'Generation ') selected @endif>Generation 5.0S</option>
                                            <option value="Generation 6.0" @if ($collected->slab_generation === 'Generation ') selected @endif>Generation 6.0</option>
                                            <option value="Generation 6.0S" @if ($collected->slab_generation === 'Generation ') selected @endif>Generation 6.0S</option>
                                            <option value="Generation 6.0S" @if ($collected->slab_generation === 'Generation ') selected @endif>Generation 6.0S T2</option>
                                            <option value="Generation 6.0S" @if ($collected->slab_generation === 'Generation ') selected @endif>Generation 6.0S T3</option>
                                            <option value="Generation 6.1" @if ($collected->slab_generation === 'Generation ') selected @endif>Generation 6.1</option>
                                            <option value="Generation 6.1S" @if ($collected->slab_generation === 'Generation ') selected @endif>Generation 6.1S</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="mb-3">
                                        <label for="pcgs_special_label" class="form-label">Special Labels</label>
                                        <select class="form-select" name="pcgs_special_label" id="pcgs_special_label">
                                            <option selected>No</option>
                                            <option value="U.S. Air Force Label">U.S. Air Force Label</option>
                                            <option value="U.S. Marine Corps Label">U.S. Marine Corps Label</option>
                                            <option value="First Strike Flag Label">First Strike Flag Label</option>

                                            <option value="Signature Label - Stephanie Sabin">Signature Label -
                                                Stephanie Sabin
                                            </option>

                                            @if($coin->mintMark == 'D')
                                                <option value="Mint Specific Label - Denver">Mint Specific Label -
                                                    Denver
                                                </option>
                                            @endif
                                            @if($coin->mintMark == 'S')
                                                <option value="Mint Specific Label - San Francisco">Mint Specific Label
                                                    - San Francisco
                                                </option>
                                            @endif
                                            @if($coin->mintMark == 'P')
                                                <option value="Mint Specific Label - Philadelphia">Mint Specific Label -
                                                    Philadelphia
                                                </option>
                                            @endif
                                            @if($coin->mintMark == 'W')
                                                <option value="Mint Specific Label - West Point">Mint Specific Label -
                                                    West Point
                                                </option>
                                            @endif

                                            @if($coin->type->coinType == 'Morgan Dollar')
                                                <option value="Morgan Dollar 100th Anniversary">Morgan Dollar 100th
                                                    Anniversary
                                                </option>
                                            @endif
                                            @if($coin->type->coinType == 'Peace Dollar')
                                                <option value="Peace Dollar 100th Anniversary">Peace Dollar 100th
                                                    Anniversary
                                                </option>
                                            @endif


                                            <option value="PCGS 35th Anniversary">PCGS 35th Anniversary</option>

                                            @if($coin->id == 7953)
                                                <option value="Naismith Memorial Basketball Hall of Fame">Naismith
                                                    Memorial Basketball Hall of Fame
                                                </option>
                                            @endif

                                            {{--   2020-W America the Beautiful Quarters in addition to 2020 Silver & Gold Eagles--}}
                                            @if($coin->coinYear == 2020 & $coin->commemorativeType == 'American Eagle' && in_array($coin->coinMetal, ['Silver', 'Gold']) || $coin->coinYear == 2020 &  $coin->type->coinType == 'America the Beautiful Quarter')
                                                <option value="Victory 75 V75">Victory 75 V75</option>
                                            @endif


                                            @if($coin->commemorativeType == 'American Eagle' && in_array($coin->coinMetal, ['Silver', 'Gold', 'Platinum', 'Palladium']))
                                                <option value="Ultraviolet Eagle Label">Ultraviolet Eagle Label</option>
                                            @endif
                                            @if($coin->type->coinType == 'Innovation Dollar')
                                                <option value="Peacnniversary">Innovation Dollar</option>
                                            @endif
                                            @if($coin->mintMark == 'W')
                                                <option value="West Point">West Point</option>
                                            @endif
                                            @if($coin->commemorativeType == 'Apollo 11 50th Anniversary')
                                                <option value="2019 Apollo 11 50th Anniversary">2019 Apollo 11 50th
                                                    Anniversary
                                                </option>
                                                <option value="2019 Apollo 11 50th Anniversary First Strike">2019 Apollo
                                                    11 50th Anniversary First Strike
                                                </option>
                                            @endif
                                            @if($coin->commemorativeType == 'National Baseball Hall of Fame')
                                                <option value="2014 National Baseball Hall of Fame">2014 National
                                                    Baseball Hall of Fame
                                                </option>
                                            @endif
                                            @if($coin->commemorativeType == 'US Marshals Service')
                                                <option value="2015 U.S. Marshals Service">2015 U.S. Marshals Service
                                                </option>
                                            @endif
                                            @if($coin->id === 7473)
                                                <option value="2015 American Liberty">2015 American Liberty</option>
                                            @endif

                                            <option value="PCGS 30th Anniversary">PCGS 30th Anniversary</option>

                                            @if($coin->strike == 'Enhanced Uncirculated' && $coin->coinYear == 2017)
                                                <option value="225th Anniversary Enhanced Uncirculated Coin Set">225th
                                                    Anniversary Enhanced Uncirculated Coin Set
                                                </option>
                                            @endif
                                            @if($coin->id === 7418)
                                                <option value="2018-W American Eagle One Ounce Silver Proof Coin Label">
                                                    2018-W American Eagle One Ounce Silver Proof Coin Label
                                                </option>
                                            @endif
                                            @if($coin->commemorativeType == 'Breast Cancer Awareness')
                                                <option
                                                    value="Breast Cancer Research Foundation Special Label and Pink Gasket">
                                                    Breast Cancer Research Foundation Special Label and Pink Gasket
                                                </option>
                                            @endif
                                            @if($coin->id === 7421)
                                                <option
                                                    value="2018-S American Eagle One Ounce Silver Proof Coin – 2018 Philadelphia ANA">
                                                    2018-S American Eagle One Ounce Silver Proof Coin – 2018
                                                    Philadelphia ANA
                                                </option>
                                            @endif

                                            <option value="September 2019 Long Beach Expo">September 2019 Long Beach
                                                Expo
                                            </option>

                                            @if($coin->id == 7886)
                                                {{--2020-W Proof Silver Eagle--}}
                                                <option value="January 2020 FUN Convention">January 2020 FUN
                                                    Convention
                                                </option>
                                            @endif

                                            {{--2020 5 Quarter Silver Proof Set, 2020-W Proof & Uncirculated Silver Eagle--}}
                                            @if($coin->commemorativeType == 'American Eagle' && in_array($coin->coinMetal, ['Silver', 'Gold', 'Platinum', 'Palladium']))
                                                <option value="February 2020 Long Beach Expo">February 2020 Long Beach
                                                    Expo
                                                </option>
                                            @endif

                                            @if($coin->commemorativeType == 'Mark Twain')
                                                <option value="2016 Mark Twain">2016 Mark Twain</option>
                                            @endif

                                            @if($coin->commemorativeType == 'National Park Service Centennial')
                                                <option value="2016 National Park Service">2016 National Park Service
                                                </option>
                                            @endif
                                            @if($coin->id == 7511)
                                                <option value="2016 American Buffalo">2016 American Buffalo</option>
                                            @endif
                                            @if($coin->id == 7513)
                                                <option value="30th Anniversary of the American Eagle Program">30th
                                                    Anniversary of the American Eagle Program
                                                </option>
                                            @endif

                                            @if($coin->commemorativeType == '2016 Centennial Series')
                                                <option value="100th Anniversary Gold Coins">100th Anniversary Gold
                                                    Coins
                                                </option>
                                            @endif
                                            @if($coin->commemorativeType == 'American Eagle' && in_array($coin->coinMetal, ['Silver', 'Gold']) && $coin->coinYear == 2017)
                                                <option value="225th Anniversary of the United States Mint">225th
                                                    Anniversary of the United States Mint
                                                </option>
                                            @endif
                                            @if($coin->commemorativeType == 'World War I Centennial')
                                                <option value="2018 World War I Centennial">2018 World War I
                                                    Centennial
                                                </option>
                                            @endif
                                            @if($coin->id == 7826)
                                                <option value="Pride of Two Nations First Strike">Pride of Two Nations
                                                    First Strike
                                                </option>
                                            @endif

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card mb-3 alert alert-primary" id="ngc_div">
                        <div class="card-body">
                            <h5 class="card-title">NGC Specific</h5>
                            <div class="ngc_specific row">
                                <div class="col-sm-6">
                                    <div class="mb-3">
                                        <label for="strike_character" class="form-label">Strike Character (NGC)</label>
                                        <select class="form-select" name="strike_character" id="ngc_strike_character">

                                            <option @isset($collected->strike_character) selected @endif>{{ $collected->strike_character }}</option>
                                            @if($coin->coinMetal == 'Copper')
                                                <optgroup label="Brown">
                                                    <option value="BN BRILLIANT">Brown, Brilliant (BN BRILLIANT)</option>
                                                    <option value="BN CAMEO">Brown, Cameo (BN CAMEO)</option>
                                                    <option value="BN DPL">Brown, Deep Prooflike (BN DPL)</option>
                                                    <option value="BN MATTE">Brown, Matte (BN MATTE)</option>
                                                    <option value="BN PL">Brown, Prooflike (BN PL)</option>
                                                    <option value="BN SATIN">Brown, Satin (BN SATIN)</option>
                                                    <option value="BN ULTRA CAMEO">Brown, Ultra Cameo (BN ULTRA CAMEO)
                                                    </option>
                                                </optgroup>
                                                <optgroup label="Red Brown">
                                                    <option value="RB BRILLIANT">Red Brown, Brilliant (RB BRILLIANT)
                                                    </option>
                                                    <option value="RB CAMEO">Red Brown, Cameo (RB CAMEO)</option>
                                                    <option value="RB DPL">Red Brown, Deep Prooflike (RB DPL)</option>
                                                    <option value="RB MATTE">Red Brown, Matte (RB MATTE)</option>
                                                    <option value="RB PL">Red Brown, Prooflike (RB PL)</option>
                                                    <option value="RB SATIN">Red Brown, Satin (RB SATIN)</option>
                                                    <option value="RB ULTRA CAMEO">Red Brown, Ultra Cameo (RB ULTRA
                                                        CAMEO)
                                                    </option>
                                                </optgroup>
                                                <optgroup label="Red">
                                                    <option value="RD BRILLIANT">Red, Brilliant (RD BRILLIANT)</option>
                                                    <option value="RD CAMEO">Red, Cameo (RD CAMEO)</option>
                                                    <option value="RD DPL">Red, Deep Prooflike (RD DPL)</option>
                                                    <option value="RD MATTE">Red, Matte (RD MATTE)</option>
                                                    <option value="RD PL">Red, Prooflike (RD PL)</option>
                                                    <option value="RD SATIN">Red, Satin (RD SATIN)</option>
                                                    <option value="RD ULTRA CAMEO">Red, Ultra Cameo (RD ULTRA CAMEO)
                                                    </option>
                                                </optgroup>

                                            @endif
                                            @if($coin->type->coinType == 'Jefferson Nickel')
                                                <optgroup label="Full Steps">
                                                    <option value="5FS DPL">Five Full Steps, Deep Prooflike</option>
                                                    <option value="5FS">Five Full Steps</option>
                                                    <option value="Five Full Steps, Prooflike">Five Full Steps,
                                                        Prooflike
                                                    </option>
                                                    <option value="6FS DPL">Six Full Steps, Deep Prooflike</option>
                                                    <option value="6FS">Six Full Steps</option>
                                                    <option value="6FS PL">Six Full Steps, Prooflike</option>
                                                </optgroup>
                                            @endif
                                            @if($coin->type->coinType == 'Presidential Dollar')
                                                <optgroup label="Presidential Dollar First Day">
                                                    <option value="DFM">Deep Prooflike, First Day of Mintage (DFM)
                                                    </option>
                                                    <option value="FD">First Day of Issue (FD)</option>
                                                    <option value="FDD">Deep Prooflike, First Day of Issue (FDD)
                                                    </option>
                                                    <option value="FDP">Prooflike, First Day of Issue (FDP)</option>
                                                </optgroup>
                                            @endif

                                            @if($coin->type->coinType == 'Standing Liberty')
                                                <option value="FH">Full Head (FH)</option>
                                                <option value="FH PL">Full Head, Prooflike (FHP)</option>
                                                <option value="FH DPL">Full Head, Deep Prooflike (FB DPL)</option>
                                            @endif

                                            @if($coin->type->coinType == 'Mercury Dime')
                                                <option value="FB">Full Band (FB)</option>
                                                <option value="FB PL">Full Bands, Prooflike (FBP)</option>
                                                <option value="FB DPL">Full Bands, Deep Prooflike (FB)</option>
                                            @endif

                                            @if($coin->type->coinType == 'Franklin Half Dollar')
                                                <option value="FBL">Full Bell Lines</option>
                                                <option value="FB PL">Full Bands, Prooflike (FB PL)</option>
                                                <option value="FBL DPL">Full Bell Lines, Deep Prooflike (FBL DPL)
                                                </option>
                                            @endif

                                            @if($coin->type->coinType == 'Roosevelt Dime')
                                                <option value="FB">Full Band</option>
                                                <option value="FT">Full Torch</option>
                                            @endif

                                            @if($proof == 1)
                                                <optgroup label="Proof Designations">
                                                    @if(in_array($coin->strike, ['Reverse Proof', 'Enhanced Reverse Proof']))
                                                        <option value="ENHANCED REV PF">Enhanced Finish, Reverse Proof
                                                            (ENHANCED REV PF)
                                                        </option>
                                                    @endif
                                                </optgroup>
                                            @endif
                                            @if($coin->strike == 'Enhanced Uncirculated')
                                                <optgroup label="Enhanced Uncirculated">
                                                    <option value="ENHANCED FINISH">Enhanced Finish (ENHANCED FINISH)
                                                    </option>
                                                    <option value="DPL ENHANCED FINISH">Enhanced Finish, Deep Prooflike
                                                        (DPL ENHANCED FINISH)
                                                    </option>
                                                    <option value="PL ENHANCED FINISH">Enhanced Finish, Prooflike (PL
                                                        ENHANCED FINISH)
                                                    </option>
                                                    <option value="UC ENHANCED FINISH">Enhanced Finish, Ultra Cameo (UC
                                                        ENHANCED FINISH)
                                                    </option>
                                                </optgroup>
                                            @endif
                                            <option value="CA">Cameo (CA)</option>
                                            <option value="UC">Ultra Cameo (UC)</option>
                                            <option value="PL">Prooflike (PL)</option>
                                            <option value="CA">Deep Prooflike (DPL)</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="mb-3">
                                        <label for="release" class="form-label">Releases Designation (NGC)</label>
                                        <select class="form-select" name="release" id="release">
                                            <option selected>None</option>
                                            <option value="Early Releases and First Releases">Early Releases and First
                                                Releases
                                            </option>
                                            <option value="First Day of Issue">First Day of Issue</option>
                                            <option value="First Day of Release">First Day of Release</option>
                                            <option value="First Day of Production">First Day of Production</option>
                                            <option value="Last Day of Production">Last Day of Production</option>
                                            <option value="Early Production">Early Production</option>
                                            <option value="First Allocation">First Allocation</option>
                                            <option value="Final T-1 Production and First T-2 Production">Final T-1
                                                Production and First T-2 Production
                                            </option>
                                            <option value="Advance Release">Advance Release</option>
                                            <option value="Show Release">Show Release</option>
                                            <option value="Numbered First Struck Edition">Numbered First Struck
                                                Edition
                                            </option>
                                            <option value="Individually Numbered Strikes">Individually Numbered
                                                Strikes
                                            </option>
                                            <option value="First Year of Issue">First Year of Issue</option>
                                            <option value="Last Year of Issue">Last Year of Issue</option>
                                            <option value="First Day Ceremony">First Day Ceremony</option>
                                            <option value="First Strike Ceremony">First Strike Ceremony</option>
                                            <option value="First Strike">First Strike</option>
                                            <option value="Limited Edition Special Label">Limited Edition Special
                                                Label
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="mb-3">
                                        <label for="ngc_special_label" class="form-label">Special Labels</label>
                                        <select class="form-select" name="ngc_special_label" id="ngc_special_label">
                                            <option value="None" selected>No</option>
                                            <option value="ULabel">ULabel</option>


                                            {{--2020 5 Quarter Silver Proof Set, 2020-W Proof & Uncirculated Silver Eagle--}}
                                            @if($coin->coinType == 'American Innovation Dollar')
                                                <option value="American Innovation Series">American Innovation Series
                                                </option>
                                            @endif
                                            @if($coin->coinType == 'America the Beautiful Quarter' || $coin->commemorativeType = 'National Park Service Centennial')
                                                <option value="America's National Treasures">America's National
                                                    Treasures
                                                </option>
                                            @endif

                                            @if($coin->commemorativeType == 'American Eagle')
                                                <option value="American Liberty Series">American Liberty Series</option>
                                            @endif

                                            @if($coin->commemorativeVersion == 'Silver American Eagle')
                                                <option value="American Liberty Series">American Liberty Series Silver
                                                    Eagle #432
                                                </option>
                                            @endif
                                            @if($coin->commemorativeVersion == 'Gold American Eagle')
                                                <option value="American Liberty Series">American Liberty Series Gold
                                                    Eagle #399
                                                </option>
                                            @endif
                                            @if($coin->commemorativeVersion == 'Platinum American Eagle')
                                                <option value="American Liberty Series">American Liberty Series Platinum
                                                    Eagle #538
                                                </option>
                                            @endif
                                            @if($coin->commemorativeVersion == 'American Palladium Eagle')
                                                <option value="American Liberty Series">American Liberty Series
                                                    Palladium Eagle #923
                                                </option>
                                            @endif
                                            @if($coin->commemorativeType == 'American Buffalo')
                                                <option value="American Liberty Series">Gold Buffalo #549</option>
                                            @endif

                                            @if($coin->designType == 'Hot Springs National Park')
                                                <option value="Arkansas - Hot Springs (2010) #13">Arkansas - Hot Springs
                                                    (2010) #13
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Mount Hood National Park')
                                                <option value="Oregon - Mt. Hood (2010) #17">Oregon - Mt. Hood (2010)
                                                    #17
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Yellowstone National Park')
                                                <option value="Wyoming - Yellowstone (2010) #25">Wyoming - Yellowstone
                                                    (2010) #25
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="California - Yosemite (2010) #26">California - Yosemite
                                                    (2010) #26
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Oklahoma - Chickasaw (2011) #8">Oklahoma - Chickasaw
                                                    (2011) #8
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Montana - Glacier (2011) #10">Montana - Glacier (2011)
                                                    #10
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Washington - Olympic (2011) #21">Washington - Olympic
                                                    (2011) #21
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Mississippi - Vicksburg (2011) #24">Mississippi -
                                                    Vicksburg (2011) #24
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Pennsylvania - Gettysburg (2011) #82">Pennsylvania -
                                                    Gettysburg (2011) #82
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Alaska - Denali (2012) #7">Alaska - Denali (2012) #7
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Hawaii - Volcanoes (2012) #12">Hawaii - Volcanoes (2012)
                                                    #12
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Maine - Acadia (2012) #14">Maine - Acadia (2012) #14
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="New Mexico - Chaco Culture (2012) #16">New Mexico - Chaco
                                                    Culture (2012) #16
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Puerto Rico - El Yunque (2012) #22">Puerto Rico - El
                                                    Yunque (2012) #22
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Maryland - Fort McHenry (2013) #15">Maryland - Fort
                                                    McHenry (2013) #15
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="New Hampshire - White Mountain (2013) #18">New Hampshire
                                                    - White Mountain (2013) #18
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Nevada - Great Basin (2013) #19">Nevada - Great Basin
                                                    (2013) #19
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Ohio - Peace Memorial (2013) #20">Ohio - Peace Memorial
                                                    (2013) #20
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="South Dakota - Mount Rushmore (2013) #259">South Dakota -
                                                    Mount Rushmore (2013) #259
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Virginia - Shenandoah (2014) #249">Virginia - Shenandoah
                                                    (2014) #249
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Florida - Everglades (2014) #250">Florida - Everglades
                                                    (2014) #250
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Utah - Arches (2014) #251">Utah - Arches (2014) #251
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Colorado - Sand Dunes (2014) #252">Colorado - Sand Dunes
                                                    (2014) #252
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Tennessee - Smoky Mountains (2014) #253">Tennessee -
                                                    Smoky Mountains (2014) #253
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Louisiana - Kisatchie (2015) #385">Louisiana - Kisatchie
                                                    (2015) #385
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="North Carolina - Blue Ridge (2015) #386">North Carolina -
                                                    Blue Ridge (2015) #386
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="New York - Saratoga (2015) #387">New York - Saratoga
                                                    (2015) #387
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Delaware - Bombay Hook (2015) #388">Delaware - Bombay
                                                    Hook (2015) #388
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Nebraska - Homestead (2015) #389">Nebraska - Homestead
                                                    (2015) #389
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Illinois - Shawnee (2016) #539">Illinois - Shawnee (2016)
                                                    #539
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Kentucky - Cumberland Gap (2016) #540">Kentucky -
                                                    Cumberland Gap (2016) #540
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="West Virginia - Harper's Ferry (2016) #541">West Virginia
                                                    - Harper's Ferry (2016) #541
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="North Dakota - Theodore Roosevelt (2016) #542">North
                                                    Dakota - Theodore Roosevelt (2016) #542
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="South Carolina - Fort Moultrie (2016) #543">South
                                                    Carolina - Fort Moultrie (2016) #543
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Iowa - Effigy Mounds (2017) #775">Iowa - Effigy Mounds
                                                    (2017) #775
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="District of Columbia - Frederick Douglass (2017) #776">
                                                    District of Columbia - Frederick Douglass (2017) #776
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Missouri - Ozark Riverways (2017) #777">Missouri - Ozark
                                                    Riverways (2017) #777
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="New Jersey - Ellis Island (2017) #778">New Jersey - Ellis
                                                    Island (2017) #778
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Indiana - George Rogers Clark (2017) #779">Indiana -
                                                    George Rogers Clark (2017) #779
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Michigan - Pictured Rocks (2018) #941">Michigan -
                                                    Pictured Rocks (2018) #941
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Wisconsin - Apostle Islands (2018) #942">Wisconsin -
                                                    Apostle Islands (2018) #942
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Minnesota - Voyageurs (2018) #943">Minnesota - Voyageurs
                                                    (2018) #943
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Georgia - Cumberland Island (2018) #944">Georgia -
                                                    Cumberland Island (2018) #944
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Rhode Island - Block Island (2018) #945">Rhode Island -
                                                    Block Island (2018) #945
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Massachusetts - Lowell NHP (2019) #1131">Massachusetts -
                                                    Lowell NHP (2019) #1131
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option
                                                    value="N. Mariana Islands - American Memorial Park (2019) #1132">N.
                                                    Mariana Islands - American Memorial Park (2019) #1132
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Guam - War in the Pacific (2019) #1133">Guam - War in the
                                                    Pacific (2019) #1133
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Texas - San Antonio Missions (2019) #1134">Texas - San
                                                    Antonio Missions (2019) #1134
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Idaho - River of No Return (2019) #1135">Idaho - River of
                                                    No Return (2019) #1135
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="American Samoa - American Samoa (2020) #1336">American
                                                    Samoa - American Samoa (2020) #1336
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Connecticut - Weir Farm (2020) #1337">Connecticut - Weir
                                                    Farm (2020) #1337
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="US Virgin Islands - Salt River Bay (2020) #1338">US
                                                    Virgin Islands - Salt River Bay (2020) #1338
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Vermont - Marsh Billings Rockefeller (2020) #1339">
                                                    Vermont - Marsh Billings Rockefeller (2020) #1339
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Kansas - Tall Prairie NP (2020) #1340">Kansas - Tall
                                                    Prairie NP (2020) #1340
                                                </option>
                                            @endif
                                            @if($coin->designType == 'Ameriarter')
                                                <option value="Alabama - Tuskegee Airman NHP (2021) #1341">Alabama -
                                                    Tuskegee Airman NHP (2021) #1341
                                                </option>
                                            @endif









<!--
@todo add PRESIDENTS -> https://www.ngccoin.com/coin-grading/labels/special-labels/american-leaders/
@todo EISENHOWER DOLLAR DIE STATES https://www.ikegroup.info/?page_id=950


-->


                                            @if($coin->coinType == 'Ameriarter')
                                                <option value="Amerures">Ameriasures</option>
                                            @endif

                                            @if(in_array($coin->coinType, ['Silver', 'Gold']))
                                                <option value="American Leaders">American Leaders</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="mb-3">
                                        <label for="slab_generation" class="form-label">
                                            Slab Generation <a class="external_link"
                                                               href="https://www.oldslabholders.com/post/ngc-slab-generations"
                                                               target="_blank">View</a>
                                        </label>
                                        <select class="form-select" name="slab_generation" id="ngc_slab_generation">
                                            <option selected>None</option>
                                            <option value="Generation 1">Generation 1</option>
                                            <option value="Generation 2">Generation 2</option>
                                            <option value="Generation 2.1">Generation 2.1</option>
                                            <option value="Generation 3">Generation 3</option>
                                            <option value="Generation 4">Generation 4</option>
                                            <option value="Generation 5">Generation 5</option>
                                            <option value="Generation 6">Generation 6</option>
                                            <option value="Generation 7">Generation 7</option>
                                            <option value="Generation 8">Generation 8</option>
                                            <option value="Generation 8.1">Generation 8.1</option>
                                            <option value="Generation 9">Generation 9</option>
                                            <option value="Generation 10">Generation 10</option>
                                            <option value="Generation 11">Generation 11</option>
                                            <option value="Generation 12">Generation 12</option>
                                            <option value="Generation 13">Generation 13</option>
                                            <option value="Generation 13.5">Generation 13.5</option>
                                            <option value="Generation 14">Generation 14</option>
                                            <option value="Generation 14.1">Generation 14.1</option>
                                            <option value="Generation 15">Generation 15</option>
                                            <option value="Generation 16">Generation 16</option>
                                            <option value="Generation 17">Generation 17</option>

                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    {{--ANACS--}}
                    <div class="card mb-3 alert alert-primary" id="anacs_div">
                        <div class="card-body">
                            <h5 class="card-title">ANACS Specific</h5>
                            <div class="anacs_specific row">
                                <div class="col-sm-6">
                                    <div class="mb-3">
                                        <label for="slab_generation" class="form-label">
                                            Slab Generation <a class="external_link"
                                                               href="https://www.pcgs.com/holdermuseum" target="_blank">View</a>
                                        </label>
                                        {{-- https://docs.google.com/presentation/d/1Y_6cj6X2v_ZbO4NrqlRLysVeJO1IgoqnAA51ErVhLf8/edit#slide=id.g13ceb62592e_1_497--}}
                                        <select class="form-select" name="slab_generation" id="anacs_slab_generation">
                                            <option selected>None</option>
                                            <optgroup label="ANACS Slabs">
                                                <option value="Generation 1">Generation 1</option>
                                                <option value="Generation 2">Generation 2</option>
                                                <option value="Generation 3">Generation 3</option>
                                                <option value="Generation 4">Generation 4</option>
                                                <option value="Generation 5">Generation 5</option>
                                                <option value="Generation 6">Generation 6</option>
                                                <option value="Generation 7">Generation 7</option>
                                                <option value="Generation 7.1">Generation 7.1</option>
                                                <option value="Generation 7.2">Generation 7.2</option>
                                                <option value="Generation 8">Generation 8</option>
                                                <option value="Generation 8.1">Generation 8.1</option>
                                                <option value="Generation 8.2">Generation 8.2</option>
                                                <option value="Generation 9">Generation 9</option>
                                                <option value="Generation 10">Generation 10</option>
                                            </optgroup>
                                            <optgroup label="ANACS Photo Certificates">
                                                <option value="Photo Certificate TYPE 1">Photo Certificate TYPE 1
                                                </option>
                                                <option value="Photo Certificate TYPE 2">Photo Certificate TYPE 2
                                                </option>
                                                <option value="Photo Certificate TYPE 3">Photo Certificate TYPE 3
                                                </option>
                                                <option value="Photo Certificate TYPE 3.1">Photo Certificate TYPE 3.1
                                                </option>
                                                <option value="Photo Certificate TYPE 4">Photo Certificate TYPE 4
                                                </option>
                                                <option value="Photo Certificate TYPE 4.1">Photo Certificate TYPE 4.1
                                                </option>
                                                <option value="Photo Certificate TYPE 5">Photo Certificate TYPE 5
                                                </option>
                                                <option value="Photo Certificate TYPE 6">Photo Certificate TYPE 6
                                                </option>
                                                <option value="Photo Certificate TYPE 7">Photo Certificate TYPE 7
                                                </option>
                                                <option value="Photo Certificate TYPE 8">Photo Certificate TYPE 8
                                                </option>
                                                <option value="Photo Certificate TYPE 9">Photo Certificate TYPE 9
                                                </option>
                                                <option value="Photo Certificate TYPE 10">Photo Certificate TYPE 10
                                                </option>
                                            </optgroup>

                                        </select>
                                    </div>
                                </div>


                                <div class="col-sm-6">
                                    <div class="mb-3">
                                        <label for="strike_character" class="form-label">Strike Character (NGC)</label>
                                        <select class="form-select" name="strike_character" id="anacs_strike_character">
                                            <option selected>None</option>
                                            @if($coin->coinMetal == 'Copper')
                                                <option value="RED">Red (RED)</option>
                                                <option value="RB">Red Brown (RB)</option>
                                                <option value="BRN">Brown (BRN)</option>
                                            @endif
                                            @if($coin->type->coinType == 'Jefferson Nickel' && $coin->strike = 'Business')
                                                <option value="5 STEPS">Five Steps</option>
                                                <option value="5.5 STEPS">Five and One Half Steps</option>
                                                <option value="6 STEPS">Six Steps</option>
                                            @endif

                                            @if($coin->type->coinType == 'Standing Liberty')
                                                <option value="FH">Full Head (FH)</option>
                                            @endif

                                            @if($coin->type->coinType == 'Mercury Dime' || $coin->type->coinType == 'Roosevelt Dime')
                                                <option value="FSB">Full Split Bands</option>
                                            @endif

                                            @if($coin->type->coinType == 'Franklin Half Dollar' && $coin->strike = 'Business')
                                                <option value="FBL">Full Bell Lines</option>
                                            @endif

                                            @if($proof == 1 || $coin->strike = 'Special Mint')
                                                <option value="CAMEO">Cameo Contrast (CAMEO)</option>
                                                <option value="DCAM">Heavy Cameo Contrast (DCAM)</option>
                                            @endif

                                            @if($proof == 0 && $coin->strike = 'Business')
                                                <option value="PL">Proof-Like (PL)</option>
                                                <option value="DMPL">Deep Mirror Proof-Like (DMPL)</option>
                                                <option value="UDM">Ultra Deep Mirror Proof-Like (UDM)</option>
                                            @endif

                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="mb-3">
                                        <label for="strike_character" class="form-label">
                                            Other Grade Designations <a href="https://anacs.com/faqs/"
                                                                        class="external_link" target="_blank">ANACS
                                                FAQ</a>
                                        </label>
                                        <select class="form-select" name="anacs_grade_designator"
                                                id="anacs_grade_designator">
                                            <option selected>None</option>
                                            <option value="GH (Genuine)">GH (Genuine)</option>
                                            <option value="V (PVC)">V (PVC)</option>
                                            <option value="N8 (Non-eligible)">N8 (Non-eligible)</option>
                                            <option value="N9">N9 (Altered coin, not genuine, questionable
                                                authenticity)
                                            </option>
                                        </select>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    {{--ANACS--}}

                    <div class="row">
                        @if(in_array($coin->strike, array_merge(Config::get('constants.coins.business_strikes'), Config::get('constants.coins.special_strikes')))  == 'Lincoheat')
                            <div class="col-sm-6">
                                <div class="mb-3">
                                    <label for="proof_like" class="form-label">Proof Like</label>
                                    <select class="form-select" name="proof_like" id="proof_like">
                                        <option selected>None</option>
                                        <option value="PL">Proof Like (PL)</option>
                                        @if($coin->type->coinType == 'Morgan Dollar')
                                            <option value="SPL">Semi-Prooflike (SPL)</option>
                                            <option value="DMPL">Deep Mirror Proof Like (DMPL)</option>
                                            <option value="UPL">Ultra Prooflike (UPL)</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        @endif


                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="toned" class="form-label">Toned</label>
                                <select class="form-select" name="toned" id="toned">
                                    <option selected>None</option>
                                    <option value="Gray">Gray</option>
                                    <option value="Gold">Gold</option>
                                    <option value="UPL">Reddish Orange</option>
                                    <option value="Blue">Blue</option>
                                    <option value="Artificial">Artificial</option>
                                    <option value="Rainbow Toning">Rainbow Toning</option>
                                    <option value="Rainbow Crescent Toning">Rainbow Crescent Toning</option>
                                    <option value="Rim Toning">Rim Toning</option>
                                    <option value="Target Toning">Target Toning</option>
                                    <option value="Mottled toning">Mottled Toning</option>
                                    <option value="Monster Toning">Monster Toning</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="designation" class="form-label">Designation</label>
                                <select class="form-select" name="designation" id="designation">
                                    <option selected>None</option>
                                    <option value="SP">Specimen (SP) Struck well like a Proof</option>
                                    <option value="Genuine">Genuine</option>
                                    <option value="Sample">Sample</option>
                                    <option value="Authentic">Authentic</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">

                        </div>
                    </div>

                    @if($color == 1)
                        <div class="mb-3">
                            <label for="color" class="form-label">Color</label>
                            <select class="form-select" name="color" id="color">
                                <option selected>None</option>
                                <option value="BN">Brown (BN) Less then 5% original red color</option>
                                <option value="RB">Red and Brown (RB) Between 5% and 95% original red color</option>
                                <option value="RD">Red (RD) More than 95% original red color</option>
                            </select>
                        </div>
                    @endif
                    @if($full == 1)
                        <div class="mb-3">
                            <label for="fullAtt" class="form-label">Full Attribute</label>
                            <select class="form-select" name="fullAtt" id="fullAtt">
                                <option selected>None</option>
                                @if($coin->type->coinType == 'Jefferson Nickel')
                                    <option value="6FS">6 Full Steps</option>
                                    <option value="5FS">5 Full Steps</option>
                                    <option value="FS">Full Steps</option>
                                @endif
                                @if($coin->type->coinType == 'Standing Liberty')
                                    <option value="FH">Full Head</option>
                                @endif

                                @if($coin->type->coinType == 'Mercury Dime')
                                    <option value="FB">Full Band</option>
                                @endif

                                @if($coin->type->coinType == 'Franklin Half Dollar')
                                    <option value="FBL">Full Bell Lines</option>
                                @endif

                                @if($coin->type->coinType == 'Roosevelt Dime')
                                    <option value="FB">Full Band</option>
                                    <option value="FT">Full Torch</option>
                                @endif
                            </select>
                        </div>
                    @endif


                    {{-- ---------------Varieties and errors ---------------------}}

                    <div class="form-group mb-3">
                        <div class="row">
                            <div class="col-sm-6">
                                <a class="btn btn-primary" data-bs-toggle="collapse" href="#collapseVariety"
                                   role="button" aria-expanded="false" aria-controls="collapseExample">
                                    Add Variety
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <button class="btn btn-primary" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseErrors" aria-expanded="false"
                                        aria-controls="collapseExample">
                                    Add Error
                                </button>
                            </div>
                        </div>


                        <div class="collapse mt-3" id="collapseVariety">
                            <div class="card card-body">
                                <div class="form-check">
                                    <input class="form-check-input has_more" type="checkbox" name="has_variety"
                                           value="1"
                                           id="has_variety">
                                    <label class="form-check-label" for="has_variety">
                                        Yes, Send me to the varieties page next
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="collapse mt-3" id="collapseErrors">
                            <div class="card card-body">
                                <div class="form-check">
                                    <input class="form-check-input has_more" type="checkbox" name="has_error" value="1"
                                           id="has_error">
                                    <label class="form-check-label" for="has_error">
                                        Yes, Send me to the errors page next
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>


                    {{-- ---------------Varieties and errors ---------------------}}


                    <div class="card mb-3">
                        <div class="card-body">
                            <h5 class="card-title">Problems</h5>

                            <div class="row">
                                <div class="col-sm-4 mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="damaged" value="1"
                                               id="damaged">
                                        <label class="form-check-label" for="damaged">
                                            Damaged
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4 mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="holed" value="1"
                                               id="holed">
                                        <label class="form-check-label" for="holed">
                                            Holed
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4 mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="cleaned" value="1"
                                               id="cleaned">
                                        <label class="form-check-label" for="cleaned">
                                            Cleaned
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4 mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="polished" value="1"
                                               id="polished">
                                        <label class="form-check-label" for="polished">
                                            Polished
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4 mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="altered" value="1"
                                               id="altered">
                                        <label class="form-check-label" for="altered">
                                            Altered
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4 mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="scratched" value="1"
                                               id="scratched">
                                        <label class="form-check-label" for="scratched">
                                            Scratched
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4 mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="pvc_damage" value="1"
                                               id="pvc_damage">
                                        <label class="form-check-label" for="pvc_damage">
                                            PVC Damage
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4 mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="corrosion" value="1"
                                               id="corrosion">
                                        <label class="form-check-label" for="corrosion">
                                            Corrosion
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4 mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="bent" value="1" id="bent">
                                        <label class="form-check-label" for="bent">
                                            Bent
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4 mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="plugged" value="1"
                                               id="plugged">
                                        <label class="form-check-label" for="plugged">
                                            Plugged
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4 mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="bag_mark" value="1"
                                               id="bag_mark">
                                        <label class="form-check-label" for="bag_mark">
                                            Bag/Contact Mark
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4 mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="counterstamp" value="1"
                                               id="counterstamp">
                                        <label class="form-check-label" for="counterstamp">
                                            Counterstamp
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4 mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="whizzing" value="1"
                                               id="whizzing">
                                        <label class="form-check-label" for="whizzing">
                                            Whizzing
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4 mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="fingermarks" value="1"
                                               id="fingermarks">
                                        <label class="form-check-label" for="fingermarks">
                                            Fingermarks
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4 mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="spotted" value="1"
                                               id="spotted">
                                        <label class="form-check-label" for="spotted">
                                            Spotted
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4 mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="slide_marks" value="1"
                                               id="slide_marks">
                                        <label class="form-check-label" for="slide_marks">
                                            Slide Marks
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="form-group mb-3">
                        <label class="control-label">Notes</label>
                        <textarea id="note_body" name="note" class="form-control" rows="8">{{ old('note') }}</textarea>
                    </div>

                    <div class="card p-3 mb-3">
                        <div class="mb-3 form-check card-body">
                            <input type="checkbox" class="form-check-input" name="private" id="private">
                            <label class="form-check-label" for="private">Private (NOT viewable)</label>
                        </div>
                    </div>

                    <!-- Submit Form Input -->
                    <div class="col-3">
                        <button type="submit" class="btn btn-primary form-control">Create</button>
                    </div>
                </div>
            </form>

            <div class="mh-100"></div>
        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <h6>Quick Add (details later)</h6>
                            <form class="row row-cols-lg-auto g-3 align-items-center" id="add_bulk_coin_form"
                                  action="{{ route('collected.quick_save_coin_id') }}">
                                <input type="hidden" name="coin_id" value="{{ $coin->id }}">
                                <div class="col-12">
                                    <label class="visually-hidden" for="inlineFormSelectPref">Preference</label>
                                    <select name="coin_amount" class="form-select" id="inlineFormSelectPref" required>
                                        <option selected>Choose...</option>
                                        <option value="1">1</option>
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="{{$coin->type->rollCount}}">1 Roll</option>
                                        <option value="{{$coin->type->bagCount}}">1 Bag</option>
                                    </select>
                                </div>

                                <div class="col-12">
                                    <button id="add_bulk_coin_form_btn" type="submit" class="btn btn-primary">Add
                                    </button>
                                </div>
                            </form>

                            <h6 class="mb-0 mt-5">Recent Additions</h6>
                            @if(count($recents) > 0)
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Name</th>
                                        <th scope="col">Grade</th>
                                        <th scope="col">Saved</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($recents as $recent)
                                        <tr>
                                            <td><a class="group_link"
                                                   href="{{ route('collected.view_collected', ['collected' => $recent->id]) }}"
                                                   title="{{$recent->grade}}">{{ Str::limit($recent->nickName, 20) ?? 'None'}}</a>
                                            </td>
                                            <td>
                                                {{ Str::limit($recent->grade, 20) ?? 'None'}}
                                            </td>
                                            <td>
                                                {{ \Carbon\Carbon::parse($recent->created_at)->diffForHumans() }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <p>None saved</p>
                            @endif


                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    @push('styles')
        <style>
            #note_body {
                min-height: 300px;
            }
        </style>
    @endpush
    @push('scripts')
        <script src="https://cdn.ckeditor.com/ckeditor5/35.2.1/classic/ckeditor.js"></script>
        <script>
            (function () {
                try {
                    // https://stackoverflow.com/questions/71201157/how-to-display-data-in-an-html-table-using-fetch-api
                    let tpg_service = document.getElementById("tpg_service");
                    let pro_div = document.getElementById("pro_div");
                    let ngc_div = document.getElementById("ngc_div");
                    let pcgs_div = document.getElementById("pcgs_div");
                    let anacs_div = document.getElementById("anacs_div");
                    let anacs_slab_generation = document.getElementById("anacs_slab_generation");
                    let ngc_slab_generation = document.getElementById("ngc_slab_generation");
                    let pcgs_slab_generation = document.getElementById("pcgs_slab_generation");

                    // selects
                    let ngc_special_label = document.getElementById("ngc_special_label");
                    let pcgs_special_label = document.getElementById("pcgs_special_label");

                    var default_checked = tpg_service.defaultChecked;
                    console.log(default_checked);
                    pro_div.style.display = "none";
                    ngc_div.style.display = "none";
                    pcgs_div.style.display = "none";
                    anacs_div.style.display = "none";

                    tpg_service.addEventListener("change", function () {
                        if (this.value === "NGC" || this.value === "PCGS" || this.value === "ICG" || this.value === "ANACS") {
                            pro_div.style.display = "flex";
                            const desc = this.selectedOptions[0].text;
                            console.log(`option desc`, desc);
                            console.log(this.value);
                        }
                        if (this.value === "NGC") {
                            pro_div.style.display = "flex";
                            ngc_div.style.display = "flex";
                            pcgs_div.style.display = "none";
                            anacs_div.style.display = "none";
                            pcgs_special_label.selectedIndex = -1;
                            pcgs_slab_generation.selectedIndex = -1;
                            anacs_slab_generation.selectedIndex = -1;
                        }
                        if (this.value === "ANACS") {
                            pro_div.style.display = "flex";
                            anacs_div.style.display = "flex";
                            ngc_div.style.display = "none";
                            pcgs_div.style.display = "none";
                            pcgs_special_label.selectedIndex = -1;
                            ngc_special_label.selectedIndex = -1;
                            pcgs_slab_generation.selectedIndex = -1;
                            ngc_slab_generation.selectedIndex = -1;
                        }
                        if (this.value === "PCGS") {
                            pro_div.style.display = "flex";
                            pcgs_div.style.display = "flex";
                            ngc_div.style.display = "none";
                            anacs_div.style.display = "none";
                            ngc_special_label.selectedIndex = -1;
                            anacs_slab_generation.selectedIndex = -1;
                            ngc_slab_generation.selectedIndex = -1;
                        }
                        if (this.value === "ICG") {
                            pro_div.style.display = "flex";
                            ngc_div.style.display = "none";
                            pcgs_div.style.display = "none";
                            anacs_div.style.display = "none";
                            pcgs_special_label.selectedIndex = -1;
                            ngc_special_label.selectedIndex = -1;
                            anacs_slab_generation.selectedIndex = -1;
                            ngc_slab_generation.selectedIndex = -1;
                            pcgs_slab_generation.selectedIndex = -1;
                        }
                        if (this.value === "None") {
                            pro_div.style.display = "none";
                            ngc_div.style.display = "none";
                            pcgs_div.style.display = "none";
                            anacs_div.style.display = "none";
                            pcgs_special_label.selectedIndex = -1;
                            ngc_special_label.selectedIndex = -1;
                            anacs_slab_generation.selectedIndex = -1;
                            ngc_slab_generation.selectedIndex = -1;
                            pcgs_slab_generation.selectedIndex = -1;
                        }
                    });


                    ClassicEditor.defaultConfig = {
                        toolbar: {
                            items: [
                                'heading',
                                '|',
                                'bold',
                                'italic',
                                '|',
                                'bulletedList',
                                'numberedList',
                                '|',
                                'insertTable',
                                '|',
                                'undo',
                                'redo'
                            ]
                        },
                        image: {
                            toolbar: [
                                'imageStyle:full',
                                'imageStyle:side',
                                '|',
                                'imageTextAlternative'
                            ]
                        },
                        table: {
                            contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells']
                        },
                        language: 'en'
                    };

                    ClassicEditor
                        .create( document.querySelector( '#note_body' ) )
                        .then( newEditor => {
                            editor = newEditor;
                        } )
                        .catch( error => {
                            console.error( error );
                        } );

                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
