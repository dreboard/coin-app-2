@extends('layouts.user.main')
@section('pageTitle', 'Add Multiple To Collection')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Add {{ $coin->coinName }} {{ $coin->type->coinType }}</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Collection Pages
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">Bacettings</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.edit_public', ['user' => auth()->user()->id]) }}">Edirofile</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_edit_avatar', ['user' => auth()->user()->id]) }}">Chaage</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('collected.start') }}">Back To Start</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('coin.view_coin', ['coin' => $coin->id]) }}">{{ $coin->coinName }} {{ $coin->type->coinType }}</a>
        </li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <div class="alert alert-primary" role="alert">
                Images, varieties and notes can be added to this coin on the view collected coins page..
            </div>

            <form id="edit_club_form" action="{{ route('collected.save_coin_id') }}"
                  method="post" id="groupForm" enctype="multipart/form-data">
                @csrf


                <div class="col-md-12">
                    <input type="hidden" name="coin_id" value="{{ $coin->id }}">
                    <div class="mb-3">
                        <label for="text" class="form-label">Nickname</label>
                        <input type="text" class="form-control" name="nickname" id="nickname"
                               value="{{ old('nickname') }}" placeholder="Auto generated if blank">
                    </div>

                    @if($sub_types !== 'None')
                        <div class="mb-3">
                            <label for="sub_type" class="form-label">Variety Type</label>
                            <select class="form-select" name="sub_type" id="sub_type">
                                <option value="Plain" selected>Plain (Regular Issue)</option>
                                @foreach($sub_types as $k => $sub_type)
                                    @if ($sub_type === 'None')
                                        @php
                                            continue;
                                        @endphp
                                    @endif
                                    <option value="{{$sub_type}}">{{$sub_type}}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif


                    @if($circulated == 1)
                        <div class="card mb-3">
                            <div class="card-body">
                                <div class="form-check">
                                    <input class="form-check-input" value="1" type="radio" name="circulated" id="circulated1"
                                           checked>
                                    <label class="form-check-label" for="circulated1">
                                        Circulated
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" value="" 0 type="radio" name="circulated" id="circulated2">
                                    <label class="form-check-label" for="circulated2">
                                        Uncirculated
                                    </label>
                                </div>
                            </div>
                        </div>

                    @endif

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="grade" class="form-label">Grade</label>
                                <select class="form-select" name="grade" id="grade">
                                    @include($gradeList)
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="adjectival_grade" class="form-label">Adjectival Grade</label>
                                <select class="form-select" name="adjectival_grade" id="adjectival_grade">
                                    <option value="">None, Not Used</option>
                                    <option value="Uncirculated">Uncirculated</option>
                                    <option value="Select or Choice Uncirculated">Select or Choice Uncirculated</option>
                                    <option value="Choice Uncirculated">Choice Uncirculated</option>
                                    <option value="Gem Uncirculated">Gem Uncirculated</option>
                                    <option value="Superb Gem Uncirculated">Superb Gem Uncirculated</option>
                                    <option value="Perfect Uncirculated">Perfect Uncirculated</option>

                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="tpg_service" class="form-label">Grading Service</label>
                                <select class="form-select" name="tpg_service" id="tpg_service">
                                    <option value="" selected>None</option>
                                    <option value="PCGS">PCGS (Professional Coin Grading Service)</option>
                                    <option value="NGC">NGC (Numismatic Guaranty Corporation of America)</option>
                                    <option value="ANACS">ANACS (American Numismatic Association Certification
                                        Service)
                                    </option>
                                    <option value="ICG">ICG (Independent Coin Grading Company)</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div id="pro_div" class="row">
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="tpg_serial_num" class="form-label">Serial Number</label>
                                <input type="text" class="form-control" name="tpg_serial_num" id="tpg_serial_num"
                                       value="{{ old('tpg_serial_num') }}">
                            </div>
                        </div>
                    </div>



                    <div class="card p-3 mb-3">
                        <div class="mb-3 form-check card-body">
                            <input type="checkbox" class="form-check-input" name="private" id="private">
                            <label class="form-check-label" for="private">Private (NOT viewable)</label>
                        </div>
                    </div>

                    <!-- Submit Form Input -->
                    <div class="col-3">
                        <button type="submit" class="btn btn-primary form-control">Create</button>
                    </div>
                </div>
            </form>

            <div class="mh-100"></div>
        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <h6>Quick Add (details later)</h6>
                            <form class="row row-cols-lg-auto g-3 align-items-center" id="add_bulk_coin_form"
                                  action="{{ route('collected.quick_save_coin_id') }}">
                                <input type="hidden" name="coin_id" value="{{ $coin->id }}">
                                <div class="col-12">
                                    <label class="visually-hidden" for="inlineFormSelectPref">Preference</label>
                                    <select name="coin_amount" class="form-select" id="inlineFormSelectPref" required>
                                        <option selected>Choose...</option>
                                        <option value="1">1</option>
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="{{$coin->type->rollCount}}">1 Roll</option>
                                        <option value="{{$coin->type->bagCount}}">1 Bag</option>
                                    </select>
                                </div>

                                <div class="col-12">
                                    <button id="add_bulk_coin_form_btn" type="submit" class="btn btn-primary">Add
                                    </button>
                                </div>
                            </form>

                            <h6 class="mb-0 mt-5">Recent Additions</h6>
                            @if(count($recents) > 0)
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Name</th>
                                        <th scope="col">Grade</th>
                                        <th scope="col">Saved</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($recents as $recent)
                                        <tr>
                                            <td><a class="group_link"
                                                   href="{{ route('collected.view_collected', ['collected' => $recent->id]) }}"
                                                   title="{{$recent->grade}}">{{ Str::limit($recent->nickName, 20) ?? 'None'}}</a>
                                            </td>
                                            <td>
                                                {{ Str::limit($recent->grade, 20) ?? 'None'}}
                                            </td>
                                            <td>
                                                {{ \Carbon\Carbon::parse($recent->created_at)->diffForHumans() }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <p>None saved</p>
                            @endif


                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    @push('styles')
        <style>
            #note_body {
                min-height: 300px;
            }
        </style>
    @endpush
    @push('scripts')
        <script src="https://cdn.ckeditor.com/ckeditor5/35.2.1/classic/ckeditor.js"></script>
        <script>
            (function (ENVIRONMENT) {
                try {
                    // https://stackoverflow.com/questions/71201157/how-to-display-data-in-an-html-table-using-fetch-api
                    let tpg_service = document.getElementById("tpg_service");
                    let pro_div = document.getElementById("pro_div");
                    let ngc_div = document.getElementById("ngc_div");
                    let pcgs_div = document.getElementById("pcgs_div");
                    let anacs_div = document.getElementById("anacs_div");
                    let icg_div = document.getElementById("icg_div");
                    let anacs_slab_generation = document.getElementById("anacs_slab_generation");
                    let ngc_slab_generation = document.getElementById("ngc_slab_generation");
                    let pcgs_slab_generation = document.getElementById("pcgs_slab_generation");
                    let icg_slab_generation_default = document.getElementById("icg_slab_generation_default");
                    let pcgs_slab_generation_div = document.getElementById("pcgs_slab_generation_div");
                    let gsa_holder_oversize = document.getElementById("gsa_holder_oversize");
                    let gsa_holder_none = document.getElementById("gsa_holder_none");
                    // pcgs_gsa_holders  gsa_holder_label


                    gsa_holder_oversize.addEventListener("click", function () {
                        if (this.checked) {
                            console.log('checked');
                            //pcgs_slab_generation_div.style.display = "disabled";
                            pcgs_slab_generation.selectedIndex = -1;
                            pcgs_slab_generation.disabled = true;
                            pcgs_special_label.disabled = true;
                        }
                    });
                    // PCGS Standard GSA Label
                    document.getElementById("gsa_holder_holder").addEventListener("click", function () {
                        if (this.checked) {
                            pcgs_slab_generation_div.style.display = "flex";
                            pcgs_slab_generation.selectedIndex = -1;
                            pcgs_slab_generation.disabled = false;
                            pcgs_special_label.disabled = false;
                        }
                    });

                    gsa_holder_none.addEventListener("click", function () {
                        if (this.checked) {
                            console.log('checked');
                            pcgs_slab_generation_div.style.display = "flex";
                            pcgs_slab_generation.selectedIndex = -1;
                            pcgs_slab_generation.disabled = false;
                            pcgs_special_label.disabled = false;
                        }
                    });

                    // selects
                    let ngc_special_label = document.getElementById("ngc_special_label");
                    let pcgs_special_label = document.getElementById("pcgs_special_label");

                    var default_checked = tpg_service.defaultChecked;
                    console.log(default_checked);
                    pro_div.style.display = "none";
                    ngc_div.style.display = "none";
                    pcgs_div.style.display = "none";
                    anacs_div.style.display = "none";
                    icg_div.style.display = "none";


                    tpg_service.addEventListener("change", function () {
                        if (this.value === "NGC" || this.value === "PCGS" || this.value === "ICG" || this.value === "ANACS") {
                            pro_div.style.display = "flex";
                            const desc = this.selectedOptions[0].text;
                            //console.log(`option desc`, desc);
                            //console.log(this.value);
                        }
                        if (this.value === "NGC") {
                            pro_div.style.display = "flex";
                            ngc_div.style.display = "flex";
                            pcgs_div.style.display = "none";
                            anacs_div.style.display = "none";
                            icg_div.style.display = "none";
                            pcgs_special_label.selectedIndex = -1;
                            pcgs_slab_generation.selectedIndex = -1;
                            anacs_slab_generation.selectedIndex = -1;
                            icg_slab_generation_default.checked = false;
                            gsa_holder_none.checked = false;
                            pcgs_slab_generation_div.style.display = "none";
                        }
                        if (this.value === "ANACS") {
                            pro_div.style.display = "flex";
                            anacs_div.style.display = "flex";
                            ngc_div.style.display = "none";
                            pcgs_div.style.display = "none";
                            icg_div.style.display = "none";
                            pcgs_special_label.selectedIndex = -1;
                            ngc_special_label.selectedIndex = -1;
                            pcgs_slab_generation.selectedIndex = -1;
                            ngc_slab_generation.selectedIndex = -1;
                            icg_slab_generation_default.checked = false;
                            pcgs_slab_generation_div.style.display = "none";
                        }
                        if (this.value === "PCGS") {
                            pro_div.style.display = "flex";
                            pcgs_div.style.display = "flex";
                            ngc_div.style.display = "none";
                            anacs_div.style.display = "none";
                            icg_div.style.display = "none";
                            ngc_special_label.selectedIndex = -1;
                            anacs_slab_generation.selectedIndex = -1;
                            ngc_slab_generation.selectedIndex = -1;
                            icg_slab_generation_default.checked = false;
                            gsa_holder_none.checked = false;
                            pcgs_slab_generation_div.style.display = "flex";
                        }
                        if (this.value === "ICG") {
                            pro_div.style.display = "flex";
                            icg_div.style.display = "flex";
                            ngc_div.style.display = "none";
                            pcgs_div.style.display = "none";
                            anacs_div.style.display = "none";
                            pcgs_special_label.selectedIndex = -1;
                            ngc_special_label.selectedIndex = -1;
                            anacs_slab_generation.selectedIndex = -1;
                            ngc_slab_generation.selectedIndex = -1;
                            pcgs_slab_generation.selectedIndex = -1;
                            gsa_holder_none.checked = false;
                            pcgs_slab_generation_div.style.display = "none";
                        }
                        if (this.value === "None") {
                            pro_div.style.display = "none";
                            ngc_div.style.display = "none";
                            pcgs_div.style.display = "none";
                            anacs_div.style.display = "none";
                            icg_div.style.display = "none";
                            icg_slab_generation_default.checked = true;
                            pcgs_special_label.selectedIndex = -1;
                            ngc_special_label.selectedIndex = -1;
                            anacs_slab_generation.selectedIndex = -1;
                            ngc_slab_generation.selectedIndex = -1;
                            pcgs_slab_generation.selectedIndex = -1;
                            gsa_holder_none.checked = false;
                            pcgs_slab_generation_div.style.display = "none";
                        }
                    });


                    ClassicEditor.defaultConfig = {
                        toolbar: {
                            items: [
                                'heading',
                                '|',
                                'bold',
                                'italic',
                                '|',
                                'bulletedList',
                                'numberedList',
                                '|',
                                'insertTable',
                                '|',
                                'undo',
                                'redo'
                            ]
                        },
                        image: {
                            toolbar: [
                                'imageStyle:full',
                                'imageStyle:side',
                                '|',
                                'imageTextAlternative'
                            ]
                        },
                        table: {
                            contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells']
                        },
                        language: 'en'
                    };

                    ClassicEditor
                        .create( document.querySelector( '#note_body' ) )
                        .then( newEditor => {
                            editor = newEditor;
                        } )
                        .catch( error => {
                            console.error( error );
                        } );

                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
