@extends('layouts.user.main')
@section('pageTitle', 'My Collection')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">My Collection/Inventory</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    My Collection
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item" href="{{ route('coin.coin_index') }}">My Coins</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Sets</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Folders</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Rolls</a></li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">Coins Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                View your U.S. Coin collection.
            </div>

            <div class="row text-center">
                <div class="col-sm-3">
                    <div class="callout callout-primary">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Coins
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('coin.coin_index') }}">Coins</a></li>
                                <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">Mintsets</a></li>
                                <li><a class="dropdown-item" href="{{ route('coin.coin_index') }}">Folders</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="callout callout-primary">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Reports
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('collected.report_index') }}">All</a></li>
                                <li><a class="dropdown-item" href="{{ route('collected.report_index') }}">Spending</a></li>
                                <li><a class="dropdown-item" href="{{ route('collected.report_index') }}">Precious Metals</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="callout callout-primary">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Errors
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('coin.error_index') }}">See All</a></li>
                                <li><a class="dropdown-item" href="{{ route('coin.error_index') }}">Create (Found A
                                        new error)</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="callout callout-primary">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                Add Something
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('collected.start') }}">Coin</a></li>
                                <li><a class="dropdown-item" href="{{ route('collected.start_folder') }}">Folder/Album</a></li>
                                <li><a class="dropdown-item" href="{{ route('collected.start_roll') }}">Rolls</a></li>
                                <li><a class="dropdown-item" href="{{ route('collected.start_mintset') }}">Minset</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-6">
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-chart-area me-1"></i>
                            What I Collect
                        </div>
                        <div class="card-body"><canvas id="myAreaChart" width="100%" height="40"></canvas></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-chart-bar me-1"></i>
                            What I Spend
                        </div>
                        <div class="card-body"><canvas id="myBarChart" width="100%" height="40"></canvas></div>
                    </div>
                </div>


                <div class="col-sm-12">
                    <table class="table">
                        <tr>
                            <th class="fw-bold">#</th>
                            <th class="fw-bold">Category</th>
                            <th class="fw-bold">Collected</th>
                            <th class="fw-bold">Investment</th>
                        </tr>

                        @foreach($categories as $id => $category)
                            <tr class="info_row">
                                <th><a href="{{ route('coin.view_category', ['id' => $id]) }}">
                                        <img src="{{config('app.image_url')}}{{ str_replace(' ', '_', $category) }}.jpg"
                                             class="forum_index_img" alt="..."></a></th>
                                <td><span class="cat_ids" data-category="{{$id}}"><a
                                            href="{{ route('member.forum_cat_index', ['cat_id' => $id]) }}"
                                        >{{ $category }}</a></span></td>
                                <td><span class="cat_date" id="cat_date_{{$id}}">Loading...</span></td>
                                <td><span class="cat_count" id="cat_count_{{$id}}">Loading...</span></td>
                            </tr>
                        @endforeach
                    </table>
                </div>


                <div class="col-sm-3">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            All Bulk Items
                        </button>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="{{ route('collected.start') }}">Coin Lots</a></li>
                            <li><a class="dropdown-item" href="{{ route('collected.start_folder') }}">Rolls</a></li>
                            <li><a class="dropdown-item" href="{{ route('collected.start_roll') }}">Boxes</a></li>
                            <li><a class="dropdown-item" href="{{ route('collected.start_mintset') }}">Minset</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Collection History
                        </button>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="{{ route('collected.start') }}">Coin</a></li>
                            <li><a class="dropdown-item" href="{{ route('collected.start_folder') }}">Folder/Album</a></li>
                            <li><a class="dropdown-item" href="{{ route('collected.start_roll') }}">Rolls</a></li>
                            <li><a class="dropdown-item" href="{{ route('collected.start_mintset') }}">Minset</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Selling/Wishlist
                        </button>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="{{ route('collected.start') }}">Coin</a></li>
                            <li><a class="dropdown-item" href="{{ route('collected.start_folder') }}">Folder/Album</a></li>
                            <li><a class="dropdown-item" href="{{ route('collected.start_roll') }}">Rolls</a></li>
                            <li><a class="dropdown-item" href="{{ route('collected.start_mintset') }}">Minset</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Dropdown button
                        </button>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="{{ route('collected.start') }}">Coin</a></li>
                            <li><a class="dropdown-item" href="{{ route('collected.start_folder') }}">Folder/Album</a></li>
                            <li><a class="dropdown-item" href="{{ route('collected.start_roll') }}">Rolls</a></li>
                            <li><a class="dropdown-item" href="{{ route('collected.start_mintset') }}">Minset</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <hr/>
            <!-- Featured blog post-->
            <h3 class="mt-3">Newest Research</h3>
            <!-- Nested row for non-featured blog posts-->
            <div class="row">
                @foreach($projects as $new_project)
                    <div class="col-lg-6">
                        <!-- Blog post-->
                        <div class="card mb-4">
                            <div class="card-body">
                                <div
                                    class="small text-muted">{{ Carbon\Carbon::parse($new_project->created_at)->format('F jS Y') }}</div>
                                <h2 class="card-title h4">{{ $new_project->title }}</h2>
                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Reiciendis aliquid atque, nulla.</p>
                                <a class="btn btn-primary"
                                   href="{{ route('member.project_public_view', ['project' => $new_project->id]) }}">View →</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">View All Reports</div>
                <div class="card-body">
                    <div class="input-group">
                        <a href="{{ route('collected.report_index') }}" class="btn btn-primary" id="button-search"
                           type="button">Go!</a>
                    </div>
                </div>
            </div>

            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Categories</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('coin.view_category', ['id' => 45]) }}">Half Cent</a></li>
                                <li><a href="{{ route('coin.view_category', ['id' => 83]) }}">Small Cent</a></li>
                                <li><a href="{{ route('coin.view_category', ['id' => 46]) }}">Large Cent</a></li>
                                <li><a href="{{ route('coin.view_category', ['id' => 47]) }}">Two Cent</a></li>
                                <li><a href="{{ route('coin.view_category', ['id' => 48]) }}">Three Cent</a></li>
                                <li><a href="{{ route('coin.view_category', ['id' => 23]) }}">Nickel</a></li>
                                <li><a href="{{ route('coin.view_category', ['id' => 28]) }}">Half Dime</a></li>
                                <li><a href="{{ route('coin.view_category', ['id' => 43]) }}">Dime</a></li>
                                <li><a href="{{ route('coin.view_category', ['id' => 44]) }}">Twenty Cent</a></li>
                                <li><a href="{{ route('coin.view_category', ['id' => 84]) }}">Quarter</a></li>
                                <li><a href="{{ route('coin.view_category', ['id' => 37]) }}">Half Dollar</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('coin.view_category', ['id' => 63]) }}">Dollar</a></li>
                                <li><a href="{{ route('coin.view_category', ['id' => 87]) }}">Gold Dollar</a></li>
                                <li><a href="{{ route('coin.view_category', ['id' => 69]) }}">Quarter Eagle</a></li>
                                <li><a href="{{ route('coin.view_category', ['id' => 71]) }}">Three Dollar</a></li>
                                <li><a href="{{ route('coin.view_category', ['id' => 72]) }}">Four Dollar</a></li>
                                <li><a href="{{ route('coin.view_category', ['id' => 74]) }}">Five Dollar</a></li>
                                <li><a href="{{ route('coin.view_category', ['id' => 82]) }}">Ten Dollar</a></li>
                                <li><a href="{{ route('coin.view_category', ['id' => 86]) }}">Twenty Dollar</a></li>
                                <li><a href="{{ route('coin.view_category', ['id' => 89]) }}">Twenty Five Dollar</a></li>
                                <li><a href="{{ route('coin.view_category', ['id' => 90]) }}">Fifty Dollar</a></li>
                                <li><a href="{{ route('coin.view_category', ['id' => 91]) }}">One Hundred Dollar</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">
                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.view_directory') }}">User Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        </div>
    </div>
    @push('styles')
        <style>
            .forum_index_img {
                height: 30px;
                width: auto;
            }
        </style>
    @endpush
    @push('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
        <script>

            (function ($) {
                try {
                    Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
                    Chart.defaults.global.defaultFontColor = '#292b2c';

// Area Chart Example
                    var ctx = document.getElementById("myAreaChart");
                    var myLineChart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: ["Mar 1", "Mar 2", "Mar 3", "Mar 4", "Mar 5", "Mar 6", "Mar 7", "Mar 8", "Mar 9", "Mar 10", "Mar 11", "Mar 12", "Mar 13"],
                            datasets: [{
                                label: "Sessions",
                                lineTension: 0.3,
                                backgroundColor: "rgba(2,117,216,0.2)",
                                borderColor: "rgba(2,117,216,1)",
                                pointRadius: 5,
                                pointBackgroundColor: "rgba(2,117,216,1)",
                                pointBorderColor: "rgba(255,255,255,0.8)",
                                pointHoverRadius: 5,
                                pointHoverBackgroundColor: "rgba(2,117,216,1)",
                                pointHitRadius: 50,
                                pointBorderWidth: 2,
                                data: [10000, 30162, 26263, 18394, 18287, 28682, 31274, 33259, 25849, 24159, 32651, 31984, 38451],
                            }],
                        },
                        options: {
                            scales: {
                                xAxes: [{
                                    time: {
                                        unit: 'date'
                                    },
                                    gridLines: {
                                        display: false
                                    },
                                    ticks: {
                                        maxTicksLimit: 7
                                    }
                                }],
                                yAxes: [{
                                    ticks: {
                                        min: 0,
                                        max: 40000,
                                        maxTicksLimit: 5
                                    },
                                    gridLines: {
                                        color: "rgba(0, 0, 0, .125)",
                                    }
                                }],
                            },
                            legend: {
                                display: false
                            }
                        }
                    });



                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }


            }(window.jQuery));

            (function ($) {
                try {
                    Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
                    Chart.defaults.global.defaultFontColor = '#292b2c';

// Bar Chart Example
                    var ctx = document.getElementById("myBarChart");
                    var myLineChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["January", "February", "March", "April", "May", "June"],
                            datasets: [{
                                label: "Revenue",
                                backgroundColor: "rgba(2,117,216,1)",
                                borderColor: "rgba(2,117,216,1)",
                                data: [4215, 5312, 6251, 7841, 9821, 14984],
                            }],
                        },
                        options: {
                            scales: {
                                xAxes: [{
                                    time: {
                                        unit: 'month'
                                    },
                                    gridLines: {
                                        display: false
                                    },
                                    ticks: {
                                        maxTicksLimit: 6
                                    }
                                }],
                                yAxes: [{
                                    ticks: {
                                        min: 0,
                                        max: 15000,
                                        maxTicksLimit: 5
                                    },
                                    gridLines: {
                                        display: true
                                    }
                                }],
                            },
                            legend: {
                                display: false
                            }
                        }
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }

                const cat_ids = document.querySelectorAll('.cat_ids');
                Array.from(cat_ids)
                    .forEach(function (cat_id) {
                        window.addEventListener('load', function (event) {
                            let url = "{{ route('member.get_category_info', [':cat_id']) }}".replace(':cat_id', cat_id.dataset.category);
                            fetch(url, {
                                "method": "GET",
                            }).then(
                                response => {
                                    response.json().then(
                                        data => {
                                            document.getElementById("cat_count_" + cat_id.dataset.category).innerHTML = data.data.total;
                                            document.getElementById("cat_date_" + cat_id.dataset.category).innerHTML = data.data.last;
                                        }
                                    )
                                })
                        }, false)
                    })
            }(window.jQuery));


        </script>
    @endpush
@endsection

