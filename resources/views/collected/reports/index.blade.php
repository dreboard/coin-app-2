@extends('layouts.user.main')
@section('pageTitle', 'Coins')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Collection Reports</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Profile Pages
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">Coins</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.edit_public', ['user' => auth()->user()->id]) }}">Currency</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_edit_avatar', ['user' => auth()->user()->id]) }}">Changmage</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">Coins Home</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                View/Save U.S. Coins.
            </div>

            <table class="table">
                <tr>
                    <th class="fw-bold" colspan="4">General Reports</th>
                </tr>
                <tr class="info_row">
                    <td class="w-25"><a href="{{ route('coin.view_coin_commem_index') }}">Commemoratives</a></td>
                    <td class="w-25"><a href="{{ route('coin.metal_index') }}">Precious Metals</a></td>
                    <td class="w-25"><a href="{{ route('coin.view_eagle_index') }}">American Eagle Coins</a></td>
                    <td class="w-25"><a href="{{ route('coin.coin_index') }}">Commemoratives</a></td>
                </tr>
                <tr class="info_row">
                    <td class="w-25"><a href="{{ route('coin.century_view', ['century' => 17]) }}">17th Century</a></td>
                    <td class="w-25"><a href="{{ route('coin.century_view', ['century' => 18]) }}">18th Century</a></td>
                    <td class="w-25"><a href="{{ route('coin.century_view', ['century' => 19]) }}">19th Century</a></td>
                    <td class="w-25"><a href="{{ route('coin.century_view', ['century' => 20]) }}">20th Century</a></td>
                </tr>
                <tr class="info_row">
                    <td class="w-25"><a href="{{ route('coin.metal_type', ['type' => 'Platinum']) }}">All Platinum</a></td>
                    <td class="w-25"><a href="{{ route('coin.metal_type', ['type' => 'Palladium']) }}">All Palladium</a></td>
                    <td class="w-25"><a href="{{ route('coin.metal_type', ['type' => 'Gold']) }}">All Gold</a></td>
                    <td class="w-25"><a href="{{ route('coin.metal_type', ['type' => 'Silver']) }}">All Silver</a></td>
                </tr>
                <tr class="info_row">
                    <td class="w-25"><a href="{{ route('coin.coin_index') }}">Commemoratives</a></td>
                    <td class="w-25"><a href="{{ route('coin.coin_index') }}">Commemoratives</a></td>
                    <td class="w-25"><a href="{{ route('coin.coin_index') }}">Commemoratives</a></td>
                    <td class="w-25"><a href="{{ route('coin.coin_index') }}">Commemoratives</a></td>
                </tr>
                <tr class="info_row">
                    <td class="w-25"><a href="{{ route('coin.coin_index') }}">Commemoratives</a></td>
                    <td class="w-25"><a href="{{ route('coin.coin_index') }}">Commemoratives</a></td>
                    <td class="w-25"><a href="{{ route('coin.coin_index') }}">Commemoratives</a></td>
                    <td class="w-25"><a href="{{ route('coin.coin_index') }}">Commemoratives</a></td>
                </tr>
                <tr class="info_row">
                    <td class="w-25"><a href="{{ route('coin.coin_index') }}">Commemoratives</a></td>
                    <td class="w-25"><a href="{{ route('coin.coin_index') }}">Commemoratives</a></td>
                    <td class="w-25"><a href="{{ route('coin.coin_index') }}">Commemoratives</a></td>
                    <td class="w-25"><a href="{{ route('coin.coin_index') }}">Commemoratives</a></td>
                </tr>
            </table>


            <table class="table">
                <tr>
                    <th class="fw-bold">#</th>
                    <th class="fw-bold">Category</th>
                    <th class="fw-bold">Collected</th>
                    <th class="fw-bold">Sets</th>
                </tr>
                @foreach($categories as $id => $category)
                    <tr class="info_row">
                        <td><a href="{{ route('coin.view_category', ['id' => $id]) }}">{{ $category }}</a></td>
                        <td><a href="{{ route('coin.coin_index') }}">Seateberty</a></td>
                        <td><a href="{{ route('coin.coin_index') }}">Seateberty</a></td>
                        <td><div class="dropdown">
                                <button class="btn btn-secondary btn-sm dropdown-toggle type_btns" type="button"
                                        data-bs-toggle="dropdown" aria-expanded="false">
                                    Type Sets
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item"
                                           href="{{ route('coin.view_category', ['id' => $id]) }}">All {{ $category }}</a>
                                    </li>
                                    @foreach($types_list[$id] as $id => $type)
                                        <li><a class="dropdown-item"
                                               href="{{ route('coin.view_type', ['id' => $id]) }}">{{$type}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div></td>
                    </tr>
                @endforeach
            </table>

        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Start/View Collection</div>
                <div class="card-body">
                    <div class="input-group">
                        <a href="{{ route('collected.index') }}" class="btn btn-primary" id="button-search"
                           type="button">Go!</a>
                    </div>
                </div>
            </div>


            <div class="card mb-4">
                <div class="card-header">Sections</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('coin.seated_index') }}">Seated Liberty</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">Barber</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">Capped Bust</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">19th Century</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('coin.coin_index') }}">Classics ()</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">Commemoratives</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">Morgan Dollars</a></li>
                                <li><a href="{{ route('coin.coin_index') }}">Pre-1933 Gold</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link"
                                       href="https://catalog.usmint.gov/product-schedule/?cm_sp=CLP-_-mintmark-sched-_-040121&scp=SCHED">US
                                        Mint Product Schedule</a></li>
                                <li><a class="external_link" href="https://minterrornews.com/">Mint Error News</a></li>
                                <li><a class="external_link" href="https://conecaonline.org/">CONECA</a></li>
                                <li><a class="external_link" href="http://www.doubleddie.com/1801.html">Wexler’s Die
                                        Varieties</a></li>
                                <li><a class="external_link" href="http://varietyvista.com/Attribution%20Services.htm">Variety
                                        Vista Attribution Services</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="http://www.maddieclashes.com/">MAD Die Clashes</a>
                                </li>
                                <li><a class="external_link" href="http://www.cuds-on-coins.com/">Cuds-On-Coins</a></li>
                                <li><a class="external_link" href="http://www.traildies.com/">Trail Dies</a></li>
                                <li><a class="external_link"
                                       href="http://ec2-13-58-222-16.us-east-2.compute.amazonaws.com/wiki/Home">VAMWorld</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">

                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.view_directory') }}">User Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            @endif

        </div>
    </div>
    @push('styles')
        <style>
            .types_div {
                position: relative;
            }

            .type_btns .btn {
                position: absolute;
                bottom: 15px;
                left: 50%;
                transform: translateX(-50%);
                -webkit-transform: translateX(-50%);
                -moz-transform: translateX(-50%);
            }

        </style>
    @endpush
@endsection

