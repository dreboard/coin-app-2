<option value="No Grade" selected="selected">No Grade</option>
<option value="B0">(B-0) Basal 0 </option>
<option value="P1" >(PO-1) Poor</option>
<option value="FR2">(FR-2) Fair</option>
<option value="AG3">(AG-3) About Good</option>
<option value="G4">(G-4) Good</option>
<option value="G6">(G-6) Good</option>
<option value="VG8">(VG-8) Very Good</option>
<option value="VG10">(VG-10) Very Good</option>
<option value="F12">(F-12) Fine</option>
<option value="F15">(F-15) Fine</option>
<option value="VF20">(VF-20) Very Fine</option>
<option value="VF25">(VF-25) Very Fine</option>
<option value="VF30">(VF-30) Very Fine</option>
<option value="VF35">(VF-35) Very Fine</option>
<option value="EF40">(EF-40) Extremely Fine</option>
<option value="EF45">(EF-45) Extremely Fine</option>
<option value="AU50">(AU-50) About Uncirculated</option>
<option value="AU53">(AU-53) About Uncirculated</option>
<option value="AU55">(AU-55) About Uncirculated</option>
<option value="AU58">(AU-58) Very Choice About Uncirculated</option>
<option value="SP60">(SP-60) Mint State Basal</option>

<option value="SP61">(SP-61) Mint State Acceptable</option>
<option value="SP62">(SP-62) Mint State Acceptable</option>
<option value="SP63">(SP-63) Mint State Acceptable</option>

<option value="SP64">(SP-64) Mint State Acceptable</option>
<option value="SP65">(SP-65) Mint State Choice</option>

<option value="SP66">(SP-66) Mint State Choice</option>
<option value="SP67">(SP-67) Mint State Choice</option>

<option value="SP68">(SP-68) Mint State Premium</option>
<option value="SP69">(SP-69) Mint State All-But-Perfect</option>
<option value="SP70">(SP-70) Mint State Perfect</option>
