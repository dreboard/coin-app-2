<option value="No Grade" selected="selected">No Grade</option>
<option value="B0">(PR-0) Impaired Proof 0 </option>
<option value="P1" >(PR-1) Impaired Proof</option>
<option value="FR2">(PR-2) Impaired Proof</option>
<option value="AG3">(PR-3) Impaired Proof</option>
<option value="G4">(PR-4) Impaired Proof</option>
<option value="G6">(PR-6) Impaired Proof</option>
<option value="VG8">(PR-8) Impaired Proof</option>
<option value="VG10">(PR-10) Impaired Proof</option>
<option value="F12">(PR-12) Impaired Proof</option>
<option value="F15">(PR-15) Impaired Proof</option>
<option value="PR20">(PR-20) Impaired Proof</option>
<option value="PR25">(PR-25) Impaired Proof</option>
<option value="PR30">(PR-30) Impaired Proof</option>
<option value="PR35">(PR-35) Impaired Proof</option>
<option value="PR40">(PR-40) Impaired Proof</option>
<option value="PR45">(PR-45) Impaired Proof</option>
<option value="PR50">(PR-50) Impaired Proof</option>
<option value="PR53">(PR-53) Impaired Proof</option>
<option value="PR55">(PR-55) Impaired Proof</option>
<option value="PR58">(PR-58) Impaired Proof</option>
<option value="PR60">(PR-60) Mint State Basal</option>

<option value="PR61">(PR-61) Mint State Acceptable</option>
<option value="PR62">(PR-62) Mint State Acceptable</option>
<option value="PR63">(PR-63) Mint State Acceptable</option>

<option value="PR64">(PR-64) Mint State Acceptable</option>
<option value="PR65">(PR-65) Mint State Choice</option>

<option value="PR66">(PR-66) Mint State Choice</option>
<option value="PR67">(PR-67) Mint State Choice</option>

<option value="PR68">(PR-68) Mint State Premium</option>
<option value="PR69">(PR-69) Mint State All-But-Perfect</option>
<option value="PR70">(PR-70) Mint State Perfect</option>
