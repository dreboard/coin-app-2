@extends('layouts.user.main')
@section('pageTitle', 'View Coin')
@section('content')

    <div class="row">
        <div class="col-6 float-start mb-2">
            <h3 class="mt-4"><span id="coin_name">{{ optional($collected)->nickname ?? 'No nickname' }}</span></h3>
            <p class="text-muted h5 fs-6"><span class="fw-bold">Coin:</span>
                <a href="{{ route('coin.view_coin', ['coin' => $collected->coin_id]) }}">{{ $collected->coin->coinName }} @if(!in_array($collected->sub_type, ['Plain', 'None']))
                        {{$collected->sub_type}}
                    @endif  {{ $collected->coin->type->coinType }}</a>
            </p>
        </div>
        <div class="col-6 mt-2">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    View
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">All {{ $collected->coin->coinYear }} {{ $collected->coin->type->coinType }}</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_type', ['id' => $collected->coin->type->id]) }}">{{ $collected->coin->type->coinType }}</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_category', ['id' => $collected->coin->category->id]) }}">{{ $collected->coin->category->coinCategory }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">All Coins</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('coin.view_type', ['id' => $collected->coin->type->id]) }}">{{ $collected->coin->type->coinType }}</a>
        </li>
        <li class="breadcrumb-item"><a
                href="{{ route('coin.view_category', ['id' => $collected->coin->category->id]) }}">{{ $collected->coin->category->coinCategory }}</a>
        </li>
    </ol>

    <div class="row">
        <!-- Blog entries-->

        <div class="col-lg-8">


            @if(isset($varieties_added[0]))
                <table class="table">
                    @foreach($varieties_added as $collected_variety)
                        <tr>
                            <td class="fw-bold">{{ $collected_variety['variety'] }}:</td>
                            <td>{{ $collected_variety['label'] }}</td>
                            <td id="current_varieties">{{ $collected_variety['type'] }}</td>
                        </tr>
                    @endforeach
                </table>
            @endif

            <div class="alert alert-primary" role="alert">
                <div class="row">
                    <div class="col-6">
                        <span id="in_span"
                              data-in_type="
                              @if($collected->set_id !== 0) set @endif
                              @if($collected->roll_id == 1) roll @endif
                              @if($collected->folder_id == 1) folder @endif
                              @if($collected->firstday_id == 1) firstday @endif
                              " class="fw-bold">In:</span>
                        @if($collected->set_id !== 0) <a href="{{ route('collected.view_collected_set', ['set' => $collected->collectedSet->id]) }}">
                            Mintset {{ Str::limit($collected->collectedSet->nickname, 20) ?? 'No Name'}}
                        </a> @endif
                    </div>
                    <div class="col-6">
                        <span id="in_span"
                              data-in_type="
                              @if($collected->lot_id !== 0) lot @endif
                              " class="fw-bold">Part Of:</span>
                        @if($collected->set_id !== 0) <a href="{{ route('collected.view_collected_set', ['set' => $collected->collectedSet->id]) }}">
                            Mintset {{ Str::limit($collected->collectedSet->nickname, 20) ?? 'No Name'}}
                        </a> @endif
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-6">
                    <table class="table">
                        <tr>
                            <th class="fw-bold">Grade</th>
                            <td>{{ $collected->grade ?? 'Not Graded' }}
                                @if($collected->tpg_service == 'PCGS')
                                    PCGS @if($collected->pcgs_plus == 1) Plus @endif
                                @endif
                                @if($collected->tpg_service == 'NGC')
                                    NGC @if($collected->ngc_plus == 1) Plus @endif @if($collected->ngc_star == 1) Star @endif
                                @endif
                                @if(!is_null($collected->cac_sticker))
                                    <span class="@if($collected->cac_sticker == 'Green') text-success @endif @if($collected->cac_sticker == 'Gold') text-warning @endif">
                                        CAC {{$collected->cac_sticker}}
                                    </span> @endif</td>
                        </tr>
                        @if(!is_null($collected->sub_type))
                            <tr>
                                <th class="fw-bold">Type</th>
                                <td>{{$collected->sub_type}}</td>
                            </tr>
                        @endif

                        <tr>
                            <th class="fw-bold">TPG</th>
                            <td>{{ $collected->tpg_service ?? 'None' }} @if(!is_null($collected->tpg_service))
                                    {{ $collected->tpg_serial_num ?? 'No Serial #' }}
                                @endif</td>
                        </tr>
                        @if(!is_null($collected->designation))
                            <tr>
                                <th class="fw-bold">Designation</th>
                                <td>{{$collected->designation}}</td>
                            </tr>
                        @endif
                        <tr>
                            <th class="fw-bold">Rank</th>
                            <td><span id="coin_rank"></span></td>
                        </tr>
                        @if($color == 1)
                            <tr>
                                <th class="fw-bold">Color</th>
                                <td>{{ $collected->color ?? 'Not Assigned' }}</td>
                            </tr>
                            <tr>
                                <th class="fw-bold">Strike Character</th>
                                <td>{{ $collected->strike_character ?? 'Not Assigned' }}</td>
                            </tr>
                        @endif

                        @if(in_array($collected->coin->strike, array_merge(Config::get('constants.coins.business_strikes'), Config::get('constants.coins.special_strikes'))))
                            <tr>
                                <th class="fw-bold">Proof Like</th>
                                <td>{{ $collected->proof_like ?? 'Not Assigned' }}</td>
                            </tr>
                        @endif

                        @if(in_array($collected->coin->strike, array_merge(Config::get('constants.coins.proof_strikes'))))
                            <tr>
                                <th class="fw-bold">DCAM/Cameo</th>
                                <td>{{ $collected->proof_like ?? 'Not Assigned' }}</td>
                            </tr>
                        @endif

                    </table>

                </div>

                <div class="col-lg-6">
                    <table class="table">
                        @if($collected->coin->type->coinType == 'Jefferson Nickel')
                            <tr>
                                <th class="fw-bold">Full Steps</th>
                                <td>{{ $collected->fullAtt ?? 'Not Assigned' }}</td>
                            </tr>
                        @endif
                        @if($collected->coin->type->coinType == 'Standing Liberty')
                            <tr>
                                <th class="fw-bold">Full Head</th>
                                <td>{{ $collected->fullAtt ?? 'Not Assigned' }}</td>
                            </tr>
                        @endif

                        @if($collected->coin->type->coinType == 'Mercury Dime')
                            <tr>
                                <th class="fw-bold">Full Band</th>
                                <td>{{ $collected->fullAtt ?? 'Not Assigned' }}</td>
                            </tr>
                        @endif

                        @if($collected->coin->type->coinType == 'Franklin Half Dollar')
                            <tr>
                                <th class="fw-bold">Full Bell Lines</th>
                                <td>{{ $collected->fullAtt ?? 'Not Assigned' }}</td>
                            </tr>
                        @endif

                        @if($collected->coin->type->coinType == 'Roosevelt Dime')
                            <tr>
                                <th class="fw-bold">Full Band/Torch</th>
                                <td>{{ $collected->fullAtt ?? 'Not Assigned' }}</td>
                            </tr>
                        @endif

                        <tr>
                            <th class="fw-bold">
                                <span id="locked_status">
                                    @if($collected->locked == 1)
                                        <span class="text-danger">Locked </span>
                                    @else
                                        <span class="text-success">Unlocked </span>
                                    @endif</span>
                            </th>
                            <td>
                                <input id="lock_collected" class="form-check-input lock_collect" type="radio"
                                       name="locked" value="1"
                                       @if($collected->locked == 1) checked @endif>
                                <label class="form-check-label" for="lock_collected">
                                    Locked
                                </label>&nbsp &nbsp
                                <input id="unlock_collected" class="form-check-input lock_collect" type="radio"
                                       name="locked" value="0"
                                       @if($collected->locked !== 1) checked @endif>
                                <label class="form-check-label" for="unlock_collected">
                                    Unlocked
                                </label>
                            </td>
                        </tr>

                        <tr>
                            <th class="fw-bold">
                                <span id="private_status">
                                    @if($collected->private == 1)
                                        <span class="text-danger">Private </span>
                                    @else
                                        <span class="text-success">Viewable </span>
                                    @endif</span>
                            </th>
                            <td>
                                <input id="private_collected" class="form-check-input private_collect" type="radio"
                                       name="private" value="1"
                                       @if($collected->private == 1) checked @endif>
                                <label class="form-check-label" for="private_collected">
                                    Private
                                </label>
                                &nbsp &nbsp
                                <input id="viewable_collected" class="form-check-input private_collect" type="radio"
                                       name="private" value="0"
                                       @if($collected->private !== 1) checked @endif>
                                <label class="form-check-label" for="viewable_collected">
                                    Viewable
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <th class="fw-bold">Remove</th>
                            <td>
                                <button class="btn btn-danger" type="button" data-bs-toggle="modal"
                                        data-bs-target="#exampleModal">Delete
                                </button>
                            </td>
                        </tr>
                    </table>
                </div>


                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h1 class="modal-title fs-5" id="exampleModalLabel">Delete This Coin</h1>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <p>All notes, images and comments will also be deleted</p>
                                <form class="row row-cols-lg-auto g-3 align-items-center"
                                      method="post" action="{{ route('collected.delete_collected') }}">
                                    <input type="hidden" name="coin_id" value="{{ $collected->coin_id }}">
                                    <input type="hidden" name="collected_id" value="{{ $collected->id }}">
                                    @csrf
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-danger">Yes Delete</button>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Never Mind
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

        </div>




















            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-4">
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button"
                                        data-bs-toggle="dropdown"
                                        aria-expanded="false">Edit Coin..
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item"
                                           href="{{ route('collected.add_collected_variety', ['collected' => $collected->id]) }}">Details</a>
                                    </li>
                                    <li><a class="dropdown-item"
                                           href="{{ route('collected.edit_collected_images', ['collected' => $collected->id]) }}">Images</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button"
                                        data-bs-toggle="dropdown"
                                        aria-expanded="false">Variety/Error
                                </button>
                                <ul class="dropdown-menu">
                                    @foreach($variety_list as $variety)
                                        <li><a class="dropdown-item"
                                               href="{{ route('collected.collected_variety_list', [
                                                    'collected' => $collected->id,
                                                    'variety' => str_replace(' ', '_', $variety)
                                                    ]) }}">{{$variety}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="col-4">


                        </div>
                    </div>
                </div>
            </div>


            <div class="mb-3">

                <div class="btn-group mb-3" role="group" aria-label="Basic example">
                    <button class="btn btn-primary" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseVariety" aria-expanded="false" aria-controls="collapseVariety">
                        Assign Variety
                    </button>
                    <button type="button" class="btn btn-primary">Middle</button>
                    <button type="button" class="btn btn-primary">Right</button>
                </div>

                <div class="collapse" id="collapseVariety">


                    <div class="card card-body">
                        <select id="varietyList" class="form-select" aria-label="Default select example">
                            <option>Select</option>
                            @foreach($variety_list as $variety)
                                <option value="{{str_replace(' ', '_', $variety)}}">{{$variety}}</option>
                            @endforeach
                        </select>
                        <table id="varietyTbl" class="table table-hover datatable">
                            <thead>
                            <tr>
                                <th scope="col">Variety</th>
                                <th scope="col">Label</th>
                                <th scope="col">Reference</th>
                                <th scope="col">Attach</th>
                            </tr>
                            </thead>
                            <tbody id="varietyTblRow">
                            <tr>
                                <td>None Found</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                        <button id="add_variety_btn" type="button" class="btn btn-primary">Save</button>

                    </div>
                </div>
            </div>




            <div class="row">
                @foreach($collected->images as $image)
                    <div class="col-sm-6">
                        <figure class="mb-4">
                            <a href="{{ route('member.view_image', ['image' => $image->id]) }}">
                                <img src="{{asset('storage/'.$image->image_url)}}" style="width: 80%; height: auto"
                                     alt="{{$image->title ?? ''}}"/>
                            </a>
                        </figure>
                    </div>
                @endforeach
            </div>
            <h4>Additional Details</h4>
            <table class="table">
                <tr>
                    <th class="fw-bold">Strike</th>
                    <td>{{ $collected->grade }}</td>
                    <td>{{ $collected->grade }}</td>
                    <td>{{ $collected->grade }}</td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    <td>@fat</td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td colspan="2">Larry the Bird</td>
                    <td>@twitter</td>
                </tr>
            </table>

            @if($collected->tpg_service == 'PCGS')
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">PCGS Details</h5>
                        <table class="table">
                            <tr>
                                <th class="fw-bold">Serial Number</th>
                                <td>{{ $collected->tpg_serial_num ?? 'No Serial #' }}</td>
                                <th class="fw-bold">Slab Condition</th>
                                <td>{{ $collected->slab ?? 'Not saved' }}</td>
                            </tr>
                            <tr>
                                <th class="fw-bold">Slab Generation</th>
                                <td>{{ $collected->slab_generation ?? 'Not saved' }}</td>
                                <th class="fw-bold">Special Labels</th>
                                <td>{{ $collected->special_label ?? 'Not saved' }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            @endif

            @if($collected->tpg_service == 'PCGS')
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">NGC Details</h5>
                        <table class="table">
                            <tr>
                                <th class="fw-bold">Serial Number</th>
                                <td>{{ $collected->tpg_serial_num ?? 'No Serial #' }}</td>
                                <th class="fw-bold">Slab Condition</th>
                                <td>{{ $collected->slab ?? 'Not saved' }}</td>
                            </tr>
                            <tr>
                                <th class="fw-bold">NGC Plus</th>
                                <td>@if($collected->ngc_plus == 1)
                                        Yes
                                    @else
                                        No
                                    @endif</td>
                                <th class="fw-bold">NGC Star</th>
                                <td>@if($collected->ngc_star == 1)
                                        Yes
                                    @else
                                        No
                                    @endif</td>
                            </tr>
                            <tr>
                                <th class="fw-bold">Slab Generation</th>
                                <td>{{ $collected->slab_generation ?? 'Not saved' }}</td>
                                <th class="fw-bold">Special Labels</th>
                                <td>{{ $collected->special_label ?? 'Not saved' }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            @endif

            @if($collected->tpg_service == 'PCGS')
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">ANACS Details</h5>
                        <table class="table">
                            <tr>
                                <th class="fw-bold">Serial Number</th>
                                <td>{{ $collected->tpg_serial_num ?? 'No Serial #' }}</td>
                                <th class="fw-bold">Slab Condition</th>
                                <td>{{ $collected->slab ?? 'Not saved' }}</td>
                            </tr>
                            <tr>
                                <th class="fw-bold">NGC Plus</th>
                                <td>@if($collected->ngc_plus == 1)
                                        Yes
                                    @else
                                        No
                                    @endif</td>
                                <th class="fw-bold">NGC Star</th>
                                <td>@if($collected->ngc_star == 1)
                                        Yes
                                    @else
                                        No
                                    @endif</td>
                            </tr>
                            <tr>
                                <th class="fw-bold">Slab Generation</th>
                                <td>{{ $collected->slab_generation ?? 'Not saved' }}</td>
                                <th class="fw-bold">Special Labels</th>
                                <td>{{ $collected->special_label ?? 'Not saved' }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            @endif


            <div class="card mb-3">
                <div class="card-body">
                    <h5 class="card-title">Problems</h5>
                    <p>Full Problems Report</p>
                    <div class="row">
                        <div class="col-sm-4 mb-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="damaged" value="1"
                                       id="damaged">
                                <label class="form-check-label" for="damaged">
                                    Damaged
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-4 mb-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="holed" value="1"
                                       id="holed">
                                <label class="form-check-label" for="holed">
                                    Holed
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-4 mb-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="cleaned" value="1"
                                       id="cleaned">
                                <label class="form-check-label" for="cleaned">
                                    Cleaned
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-4 mb-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="polished" value="1"
                                       id="polished">
                                <label class="form-check-label" for="polished">
                                    Polished
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-4 mb-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="altered" value="1"
                                       id="altered">
                                <label class="form-check-label" for="altered">
                                    Altered
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-4 mb-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="scratched" value="1"
                                       id="scratched">
                                <label class="form-check-label" for="scratched">
                                    Scratched
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-4 mb-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="pvc_damage" value="1"
                                       id="pvc_damage">
                                <label class="form-check-label" for="pvc_damage">
                                    PVC Damage
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-4 mb-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="corrosion" value="1"
                                       id="corrosion">
                                <label class="form-check-label" for="corrosion">
                                    Corrosion
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-4 mb-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="bent" value="1" id="bent">
                                <label class="form-check-label" for="bent">
                                    Bent
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-4 mb-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="plugged" value="1"
                                       id="plugged">
                                <label class="form-check-label" for="plugged">
                                    Plugged
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-4 mb-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="bag_mark" value="1"
                                       id="bag_mark">
                                <label class="form-check-label" for="bag_mark">
                                    Bag/Contact Mark
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-4 mb-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="counterstamp" value="1"
                                       id="counterstamp">
                                <label class="form-check-label" for="counterstamp">
                                    Counterstamp
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-4 mb-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="whizzing" value="1"
                                       id="whizzing">
                                <label class="form-check-label" for="whizzing">
                                    Whizzing
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-4 mb-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="fingermarks" value="1"
                                       id="fingermarks">
                                <label class="form-check-label" for="fingermarks">
                                    Fingermarks
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-4 mb-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="spotted" value="1"
                                       id="spotted">
                                <label class="form-check-label" for="spotted">
                                    Spotted
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-4 mb-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="slide_marks" value="1"
                                       id="slide_marks">
                                <label class="form-check-label" for="slide_marks">
                                    Slide Marks
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <h4 class="mt-2">Notes</h4>
            <div class="form-group mb-3">
                <textarea id="note_body" name="note" class="form-control">

                </textarea>
                <button id="note_body_btn" type="button" class="btn btn-primary">Save</button>
            </div>


        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">{{ $collected->coin->coinName }}</div>
                <div class="card-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                        data-bs-toggle="dropdown" aria-expanded="false">
                                    Add New
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li><a class="dropdown-item"
                                           href="{{ route('collected.save_by_coin_id', ['id' => $collected->coin->id]) }}">Single
                                            Coin</a>
                                    </li>
                                    <li><a class="dropdown-item"
                                           href="{{ route('collected.save_by_coin_id', ['id' => $collected->coin->id]) }}">Multiple
                                            Coins</a>
                                    </li>
                                    <li><a class="dropdown-item"
                                           href="{{ route('collected.save_by_coin_id', ['id' => $collected->coin->id]) }}">Roll(s)</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                        data-bs-toggle="dropdown" aria-expanded="false">
                                    Add To
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li><a class="dropdown-item"
                                           href="{{ route('collected.save_by_coin_id', ['id' => $collected->coin->id]) }}">Sell List</a>
                                    </li>
                                    <li><a class="dropdown-item"
                                           href="{{ route('collected.save_by_coin_id', ['id' => $collected->coin->id]) }}">Multins</a>
                                    </li>
                                    <li><a class="dropdown-item"
                                           href="{{ route('collected.save_by_coin_id', ['id' => $collected->coin->id]) }}">Roll</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>




                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Reports</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="{{ route('coin.year_view', ['year' => $collected->coin->coinYear]) }}">All {{ $collected->coin->coinYear }}</a>
                                </li>
                                <li><a href="{{ route('coin.coin_id_grades', ['coin' => $collected->coin->id]) }}">Grade
                                        Report</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="http://www.maddieclashes.com/">MAD Die Clashes</a>
                                </li>
                                <li><a class="external_link" href="http://www.cuds-on-coins.com/">Cuds-On-Coins</a></li>
                                <li><a class="external_link" href="http://www.traildies.com/">Trail Dies</a></li>
                                <li><a class="external_link"
                                       href="http://ec2-13-58-222-16.us-east-2.compute.amazonaws.com/wiki/Home">VAMWorld</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Projects</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('user.user_types', ['type' => 'Research']) }}">Scholar
                                        Directory</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @push('styles')
        <style>
            .ck-editor__editable {
                min-height: 300px;
            }

            figure.zoom {
                background-position: 50% 50%;
                position: relative;
                margin: 100px auto;
                border: 5px solid white;
                box-shadow: -1px 5px 15px black;
                height: 300px;
                width: 500px;
                overflow: hidden;
                cursor: zoom-in;
            }

            figure.zoom img {
                transition: opacity 0.5s;
                display: block;
                width: 100%;
            }

            figure.zoom img:hover {
                opacity: 0;
            }


        </style>
    @endpush
    @push('scripts')
        <script src="https://cdn.ckeditor.com/ckeditor5/35.2.1/classic/ckeditor.js"></script>
        <script>
            (function (ENVIRONMENT) {
                try {
                    const token = document.head.querySelector('meta[name="csrf-token"]').content;

                    function zoom(e) {
                        var zoomer = e.currentTarget;
                        e.offsetX ? offsetX = e.offsetX : offsetX = e.touches[0].pageX
                        e.offsetY ? offsetY = e.offsetY : offsetX = e.touches[0].pageX
                        x = offsetX / zoomer.offsetWidth * 100
                        y = offsetY / zoomer.offsetHeight * 100
                        zoomer.style.backgroundPosition = x + '% ' + y + '%';
                    }

                    let editor;

                    ClassicEditor.defaultConfig = {
                        toolbar: {
                            items: [
                                'heading',
                                '|',
                                'bold',
                                'italic',
                                '|',
                                'bulletedList',
                                'numberedList',
                                '|',
                                'insertTable',
                                '|',
                                'undo',
                                'redo'
                            ]
                        },
                        image: {
                            toolbar: [
                                'imageStyle:full',
                                'imageStyle:side',
                                '|',
                                'imageTextAlternative'
                            ]
                        },
                        table: {
                            contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells']
                        },
                        language: 'en'
                    };

                    ClassicEditor
                        .create(document.querySelector('#note_body'))
                        .then(newEditor => {
                            editor = newEditor;
                            editor.setData('{!! $collected->notes[0]->note ?? '' !!}');
                        })
                        .catch(error => {
                            console.error(error);
                        });

                    // Locked  ------------------------------------------------------------------------------
                    let lock_collected = document.getElementById("lock_collected"); // save_collected_privacy
                    let lock_collect = document.getElementsByClassName('lock_collect');
                    Array.prototype.forEach.call(lock_collect, function(el, index, array){
                        el.addEventListener('focus', function () {

                            document.getElementById("locked_status").innerHTML = "Changing..";
                            let url = "{{ route('collected.save_collected_lock') }}";
                            console.log(url);
                            fetch(url, {
                                method: 'POST',
                                credentials: "same-origin",
                                headers: {
                                    'Content-Type': 'application/json',
                                    'Accept': 'application/json',
                                    "X-Requested-With": "XMLHttpRequest",
                                    "X-CSRF-TOKEN": token
                                },
                                body: JSON.stringify({"locked": this.value, "collect_id": '{{$collected->id}}'}),
                            }).then(
                                response => {
                                    response.json().then(
                                        data => {
                                            console.log(data);
                                            if (data.locked === '0') {
                                                document.getElementById("locked_status").innerHTML = "Unlocked";
                                                document.getElementById("locked_status").classList.add("text-success");
                                                document.getElementById("locked_status").classList.remove("text-danger");
                                            }else {
                                                document.getElementById("locked_status").innerHTML = "Locked";
                                                document.getElementById("locked_status").classList.add("text-danger");
                                                document.getElementById("locked_status").classList.remove("text-success");
                                            }
                                        }
                                    )
                                })
                        });
                    });


                    // Privacy  ------------------------------------------------------------------------------

                    let private_collect = document.getElementsByClassName('private_collect');
                    Array.prototype.forEach.call(private_collect, function(el, index, array){
                        el.addEventListener('focus', function () {
                            document.getElementById("private_status").innerHTML = "Changing..";
                            let url = "{{ route('collected.save_collected_privacy') }}";
                            console.log(url);
                            fetch(url, {
                                method: 'POST',
                                credentials: "same-origin",
                                headers: {
                                    'Content-Type': 'application/json',
                                    'Accept': 'application/json',
                                    "X-Requested-With": "XMLHttpRequest",
                                    "X-CSRF-TOKEN": token
                                },
                                body: JSON.stringify({"private": this.value, "collect_id": '{{$collected->id}}'}),
                            }).then(
                                response => {
                                    response.json().then(
                                        data => {
                                            console.log(data);
                                            if (data.private === '0') {
                                                document.getElementById("private_status").innerHTML = "Viewable";
                                                document.getElementById("private_status").classList.add("text-success");
                                                document.getElementById("private_status").classList.remove("text-danger");

                                            }else {
                                                document.getElementById("private_status").innerHTML = "Private";
                                                document.getElementById("private_status").classList.add("text-danger");
                                                document.getElementById("private_status").classList.remove("text-success");
                                            }
                                        }
                                    )
                                })
                        });
                    });

                    let coin_rank = document.getElementById("coin_rank");

                    let note_body_btn = document.getElementById("note_body_btn");

                    note_body_btn.addEventListener("click", function () {
                        note_body_btn.innerHTML = "Saving...";

                        const editorData = editor.getData();

                        let url = "{{ route('collected.save_collected_note') }}";
                        console.log(url);
                        fetch(url, {
                            method: 'POST',
                            credentials: "same-origin",
                            headers: {
                                'Content-Type': 'application/json',
                                'Accept': 'application/json',
                                "X-Requested-With": "XMLHttpRequest",
                                "X-CSRF-TOKEN": token
                            },
                            body: JSON.stringify({"note": editorData, "collect_id": '{{$collected->id}}'}),
                        }).then(
                            response => {
                                response.json().then(
                                    data => {
                                        if (ENVIRONMENT === 'local') {
                                            console.log(data);
                                        }
                                        editor.setData(data.note);
                                        note_body_btn.innerHTML = "Save";
                                    }
                                )
                            })
                    });

                    // no rank if not graded
                    @if(is_null($collected->grade))
                        coin_rank.innerHTML = 'Not Graded';
                    @else
                    // get coin rank if graded
                    window.addEventListener('load', (event) => {
                        let url = "{{ route('coin.coin_id_rank', [':collect']) }}".replace(':collect', {{$collected->id}});
                        fetch(url, {
                            "method": "GET",
                        }).then(
                            response => {
                                response.json().then(
                                    data => {
                                        if (ENVIRONMENT === 'local') {
                                            console.log(data);
                                        }
                                        coin_rank.innerHTML = data.rank;
                                    }
                                )
                            })
                    });
                    @endif


                    // get set/roll/... info
                    window.addEventListener('load', (event) => {
                        let url = "{{ route('collected.collected_get_info', [':collected']) }}"
                            .replace(':collected', {{$collected->id}});
                        fetch(url, {
                            "method": "GET",
                        }).then(
                            response => {
                                response.json().then(
                                    data => {
                                        if (ENVIRONMENT === 'local') {
                                            console.log(data);
                                        }
                                        //coin_rank.innerHTML = data.in_type;
                                    }
                                )
                            })
                    });

                    let remove_variety_btn = document.getElementById("remove_variety_btn");
                    let add_variety_btn = document.getElementById("add_variety_btn");
                    let current_varieties = document.getElementById("current_varieties");
                    let varietyList = document.getElementById("varietyList");
                    let varietyTbl = document.getElementById("varietyTbl");
                    let varietyTblRow = document.getElementById("varietyTblRow");

                    varietyList.addEventListener("change", function () {
                        delete variety_data;
                        varietyTblRow.innerHTML = '';

                        const csrf = document.querySelector('meta[name="csrf-token"]').content;
                        const csrf_field = '<input type="hidden" name="_token" value="' + csrf + '">';
                        variety_type = varietyList.options[varietyList.selectedIndex].value;
                        console.log(variety_type);
                        let url = "{{ route('collected.get_variety_list', [':coin_id', ':variety_type']) }}".replace(':coin_id', {{$collected->coin_id}}).replace(':variety_type', variety_type);

                        fetch(url, {
                            "method": "GET",
                        }).then(
                            response => {
                                response.json().then(
                                    data => {
                                        if (ENVIRONMENT === 'local') {

                                        }
                                        console.log(data);
                                        variety_data = "";

                                        data.variety_list.forEach((x) => {
                                            let save_url = "{{ route('collected.attach_variety') }}";
                                            variety_data += "<tr><td class='text-start'>" + x.label + "</td>";
                                            variety_data += "<td class='text-start'>" + x.designation + "</td>";
                                            variety_data += "<td class='text-start'>" + x.type + "</td>";
                                            variety_data += "<td class='text-start'>";
                                            variety_data += "<input class=\"form-check-input variety_check\" type=\"radio\" name=\"variety_radio\" value='" + x.id + "' id='variety_" + x.id + "'>";
                                            variety_data += "</td></tr>";
                                            jQuery('set_form_' + x.id).append(csrf_field);

                                        });
                                        jQuery("#varietyTbl").dataTable().fnDestroy();
                                        varietyList.selectedIndex = 0;
                                        varietyTblRow.innerHTML = variety_data;

                                        jQuery('#varietyTbl').DataTable( {
                                            "iDisplayLength": 10
                                        } );
                                    }
                                )
                            })
                    });
                    add_variety_btn.addEventListener('click', function (event) {
                        const variety_id = document.querySelector('input[name="variety_radio"]:checked').value;
                        fetch('{{ route('collected.attach_variety') }}', {
                            headers: {"Content-Type": "application/json; charset=utf-8"},
                            method: 'POST',
                            body: JSON.stringify({
                                _token: @json(csrf_token()),
                                collect_id: "{{$collected->id}}",
                                variety_id: variety_id,
                            })
                        }).then(response => response.json())
                            .then(data => {
                                console.log(data);

                            }).catch((error) => {
                            console.log(error)
                        })
                    }, false);
                    remove_variety_btn.addEventListener('click', function (event) {
                        this.innerText = 'Saving.....';
                        fetch('{{ route('collected.attach_variety') }}', {
                            headers: {"Content-Type": "application/json; charset=utf-8"},
                            method: 'POST',
                            body: JSON.stringify({
                                _token: @json(csrf_token()),
                                collect_id: "{{$collected->coin_id}}",
                                variety_id: "{{$variety}}",
                            })
                        }).then(response => response.json())
                            .then(data => {
                                console.log(data);
                                this.innerText = 'Saved';
                            }).catch((error) => {
                            console.log(error)
                        })
                    }, false);

                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection

