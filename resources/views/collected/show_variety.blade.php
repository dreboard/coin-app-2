@extends('layouts.user.main')
@section('pageTitle', 'View Coin')
@section('content')

    <div class="row">
        <div class="col-6 float-start mb-2">
            <h3 class="mt-4"><span id="coin_name">{{$variety->variety}}</span></h3>

        </div>
        <div class="col-6 mt-2">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    View
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">All {{ $collected->coin->coinYear }} {{ $collected->coin->type->coinType }}</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_type', ['id' => $collected->coin->type->id]) }}">{{ $collected->coin->type->coinType }}</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_category', ['id' => $collected->coin->category->id]) }}">{{ $collected->coin->category->coinCategory }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">All Coins</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('coin.view_type', ['id' => $collected->coin->type->id]) }}">{{ $collected->coin->type->coinType }}</a>
        </li>
        <li class="breadcrumb-item"><a
                href="{{ route('coin.view_category', ['id' => $collected->coin->category->id]) }}">{{ $collected->coin->category->coinCategory }}</a>
        </li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                <p>{{$variety->variety}} listings {{$variety->label}}</p>
            </div>
            <h4>{{$variety->label}}</h4>
            <p class="h5 fs-6">
                <span class="fw-bold">Coin: </span><a href="{{ route('coin.view_coin', ['coin' => $collected->coin_id]) }}">{{ $collected->coin->coinName }} @if(!in_array($collected->sub_type, ['Plain', 'None']))
                        {{$collected->sub_type}}
                    @endif  {{ $collected->coin->type->coinType }}</a>
            </p>
            <p class="h5 fs-6">
                <span class="fw-bold">Collected: </span><a href="{{ route('collected.view_collected', ['collected' => $collected->id]) }}">{{ $collected->nickname }}</a>
            </p>

            <a href="{{ route('collected.collected_variety_list', [
                                                    'collected' => $collected->id,
                                                    'variety' => str_replace(' ', '_', $variety->variety)
                                                    ]) }}">{{$variety->variety}}</a>

            @if($variety->variety == 'Broken Die Error')
                Broken Die Error
            @endif





        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">{{ $collected->coin->coinName }}</div>
                <div class="card-body">

                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                            data-bs-toggle="dropdown" aria-expanded="false">
                        Assign To Coin
                    </button>


                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                data-bs-toggle="dropdown" aria-expanded="false">
                            Assign To Coin
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.save_by_coin_id', ['id' => $collected->coin->id]) }}">Single
                                    Coin</a>
                            </li>
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.save_by_coin_id', ['id' => $collected->coin->id]) }}">Multiple
                                    Coins</a>
                            </li>
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.save_by_coin_id', ['id' => $collected->coin->id]) }}">Roll(s)</a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Reports</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="{{ route('coin.year_view', ['year' => $collected->coin->coinYear]) }}">All {{ $collected->coin->coinYear }}</a>
                                </li>
                                <li><a href="{{ route('coin.coin_id_grades', ['coin' => $collected->coin->id]) }}">Grade
                                        Report</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="http://www.maddieclashes.com/">MAD Die Clashes</a>
                                </li>
                                <li><a class="external_link" href="http://www.cuds-on-coins.com/">Cuds-On-Coins</a></li>
                                <li><a class="external_link" href="http://www.traildies.com/">Trail Dies</a></li>
                                <li><a class="external_link"
                                       href="http://ec2-13-58-222-16.us-east-2.compute.amazonaws.com/wiki/Home">VAMWorld</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">Projects</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">

                                <li><a href="{{ route('user.user_types', ['type' => 'Research']) }}">Scholar
                                        Directory</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @push('styles')
        <style>



        </style>
    @endpush
    @push('scripts')
        <script>
            (function (ENVIRONMENT) {
                try {
                    const token = document.head.querySelector('meta[name="csrf-token"]').content;

                    // Locked  ------------------------------------------------------------------------------
                    let lock_collected = document.getElementById("lock_collected"); // save_collected_privacy
                    let lock_collect = document.getElementsByClassName('lock_collect');
                    Array.prototype.forEach.call(lock_collect, function(el, index, array){
                        el.addEventListener('focus', function () {

                            let url = "{{ route('collected.save_collected_lock') }}";
                            console.log(url);
                            fetch(url, {
                                method: 'POST',
                                credentials: "same-origin",
                                headers: {
                                    'Content-Type': 'application/json',
                                    'Accept': 'application/json',
                                    "X-Requested-With": "XMLHttpRequest",
                                    "X-CSRF-TOKEN": token
                                },
                                body: JSON.stringify({"locked": this.value, "collect_id": '{{$collected->id}}'}),
                            }).then(
                                response => {
                                    response.json().then(
                                        data => {
                                            console.log(data);
                                            if (data.locked === '0') {
                                                document.getElementById("locked_status").innerHTML = "Unlocked";
                                                document.getElementById("locked_status").classList.add("text-success");
                                                document.getElementById("locked_status").classList.remove("text-danger");
                                            }else {
                                                document.getElementById("locked_status").innerHTML = "Locked";
                                                document.getElementById("locked_status").classList.add("text-danger");
                                                document.getElementById("locked_status").classList.remove("text-success");
                                            }
                                        }
                                    )
                                })
                        });
                    });

                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection

