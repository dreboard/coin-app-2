@extends('layouts.user.main')
@section('pageTitle', 'Edit An Article')
@section('content')

    <div class="row">
        <div class="col-6 float-start mb-2">
            <h3 class="mt-4"><span id="coin_name">{{ optional($collected)->nickname ?? 'No nickname' }}</span></h3>
            <p class="text-muted h5 fs-6"><span class="fw-bold">Coin:</span>
                <a href="{{ route('coin.view_coin', ['coin' => $collected->coin_id]) }}">{{ $collected->coin->coinName }} @if(!in_array($collected->sub_type, ['Plain', 'None'])) {{$collected->sub_type}} @endif  {{ $collected->coin->type->coinType }}</a>
            </p>
        </div>
        <div class="col-6 mt-2">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    View
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">All {{ $collected->coin->coinYear }} {{ $collected->coin->type->coinType }}</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_type', ['id' => $collected->coin->type->id]) }}">{{ $collected->coin->type->coinType }}</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('coin.view_category', ['id' => $collected->coin->category->id]) }}">{{ $collected->coin->category->coinCategory }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('coin.coin_index') }}">All Coins</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('coin.view_type', ['id' => $collected->coin->type->id]) }}">{{ $collected->coin->type->coinType }}</a>
        </li>
        <li class="breadcrumb-item"><a
                href="{{ route('coin.view_category', ['id' => $collected->coin->category->id]) }}">{{ $collected->coin->category->coinCategory }}</a>
        </li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <a class="btn btn-primary" href="{{ route('collected.view_collected', ['collected' => $collected->id]) }}">Back to {{ $collected->coin->coinName ?? 'Saved Coin'}}</a>

            <form id="edit_post_form" action="{{ route('collected.save_collected_images') }}"
                  method="post" id="groupForm" enctype="multipart/form-data" class="mt-3">
                @csrf
                <div class="col-md-12">
                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                    <input type="hidden" name="collected_id" value="{{$collected->id }}">
                    <div class="form-group mb-3">
                        <label for="text" class="form-label">Title</label>
                        <input type="text" class="form-control" name="nickname" id="nickname"
                               value="{{ old('nickname', $collected->nickname) }}">
                        @error('nickname')
                        <div class="text-sm text-red-600">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="card mb-3">
                        <div class="card-body">
                            @if (!empty($collected->images))
                                <div class="row mb-2">
                                    <div class="col-10 float-start">
                                        <h5>Current Images (<span id="current_count_display">{{$imgCount}}</span>) (Allowed up to 4)</h5>
                                    </div>
                                    <div class="col-2 float-end">
                                        <button data-bs-toggle="modal" data-bs-target="#deleteImagesModal" type="button" class="btn btn-danger">Delete All</button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="modal fade" id="deleteImagesModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h1 class="modal-title fs-5" id="exampleModalLabel">Delete All Images</h1>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <button id="image_delete_all_btn" type="button" class="btn btn-large btn-danger">Yes Delete All</button>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row row-cols-4 edit_preview_div text-center">
                                    @foreach($collected->images as $image)
                                        <div id="image_preview_div_{{$image->id}}" class="col form-check">
                                            <figure class="mb-4">
                                                <img id="image_tag_{{$image->id}}" class="edit_preview image_preview_tags" src="{{asset('storage/'.$image->image_url)}}"
                                                     alt=""/>
                                            </figure>
                                            <button type="button"
                                                    id="image_delete_btn_{{$image->id}}"
                                                    data-image_id="{{$image->id}}"
                                                    data-post_id="{{$collected->id}}"
                                                    onclick="deleteImg({{$image->id}})"
                                                    class="btn btn-danger image_delete_btns">
                                                <i class="bi bi-trash-fill"></i>
                                            </button>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                    {{-- Start Image--}}

{{--                    @if($imgCount == 4)--}}
                        <div id="images_full_div" class="card mb-3 alert alert-warning">
                            <div class="card-body">
                                <p>Your image count is 4, please delete an image above or change it</p>
                            </div>
                        </div>
{{--                    @else--}}
                        <div id="all_image_inputs_div" class="card mb-3">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-10"><h5 class="card-title">Add Images</h5></div>
                                    <div class="col-2">
                                        <button id="image_clear_btn" type="button" class="btn btn-danger">Clear All
                                        </button>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div id="image_div_1" class="mb-1">
                                            <label for="image_url_1" class="form-label">Select Image</label>
                                            <input class="form-control img_input" type="file" name="image_url[]"
                                                   id="image_url_1" onchange="preview('image_preview_frame_1')">
                                        </div>
                                        <div id="image_div_2" class="mb-1">
                                            <label for="image_url_2" class="form-label">Select Image</label>
                                            <input class="form-control img_input" type="file" name="image_url[]"
                                                   id="image_url_2" onchange="preview('image_preview_frame_2')">
                                        </div>
                                        <div id="image_div_3" class="mb-1">
                                            <label for="image_url_3" class="form-label">Select Image</label>
                                            <input class="form-control img_input" type="file" name="image_url[]"
                                                   id="image_url_3" onchange="preview('image_preview_frame_3')">
                                        </div>
                                        <div id="image_div_4" class="mb-1">
                                            <label for="image_url_4" class="form-label">Select Image</label>
                                            <input class="form-control img_input" type="file" name="image_url[]"
                                                   id="image_url_4" onchange="preview('image_preview_frame_4')">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-6 align-middle text-center"><img
                                                    id="image_preview_frame_1"
                                                    src=""
                                                    class="image_preview"/>
                                            </div>
                                            <div class="col-sm-6 align-middle text-center"><img
                                                    id="image_preview_frame_2"
                                                    src=""
                                                    class="image_preview"/>
                                            </div>
                                            <div class="col-sm-6 align-middle text-center"><img
                                                    id="image_preview_frame_3"
                                                    src=""
                                                    class="image_preview"/>
                                            </div>
                                            <div class="col-sm-6 align-middle text-center"><img
                                                    id="image_preview_frame_4"
                                                    src=""
                                                    class="image_preview"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
{{--                    @endif--}}


                    {{-- End Image--}}

                    <div class="form-group mb-3">
                        <label class="control-label">Message</label>
                        <textarea id="post_body" name="body"
                                  class="form-control">{{ old('nickname', $collected->nickname) }}</textarea>
                    </div>


                    <!-- Submit Form Input -->
                    <div class="col-3">
                        <button type="submit" class="btn btn-primary form-control">Update</button>
                    </div>
                </div>
            </form>
            <div class="mh-100"></div>
        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Your Unpublished</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">

                            <table class="table fixed_header table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">Unpublished</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
@push('styles')
    <style>
        .image_preview {
            max-height: 200px;
            width: 100%;
            height: auto;
        }

        .edit_preview_div img {
            max-height: 100px;
            width: 100%;
            height: auto;
        }

        .edit_preview_div2 img {
            object-fit: cover;
        }

        .edit_preview_div img {
            object-fit: cover;
        }
    </style>
@endpush
@push('scripts')
    <script src="https://cdn.ckeditor.com/ckeditor5/35.2.1/classic/ckeditor.js"></script>
    <script>
        try{
            let image_count = {{$imgCount}};
            let images_full_div = document.getElementById('images_full_div');
            let all_image_inputs_div = document.getElementById('all_image_inputs_div');
            let image_preview_frame_2 = document.getElementById('image_preview_frame_2');
            let image_preview_frame_3 = document.getElementById('image_preview_frame_3');
            let image_preview_frame_4 = document.getElementById('image_preview_frame_4');
            let image_url_1 = document.getElementById('image_url_1');
            let image_url_2 = document.getElementById('image_url_2');
            let image_url_3 = document.getElementById('image_url_3');
            let image_url_4 = document.getElementById('image_url_4');
            let image_div_1 = document.getElementById('image_div_1');
            let image_div_2 = document.getElementById('image_div_2');
            let image_div_3 = document.getElementById('image_div_3');
            let image_div_4 = document.getElementById('image_div_4');
            images_full_div.style.display = "none";

            if (image_count === 1) {image_div_4.style.display = "none";}
            if (image_count === 2) {image_div_3.style.display = "none";image_div_4.style.display = "none";}
            if (image_count === 3) {image_div_2.style.display = "none";image_div_3.style.display = "none";image_div_4.style.display = "none";}
            if (image_count === 4) {
                images_full_div.style.display = "block";
                all_image_inputs_div.style.display = "none";
            }

            function preview(id) {
                document.getElementById(id).style.display = "inline";
                document.getElementById(id).src = URL.createObjectURL(event.target.files[0]);
            }

            function deleteImg() {

            }

            function preview_2() {
                image_preview_frame_2.style.display = "inline";
                image_preview_frame_2.src = URL.createObjectURL(event.target.files[0]);
            }

            function preview_3() {
                image_preview_frame_3.style.display = "inline";
                image_preview_frame_3.src = URL.createObjectURL(event.target.files[0]);
            }

            function preview_4() {
                image_preview_frame_4.style.display = "inline";
                image_preview_frame_4.src = URL.createObjectURL(event.target.files[0]);
            }


            document.getElementById('image_clear_btn').addEventListener('click', function () {
                if (image_preview_frame_1 !== null) {image_preview_frame_1.src = "";}
                if (image_preview_frame_2 !== null) {image_preview_frame_2.src = "";}
                if (image_preview_frame_3 !== null) {image_preview_frame_3.src = "";}
                if (image_preview_frame_4 !== null) {image_preview_frame_4.src = "";}
                if (image_url_1 !== null) {image_url_1.value = '';}
                if (image_url_2 !== null) {image_url_2.value = '';}
                if (image_url_3 !== null) {image_url_3.value = '';}
                if (image_url_4 !== null) {image_url_4.value = '';}
            });
        } catch (error) {
            if (ENVIRONMENT === 'local') {
                console.error(error);
            }
        }

        (function (ENVIRONMENT) {
            try {


                let image_delete_btns = document.getElementsByClassName("image_delete_btns");
                let result_span = document.getElementById('result_span');
                let current_count_display = document.getElementById('current_count_display');

                for (var i = 0; i < image_delete_btns.length; i++) {
                    image_delete_btns[i].addEventListener('click', function () {
                        //console.log(this.dataset.image_id);
                        fetch('{{ route('collected.delete_collected_images') }}', {
                            headers: {"Content-Type": "application/json; charset=utf-8"},
                            method: 'POST',
                            body: JSON.stringify({
                                _token: @json(csrf_token()),
                                image_id: this.dataset.image_id,
                                image_model: "App\\Models\\Coins\\Collected",
                                image_model_id: this.dataset.post_id,
                            })
                        }).then(response => response.json())
                            .then(data => {
                                console.log(data);
                                if (data.success === 'Image Deleted') {

                                    current_count_display.innerHTML = data.image_count;
                                    document.getElementById('image_preview_div_' + this.dataset.image_id).style.display = "none";
                                    if (data.image_count === 1) {
                                        image_div_1.style.display = "block";
                                        image_div_2.style.display = "block";
                                        image_div_3.style.display = "block";
                                        image_div_4.style.display = "none";
                                        all_image_inputs_div.style.display = "block";
                                        images_full_div.style.display = "none";
                                    }
                                    if (data.image_count === 2) {
                                        image_div_1.style.display = "block";
                                        image_div_2.style.display = "block";
                                        image_div_3.style.display = "none";
                                        image_div_4.style.display = "none";
                                        all_image_inputs_div.style.display = "block";
                                        images_full_div.style.display = "none";
                                    }
                                    if (data.image_count === 3) {
                                        image_div_1.style.display = "block";
                                        image_div_2.style.display = "none";
                                        image_div_3.style.display = "none";
                                        image_div_4.style.display = "none";
                                        all_image_inputs_div.style.display = "block";
                                        images_full_div.style.display = "none";
                                    }
                                    if (data.image_count === 4) {
                                        images_full_div.style.display = "block";
                                        all_image_inputs_div.style.display = "none";
                                    }
                                    if (data.image_count === 0) {
                                        images_full_div.style.display = "none";
                                        image_div_1.style.display = "block";
                                        image_div_2.style.display = "block";
                                        image_div_3.style.display = "block";
                                        image_div_4.style.display = "block";
                                        all_image_inputs_div.style.display = "block";
                                    }
                                } else {
                                    result_span.classList.add('text-danger');
                                    result_span.innerHTML = 'Error In Unfollowing';
                                }
                            })
                    });
                }
                document.getElementById('image_delete_all_btn').addEventListener('click', function () {
                    //console.log(this.dataset.image_id);
                    fetch('{{ route('collected.delete_collected_images') }}', {
                        headers: {"Content-Type": "application/json; charset=utf-8"},
                        method: 'POST',
                        body: JSON.stringify({
                            _token: @json(csrf_token()),
                            image_id: this.dataset.image_id,
                            image_model: "App\\Models\\Coins\\Collected",
                            image_model_id: this.dataset.post_id,
                        })
                    }).then(response => response.json())
                        .then(data => {
                            console.log(data);
                            if (data.success === 'Image Deleted') {

                                current_count_display.innerHTML = data.image_count;
                                document.getElementById('image_preview_div_' + this.dataset.image_id).style.display = "none";
                                if (data.image_count === 1) {
                                    image_div_1.style.display = "block";
                                    image_div_2.style.display = "block";
                                    image_div_3.style.display = "block";
                                    image_div_4.style.display = "none";
                                    all_image_inputs_div.style.display = "block";
                                    images_full_div.style.display = "none";
                                }
                                if (data.image_count === 2) {
                                    image_div_1.style.display = "block";
                                    image_div_2.style.display = "block";
                                    image_div_3.style.display = "none";
                                    image_div_4.style.display = "none";
                                    all_image_inputs_div.style.display = "block";
                                    images_full_div.style.display = "none";
                                }
                                if (data.image_count === 3) {
                                    image_div_1.style.display = "block";
                                    image_div_2.style.display = "none";
                                    image_div_3.style.display = "none";
                                    image_div_4.style.display = "none";
                                    all_image_inputs_div.style.display = "block";
                                    images_full_div.style.display = "none";
                                }
                                if (data.image_count === 4) {
                                    images_full_div.style.display = "block";
                                    all_image_inputs_div.style.display = "none";
                                }
                                if (data.image_count === 0) {
                                    images_full_div.style.display = "none";
                                    image_div_1.style.display = "block";
                                    image_div_2.style.display = "block";
                                    image_div_3.style.display = "block";
                                    image_div_4.style.display = "block";
                                    all_image_inputs_div.style.display = "block";
                                }
                            } else {
                                result_span.classList.add('text-danger');
                                result_span.innerHTML = 'Error In Unfollowing';
                            }
                        })
                });

                ClassicEditor.defaultConfig = {
                    toolbar: {
                        items: [
                            'heading',
                            '|',
                            'bold',
                            'italic',
                            '|',
                            'bulletedList',
                            'numberedList',
                            '|',
                            'insertTable',
                            '|',
                            'undo',
                            'redo'
                        ]
                    },
                    image: {
                        toolbar: [
                            'imageStyle:full',
                            'imageStyle:side',
                            '|',
                            'imageTextAlternative'
                        ]
                    },
                    table: {
                        contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells']
                    },
                    language: 'en'
                };


                ClassicEditor
                    .create(document.querySelector('#post_body'))
                    .catch(error => {
                    console.error(error);
                });


            } catch (error) {
                if (ENVIRONMENT === 'local') {
                    console.error(error);
                }
            }
        })();



    </script>
@endpush
