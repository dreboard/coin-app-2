@extends('layouts.user.main')
@section('pageTitle', 'Add To Collection')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Add {{ $collected->coin->coinName }} {{ $collected->coin->type->coinType }}</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    Collection Pages
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_profile', ['id' => auth()->user()->id]) }}">Bacettings</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.edit_public', ['user' => auth()->user()->id]) }}">Edirofile</a>
                    </li>
                    <li><a class="dropdown-item"
                           href="{{ route('user.user_edit_avatar', ['user' => auth()->user()->id]) }}">Chaage</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.research') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('collected.start') }}">Back To Start</a></li>
        <li class="breadcrumb-item"><a
                href="{{ route('coin.view_coin', ['coin' => $collected->coin->id]) }}">{{ $collected->coin->coinName }} {{ $collected->coin->type->coinType }}</a>
        </li>
    </ol>
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">

            <div class="alert alert-primary" role="alert">
                Images, varieties and notes can be added to this coin on the view collected coins page..
            </div>

            <form id="edit_club_form" action="{{ route('collected.save_coin_id') }}"
                  method="post" id="groupForm" enctype="multipart/form-data">
                @csrf
                <div class="col-md-12">
                    <input type="hidden" name="coin_id" value="{{ $collected->coin->id }}">
                    <div class="mb-3">
                        <label for="text" class="form-label">Nickname</label>
                        <input type="text" class="form-control" name="nickname" id="nickname"
                               value="{{ old('nickname') }}">
                    </div>
                    @if($circulated ?? 1)
                        <div class="form-check">
                            <input class="form-check-input" value="1" type="radio" name="circulated" id="circulated1" checked>
                            <label class="form-check-label" for="circulated1">
                                Circulated
                            </label>
                        </div>
                        <div class="form-check mb-3">
                            <input class="form-check-input" value=""0 type="radio" name="circulated" id="circulated2">
                            <label class="form-check-label" for="circulated2">
                                Uncirculated
                            </label>
                        </div>
                    @endif





                    <div class="form-group mb-3">
                        <label for="coin_note" class="control-label">Article Text</label>
                        <textarea id="coin_note" name="coin_note" class="form-control">{{ old('coin_note') }}</textarea>
                    </div>




                    <div class="mb-3">
                        <table class="table table-hover datatable">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Created</th>
                                <th scope="col">Purchase</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($collected->coin->varieties as $variety)
                                <tr>
                                    <td>
                                        {{ $variety->variety }}
                                    </td>
                                    <td></td>
                                    <td>
                                        <div class="form-check">
                                            <input class="form-check-input has_more" type="checkbox" name="has_variety" value="{{ $variety->id }}"
                                                   id="has_variety">
                                            <label class="form-check-label" for="has_variety">
                                                {{ $variety->label }}
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td></td>
                                    <td>No varieties for this coin</td>
                                    <td></td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>

                    </div>







                    <!-- Submit Form Input -->
                    <div class="col-3">
                        <button type="submit" class="btn btn-primary form-control">Create</button>
                    </div>
                </div>
            </form>
            <div class="mh-100"></div>
        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <h6>Quick Add (details later)</h6>
                            <form class="row row-cols-lg-auto g-3 align-items-center" id="add_bulk_coin_form"
                                  action="{{ route('collected.quick_save_coin_id') }}">
                                <input type="hidden" name="coin_id" value="{{ $collected->coin->id }}">
                                <div class="col-12">
                                    <label class="visually-hidden" for="inlineFormSelectPref">Preference</label>
                                    <select name="coin_amount" class="form-select" id="inlineFormSelectPref" required>
                                        <option selected>Choose...</option>
                                        <option value="1">1</option>
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                    </select>
                                </div>

                                <div class="col-12">
                                    <button id="add_bulk_coin_form_btn" type="submit" class="btn btn-primary">Add
                                    </button>
                                </div>
                            </form>

                            <h6 class="mb-0">Recent Additions</h6>
                            @if(count($already_collected) > 0)
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Grade</th>
                                        <th scope="col">Saved</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($already_collected as $recent)
                                        <tr>
                                            <td>
                                                <a class="group_link"
                                                   href="{{ route('collected.view_collected', ['collected' => $recent->id]) }}"
                                                   title="{{$recent->grade}}">{{ Str::limit($recent->grade, 20) ?? 'None'}}</a>
                                            </td>
                                            <td>
                                                {{ \Carbon\Carbon::parse($recent->created_at)->diffForHumans() }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <p>None saved</p>
                            @endif


                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    @push('styles')
        <style>
            .ck-editor__editable {
                min-height: 300px;
            }
        </style>
    @endpush
    @push('scripts')
        <script>
            (function () {
                try {
                    // https://stackoverflow.com/questions/71201157/how-to-display-data-in-an-html-table-using-fetch-api
                    let tpg_service = document.getElementById("tpg_service");
                    let pro_div = document.getElementById("pro_div");
                    let ngc_div = document.getElementById("ngc_div");
                    let pcgs_div = document.getElementById("pcgs_div");
                    let anacs_div = document.getElementById("anacs_div");
                    let anacs_slab_generation = document.getElementById("anacs_slab_generation");
                    let ngc_slab_generation = document.getElementById("ngc_slab_generation");
                    let pcgs_slab_generation = document.getElementById("pcgs_slab_generation");

                    // selects
                    let ngc_special_label = document.getElementById("ngc_special_label");
                    let pcgs_special_label = document.getElementById("pcgs_special_label");

                    var default_checked = tpg_service.defaultChecked;
                    console.log(default_checked);
                    pro_div.style.display = "none";
                    ngc_div.style.display = "none";
                    pcgs_div.style.display = "none";
                    anacs_div.style.display = "none";

                    tpg_service.addEventListener("change", function () {
                        if (this.value === "NGC" || this.value === "PCGS" || this.value === "ICG" || this.value === "ANACS") {
                            pro_div.style.display = "flex";
                            const desc = this.selectedOptions[0].text;
                            console.log(`option desc`, desc);
                            console.log(this.value);
                        }
                        if (this.value === "NGC") {
                            pro_div.style.display = "flex";
                            ngc_div.style.display = "flex";
                            pcgs_div.style.display = "none";
                            anacs_div.style.display = "none";
                            pcgs_special_label.selectedIndex = -1;
                            pcgs_slab_generation.selectedIndex = -1;
                            anacs_slab_generation.selectedIndex = -1;
                        }
                        if (this.value === "ANACS") {
                            pro_div.style.display = "flex";
                            anacs_div.style.display = "flex";
                            ngc_div.style.display = "none";
                            pcgs_div.style.display = "none";
                            pcgs_special_label.selectedIndex = -1;
                            ngc_special_label.selectedIndex = -1;
                            pcgs_slab_generation.selectedIndex = -1;
                            ngc_slab_generation.selectedIndex = -1;
                        }
                        if (this.value === "PCGS") {
                            pro_div.style.display = "flex";
                            pcgs_div.style.display = "flex";
                            ngc_div.style.display = "none";
                            anacs_div.style.display = "none";
                            ngc_special_label.selectedIndex = -1;
                            anacs_slab_generation.selectedIndex = -1;
                            ngc_slab_generation.selectedIndex = -1;
                        }
                        if (this.value === "ICG") {
                            pro_div.style.display = "flex";
                            ngc_div.style.display = "none";
                            pcgs_div.style.display = "none";
                            anacs_div.style.display = "none";
                            pcgs_special_label.selectedIndex = -1;
                            ngc_special_label.selectedIndex = -1;
                            anacs_slab_generation.selectedIndex = -1;
                            ngc_slab_generation.selectedIndex = -1;
                            pcgs_slab_generation.selectedIndex = -1;
                        }
                        if (this.value === "None") {
                            pro_div.style.display = "none";
                            ngc_div.style.display = "none";
                            pcgs_div.style.display = "none";
                            anacs_div.style.display = "none";
                            pcgs_special_label.selectedIndex = -1;
                            ngc_special_label.selectedIndex = -1;
                            anacs_slab_generation.selectedIndex = -1;
                            ngc_slab_generation.selectedIndex = -1;
                            pcgs_slab_generation.selectedIndex = -1;
                        }
                    });

                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection
