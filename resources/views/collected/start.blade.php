@extends('layouts.user.main')
@section('pageTitle', 'Add To Collection')
@section('content')

    <div class="row">
        <div class="col-6 float-start">
            <h3 class="mt-4">Add Coin To Collection</h3>
        </div>
        <div class="col-6">
            <div class="dropdown mt-4 float-end">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown" aria-expanded="false">
                    My Sets
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item" href="{{ route('coin.coin_index') }}">My Coins</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Sets</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Folders</a></li>
                    <li><a class="dropdown-item" href="{{ route('coin.set_index') }}">My Rolls</a></li>
                </ul>
            </div>
        </div>
    </div>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="{{ route('user.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('collected.index') }}">Collection Home</a></li>
    </ol>

    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <div class="alert alert-primary" role="alert">
                You can add coins in 3 ways: <span class="fw-bold">Quick, Detailed and Very Detailed</span>
            </div>

            <div class="row">
                <div class="col-4">
                    <div class="callout callout-primary">
                        <h4>By Denomination</h4>
                        <div class="input-group mb-3">
                            <select id="coin_cat_select" class="form-select" aria-label="Default select example">
                                <option selected>Select Category</option>
                                @foreach($categories as $id => $category)
                                    <option value="{{$id}}">{{$category}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div id="coin_type_div" class="col-4">
                    <div class="callout callout-primary">
                        <h4>By Type</h4>
                        <div class="input-group mb-3">
                            <select id="coin_type_select" class="form-select" aria-label="Default select example">
                                <option selected>Select type</option>

                            </select>
                            <button type="button" id="coins_type_btn" class="btn btn-outline-primary">Load</button>
                        </div>
                    </div>
                </div>
                <div id="coin_type_img_div" class="col-4 pb-2">
                    <img id="coin_type_img" src=""/>
                </div>
            </div>


            <div class="table-responsive">
                <table id="coins_type_tbl" class="table">
                    <thead>
                    <tr>
                        <th class="fw-bold">Name</th>
                        <th class="fw-bold">Quick Add</th>
                        <th class="fw-bold">Add</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th class="fw-bold">Name</th>
                        <th class="fw-bold">Quick Add</th>
                        <th class="fw-bold">Add</th>
                    </tr>
                    </tfoot>
                    <tbody id="coins_type_row"></tbody>
                </table>
            </div>

        </div>

        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Add More</div>
                <div class="card-body">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                data-bs-toggle="dropdown" aria-expanded="false">
                            Start Pages
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.start_mintset') }}">Mintset</a>
                            </li>
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.start_folder') }}">Folder</a>
                            </li>
                            <li><a class="dropdown-item"
                                   href="{{ route('collected.start_roll') }}">Rolls</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>


            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Resources</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="https://www.error-ref.com/">Error-Variety Ready
                                        Reference</a></li>
                                <li><a class="external_link" href="https://minterrornews.com/">Mint Error News</a></li>
                                <li><a class="external_link" href="https://conecaonline.org/">CONECA</a></li>
                                <li><a class="external_link" href="http://www.doubleddie.com/1801.html">Wexler’s Die
                                        Varieties</a></li>
                                <li><a class="external_link" href="http://varietyvista.com/Attribution%20Services.htm">Variety
                                        Vista Attribution Services</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a class="external_link" href="http://www.maddieclashes.com/">MAD Die Clashes</a>
                                </li>
                                <li><a class="external_link" href="http://www.cuds-on-coins.com/">Cuds-On-Coins</a></li>
                                <li><a class="external_link" href="http://www.traildies.com/">Trail Dies</a></li>
                                <li><a class="external_link"
                                       href="http://ec2-13-58-222-16.us-east-2.compute.amazonaws.com/wiki/Home">VAMWorld</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @if(optional(auth()->user())->status !== 'warn')
                <div class="card mb-4">
                    <div class="card-header">Connect</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('group.all') }}">Group Directory</a></li>
                                    <li><a href="{{ route('group.mine') }}">My Groups</a></li>
                                    <li><a href="{{ route('user.groups_i_follow') }}">Groups I Follow</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="list-unstyled mb-0">
                                    <li><a href="{{ route('user.view_directory') }}">User Directory</a></li>
                                    <li><a href="{{ route('user.user_who_follow_me') }}">My Followers</a></li>
                                    <li><a href="{{ route('user.users_i_follow') }}">Users I Follow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        </div>
    </div>
    @push('scripts')
        <script>

            (function () {
                try {

                    const csrf = document.querySelector('meta[name="csrf-token"]').content;
                    const csrf_field = '<input type="hidden" name="_token" value="' + csrf + '">';
                    // https://stackoverflow.com/questions/71201157/how-to-display-data-in-an-html-table-using-fetch-api
                    let coin_type_div = document.getElementById("coin_type_div");
                    let coin_cat_select = document.getElementById("coin_cat_select");
                    let coin_type_select = document.getElementById("coin_type_select");
                    let coins_type_row = document.getElementById("coins_type_row");
                    let coin_type_img = document.getElementById("coin_type_img");
                    let coins_type_btn = document.getElementById("coins_type_btn");
                    coin_type_div.style.display = "none";

                    coin_type_select.addEventListener("change", function () {
                        coins_type_row.innerHTML = '';
                        coin_type_img.src = '';
                    });
                    coin_cat_select.addEventListener("change", function () {
                        coin_type_select.innerHTML = '';
                        coins_type_row.innerHTML = '';
                        coin_type_img.src = '';
                        coin_cat_id = coin_cat_select.options[coin_cat_select.selectedIndex].value;

                        let url = "{{ route('coin.populate_cat', [':coin_cat_id']) }}".replace(':coin_cat_id', coin_cat_id);
                        console.log(url);
                        fetch(url, {
                            "method": "GET",
                        }).then(
                            response => {
                                response.json().then(
                                    data => {
                                        var temp = "";
                                        Object.entries(data.coin_types).forEach(entry => {
                                            const [key, value] = entry;
                                            console.log(key, value);
                                            temp += "<option value='" + key + "'>";
                                            temp += value;
                                            temp += "</option>"
                                        });
                                        coin_type_div.style.display = "block";
                                        coin_type_select.innerHTML += temp;
                                    }
                                )
                            })
                    });


                    coins_type_btn.addEventListener("click", function () {
                        coins_type_btn.innerHTML = 'Loading...';
                        coins_type_btn.disabled = true;
                        coin_type_id = coin_type_select.options[coin_type_select.selectedIndex].value;
                        let url = "{{ route('coin.populate_type', [':coin_type_id']) }}".replace(':coin_type_id', coin_type_id);
                        console.log(url);
                        coins_type_row.innerHTML = '';
                        fetch(url, {
                            "method": "GET",
                        }).then(
                            response => {
                                response.json().then(
                                    data => {
                                        if (ENVIRONMENT === 'local') {
                                            console.log(data);
                                        }

                                        var temp = "";

                                        data.coins.forEach((x) => {
                                            let quick_url = "{{ route('collected.quick_save_coin_id') }}";
                                            let detail_url = "{{ route('collected.save_by_coin_id', [':coin_id']) }}".replace(':coin_id', x.id);
                                            let very_detail_url = "{{ route('collected.save_by_coin_id', [':coin_id']) }}".replace(':coin_id', x.id);
                                            let multiple_url = "{{ route('collected.save_by_coin_id', [':coin_id']) }}".replace(':coin_id', x.id);
                                            temp += "<tr>";
                                            temp += "<td class='text-start w-75'>" + x.coinName + "</td>";
                                            temp += "<td class='text-start'><form id='coin_form_" + x.id + "' method=\"POST\" class=\"row row-cols-lg-auto g-3 align-items-center\" action='" + quick_url + "'>";
                                            temp += "<input type=\"hidden\" name=\"coin_id\" value='" + x.id + "'>";
                                            temp += "<input type=\"hidden\" name=\"_token\" value='" + csrf + "'>";
                                            temp += "<div class=\"col-12\"><button type=\"submit\" class=\"btn btn-primary\">Quick Add</button></div></form></td>";
                                            temp += "<td><div class=\"dropdown\"><button class=\"btn btn-secondary dropdown-toggle\" type=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">More</button>";
                                            temp += "<ul class=\"dropdown-menu\">"
                                            temp += "<li><a class=\"dropdown-item\" href='" + detail_url + "'>Detailed</a></li>";
                                            temp += "<li><a class=\"dropdown-item\" href='" + very_detail_url + "'>Very Detailed</a></li></ul></div></td>";
                                            temp += "<li><a class=\"dropdown-item\" href='" + multiple_url + "'>Multiple</a></li></ul></div></td>";
                                            temp += "</tr>"
                                            jQuery('coin_form_' + x.id).append(csrf_field);

                                        });
                                        coins_type_row.innerHTML = temp;
                                        jQuery('#coins_type_tbl').dataTable();
                                        coin_type_img.src = data.image;
                                        coins_type_btn.innerHTML = 'Load';
                                        coins_type_btn.disabled = false;
                                    }
                                )
                            })
                    });
                } catch (error) {
                    if (ENVIRONMENT === 'local') {
                        console.error(error);
                    }
                }
            })();
        </script>
    @endpush
@endsection

