
// external_link
(function ($) {
    try {
        $( document ).ready( function( $ ) {
            $('.datatable').DataTable();
            $( 'a.external_link' ).click( function( event ){
                event.preventDefault();
                window.open(( this ).href, '_blank');
            });

            $('.dropdown-menu a').click(function () {
                $('button[data-toggle="dropdown"]').text('Loading...');
            });

        });
    } catch (error) {

    }


}(window.jQuery));

window.addEventListener('DOMContentLoaded', event => {

    // Toggle the side navigation
    const sidebarToggle = document.body.querySelector('#sidebarToggle');
    if (sidebarToggle) {
        // Uncomment Below to persist sidebar toggle between refreshes
        // if (localStorage.getItem('sb|sidebar-toggle') === 'true') {
        //     document.body.classList.toggle('sb-sidenav-toggled');
        // }
        sidebarToggle.addEventListener('click', event => {
            event.preventDefault();
            document.body.classList.toggle('sb-sidenav-toggled');
            localStorage.setItem('sb|sidebar-toggle', document.body.classList.contains('sb-sidenav-toggled'));
        });
    }

});


