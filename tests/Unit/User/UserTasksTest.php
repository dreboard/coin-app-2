<?php

namespace Tests\Unit\User;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Tests\UserTestCase;

class UserTasksTest extends UserTestCase
{


    /**
     * Unit test setup.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->task_user = User::factory()->create();
        $this->unverified_task_user = User::factory()->unverified()->create();
    }


    public function test_users_can_authenticate_using_the_login_screen()
    {
        $this->withoutExceptionHandling();
        $this->withMiddleware();
        $response = $this->post('/login', [
            'email' => $this->task_user->email,
            'password' => self::USER_PASSWORD,

        ]);

        $this->assertAuthenticated();
        $response->assertRedirect(RouteServiceProvider::HOME);
    }


    /** @test */
    public function test_users_can_view_dashboard_when_active()
    {
        $this->withoutExceptionHandling();
        $response = $this->actingAs($this->task_user)->get(route('user.user_profile'));
        $response->assertViewIs("users.settings.profile");
    }



    /**
     * @test
     *
     * not authenticated and requested a protected route, you'll receive a redirection response to login with status 302
     */
    public function test_users_cannot_view_dashboard_when_unverified()
    {
        $this->withoutExceptionHandling();
        $response = $this->actingAs($this->unverified_task_user)->get(route('user.dashboard'));
        $response->assertRedirect('verify-email');
    }

    /**
     * @test
     * not authenticated and requested a protected route, you'll receive a redirection response to login with status 302
     */
    public function test_user_current_password_required_on_change_password()
    {
        $this->withExceptionHandling();
        $this->withOutMiddleware();

        $response = $this->actingAs($this->task_user)->post('user/user_save_password', [
            'current_password' => '', //$this->active_user->password,
            'password' => self::USER_PASSWORD,
            'confirm_password' => self::USER_PASSWORD
        ]);
        $response->assertSessionHasErrors([
            'current_password' => 'The current password field is required.'
        ]);
    }

    public function test_user_new_password_required_on_change_password()
    {
        $this->withExceptionHandling();
        $this->withoutMiddleware();

        $response = $this->actingAs($this->task_user)->post('user/user_save_password', [
            'current_password' => self::USER_PASSWORD, //$this->active_user->password,
            'password' => '',
            'confirm_password' => self::USER_PASSWORD,
        ]);

        $response->assertSessionHasErrors([
            'password' => 'The password field is required.'
        ]);
    }

    public function test_user_confirm_password_required_on_change_password()
    {
        $this->withExceptionHandling();
        $this->withoutMiddleware();

        $response = $this->actingAs($this->task_user)->post('user/user_save_password', [
            'current_password' => self::USER_PASSWORD, //$this->active_user->password,
            'password' => self::USER_PASSWORD,
            'confirm_password' => '',
        ]);

        $response->assertSessionHasErrors([
            'confirm_password' => 'The confirm password field is required.'
        ]);
    }


    /**
     * @test
     *
     * basic users can not access admin dashboard
     */
    public function test_basic_user_cannot_view_admin_dashboard()
    {
        $this->withoutExceptionHandling();
        $response = $this->actingAs($this->task_user)->get(route('admin.dashboard'));
        $response->assertRedirect('user/dashboard');
    }




}
