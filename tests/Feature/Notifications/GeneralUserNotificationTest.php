<?php

namespace Notifications;

use App\Notifications\GeneralUserNotification;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PHPUnit\Framework\TestCase;
use Tests\UserTestCase;

class GeneralUserNotificationTest extends UserTestCase
{

    public function test_general_user_notification_sent()
    {
        Notification::fake();
        Mail::fake();
        $post_data = [
            'user_id' => $this->active_user->id,
            'subject' => 'Test subject',
            'body' => 'Test body'
        ];

        Notification::send($this->active_user, new GeneralUserNotification($post_data));
        //Mail::assertSent(GeneralUserNotification::class);
        Notification::assertSentTo(
            [$this->active_user], GeneralUserNotification::class
        );
    }

}
