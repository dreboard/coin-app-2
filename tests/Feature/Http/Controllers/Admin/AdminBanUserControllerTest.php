<?php

namespace Http\Controllers\Admin;

use App\Events\User\BannedUser;
use App\Events\User\UnbanUser;
use App\Http\Controllers\Admin\AdminBanUserController;
use App\Listeners\Banned\EnterBannedUser;
use App\Listeners\Banned\SendBannedUserNotification;
use App\Listeners\Banned\SendRestoreUserNotification;
use App\Mail\Users\NotifyBannedUser;
use App\Mail\Users\RestoreBannedUser as RestoreMail;
use App\Models\User;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PHPUnit\Framework\TestCase;
use Tests\UserTestCase;

class AdminBanUserControllerTest extends UserTestCase
{

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

    }


    public function view_all_banned_users()
    {
        $users = [$this->active_user,$this->another_user,$this->warned_user];

        $this->view('admin.user-all-banned', [
            'users' => $users,
            'errors' => [],
            'status' => $this->active_user->status,
            'reported' => 0,
            'loginsToday' => 2,
            'loginsMonth' => 2,
            'loginsYear' => 2,
            'createdToday' => 2,
            'createdMonth' => 2,
            'createdYear' => 2
        ]);

        $response = $this->actingAs($this->admin_user)->get(route('admin.view_banned_users'));
        $this->assertEquals('200', $response->getStatusCode());
        $response->assertViewIs('admin.user-all-banned');
        $response->assertSeeText('Banned Users');
    }


    public function test_ban_a_user()
    {
        Event::fake();
        $response = $this->actingAs($this->admin_user)->post(route('admin.ban_user'), [
            'user_id' => $this->active_user->id,
            'length' => '7',
        ])->assertRedirect();
        Event::assertDispatched(BannedUser::class);
        $response->assertSessionHasNoErrors();
        $response->assertValid(['user_id', 'length']);
    }


    public function test_ban_a_user_error()
    {
        Event::fake();
        $response = $this->actingAs($this->admin_user)->post(route('admin.ban_user'), [
            'user_id' => 'Zero',
            'length' => '7',
        ]);
        Event::assertNotDispatched(BannedUser::class);
        $response->assertInvalid(['user_id']);
        $response->assertSessionHasErrors();
    }


    public function test_ban_a_user_not_found_error()
    {
        Event::fake();
        $response = $this->actingAs($this->admin_user)->post(route('admin.ban_user'), [
            'user_id' => 0,
            'length' => '7',
        ]);
        Event::assertNotDispatched(BannedUser::class);
        $response->assertSessionHasErrors();
    }


    public function test_unban_a_user()
    {
        Event::fake();
        $this->actingAs($this->admin_user)->get('/admin/unban_user/'.$this->active_user->id)
            ->assertRedirect();
        Event::assertDispatched(UnbanUser::class);
    }


    public function test_unban_expired_banned_users()
    {
        $this->actingAs($this->admin_user)->get('/admin/unban_all_users/')
            ->assertRedirect();
    }

    public function test_unban_a_user_error()
    {
        $this->withExceptionHandling();
        $this->withOutMiddleware();
        Event::fake();
        $response = $this->actingAs($this->admin_user)->get('/admin/unban_user/0');
        Event::assertNotDispatched(UnbanUser::class);
        $response->assertSessionHasErrors();
    }




    public function test_ban_event_mail_sent_to_user()
    {
        $this->withoutExceptionHandling();
        Notification::fake();
        Mail::fake();

        $event = new BannedUser($this->active_user, 7);
        $listener = new SendBannedUserNotification();
        $listener->handle($event);

        Mail::assertSent(NotifyBannedUser::class);
    }

    public function test_unban_event_mail_sent_to_user()
    {
        $this->banable_user = User::factory()->create();
        $this->withoutExceptionHandling();
        Notification::fake();
        Mail::fake();

        $event = new UnbanUser($this->banable_user);
        $listener = new SendRestoreUserNotification();
        $listener->handle($event);

        Mail::assertSent(RestoreMail::class);
    }


    public function test_db_listeners_attached_banned_event()
    {
        Event::fake();
        Event::assertListening(
            BannedUser::class,
            EnterBannedUser::class
        );
    }


    public function test_mail_listeners_attached_banned_event()
    {
        Event::fake();
        Event::assertListening(
            BannedUser::class,
            SendBannedUserNotification::class
        );
    }

    public function test_account_warning_banner()
    {
        $this->withoutExceptionHandling();
        $response = $this->actingAs($this->warned_user)->get(route('user.user_profile'));
        $response->assertSeeText('The administrator has issued warning(s) for your account');
    }

    public function test_account_dont_see_warning_banner()
    {
        $this->withoutExceptionHandling();
        $response = $this->actingAs($this->active_user)->get(route('user.user_profile'));
        $response->assertDontSeeText('Your Account is in warning status');
    }
}
