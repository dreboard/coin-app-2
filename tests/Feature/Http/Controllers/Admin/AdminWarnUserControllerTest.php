<?php

namespace Http\Controllers\Admin;

use App\Events\User\WarnUser;
use App\Http\Controllers\Admin\AdminWarnUserController;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Notification;
use PHPUnit\Framework\TestCase;
use Tests\UserTestCase;

class AdminWarnUserControllerTest extends UserTestCase
{

    public function test_warn_user()
    {
        Event::fake();
        $this->withoutExceptionHandling();
        $this->withMiddleware();
        Notification::fake();
        $response = $this->actingAs($this->admin_user)->post('admin/warn_user', [
            'user_id' => $this->active_user->id,
            'status' => 'warn',
        ]);
        Event::assertDispatched(WarnUser::class);
        $this->assertDatabaseHas('users', [
            'id' => $this->active_user->id,
            'status' => 'warn',
        ]);

        $this->assertEquals('302', $response->getStatusCode());
    }

    public function test_unwarn_user()
    {
        $this->withoutExceptionHandling();
        $this->withMiddleware();
        $response = $this->actingAs($this->admin_user)->post('admin/warn_user', [
            'user_id' => $this->warned_user->id,
            'status' => 'good',
        ]);
        $this->assertDatabaseHas('users', [
            'id' => $this->warned_user->id,
            'status' => 'good',
        ]);
        $this->assertEquals('302', $response->getStatusCode());

    }


    public function test_warn_user_with_error()
    {
        Event::fake();
        $this->withoutExceptionHandling();
        $this->withMiddleware();
        $response = $this->actingAs($this->admin_user)->post('admin/warn_user', [
            'user_id' => 0,
            'status' => 'warn',
        ]);
        Event::assertNotDispatched(WarnUser::class);
        $this->assertDatabaseHas('users', [
            'id' => $this->active_user->id,
            'status' => 'good',
        ]);
        $response->assertSessionHasErrors();
    }
}
