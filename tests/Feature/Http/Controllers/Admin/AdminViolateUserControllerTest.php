<?php

namespace Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminViolateUserController;
use App\Models\User;
use App\Models\Users\Violation;
use PHPUnit\Framework\TestCase;
use Tests\UserTestCase;

class AdminViolateUserControllerTest extends UserTestCase
{

    public function test_view_violations_reported_against_user()
    {
        $this->another_violating_user = User::factory()->create();
        $view = $this->view('admin.user.violations_reported_against', [
            'user' => $this->active_user,
            'errors' => [],
            'status' => $this->active_user->status,
            'reported' => 0,
            'user_list' => [$this->another_violating_user]
        ]);

        $violation = Violation::factory()->create([
            'reporter' => $this->active_user->id,
            'violator' => $this->another_violating_user->name,

        ]);
        $post_data = [
            'user_id' => $this->active_user->id
        ];
        $response = $this->actingAs($this->admin_user)->get(route('admin.violations_against_user', $post_data));
        $this->assertEquals('200', $response->getStatusCode());
        $response->assertViewIs('admin.user.violations_reported_against');
        $this->assertDatabaseHas('violations', [
            'reporter' => $this->active_user->id,
            'violator' => $this->another_violating_user->name,
        ]);
    }


    public function test_view_violations_reported_by_user()
    {
        $this->another_violating_user = User::factory()->create();
        $view = $this->view('admin.user.violations_reported_by', [
            'user' => $this->active_user,
            'errors' => [],
            'status' => $this->active_user->status,
            'reported' => 0,
            'user_list' => [$this->another_violating_user]
        ]);

        $violation = Violation::factory()->create([
            'reporter' => $this->active_user->id,
            'violator' => $this->another_violating_user->name,

        ]);
        $post_data = [
            'user_id' => $this->active_user->id
        ];
        $response = $this->actingAs($this->admin_user)->get(route('admin.violations_by_user', $post_data));
        $this->assertEquals('200', $response->getStatusCode());
        $response->assertViewIs('admin.user.violations_reported_by');
        $this->assertDatabaseHas('violations', [
            'reporter' => $this->active_user->id,
            'violator' => $this->another_violating_user->name,
        ]);
    }


    public function test_view_all_pending_violations()
    {
        $this->another_violating_user = User::factory()->create();

        $violations = Violation::factory()->create([
            'reporter' => $this->active_user->id,
            'violator' => $this->another_violating_user->name,

        ]);
        $this->view('admin.user.violations_reported_by', [
            'violations' => $violations,
            'user' => $this->active_user,
            'errors' => [],
            'status' => $this->active_user->status,
            'reported' => 0,
            'user_list' => [$this->another_violating_user]
        ]);
        $response = $this->actingAs($this->admin_user)->get(route('admin.violation_pending'));
        $this->assertEquals('200', $response->getStatusCode());
        $response->assertViewIs('admin.violations.pending');
    }


    public function test_view_all_violations()
    {
        $this->another_violating_user = User::factory()->create();

        $violations = Violation::factory()->create([
            'reporter' => $this->active_user->id,
            'violator' => $this->another_violating_user->name,

        ]);
        $this->view('admin.user.violations_reported_by', [
            'violations' => $violations,
            'user' => $this->active_user,
            'errors' => [],
            'status' => $this->active_user->status,
            'reported' => 0,
            'user_list' => [$this->another_violating_user]
        ]);
        $response = $this->actingAs($this->admin_user)->get(route('admin.violation_all'));
        $this->assertEquals('200', $response->getStatusCode());
        $response->assertViewIs('admin.violations.all');
    }


    public function test_view_violations_by_type()
    {
        $this->withoutExceptionHandling();
        $this->withOutMiddleware();

        $violation_type = 'Terms of Services';
        $this->another_violating_user = User::factory()->create();

        $violations[] = Violation::factory()->create([
            'reporter' => $this->active_user->id,
            'violator' => $this->another_violating_user->name,
            'violation_type' => $violation_type,
        ]);
        $this->withViewErrors([
            'errors' => [],
        ])->view('admin.violations.type', [
            'violations' => $violations,
            'violation_type' => $violation_type,
            'errors' => [],
            'action' => 'Case Pending',
        ]);
        $post_data = [
            'violation_type' => $violation_type
        ];
        $response = $this->actingAs($this->admin_user)->get(route('admin.reported_type', $post_data));
        $this->assertEquals('200', $response->getStatusCode());
        $response->assertViewIs('admin.violations.type');
        $response->assertSeeText($violation_type);
    }


}
