<?php

namespace Http\Controllers\Admin;

use App\Models\User;
use App\Notifications\GeneralUserNotification;
use Illuminate\Support\Facades\Notification;
use Tests\UserTestCase;

class AdminUserActionsControllerTest extends UserTestCase
{

    public function test_admin_can_view_a_user()
    {
        $this->searchable_user = User::factory()->create();
        $view = $this->view('admin.user.view', [
            'user' => $this->searchable_user,
            'errors' => [],
            'status' => $this->searchable_user->status,
            'reported' => 0
        ]);
        $post_data = [
            'user_id' => $this->searchable_user->id
        ];
        $response = $this->actingAs($this->admin_user)->get(route('admin.view_user', $post_data));
        $this->assertEquals('200', $response->getStatusCode());
        $response->assertViewIs('admin.user.view');
    }

    public function test_admin_can_view_a_user_with_error()
    {
        $post_data = [
            'user_id' => 0
        ];
        $response = $this->actingAs($this->admin_user)->get(route('admin.view_user', $post_data));
        $this->assertEquals('302', $response->getStatusCode());
        $response->assertSessionHasErrors();
    }

    public function test_admin_can_find_a_user()
    {
        $this->searchable_user = User::factory()->create();
        $view = $this->view('admin.user-results', [
            'users' => $this->searchable_user,
            'errors' => [],
            'status' => $this->searchable_user->status,
        ]);
        $post_data = [
            'searchTerm' => $this->searchable_user->email
        ];
        $response = $this->actingAs($this->admin_user)->post(route('admin.find_user', $post_data));
        $this->assertEquals('200', $response->getStatusCode());
        $response->assertViewIs('admin.user-results');
        $view->assertSee('Search Results');
    }

    public function test_admin_can_view_all_users(){

        $users = [$this->active_user,$this->another_user,$this->warned_user];

        $view = $this->view('admin.user.all', [
            'users' => $users,
            'errors' => [],
            'status' => $this->active_user->status,
            'reported' => 0,
            'loginsToday' => 2,
            'loginsMonth' => 2,
	        'loginsYear' => 2,
            'createdToday' => 2,
            'createdMonth' => 2,
            'createdYear' => 2
        ]);

        $response = $this->actingAs($this->admin_user)->get(route('admin.view_users'));
        $this->assertEquals('200', $response->getStatusCode());
        $response->assertViewIs('admin.user.all');

    }

    public function test_admin_can_view_a_user_logins()
    {
        $view = $this->view('admin.user.logins', [
            'user' => $this->active_user,
            'errors' => [],
            'status' => $this->active_user->status,
            'reported' => 0,
            'last_login' => ''
        ]);
        $post_data = [
            'user_id' => $this->active_user->id
        ];
        $response = $this->actingAs($this->admin_user)->get(route('admin.view_user_logins', $post_data));
        $this->assertEquals('200', $response->getStatusCode());
        $response->assertViewIs('admin.user.logins');
    }

    public function test_admin_can_view_a_user_logins_with_errors()
    {
        $post_data = [
            'user_id' => 0
        ];
        $response = $this->actingAs($this->admin_user)->get(route('admin.view_user_logins', $post_data));
        $this->assertEquals('302', $response->getStatusCode());
        $response->assertSessionHasErrors();
    }

    public function test_admin_can_delete_a_user()
    {
        $this->deleteable_user = User::factory()->create();
        $post_data = [
            'user_id' => $this->deleteable_user->id
        ];
        $response = $this->actingAs($this->admin_user)->get(route('admin.delete_user', $post_data));
        $this->assertEquals('302', $response->getStatusCode());
        $response->assertSessionHasErrors();
        $this->assertModelMissing($this->deleteable_user);
        $this->assertDatabaseMissing('users', [
            'id' => $this->deleteable_user->id,
        ]);
    }

    public function test_admin_can_verify_a_user()
    {
        $this->verifyable_user = User::factory()->create([
            'email_verified_at' => null
        ]);
        $post_data = [
            'user_id' => $this->verifyable_user->id
        ];
        $response = $this->actingAs($this->admin_user)->post(route('admin.verify_user', $post_data));
        $this->assertEquals('302', $response->getStatusCode());
        $this->assertDatabaseHas('users', [
            'id' => $this->verifyable_user->id,
        ]);
    }

    public function test_admin_can_clone_a_user()
    {
        $this->withoutExceptionHandling();
        $this->withOutMiddleware();
        $this->cloneable_user = User::factory()->create();
        $post_data = [
            'user_id' => $this->cloneable_user->id
        ];
        $response = $this->actingAs($this->admin_user)->get(route('admin.clone_user', $post_data));
        $this->assertEquals('302', $response->getStatusCode());
        $response->assertSessionHasErrors();
    }


    public function test_admin_can_change_a_user_status()
    {
        $this->verifyable_user = User::factory()->create([
            'status' => 1
        ]);
        $post_data = [
            'user_id' => $this->verifyable_user->id
        ];
        $response = $this->actingAs($this->admin_user)->post(route('admin.verify_user', $post_data));
        $this->assertEquals('302', $response->getStatusCode());
        $this->assertDatabaseHas('users', [
            'id' => $this->verifyable_user->id,
        ]);
    }


    public function test_admin_can_view_send_user_message_form(){

        $this->withoutExceptionHandling();
        $this->withOutMiddleware();
        $view = $this->withViewErrors([
            'errors' => [],
        ])->view('admin.messages.to_user', [
            'user' => $this->active_user,
            'status' => 'good',
            'id' => $this->active_user->id
        ]);
        $post_data = [
            'user_id' => $this->active_user->id
        ];
        $response = $this->actingAs($this->admin_user)->get(route('admin.message_user', $post_data));
        $this->assertEquals('200', $response->getStatusCode());
        $response->assertViewIs('admin.messages.to_user');
        $view->assertSee('Send Message to '.$this->active_user->name);
    }



    public function test_admin_can_send_user_a_message(){

        $this->withoutExceptionHandling();
        $this->withOutMiddleware();
        Notification::fake();
        $post_data = [
            'user_id' => $this->active_user->id,
            'subject' => $this->active_user->id,
            'body' => $this->active_user->id
        ];
        $response = $this->actingAs($this->admin_user)->post(route('admin.message_user_save', $post_data));
        $this->assertEquals('302', $response->getStatusCode());
        Notification::assertSentTo(
            [$this->active_user], GeneralUserNotification::class
        );
    }


    public function test_admin_can_send_user_a_message_with_error(){

        $this->withExceptionHandling();
        $this->withOutMiddleware();
        Notification::fake();
        $post_data = [
            'user_id' => 0,
            'subject' => $this->active_user->id,
            'body' => $this->active_user->id
        ];
        $response = $this->actingAs($this->admin_user)->post(route('admin.message_user_save', $post_data));
        $this->assertEquals('302', $response->getStatusCode());
        $response->assertSessionHasErrors();
        Notification::assertNotSentTo(
            [$this->active_user], GeneralUserNotification::class
        );
    }

    public function test_admin_can_view_send_system_message_form(){

        $this->withoutExceptionHandling();
        $this->withOutMiddleware();
        $view = $this->withViewErrors([
            'errors' => [],
        ])->view('admin.message_new');
        $response = $this->actingAs($this->admin_user)->get(route('admin.message_new'));
        $this->assertEquals('200', $response->getStatusCode());
        $response->assertViewIs('admin.message_new');
        $view->assertSee('Create System Message');
    }
}
