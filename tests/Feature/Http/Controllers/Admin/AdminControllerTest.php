<?php

namespace Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminController;
use PHPUnit\Framework\TestCase;
use Tests\UserTestCase;

class AdminControllerTest extends UserTestCase
{

    public function index()
    {

        $this->withExceptionHandling();
        $this->withOutMiddleware();
        $this->actingAs($this->admin_user);

        $view = $this->withViewErrors([
            //'name' => ['Please provide a valid name.']
        ])->view('admin.dashboard');

        $view->assertSee('Admin Dashboard');
        /**/
        $response = $this->actingAs($this->admin_user)->get(route('admin.dashboard'));
        $response->dumpSession();
        $response->assertSessionHasErrors([
            'name' => ['Please provide a valid name.']
        ]);
        $this->assertEquals('200', $response->getStatusCode());
        $response->assertViewIs('admin.dashboard');
    }

    public function testIndex()
    {

        $this->withoutExceptionHandling();
        //$this->withOutMiddleware();
        $this->actingAs($this->admin_user);

        $response = $this->actingAs($this->admin_user)->get(route('admin.dashboard'));
        $response->assertSessionHasErrors();
        $this->assertEquals('200', $response->getStatusCode());
       // $response->assertViewIs('admin.dashboard');
    }
}
