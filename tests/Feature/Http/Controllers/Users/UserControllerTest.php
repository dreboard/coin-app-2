<?php

namespace Http\Controllers\Users;

use App\Http\Controllers\Users\UserController;
use App\Models\User;
use App\Notifications\GeneralUserNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Tests\UserTestCase;

class UserControllerTest extends UserTestCase
{




    public function test_user_can_view_profile()
    {
        $this->withoutExceptionHandling();
        $response = $this->actingAs($this->active_user)->get(route('user.user_profile'));
        $response->assertSeeText('My Profile');
    }

    public function test_user_can_view_warning_page()
    {
        $this->withoutExceptionHandling();
        $response = $this->actingAs($this->active_user)->get(route('user.warning_page'));
        $response->assertSeeText('Warning Page');
    }

}
