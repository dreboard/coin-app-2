<?php

namespace Http\Controllers\Users;

use App\Http\Controllers\Users\UserActionsController;
use App\Notifications\GeneralUserNotification;
use Illuminate\Support\Facades\Hash;
use PHPUnit\Framework\TestCase;
use Tests\UserTestCase;

class UserActionsControllerTest extends UserTestCase
{

    public function test_user_can_toggle_visibility_public()
    {
        $this->withoutExceptionHandling();
        $this->withOutMiddleware();
        $response = $this->actingAs($this->active_user)->post('user/user_change_visibility', [
            'profile_visibility' => 1,
            'id' => $this->active_user->id
        ]);
        $this->assertEquals('200', $response->getStatusCode());
    }

    public function test_user_can_toggle_visibility_private()
    {
        $this->withoutExceptionHandling();
        $this->withOutMiddleware();
        $response = $this->actingAs($this->active_user)->post('user/user_change_visibility', [
            'profile_visibility' => 0,
            'id' => $this->active_user->id
        ]);
        $this->assertEquals('200', $response->getStatusCode());
    }

    public function test_another_user_can_not_change_your_visibility()
    {
        $this->withExceptionHandling();
        $this->withOutMiddleware();
        $response = $this->actingAs($this->another_user)->post('user/user_change_visibility', [
            'profile_visibility' => 1,
            'id' => $this->active_user->id
        ]);
        $this->assertEquals('401', $response->getStatusCode());
    }



    public function test_user_can_view_profile_page()
    {
        $this->withoutExceptionHandling();
        $response = $this->actingAs($this->active_user)->get(route('user.user_profile'));
        $response->assertSeeText('My Profile');
    }

    public function test_user_can_view_profile()
    {
        $this->withoutExceptionHandling();
        $this->withOutMiddleware();
        $this->actingAs($this->active_user);
        $view = $this->withViewErrors([
            'errors' => ['An Error'],
            'name' => $this->active_user->name
        ])->view('users.settings.profile');

        $view->assertSee($this->active_user->name);
    }

    public function test_user_can_save_password()
    {
        $this->withExceptionHandling();
        $this->withOutMiddleware();
        $new_password = 'password2';
        $post_data = [
            'current_password' => 'password',
            'password' => $new_password,
            'confirm_password' => $new_password
        ];

        $response = $this->actingAs($this->active_user)->post('user/user_save_password', $post_data);
        $this->assertEquals('302', $response->getStatusCode());
        $this->active_user->refresh();
        $this->assertTrue(Hash::check($new_password, $this->active_user->password));
        $response->assertSessionHasNoErrors();
    }

    public function test_user_can_save_password_error()
    {
        $this->withExceptionHandling();
        $this->withOutMiddleware();
        $new_password = 'password2';
        $post_data = [
            'current_password' => 'password2',
            'password' => $new_password,
            'confirm_password' => $new_password
        ];

        $response = $this->actingAs($this->active_user)->post('user/user_save_password', $post_data);
        $this->assertEquals('302', $response->getStatusCode());
        $response->assertSessionHasErrors();
    }


    public function test_user_can_view_change_password_page()
    {
        $this->withoutExceptionHandling();
        $response = $this->actingAs($this->active_user)->get(route('user.user_change_password'));
        $response->assertSeeText('Change Password');
        $response->assertViewIs('users.settings.edit');
    }

    public function test_user_can_view_change_email_page()
    {
        $this->withoutExceptionHandling();
        $response = $this->actingAs($this->active_user)->get(route('user.user_change_email'));
        $response->assertSeeText('Change Email');
        $response->assertViewIs('users.settings.edit-mail');
    }



    public function test_user_can_save_email()
    {
        $this->withExceptionHandling();
        $this->withOutMiddleware();
        $new_email = $this->faker->unique()->safeEmail();
        $post_data = [
            'email' => $new_email,
            'confirm_email' => $new_email
        ];

        $response = $this->actingAs($this->active_user)->post('user/user_save_email', $post_data);
        $this->assertEquals('302', $response->getStatusCode());
        $this->active_user->refresh();
        $this->assertDatabaseHas('users', [
            'email' => $new_email,
            'email_verified_at' => null,
            'id' => $this->active_user->id,
        ]);
        $response->assertSessionHasNoErrors();
    }

    public function test_user_error_on_save_email()
    {
        $this->withExceptionHandling();
        $this->withOutMiddleware();

        $post_data = [
            'email' => '',
            'confirm_email' => ''
        ];

        $response = $this->actingAs($this->active_user)->post('user/user_save_email', $post_data);
        $this->assertEquals('302', $response->getStatusCode());

        $response->assertSessionHasErrors([
            'email' => 'The email field is required.',
            'confirm_email' => 'The confirm email field is required.',
        ]);
    }

    public function test_user_error_on_save_email_confirm()
    {
        $this->withExceptionHandling();
        $this->withOutMiddleware();
        $post_data = [
            'email' => $this->faker->unique()->safeEmail(),
            'confirm_email' => $this->faker->unique()->safeEmail()
        ];

        $response = $this->actingAs($this->active_user)->post('user/user_save_email', $post_data);
        $this->assertEquals('302', $response->getStatusCode());
        $response->assertSessionHasErrors([
            'email' => 'The email and confirm email must match.'
        ]);
    }

    public function test_user_can_delete_account()
    {
        $this->withExceptionHandling();
        $this->withOutMiddleware();
        $post_data = [
            'user_id' => $this->active_user->id
        ];
        $response = $this->actingAs($this->active_user)->post('user/user_delete_account', $post_data);
        $this->assertEquals('302', $response->getStatusCode());
        $this->assertDatabaseMissing('users', [
            'id' => $this->active_user->id,
        ]);
        $this->assertModelMissing($this->active_user);
    }

    public function test_user_can_delete_account_with_error()
    {
        $this->withExceptionHandling();
        $this->withOutMiddleware();
        $post_data = [
            'user_id' => $this->active_user->id
        ];
        $response = $this->actingAs($this->another_user)->post('user/user_delete_account', $post_data);
        $this->assertEquals('302', $response->getStatusCode());
        $this->assertDatabaseHas('users', [
            'id' => $this->active_user->id,
        ]);
        $response->assertSessionHasErrors();
    }



    public function test_user_can_delete_a_message()
    {
        $this->withoutExceptionHandling();
        $this->active_user->notify(new GeneralUserNotification([
            'subject' => 'Test Subject',
            'body' => 'Test body',
            'from' => 'The Administrator'
        ]));
        $notification = $this->active_user
            ->notifications
            ->where('notifiable_id', $this->active_user->id)
            ->first();

        $response = $this->actingAs($this->active_user)->post(route('user.system_message_delete', [
            'id' => $notification->id,
        ]));
        $this->assertEquals('200', $response->getStatusCode());
        $response->assertViewIs('users.system.all');
    }


    public function test_user_can_delete_all_messages()
    {
        $this->withoutExceptionHandling();
        $this->active_user->notify(new GeneralUserNotification([
            'subject' => 'Test Subject',
            'body' => 'Test body',
            'from' => 'The Administrator'
        ]));
        $this->active_user->notify(new GeneralUserNotification([
            'subject' => 'Test Subject',
            'body' => 'Test body',
            'from' => 'The Administrator'
        ]));

        $response = $this->actingAs($this->active_user)->post(route('user.system_message_delete_all', ['user_id' => $this->active_user->id]));
        $this->assertDatabaseMissing('notifications', [
            'notifiable_id' => $this->active_user->id,
            'notifiable_type' => 'App\Notifications\GeneralUserNotification',
        ]);
        $this->assertEquals('302', $response->getStatusCode());
    }

}
