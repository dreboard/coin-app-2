<?php

namespace Http\Controllers\Users;

use App\Http\Controllers\Users\DashController;
use PHPUnit\Framework\TestCase;
use Tests\UserTestCase;

class DashControllerTest extends UserTestCase
{

    public function test_user_can_view_dashboard()
    {
        $this->withExceptionHandling();
        $this->withOutMiddleware();
        $response = $this->actingAs($this->active_user)->get(route('user.dashboard'));
        $response->assertSeeText('My Dashboard');
    }

    public function test_user_can_view_policies_page()
    {
        $this->withExceptionHandling();
        $this->withOutMiddleware();
        $response = $this->actingAs($this->active_user)->get(route('policies'));
        $response->assertSeeText('Site Policies');
    }

    public function test_user_can_view_a_policy_type_page()
    {
        $this->withExceptionHandling();
        $this->withOutMiddleware();
        $response = $this->actingAs($this->active_user)->get(route('view_policy', ['id' => 3]));
        $response->assertSeeText('Terms of Services');
    }
}
