<?php

namespace Http\Controllers\Users;


use App\Events\User\BannedUser;
use App\Events\User\WarnUser;
use App\Http\Controllers\Users\UserViolationsController;
use App\Models\User;
use App\Models\Users\Violation;
use App\Notifications\GeneralUserNotification;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Tests\Unit\User\UserTasksTest;

class UserViolationsControllerTest extends UserTasksTest
{

    public function test_user_can_view_create_violation_form()
    {
        $this->withoutExceptionHandling();
        $response = $this->actingAs($this->active_user)->get(route('user.violation_create'));
        $response->assertSeeText('Report User');
    }

    public function test_user_can_create_a_new_violation()
    {
        //$this->withoutExceptionHandling();
        $this->withExceptionHandling();
        $this->withOutMiddleware();
        Storage::fake('violations');
        Notification::fake();
        $violator = User::factory()->create();

        $file = UploadedFile::fake()->image('violations.jpg');
        $response = $this->actingAs($this->active_user)->post('user/violation_save', [
            'violation_type' => 'Anti Spam Policy',
            'violator' => $violator->name,
            'details' => 'Details of incident',
            'reporter' => $this->active_user->id,
            'url' => 'http://localhost/example-app/public/user/violation_create',
            'screenshot' => $file
        ]);

        Notification::assertSentTo(
            [$this->active_user], GeneralUserNotification::class
        );
        $this->assertEquals('302', $response->getStatusCode());
        $this->assertDatabaseHas('violations', [
            'violator' => $violator->name,
            'reporter' => $this->active_user->id,
            'violation_type' => 'Anti Spam Policy'
        ]);
    }


    public function test_user_can_update_violation_with_warned()
    {
        //$this->withoutExceptionHandling();
        $this->withExceptionHandling();
        $this->withOutMiddleware();
        Notification::fake();
        Event::fake();
        $this->another_violating_user = User::factory()->create();
        $violation = Violation::factory()->create([
            'reporter' => $this->active_user->id,
            'violator' => $this->another_violating_user->name,

        ]);

        $response = $this->actingAs($this->active_user)->post('user/violation_update', [
            'action' => UserViolationsController::ViolationActions['ACTION_WARNED'],
            'violation_id' => $violation->id,
            'violator_id' => $this->another_violating_user->id,
            'reporter' => $this->active_user->id,
        ]);

        $response->assertSessionHasNoErrors();
        $this->assertEquals('302', $response->getStatusCode());
        $this->assertDatabaseHas('violations', [
            'id' => $violation->id,
            'violator_id' => $this->another_violating_user->id,
            'action' => UserViolationsController::ViolationActions['ACTION_WARNED'],
        ]);
        Notification::assertSentTo(
            [$this->active_user], GeneralUserNotification::class
        );
        Event::assertDispatched(WarnUser::class);
    }


    public function test_user_can_update_violation_with_banned()
    {
        $this->withExceptionHandling();
        $this->withOutMiddleware();
        Notification::fake();
        Event::fake();
        $this->another_violating_user = User::factory()->create();
        $violation = Violation::factory()->create([
            'reporter' => $this->active_user->id,
            'violator' => $this->another_violating_user->name,

        ]);

        $response = $this->actingAs($this->active_user)->post('user/violation_update', [
            'action' => UserViolationsController::ViolationActions['ACTION_BANNED'],
            'violation_id' => $violation->id,
            'violator_id' => $this->another_violating_user->id,
            'reporter' => $this->active_user->id,
        ]);

        $response->assertSessionHasNoErrors();
        $this->assertEquals('302', $response->getStatusCode());
        $this->assertDatabaseHas('violations', [
            'id' => $violation->id,
            'violator_id' => $this->another_violating_user->id,
            'action' => UserViolationsController::ViolationActions['ACTION_BANNED'],
        ]);
        Notification::assertSentTo(
            [$this->active_user], GeneralUserNotification::class
        );
        Event::assertDispatched(BannedUser::class);
    }


    public function test_multiple_users_notified_on_violation_update_with_banned()
    {
        $this->withExceptionHandling();
        $this->withOutMiddleware();
        Notification::fake();
        Event::fake();
        $this->another_reporting_user = User::factory()->create();
        $this->another_violating_user = User::factory()->create();
        $violation = Violation::factory()->create([
            'reporter' => $this->active_user->id,
            'violator' => $this->another_violating_user->name,
        ]);
        $violation_2 = Violation::factory()->create([
            'reporter' => $this->another_reporting_user->id,
            'violator' => $this->another_violating_user->name,
        ]);

        $response = $this->actingAs($this->active_user)->post('user/violation_update', [
            'action' => UserViolationsController::ViolationActions['ACTION_BANNED'],
            'violation_id' => $violation->id,
            'violator_id' => $this->another_violating_user->id,
            'reporter' => $this->active_user->id,
        ]);

        $response->assertSessionHasNoErrors();
        $this->assertEquals('302', $response->getStatusCode());
        $this->assertDatabaseHas('violations', [
            'id' => $violation->id,
            'violator_id' => $this->another_violating_user->id,
            'reporter' => $this->active_user->id,
            'action' => UserViolationsController::ViolationActions['ACTION_BANNED'],
        ]);
        $this->assertDatabaseHas('violations', [
            'id' => $violation_2->id,
            'reporter' => $this->another_reporting_user->id,
            'action' => UserViolationsController::ViolationActions['ACTION_BANNED'],
        ]);
        $this->assertDatabaseMissing('violations', [
            'violator_id' => $this->another_violating_user->id,
            'violator' => $this->another_violating_user->name,
            'action' => UserViolationsController::ViolationActions['ACTION_PENDING'],
        ]);
        Notification::assertSentTo(
            [$this->active_user], GeneralUserNotification::class
        );
        Notification::assertSentTo(
            [$this->another_reporting_user], GeneralUserNotification::class
        );
        Event::assertDispatched(BannedUser::class);
    }

    public function test_user_can_create_a_new_violation_failed()
    {
        $this->withoutExceptionHandling();
        $this->withOutMiddleware();
        Storage::fake('violations');
        Notification::fake();

        $file = UploadedFile::fake()->image('violations.jpg');

        $response = $this->actingAs($this->active_user)->post('user/violation_save', [
            'violation_type' => 'Anti Spam Policy',
            'violator' => '',
            'details' => '',
            'reporter' => $this->active_user->id,
            'url' => 'http://localhost/example-app/public/user/violation_create',
            'screenshot' => $file
        ]);

        Notification::assertNotSentTo(
            [$this->active_user], GeneralUserNotification::class
        );
        $response->assertSessionHasErrors([
            'violator' => 'The violator field is required.',
            'details' => 'The details field is required.',
        ]);
    }


    public function test_user_can_view_a_violation()
    {
        $this->withExceptionHandling();
        $this->withOutMiddleware();
        $violation = Violation::factory()->create([
            'reporter' => $this->active_user->id,
            'violator' => $this->violating_user->name,
            'violator_id' => $this->violating_user->id,
        ]);

        $response = $this->actingAs($this->active_user)->get(route('user.violation_view', [
            'id' => $violation->id,
        ]));

        $response->assertSeeText('Pending Violations');
    }

    public function test_violator_can_view_a_violation_against_them()
    {
        $this->withExceptionHandling();
        $this->withOutMiddleware();
        $violation = Violation::factory()->create([
            'reporter' => $this->active_user->id,
            'violator' => $this->violating_user->name,
        ]);

        $response = $this->actingAs($this->violating_user)->get(route('user.violation_view', [
            'id' => $violation->id,
        ]));

        $response->assertSeeText('Pending Violations');
    }

    public function test_another_user_cannot_view_my_violation()
    {
        $this->withExceptionHandling();
        $this->withOutMiddleware();

        $violation = Violation::factory()->create([
            'reporter' => $this->active_user->id,
            'violator' => $this->violating_user->name,
            'violator_id' => $this->violating_user->id,
        ]);

        $response = $this->actingAs($this->another_user)->get(route('user.violation_view', [
            'id' => $violation->id,
        ]));

        $response->assertRedirect(route('user.dashboard'));
    }


    public function test_user_can_view_reported_by_violations()
    {
        $this->withoutExceptionHandling();
        $response = $this->actingAs($this->active_user)->get(route('user.violation_reported'));
        $response->assertSeeText('My Reported Violations');
    }

    public function test_user_can_view_reported_against_violations()
    {
        $this->withoutExceptionHandling();
        $response = $this->actingAs($this->active_user)->get(route('user.violation_against_user'));
        $response->assertSeeText('Violations Reported Against You');
    }

}
