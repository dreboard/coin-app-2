<?php

namespace Http\Controllers\Users;

use App\Http\Controllers\Users\UserMessageController;
use App\Notifications\GeneralUserNotification;
use PHPUnit\Framework\TestCase;
use Tests\UserTestCase;

class UserMessageControllerTest extends UserTestCase
{


    public function test_user_can_view_a_message()
    {
        $this->withoutExceptionHandling();
        $this->active_user->notify(new GeneralUserNotification([
            'subject' => 'Test Subject',
            'body' => 'Test body',
            'from' => 'The Administrator'
        ]));
        $notification = $this->active_user
            ->notifications
            ->where('notifiable_id', $this->active_user->id)
            ->first();

        $response = $this->actingAs($this->active_user)->get(route('user.system_message_view', ['id' => $notification->id]));
        $response->assertSeeText('Test Subject');
    }

    public function test_user_can_view_messages()
    {
        $this->withoutExceptionHandling();
        $response = $this->actingAs($this->active_user)->get(route('user.message_all'));
        $response->assertSeeText('My Messages');
    }



}
