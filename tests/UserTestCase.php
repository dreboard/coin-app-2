<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

abstract class UserTestCase extends TestCase
{

    use RefreshDatabase;

    use WithFaker;

    protected const USER_PASSWORD = 'password';

    protected const PASSWORD_MSG = [
        'current_password' => 'The current password field is required.',
        'password' => 'The password field is required.',
        'confirm_password' => 'The confirm password field is required.'
    ];

    protected User $unverified_user;

    /**
     * @var
     */
    protected User $banned_user;

    /**
     * @var
     */
    protected User $active_user;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->active_user = User::factory()->create();
        $this->another_user = User::factory()->create();
        $this->warned_user = User::factory()->create([
            'status' => 'warn'
        ]);
        $this->violating_user = User::factory()->create();
        $this->unverified_user = User::factory()->unverified()->create();
        $this->admin_user = User::factory()->isAdmin()->create();
    }

}
