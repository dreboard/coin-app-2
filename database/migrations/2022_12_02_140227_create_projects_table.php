<?php

use App\Models\Comment;
use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // comments, reports, notes and ratings
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description');
            $table->text('overview')->nullable();
            $table->integer('progress')->default(0);
            $table->integer('private')->default(0);
            $table->integer('views')->default(0);
            $table->string('type'); // ['Research', 'General', 'Variety']
            $table->text('data')->nullable(); // die markers and die stage, numbering system
            $table->text('image_url')->nullable();
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('coin_id');
            $table->unsignedBigInteger('err_id');
            $table->integer('join_id'); // generated floor(time()-999999999)
            $table->integer('projectable_id')->nullable();
            $table->string('projectable_type')->nullable();
            $table->timestamps();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('project_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('project_id')->index();
            $table->string('project_level')->default('Full'); // ['Manager', 'Associate', 'Guest', 'Invited']
            $table->timestamp('approved_at')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->index(['project_id', 'user_id']);
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('project_id')
                ->references('id')
                ->on('projects')
                ->onDelete('cascade');
        });


        Schema::create('project_request', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('project_id')->index();
            $table->index(['project_id', 'user_id']);
            $table->timestamps();

            $table->foreign('project_id')
                ->references('id')
                ->on('projects')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id');
            $table->text('comment');
            $table->integer('score');
            $table->integer('reviewable_id')->unsigned();
            $table->string('reviewable_type');
            $table->timestamps();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        $project_user = User::find(1);
        $project_associate = User::find(2);
        $project_1 = Project::factory()
            ->create([
                'user_id' => $project_user->id,
                'title' => 'Collecting United\'s Official Project',
                'description' => 'To find Lincoln Cent Varieties and Errors',
                'overview' => 'To find Lincoln Cent Varieties and Errors',
                'type' => 'General',
                'private' => 0,
                'views' => 33,
                'projectable_id' => 1,
                'projectable_type' => 'App\Models\Groups',
            ]);

        DB::table('project_user')->insert([
            'user_id' => $project_user->id,
            'project_level' => 'Manager',
            'project_id' => $project_1->id,
            'created_at' => now(),
            'updated_at' => now(),
            'approved_at' => now(),
        ]);
        DB::table('project_user')->insert([
            'user_id' => $project_associate->id,
            'project_level' => 'Associate',
            'project_id' => $project_1->id,
            'created_at' => now(),
            'updated_at' => now(),
            'approved_at' => now(),
        ]);
        DB::table('project_request')->insert([
            'user_id' => 3,
            'project_id' => $project_1->id,
            'created_at' => now()
        ]);

        User::factory()->hasAttached(
            Project::factory(),
            ['project_id' => $project_1->id, 'project_level' => 'Associate']
        )->count(7)->create();

        $members = DB::table('project_user')
            ->select('project_user.user_id AS commenter')
            ->where('project_user.project_id', '=', $project_1->id)
            ->get();
        foreach ($members as $k) {
            Comment::factory()->count(rand(1,2))->for(
                $project_1, 'commentable'
            )->create([
                'user_id' => $k->commenter,
            ]);
        }

        // OTHER projects
        $project_2 = Project::factory()
            ->create([
                'user_id' => 4,
                'title' => '1938 D Lincoln Wheat RPM',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla.',
                'overview' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla.',
                'type' => 'General',
                'private' => 0,
                'views' => 12,
            ]);
        $project_3 = Project::factory()
            ->create([
                'user_id' => 5,
                'title' => '1888 Barber Dime DDO',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla.',
                'overview' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla.',
                'type' => 'General',
                'private' => 0,
                'views' => 7,
            ]);



        User::factory()->hasAttached(
            Project::factory(),
            ['project_id' => $project_2->id, 'project_level' => 'Associate']
        )->count(3)->create();

        Project::factory()
            ->count(10)
            ->create();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
        Schema::dropIfExists('project_user');
        Schema::dropIfExists('project_request');
        Schema::dropIfExists('reviews');
    }
};
