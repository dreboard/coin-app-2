<?php

use App\Models\Coins\Collected;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collected_coins', function (Blueprint $table) {
            $table->id();
            $table->string('nickname')->nullable();
            $table->unsignedBigInteger('user_id')->index();
            $table->integer('coin_id')->index();
            $table->string('sub_type')->default('Plain');
            $table->string('obv')->nullable();
            $table->string('rev')->nullable();
            $table->string('mms')->nullable();

            $table->string('grade')->nullable();
            $table->string('old_grade')->nullable();
            $table->integer('net_grade')->default(0);
            $table->integer('crack_out')->default(0);
            $table->integer('crossover')->default(0);
            $table->integer('dnc')->default(0);
            $table->integer('reholder')->default(0);
            $table->string('image_service')->nullable(); //ANACS Imaging,PCGS TrueView,NGC Internet Imaging,NGC Internet Imaging
            $table->integer('chop_mark')->default(0); // did not cross
            $table->string('point_grade')->nullable();
            $table->string('adjectival_grade')->nullable();
            $table->string('ten_grade')->nullable();
            $table->string('die_state')->nullable();
            $table->string('gradable')->default(1);
            $table->integer('circulated')->default(1); // [circulated = 1, uncirculated = 0]tpg_serial_num
            $table->string('color')->nullable();
            $table->string('tpg_service')->nullable(); // tpg
            $table->string('cac_sticker')->nullable(); // 1 = green, 2 = gold
            $table->string('tpg_serial_num')->nullable(); // tpg_serial
            $table->string('slab_condition')->nullable();
            $table->string('ngcx_grade')->nullable();
            $table->integer('ngc_star')->nullable();
            $table->integer('ngc_plus')->nullable();
            $table->integer('pcgs_plus')->nullable();
            $table->integer('coa')->nullable();
            $table->integer('coa_cert')->nullable();

            $table->integer('gsa_holder')->default(0);
            $table->string('gsa_holder_pro')->nullable();
            $table->string('strike_character')->nullable();
            $table->string('designation')->nullable();
            $table->string('special_label')->nullable();
            $table->string('fullAtt')->nullable();
            $table->string('proof_like')->nullable();
            $table->string('toned')->nullable();
            $table->string('slab_generation')->nullable();


            $table->integer('chop_mark')->default(0);
            $table->string('chop_mark_place')->nullable();
            $table->integer('problem')->default(0);
            $table->integer('damaged')->default(0);
            $table->integer('holed')->default(0);
            $table->integer('cleaned')->default(0);
            $table->integer('polished')->default(0);
            $table->integer('altered')->default(0);
            $table->integer('scratched')->default(0);
            $table->integer('pvc_damage')->default(0);
            $table->integer('corrosion')->default(0);
            $table->integer('bent')->default(0);
            $table->integer('plugged')->default(0);
            $table->integer('spotted')->default(0);
            $table->integer('slide_marks')->default(0);
            $table->integer('whizzing')->default(0);
            $table->integer('fingermarks')->default(0);
            $table->integer('counterstamp')->default(0);
            $table->integer('bag_mark')->default(0);

            $table->tinyInteger('for_sale')->default(0);
            $table->float('cost')->default(0.00);
            $table->float('value')->default(0.00);  //move to purchases

            $table->tinyInteger('private')->default(0);
            $table->tinyInteger('locked')->default(0);
            $table->tinyInteger('favorite')->default(0);
            $table->integer('views')->default(0);

            $table->integer('bulk_id')->default(0)->index();
            $table->integer('lot_id')->default(0)->index();
            $table->integer('set_id')->default(0)->index();
            $table->integer('roll_id')->default(0)->index();
            $table->integer('folder_id')->default(0)->index();
            $table->integer('firstday_id')->default(0)->index();
            $table->string('die_stage')->default(0);
            $table->integer('error_coin')->default(0);
            $table->integer('variety_coin')->default(0);
            $table->text('data')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('collected_log', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('collected_id')->nullable();
            $table->string('model',100)->index();
            $table->string('action',7);
            $table->text('message');
            $table->json('models');
            $table->timestamps();

            $table->foreign('collected_id')
                ->references('id')
                ->on('collected')
                ->onDelete('cascade');
        });

        Schema::create('collected_variety', function (Blueprint $table) {
            $table->unsignedBigInteger('collected_id')->index();
            $table->unsignedBigInteger('variety_id')->index();
            $table->string('degrees')->nullable(); // 170 degrees
            $table->string('direction')->nullable(); // cw,ccw
            $table->text('description')->nullable(); // Double 4 and 3, No AW
            $table->timestamps();

            $table->foreign('collected_id')
                ->references('id')
                ->on('collected')
                ->onDelete('cascade');
        });

        Schema::create('collected_error', function (Blueprint $table) {
            $table->unsignedBigInteger('collected_id')->index();
            $table->unsignedBigInteger('err_id')->index();
            $table->string('degrees')->nullable(); // 170 degrees
            $table->string('direction')->nullable(); // cw,ccw
            $table->text('description')->nullable(); // Double 4 and 3, No AW
            $table->timestamps();

            $table->foreign('collected_id')
                ->references('id')
                ->on('collected')
                ->onDelete('cascade');
        });

        // collected lot_id
        Schema::create('coins_errors', function (Blueprint $table) {
            $table->comment('Known errors');
            $table->id();
            $table->unsignedBigInteger('coin_id')->index();
            $table->string('sub_type')->default('None');
            $table->integer('err_id');
            $table->string('degrees')->nullable(); // 170 degrees
            $table->string('direction')->nullable(); // cw,ccw
            $table->text('description')->nullable(); // Double 4 and 3, No AW
            $table->float('cost')->default(0.00);
            $table->integer('private')->default(0);
            $table->integer('locked')->default(0);
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('collected_blank', function (Blueprint $table) {
            $table->comment('No date coins');
            $table->id();
            $table->integer('cointype_id')->index();
            $table->integer('date')->nullable();
            $table->unsignedBigInteger('user_id')->index();
            $table->float('cost')->default(0.00);
            $table->string('reason')->index(); // planchet,worn,error
            $table->integer('error_id')->index();
            $table->timestamps();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

        });


        Schema::create('collected_bulk', function (Blueprint $table) {
            $table->comment('Bulk coins');
            $table->unsignedBigInteger('coin_id')->index();
            $table->unsignedBigInteger('user_id')->index();
            $table->integer('amount')->index();
            $table->string('type')->index();
            $table->timestamps();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        // collected lot_id
        Schema::create('coin_lots', function (Blueprint $table) {
            $table->comment('lots or sub collect');
            $table->id();
            $table->string('nickname')->nullable();
            $table->string('lot_type')->default('group'); // sell, group,
            $table->text('description')->nullable();
            $table->string('source')->nullable();
            $table->unsignedBigInteger('user_id')->index();
            $table->float('cost')->default(0.00);
            $table->integer('private')->default(0);
            $table->integer('locked')->default(0);
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });


        Schema::create('sale_list', function (Blueprint $table) {
            $table->comment('Coins for sale');
            $table->id();
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('collected_id')->index();
            $table->float('asking')->default(0.00);
            $table->text('description')->nullable();
            $table->unsignedBigInteger('error_id')->index();
            $table->unsignedBigInteger('variety_id')->index();
            $table->timestamps();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('collected_id')
                ->references('id')
                ->on('collected')
                ->onDelete('cascade');

        });


        Schema::create('offers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sale_id')->nullable();
            $table->float('offer')->default(0.00);
            $table->string('action');
            $table->unsignedBigInteger('seller_id')->index();
            $table->timestamps();

            $table->foreign('sale_id')
                ->references('id')
                ->on('sale_list')
                ->onDelete('cascade');
        });

        Collected::factory()
            ->create( [
                'nickname' => '2021 ASE',
                'user_id' => 1,
                'coin_id' => 3885,
                'grade' => '70',
                'circulated' => 0,
            ]);
        Collected::factory()
            ->create( [
                'nickname' => '1909 P Proof Lincoln #1',
                'user_id' => 1,
                'coin_id' => 4005,
                'grade' => '68',
                'circulated' => 0,
            ]);
        Collected::factory()
            ->create( [
                'nickname' => '2022 Lincoln #1',
                'user_id' => 1,
                'coin_id' => 7936,
                'grade' => '69',
                'circulated' => 1,
            ]);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collected');
        Schema::dropIfExists('collected_variety');
        Schema::dropIfExists('collected_error');
        Schema::dropIfExists('collected_blank');
        Schema::dropIfExists('collected_bulk');
        Schema::dropIfExists('coin_lots');
        Schema::dropIfExists('collected_log');
    }
};
