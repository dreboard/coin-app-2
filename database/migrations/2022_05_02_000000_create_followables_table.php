<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateFollowablesTable extends Migration
{
    public function up()
    {
        Schema::create(config('follow.followables_table', 'followables'), function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger(config('follow.user_foreign_key', 'user_id'))->index()->comment('user_id');
            if (config('follow.uuids')) {
                $table->uuidMorphs('followable');
            } else {
                $table->morphs('followable');
            }

            $table->timestamp('accepted_at')->nullable();
            $table->timestamps();

            $table->index(['followable_type', 'accepted_at']);
        });

        // create followers
        $users = [11,111,1212];
        foreach($users as $user){
            DB::table('followables')->insert([
                ['followable_type' => 'App\Models\User', 'followable_id' => 1, 'user_id' => $user]
            ]);
        }
        foreach($users as $user){
            DB::table('followables')->insert([
                ['followable_type' => 'App\Models\User', 'followable_id' => $user, 'user_id' => 1]
            ]);
        }
    }

    public function down()
    {
        Schema::dropIfExists(config('follow.followables_table', 'followables'));
    }
}
