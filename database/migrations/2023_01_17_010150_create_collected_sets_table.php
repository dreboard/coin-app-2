<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collected_sets', function (Blueprint $table) {
            $table->id();
            $table->string('nickname')->nullable();
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('set_id')->index();
            $table->string('set_version')->default('Issued'); // Loose, Slabbed, Issued
            $table->string('tpg_service')->nullable();
            $table->string('tpg_serial_num')->nullable();
            $table->string('grade')->nullable();
            $table->string('condition')->default('Excellent');
            $table->float('cost')->default(0.00);
            $table->integer('views')->default(0);
            $table->integer('private')->default(0);
            $table->integer('locked')->default(0);
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('collected_rolls', function (Blueprint $table) {
            $table->id();
            $table->string('nickname')->nullable();
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('roll_id')->nullable();
            $table->string('roll_type')->nullable();
            $table->string('tpg_service')->nullable();
            $table->string('tpg_serial_num')->nullable();
            $table->string('slab_condition')->nullable();
            $table->string('condition')->default('Excellent');
            $table->float('cost')->default(0.00);
            $table->integer('views')->default(0);
            $table->integer('private')->default(0);
            $table->integer('locked')->default(0);
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('collected_folders', function (Blueprint $table) {
            $table->id();
            $table->string('nickname')->nullable();
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('folder_id')->nullable();
            $table->string('condition')->default('Excellent');
            $table->float('cost')->default(0.00);
            $table->integer('views')->default(0);
            $table->integer('private')->default(0);
            $table->integer('locked')->default(0);
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('collected_firstday', function (Blueprint $table) {
            $table->id();
            $table->string('nickname')->nullable();
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('firstday_id')->index();
            $table->string('condition')->default('Excellent');
            $table->float('cost')->default(0.00);
            $table->integer('views')->default(0);
            $table->integer('private')->default(0);
            $table->integer('locked')->default(0);
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('collected_bags', function (Blueprint $table) {
            $table->id();
            $table->string('nickname')->nullable();
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('bag_id')->nullable();
            $table->string('condition')->default('Excellent');
            $table->float('cost')->default(0.00);
            $table->integer('views')->default(0);
            $table->integer('private')->default(0);
            $table->integer('locked')->default(0);
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('purchases', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index();
            $table->float('cost')->default(0.00);
            $table->string('purchase_source')->nullable(); // ebay, coin shop, coin store, circulation
            $table->integer('purchasable_id')->nullable();
            $table->string('purchasable_type')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collected_sets');
        Schema::dropIfExists('collected_rolls');
        Schema::dropIfExists('collected_folders');
        Schema::dropIfExists('collected_firstday');
        Schema::dropIfExists('collected_bags');
        Schema::dropIfExists('purchases');
    }
};
