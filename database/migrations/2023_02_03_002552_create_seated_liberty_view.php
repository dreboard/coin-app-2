<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //DB::statement($this->createView());
    }

    public function down()

    {
        //DB::statement($this->dropView());
    }

    // SELECT *  FROM `coins` WHERE `design` LIKE 'Seated Liberty'
    private function createView(): string|false

    {
        return false;
        return <<<SQL
            CREATE VIEW view_seated_liberty AS
                SELECT
                    coins.id,
                    coins.coinName,
                    coins.mint,
                    coins.coinYear,
                    coins.mintMark,
                    coins.cointypes_id,
                    coins.strike,
                    coins.seriesType,
                    coins.designType,
                    (SELECT count(*) FROM posts
                                WHERE posts.user_id = users.id
                            ) AS total_posts
                FROM coins WHERE design = 'Seated Liberty'
            SQL;
    }


    private function dropView(): string
    {
        return <<<SQL
            DROP VIEW IF EXISTS `view_seated_liberty`;
            SQL;
    }
};
