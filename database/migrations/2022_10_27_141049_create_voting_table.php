<?php

use App\Helpers\ManageGroupHelper;
use App\Models\Groups\Group;
use App\Models\Groups\GroupElection;
use App\Models\Groups\GroupNominations;
use App\Models\Groups\Vote;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_elections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('election_type');
            $table->unsignedBigInteger('group_id')->index();
            $table->string('roles')->index();
            $table->timestamps();
            $table->timestamp('start_at')->nullable();
            $table->timestamp('expired_at')->nullable();

            $table->foreign('group_id')
                ->references('id')
                ->on('groups')
                ->onDelete('cascade');
        });

        Schema::create('group_nominees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('election_id')->index();
            $table->unsignedBigInteger('role_id')->index();
            $table->unsignedBigInteger('nominee')->index();
            $table->unsignedBigInteger('nominator')->index();
            $table->unique(['nominee', 'nominator', 'role_id', 'election_id']);
            $table->timestamps();
            $table->timestamp('start_at')->nullable();
            $table->timestamp('expired_at')->nullable();

            $table->foreign('election_id')
                ->references('id')
                ->on('group_elections')
                ->onDelete('cascade');

            $table->foreign('nominee')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('nominator')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('votes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('election_id');
            $table->unsignedBigInteger('role_id')->index();
            $table->unsignedBigInteger('candidate')->index();
            $table->unsignedBigInteger('voter')->index();
            $table->unique(['voter', 'candidate', 'role_id']);
            $table->timestamps();

            $table->foreign('election_id')
                ->references('id')
                ->on('group_elections')
                ->onDelete('cascade');

            $table->foreign('voter')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('candidate')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        $start = Carbon::now();
        $end = Carbon::now()->addDays(5);

        $start2 = Carbon::now()->subDays(365);
        $end2 = Carbon::now()->subDays(355);
        $president = ManageGroupHelper::GROUP_BOARD_POSITIONS['President'];
        $vice_president = ManageGroupHelper::GROUP_BOARD_POSITIONS['Vice President'];
        $secretary = ManageGroupHelper::GROUP_BOARD_POSITIONS['Secretary'];
        $treasurer = ManageGroupHelper::GROUP_BOARD_POSITIONS['Treasurer'];
        $voters = DB::table('group_user')
            ->select('group_user.user_id AS voter')
            ->where('group_user.group_id', '=', 1)
            ->where('member_type', 'Full')
            ->get();

        $election = GroupElection::factory()
            ->create([
                'name' => date('Y') . ' Board Election',
                'election_type' => 'Board',
                'group_id' => 1,
                'roles' => '1,2,3,4',
                'start_at' => $start,
                'expired_at' => $end,
            ]);

        $election_2 = GroupElection::factory()
            ->create([
                'name' => Carbon::now()->subYear()->format('Y') . ' Board Election',
                'election_type' => 'Board',
                'group_id' => 1,
                'roles' => '1,2,3,4',
                'start_at' => $start2,
                'expired_at' => $end2,
            ]);

        $elections = [$election, $election_2];
        $nominee_1 = DB::table('group_nominees')->insert([
            [
                'election_id' => $election->id,
                'role_id' => $president,
                'nominee' => 1,
                'nominator' => 2,
                'start_at' => $start,
                'expired_at' => $end,
            ],
            [
                'election_id' => $election_2->id,
                'role_id' => $president,
                'nominee' => 1,
                'nominator' => 2,
                'start_at' => $start2,
                'expired_at' => $end2,
            ]
        ]);

        $nominee_2 = DB::table('group_nominees')->insert([
            [
                'election_id' => $election->id,
                'role_id' => $president,
                'nominee' => 2,
                'nominator' => 1,
                'start_at' => $start,
                'expired_at' => $end,
            ],
            [
                'election_id' => $election_2->id,
                'role_id' => $president,
                'nominee' => 2,
                'nominator' => 1,
                'start_at' => $start2,
                'expired_at' => $end2,
            ]
        ]);

        $nominee_3 = DB::table('group_nominees')->insert([
            [
                'election_id' => $election->id,
                'role_id' => $vice_president,
                'nominee' => 3,
                'nominator' => 1,
                'start_at' => $start,
                'expired_at' => $end,
            ],
            [
                'election_id' => $election_2->id,
                'role_id' => $vice_president,
                'nominee' => 3,
                'nominator' => 1,
                'start_at' => $start2,
                'expired_at' => $end2,
            ]
        ]);

        $nominee_4 = DB::table('group_nominees')->insert([
            [
                'election_id' => $election->id,
                'role_id' => $vice_president,
                'nominee' => 4,
                'nominator' => 2,
                'start_at' => $start,
                'expired_at' => $end,
            ],
            [
                'election_id' => $election_2->id,
                'role_id' => $vice_president,
                'nominee' => 4,
                'nominator' => 2,
                'start_at' => $start2,
                'expired_at' => $end2,
            ],

        ]);

        $nominee_5 = DB::table('group_nominees')->insert([
            [
                'election_id' => $election->id,
                'role_id' => $secretary,
                'nominee' => 5,
                'nominator' => 1,
                'start_at' => $start,
                'expired_at' => $end,
            ],
            [
                'election_id' => $election_2->id,
                'role_id' => $secretary,
                'nominee' => 5,
                'nominator' => 1,
                'start_at' => $start2,
                'expired_at' => $end2,
            ],

        ]);

        $nominee_6 = DB::table('group_nominees')->insert([
            [
                'election_id' => $election->id,
                'role_id' => $secretary,
                'nominee' => 6,
                'nominator' => 2,
                'start_at' => $start,
                'expired_at' => $end,
            ],
            [
                'election_id' => $election_2->id,
                'role_id' => $secretary,
                'nominee' => 6,
                'nominator' => 2,
                'start_at' => $start2,
                'expired_at' => $end2,
            ],
        ]);

        $nominee_7 = DB::table('group_nominees')->insert([
            [
                'election_id' => $election->id,
                'role_id' => $treasurer,
                'nominee' => 7,
                'nominator' => 1,
                'start_at' => $start,
                'expired_at' => $end,
            ],
            [
                'election_id' => $election_2->id,
                'role_id' => $treasurer,
                'nominee' => 7,
                'nominator' => 1,
                'start_at' => $start2,
                'expired_at' => $end2,
            ],
        ]);

        $nominee_8 = DB::table('group_nominees')->insert([
            [
                'election_id' => $election->id,
                'role_id' => $treasurer,
                'nominee' => 8,
                'nominator' => 2,
                'start_at' => $start,
                'expired_at' => $end,
            ],
            [
                'election_id' => $election_2->id,
                'role_id' => $treasurer,
                'nominee' => 8,
                'nominator' => 2,
                'start_at' => $start2,
                'expired_at' => $end2,
            ]
        ]);


        foreach ($elections as $e => $election) {
            foreach ($voters as $k) {
                Vote::factory()->create([
                    'election_id' => $election->id,
                    'role_id' => 1,
                    'candidate' => function () use ($k, $president, $election) {
                        $exist = DB::table('votes')
                            ->where('role_id', $president)
                            //->where('election_id',$election)
                            ->where('voter', $k->voter)
                            ->where('candidate', 2)
                            ->exists();

                        if ($exist) {
                            return 1;
                        }
                        return 2;
                    },
                    'voter' => $k->voter,
                ]);
                Vote::factory()->create([
                    'election_id' => $election->id,
                    'role_id' => $vice_president,
                    'candidate' => function () use ($k, $vice_president, $election) {
                        $exist = DB::table('votes')
                            ->where('role_id', $vice_president)
                            //->where('election_id',$election)
                            ->where('voter', $k->voter)
                            ->where('candidate', 4)
                            ->exists();

                        if ($exist) {
                            return 3;
                        }
                        return 4;
                    },
                    'voter' => $k->voter,
                ]);
                Vote::factory()->create([
                    'election_id' => $election->id,
                    'role_id' => $secretary,
                    'candidate' => function () use ($k, $secretary, $election) {
                        $exist = DB::table('votes')
                            ->where('role_id', $secretary)
                            //->where('election_id',$election)
                            ->where('voter', $k->voter)
                            ->where('candidate', 6)
                            ->exists();

                        if ($exist) {
                            return 5;
                        }
                        return 6;
                    },
                    'voter' => $k->voter,
                ]);
                Vote::factory()->create([
                    'election_id' => $election->id,
                    'role_id' => $treasurer,
                    'candidate' => function () use ($k, $treasurer, $election) {
                        $exist = DB::table('votes')
                            ->where('role_id', $treasurer)
                            //->where('election_id',$election->id)
                            ->where('voter', $k->voter)
                            ->where('candidate', 7)
                            ->exists();

                        if ($exist) {
                            return 8;
                        }
                        return 7;
                    },
                    'voter' => $k->voter,
                ]);
            }
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_elections');
        Schema::dropIfExists('group_nominees');
        Schema::dropIfExists('votes');
    }
};
