<?php

use App\Models\Groups\Committee;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('committees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('group_id')->index();
            $table->string('title')->default('None'); //
            $table->unsignedBigInteger('chair'); // user_id of chairman
            $table->integer('seats')->default(3);
            $table->text('type'); // ['Educational','Nominations','Membership','Show','General']
            $table->text('details');
            $table->timestamp('approved_at')->useCurrent();
            $table->timestamp('expired_at')->nullable();
            $table->index(['group_id', 'chair']);
            $table->timestamps();

            $table->foreign('chair')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('group_id')
                ->references('id')
                ->on('groups')
                ->onDelete('cascade');
        });

        Schema::create('committee_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('committee_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamp('approved_at')->useCurrent();
            $table->timestamp('expired_at')->nullable();
            $table->index(['committee_id', 'user_id']);
            $table->timestamps();

            $table->foreign('committee_id')
                ->references('id')
                ->on('committees')
                ->onDelete('cascade');
        });

        $committee_1 = Committee::factory()
            ->create([
                'group_id' => 1,
                'title' => 'Members Committee',
                'type' => 'Membership',
                'chair' => 1,
                'seats' => 5,
                'details' => 'Approve and manage the groups users and assign duties and levels',
                'approved_at' => Carbon::now(),
                'expired_at' => Carbon::now()->addYear(1),
            ]);

        $chair = DB::table('group_user')->select('group_user.user_id AS chairman')
            ->where('group_user.group_id', '=', 1)
            ->where('group_user.user_id', '!=', 1)
            ->where( 'member_type' ,'Full')->inRandomOrder()->limit(1)->value('group_user.user_id');
        Committee::factory()
            ->create([
                'group_id' => 1,
                'title' => 'Educational',
                'type' => 'Educational',
                'chair' => $chair,
                'seats' => 5,
                'details' => 'Manage the different types of users',
                'approved_at' => Carbon::now(),
                'expired_at' => Carbon::now()->addYear(1),
            ]);

        $committee_members = DB::table('group_user')
            ->select('group_user.user_id AS member')
            ->where('group_user.group_id', '=', 1)
            ->where( 'member_type' ,'Full')
            ->limit(5)->get();

        $committee_1->members()
            ->attach(2);
        foreach ($committee_members as $committee_member){

            DB::table('group_role')->insert([
                'group_id' => 1,
                'user_id' => $committee_member->member,
                'role_id' => 14,
                'expired_at' => Carbon::now()->addYear(1)
                ]);

            $committee_1->members()
                ->attach($committee_member->member);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('committees');
        Schema::dropIfExists('committee_user');
    }
};
