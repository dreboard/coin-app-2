<?php

use App\Models\User;
use App\Models\Users\Login;
use Database\Factories\LoginFactory;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('users');
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->integer('is_new')->default(1);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('is_admin')->default(0);
            $table->timestamp('last_seen')->nullable();
            $table->boolean('profile_visibility')->default('1'); // for directory and groups
            $table->string('avatar')->nullable();
            $table->timestamp('banned_at')->nullable();
            $table->string('status')->default('good');
            $table->integer('incidents')->default(0);
            $table->string('ana')->nullable();
            $table->string('png')->nullable(); // Professional Numismatists Guild
            $table->string('cac')->nullable(); // Certified Acceptance Corporation
            $table->string('pcgs')->nullable(); // PCGS AUTHORIZED DEALER or PCGS Collectors Club
            $table->string('ebay_id')->nullable(); // modernnumismaticsguy
            $table->string('ebay_store')->nullable(); // https://www.ebay.com/str/modernnumismaticsguy
            $table->string('youtube')->nullable(); // channel link
            $table->string('user_level')->nullable(); // Beginner, Advanced, Expert
            $table->string('user_type')->nullable(); // Collector, Dealer, Research, Investor, Writer
            $table->string('sub_type')->nullable(); // Dealer [Broker], Collector [Hobbyist, Specialist, Generalist ], Investor, Hobbyist, Specialist
            $table->string('specialty')->nullable(); // for directory
            $table->text('public_message')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('postalcode')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });

//        DB::unprepared( file_get_contents( database_path("coins.sql")));
//        DB::unprepared( file_get_contents( database_path("coins_variety.sql")));
//        DB::unprepared( file_get_contents( database_path("cointypes.sql")));
//        DB::unprepared( file_get_contents( database_path("coincategories.sql")));

        // Known admin for testing
        DB::table('users')->insert([
            'name' => 'TheAdministrator',
            'first_name' => 'The',
            'last_name' => 'Administrator',
            'email' => 'dre.board@gmail.com',
            'password' => Hash::make('password'),
            'last_seen' => now(),
            'email_verified_at' => now(),
            'is_admin' => 1,
            'user_level' => 'Expert',
            'user_type' => 'Collector',
            'ebay_id' => 'liberty.coin',
            'ebay_store' => 'libertycoingalleries',
            'youtube' => 'liberty.coin',
            'ana' => '2000000',
            'public_message' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
            'created_at' => '2021-11-01 02:17:51.000000',
            'phone' => '555-555-5555',
            'address' => 'P.O. Box 1000',
            'postalcode' => '44035',
            'city' => 'Elyria',
            'state' => 'OH',
        ]);
        User::factory()
            ->create([
                'name' => 'andreboard',
                'first_name' => 'Andre',
                'last_name' => 'Board',
                'email' => 'dre.board70@gmail.com',
                'password' => Hash::make('password'),
                'last_seen' => now(),
                'email_verified_at' => now(),
                'is_admin' => 1,
                'user_level' => 'Expert',
                'user_type' => 'Collector',
                'created_at' => '2021-11-01 02:17:51.000000',
                'phone' => '555-555-5555',
                'address' => 'P.O. Box 1000',
                'postalcode' => '44035',
                'city' => 'Elyria',
                'state' => 'OH',
            ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
