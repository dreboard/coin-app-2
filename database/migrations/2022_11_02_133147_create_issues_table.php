<?php

use App\Helpers\ManageGroupHelper;
use App\Models\Groups\Issue;
use App\Models\Groups\IssueVote;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('issues', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('issue');
            $table->text('description');
            $table->integer('transparency')->default(0); // [Just Votes, Votes and Names]
            $table->integer('vote_level')->default(0); // [Majority = 0, Unanimous = 1]
            $table->unsignedBigInteger('group_id')->index();
            $table->timestamp('start_at')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->timestamps();
            $table->foreign('group_id')
                ->references('id')
                ->on('groups')
                ->onDelete('cascade');
        });

        Schema::create('issue_votes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('issue_id')->index();
            $table->unsignedBigInteger('voter')->index();
            $table->integer('vote')->index();
            $table->timestamps();
            $table->index(['issue_id', 'vote']);
            $table->foreign('voter')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('issue_id')
                ->references('id')
                ->on('issues')
                ->onDelete('cascade');
        });

        $start = Carbon::now();
        $end = Carbon::now()->addDays(5);
        $group_id = 1;
        $voters = DB::table('group_user')
            ->select('group_user.user_id AS voter')
            ->where('group_user.group_id', '=', 1)
            ->where( 'member_type' ,'Full')
            ->get();

        $issue = Issue::factory()
            ->create([
                'issue' => 'Allow stuff to happen',
                'description' => 'Allow stuff to happen',
                'transparency' => array_rand(array_flip(ManageGroupHelper::GROUP_TRANSPARENCY_LEVELS), 1),
                'group_id' => $group_id,
                'vote_level' => array_rand(array_flip(ManageGroupHelper::GROUP_VOTE_LEVELS), 1),
                'start_at' => $start,
                'expired_at' => $end,
            ]);

        foreach($voters as $k){
            IssueVote::factory()->create([
                'issue_id' => $issue->id,
                'voter' => $k->voter,
                'vote' => rand(0,1),
            ]);
        }
        // will actually vote
        Issue::factory()
            ->create([
                'issue' => 'Allow more stuff to happen',
                'transparency' => array_rand(array_flip(ManageGroupHelper::GROUP_TRANSPARENCY_LEVELS), 1),
                'group_id' => $group_id,
                'vote_level' => array_rand(array_flip(ManageGroupHelper::GROUP_VOTE_LEVELS), 1),
                'start_at' => $start,
                'expired_at' => $end,
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issues');
    }
};
