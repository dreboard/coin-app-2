<?php

use App\Helpers\ManageGroupHelper;
use App\Models\Groups\Agenda;
use App\Models\Groups\Event;
use App\Models\Groups\Group;
use App\Models\Groups\Issue;
use App\Models\Groups\IssueVote;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('directory', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('user_id');
            $table->text('title');
            $table->text('specialty');
            $table->string('directory_type'); // ['Show', 'Club', 'Dealer', 'Organization', 'Auction Services', 'Appraisals', 'Other']
            $table->string('address')->nullable();
            $table->string('postalcode')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->string('affiliations')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->text('title');
            $table->string('event_type'); // ['Board Meeting', 'Meetup', 'General', 'Show','Committee', 'Club', 'Special]
            $table->string('location'); // ['Virtual', 'Physical']
            $table->string('address')->nullable();
            $table->string('postalcode')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->text('description')->nullable();
            $table->unsignedBigInteger('group_id');
            $table->unsignedBigInteger('user_id');
            $table->index(['user_id', 'group_id']);
            $table->timestamp('start_at')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->timestamps();

            $table->foreign('group_id')
                ->references('id')
                ->on('groups')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        // Board Meeting
        Schema::create('agendas', function (Blueprint $table) {
            $table->id();
            $table->text('topic')->nullable(); // [ 'minutes']
            $table->text('action')->nullable(); // ['Completed', 'Skipped', 'Pending']
            $table->text('description')->nullable();
            $table->timestamp('start_at')->nullable();
            $table->integer('length')->nullable();
            $table->unsignedBigInteger('issue_id')->default(0);
            $table->unsignedBigInteger('event_id');
            $table->unsignedBigInteger('group_id');
            $table->index(['event_id', 'group_id']);
            $table->timestamps();

            $table->foreign('event_id')
                ->references('id')
                ->on('events')
                ->onDelete('cascade');
            $table->foreign('group_id')
                ->references('id')
                ->on('groups')
                ->onDelete('cascade');
        });

        /*
        Reading and approval of minutes, Report of officers,
        boards and standing committees, Report of special committees,
        Special orders, Unfinished business and general orders,New business

        Opening Ceremonies
        MintRoll Call
        Good of the Order
        Announcements
        Programs

        */


        Schema::create('event_user', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('event_id')->index();
            $table->unsignedBigInteger('participant');
            $table->unique(['event_id', 'participant']);
            $table->timestamps();

            $table->foreign('event_id')
                ->references('id')
                ->on('events')
                ->onDelete('cascade');
            $table->foreign('participant')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });


        Schema::create('agenda_event', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('event_id')->index();
            $table->unsignedBigInteger('agenda_id');
            $table->unique(['event_id', 'agenda_id']);
            $table->timestamps();

            $table->foreign('event_id')
                ->references('id')
                ->on('events')
                ->onDelete('cascade');
            $table->foreign('agenda_id')
                ->references('id')
                ->on('agendas')
                ->onDelete('cascade');
        });




        /*
         * Create an event and attach issues and participants
         * */
        $event = Event::factory()
            ->create([
                'title' => date('F Y').' Board Meeting',
                'event_type' => 'Board Meeting',
                'location' => 'Virtual',
                //'description' => date('F Y').' Board Meeting',
                'group_id' => 1,
                'user_id' => 1,
                'start_at' => Carbon::now(),
                'expired_at' => Carbon::now()->addHour(1),
            ]);
        Event::factory()
            ->create([
                'title' => 'Committee Meetup',
                'event_type' => 'Committee',
                'location' => 'Virtual',
                'description' => 'First Committee Meeting',
                'group_id' => 1,
                'user_id' => 1,
                'start_at' => Carbon::now(),
                'expired_at' => Carbon::now()->addHour(1),
            ]);
        Event::factory()->count(5)
            ->create([
                'group_id' => 1,
                'user_id' => 1,
            ]);
        $issue_1 = Issue::factory()->create([
            'issue' => 'Issue 1',
            'description' => 'An issue to do something',
            'transparency' => 0,
            'group_id' => 1,
            'vote_level' => 1,
            'start_at' => Carbon::now(),
            'expired_at' => Carbon::now()->addMinutes(10),
        ]);
        $issue_2 = Issue::factory()->create([
            'issue' => 'Issue 2',
            'description' => 'An issue to not do something',
            'transparency' => 0,
            'group_id' => 1,
            'vote_level' => 1,
            'start_at' => Carbon::now(),
            'expired_at' => Carbon::now()->addMinutes(10),
        ]);

        $agenda_1 = Agenda::factory()
            ->create([
                'topic' => 'Vote on Issue 1',
                'action' => 'Pending',
                'description' => 'Vote on Issue 1',
                'issue_id' => $issue_1->id,
                'event_id' => $event->id,
                'group_id' => 1,
                'length' => 600,// CarbonInterval::seconds($agenda->length)->forHumans();
            ]);
        $agenda_2 = Agenda::factory()
            ->create([
                'topic' => 'Vote on Issue 2',
                'action' => 'Pending',
                'description' => 'Vote on Issue 2',
                'issue_id' => $issue_2->id,
                'event_id' => $event->id,
                'group_id' => 1,
                'length' => 600, // CarbonInterval::seconds($agenda->length)->forHumans();
            ]);

        $participants = DB::table('group_user')
            ->select('group_user.user_id AS voter')
            ->where('group_user.group_id', '=', 1)
            ->where( 'member_type' ,'Full')
            ->get();

        foreach ($participants as $participant){
            $event->participants()
                ->attach($participant);

            IssueVote::factory()->create([
                'issue_id' => $issue_1->id,
                'voter' => $participant->voter,
                'vote' => rand(0,1),
            ]);
            IssueVote::factory()->create([
                'issue_id' => $issue_2->id,
                'voter' => $participant->voter,
                'vote' => rand(0,1),
            ]);
        }



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
        Schema::dropIfExists('agendas');
        Schema::dropIfExists('event_user');
        Schema::dropIfExists('directory');
    }
};
