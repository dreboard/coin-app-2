<?php

use App\Models\Groups\Group;
use App\Models\Groups\Library;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('library', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('group_id')->index();
            $table->string('publication_type')->default('Book'); // [Article,Book,Video]
            $table->text('publication_title')->default('None');
            $table->string('isbn')->default('None'); // ['Complete Encyclopedia of U.S. and Colonial Coins']
            $table->text('details')->default('None');
            $table->float('cost')->default(0.00);
            $table->integer('status')->default(1);
            $table->timestamps();

            $table->foreign('group_id')
                ->references('id')
                ->on('groups')
                ->onDelete('cascade');
        });

        Schema::create('library_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('library_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamp('approved_at')->useCurrent();
            $table->timestamp('expired_at')->nullable();
            $table->unique(['library_id', 'user_id']);
            $table->timestamps();

            $table->foreign('library_id')
                ->references('id')
                ->on('library')
                ->onDelete('cascade');
        });

        Schema::create('library_request', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('group_id');
            $table->unsignedBigInteger('requester');
            $table->string('publication_type')->default('Book'); // [Article,Book,Video]
            $table->text('publication_title');
            $table->text('isbn');
            $table->float('cost')->default(0.00);
            $table->text('decision')->default('Deny'); // ['Deny','Purchase','Unavailable']
            $table->index(['group_id', 'requester']);
            $table->timestamps();
            $table->foreign('requester')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('group_id')
                ->references('id')
                ->on('groups')
                ->onDelete('cascade');
        });


        Schema::create('sources', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('title');
            $table->text('description')->nullable();
            $table->string('publication_type')->default('Book'); // [Article,Book,Video,Website]
            $table->text('author')->nullable();
            $table->text('isbn')->nullable();
            $table->text('category')->default('Collecting');
            $table->text('publisher')->nullable();
            $table->text('url')->nullable();
            $table->integer('sourceable_id')->nullable();
            $table->string('sourceable_type')->nullable();
            $table->timestamps();
        });
        //DB::unprepared( file_get_contents( database_path("sources_data.sql") ) );

        Library::factory()
            ->create([
                'group_id' => 1,
                'publication_type' => 'Book',
                'publication_title' => 'Complete Encyclopedia of U.S. and Colonial Coins',
                'isbn' => '0385142072',
                'details' => 'Drawing on a lifetime of research, Americas top coin historian presents the most comprehensive guide to U.S. coins ever published.',
                'cost' => 90.24,
                'status' => 1,
            ]);
        Library::factory()
            ->create([
                'group_id' => 1,
                'publication_type' => 'Book',
                'publication_title' => 'A Guide Book of US Coins 2023 (Guide Book of United States Coins)',
                'isbn' => '079484961X',
                'details' => 'A Guide Book of United States Coins--is 76 years young and going strong. Since 1946 collectors around the country have loved the books grade-by-grade coin values, historical background, detailed specifications, high-resolution photographs, and accurate mintage data.',
                'cost' => 16.16,
                'status' => 1,
            ]);

        // fill library and loans
        DB::unprepared("

            INSERT INTO `library_user` (`library_id`, `user_id`)
             VALUES
            (1,1);

            INSERT INTO `library_request` (`group_id`, `requester`, `publication_type`, `publication_title`, `isbn`, `cost`)
             VALUES
            (1,1,'Book','The Coin Collecting Bible', '979-8357151384','18.89');






        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('library');
        Schema::dropIfExists('library_user');
        Schema::dropIfExists('library_request');
        Schema::dropIfExists('sources');
    }
};
