<?php

use App\Models\Comment;
use App\Models\Groups\Group;
use App\Models\Post;
use App\Models\Tag;
use App\Models\User;
use Database\Factories\CommentFactory;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('group_id')->nullable();
            $table->string('title');
            $table->text('body');
            $table->string('image_url')->default('None'); // forum/default.jpg
            $table->string('slug');
            $table->string('post_type')->default('Article'); // Article, Guide
            $table->integer('publish')->default(1);
            $table->integer('public')->default(1);
            $table->integer('featured')->default(0);
            $table->integer('views')->default(0);
            $table->integer('notify')->default(0);
            $table->integer('cat_id')->default(29);
            $table->integer('type_id')->default(113);
            $table->integer('coin_id')->default(4003);
            $table->integer('postable_id')->nullable();
            $table->string('postable_type')->nullable();
            $table->string('reference_url')->default('None');
            $table->string('reference_book')->default('None');
            $table->index(['group_id', 'user_id']);
            $table->timestamps();

            $table->foreign('group_id')
                ->references('id')
                ->on('groups')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('tags', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('post_tag', function (Blueprint $table) {
            $table->id();

            $table->integer("taggable_id");
            $table->string("taggable_type");

            $table->unsignedBigInteger('tag_id');
            $table->foreign('tag_id')
                ->references('id')
                ->on('tags')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->text('body');
            $table->integer('commentable_id')->unsigned();
            $table->string('commentable_type');
            $table->timestamps();
        });


        // Post tags
        DB::unprepared("INSERT INTO `tags` (`name`, `slug`)
             VALUES
            ('Collecting','collect'),
            ('Investing','invest'),
            ('Storing','store'),
            ('Selling','sell'),
            ('Purchasing','purchase'),
            ('Cleaning','clean');
        ");

        $tags = Tag::all();

        $group_1_user = User::find(1);
        $group_1 = Group::find(1);
        $title = 'Collecting United\'s First Post';
        $title2 = 'Collecting United\'s Second Post';

        $post = Post::factory()
            //->count(3)
            ->for($group_1_user)
            ->has(Comment::factory()->count(3))
            ->create([
                'user_id' => $group_1_user->id,
                'group_id' => $group_1->id,
                'title' => $title,
                'slug' => Str::of($title)->slug('_'),
            ]);
        $post->tags()->attach([1, 2]);

        $members = DB::table('group_user')
            ->select('group_user.user_id AS commenter')
            ->where('group_user.group_id', '=', 1)
            ->where('member_type', 'Full')
            ->get();
        foreach ($members as $k) {
            Comment::factory()->count(rand(1,2))->for(
                $post, 'commentable'
            )->create([
                'user_id' => $k->commenter,
            ]);
        }

        $post_2 = Post::factory()
            //->count(3)
            ->for($group_1_user)
            ->has(Comment::factory()->count(3))
            ->create([
                'user_id' => $group_1_user->id,
                'group_id' => $group_1->id,
                'title' => $title2,
                'slug' => Str::of($title2)->slug('_'),
            ]);
        $post_2->tags()->attach([1, 2]);
        Post::factory()
            ->count(21)
            ->for($group_1_user)
            ->has(Comment::factory()->count(2))
            ->create([
                'user_id' => $group_1_user->id,
                'group_id' => $group_1->id,
            ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
        Schema::dropIfExists('tags');
        Schema::dropIfExists('post_tag');
        Schema::dropIfExists('comments');
    }
};
