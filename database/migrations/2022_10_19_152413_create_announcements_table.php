<?php

use Database\Factories\AnnouncementFactory;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcements', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('group_id')->index()->default('1');
            $table->index(['user_id', 'group_id']);
            $table->text('message');
            $table->timestamps();

            $table->foreign('group_id')
                ->references('id')
                ->on('groups')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('post_cards', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('group_id');
            $table->index(['user_id', 'group_id']);
            $table->text('body');
            $table->text('category');
            $table->timestamps();

            $table->foreign('group_id')
                ->references('id')
                ->on('groups')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });


        Schema::create('surveys', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('group_id');
            $table->index(['user_id', 'group_id']);
            $table->string('title');
            $table->string('description');
            $table->text('category');
            $table->timestamps();

            $table->foreign('group_id')
                ->references('id')
                ->on('groups')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('survey_questions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('survey_id')->index();
            $table->unsignedBigInteger('user_id'); // who made
            $table->string('question', 255); // 'Rate the service'
            $table->json('options'); // 'options' => ['One', 'Two', 'Three', 'Four', 'Five'],

            $table->timestamps();

            $table->foreign('survey_id')
                ->references('id')
                ->on('surveys')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
        /*
        $question = new SurveyQuestion([
            'content' => 'Rate the service',
            'options' => 'options' => ['One', 'Two', 'Three', 'Four', 'Five'],
        ]);
        */

        Schema::create('survey_answers', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->unsignedBigInteger('question_id');
            $table->unsignedBigInteger('user_id'); // who answered
            $table->integer('answer');
            $table->timestamps();

            $table->foreign('question_id')
                ->references('id')
                ->on('survey_questions')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        DB::unprepared("
            INSERT INTO `announcements` (`user_id`, `group_id`, `created_at`, `message`) VALUES
            (1, 1, '2022-10-18 01:15:48', 'We are excited to announce that, due to our remarkable growth over the last [enter number] of years, we are expanding! In fact, we are opening a new store. We invite you to celebrate with us during the big opening day'),
            (1, 1, '2022-09-18 01:15:48', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. '),
            (1, 1, '2022-08-18 01:15:48', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ');
        ");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcements');
        Schema::dropIfExists('post_cards');
        Schema::dropIfExists('surveys');
        Schema::dropIfExists('survey_questions');
        Schema::dropIfExists('survey_answers');
    }
};
