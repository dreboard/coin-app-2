<?php

use App\Models\User;
use Database\Factories\LoginFactory;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class CreateLoginHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logins', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('ip_address');
            $table->dateTime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->unsignedBigInteger('login_time');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        // Test User #2
        User::factory()
            ->has(LoginFactory::factoryForModel('Login')
                ->count(rand(2,11)), 'logins')
            ->create([
                'name' => 'defaultUser',
                'first_name' => 'Default',
                'last_name' => 'User',
                'email' => 'defaultUser@gmail.com',
                'password' => Hash::make('password'),
                'last_seen' => now(),
                'email_verified_at' => now(),
                'profile_visibility' => 1,
                'is_admin' => 0,
                'created_at' => now(),
                'user_level' => 'Advanced',
                'user_type' => 'Collector',
            ]);
        // Test User #2
        User::factory()
            ->has(LoginFactory::factoryForModel('Login')
            ->count(rand(2,11)), 'logins')
            ->create([
                'name' => 'defaultUser2',
                'first_name' => 'Default2',
                'last_name' => 'User2',
                'email' => 'defaultUser2@gmail.com',
                'password' => Hash::make('password'),
                'last_seen' => now(),
                'email_verified_at' => now(),
                'profile_visibility' => 1,
                'is_admin' => 0,
                'created_at' => now(),
                'user_level' => 'Expert',
                'user_type' => 'Dealer',
        ]);
        // Random Users login history
        User::factory()
            ->count(22)
            ->has(LoginFactory::factoryForModel('Login')->count(rand(2,11)), 'logins')
            ->create();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logins');
    }
}
