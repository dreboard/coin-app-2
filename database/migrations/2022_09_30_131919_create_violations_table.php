<?php

use App\Models\Users\Violation;
use Database\Factories\ViolationFactory;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('violations', function (Blueprint $table) {
            $table->id();
            $table->string('violation_type');
            $table->unsignedBigInteger('reporter');
            $table->string('violator');
            $table->unsignedBigInteger('violator_id')->nullable();
            $table->string('details');
            $table->string('screenshot')->nullable();
            $table->string('url');
            $table->string('action')->default('Case Pending'); // for admin and response
            $table->timestamp('read_at')->nullable();
            $table->timestamps();
            $table->foreign('violator_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('reporter')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Violation::factory()
            ->count(3)
            ->create();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('violations');
    }
};
