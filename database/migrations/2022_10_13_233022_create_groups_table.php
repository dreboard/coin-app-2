<?php
/**
 *
 * Tools -- EasyMDE - Markdown Editor https://github.com/Ionaru/easy-markdown-editor
 */

use App\Helpers\ManageGroupHelper;
use App\Models\Groups\Group;
use App\Models\Groups\GroupRequest;
use App\Models\Groups\Role;
use App\Models\Groups\Settings;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('group_type')->default('Group'); // [User Group, Club, Association ]
            // User Group = [Discussion, Project, Group], Club [Virtual, Physical], Association [Virtual, Physical]
            $table->string('group_level')->default('Group'); // [Virtual, Physical],[Discussion, Project, Group]
            $table->integer('parent_organization')->default(0); //
            $table->text('description')->nullable();
            $table->string('short_description')->nullable();
            $table->integer('join_id'); // generated floor(time()-999999999)  MAKE TABLE
            $table->string('ana')->nullable();
            $table->string('image_url')->nullable();
            $table->string('url')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('place')->nullable();
            $table->string('address')->nullable();
            $table->string('postalcode')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('meets')->nullable();
            $table->string('specialty')->nullable();
            $table->string('formed')->nullable();
            $table->float('regular')->default(0.00);
            $table->float('associate')->default(0.00);
            $table->float('life')->default(0.00);
            $table->unsignedBigInteger('user_id')->unique();
            $table->boolean('private')->unsigned()->default(false);
            $table->text('extra_info')->nullable();
            $table->integer('require_info')->default(0);
            $table->text('settings')->nullable();
            $table->timestamps();

            $table->index(['user_id', 'id']);
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('group_laws', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('group_id')->index();
            $table->string('law_type')->default('None'); // ['Constitution', 'By-Laws']
            $table->text('details')->default('None'); // [articles and sections]
            $table->timestamp('approved_at')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->timestamps();

            $table->foreign('group_id')
                ->references('id')
                ->on('groups')
                ->onDelete('cascade');
        });

        // who can do what by level
        Schema::create('group_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('group_id')->index();
            $table->integer('edit')->default(0); // 0 anyone,1 by position (writer, editor)
            $table->integer('posts')->default(0); // 0 anyone,1 by position (writer, editor)
            $table->integer('events')->default(0); // 0 anyone,1 by position
            $table->integer('committees')->default(0); // 0 anyone,1 by position
            $table->integer('officers')->default(0); // 0 appoint, 1 vote
            $table->integer('positions')->default(0); // 0 appoint, 1 vote
            $table->integer('announcements')->default(0); // 0 anyone,1 by position
            $table->integer('members')->default(0); // 0 anyone,1 by position
            $table->integer('elections')->default(0); // 0 anyone,1 by position
            $table->integer('media')->default(0);  // 0 anyone,1 by position
            $table->timestamps();

            $table->foreign('group_id')
                ->references('id')
                ->on('groups')
                ->onDelete('cascade');
        });

        // Carbon::now()->addDays(30)


        Schema::create('group_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('group_id')->index();
            $table->string('club_position')->default('None'); // [pres, vp, sec(roles table)]
            $table->string('member_type')->default('Full'); // ['Guest', 'Associate', 'Honorary', 'Full', 'Life']
            $table->string('member_level')->default('Member'); // ['Member', 'Staff', 'Executive']
            $table->tinyInteger('manage')->default(ManageGroupHelper::MANAGE_LEVELS['Member Level']); // [0 = none, 1 = members, 2 = edit and manage, 3 = anything]
            $table->timestamp('approved_at')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->index(['group_id', 'user_id']);
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('group_id')
                ->references('id')
                ->on('groups')
                ->onDelete('cascade');
        });

        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('guard_name');
            $table->integer('manage');
            $table->timestamps();
        });

        Schema::create('group_role', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unique(['user_id', 'group_id', 'role_id']);
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('group_id')->index();
            $table->unsignedBigInteger('role_id')->index();
            $table->timestamp('approved_at')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('group_id')
                ->references('id')
                ->on('groups')
                ->onDelete('cascade');
        });


        Schema::create('group_request', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('group_id')->index();
            $table->string('type')->default('request'); //['request', 'invite']
            $table->index(['group_id', 'user_id']);
            $table->timestamps();

            $table->foreign('group_id')
                ->references('id')
                ->on('groups')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });


        // public page views
        Schema::create('group_analytics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('group_id')->index();
            $table->integer('page_view')->index();
            $table->timestamps();

            $table->foreign('group_id')
                ->references('id')
                ->on('groups')
                ->onDelete('cascade');
        });



        try{
            /**
             * Group setup
             *
             */
            $group_1_user = User::find(1);
            $group_1 = Group::factory()
                ->create([
                    'name' => 'Collecting United',
                    'group_type' => 'Club',
                    'short_description' => 'The Official Coin Club of Collecting United',
                    'description' => 'The Official Coin Club of Collecting United',
                    'url' => 'collectingunited.com',
                    'parent_organization' => 2,
                    'group_type' => 'User Group',
                    'group_type' => 'Group',
                    'specialty' => 'General Numismatics',
                    'private' => 0,
                    'regular' => 20.00,
                    'associate' => 0.00,
                    'life' => 150.00,
                    'user_id' => $group_1_user->id,
                    'phone' => $group_1_user->phone,
                    'email' => $group_1_user->email,
                    'address' => $group_1_user->address,
                    'postalcode' => $group_1_user->postcode,
                    'first_name' => $group_1_user->first_name,
                    'last_name' => $group_1_user->last_name,
                    'city' => $group_1_user->city,
                    'state' => $group_1_user->state,
                ]);
            Settings::factory()
                ->create([
                    'group_id' => $group_1->id,
                    'edit' => 3,
                    'posts' => 2,
                    'events' => 2,
                    'committees' => 3,
                    'officers' => 3,
                    'positions' => 3,
                    'announcements' => 3,
                    'members' => 3,
                    'elections' => 3,
                    'media' => 2,
                ]);

            $association = Group::factory()
                ->create([
                    'name' => 'A Coin Association',
                    'group_type' => 'Association',
                    'specialty' => 'General Numismatics',
                    'private' => 1,
                    'user_id' => 3,
                ]);
            $group_1_user->groups()->attach($group_1->id, [
                'club_position' => 'President',
                'member_type' => 'Full',
                'member_level' => 'Executive',
                'manage' => ManageGroupHelper::MANAGE_LEVELS['Executive Level'],
                'created_at' => now(),
                'updated_at' => now(),
                'approved_at' => now(),
            ]);

            DB::table('group_user')->insert([
                'user_id' => $group_1_user->id,
                'group_id' => 2,
                'club_position' => 'None',
                'member_type' => 'Full',
                'member_level' => 'Member',
                'manage' => ManageGroupHelper::MANAGE_LEVELS['Executive Level'],
                'created_at' => now(),
                'updated_at' => now(),
                'approved_at' => now(),
            ]);

            $group_1_user->groups()->attach($association->id, [
                'club_position' => 'None',
                'member_type' => 'Full',
                'member_level' => 'Executive',
                'manage' => ManageGroupHelper::MANAGE_LEVELS['Executive Level'],
                'created_at' => now(),
                'updated_at' => now(),
                'approved_at' => now(),
            ]);

            User::factory()->hasAttached(
                Group::factory(),
                ['group_id' => $group_1->id, 'member_type' => 'Full']
            )->count(45)->create();

            User::factory()->hasAttached(
                Group::factory(),
                ['group_id' => $group_1->id, 'member_type' => 'Associate']
            )->count(7)->create();

            User::factory()->hasAttached(
                Group::factory(),
                ['group_id' => $group_1->id, 'member_type' => 'Guest']
            )->count(2)->create();

            $group_2_user = User::find(2);
            $group_2 = Group::factory()
                ->create([
                    'name' => 'A Coin Group',
                    'group_type' => 'Club',
                    'private' => 0,
                    'user_id' => $group_2_user->id,
                    'phone' => $group_2_user->phone,
                    'email' => $group_2_user->email,
                    'address' => $group_2_user->address,
                    'postalcode' => $group_2_user->postcode,
                    'first_name' => $group_2_user->first_name,
                    'last_name' => $group_2_user->last_name,
                    'city' => $group_2_user->city,
                    'state' => $group_2_user->state,
                ]);
            User::factory()->hasAttached(
                Group::factory(),
                ['group_id' => $group_2->id, 'member_type' => 'Full']
            )->count(15)->create();

            $group_1_user->groups()->attach($group_2->id, [
                'club_position' => 'Member',
                'member_type' => 'Full',
                'member_level' => 'Executive',
                'manage' => ManageGroupHelper::MANAGE_LEVELS['Member Level'],
                'created_at' => now(),
                'updated_at' => now(),
                'approved_at' => now(),
            ]);

            $group_2_user->groups()->attach($group_2->id, [
                'club_position' => 'President',
                'member_type' => 'Executive',
                'manage' => ManageGroupHelper::MANAGE_LEVELS['Executive Level'],
                'created_at' => now(),
                'updated_at' => now(),
                'approved_at' => now(),
            ]);
            $group_3_user = User::find(3);
            $group_3_user->groups()->attach($group_2->id, [
                'club_position' => 'Member',
                'member_type' => 'Full',
                'manage' => ManageGroupHelper::MANAGE_LEVELS['Member Level'],
                'created_at' => now(),
                'updated_at' => now(),
                'approved_at' => now(),
            ]);
            Group::factory()
                ->create([
                    'name' => 'A Parent Association',
                    'group_type' => 'Association',
                    'specialty' => 'Small Cents',
                    'private' => 1,
                    'user_id' => 4,
                ]);
            Group::factory()
                ->count(10)
                ->create();

            GroupRequest::factory()
                ->count(10)
                ->create([
                    'group_id' => $group_1->id,
                    'user_id'  => User::factory(),
                ]);

            Role::factory()->create([
                'user_id'  => $group_1_user->id,
                'group_id' => $group_1->id,
                'role_id'  => ManageGroupHelper::GROUP_BOARD_POSITIONS['President'],
                'approved_at' => Carbon::now(),
                'expired_at' => Carbon::now()->addDays(365),
            ]);

            Role::factory()->create([
                'user_id'  => $group_2_user->id,
                'group_id' => $group_2->id,
                'role_id'  => ManageGroupHelper::GROUP_BOARD_POSITIONS['President'],
                'approved_at' => Carbon::now(),
                'expired_at' => Carbon::now()->addDays(365),
            ]);

            Role::factory()->create([
                'user_id'  => $group_1_user->id,
                'group_id' => $group_1->id,
                'role_id'  => 10,
                'approved_at' => Carbon::now(),
                'expired_at' => Carbon::now()->addDays(365),
            ]);

            // Roles and abilities
            DB::unprepared("

            INSERT INTO `roles` (`name`, `guard_name`, `manage`)
             VALUES
            ('President','pres', ".ManageGroupHelper::MANAGE_LEVELS['Executive Level']."),
            ('Vice President','vice_pres', ".ManageGroupHelper::MANAGE_LEVELS['Executive Level']."),
            ('Treasurer','tres', ".ManageGroupHelper::MANAGE_LEVELS['Executive Level']."),
            ('Secretary','sec', ".ManageGroupHelper::MANAGE_LEVELS['Executive Level']."),
            ('Director','dir', ".ManageGroupHelper::MANAGE_LEVELS['Executive Level']."),
            ('Awards Director','awards', ".ManageGroupHelper::MANAGE_LEVELS['Executive Level']."),
            ('Program Director','pro_chair', ".ManageGroupHelper::MANAGE_LEVELS['Executive Level']."),
            ('Membership Director','mem_dir', ".ManageGroupHelper::MANAGE_LEVELS['Executive Level']."),
            ('Activities Director','act_dir', ".ManageGroupHelper::MANAGE_LEVELS['Executive Level']."),
            ('Committee Chair','commit_chair', ".ManageGroupHelper::MANAGE_LEVELS['Executive Level']."),
            ('Show Chairman','show_chair', ".ManageGroupHelper::MANAGE_LEVELS['Executive Level']."),
            ('Special Events Coordinator', 'sp_ev', ".ManageGroupHelper::MANAGE_LEVELS['Executive Level']."),
            ('Advisor','advise', ".ManageGroupHelper::MANAGE_LEVELS['Executive Level']."),
            ('Committee Member','commit_mem', ".ManageGroupHelper::MANAGE_LEVELS['Staff Level']."),
            ('Dealer Director','deal_dir', ".ManageGroupHelper::MANAGE_LEVELS['Staff Level']."),
            ('Sergeant-at-Arms','saa', ".ManageGroupHelper::MANAGE_LEVELS['Staff Level']."),
            ('Librarian','library', ".ManageGroupHelper::MANAGE_LEVELS['Staff Level']."),
            ('Historian','historian', ".ManageGroupHelper::MANAGE_LEVELS['Staff Level']."),
            ('ANA Club Rep','ana_rep', ".ManageGroupHelper::MANAGE_LEVELS['Staff Level']."),
            ('Bourse','bourse', ".ManageGroupHelper::MANAGE_LEVELS['Staff Level']."),
            ('Show Promoter','show_promo', ".ManageGroupHelper::MANAGE_LEVELS['Staff Level']."),
            ('Show Rep','show_rep', ".ManageGroupHelper::MANAGE_LEVELS['Staff Level']."),
            ('Show Hospitality','show_hos', ".ManageGroupHelper::MANAGE_LEVELS['Staff Level']."),
            ('Marketing','market', ".ManageGroupHelper::MANAGE_LEVELS['Staff Level']."),
            ('Sales Manager','sale_man', ".ManageGroupHelper::MANAGE_LEVELS['Staff Level']."),
            ('Editor','editor', ".ManageGroupHelper::MANAGE_LEVELS['Staff Level']."),
            ('Webmaster','webmaster', ".ManageGroupHelper::MANAGE_LEVELS['Staff Level']."),
            ('Social Media Manager','soc_media', ".ManageGroupHelper::MANAGE_LEVELS['Staff Level']."),
            ('Writer','write', ".ManageGroupHelper::MANAGE_LEVELS['Staff Level']."),
            ('Legal','legal', ".ManageGroupHelper::MANAGE_LEVELS['Staff Level']."),
            ('Site Manager','site_man', ".ManageGroupHelper::MANAGE_LEVELS['Staff Level']."),
            ('Writer','writer', ".ManageGroupHelper::MANAGE_LEVELS['Staff Level']."),
            ('Consignment','consign', ".ManageGroupHelper::MANAGE_LEVELS['Staff Level']."),
            ('2nd Vice President','2vice_pres', ".ManageGroupHelper::MANAGE_LEVELS['Executive Level']."),
            ('President Emeritus','pres_em', ".ManageGroupHelper::MANAGE_LEVELS['Staff Level'].");
        ");

        }catch(Exception $e){
            echo $e->getMessage();
        }



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
        Schema::dropIfExists('group_user');
        Schema::dropIfExists('group_request');
        Schema::dropIfExists('roles');
        Schema::dropIfExists('group_role');
        Schema::dropIfExists('group_analytics');
    }
};
