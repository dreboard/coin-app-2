<?php

use App\Models\Comment;
use App\Models\Forum;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forums', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index();
            $table->string('title');
            $table->string('slug');
            $table->text('body');
            $table->integer('private')->default(0); // open = listed in forum, closed = discussion
            $table->string('subject')->default('General');// [Seated Liberty, Double Die, Lincoln Cents] -- BE SPECIFIC
            $table->string('type')->default('Discussion'); // [Collaborative,Discussion, Question]
            $table->uuid('notification_id')->nullable();
            $table->integer('views')->default(0);
            $table->integer('notify')->default(0);
            $table->integer('forumable_id')->nullable();
            $table->string('forumable_type')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('participants', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('forum_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamp('last_read')->nullable();
            $table->timestamps();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('forum_id')
                ->references('id')
                ->on('forums')
                ->onDelete('cascade');
        });

        $tags = Tag::all();

        $user_1 = User::find(1);
        $title = 'List of master die doubles';
        $body = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!';

        $post = Forum::factory()
            ->for($user_1)
            ->has(Comment::factory()->count(3))
            ->create([
                'user_id' => $user_1->id,
                'title' => $title,
                'body' => $body,
                'views' => 33,
                'slug' => Str::of($title)->slug('_'),
            ]);
        $post->tags()->attach([1, 2]);

        $post2 = Forum::factory()
            ->for($user_1)
            ->has(Comment::factory()->count(51))
            ->create([
                'user_id' => $user_1->id,
                'title' => 'Open Chat',
                'body' => $body,
                'views' => 33,
                'slug' => Str::of($title)->slug('_'),
            ]);
        $post->tags()->attach([1, 2]);

        Forum::factory()
            ->count(21)
            //->for($user_1)
            ->has(Comment::factory()->count(rand(0,22)))
            ->create([
                //'user_id' => $user_1->id,
            ]);

        $discussion = Forum::factory()
            ->has(Comment::factory()->count(rand(0,22)))
            ->create([
                'user_id' => $user_1->id,
                'title' => 'Variety #22',
                'body' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla.',
                'private' => 1,
                'type' => 'discussion',
                'notification_id' => $user_1->id,
                'views' => 22,
                'notify' => 1,
            ]);
        $data = [
            'from_id' => 1,
            'from' => 'TheAdministrator',
            'subject' => $discussion->title,
            'body' => $discussion->body,
            'forum_id' => $discussion->id
        ];
        $encoded_data = json_encode($data);
        $users = [11,111,121];
        foreach($users as $user){
            //$user = User::find($recipient);
            DB::table('participants')->insert([
                ['forum_id' => $discussion->id, 'user_id' => $user]
            ]);
            DB::table('notifications')->insert([
                "id" => Str::uuid()->toString(),
                "type" => "App\Notifications\ContactUserNotification",
                "notifiable_type" => "App\Models\User",
                "notifiable_id" => $user, // id of the notifiable model
                "data" => $encoded_data,
                "created_at" => now(),
                "read_at" => now(),
            ]);
        }
        DB::table('notifications')->insert([
            "id" => Str::uuid()->toString(),
            "type" => "App\Notifications\ContactUserNotification",
            "notifiable_type" => "App\Models\User",
            "notifiable_id" => 1,
            "data" => $encoded_data,
            "created_at" => now(),
            //"read_at" => now(),
        ]);
        // php artisan migrate:refresh --path=/database/migrations/2022_12_15_150825_create_forums_table.php
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forums');
        Schema::dropIfExists('participants');
    }
};
