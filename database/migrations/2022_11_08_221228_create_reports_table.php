<?php

use App\Models\Groups\Committee;
use App\Models\Groups\Report;
use Database\Factories\Groups\ReportFactory;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('type'); // ['Activity','Financial','Research','Progress','Annual','Inventory','Performance','Committee']
            $table->text('overview')->nullable();
            $table->text('background')->nullable();
            $table->text('discussion')->nullable();
            $table->text('budget')->nullable();
            $table->text('conclusion')->nullable();
            $table->text('recommendations')->nullable();
            $table->unsignedBigInteger('group_id')->index();
            $table->unsignedBigInteger('user_id')->index();
            $table->integer('published')->default(0); // NOT published
            $table->morphs('reportable'); // committee, group,
            $table->timestamps();
            $table->foreign('group_id')
                ->references('id')
                ->on('groups')
                ->onDelete('cascade');
        });


        Schema::create('notes', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('note');
            $table->unsignedBigInteger('user_id')->index();
            $table->timestamps();
            $table->morphs('notable');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        $report = Report::factory()
            ->create([
                'type' => 'Activity',
                'title' => 'For '.date('Y').' Board Meeting',
                'group_id' => 1,
                'published' => 1,
                'user_id' => 1,
                'reportable_id' => 1,
                'reportable_type' => Committee::class,
            ]);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
        Schema::dropIfExists('notes');
    }
};
