<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Users\UserLogin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //User::factory(50)->create();

        // Known admin for testing
        DB::table('users')->insert([
            'name' => 'TheAdministrator',
            'first_name' => 'The',
            'last_name' => 'Administrator',
            'email' => 'dre.board@gmail.com',
            'password' => Hash::make('password'),
            'last_seen' => now(),
            'email_verified_at' => now(),
            'is_admin' => 1,
            'user_level' => 'Expert',
            'user_type' => 'Collector',
            'ebay_id' => 'liberty.coin',
            'ebay_store' => 'libertycoingalleries',
            'youtube' => 'liberty.coin',
            'ana' => '2000000',
            'public_message' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
            'created_at' => '2021-11-01 02:17:51.000000',
            'phone' => '555-555-5555',
            'address' => 'P.O. Box 1000',
            'postalcode' => '44035',
            'city' => 'Elyria',
            'state' => 'OH',
        ]);
        User::factory()
            ->create([
                'name' => 'andreboard',
                'first_name' => 'Andre',
                'last_name' => 'Board',
                'email' => 'dre.board70@gmail.com',
                'password' => Hash::make('password'),
                'last_seen' => now(),
                'email_verified_at' => now(),
                'is_admin' => 1,
                'user_level' => 'Expert',
                'user_type' => 'Collector',
                'created_at' => '2021-11-01 02:17:51.000000',
                'phone' => '555-555-5555',
                'address' => 'P.O. Box 1000',
                'postalcode' => '44035',
                'city' => 'Elyria',
                'state' => 'OH',
            ]);

        User::factory()
            ->count(22)
            ->create();
    }
}
