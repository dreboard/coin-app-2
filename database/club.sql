


-- content = Club, Video, Article, Book, Website, Reference
-- user_type = collector, dealer, scholar, investor
INSERT INTO `sources` (`name`, `description`, `url`, `design`, `cat_id`, `type_id`,`content`, `user_type`)
VALUES
    ('The Newman Numismatic Portal','Numismatic research and reference material', 'https://nnp.wustl.edu/',29, 113, ''),
    ('Early Coinage of the United States of America','Dollar', 'http://earlydollars.org/',29, 113, ''),
    ('The Combined Organization of Numismatic Error Collectors of America (CONECA)','Education of error and variety coin collectors', 'https://conecaonline.org/',29, 113, ''),
    ('VAMWorld','This site is devoted to Morgan and Peace Dollar die varieties, known as VAM varieties.', 'http://ec2-13-58-222-16.us-east-2.compute.amazonaws.com/wiki/Home',63, 33, ''),
    ('Flying Eagle and Indian Head Cent Die Varieties','This online database is the most complete index of die varieties for early small cents, or pennies, ever assembled. It includes every Flying Eagle and Indian Head die variety from Kevin Flynn, Rick Snow, David Poliquin, Walter Breen, Paul Marvin, Arnold Margolis, Mort Goodman, Sam Thurman, Russell Doughty, and others. All varieties are clearly cross-referenced in our index.', 'https://www.indianvarieties.com/',29, 113, ''),
    ('President','pres', '',29, 113, ''),
    ('President','pres', '',29, 113, ''),
    ('President','pres', '',29, 113, ''),
    ('President','pres', '',29, 113, ''),
    ('President','pres', '',29, 113, ''),
    ('President','pres', '',29, 113, ''),
    -- start videos
    ('Morgan Dollars on YouTube','Morgan Dollar Videos on YouTube', 'https://www.youtube.com/results?search_query=morgan+dollars',63, 33, ''),
    ('Peace Dollars on YouTube','Peace Dollars Videos on YouTube', 'https://www.youtube.com/results?search_query=peace+dollars',63, 64, ''),
    ('Seated Liberty Dollars on YouTube','Seated Liberty Dollars Videos on YouTube', 'https://www.youtube.com/results?search_query=seated+liberty+dollars',63, 59, ''),
    ('Morllars on YouTube','Morgllar Videos on YouTube', '',29, 113, '');


create table `coindetail`
(
    `id`              int(10)                      not null primary key,
    `coin_id`         int(100)      default 29     not null,
    `obverse`         varchar(100)                 not null,
    `reverse`         varchar(100)                 not null,
    `obverse_img`     varchar(100)                 not null,
    `reverse_img`     varchar(100)                 not null,
    `mintage`         int(100)      default 29     not null,
    `composition`     varchar(100)                 not null,
    `weight`          decimal(7, 3) default 0.000  not null,
    `diameter`        decimal(7, 3) default 0.000  not null,
    `thickness`       decimal(7, 3) default 0.000  not null,
    `edge`            varchar(100)                 not null,
    `reeds`           int(100)      default 29     not null,
    `designers`       varchar(100)                 not null,
    `engraver`        varchar(100)                 not null,

)
    charset = utf8;

create index fk_coin
    on coindetail (coin_id);


--
/*


https://lincolncents.net/lincoln-cent-specifications/

*/




























-- end
