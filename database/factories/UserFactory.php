<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{

    protected $model = User::class;

    private $userLevels = ['Beginner', 'Advanced', 'Expert'];

    private $userTypes = ['Collector', 'Dealer', 'Research', 'Investor', 'Writer'];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->userName(),
            'email' => $this->faker->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'is_admin' => 0,
            'profile_visibility' => 1,
            'user_level' => array_rand(array_flip($this->userLevels), 1),
            'user_type' => array_rand(array_flip($this->userTypes), 1),
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'phone' => $this->faker->phoneNumber(),
            'address' => $this->faker->streetAddress(),
            'postalcode' => $this->faker->postcode(),
            'city' => $this->faker->city(),
            'state' => $this->faker->stateAbbr(),
            'remember_token' => Str::random(10),
            'avatar' => 'https://dummyimage.com/250x250/ced4da/6c757d.jpg',
            'incidents' => 0,
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return Factory
     */
    public function unverified(): Factory
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }


    /**
     * Indicate that the user is an admin.
     *
     * @return Factory
     */
    public function isAdmin(): Factory
    {
        return $this->state(function (array $attributes) {
            return [
                'is_admin' => 1,
            ];
        });
    }


    /**
     * Indicate that the user is an admin.
     *
     * @return Factory
     */
    public function withIncidents(): Factory
    {
        return $this->state(function (array $attributes) {
            return [
                'incidents' => 3,
            ];
        });
    }

}
