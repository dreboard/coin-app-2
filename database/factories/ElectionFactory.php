<?php

namespace Database\Factories;

use App\Helpers\ManageGroupHelper;
use App\Models\Groups\Group;
use App\Models\Groups\GroupElection;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ElectionFactory extends Factory
{
    protected $model = GroupElection::class;

    private $electionTypes = ['Board', 'Special', 'General'];

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => date('Y') . 'Board Election',
            'election_type' => array_rand(array_flip($this->electionTypes), 1),
            'group_id' => Group::factory(),
            'roles' => implode(',', array_rand(array_flip(ManageGroupHelper::GROUP_BOARD_POSITIONS), 3)),
            'start_at' => Carbon::now(),
            'expired_at' => Carbon::now()->addDays(5),
        ];
    }
}
