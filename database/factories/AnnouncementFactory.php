<?php

namespace Database\Factories;

use App\Models\Announcement;
use App\Models\Groups\Group;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Announcement>
 */
class AnnouncementFactory extends Factory
{

    protected $model = Announcement::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'group_id' => Group::factory(),
            'user_id' => User::factory(),
            'message' => $this->faker->paragraph(),
        ];
    }



    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return Factory
     */
    public function for_group(): Factory
    {
        return $this->state(function (array $attributes) {
            return [
                'group_id' => 1,
                'user_id' => 1,
            ];
        });
    }
}
