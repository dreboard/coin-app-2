<?php

namespace Database\Factories;

use App\Models\Comment;
use App\Models\Forum;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Forum>
 */
class ForumFactory extends Factory
{

    protected $model = Forum::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $title = $this->faker->sentence();
        return [
            'user_id' => User::factory(),
            'title' => $title,
            'body' => $this->faker->paragraph(10),
            'slug' => Str::of($title)->slug('_'),
            'views' => rand(0,51),
        ];
    }

    public function withComments()
    {
        return $this->afterCreating(function (Forum $forum) {
            Comment::factory()->count(rand(6,11))->create([
                'commentable_id' => $forum->id,
                'commentable_type' => Forum::class
            ]);
        });
    }
}
