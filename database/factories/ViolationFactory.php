<?php

namespace Database\Factories;


use App\Http\Controllers\Users\UserViolationsController;
use App\Models\User;
use App\Models\Users\Violation;
use Illuminate\Database\Eloquent\Factories\Factory;


class ViolationFactory extends Factory
{

    protected $model = Violation::class;

    private array $violationTypes = [
        'Harassment',
        'Illegal Activity',
        'Terms of Services',
        'Anti Spam Policy',
        'Prohibited Activities',
        'Uploaded Images Policy'
    ];

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $violator = User::factory()->create();
        $reporter = User::factory()->create();
        return [
            'violation_type' => array_rand(array_flip($this->violationTypes), 1),
            'reporter' => $reporter->id,
            'violator' => $violator->name,
            'violator_id' => $violator->id,
            'details' => $this->faker->sentence(),
            'url' => $this->faker->url(),
            'screenshot' => '',
            'action' => UserViolationsController::ViolationActions['ACTION_PENDING'],
        ];
    }
}
