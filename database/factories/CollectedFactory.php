<?php

namespace Database\Factories;

use App\Models\Coins\Collected;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class CollectedFactory extends Factory
{

    protected $model = Collected::class;


    private $grades = [
        'MS-65',
        'VF-20',
        'AU-55',
        'VG-8',
    ];

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'nickname' => $this->faker->words(5),
            'user_id' => User::factory()->create(),
            'coin_id' => $this->faker->numberBetween(1, 6000),
            'grade' => array_rand(array_flip($this->grades), 1),
            'circulated' => rand(1,0),
        ];
    }
}
