<?php

namespace Database\Factories;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Comment>
 */
class CommentFactory extends Factory
{

    protected $model = Comment::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $user = User::factory()->create();
        $post = Post::factory()->create();
        return [
            'user_id' => $user->id,
            'parent_id' => null,
            'body' => $this->faker->paragraph(),
            'commentable_id' => $post->id,
            'commentable_type' => Post::class,
        ];
    }
}
