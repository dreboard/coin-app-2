<?php

namespace Database\Factories;

use App\Models\Groups\Group;
use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Project>
 */
class ProjectFactory extends Factory
{

    protected $model = Project::class;

    private $projectTypes = ['Research', 'General', 'Variety'];

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'title' => $this->faker->sentence(),
            'join_id' => floor(time()-999999999),
            'description' => $this->faker->paragraph(10),
            'overview' => $this->faker->sentence(),
            'type' => array_rand(array_flip($this->projectTypes), 1),
            'private' => rand(0,1),
        ];
    }
}
