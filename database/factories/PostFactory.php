<?php

namespace Database\Factories;

use App\Models\Comment;
use App\Models\Groups\Group;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{

    protected $model = Post::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $title = $this->faker->sentence();
        return [
            'user_id' => User::factory(),
            'group_id' => Group::factory(),
            'title' => $title,
            'body' => $this->faker->paragraph(10),
            'image_url' => 'https://via.placeholder.com/150',
            'slug' => Str::of($title)->slug('_'),
            'publish' => rand(0,1),
        ];
    }

    public function withComments()
    {
        return $this->afterCreating(function (Post $post) {
            Comment::factory()->count(rand(6,11))->create([
                'commentable_id' => $post->id,
                'commentable_type' => Post::class
            ]);
        });
    }

}
