<?php

namespace Database\Factories;

use App\Models\Groups\Group;
use App\Models\Groups\GroupRequest;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class GroupRequestFactory extends Factory
{

    protected $model = GroupRequest::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'group_id' => Group::factory(),
            'user_id'  => User::factory(),
        ];
    }
}
