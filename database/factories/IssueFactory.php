<?php

namespace Database\Factories;

use App\Helpers\ManageGroupHelper;
use App\Models\Groups\Group;
use App\Models\Groups\Issue;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Issue>
 */
class IssueFactory extends Factory
{

    protected $model = Issue::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'issue' => $this->faker->bs(),
            'description' => $this->faker->sentence(),
            'transparency' => array_rand(array_flip(ManageGroupHelper::GROUP_TRANSPARENCY_LEVELS), 1),
            'group_id' => Group::factory(),
            'vote_level' => array_rand(array_flip(ManageGroupHelper::GROUP_VOTE_LEVELS), 1),
            'start_at' => Carbon::now(),
            'expired_at' => Carbon::now()->addDays(5),
        ];
    }
}
