<?php

namespace Database\Factories;

use App\Models\Groups\Agenda;
use App\Models\Groups\Event;
use App\Models\Groups\Group;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Groups\Agenda>
 */
class AgendaFactory extends Factory
{

    protected $model = Agenda::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $event = Event::factory()
        ->create([

        ]);
        return [
            'topic' => $this->faker->bs(),
            'action' => array_rand(array_flip(Agenda::GROUP_AGENDA_ACTIONS), 1),
            'description' => $this->faker->sentence(),
            'event_id' => $event->id,
            'group_id' => Group::factory(),
            'length' => 900,
        ];
    }
}
