<?php

namespace Database\Factories;

use App\Models\Groups\Issue;
use App\Models\Groups\IssueVote;
use App\Models\Model;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Model>
 */
class IssueVoteFactory extends Factory
{

    protected $model = IssueVote::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'issue_id' => Issue::factory(),
            'voter' => User::factory(),
            'vote' => rand(0,1)
        ];
    }
}
