<?php

namespace Database\Factories;

use App\Models\Groups\Group;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class GroupFactory extends Factory
{

    protected $model = Group::class;

    private $groupTypes = ['Open', 'Club', 'Association'];

    private $groupSpecialties = [
        'General Numismatics',
        'Large Cents',
        'Small Cents',
        'Large Cents',
        'Morgan Dollars'
    ];

    private $groupPlaces = [
        'A Building',
        'A Church',
        'McDonalds',
        'Virtual',
    ];

    private $groupMeets = [
        'Every 2nd Thursday',
        'Every 1st Monday',
        'Every 3rd Wednesday',
        'Every 1st of The Month',
    ];


    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $user = User::factory()->create();
        return [
            'name' => $this->faker->company(),
            'group_type' => array_rand(array_flip($this->groupTypes), 1),
            'user_id' => $user->id,
            'description' => $this->faker->paragraph(),
            'short_description' => $this->faker->bs(),
            'image_url' => 'https://dummyimage.com/250x250/ced4da/6c757d.jpg',
            'join_id' => floor(time()-999999999),
            'url' => $this->faker->url(),
            'phone' => $user->phone,
            'email' => $user->email,
            'address' => $user->address,
            'postalcode' => $user->postcode,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'city' => $user->city,
            'state' => $user->state,
            'meets' => array_rand(array_flip($this->groupMeets), 1),
            'place' => array_rand(array_flip($this->groupPlaces), 1),
            'formed' => $this->faker->year('-20 years'),
            'specialty' => array_rand(array_flip($this->groupSpecialties), 1),
            'private' => rand(1,0),
            'extra_info' => $this->faker->sentence(),
            'settings' => $this->faker->sentence(),
        ];
    }


}
