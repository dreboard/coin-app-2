<?php

namespace Database\Factories;

use App\Models\Groups\Group;
use App\Models\Groups\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class GroupRoleFactory extends Factory
{

    protected $model = Role::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'user_id'  => User::factory(),
            'group_id' => Group::factory(),
            'role_id'  => Role::factory(),
            'approved_at' => Carbon::now(),
            'expired_at' => Carbon::now()->addDays(365),
        ];
    }
}
