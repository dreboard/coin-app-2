<?php

namespace Database\Factories;

use App\Helpers\ManageGroupHelper;
use App\Models\Groups\Group;
use App\Models\Groups\GroupElection;
use App\Models\Groups\GroupNominations;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class NomineeFactory extends Factory
{

    protected $model = GroupNominations::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'election_id' => GroupElection::factory(),
            'role_id' => array_rand(array_flip(ManageGroupHelper::GROUP_BOARD_POSITIONS), 1),
            'nominee' => User::factory(),
            'nominator' => User::factory(),
            'start_at' => Carbon::now(),
            'expired_at' => Carbon::now()->addDays(5),
        ];
    }
}
