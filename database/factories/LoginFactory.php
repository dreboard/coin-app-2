<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Users\Login;
use Illuminate\Database\Eloquent\Factories\Factory;


class LoginFactory extends Factory
{

    protected $model = Login::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'ip_address' => $this->faker->ipv6(),
            'created_at' => $this->faker->dateTimeBetween('-3 years', 'now'),
            'login_time' => $this->faker->unixTime(),
        ];
    }
}
