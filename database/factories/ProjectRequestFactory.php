<?php

namespace Database\Factories;

use App\Models\Groups\Group;
use App\Models\Groups\GroupRequest;
use App\Models\Model;
use App\Models\Project;
use App\Models\ProjectRequest;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Model>
 */
class ProjectRequestFactory extends Factory
{

    protected $model = ProjectRequest::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'project_id' => Project::factory(),
            'user_id'  => User::factory(),
        ];
    }
}
