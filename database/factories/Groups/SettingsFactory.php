<?php

namespace Database\Factories\Groups;

use App\Models\Groups\Group;
use App\Models\Groups\Settings;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Groups\Settings>
 */
class SettingsFactory extends Factory
{

    protected $model = Settings::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'group_id' => Group::factory(),
            'edit' => rand(0,3),
            'posts' => rand(0,2),
            'events' => rand(0,1),
            'committees' => rand(0,1),
            'officers' => rand(0,1),
            'positions' => rand(0,1),
            'announcements' => rand(0,1),
            'members' => rand(0,1),
            'elections' => rand(0,1),
            'media' => rand(0,1),
        ];
    }
}
