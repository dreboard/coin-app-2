<?php

namespace Database\Factories\Groups;

use App\Models\Groups\Committee;
use App\Models\Groups\Event;
use App\Models\Groups\Group;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Groups\Committee>
 */
class CommitteeFactory extends Factory
{

    protected $model = Committee::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'group_id' => Group::factory(),
            'title' => $this->faker->sentence(),
            'chair' => User::factory(),
            'seats' => 3,
            'details' => $this->faker->paragraph(),
            'type' => 'General', // ['Educational','Nominations','Membership','Show','General']
            'approved_at' => Carbon::now(),
            'expired_at' => Carbon::now()->addHours(1),
        ];
    }
}
