<?php

namespace Database\Factories\Groups;

use App\Helpers\ManageGroupHelper;
use App\Models\Groups\Event;
use App\Models\Groups\Group;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Groups\Event>
 */
class EventFactory extends Factory
{

    protected $model = Event::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => $this->faker->bs(),
            'event_type' => array_rand(array_flip(Event::GROUP_EVENT_TYPES), 1),
            'location' => array_rand(array_flip(Event::GROUP_EVENT_LOCATIONS), 1),
            'address' => $this->faker->streetAddress(),
            'postalcode' => $this->faker->postcode(),
            'city' => $this->faker->city(),
            'state' => $this->faker->stateAbbr(),
            'description' => $this->faker->paragraph(),
            'group_id' => Group::factory(),
            'user_id' => User::factory(),
            'start_at' => Carbon::now(),
            'expired_at' => Carbon::now()->addHours(1),
        ];
    }
}
