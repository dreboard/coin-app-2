<?php

namespace Database\Factories\Groups;

use App\Helpers\ManageGroupHelper;
use App\Models\Groups\Group;
use App\Models\Groups\Report;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Report>
 */
class ReportFactory extends Factory
{

    protected $model = Report::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $user = User::factory()->create();
        $group = Group::factory()->create();
        //$user->groups()->attach($group->id);
        return [
            'title' => $this->faker->sentence(),
            'type' => array_rand(array_flip(ManageGroupHelper::REPORT_TYPES), 1),// ['Activity','Financial','Research','Progress','Annual','Inventory','Performance','Committee']
            'overview' => $this->faker->sentence(),
            'background' => $this->faker->sentence(),
            'discussion' => $this->faker->sentence(),
            'budget' => $this->faker->sentence(),
            'conclusion' => $this->faker->sentence(),
            'recommendations' => $this->faker->sentence(),
            'group_id' => $group->id,
            'user_id' => $user->id,
            'published' => rand(0,1),
            'reportable_id' => $group->id,
            'reportable_type' => Group::class,
        ];
    }
}
