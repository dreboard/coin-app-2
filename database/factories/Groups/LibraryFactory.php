<?php

namespace Database\Factories\Groups;

use App\Models\Groups\Group;
use App\Models\Groups\Library;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Groups\Library>
 */
class LibraryFactory extends Factory
{

    protected $model = Library::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        //$group = Group::inRandomOrder()->take(1)->id;

        return [
            'group_id' => Group::factory(),
            'publication_type' => 'Book',
            'publication_title' => 'A Book Title',
            'isbn' => $this->faker->isbn10(),
            'details' => $this->faker->sentence(),
            'cost' => $this->faker->randomFloat(2),
            'status' => 1,
        ];
    }
}
