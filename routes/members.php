<?php


use App\Http\Controllers\Coins\Forum\CategoryController as ForumCategoryController;
use App\Http\Controllers\Coins\Forum\TypeController as ForumTypeController;
use App\Http\Controllers\Members\ForumController;
use App\Http\Controllers\Members\ImageController;
use App\Http\Controllers\Members\LeaveController;
use App\Http\Controllers\Members\PostController;
use App\Http\Controllers\Members\ProjectController;
use App\Http\Controllers\Members\ResearchController;
use App\Http\Controllers\Members\WriterController;
use App\Http\Controllers\System\MessageController;
use App\Http\Controllers\Users\FollowController;
use App\Models\Coins\CoinType;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth', 'verified'])->group(function () {

    /*
    |-------------------------------------------------------------------------------
    | Member section
    |-------------------------------------------------------------------------------
    | Prefix:         user/ OR .user
    | Controller:     Member/(*))Controller
    | Method:         MIXED
    | Description:    Member actions
    */
    Route::prefix('member')
        ->name('member.')
        ->middleware(['is_banned', 'is_online', 'is_warned'])
        ->group(function () {

            // System messages
            Route::controller(FollowController::class)->group(function () {

                Route::post('/user_follow', 'followUser')->name('user_follow');
                Route::get('/user_who_follow_me', 'usersWhofollowMe')->name('user_who_follow_me');
                Route::get('/users_i_follow', 'userIFollow')->name('users_i_follow');
                Route::get('/groups_i_follow', 'groupsIFollow')->name('groups_i_follow');
                Route::post('/remove_all', 'removeAllFollowingUsers')->name('remove_all');
                Route::post('/remove_user', 'removeFollowingUser')->name('remove_user');
                Route::post('/unfollow_all', 'unfollowAll')->name('unfollow_all');
                Route::post('/unfollow_model', 'unfollowModel')->name('unfollow_model');
                Route::post('/user_contact_followers', 'contactFollowers')->name('user_contact_followers');

            });

            // Basic account actions
            Route::controller(ForumController::class)->group(function () {
                Route::get('/forum', 'index')->name('forum');
                Route::get('/forum_create', 'create')->name('forum_create');
                Route::get('/forum_user/{id}', 'getForumPostByUser')->name('forum_user');
                Route::post('/forum_save', 'save')->name('forum_save');
                Route::post('/forum_save_edit', 'update')->name('forum_save_edit');
                Route::get('/forum_edit/{forum}', 'edit')->name('forum_edit');

                Route::get('/forum_edit_images/{forum}', 'editImages')->name('forum_edit_images');
                Route::get('/forum_view_image/{id}', 'viewImage')->name('forum_view_image');

                Route::get('/forum_category/{slug}', 'category')->name('forum_category');
                Route::get('/view_forum_post/{forum}', 'view')->name('view_forum_post');
                Route::post('/save_forum_comment', 'saveComment')->name('save_forum_comment');
                Route::post('/save_forum_comment_reply', 'saveCommentReply')->name('save_forum_comment_reply');


                // type

                Route::get('/forum_related/{id}/{model}', 'viewById')->name('forum_related');
                Route::get('/forum_error/{id}', 'viewByError')->name('forum_error');
                Route::get('/forum_coin/{id}', 'viewByCoin')->name('forum_coin');

            });


            Route::controller(ForumCategoryController::class)->group(function () {
                Route::get('/forum_cat_index/{cat_id}', 'index')->name('forum_cat_index');
                Route::get('/get_category_info/{cat_id}', 'getCategoryInfo')->name('get_category_info');

            });


            Route::controller(ForumTypeController::class)->group(function () {
                Route::get('/forum_type_index/{type_id}', 'index')->name('forum_type_index');
                Route::get('/get_type_info/{type_id}', 'getTypeInfo')->name('get_type_info');


            });

            // Basic account actions
            Route::controller(ImageController::class)->group(function () {
                Route::get('/view_image/{image}', 'viewImage')->name('view_image');
                Route::get('/view_images/{model}/{id}', 'viewImages')->name('view_images');
                Route::post('/image_delete', 'delete')->name('image_delete');
                Route::post('/image_edit', 'update')->name('image_edit');
                Route::post('/save_image_comment', 'saveComment')->name('save_image_comment');

            });

            // Basic account actions
            Route::controller(ResearchController::class)->group(function () {
                Route::get('/research', 'index')->name('research');

            });

            // leave/delete groups, forums, post comments
            Route::controller(LeaveController::class)->group(function () {
                Route::post('/leave_forum', 'leaveForum')->name('leave_forum');

            });

            // Book reviews and ratings
            Route::controller(WriterController::class)->group(function () {
                Route::get('/writers', 'index')->name('writers');
                Route::get('/view_books', 'viewBooks')->name('view_books');
                Route::get('/view_book/{id}', 'viewBook')->name('view_book');
                Route::get('/review_book/{id}', 'reviewBook')->name('review_book');
                Route::post('/save_review', 'saveBookReview')->name('save_review');
                Route::get('/all_books', 'bookReviews')->name('all_books');
            });

            // Basic account actions
            Route::controller(PostController::class)->group(function () {
                Route::get('/posts_index', 'index')->name('posts_index');
                Route::get('/my_posts', 'myPosts')->name('my_posts');
                Route::get('/unpublished_posts', 'myUnpublishedPosts')->name('unpublished_posts');
                Route::get('/user_posts/{id}', 'userPosts')->name('user_posts');
                Route::get('/create_posts', 'create')->name('create_posts');
                Route::post('/save_posts', 'store')->name('save_posts');
                Route::get('/edit_posts/{post}', 'edit')->name('edit_posts');
                Route::post('/save_posts_edit', 'update')->name('save_posts_edit');
                Route::get('/view_post/{post}', 'view')->name('view_post');

                Route::post('/save_post_comment', 'saveComment')->name('save_post_comment');
                Route::post('/save_post_comment_reply', 'saveCommentReply')->name('save_post_comment_reply');

                Route::get('/posts_by_tag/{tag}', 'postsByTag')->name('posts_by_tag');

            });

            // Basic account actions
            Route::controller(ProjectController::class)->group(function () {
                Route::get('/project_index', 'index')->name('project_index');
                Route::get('/project_intro', 'intro')->name('project_intro');
                Route::get('/project_create', 'create')->name('project_create');
                Route::get('/project_coin_create/{id}', 'createCoinProject')->name('project_coin_create');
                Route::get('/user_projects', 'projects')->name('user_projects');
                Route::get('/project_edit/{project}', 'edit')->name('project_edit');
                Route::post('/project_save', 'save')->name('project_save');
                Route::post('/project_update', 'update')->name('project_update');
                Route::get('/project_view/{project}', 'view')->name('project_view');
                Route::get('/project_public_view/{project}', 'publicView')->name('project_public_view');
                Route::post('/save_project_comment', 'saveComment')->name('save_project_comment');
                Route::post('/save_project_comment_reply', 'saveCommentReply')->name('save_project_comment_reply');
                Route::post('/project_contact', 'contact')->name('project_contact');
                Route::post('/project_progress', 'progress')->name('project_progress');
                Route::post('/project_delete', 'delete')->name('project_delete');

            });



        });






});
