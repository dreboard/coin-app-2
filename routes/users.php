<?php


use App\Http\Controllers\Members\PostController;
use App\Http\Controllers\Members\ProjectController;
use App\Http\Controllers\Members\ResearchController;
use App\Http\Controllers\Members\WriterController;
use App\Http\Controllers\System\MessageController;
use App\Http\Controllers\Users\DashController;
use App\Http\Controllers\Users\FollowController;
use App\Http\Controllers\Users\InviteController;
use App\Http\Controllers\Users\UserActionsController;
use App\Http\Controllers\Users\UserController;
use App\Http\Controllers\Users\UserViolationsController;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth', 'verified'])->group(function () {


    /*
    |-------------------------------------------------------------------------------
    | User section
    |-------------------------------------------------------------------------------
    | Prefix:         user/ OR .user
    | Controller:     Users/UserController
    | Method:         MIXED
    | Description:    User actions
    */
    Route::prefix('user')
        ->name('user.')
        ->middleware(['is_banned', 'is_online'])
        //->middleware(['forbid-banned-user', 'is_online'])
        ->group(function () {

            // General routes and settings
            Route::controller(DashController::class)->group(function () {
                Route::get('/dashboard', 'index')->name('dashboard');
                Route::get('/is_new', 'newUser')->name('is_new');


            });

            // System messages
            Route::controller(MessageController::class)->group(function () {

                Route::get('/message_all', 'all')->name('message_all');
                Route::get('/message_create', 'create')->name('message_create');
                Route::get('/message_view/{id}', 'view')->name('message_view');
                Route::post('/message_delete', 'destroy')->name('message_delete');
                Route::post('/message_reply', 'reply')->name('message_reply');
                Route::post('/message_save', 'save')->name('message_save');

            });

            Route::controller(UserController::class)->group(function () {
                Route::get('/user_profile', 'viewProfile')->name('user_profile');
                Route::get('/my_area', 'viewMyArea')->name('my_area')->middleware('is_warned');
                Route::get('/public/{id}', 'viewPublicProfile')->name('public')->middleware('is_warned');
                Route::get('/edit_public', 'editPublicProfile')->name('edit_public')->middleware('is_warned');
                Route::get('/warning_page', 'viewWarningPage')->name('warning_page');
                Route::get('/intro_page', 'viewIntroPage')->name('intro_page')->middleware('is_warned');

                Route::post('/user_contact', 'contact')->name('user_contact')->middleware('is_warned');
                Route::get('/view_directory', 'viewDirectory')->name('view_directory')->middleware('is_warned');
                Route::get('/search/{param}', 'search')->name('search')->middleware('is_warned');
                Route::post('/searchForm', 'formSearch')->name('searchForm');
                Route::get('/user_specialties/{specialty}', 'userSpecialties')->name('user_specialties')->middleware('is_warned');
                Route::get('/user_types/{type}', 'getUserType')->name('user_types')->middleware('is_warned');

            });




            Route::middleware(['is_warned'])->group(function () {

                Route::controller(FollowController::class)->group(function () {

                    Route::post('/user_follow', 'followUser')->name('user_follow');
                    Route::get('/user_who_follow_me', 'usersWhofollowMe')->name('user_who_follow_me');
                    Route::get('/users_i_follow', 'userIFollow')->name('users_i_follow');
                    Route::get('/groups_i_follow', 'groupsIFollow')->name('groups_i_follow');
                    Route::post('/remove_all', 'removeAllFollowingUsers')->name('remove_all');
                    Route::post('/remove_user', 'removeFollowingUser')->name('remove_user');
                    Route::post('/unfollow_all', 'unfollowAll')->name('unfollow_all');
                    Route::post('/unfollow_model', 'unfollowModel')->name('unfollow_model');
                    Route::post('/user_contact_followers', 'contactFollowers')->name('user_contact_followers');

                });
                // Basic account actions
                Route::controller(InviteController::class)->group(function () {
                    Route::post('/group_invite', 'inviteToGroup')->name('group_invite');
                    Route::post('/project_invite', 'inviteToProject')->name('project_invite');

                });
                // Basic account actions
                Route::controller(ResearchController::class)->group(function () {
                    Route::get('/research', 'index')->name('research');

                });
                // Basic account actions
                Route::controller(WriterController::class)->group(function () {
                    Route::get('/writers', 'index')->name('writers');

                });
                // Basic account actions
                Route::controller(PostController::class)->group(function () {
                    Route::get('/posts_index', 'index')->name('posts_index');

                });

                // Basic account actions
                Route::controller(ProjectController::class)->group(function () {
                    Route::get('/project_index', 'index')->name('project_index');
                    Route::get('/project_create', 'create')->name('project_create');
                    Route::get('/project_view', 'view')->name('project_view');

                });

                // Basic account actions
                Route::controller(UserActionsController::class)->group(function () {
                    Route::get('/user_change_password', 'changePassword')->name('user_change_password');
                    Route::post('/user_save_password', 'savePassword')->name('user_save_password');
                    Route::get('/user_change_email', 'changeEmail')->name('user_change_email');
                    Route::post('/user_save_email', 'saveEmail')->name('user_save_email');
                    Route::post('/user_change_visibility', 'toggleVisibility')->name('user_change_visibility');
                    Route::post('/save_edit_public', 'savePublicProfile')->name('save_edit_public');
                    Route::post('/user_delete_account', 'deleteAccount')->name('user_delete_account');
                    Route::get('/user_edit_avatar', 'editUserAvatar')->name('user_edit_avatar');
                    Route::post('/user_save_avatar', 'saveUserAvatar')->name('user_save_avatar');
                });

                // Violations
                Route::controller(UserViolationsController::class)->group(function () {
                    Route::get('/violation_create/{id?}', 'violationCreate')->name('violation_create');
                    Route::post('/violation_update', 'violationUpdate')->name('violation_update');
                    Route::post('/violation_save', 'violationSave')->name('violation_save');
                    Route::get('/violation_view/{id}', 'violationView')->name('violation_view');
                    Route::get('/violation_reported', 'violationViewReported')->name('violation_reported');
                    Route::get('/violation_against_user', 'violationViewReportedAgainst')->name('violation_against_user');
                });

            });


        });






});
