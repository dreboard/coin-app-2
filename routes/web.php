<?php


use App\Http\Controllers\BannedController;
use App\Http\Controllers\Groups\ClubController;
use App\Http\Controllers\News\RSSController;
use App\Http\Controllers\News\VideoController;
use App\Http\Controllers\System\MessageController;
use App\Http\Controllers\Users\DashController;
use App\Http\Controllers\Users\UserActionsController;
use App\Http\Controllers\Users\UserController;
use App\Http\Controllers\Users\UserViolationsController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

try {

    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('/clear_cache', function() {
        $exitCode = Artisan::call('config:clear');
        $exitCode = Artisan::call('cache:clear');
        $exitCode = Artisan::call('event:clear');
        $exitCode = Artisan::call('route:clear');
        $exitCode = Artisan::call('view:clear');
        return 'DONE'; //Return anything
    });

    Route::get('/banned', [BannedController::class, 'banned'])->name('banned');
    Route::post('/restore_ban', [BannedController::class, 'restoreBan'])->name('restore_ban');
    /*
    |--------------------------------------------------------------------------
    | Site web Routes
    |--------------------------------------------------------------------------
    |
    */
    Route::middleware(['auth', 'verified'])->group(function () {

        Route::get('/policies', [DashController::class, 'viewPolicyPage'])->name('policies');
        Route::get('/view_policy/{id}', [DashController::class, 'viewPolicyTypePage'])->name('view_policy');


        /*
        |-------------------------------------------------------------------------------
        | User section
        |-------------------------------------------------------------------------------
        | Prefix:         user/ OR .user
        | Controller:     Users/UserController
        | Method:         MIXED
        | Description:    User actions
        */
        Route::prefix('user')
            ->name('user.')
            ->middleware(['is_banned', 'is_online'])
            //->middleware(['forbid-banned-user', 'is_online'])
            ->group(function () {

                // General routes and settings
                Route::controller(DashController::class)->group(function () {
                    Route::get('/dashboard', 'index')->name('dashboard');
                });


                Route::controller(UserController::class)->group(function () {
                    Route::get('/user_profile', 'viewProfile')->name('user_profile');
                    Route::get('/directory', 'viewDirectory')->name('directory')->middleware('is_warned');
                    Route::get('/public/{id}', 'viewPublicProfile')->name('public')->middleware('is_warned');
                    Route::get('/warning_page', 'viewWarningPage')->name('warning_page');
                });

                Route::middleware(['is_warned'])->group(function () {

                    // Basic account actions
                    Route::controller(UserActionsController::class)->group(function () {
                        Route::get('/user_change_password', 'changePassword')->name('user_change_password');
                        Route::post('/user_save_password', 'savePassword')->name('user_save_password');
                        Route::get('/user_change_email', 'changeEmail')->name('user_change_email');
                        Route::post('/user_save_email', 'saveEmail')->name('user_save_email');
                        Route::post('/user_change_visibility', 'toggleVisibility')->name('user_change_visibility');
                        Route::post('/user_delete_account', 'deleteAccount')->name('user_delete_account');
                    });

                    // Violations
                    Route::controller(UserViolationsController::class)->group(function () {
                        Route::get('/violation_create', 'violationCreate')->name('violation_create');
                        Route::post('/violation_update', 'violationUpdate')->name('violation_update');
                        Route::post('/violation_save', 'violationSave')->name('violation_save');
                        Route::get('/violation_view/{id}', 'violationView')->name('violation_view');
                        Route::get('/violation_reported', 'violationViewReported')->name('violation_reported');
                        Route::get('/violation_against_user', 'violationViewReportedAgainst')->name('violation_against_user');
                    });

                });


            });

        /*
        |-------------------------------------------------------------------------------
        | Clubs section
        |-------------------------------------------------------------------------------
        | Prefix:         club/ OR .club
        | Controller:     Groups/ClubController, ManageClubController
        | Method:         MIXED
        | Description:    Group actions
        */
        Route::prefix('club')
            ->name('club.')
            ->middleware(['is_banned', 'is_warned'])
            ->group(function () {

                // Basic routes
                Route::controller(ClubController::class)->group(function () {
                    Route::get('/view/{id}', 'view')->name('view');
                    Route::get('/edit/{id}', 'create')->name('edit');
                    Route::post('/update', 'create')->name('update');
                    Route::post('/delete_all', 'destroyAll')->name('delete_all');
                });


            });

        /*
        |-------------------------------------------------------------------------------
        | Messages section
        |-------------------------------------------------------------------------------
        | Prefix:         messages/ OR .messages
        | Controller:     Users/UserController
        | Method:         MIXED
        | Description:    messages actions
        */
        Route::prefix('messages')
            ->name('messages.')
            ->group(function () {
                Route::middleware(['is_warned'])->controller(MessageController::class)->group(function () {
                    Route::get('/', 'index')->name('messages');
                    Route::get('/create', 'create')->name('create');
                    Route::post('/', 'store')->name('store');
                    Route::get('{id}', 'show')->name('show');
                    Route::put('{id}', 'update')->name('update');
                });
       });

        /*
        |-------------------------------------------------------------------------------
        | News section
        |-------------------------------------------------------------------------------
        | Prefix:         news/ OR .news
        | Controller:     News/RSSController
        | Method:         MIXED
        | Description:    news actions
        */
        Route::prefix('news')
            ->name('news.')
            ->group(function () {
                Route::controller(RSSController::class)->group(function () {
                    Route::get('all', 'index')->name('all');
                    Route::get('feed/{slug}', 'loadFeed')->name('feed');
                    Route::get('ana', 'getANA')->name('ana');
                });
            });

        /*
        |-------------------------------------------------------------------------------
        | Video section
        |-------------------------------------------------------------------------------
        | Prefix:         video/ OR .video
        | Controller:     News/VideoController
        | Method:         MIXED
        | Description:    Video actions
        */
        Route::prefix('video')
            ->name('video.')
            ->group(function () {
                Route::controller(VideoController::class)->group(function () {
                    Route::get('all', 'index')->name('all');
                    Route::get('channel/{slug}', 'loadChannel')->name('channel');
                    Route::get('playlist/{slug}', 'loadChannel')->name('playlist');
                    Route::get('url', 'loadChannel')->name('url');
                    Route::get('search', 'loadSearchVideos')->name('search');
                });
            });


    });



    /*
    |-------------------------------------------------------------------------------
    | Auth Routes
    |-------------------------------------------------------------------------------
    | Prefix:         NONE
    | Controller:     RegisteredUserController, AuthenticatedSessionController, PasswordResetLinkController
    | Method:         MIXED
    | Description:    Auth routes
    */
    require __DIR__.'/auth.php';

}catch (\Throwable $e){
    Log::error($e->getMessage());
    return view('welcome');
}
