<?php

use App\Http\Controllers\Groups\AnnouncementController;
use App\Http\Controllers\Groups\CommitteeController;
use App\Http\Controllers\Groups\GroupController;
use App\Http\Controllers\Groups\GroupCreateController;
use App\Http\Controllers\Groups\GroupEventController;
use App\Http\Controllers\Groups\GroupIssueController;
use App\Http\Controllers\Groups\GroupMessageController;
use App\Http\Controllers\Groups\GroupPostController;
use App\Http\Controllers\Groups\GroupRequestController;
use App\Http\Controllers\Groups\GroupUsersController;
use App\Http\Controllers\Groups\GroupVotingController;
use App\Http\Controllers\Groups\LibraryController;
use App\Http\Controllers\Groups\ManageGroupController;
use App\Http\Controllers\Groups\MemberController;
use Illuminate\Support\Facades\Route;

/*
|-------------------------------------------------------------------------------
| Group section
|-------------------------------------------------------------------------------
| Prefix:         group/ OR .group
| Controller:     Groups/GroupController, ManageGroupController
| Method:         MIXED
| Description:    Group actions
*/
Route::middleware(['auth', 'verified'])->group(function () {
    Route::prefix('group')
        ->name('group.')
        ->middleware(['is_banned', 'is_warned'])
        ->group(function () {

            // Basic routes
            Route::controller(GroupController::class)->group(function () {
                Route::get('/index', 'index')->name('index');
                Route::get('/all', 'allGroups')->name('all');
                Route::get('/mine', 'myGroups')->name('mine');
                Route::get('/view_directory', 'viewDirectory')->name('view_directory');
                Route::get('/search/{param}', 'search')->name('search');
                Route::post('/searchForm', 'searchForm')->name('searchForm');

                Route::post('/delete_all', 'destroyAll')->name('delete_all');


                // group events
                Route::get('/calendar', 'groupCalendar')->name('calendar');
                Route::get('/event/{group}', 'groupEvent')->name('event');


            });

            // Group requests
            Route::controller(GroupRequestController::class)->group(function () {
                Route::get('/view_requests/{group}', 'view')->name('view_requests');
                Route::post('/assign_director', 'assignRole')->name('assign_director');
                Route::get('/public/{group}', 'viewPublicPage')->name('public');
                Route::get('/public_preview/{group}', 'viewPublicPagePreview')->name('public_preview');

                Route::get('/application/{group}', 'viewApplicationPage')->name('application');
                Route::post('/request_store', 'store')->name('request_store');
                Route::post('/request_join', 'joinGroup')->name('request_join');
                Route::post('/request_process', 'process')->name('request_process');
                Route::post('/request_process_all', 'processAllRequest')->name('request_process_all');
            });


            // Manage routes
            Route::controller(ManageGroupController::class)->group(function () {

                Route::get('/view/{group}', 'view')->name('view');
                Route::get('/intro/{group}', 'intro')->name('intro');

                Route::get('/manage/{group}', 'manage')->name('manage');
                Route::get('/manage_levels/{group}', 'manageLevels')->name('manage_levels');
                Route::get('/manage_request/{group}', 'manageRequest')->name('manage_request');
                Route::post('/manage_request_save', 'manageRequestSave')->name('manage_request_save');

                Route::get('/transfer_user/{group}', 'groupTransferPage')->name('transfer_user');
                Route::post('/transfer', 'groupTransferOwner')->name('transfer');
                Route::post('/transfer_request', 'groupTransferOwner')->name('transfer_request');

                Route::get('/edit/{group}', 'edit')->name('edit');
                Route::post('/delete_group', 'deleteGroup')->name('delete_group');
                Route::get('/delete_group_page/{group}', 'deleteGroupForm')->name('delete_group_page');
                Route::post('/save_group_edit', 'saveEdit')->name('save_group_edit');
                Route::post('/save', 'save')->name('save');


                Route::post('/change_manage_settings', 'saveSettings')->name('change_manage_settings');


            });

            // Group messages
            Route::controller(MemberController::class)->group(function () {
                // Group messages
                Route::get('/members_home/{group}', 'index')->name('members_home');

            });
            // Group posts
            Route::controller(GroupPostController::class)->group(function () {
                // Group messages
                Route::get('/view_editor/{group}', 'viewEditor')->name('view_editor');
                Route::get('/posts_home/{group}', 'index')->name('posts_home');
                Route::get('/create_post/{group}', 'create')->name('create_post');
                Route::post('/save_post', 'store')->name('save_post');
                Route::post('/save_post_edit', 'save')->name('save_post_edit');
                Route::get('/view_post/{group}/{post}', 'view')->name('view_post');
                Route::get('/edit_post/{group}/{post}', 'edit')->name('edit_post');

                Route::post('/save_post_comment', 'saveComment')->name('save_post_comment');
                Route::post('/save_post_comment_reply', 'saveCommentReply')->name('save_post_comment_reply');
            });
            // Group events
            Route::controller(GroupEventController::class)->group(function () {
                // Group messages
                Route::get('/events_home/{group}', 'index')->name('events_home');
                Route::get('/create_event/{group}', 'create')->name('create_event');
                Route::post('/save_event', 'save')->name('save_event');
                Route::get('/view_event/{group}/{event}', 'view')->name('view_event');
                Route::get('/event_check_in/{group}/{event}', 'view')->name('event_check_in');
                Route::post('/change_agenda_action', 'changeAgendaAction')->name('change_agenda_action');
                Route::post('/add_participant_to_event', 'addParticipantToEvent')->name('add_participant_to_event');

            });

            // Group messages
            Route::controller(GroupMessageController::class)->group(function () {
                // Group messages
                Route::get('/message_index/{group}', 'index')->name('message_index');
                Route::get('/group_inbox/{group}', 'inbox')->name('group_inbox');
                Route::get('/message_view/{group}/{id}', 'view')->name('message_view');

                Route::post('/group_message_delete', 'destroy')->name('group_message_delete');
                Route::post('/group_message_delete_all', 'destroyAll')->name('group_message_delete_all');

                Route::get('/group_contact_form/{group}', 'contactForm')->name('group_contact_form');
                Route::post('/group_contact', 'contact')->name('group_contact');


                Route::get('/message_group/{group}', 'groupMessage')->name('message_group');

            });

            // Group messages
            Route::controller(LibraryController::class)->group(function () {
                // Group messages
                Route::get('/library_index/{group}', 'index')->name('library_index');

            });

            // Group messages
            Route::controller(CommitteeController::class)->group(function () {
                // Group messages
                Route::get('/committee_index/{group}', 'index')->name('committee_index');
                Route::get('/create_committee/{group}', 'create')->name('create_committee');
                Route::post('/save_committee', 'saveCommittee')->name('save_committee');

                // by committee
                Route::get('/view_committee/{group}/{committee}', 'view')->name('view_committee');
                Route::get('/committee_assign_members/{group}/{committee}', 'assignCommitteeMembers')->name('committee_assign_members');
                Route::post('/committee_attach_members', 'saveNewMembers')->name('committee_attach_members');
                Route::post('/remove_committee_member', 'removeCommitteeMember')->name('remove_committee_member');
                Route::post('/committee_new_chair', 'newCommitteeChair')->name('committee_new_chair');

                // issues
                Route::get('/committee_issues/{group}/{committee}', 'viewIssues')->name('committee_issues');
                Route::get('/new_committee_issue/{group}/{committee}', 'createIssue')->name('new_committee_issue');
                Route::get('/view_committee_issue/{group}/{committee}/{report}', 'viewIssue')->name('view_committee_issue');
                Route::post('/save_committee_issue', 'saveIssue')->name('save_committee_issue');
                Route::post('/update_committee_issue', 'updateIssue')->name('update_committee_issue');

                // reports
                Route::get('/committee_reports/{group}/{committee}', 'viewReports')->name('committee_reports');
                Route::get('/new_committee_report/{group}/{committee}', 'createReport')->name('new_committee_report');
                Route::get('/view_committee_report/{group}/{committee}/{report}', 'viewReport')->name('view_committee_report');
                Route::post('/save_committee_report', 'saveReport')->name('save_committee_report');
                Route::post('/update_committee_report', 'updateReport')->name('update_committee_report');

            });

            // Group issues
            Route::controller(GroupIssueController::class)->group(function () {
                // Group issues
                Route::get('/issue_home/{group}', 'index')->name('issue_home');
                Route::get('/create_issue/{group}', 'create')->name('create_issue');
                Route::post('/save_issue', 'save')->name('save_issue');
                Route::get('/view_issue/{group}/{issue}', 'view')->name('view_issue');
                Route::post('/save_issue_vote', 'saveIssueVote')->name('save_issue_vote');

            });

            // Group Creation
            Route::controller(GroupCreateController::class)->group(function () {
                Route::get('/start', 'start')->name('start');
                Route::get('/create', 'create')->name('create');
                Route::get('/create_club', 'createClub')->name('create_club');
                Route::get('/create_association', 'createAssociation')->name('create_association');
                Route::post('/store', 'store')->name('store');
                Route::post('/store_club', 'storeClub')->name('store_club');
                Route::post('/store_association', 'store')->name('store_association');

                Route::post('/save', 'save')->name('save');
            });


            // Group voting
            Route::controller(GroupVotingController::class)->group(function () {
                Route::get('/vote_home/{group}', 'index')->name('vote_home');
                Route::get('/vote_election/{group}/{election}', 'voteElection')->name('vote_election');
                Route::get('/create_vote/{group}', 'create')->name('create_vote');
                Route::post('/save_vote', 'saveElectionVotes')->name('save_vote');
                Route::get('/election_progress/{group}/{election}', 'electionProgress')->name('election_progress');
                Route::get('/election_results/{group}/{election}', 'electionResults')->name('election_results');
                Route::get('/past_elections/{group}', 'pastElections')->name('past_elections');
                Route::get('/past_election/{group}/{election}', 'pastElection')->name('past_election');
                Route::get('/my_voting/{group}', 'myVotingHistory')->name('my_voting');
                Route::get('/create_election/{group}', 'createBoardElection')->name('create_election');
                Route::post('/save_board_election', 'saveBoardElection')->name('save_board_election');
                Route::post('/save_election', 'saveElection')->name('save_election');
                Route::get('/nominate/{group}/{election}', 'nominate')->name('nominate');
                Route::post('/store_vote', 'store')->name('store_vote');

            });


            // announcements
            Route::controller(AnnouncementController::class)->group(function () {
                Route::get('/show_announcement/{announcement}/{group}', 'view')->name('show_announcement');
                Route::get('/edit_announcement/{group}/{announcement}', 'edit')->name('edit_announcement');
                Route::post('/update_announcement', 'update')->name('update_announcement');
                Route::get('/show_announcement_all/{group}', 'all')->name('show_announcement_all');
                Route::get('/create_announcement/{group}', 'create')->name('create_announcement');
                Route::post('/save_announcement', 'save')->name('save_announcement');
                Route::post('/delete_announcement/{id}', 'destroy')->name('delete_announcement');
                Route::post('/delete_announcement_all', 'destroyAll')->name('delete_announcement_all');
            });


            Route::controller(GroupUsersController::class)->group(function () {
                Route::get('/positions/{group}', 'groupViewPositions')->name('positions');

                Route::get('/upgrade', 'upgradeAssociateMembers')->name('upgrade');
                Route::get('/view_positions/{group}', 'groupViewPositions')->name('view_positions');
                Route::get('/view_user_positions/{group}', 'groupPositionsByUser')->name('view_user_positions');
                Route::post('/save_user_position', 'saveUserPosition')->name('save_user_position');

                Route::get('/view_user/{group}/{user_id}', 'viewGroupUser')->name('view_user');
                Route::get('/view_users/{group}', 'viewGroupUsers')->name('view_users');
                Route::get('/view_role/{group}/{role}', 'viewGroupUsersByRole')->name('view_role');
                Route::get('/view_type/{group}/{type}', 'viewGroupUsersByMemberType')->name('view_type');
                Route::post('/remove_user', 'removeUser')->name('remove_user');
                Route::post('/remove_myself', 'removeMyself')->name('remove_myself');
                Route::post('/user_change_position', 'updateUserPosition')->name('user_change_position');
                Route::post('/user_remove_position', 'removeUserPosition')->name('user_remove_position');
                Route::post('/get_user_roles', 'getCurrentUserRoles')->name('get_user_roles');


                Route::post('/user_manage_level', 'changeUserManageLevel')->name('user_manage_level');

            });


        });
});


