<?php
use App\Http\Controllers\Admin\AdminBanUserController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\AdminUserActionsController;
use App\Http\Controllers\Admin\AdminViolateUserController;
use App\Http\Controllers\Admin\AdminWarnUserController;
use Illuminate\Support\Facades\Route;
/*
|-------------------------------------------------------------------------------
| Admin section
|-------------------------------------------------------------------------------
| Prefix:         admin/ OR .admin
| Controller:     Admin/AdminController
| Method:         MIXED
| Description:    Admin actions
*/
Route::middleware(['auth', 'verified'])->group(function () {

    Route::prefix('admin')
        ->middleware(['is_admin'])
        ->name('admin.')
        ->group(function () {

            Route::get('/dashboard', [AdminController::class, 'index'])->name('dashboard');

            // User actions
            Route::controller(AdminUserActionsController::class)->group(function () {
                Route::get('/view_user/{user_id}', 'viewUser')->name('view_user')->where('user_id', '[0-9]+');
                Route::get('/view_user_logins/{user_id}', 'viewUserLogins')->name('view_user_logins')->where('user_id', '[0-9]+');
                Route::get('/view_users', 'viewAllUsers')->name('view_users');
                Route::post('/find_user', 'findUser')->name('find_user');
                Route::post('/verify_user', 'verifyUser')->name('verify_user');
                Route::get('/delete_user/{user_id}', 'deleteUser')->name('delete_user')->where('user_id', '[0-9]+');
                Route::get('/user_status/{user_id}', 'changeUserStatus')->name('user_status')->where('user_id', '[0-9]+');
                // messages
                Route::get('/message_new', 'newMessage')->name('message_new');
                Route::post('/message_system_save', 'saveUserMessage')->name('message_system_save');
                Route::get('/message_user/{user_id}', 'newUserMessage')->name('message_user')->where('user_id', '[0-9]+');
                Route::post('/message_user_save', 'saveUserMessage')->name('message_user_save');
                // Clone users
                Route::get('/clone_user/{user_id}', 'cloneUser')->name('clone_user')->where('user_id', '[0-9]+');

            });


            // Group actions
            Route::controller(\App\Http\Controllers\Admin\GroupController::class)->group(function () {
                // Viewing
                Route::get('/group_all', 'viewGroups')->name('group_all');
                Route::get('/group_view/{id}', 'viewGroup')->name('group_view')->where('id', '[0-9]+');


            });
            // Warned users
            Route::post('/warn_user', [AdminWarnUserController::class, 'warnUser'])->name('warn_user');

            // Banned users
            Route::controller(AdminBanUserController::class)->group(function () {
                Route::post('/ban_user', 'banUser')->name('ban_user');
                Route::get('/unban_user/{user_id}', 'unbanUser')->name('unban_user')->where('user_id', '[0-9]+');
                Route::get('/unban_all_users', 'unbanAllExpiredBannedUsers')->name('unban_all_users');
                Route::get('/view_banned_users', 'viewAllBannedUsers')->name('view_banned_users');
            });

            // Violations reported users
            Route::controller(AdminViolateUserController::class)->group(function () {
                Route::get('/violation_pending', 'viewAllPendingViolations')->name('violation_pending');
                Route::get('/violation_all', 'viewAllViolations')->name('violation_all');
                Route::get('/reported_type/{violation_type}', 'viewAllViolationsByType')->name('reported_type');
                Route::get('/violations_by_user/{user_id}', 'viewReportedByUserViolations')->name('violations_by_user')->where('user_id', '[0-9]+');
                Route::get('/violations_against_user/{user_id}', 'viewReportedAgainstUserViolations')->name('violations_against_user')->where('user_id', '[0-9]+');
            });


        });

});
