<?php

use App\Http\Controllers\Coins\AmericanEagleController;
use App\Http\Controllers\Coins\CoinCategoryController;
use App\Http\Controllers\Coins\CoinCommemorativeController;
use App\Http\Controllers\Coins\CoinController;
use App\Http\Controllers\Coins\CoinGradeController;
use App\Http\Controllers\Coins\CoinTypeController;
use App\Http\Controllers\Coins\CoinVarietyController;
use App\Http\Controllers\Coins\CoinYearController;
use App\Http\Controllers\Coins\Errors\ErrorController;
use App\Http\Controllers\Coins\MetalController;
use App\Http\Controllers\Coins\MintsetController;
use App\Http\Controllers\Coins\Reports\ReportController;
use App\Http\Controllers\Coins\Series\BarberController;
use App\Http\Controllers\Coins\Series\CappedBustController;
use App\Http\Controllers\Coins\Series\ClassicController;
use App\Http\Controllers\Coins\Series\DesignController;
use App\Http\Controllers\Coins\Series\DrapedBustController;
use App\Http\Controllers\Coins\Series\LincolnCentController;
use App\Http\Controllers\Coins\Series\SeatedLibertyController;
use App\Http\Controllers\Collected\CollectedController;
use App\Http\Controllers\Collected\CollectedSetController;
use App\Http\Controllers\Users\UserController;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth', 'verified'])->group(function () {


    /*
    |-------------------------------------------------------------------------------
    | Collected section
    |-------------------------------------------------------------------------------
    | Prefix:         collected/ OR .collected
    | Controller:     Coins/CollectedController
    | Method:         MIXED
    | Description:    Collected coin actions
    */
    Route::prefix('collected')
        ->name('collected.')
        ->middleware(['is_banned', 'is_online'])
        //->middleware(['forbid-banned-user', 'is_online'])
        ->group(function () {

            // General routes and settings
            Route::controller(CollectedController::class)->group(function () {
                Route::get('/index', 'index')->name('index');
                Route::get('/view_collected/{collected}', 'viewCollected')->name('view_collected');


                Route::get('/edit_collected_images/{collected}', 'editImages')->name('edit_collected_images');
                Route::post('/save_collected_images', 'saveImages')->name('save_collected_images');
                Route::post('/view_collected_images', 'viewImages')->name('view_collected_images');
                Route::post('/delete_collected_images', 'saveImages')->name('delete_collected_images');


                Route::get('/add_collected_variety/{collected}', 'createVariety')->name('add_collected_variety');
                Route::get('/start', 'start')->name('start'); // select coin
                Route::get('/save_by_coin_id/{id}', 'createById')->name('save_by_coin_id');
                Route::get('/save_multi_by_coin_id/{id}/{amount}', 'createById')->name('save_multi_by_coin_id');


                Route::post('/save_coin_id', 'save')->name('save_coin_id');
                Route::post('/quick_save_coin_id', 'quickSave')->name('quick_save_coin_id');

                Route::get('/add_variety/{id}', 'addVariety')->name('add_variety');

                Route::get('/collected_variety/{collected}/{variety}', 'showCollectedVarieties')->name('collected_variety');
                Route::get('/collected_variety_list/{collected}/{variety}', 'showCollectedVarietiesList')->name('collected_variety_list');

                Route::post('/collected_assign_variety', 'assignVariety')->name('collected_assign_variety');
                Route::get('/collected_get_info/{collected}', 'getInInfo')->name('collected_get_info');

                // collected bulk data

                Route::get('/collected_get_bulk_data/{type}', 'loadTypeBulkData')->name('collected_get_bulk_data');


                // varieties

                Route::get('/get_variety_list/{coin_id}/{variety}', 'getVarietyData')->name('get_variety_list');
                Route::post('/attach_variety', 'attachVariety')->name('attach_variety');

                // folder, mintset....

                Route::get('/start_folder', 'start')->name('start_folder');
                Route::get('/start_roll', 'start')->name('start_roll');
                Route::get('/start_bag', 'start')->name('start_bag');

                // editing
                Route::post('/save_collected_note', 'saveNote')->name('save_collected_note');
                Route::post('/save_collected_privacy', 'savePrivacy')->name('save_collected_privacy');
                Route::post('/save_collected_lock', 'saveLocked')->name('save_collected_lock');

                // delete
                Route::post('/delete_collected', 'delete')->name('delete_collected');

            });


            // General routes and settings
            Route::controller(CollectedSetController::class)->group(function () {
                Route::get('/start_mintset', 'start')->name('start_mintset');
                Route::get('/populate_set/{set_year}', 'populateSetYears')->name('populate_set');
                Route::get('/populate_set_type/{set_type}', 'populateSetTypes')->name('populate_set_type');
                Route::get('/view_collected_set/{set}', 'viewCollectedSet')->name('view_collected_set');
                Route::get('/view_collected_sets', 'viewCollectedSets')->name('view_collected_sets');

                Route::get('/create_set_by_id/{set_id}', 'createById')->name('create_set_by_id'); // detailed
                Route::get('/create_set_loose/{set_id}', 'createLooseSet')->name('create_set_loose'); //loose sets
                Route::get('/create_set_slab/{set_id}', 'createSlabbedSet')->name('create_set_slab'); //loose sets

                Route::post('/quick_save_set_id', 'quickSave')->name('quick_save_set_id'); // quick
                Route::post('/save_set_by_id', 'saveById')->name('save_set_by_id'); // detailed
                Route::post('/save_set_loose', 'saveLooseSet')->name('save_set_loose'); //loose sets
                Route::post('/save_set_slab', 'saveSlabbedSet')->name('save_set_slab'); //loose sets

                Route::post('/delete_set/{set_id}', 'delete')->name('delete_set');
                Route::post('/delete_set_all/{set_id}', 'deleteAll')->name('delete_set_all');

            });

            // General routes and settings
            Route::controller(ReportController::class)->group(function () {
                Route::get('/report_index', 'index')->name('report_index');


            });

        });


    /*
    |-------------------------------------------------------------------------------
    | Coins section
    |-------------------------------------------------------------------------------
    | Prefix:         coin/ OR .coin
    | Controller:     Coins/CoinController
    | Method:         MIXED
    | Description:    Coin actions
    */
    Route::prefix('coin')
        ->name('coin.')
        ->middleware(['is_banned', 'is_online'])
        //->middleware(['forbid-banned-user', 'is_online'])
        ->group(function () {

            // General routes and settings
            Route::controller(CoinController::class)->group(function () {
                Route::get('/coin_index', 'index')->name('coin_index');
                Route::get('/is_new', 'newUser')->name('is_new');
                Route::get('/view_coin/{coin}', 'viewCoin')->name('view_coin');
                Route::get('/view_coin_subtype/{coin}/{sub_type}', 'viewCoin')->name('view_coin_subtype');
                Route::get('/view_coin_mms/{coin}/{mms}', 'viewCoin')->name('view_coin_mms');

                // reports
                Route::get('/view_coin_color/{id}', 'viewCoin')->name('view_coin_color');
                Route::get('/view_coin_steps/{id}', 'viewCoin')->name('view_coin_steps');
                Route::get('/view_coin_head/{id}', 'viewCoin')->name('view_coin_head');
                Route::get('/view_coin_bell/{id}', 'viewCoin')->name('view_coin_bell');
                Route::get('/view_coin_bands/{id}', 'viewCoin')->name('view_coin_bands');
                Route::get('/view_coin_damage/{id}', 'viewCoin')->name('view_coin_damage');
                Route::get('/view_coin_variety/{id}', 'viewCoin')->name('view_coin_variety');
                Route::get('/view_coin_error/{id}', 'viewCoin')->name('view_coin_error');
                Route::get('/view_coin_color/{id}', 'viewCoin')->name('view_coin_color');



                Route::get('/coin_tags', 'tags')->name('coin_tags');
                Route::get('/coin_genre/{genre}', 'getGenre')->name('coin_genre');
                Route::get('/coin_subgenre', 'tags')->name('coin_subgenre');
                Route::get('/coin_subgenre2', 'tags')->name('coin_subgenre2');
                Route::get('/coin_subset/{subset}', 'getSubset')->name('coin_subset');


                // key dates

                Route::get('/coin_key_dates', 'keyDatesList')->name('coin_key_dates');
                Route::get('/type_key_dates/{type_id}', 'typeKeyDatesList')->name('type_key_dates');

                // @todo CoinDetailsController
                Route::get('/view_coin_mints/{detail}', 'viewCoin')->name('view_coin_mints');
                Route::get('/view_coin_strikes/{detail}', 'viewCoin')->name('view_coin_strikes');
                Route::get('/view_coin_edges/{detail}', 'viewCoin')->name('view_coin_edges');

                // @todo CoinSeriesController
                Route::get('/view_coin_series/{series}', 'viewCoin')->name('view_coin_series');
                // @todo CoinDesignController
                Route::get('/view_coin_design/{design}', 'viewCoin')->name('view_coin_design');
                // @todo CoinGenreController
                Route::get('/view_coin_genre/{genre}', 'viewCoin')->name('view_coin_genre');
                // @todo CoinVersionController
                Route::get('/view_coin_version/{version}', 'viewCoin')->name('view_coin_version');
                // @todo CoinKeyController
                Route::get('/view_coin_key/{id}', 'viewCoin')->name('view_coin_key');

                // @todo



            });


            // General routes and settings
            Route::controller(CoinCommemorativeController::class)->group(function () {
                Route::get('/view_coin_commem_index', 'index')->name('view_coin_commem_index');
                Route::get('/view_coin_commem_type/{type}', 'viewType')->name('view_coin_commem_type');
                Route::get('/view_coin_commem_version/{version}', 'viewVersion')->name('view_coin_commem_version');
                Route::get('/view_coin_commem_category/{category}', 'viewCategory')->name('view_coin_commem_category');
                Route::get('/view_coin_commem_group/{group}', 'viewGroup')->name('view_coin_commem_group');

            });


            // General routes and settings
            Route::controller(AmericanEagleController::class)->group(function () {
                Route::get('/view_eagle_index', 'index')->name('view_eagle_index');

                Route::get('/view_eagle_type/{type}', 'viewType')->name('view_eagle_type');


            });


            // General routes and settings
            Route::controller(MetalController::class)->group(function () {
                Route::get('/metal_index', 'index')->name('metal_index');

                Route::get('/metal_type/{type}', 'viewType')->name('metal_type');


            });

            // General routes and settings
            Route::controller(CoinTypeController::class)->group(function () {
                Route::get('/type_index', 'index')->name('type_index');
                Route::get('/view_type/{id}', 'viewCoinType')->name('view_type');
                Route::get('/populate_type/{type_id}', 'loadTypeCoins')->name('populate_type');


                // reports
                Route::get('/view_type_color/{id}', 'viewCoinType')->name('view_type_color');
                Route::get('/view_type_mint/{coinType}/{mint}', 'viewCoinTypeMint')->name('view_type_mint');
                Route::get('/view_type_mms/{coinType}/{mint}/{mms}', 'viewCoinTypeMintMarkStyle')->name('view_type_mms');
                Route::get('/view_type_strike/{coinType}/{strike}', 'viewCoinTypeStrike')->name('view_type_strike');
                Route::get('/view_type_design_detail/{coinType}/{detail}/{variety}', 'viewCoinTypeDesignVariety')->name('view_type_design_detail');
                Route::get('/view_type_steps/{id}', 'viewCoinType')->name('view_type_steps');
                Route::get('/view_type_head/{id}', 'viewCoinType')->name('view_type_head');
                Route::get('/view_type_bell/{id}', 'viewCoinType')->name('view_type_bell');
                Route::get('/view_type_bands/{id}', 'viewCoinType')->name('view_type_bands');
                Route::get('/view_type_damage/{id}', 'viewCoinType')->name('view_type_damage');
                Route::get('/view_type_variety/{id}', 'viewCoinType')->name('view_type_variety');
                Route::get('/view_type_error/{id}', 'viewCoinType')->name('view_type_error');
                Route::get('/view_type_color/{id}', 'viewCoinType')->name('view_type_color');
                Route::get('/view_type_group/{type}', 'viewCoinType')->name('view_type_group');

            });
            // General routes and settings
            Route::controller(CoinCategoryController::class)->group(function () {
                Route::get('/cat_index', 'index')->name('cat_index');
                Route::get('/view_category/{id}', 'viewCoinCategory')->name('view_category');
                Route::get('/populate_cat/{cat_id}', 'populateCoinType')->name('populate_cat');
                Route::get('/view_cat_collected/{id}', 'viewCoinCategory')->name('view_cat_collected');


            });
            // General routes and settings
            Route::controller(CoinVarietyController::class)->group(function () {
                Route::get('/variety_index', 'index')->name('variety_index');
                Route::get('/cat_index', 'index')->name('cat_index');
                Route::get('/view_variety/{variety}', 'viewVariety')->name('view_variety');
                Route::get('/view_variety_list/{coin_id}/{variety}', 'viewVarietyListFor')->name('view_variety_list');
                Route::get('/view_variety_for_coin/{coin}', 'viewVarietiesForCoin')->name('view_variety_for_coin');
                Route::get('/view_variety_type/{variety}', 'viewVarietyType')->name('view_variety_type');



            });
            // General routes and settings
            Route::controller(CoinGradeController::class)->group(function () {
                Route::get('/cat_index', 'index')->name('cat_index');


                // Grades by type
                Route::get('/type_grades/{coinType}', 'getTypeGrades')->name('type_grades');
                Route::get('/coin_grade/{coinType}/{grade}', 'viewTypeGrade')->name('coin_grade');

                Route::get('/coin_strike_grade/{coinType}/{grade}/{strike}', 'viewTypeStrikeGrade')->name('coin_strike_grade');
                Route::get('/coin_type_strike_grade/{strike}/{coinType}', 'getTypeStrikeGrades')->name('coin_type_strike_grade');

                // Grades by coin
                Route::get('/coin_id_grade/{coin}/{grade}', 'viewGradeForCoin')->name('coin_id_grade');
                Route::get('/coin_id_grades/{coin}', 'getCoinGrades')->name('coin_id_grades');
                Route::get('/coin_id_rank/{collected}', 'getCoinRank')->name('coin_id_rank');

                //ajax details
                Route::get('/coin_id_grade_details/{coin_id}', 'getCoinStrikeDetails')->name('coin_id_grade_details');
                Route::get('/coin_grade_details/{type_id}', 'getTypeStrikeDetails')->name('coin_grade_details');


            });


            // General routes and settings
            Route::controller(MintsetController::class)->group(function () {
                Route::get('/set_index', 'index')->name('set_index');
                Route::get('/set_view/{mintset}', 'viewMintset')->name('set_view');

            });


            // General routes and settings
            Route::controller(CoinYearController::class)->group(function () {
                Route::get('/year_view/{year?}', 'viewYear')->name('year_view');
                Route::get('/year_count_by_coin/{coin_id}/{year}', 'getCoinCollectedCountForCoinYear')->name('year_count_by_coin');
                Route::get('/year_count_by_set/{set_id}/{year}', 'getSetCollectedCountForMintsetYear')->name('year_count_by_set');


                Route::get('/century_view/{century?}', 'viewYear')->name('century_view');

            });

            // Designs---------------------------------------------------------------------------------------------------------



            Route::controller(DesignController::class)->group(function () {
                Route::get('/design_index/{design}', 'index')->name('design_index');
                Route::get('/design_type_set/{design}', 'index')->name('design_type_set');

            });

            Route::controller(SeatedLibertyController::class)->group(function () {
                Route::get('/seated_index', 'index')->name('seated_index');
                Route::get('/seated_year_view/{year?}', 'viewYear')->name('seated_year_view');
                Route::get('/seated_year_count_by_coin/{coin_id}/{year}', 'getCoinCollectedCountForCoinYear')->name('seated_year_count_by_coin');

                Route::get('/seated_type/{type}', 'viewHalf')->name('seated_type');


                Route::get('/barber_index', 'index')->name('barber_index');
                Route::get('/draped_index', 'index')->name('draped_index');


            });


            Route::controller(CappedBustController::class)->group(function () {
                Route::get('/capped_index', 'index')->name('capped_index');
                Route::get('/capped_year_view/{year?}', 'viewYear')->name('capped_year_view');
                Route::get('/capped_year_count_by_coin/{coin_id}/{year}', 'getCoinCollectedCountForCoinYear')->name('capped_year_count_by_coin');

                Route::get('/seated_type/{type}', 'viewHalf')->name('seated_type');



            });
            Route::controller(LincolnCentController::class)->group(function () {
                Route::get('/lincoln_index', 'index')->name('lincoln_index');
                Route::get('/lincoln_index_year_view/{year?}', 'viewYear')->name('lincoln_index_year_view');
                Route::get('/view_lincoln_design_detail/{detail}/{variety}', 'viewCoinTypeDesignVariety')->name('view_lincoln_design_detail');
                Route::get('/view_lincoln_mms/{mint}/{mms}', 'viewCoinTypeMintMarkStyle')->name('view_lincoln_mms');
                Route::get('/lincoln_year_count_by_coin/{coin_id}/{year}', 'getCoinCollectedCountForCoinYear')->name('lincoln_year_count_by_coin');

                Route::get('/view_lincoln_metal/{group}', 'viewLincolnMetal')->name('view_lincoln_metal');
                Route::get('/view_lincoln_by_mint/{mint}', 'viewByMint')->name('view_lincoln_by_mint');
                Route::get('/lincoln_year_count_by_metal/{coin_id}', 'getCoinCollectedCountForCoinYear')->name('lincoln_year_count_by_metal');

                //view_design_variety viewCoinTypeDesignVariety
                Route::get('/view_obv_design_variety/{design}/{variety}', 'viewObverseDesignVarietyByDesign')->name('view_design_variety');
                Route::get('/view_rev_design_variety/{design}/{variety}', 'viewObverseDesignVarietyByDesign')->name('view_rev_design_variety');

                Route::post('/get_other_design_variety', 'getOtherDesigns')->name('get_other_design_variety');



                Route::get('/view_lincoln_bie', 'bieList')->name('view_lincoln_bie');
                Route::get('/view_lincoln_bie_variety/{variety}', 'bieByType')->name('view_lincoln_bie_variety');

            });

            Route::controller(ClassicController::class)->group(function () {
                Route::get('/classic_index', 'index')->name('classic_index');
                Route::get('/classic_index_year_view/{year?}', 'viewYear')->name('classic_index_year_view');

            });

            Route::controller(BarberController::class)->group(function () {
                Route::get('/barber_index', 'index')->name('barber_index');
                Route::get('/barber_index_year_view/{year?}', 'viewYear')->name('barber_index_year_view');

            });

            Route::controller(DrapedBustController::class)->group(function () {
                Route::get('/draped_index', 'index')->name('draped_index');
                Route::get('/draped_index_year_view/{year?}', 'viewYear')->name('draped_index_year_view');

            });

            // Errors---------------------------------------------------------------------------------------------------------

            Route::controller(ErrorController::class)->group(function () {
                Route::get('/error_index', 'index')->name('error_index');
                Route::get('/error_category/{category}', 'viewCategory')->name('error_category');
                Route::get('/error_subcategory/{subcategory}', 'viewSubCategory')->name('error_subcategory');
                Route::get('/error_type/{type}', 'viewSubCategory')->name('error_type');
                Route::get('/error_view/{error}', 'viewError')->name('error_view');

                Route::post('/search_error', 'findError')->name('search_error');


            });



        });


});
