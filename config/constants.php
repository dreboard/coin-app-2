<?php


use App\Helpers\CoinHelper;
use App\Helpers\GradeHelper;
use App\Helpers\ManageGroupHelper;
use App\Helpers\Types\TypeHelper;
use App\Http\Controllers\Users\UserViolationsController;

return [
    // @usage @foreach (Config::get('constants.manage_position_levels') as $k => $position)

    // --------- Groups
    'violation_actions' => UserViolationsController::ViolationActions,

    'board_positions' => ManageGroupHelper::ELECTED_POSITIONS,

    'manage_levels' => ManageGroupHelper::MANAGE_LEVELS,

    'manage_position_levels' => ManageGroupHelper::MANAGE_POSITIONS_LEVEL,

    'translate_vote' => ManageGroupHelper::GROUP_VOTE_RESULT,

    'committee_types' => ManageGroupHelper::GROUP_COMMITTEE_TYPES,
    'report_types' => ManageGroupHelper::REPORT_TYPES,



    // ------- Coins Config::get('constants.coins.cat_id_array')
    'coins' => [
        'ms_grade_prefix' => GradeHelper::COIN_MS_GRADES_PREFIX,
        'pr_grade_prefix' => GradeHelper::COIN_PR_GRADES_PREFIX,

        'business_strikes' => GradeHelper::BUSINESS_STRIKE,
        'proof_strikes' => GradeHelper::PROOF_STRIKE,
        'special_strikes' => GradeHelper::SPECIAL_STRIKE,
        'color_categories' => TypeHelper::COLOR_CATEGORIES,
        'cat_id_array' => TypeHelper::COIN_CATEGORIES,

        'lincoln_reverse' => TypeHelper::LINCOLN_TYPES,
        'jefferson_reverse' => TypeHelper::JEFFERSON_TYPES,
        'full_band_types' => TypeHelper::FULL_BAND_TYPES,

        'types_list' => TypeHelper::COMBINED_LIST,
        'type_name_from_id' => TypeHelper::COIN_TYPES,
        'american_eagle_types' => TypeHelper::AMERICAN_EAGLE_TYPES,


    ],
    'type_id_list' => TypeHelper::TYPES_LIST,
    'cat_id_list' => TypeHelper::COIN_CATEGORIES_LIST,

];
